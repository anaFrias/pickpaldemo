//
//  UploadingView.swift
//  DynamicGlucose
//
//  Created by Ana Victoria Frias on 24/01/18.
//  Copyright © 2018 IW. All rights reserved.
//

import UIKit

public struct UploadingView {
    private static let loadingViewTag = 34
    private static let messageLabelTag = 35
    static var timer : Timer?
    static var isShowing = false
    static var showClose = false
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    public static func show(message: String? = nil){
        DispatchQueue.main.async {
            isShowing = true
            //Add view with the mainscreen size
            
            let view = UIView(frame: UIScreen.main.bounds)
            view.tag = self.loadingViewTag
            
//             Transparent block view
//            let blockView = UIView(frame: view.bounds)
//            blockView.backgroundColor = UIColor.clear
//            view.addSubview(blockView)
            
            // Main View
//            let mainView = UIView(frame: CGRect(x: view.center.x, y: view.center.y, width: 75, height: 75))
//            mainView.center = CGPoint(x: view.center.x, y: view.center.y)
//            mainView.backgroundColor = UIColor.gray
            
            overlayView.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
            overlayView.center = view.center
            overlayView.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
            overlayView.clipsToBounds = true
            overlayView.layer.cornerRadius = 10
        
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator.activityIndicatorViewStyle = .whiteLarge
            activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
            overlayView.addSubview(activityIndicator)
            view.addSubview(overlayView)
            activityIndicator.startAnimating()
            
            //Add label message
            let messageString = message
            let messageLabel = UILabel()
//            messageLabel.font = UIFont.systemFont(ofSize: 30.0, weight: UIFontWeightRegular)
            messageLabel.font = UIFont.systemFont(ofSize: 30.0, weight: .regular)
            messageLabel.textColor = UIColor.white
            messageLabel.textAlignment = .center
            
            
            messageLabel.frame = CGRect(x: 0, y: 100, width: overlayView.frame.width, height: 36)
            messageLabel.text = messageString
            messageLabel.tag = self.messageLabelTag
            overlayView.addSubview(messageLabel)
            
            if let window = UIApplication.shared.keyWindow{
                view.alpha = 0
                UIView.animate(withDuration: 0.1, animations: {
                    window.addSubview(view)
                    view.alpha = 1
                }) { (complete) in
                }
            }
        }
    }
    
    static func stopTimer(){
        
    }
    
    public static func changeMessage(newMessage: String){
        DispatchQueue.main.async {
            if let window = UIApplication.shared.keyWindow{
                if let loadingView = window.viewWithTag(self.loadingViewTag){
                    if let messageLabel = loadingView.viewWithTag(self.messageLabelTag) as? UILabel{
                        messageLabel.text = newMessage
                    }
                }
            }
        }
        
    }
    
    public static func hide(){
        DispatchQueue.main.async {
            isShowing = false            
            if let window = UIApplication.shared.keyWindow{
                if let loadingView = window.viewWithTag(self.loadingViewTag){
                    UIView.animate(withDuration: 0.1, animations: {
                        loadingView.alpha = 0
                    }) { (complete) in
                        activityIndicator.stopAnimating()
                        loadingView.removeFromSuperview()
                        stopTimer()
                    }
                }else{
                    stopTimer()
                }
            }
        }
    }
        
}
