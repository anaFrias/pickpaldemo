//
//  PaymentWS.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol paymentDelegate: class {
    @objc optional func didSuccessSetBillingAddress(billing_address_id: Int)
    @objc optional func didFailSetBillingAddress(error: String, subtitle: String)
    
    @objc optional func didSuccessGetBillingAddress(info: [BillingAddress])
    @objc optional func didFailGetBillingAddress(error: String, subtitle: String)
    
    @objc optional func didSuccessGetSingleBillingAddress(info: BillingAddress)
    @objc optional func didFailGetSingleBillingAddress(error: String, subtitle: String)
    
    @objc optional func didSuccessAddCard()
    @objc optional func didFailAddCard(error: String, subtitle: String)
    
    @objc optional func didSuccessRemoveCard()
    @objc optional func didFailRemoveCard(error: String, subtitle: String)
    
    @objc optional func didSuccessUpdateCard()
    @objc optional func didFailUpdateCard(error: String, subtitle: String)
    
    @objc optional func didSuccessGetCards(cards: [Card])
    @objc optional func didFailGetCards(error: String, subtitle: String)
    
    @objc optional func didSuccessRegisterOrder(code: String, folio: String, order_id: Int, prep_time: Int, allow_process_nopayment: Bool)
    @objc optional func didFailRegisterOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessRegisterCode(codigo_pk: Int, discount: Double, type_discount: Int, type_discount_string: String)
    @objc optional func didFailRegisterCode(error: String, subtitle: String)
    
    @objc optional func didSuccessGetPickpalService(service: Double)
    @objc optional func didFailGetPickpalService(error: String, subtitle: String)
}

class PaymentWS: NSObject {
    
    var delegate: paymentDelegate?
    
    func setBillingAddress(client_id: Int, billing_address_id: Int, RFC: String, nombre_completo: String, email: String, tax_regime: Int, zip_Code: String){
        let parameters = [
            "tax_regime": tax_regime,
            "zip_code": zip_Code,
            "RFC": RFC,
            "client_id":client_id,
            "billing_address_id": billing_address_id,
            "email": email,
            "name": nombre_completo
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.set_billing_addres)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetBillingAddress!(billing_address_id: ((response.value)as! NSDictionary).value(forKey: "billing_address_id") as! Int)
                        break
                    case 400:
                        self.delegate?.didFailSetBillingAddress!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailSetBillingAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailSetBillingAddress!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetBillingAddress!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getBillingAddress(client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.view_billing_addres + "\(client_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
//                    debugPrint(response.value)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let address = self.makeBillingAddress(info: (response.value as! NSDictionary).value(forKey: "billings_address") as! NSArray)
                        self.delegate?.didSuccessGetBillingAddress!(info: address)
                        break
                    case 204:
                        self.delegate?.didFailGetBillingAddress!(error: "empty", subtitle: "")
                        break
                    case 400:
                        self.delegate?.didFailGetBillingAddress!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetBillingAddress!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetBillingAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailGetBillingAddress!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func getSingleAddress(address_id: Int, client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.get_single_address + "\(address_id)/" + "\(client_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print()
                        let address = self.makeSingleBillingAddress(info: (response.value as! NSDictionary) )
                        self.delegate?.didSuccessGetSingleBillingAddress!(info: address)
                        break
                    case 400:
                        self.delegate?.didFailGetSingleBillingAddress!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetSingleBillingAddress!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetSingleBillingAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailGetSingleBillingAddress!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func addNewCardOppa(client_id: Int, token_card: String, session_id: String){
            let parameters = [
                "user_id":client_id,
                "token_card": token_card,
                "device_id": session_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.add_card_oppa)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessAddCard!()
                        break
                    case 400:
                        self.delegate?.didFailAddCard!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailAddCard!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailAddCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailAddCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func deleteCard(client_id: Int, payment_source_id: String){
        let parameters = [
            "user_id":client_id,
            "payment_source_id": payment_source_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.remove_card_oppa)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRemoveCard!()
                        break
                    case 400:
                        self.delegate?.didFailRemoveCard!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailRemoveCard!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRemoveCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRemoveCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func updateCard(client_id: Int, payment_source_id: String, month: String, year: String, name: String){
        let parameters = [
            "user_id":client_id,
            "payment_source_id": payment_source_id,
            "exp_month":month,
            "exp_year": year,
            "name":name
            
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.update_card_oppa)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessUpdateCard!()
                        break
                    case 400:
                        self.delegate?.didFailUpdateCard!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailUpdateCard!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailUpdateCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailUpdateCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func registerOrder(establishment_id: Int, client_id: Int, in_site: Bool, total: Double, detail: [Dictionary<String, Any>], payment_method: String, billing_address: Int, discount_code: Any, notes: String, discount_total: Double, pickpal_service: Double, hour_schedule: String, is_schedule: Bool, in_bar: Bool, percent: Double, number_table: Int, points: Int, type_order: String, delivery_address: Int, shipping_cost: Float,  time_delivery: Int, moneyPoints: Double, pickpal_cash_money:Double){
        
        let temp = ["establishment": establishment_id, "client": client_id, "in_site": in_site, "total": total, "detail": detail, "discount_code": discount_code, "notes": notes, "discount":discount_total, "service_pickpal":pickpal_service, "hour_schedule":hour_schedule , "is_schedule":is_schedule, "in_bar": in_bar, "service_reward": percent, "number_table": number_table, "points": points, "type_order": type_order, "delivery_address": delivery_address, "shipping_cost": shipping_cost, "time_delivery": time_delivery, "money_points": moneyPoints, "pickpal_cash_money": pickpal_cash_money] as [String : Any]
        
        var temp2 = Dictionary<String,Any>()
     //   print("Registrar orden ", temp)
        if payment_method == ""{
            temp2 = ["payment_method": 0, "billing_address": billing_address,"device_session_id": "\(UIDevice.current.identifierForVendor!)"] as [String : Any]
        }else{
            temp2 = ["payment_method": payment_method, "billing_address": billing_address,"device_session_id": "\(UIDevice.current.identifierForVendor!)"] as [String : Any]
        }
        let parameters = [
            "order": temp,
            "payments":temp2
            ] as [String: Any]
        
        print("Registrar orden completa", parameters)
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.register_order_oppa)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                debugPrint(response.result, response.response?.statusCode as Any)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRegisterOrder!(code: (response.value as! NSDictionary).value(forKey: "code") as! String, folio: (response.value as! NSDictionary).value(forKey: "folio") as! String, order_id: (response.value as! NSDictionary).value(forKey: "order_id") as! Int, prep_time: (response.value as! NSDictionary).value(forKey: "preparation_time") as! Int, allow_process_nopayment: (response.value as! NSDictionary).value(forKey: "allow_process_nopayment") as! Bool )
                        break
                    case 400:
                        self.delegate?.didFailRegisterOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailRegisterOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        
                        self.delegate?.didFailRegisterOrder!(error: "Tu transacción ha sido rechazada", subtitle: "Por favor elige otro método de pago")
                        break
                    case 504:
                        self.delegate?.didFailRegisterOrder!(error: "Establecimiento cerrado", subtitle: "El establecimiento se encuentra cerrado")
                        break
                    default:
                        self.delegate?.didFailRegisterOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( let error ):
                    print(error.localizedDescription)
                    self.delegate?.didFailRegisterOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func registerCode(client_id: Int, code: String){
        let parameters = [
            "client_id": client_id,
            "code": code
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.generals)\(Constants.register_code)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRegisterCode!(codigo_pk: ((response.value)as! NSDictionary).value(forKey: "codigo_pk") as! Int, discount: ((response.value)as! NSDictionary).value(forKey: "discount") as! Double, type_discount: ((response.value)as! NSDictionary).value(forKey: "type_discount") as! Int, type_discount_string: ((response.value)as! NSDictionary).value(forKey: "type_discount_string") as! String)
                        break
                    case 203:
                        self.delegate?.didFailRegisterCode!(error: "error", subtitle: ((response.value)as! NSDictionary).value(forKey: "message") as! String)
                        break
                    case 404:
                        self.delegate?.didFailRegisterCode!(error: "error", subtitle: ((response.value)as! NSDictionary).value(forKey: "message") as! String)
                        break
                    case 400:
                        self.delegate?.didFailRegisterCode!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailRegisterCode!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRegisterCode!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRegisterCode!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getPickPalComission(total: Double){
        let parameters = [
            "total": total
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.get_service_pickpal)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                debugPrint(response.result)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
//                        print((response.value as? NSDictionary)?.value(forKey: "service") as? Double)
                        self.delegate?.didSuccessGetPickpalService!(service: (response.value as? NSDictionary)?.value(forKey: "service") as! Double)
                        break
                    case 400:
//                        self.delegate?.didFailRegisterCode!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        self.delegate?.didFailGetPickpalService!(error: "", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetPickpalService!(error: "", subtitle: "")
//                        self.delegate?.didFailRegisterCode!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetPickpalService!(error: "", subtitle: "")
//                        self.delegate?.didFailRegisterCode!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetPickpalService!(error: "", subtitle: "")
//                    self.delegate?.didFailRegisterCode!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getUserCards(client_id: Int){
        let parameters = [
            "user_id":client_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.get_cards_oppa)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let cards = self.makeCards(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetCards!(cards: cards)
                        break
                    case 204:
                        self.delegate?.didFailGetCards!(error: "empty", subtitle: "")
                        break
                    case 409:
                        self.delegate?.didFailGetCards!(error: "empty", subtitle: "")
                        break
                    case 400:
                        self.delegate?.didFailGetCards!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetCards!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetCards!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetCards!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }

}
extension NSObject {
    func makeBillingAddress (info: NSArray ) -> [BillingAddress] {
        var address = [BillingAddress]()
        for add in info {
            let ad = BillingAddress()
            ad.id = (add as! NSDictionary).value(forKey: "id") as! Int
            if let rfc = (add as! NSDictionary).value(forKey: "rfc") as? String {
                ad.RFC = rfc
            }
            if let email_send = (add as! NSDictionary).value(forKey: "email_send") as? String {
                ad.email_send = email_send
            }
            if let full_name = (add as! NSDictionary).value(forKey: "full_name") as? String {
                ad.full_name = full_name
            }
        
            if let tax_regime = (add as! NSDictionary).value(forKey: "tax_regime") as? Int {
                ad.tax_regime = tax_regime
            }
            
            
            if let zipcode = (add as! NSDictionary).value(forKey: "zip_code") as? String {
                ad.zip_code = zipcode
            }
            
            address.append(ad)
        }
        return address
    }
    
    func makeSingleBillingAddress (info: NSDictionary) -> BillingAddress {
        let ad = BillingAddress()
        if let id = info.value(forKey: "id") as? Int, let rfc = info.value(forKey: "rfc") as? String, let email = info.value(forKey: "email_send") as? String, let full_n = info.value(forKey: "full_name") as? String {
            ad.id = id
            ad.RFC = rfc
            ad.email_send = email
            ad.full_name = full_n
        }else{
            ad.id = 0
        }
        return ad
    }
    
    func makeCards (info: NSArray) -> [Card] {
        var cards = [Card]()
        for add in info {
            let car = Card()
            car.card = (add as! NSDictionary).value(forKey: "card") as! String
            car.brand = (add as! NSDictionary).value(forKey: "brand") as! String
            car.payment_source_id = (add as! NSDictionary).value(forKey: "payment_source_id") as! String
            car.name = (add as! NSDictionary).value(forKey: "name") as! String
            cards.append(car)
        }
        return cards
    }
    
}

