//
//  CustomerServiceWS.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 24/03/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation


import Foundation
import Alamofire

@objc protocol CustomerServiceDelegate: class {
    
    @objc optional func getCustomerServiceList(listCustomerService: [EstablishtmentCustomerService])
    @objc optional func didFailGetCustomerServiceList(error: String, subtitle: String)
    
    @objc optional func getSingleCustomerService(infoSingle: MySingleServiceClient )
    @objc optional func didFailGetSingleCustomerService(error: String, subtitle: String)
    
    
    @objc optional func getSingleCustomerServiceById(infoSingle: MySingleServiceClient )
    @objc optional func didFailGetSingleCustomerServiceById(error: String, subtitle: String)
    
    @objc optional func getJoinToTable(establishmentName: String, numberTable: Int)
    @objc optional func didFailJoinToTable(error: String, subtitle: String, code: Int)
    
    
    @objc optional func getJoinToTableOrder(establishmentName: String, numberTable: Int)
    @objc optional func didFailJoinToTableOrder(error: String, subtitle: String)
    @objc optional func didFailJoinToTableUser(error: String, subtitle: String)
    
    
    @objc optional func getMySignedTables(idEstablishment: Int, deskUserId: Int, listTable: [TableJointUser])
    @objc optional func didFailMySignedTables(error: String, subtitle: String)
    
    @objc optional func didDisjointTable()
    @objc optional func didFailDisjointTable(error: String, subtitle: String)
    
    @objc optional func didCheckStatusJoindtable()
    @objc optional func didFailCheckStatusJoindtable(error: String, subtitle: String)
    
    

    
    
    
}


class CustomerServiceWS: NSObject {
    
    
    var delegate: CustomerServiceDelegate?
    
    func getMyListCustomerService(){
  
        let request = Constants.baseURL + Constants.helpdesk + Constants.get_my_service+"\(UserDefaults.standard.object(forKey: "client_id") as! Int)/"
        print("URL: \(request)")
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            let myList = self.makeListCustomerService(info: (response.value! as! NSArray))
                            
                            self.delegate?.getCustomerServiceList!(listCustomerService: myList)
                            
                            break
                        case 204:
                            self.delegate?.didFailGetCustomerServiceList!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailGetCustomerServiceList!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailGetCustomerServiceList!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailGetCustomerServiceList!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        
    }
    
    func getSingleServiceClient(client_id: Int,establishment_id: Int, number_table: Int){
        
        let parameters = ["client_id": client_id, "establishment_id": establishment_id, "number_table":number_table ] as [String : Any]
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.get_my_single_service_client
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        let infoSingel = self.makeInfoCustomerService(info: response.value! as! NSDictionary)
                        
                        self.delegate?.getSingleCustomerService!(infoSingle: infoSingel)
                        
                        break
                    case 400:
                        self.delegate?.didFailGetSingleCustomerService!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetSingleCustomerService!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetSingleCustomerService!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetSingleCustomerService!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetSingleCustomerService!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })

    }
    
    
    
    func mySignedTables(client_id: Int, establishment_id: Int){
        
        let parameters = ["client_id": client_id,"establishment_id":establishment_id] as [String : Any]
        
        let request = Constants.baseURL + Constants.users + Constants.view_my_signed_tables
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        let info = response.value! as! NSDictionary
                        
                        
                        var listTable = [TableJointUser]()
                        var table = info.value(forKey: "table") as! Int
                        var desk_user_id = info.value(forKey: "desk_user_id") as! Int
                        
                        for other_tables in info.value(forKey: "other_tables") as! NSArray{
                        
                            let infoTable = TableJointUser()
                            
                            infoTable.deskUserId = (other_tables as! NSDictionary).value(forKey: "desk_user_id") as! Int
                            infoTable.numberTable = (other_tables as! NSDictionary).value(forKey: "table") as! Int
                            infoTable.establishmentId = (other_tables as! NSDictionary).value(forKey: "establishment_id") as! Int
                            infoTable.establishmentName = (other_tables as! NSDictionary).value(forKey: "establishment_name") as! String
                            listTable.append(infoTable)
                            
                        }
                        
                        self.delegate?.getMySignedTables!(idEstablishment:  table,deskUserId: desk_user_id, listTable: listTable)
                        break
                    case 400:
                        self.delegate?.didFailMySignedTables!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailMySignedTables!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailMySignedTables!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailMySignedTables!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailMySignedTables!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
        
    }
    
    
    
    
    func joinToTableOrder(client_id: Int,folio: String, establishment_id: Int){
        
        let parameters = ["client_id": client_id, "folio": folio, "establishment_id":establishment_id] as [String : Any]
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.join_to_table_order
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        let info = response.value! as! NSDictionary
                        
                        let numberTable = info.value(forKey: "number_table")! as? Int ?? 0
                        let establishment_id = info.value(forKey: "establishment_id")! as? Int ?? 0
                        let establishment_name = info.value(forKey: "establishment_name")! as? String ?? ""
                        let desk_user_id = info.value(forKey: "desk_user_id")! as? Int ?? 0
                        
                        self.delegate?.getJoinToTableOrder!(establishmentName: establishment_name, numberTable: numberTable)
                        
                        break
                    case 400:
                        self.delegate?.didFailJoinToTableOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailJoinToTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.", code: 404)
                        break
                    case 500:
                        self.delegate?.didFailJoinToTableOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailJoinToTableOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                    
                case .failure(let error):
                    
                    if let httpStatusCode = response.response?.statusCode {
                       switch(httpStatusCode) {
                       case 406:
                        self.delegate?.didFailJoinToTableUser!(error: "Error", subtitle: "El folio o código QR pertenece a otro establecimiento.")
                           
                       case 403:
                        self.delegate?.didFailJoinToTableUser!(error: "Error", subtitle: "No puedes unirte a 2 mesas en el mismo establecimiento.")
                       case 404:
                        self.delegate?.didFailJoinToTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.", code: httpStatusCode)
                           break
                       default:
                        self.delegate?.didFailJoinToTableOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                       }
                    } else {
                        self.delegate?.didFailJoinToTableOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    
                    break
                }
            })
        
        
    }
    
   
    
    
    func joinToTable(client_id: Int,folio: String){
        
        let parameters = ["client_id": client_id, "folio": folio] as [String : Any]
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.join_to_table
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        let info = response.value! as! NSDictionary
                        
                        let numberTable = info.value(forKey: "number_table")! as? Int ?? 0
                        let establishment_id = info.value(forKey: "establishment_id")! as? Int ?? 0
                        let establishment_name = info.value(forKey: "establishment_name")! as? String ?? ""
                        let desk_user_id = info.value(forKey: "desk_user_id")! as? Int ?? 0
                        
                        self.delegate?.getJoinToTable!(establishmentName:  establishment_name, numberTable: numberTable)
                        
                        break
                    case 400:
                        self.delegate?.didFailJoinToTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.", code: Int((response.response?.statusCode)!))
                        break
                    case 404:
                        self.delegate?.didFailJoinToTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.", code: Int((response.response?.statusCode)!))
                        break
                    case 500:
                        self.delegate?.didFailJoinToTable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.", code: Int((response.response?.statusCode)!))
                        break
                    default:
                        self.delegate?.didFailJoinToTable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.", code: Int((response.response?.statusCode)!))
                        break
                    }
                    break
                case .failure(let error):
                    
                    if let httpStatusCode = response.response?.statusCode {
                       switch(httpStatusCode) {
                       case 406:
                        self.delegate?.didFailJoinToTableUser!(error: "Error", subtitle: "El Establecimiento no tiene contratado servicio al cliente.")
                           
                       case 403:
                        self.delegate?.didFailJoinToTableUser!(error: "Error", subtitle: "No puedes unirte a 2 mesas en el mismo establecimiento.")
                       case 400:
                        self.delegate?.didFailJoinToTableUser!(error: "Error", subtitle: "El folio o código QR es inválido.")
                      
                       case 404:
                        self.delegate?.didFailJoinToTable!(error: "Ha ocurrido un error", subtitle: "No existe el folio.", code: Int((response.response?.statusCode)!))
                        break
                        
                        
                       default:
                        self.delegate?.didFailJoinToTableOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                       }
                    } else {
                        self.delegate?.didFailJoinToTableOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    break
                }
            })

    }
    
    
    func disJointTable(help_desk_id: Int){
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.disjoint_table+"\(help_desk_id)"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        self.delegate?.didDisjointTable!()
                        
                        break
                    case 400:
                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 403:
//                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: " No se puede desligar por ordenes pendientes o peticiones pendientes.")
                        self.delegate?.didFailDisjointTable!(error: "Aun no puedes desafiliarte ", subtitle: "No podrás \"Salir de la mesa\" hasta que el pedido que ordenaste sea entregado. \n \n En caso de que tengas una solicitud pendiente ya sea \"Llamar mesero\" o \"Pedir cuenta\", esta la deberás de cancelar o esperar a que el mesero atienda tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailDisjointTable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailDisjointTable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailDisjointTable!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })

    }
    
    
    func checkStatusJoindtableOrder(client_id: Int, establishment_id: Int){
        
        let parameters = ["client_id": client_id, "establishment_id": establishment_id] as [String : Any]
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.disjoint_cart_table
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                      
                        self.delegate?.didCheckStatusJoindtable!()
                        
                        break
                    case 400:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 403:
//                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: " No se puede desligar por ordenes pendientes o peticiones pendientes.")
                        self.delegate?.didFailDisjointTable!(error: "Aun no puedes desafiliarte ", subtitle: "No podrás \"Salir de la mesa\" hasta que el pedido que ordenaste sea entregado. \n \n En caso de que tengas una solicitud pendiente ya sea \"Llamar mesero\" o \"Pedir cuenta\", esta la deberás de cancelar o esperar a que el mesero atienda tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure(let error):
                    
                    if let httpStatusCode = response.response?.statusCode {
                       switch(httpStatusCode) {
                       case 406:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "El Folio o código QR pertenece a otro establecimiento.")
                           
                       case 403:
   //                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: " No se puede desligar por ordenes pendientes o peticiones pendientes.")
                        self.delegate?.didFailDisjointTable!(error: "Aun no puedes desafiliarte ", subtitle: "No podrás \"Salir de la mesa\" hasta que el pedido que ordenaste sea entregado. \n \n En caso de que tengas una solicitud pendiente ya sea \"Llamar mesero\" o \"Pedir cuenta\", esta la deberás de cancelar o esperar a que el mesero atienda tu solicitud.")
                           break
                       case 400:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "El Folio o código QR inválido.")
                      
                       case 404:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "El Folio o código QR inválido.")
                        break
                        
                        
                       default:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                       }
                    } else {
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    break
                }
            })


        
    }
    
    
    
    func checkStatusJoindtable(help_desk_id: Int){
        
        let request = Constants.baseURL + Constants.helpdesk + Constants.check_disjoint_table+"\(help_desk_id)"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        self.delegate?.didCheckStatusJoindtable!()
                        
                        break
                    case 400:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 403:
//                        self.delegate?.didFailDisjointTable!(error: "Error", subtitle: " No se puede desligar por ordenes pendientes o peticiones pendientes.")
                        self.delegate?.didFailDisjointTable!(error: "Aun no puedes desafiliarte ", subtitle: "No podrás \"Salir de la mesa\" hasta que el pedido que ordenaste sea entregado. \n \n En caso de que tengas una solicitud pendiente ya sea \"Llamar mesero\" o \"Pedir cuenta\", esta la deberás de cancelar o esperar a que el mesero atienda tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Error", subtitle: "El Folio o código QR inválido.")
                        break
                    case 500:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCheckStatusJoindtable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailCheckStatusJoindtable!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })


        
    }
    
    
    
    func getSingleServiceClientById(help_desk_id: Int){
        
      
        let request = Constants.baseURL + Constants.helpdesk + Constants.get_my_single_service_client+"\(help_desk_id)"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                        let infoSingel = self.makeInfoCustomerService(info: response.value! as! NSDictionary)
                        
                        self.delegate?.getSingleCustomerServiceById!(infoSingle: infoSingel)
                        
                        break
                    case 400:
                        self.delegate?.didFailGetSingleCustomerServiceById!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetSingleCustomerServiceById!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetSingleCustomerServiceById!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetSingleCustomerServiceById!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailGetSingleCustomerService!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })

    }
    
    
    
    
    
    
    
    
    func makeListCustomerService (info: NSArray) -> [EstablishtmentCustomerService] {
    
        
        var infoList = [EstablishtmentCustomerService]()
    
        for list in info {
            
             let item = EstablishtmentCustomerService()
            
            item.establishment_name = (list as! NSDictionary).value(forKey: "establishment_name")! as! String
            item.business_line = (list as! NSDictionary).value(forKey: "business_line")! as! String
            item.establishment_UPPER = (list as! NSDictionary).value(forKey: "establishment_UPPER")! as! String
            item.state = (list as! NSDictionary).value(forKey: "state")! as! String
            item.place = (list as! NSDictionary).value(forKey: "place")! as! String
            item.logo = (list as! NSDictionary).value(forKey: "logo")! as! String
            item.table = (list as! NSDictionary).value(forKey: "table")! as! Int
            item.id_desk_user = (list as! NSDictionary).value(forKey: "id_desk_user")! as! Int
            item.establishment_id = (list as! NSDictionary).value(forKey: "establishment_id")! as! Int
            item.status_request_bill = (list as! NSDictionary).value(forKey: "status_request_bill")! as! Bool
            item.status_request_waiter = (list as! NSDictionary).value(forKey: "status_request_waiter")! as! Bool
            item.image = (list as! NSDictionary).value(forKey: "image")! as! String
            
            
            infoList.append(item)
        }
        return infoList
    
    }
    
    
    
    func makeInfoCustomerService (info: NSDictionary) -> MySingleServiceClient {
    
   
            var pickpalService = [String]()
            var order_types = [String]()
            var messages_history  = [userMessages]()
             let item = MySingleServiceClient()
            
            item.establishment_name = info.value(forKey: "establishment_name")! as? String
            item.business_line = info.value(forKey: "business_line")! as? String
            item.establishment_UPPER = info.value(forKey: "establishment_UPPER")! as? String
            item.state = info.value(forKey: "state")! as? String
            item.place = info.value(forKey: "place")! as? String
            item.logo = info.value(forKey: "logo")! as? String
            item.numberTable = info.value(forKey: "table")! as? Int
            item.establishment_id = info.value(forKey: "establishment_id")! as? Int
            item.status_request_waiter  = info.value(forKey: "status_request_waiter")! as? Bool
            item.status_request_bill  = info.value(forKey: "status_request_bill")! as? Bool
            item.id  = info.value(forKey: "establishment_id")! as? Int
            item.service_plus  = info.value(forKey: "establishment_id")! as? Bool
            item.id_desk_user  = info.value(forKey: "id_desk_user")! as? Int
            item.image = info.value(forKey: "image")! as? String
        
        
            for servicePickPal in info.value(forKey: "pickpal_services") as! NSArray{
                pickpalService.append(servicePickPal as! String)
            }
            item.pickpal_services = pickpalService
            
            for order_type in info.value(forKey: "order_types") as! NSArray{
                order_types.append(order_type as! String)
            }
            item.order_types = order_types
        
            for messagesHistory in info.value(forKey: "messages_history") as! NSArray{
                
                var mesaje = userMessages()
                
                mesaje.message = (messagesHistory as! NSDictionary).value(forKey: "message") as! String
                mesaje.time = (messagesHistory as! NSDictionary).value(forKey: "time") as! String
                mesaje.user = (messagesHistory as! NSDictionary).value(forKey: "username") as! String
                
                messages_history.append(mesaje)
            }
        
        
            item.messages_history = messages_history
        
            return item
    
    }
    
    
}


