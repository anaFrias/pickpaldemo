//
//  OrdersWS.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 07/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol ordersDelegate: class {
    @objc optional func didSuccessGetPendingOrder(id: Int)
    @objc optional func didFailGetPendingOrder(error: String, statusCode: Int, subtitle: String)
    
    @objc optional func didSuccessGetViewOrder(status: String)
    @objc optional func didFailGetViewOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessChangeOrderStatus()
    @objc optional func didFailChangeOrderStatus(error: String, subtitle: String)
    
    @objc optional func didSuccessViewSingleProduct(item: Items)
    @objc optional func didFailViewSingleProduct(error: String, subtitle: String)
    
    @objc optional func didSuccessCheckCard()
    @objc optional func didFailCheckCard(error: String, subtitle: String)
    
    @objc optional func didSuccessGetLastOrder(orders: [MultipleOrders])
    @objc optional func didFailGetLastOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessGetOrdersQuiz(items: [OrderQuizItem])
    @objc optional func didFailGetOrdersQuiz(error: String, subtitle: String)
    
    @objc optional func didSuccessSkipEvaluation()
    @objc optional func didFailSkipEvaluation(error: String, subtitle: String)
    
    @objc optional func didSuccessRetryOrder(order_id: Int)
    @objc optional func didFailRetryOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessCheckRefund(isRefund: Bool, isRefunded: Bool, paymentMethod: Int)
    @objc optional func didFailCheckRefund(error: String, subtitle: String)
    
    @objc optional func didSuccessGetCheckTimeOrder(total_time: Int)
    @objc optional func didFailGetCheckTimeOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessGetWinsOrder(wins: WindsRewards)
    @objc optional func didFailGetWinsOrder(error: String, subtitle: String)
    
    @objc optional func didSuccessGetServicePickPal(service_amount: Double, service_percent: Double)
    @objc optional func didFailGetServicePickPal(error: String, subtitle: String)
    
    @objc optional func didSuccessGetPointsToUse(points: CountEstablishmentPoints)
    @objc optional func didFailGetPointsToUse(error: String, subtitle: String)
    
    @objc optional func didSuccessGetRefundDetails(refundDetails: RefundDetails)
    @objc optional func didFailGetRefundDetails(error: String, subtitle: String)
}

class OrdersWS: NSObject {
    
    var delegate: ordersDelegate!
    
    func viewPendingOrder(client_id: Int){
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.pending_order)\(client_id)/", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response.value)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate.didSuccessGetPendingOrder!(id: (response.value as! NSDictionary).value(forKey: "id") as! Int)
                        break
                    case 204:
                        self.delegate.didFailGetPendingOrder!(error: "Error", statusCode: 204, subtitle: "No hay ordenes pendientes")
                        break
                    case 400:
                        self.delegate.didFailGetPendingOrder!(error: "Error", statusCode: 400, subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetPendingOrder!(error: "Ha ocurrido un error", statusCode: 500, subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetPendingOrder!(error: "Error de comunicación", statusCode: 0, subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getPointToUse(establishment_id: Int, client_id: Int, subtotal: Double ){
        let parameters = [
            "establishment_id":establishment_id,
            "client_id": client_id,
            "subtotal": subtotal
        ] as [String: Any]
        
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.check_points_bysubtotal)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let point = CountEstablishmentPoints()
                        point.amount = (response.value as! NSDictionary).value(forKey: "amount") as? Double
                        point.amount_money = (response.value as! NSDictionary).value(forKey: "amount_money") as? Double
                        point.total_amount = (response.value as! NSDictionary).value(forKey: "total_amount") as? Double
                        point.total_amount_money = (response.value as! NSDictionary).value(forKey: "total_amount_money") as? Double
                        self.delegate.didSuccessGetPointsToUse?(points: point)
                        
                        
                        
                        break
                    case 204:
                        self.delegate.didFailGetPointsToUse!(error: "Error", subtitle: "No tines puntos")
                        break
                    case 400:
                        self.delegate.didFailGetPointsToUse!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetPointsToUse!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    print("Hola")
                    self.delegate?.didFailGetPointsToUse?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    
    
    
    
    
    /*func getPointToUse(establishment_id: Int, client_id: Int, subtotal: Float ){
        let parameters = [
            "establishment_id":establishment_id,
            "client_id": client_id,
            "subtotal": subtotal
            ] as [String: Any]
        Alamofire.request("http://pickpal2.iwsandbox.com/api/orders/count_establishment_points/", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let point = CountEstablishmentPoints()
                        point.amount = (response.value as! NSDictionary).value(forKey: "amount") as? Double
                        point.amount_money = (response.value as! NSDictionary).value(forKey: "amount_money") as? Double
                        self.delegate.didSuccessGetPointsToUse?(points: point)
                        
                        break
                    case 204:
                        self.delegate.didFailGetPointsToUse!(error: "Error", subtitle: "No tines puntos")
                        break
                    case 400:
                        self.delegate.didFailGetPointsToUse!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetPointsToUse!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetPointsToUse!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }*/
    
    func viewMyOrders(order_id: Int){
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.view_my_orders)\(order_id)/", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate.didSuccessGetViewOrder!(status: (response.value as! NSDictionary).value(forKey: "status") as! String)
                        break
                    case 204:
                        self.delegate.didFailGetViewOrder!(error: "Error", subtitle: "No hay ordenes pendientes")
                        break
                    case 400:
                        self.delegate.didFailGetViewOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetViewOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetViewOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getRewardsWins(order_id: Int){
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)points_earned_order/\(order_id)/", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let wins = self.makeWindsRewards(info: (response.value as! NSDictionary))
                        self.delegate.didSuccessGetWinsOrder?(wins: wins)
                        break
                    case 204:
                        self.delegate.didFailGetWinsOrder!(error: "Error", subtitle: "No hay ordenes pendientes")
                        break
                    case 400:
                        self.delegate.didFailGetWinsOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetWinsOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetWinsOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    
    
    
    func changeOrderStatus(order_id: Int, status: Int){
        let parameters = [
            "order_id":order_id,
            "status": status
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.change_order_status)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessChangeOrderStatus!()
                        break
                    case 400:
                        self.delegate?.didFailChangeOrderStatus!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailChangeOrderStatus!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailChangeOrderStatus!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func viewSingleProduct(product_id: Int, establishment_id: Int){
        Alamofire.request("\(Constants.baseURL)\(Constants.menu)\(Constants.view_single_product)\(product_id)/\(establishment_id)/", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let item = self.makeSingleItem(info: response.value as! NSDictionary)
                        self.delegate?.didSuccessViewSingleProduct!(item: item)
                        break
                    case 400:
                        self.delegate?.didFailViewSingleProduct!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailViewSingleProduct!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailViewSingleProduct!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getOrdersQuiz(client_id: Int){
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.get_orders_quiz)\(client_id)/", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let items = (response.value as! NSDictionary).value(forKey: "orders") as! NSArray
                        self.delegate?.didSuccessGetOrdersQuiz!(items: self.makeOrderQuiz(info: items))
                        break
                    case 400:
                        self.delegate?.didFailGetOrdersQuiz!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetOrdersQuiz!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetOrdersQuiz!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func checkCard(client_id: Int, cc: String, month: String, year: String){
        let parameters = [
            "client_id": client_id,
            "cc": cc,
            "month": month,
            "year": year
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.check_card)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessCheckCard!()
                        break
                    case 403:
                        self.delegate?.didFailCheckCard!(error: "Existe", subtitle: "Este número de tarjeta ya fue registrado anteriormente")
                        break
                    case 400:
                        self.delegate?.didFailCheckCard!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCheckCard!(error: "Error", subtitle: "Cliente no existente")
                        break
                    case 500:
                        self.delegate?.didFailCheckCard!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailCheckCard!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailCheckCard!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func get_last_order(client_id: Int){
        let url = "\(Constants.baseURL)\(Constants.orders)\(Constants.get_last_order)\(client_id)/"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let multipleOrders = self.makeMultipleOrders(info: response.value as! NSArray)
                        self.delegate.didSuccessGetLastOrder!(orders: multipleOrders)
                        break
                    case 404:
                        self.delegate?.didFailGetLastOrder!(error: "Error", subtitle: "404")
                        print(response)
                        break
                    case 500:
                        self.delegate?.didFailGetLastOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailGetLastOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetLastOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    if let response = (response.response?.statusCode) as? Int {
                        if response == 404 {
                            self.delegate?.didFailGetLastOrder!(error: "Error", subtitle: "404")
                        }
                    }else{
                        self.delegate?.didFailGetLastOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    break
                }
            })
    }
    
    func getRefundDetails(idOrder: Int){
        let url = "\(Constants.baseURL)\(Constants.orders)\(Constants.get_refund_details)\(idOrder)/"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let info = self.makeInfoRefund(info: response.value as! NSDictionary)
                        self.delegate.didSuccessGetRefundDetails!(refundDetails:  info)
                        break
                    case 404:
                        self.delegate?.didFailGetRefundDetails!(error: "Error", subtitle: "Intentalo de nuevo más tarde.")
                        print(response)
                        break
                    case 500:
                        self.delegate?.didFailGetRefundDetails!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailGetRefundDetails!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetRefundDetails!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    if let response = (response.response?.statusCode) as? Int {
                        if response == 404 {
                            self.delegate?.didFailGetLastOrder!(error: "Error", subtitle: "404")
                        }
                    }else{
                        self.delegate?.didFailGetLastOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    break
                }
            })
    }
    
    func SkipEvaluation(order_id: Int){
        let parameters = [
            "order_id": order_id
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.skip_evaluation)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSkipEvaluation!()
                        break
                    case 404:
                        self.delegate?.didFailSkipEvaluation!(error: "Error", subtitle: "404")
                        break
                    case 500:
                        self.delegate?.didFailSkipEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailSkipEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailSkipEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSkipEvaluation!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func retryOrder(order_id: Int){
        let parameters = [
            "order_id": order_id
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.retry_order)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRetryOrder!(order_id: (response.value as? NSDictionary)?.value(forKey: "order_id") as! Int)
                        break
                    case 404:
                        self.delegate?.didFailRetryOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 500:
                        self.delegate?.didFailRetryOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailRetryOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRetryOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRetryOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func checkRefund(folio: String){
        let parameters = [
            "folio": folio
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.check_refund)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let refund = (response.value as! NSDictionary).value(forKey: "is_refund") as! Bool
                        let refunded = (response.value as! NSDictionary).value(forKey: "is_refunded") as! Bool
                        let payment = (response.value as! NSDictionary).value(forKey: "payment_method") as! Int
                        self.delegate?.didSuccessCheckRefund!(isRefund: refund, isRefunded: refunded, paymentMethod: payment)
                        break
                    case 404:
                        self.delegate?.didFailCheckRefund!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 500:
                        self.delegate?.didFailCheckRefund!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailCheckRefund!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCheckRefund!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailCheckRefund!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func checktimeOrder(establishmentId: Int, client_id: Int, order: [Dictionary<String,Any>]){
        let temp = ["detail": order, "client": client_id, "establishment": establishmentId] as [String : Any]
        print(temp)
        let parameters = [
            "order": temp
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.orders)\(Constants.checktime_order)/", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print((response.value as! NSDictionary).value(forKey: "total_time") as! Int)
                        self.delegate?.didSuccessGetCheckTimeOrder!(total_time: (response.value as! NSDictionary).value(forKey: "total_time") as! Int)
                        break
                    case 404:
                        self.delegate?.didFailGetCheckTimeOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 500:
                        self.delegate?.didFailGetCheckTimeOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 503:
                        self.delegate?.didFailGetCheckTimeOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetCheckTimeOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetCheckTimeOrder!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    func getServicesPickPal(total: Double,type_serivice: String){
       
        let parameters = [
                "total": total,
                "order_type":type_serivice,
                      
                  ] as [String : Any]
              print("  pickpal ", parameters)
        let request = Constants.baseURL+Constants.orders+Constants.get_service_pickpal_updated
           

        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
                
            //print("Response ", response.value!)
                      switch response.result{
                      case .success:
                          switch Int((response.response?.statusCode)!){
                          case 200:
                              print("Response ", response.value!)
    
//                              let service_amount =  ((response.value! as! NSDictionary).value(forKey: "service_amount") as? NSNumber )?.doubleValue
                              
                              let service_amount = (response.value! as! NSDictionary).value(forKey: "service_amount") as! Double
                              let service_percent = ((response.value! as! NSDictionary).value(forKey: "pickpal_service") as? NSNumber )?.doubleValue
                              
                              print("Costo de Pickpal ", service_amount)
                              self.delegate.didSuccessGetServicePickPal?(service_amount: service_amount ?? 0.0, service_percent: service_percent ?? 0.0)
                             
                              break
                          case 204:
                            self.delegate.didFailGetServicePickPal!(error: "No pudimos obtener información", subtitle: "Parece que se pudo obtener el costo de servicio")
                              break
                          case 400:
                              self.delegate.didFailGetServicePickPal!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                              break
                            
                          case 406:
                            
                             let mesagge = (response.value! as! NSDictionary).value(forKey: "message")! as! String
                            
                            self.delegate.didFailGetServicePickPal!(error: "No hay cobertura en tu zona", subtitle: mesagge)
                            break
                          case 500:
                              self.delegate.didFailGetServicePickPal!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                              break
                          default:
                              break
                          }
                          break
                      case .failure(let error):
                          print(error)
                          self.delegate.didFailGetServicePickPal!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                          break
                      }
                  })
          }
}

extension NSObject {
    
    func makeSingleItem (info: NSDictionary) -> Items {
        print(info)
        let itm = Items()
        itm.id = info.value(forKey: "id") as! Int
        itm.images = info.value(forKey: "images") as! [String]
        itm.name = info.value(forKey: "name") as! String
        itm.descr = info.value(forKey: "description") as! String
        itm.is_package = info.value(forKey: "is_package") as! Bool
        itm.prep_time = info.value(forKey: "preparation_time") as! Int
        itm.prep_time_max = info.value(forKey: "preparation_time_max") as! Int
        //itm.rating_neg = info.value(forKey: "rating_negative") as! Int
        //itm.rating_pos = info.value(forKey: "rating_positive") as! Int
        itm.season = info.value(forKey: "season") as? Int
        itm.price = info.value(forKey: "price") as! Double
        itm.limit_extra = info.value(forKey: "limit_extra") as? Int
        var comp = [Complements]()
        if let complements = info.value(forKey: "complements") as? NSArray{
            if complements.count > 0 {
                let compl = Complements()
                compl.complement_name = "Extras"
                var comp_prop = [complement_prop]()
                for comple in complements {
                    let compl_prop = complement_prop()
                    compl_prop.complement_name = (comple as! NSDictionary).value(forKey: "complement_name") as! String
                    compl_prop.id = (comple as! NSDictionary).value(forKey: "id") as! Int
                    compl_prop.value = (comple as! NSDictionary).value(forKey: "price") as! String
                    compl_prop.in_stock = (comple as! NSDictionary).value(forKey: "in_stock") as! Bool
                    compl_prop.is_active = (comple as! NSDictionary).value(forKey: "is_active") as! Bool
                    comp_prop.append(compl_prop)
                }
                compl.complements = comp_prop
                comp.append(compl)
                
            }
            itm.complements = comp
        }
        if let modifiers = info.value(forKey: "modifiers") as? NSArray {
            var modi = [Modifiers]()
            for modifi in modifiers {
                let mod = Modifiers()
                mod.modifiersName = (modifi as! NSDictionary).value(forKey: "category") as! String
                mod.max_required = ((modifi as! NSDictionary).value(forKey: "max") as! Int)
                mod.requiredInfo = ((modifi as! NSDictionary).value(forKey: "required") as! Bool)
                if let modifiers_inside = (modifi as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                    var mod_prop = [modifier_prop]()
                    for modi2 in modifiers_inside {
                        let modifiers_prop = modifier_prop()
                        modifiers_prop.modifier_name = (modi2 as! NSDictionary).value(forKey: "modifier_name") as! String
                        modifiers_prop.id = (modi2 as! NSDictionary).value(forKey: "id") as! Int
                        modifiers_prop.value = (modi2 as! NSDictionary).value(forKey: "price") as! Double
                        modifiers_prop.is_active = (modi2 as! NSDictionary).value(forKey: "is_active") as! Bool
                        mod_prop.append(modifiers_prop)
                    }
                    mod.modifiers = mod_prop
                }
                modi.append(mod)
            }
            itm.modifiers = modi
        }
        return itm
    }
    
    func makeOrderQuiz (info: NSArray) -> [OrderQuizItem] {
        //       print(info)
        var items = [OrderQuizItem]()
        for element in info{
            let item = OrderQuizItem()
            item.id = (element as! NSDictionary).value(forKey: "id") as! Int
            item.delivery_at = (element as! NSDictionary).value(forKey: "delivery_at") as! String
            item.establishment = (element as! NSDictionary).value(forKey: "establishment") as! String
            items.append(item)
        }
        //         print(items)
        return items
        
    }
    func makeMultipleOrders (info: NSArray) -> [MultipleOrders] {
        var orders = [MultipleOrders]()
        //        print(info)
        for order in info {
            let singleOrder = MultipleOrders()
            singleOrder.establishment_id = (order as! NSDictionary).value(forKey: "establishment_id") as! Int
            singleOrder.establishment_name = (order as! NSDictionary).value(forKey: "establishment_name") as! String
            singleOrder.order_id = (order as! NSDictionary).value(forKey: "order_id") as! Int
            singleOrder.register_date = (order as! NSDictionary).value(forKey: "register_date") as! String
            singleOrder.register_date_key = (order as! NSDictionary).value(forKey: "register_date_key") as! String
            singleOrder.register_hour = ((order as! NSDictionary).value(forKey: "register_hour") as! [String])[0]
            singleOrder.points = (order as! NSDictionary).value(forKey: "points") as! Double
            singleOrder.address = (order as! NSDictionary).value(forKey: "address") as! String
            singleOrder.order_type = (order as! NSDictionary).value(forKey: "order_type") as! String
            singleOrder.shipping_cost = (order as! NSDictionary).value(forKey: "shipping_cost") as! Double
            singleOrder.to_refund = (order as! NSDictionary).value(forKey: "to_refund") as! Bool
            singleOrder.days_elapsed = (order as! NSDictionary).value(forKey: "days_elapsed") as! Int
            singleOrder.time_delivery = (order as! NSDictionary).value(forKey: "time_delivery") as! Int
            singleOrder.allow_process_nopayment = (order as! NSDictionary).value(forKey: "allow_process_nopayment") as! Bool
            singleOrder.is_generic = (order as! NSDictionary).value(forKey: "is_generic") as! Bool
                
            singleOrder.est_delivery_hour = (order as! NSDictionary).value(forKey: "est_delivery_hour") as! String
            singleOrder.est_ready_hour = (order as! NSDictionary).value(forKey: "est_ready_hour") as! String
            singleOrder.two_devices = (order as! NSDictionary).value(forKey: "two_devices") as! Bool
            singleOrder.active_customer_service = (order as! NSDictionary).value(forKey: "active_customer_service") as? Bool ?? false
            
            singleOrder.total_cash = (order as! NSDictionary).value(forKey: "total_cash") as? Double ?? 0
            singleOrder.total_to_pay = (order as! NSDictionary).value(forKey: "total_to_pay") as? Double ?? 0
            singleOrder.total_done_payment = (order as! NSDictionary).value(forKey: "total_done_payment") as? Double ?? 0
            singleOrder.total_card = (order as! NSDictionary).value(forKey: "total_card") as? Double ?? 0
            
            //            print(((order as! NSDictionary).value(forKey: "register_hour") as! [String])[0])
            orders.append(singleOrder)
        }
        return orders
    }
    
    func makeWindsRewards(info: NSDictionary) -> WindsRewards{
        let wins = WindsRewards()
        wins.order_id = info.value(forKey: "order_id") as? Int
        wins.points = info.value(forKey: "points") as? Double
        wins.money_points = info.value(forKey: "money_points") as? Double
        wins.establishment_name = info.value(forKey: "establishment_name") as? String
        
        return wins
    }
    
    func makeInfoRefund(info: NSDictionary) -> RefundDetails{
        let refundDetails = RefundDetails()
        refundDetails.refund_points_money = info.value(forKey: "refund_points_money") as? Double
        refundDetails.total = info.value(forKey: "total") as? Double
        refundDetails.refund_pickpal_cash = info.value(forKey: "refund_pickpal_cash") as? Double
        refundDetails.refund_points = info.value(forKey: "refund_points") as? Double
        refundDetails.refund_points_money = info.value(forKey: "refund_points_money") as? Double
        refundDetails.type_refund = info.value(forKey: "type_refund") as? String
        refundDetails.refund_extra = info.value(forKey: "refund_extra") as? Double
        refundDetails.subtotal = info.value(forKey: "subtotal") as? Double
        refundDetails.total_aprox_to_refund = info.value(forKey: "total_aprox_to_refund") as? Double
        refundDetails.refund_extra_method = info.value(forKey: "refund_extra_method") as? String
        refundDetails.fail_percent = info.value(forKey: "fail_percent") as? Double
        refundDetails.order_type = info.value(forKey: "order_type") as? String
        refundDetails.order_number = info.value(forKey: "order_number") as? String
        
        return refundDetails
    }
}
