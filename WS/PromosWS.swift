//
//  PromosWS.swift
//  PickPalTesting
//
//  Created by Hector Vela on 11/25/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol PromosDelegate: class {
    @objc optional func didSuccessGetViewEstablishmentAround(info: [Int], total: Int)
    @objc optional func didFailGetViewEstablishmentAround(error: String, subtitle: String)
    
    @objc optional func didSuccessGetPromoEstablshmentsById(info: [PromoEstablishment])
    @objc optional func didFailGetEstablshmentsById(error: String, subtitle: String)
    
    @objc optional func didSuccessGetLastPromotionals(info: [Promotional])
    @objc optional func didFailGetLastPromotionals(error: String, subtitle: String)
    
    @objc optional func didSuccessGetEstablishmentsAdvs(info: [EstablishmentsAdvs])
    @objc optional func didFailGetEstablishmentsAdvs(error: String, subtitle: String)
    
    @objc optional func didSuccessGetMyPromos(info: AdminPromotionals)
    @objc optional func didFailGetMyPromos(error: String, subtitle: String)
    
    @objc optional func didSuccessDeletePromotional(id: Int)
    @objc optional func didFailDeletePromotional(error: String, subtitle: String)
    
    @objc optional func didSuccessSavePromo()
    @objc optional func didFailSavePromo(error: String, subtitle: String)
    
    @objc optional func didSuccessSingleEstablishment(establishment: PromoSingleEstablishment)
    
    @objc optional func sendMessage(title: String, subtitle: String)
    @objc optional func didSuccessSuscribre(position: Int, isSuscribe: Bool)
    
    @objc optional func didSuccessSingleAdvs(info: SingleEstabAdvs)
    @objc optional func didFailSingleAdvs(error: String, subtitle: String)
    
    @objc optional func didSuccessGetPickPalCount(totalPickPal: Int)
    @objc optional func didFailGetPickPalCount(error: String, subtitle: String)
}

class PromosWS: NSObject {
    
    var delegate: PromosDelegate!
    
    func viewEstablishmentAround(latitude: Double, longitude: Double, codeState: String) {
        let parameters = [
            "latitud": latitude,
            "longitud": longitude,
            "state_code": codeState.stripingDiacritics
            ] as [String:Any]
        let request = Constants.baseURL + Constants.establishment + Constants.view_establishment_around_h2
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                                print(response)
                print("paso")
                print(response)
                switch response.result{
                case .success:
                    //                                        debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let total = (response.value as! NSDictionary).value(forKey: "total_pickpal") as! Int
                        if let ids = (response.value as! NSDictionary).value(forKey: "ids") as? [Int] {
                            self.delegate?.didSuccessGetViewEstablishmentAround!(info: ids, total: total)
                        }else{
                            self.delegate?.didFailGetViewEstablishmentAround!(error: "empty", subtitle: "empty")
                        }
                        
                        break
                    case 400:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        //                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        //                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        //                        self.delegate?.didFailGetAllTypeKitchen!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetViewEstablishmentAround!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getEstablishmentsById(ids: [Int], client_id:Int){
        let parameters = [
            "ids": ids,
            "order_id": false,
            "client_id": client_id
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_establishments_by_ids_h2)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishments = self.makePromoEstablishments(info: (response.value as! NSArray))
                        print((response.value as! NSArray))
                        self.delegate.didSuccessGetPromoEstablshmentsById!(info: establishments)
                        break
                    case 204:
                        self.delegate.didFailGetEstablshmentsById!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetEstablshmentsById!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetEstablshmentsById!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetEstablshmentsById!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getPromotionals(ids: [Int],inicio: Int, fin: Int) {
        let url = Constants.baseURL + Constants.establishment + Constants.get_last_promotionals
        let parameter = [
            "ids":ids,
            "inicio": inicio,
            "fin": fin
            ] as [String: Any]
        Alamofire.request(url,method: .post, parameters: parameter, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response) in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    let promos = self.makePromotional(info: (response.value as! NSArray))
                    self.delegate.didSuccessGetLastPromotionals!(info: promos)
                    break
                case 204:
                    self.delegate.didFailGetLastPromotionals!(error: "No hay promociones", subtitle: "para los filtros seleccionados")
                    break
                case 400:
                    self.delegate.didFailGetLastPromotionals!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                    break
                case 500:
                    self.delegate.didFailGetLastPromotionals!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                default:
                    break
                }
                break
            case .failure(let error):
                print(error)
                self.delegate.didFailGetLastPromotionals!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                break
            }
        })
    }
    
    func getEstablishmentAdvs(client_id: Int) {
        let url = Constants.baseURL + Constants.establishment + Constants.get_establishments_advs + "\(client_id)/"
        print("PROMOS ",url)
        Alamofire.request(url,method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response) in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    print(response.value as! NSArray)
                    let estabAdvs = self.makeEstablishmentsAdvs(info: (response.value as! NSArray))
                    self.delegate.didSuccessGetEstablishmentsAdvs!(info: estabAdvs)
                    break
                case 400:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                    break
                case 404:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Error", subtitle: "No existe el cliente actual.")
                    break
                case 500:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                default:
                    break
                }
                break
            case .failure(let error):
                print(error)
                self.delegate.didFailGetEstablishmentsAdvs!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                break
            }
        })
    }
    
    func businessSuscription(establishmentID: Int, clientID: Int, suscribe: Bool, position: Int){
        print("cliente", clientID)
        print("negocio", establishmentID)
        print("valor", suscribe)
        
        let url = "\(Constants.baseURL)\(Constants.establishment)\((suscribe) ? Constants.suscribe : Constants.unsuscribe)\(establishmentID)/\(clientID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseData(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate.didSuccessSuscribre!(position: position, isSuscribe: suscribe)
                        print("exito")
                        break
                    case 400:
                        self.delegate.sendMessage!(title: "Error", subtitle: "Error en la petición")
                        break
                    case 404:
                        self.delegate.sendMessage!(title: "Sin coincidencias", subtitle: "establecimiento o cliente no encontrado")
                        break
                    default:
                        self.delegate.sendMessage!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            })
        
    }
    
    func viewSingleEstablishment(establishmentID: Int, clientID:Int){
        let url = "\(Constants.baseURL)\(Constants.establishment)\(Constants.view_single_advsestablishment)\(establishmentID)/\(clientID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print(response)
                        let single = self.makePromoSingleEstablishment(info: (response.value as! NSDictionary))
                        print(single)
                        self.delegate.didSuccessSingleEstablishment!(establishment: single)
                        break
                    case 400:
                        self.delegate.sendMessage!(title: "Error", subtitle: "Error en la petición")
                        break
                    case 404:
                        self.delegate.sendMessage!(title: "Sin coincidencias", subtitle: "establecimiento o cliente no encontrado")
                        break
                    default:
                        self.delegate.sendMessage!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            })
        
    }
    
    func viewSingleAdvsEstablishment(establishmentID: Int, clientID:Int){
        let url = "\(Constants.baseURL)\(Constants.establishment)\(Constants.view_single_advsestablishment)\(establishmentID)/\(clientID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let SingleEstabAdvs = self.makeSingleAdvsEstab(info: (response.value as! NSDictionary))
                        self.delegate.didSuccessSingleAdvs?(info: SingleEstabAdvs)
                        break
                    case 400:
                        self.delegate.didFailSingleAdvs?(error: "Error", subtitle: "Error en la petición")
                        break
                    case 404:
                        self.delegate.didFailSingleAdvs?(error: "Sin coincidencias", subtitle: "establecimiento o cliente no encontrado")
                        break
                    default:
                        self.delegate.didFailSingleAdvs?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            })
        
    }
    
    func makePromotional(info: NSArray) -> [Promotional] {
        var promos = [Promotional]()
        
        if info.count > 0{
            let pro = Promotional()
            pro.Header = "Mis Promociones"
            promos.append(pro)
        }
        
        for promoInfo in info {
            let promotional = Promotional()
            var content = NSDictionary()
            content = promoInfo as! NSDictionary
            
            promotional.Header = "Promociones"
            promotional.id = content.value(forKey: "id") as? Int
            promotional.establishment = content.value(forKey: "establishment") as? String
            promotional.category_info = content.value(forKey: "category_info") as? String
            promotional.name = content.value(forKey: "name") as? String
            promotional.descr = content.value(forKey: "description") as? String
            promotional.deadline = content.value(forKey: "deadline") as? String
            promotional.image = content.value(forKey: "image") as? String
            promotional.isPromo = true
            promotional.isCustomImage = content.value(forKey: "is_custom_image") as? Bool
            
            
            
            if let deleted = content.value(forKey: "deleted") as? Bool {
                promotional.deleted = deleted
            }
            promos.append(promotional)
        }
        
        return promos
    }
    
    func makeNews(info: NSArray) -> [Promotional] {
        var promos = [Promotional]()
        if info.count > 0{
            let pro = Promotional()
            pro.Header = "Mis Noticias"
            promos.append(pro)
        }
        
        for promoInfo in info {
            let promotional = Promotional()
            var content = NSDictionary()
            content = promoInfo as! NSDictionary
            
            promotional.Header = "noticias"
            promotional.id = content.value(forKey: "id") as? Int
            promotional.name = content.value(forKey: "name") as? String
            promotional.descr = content.value(forKey: "description") as? String
            promotional.isPromo = false
            promos.append(promotional)
        }
        
        return promos
    }
    
    func makePromoEstablishments (info: NSArray) -> [PromoEstablishment] {
        var st = [PromoEstablishment]()
        for storeInfo in info {
            let storeI = PromoEstablishment()
            let content = storeInfo as! NSDictionary
            
            storeI.id = content.value(forKey: "id") as? Int
            storeI.isOpen = content.value(forKey: "is_open") as? Bool
            var category = NSDictionary()
            if let cat = content.value(forKey: "establishment_category") as? NSDictionary {
                category = cat
            }else{
                category = ["name":""]
            }
            storeI.establishmentCategory  = category.value(forKey: "name") as? String
            storeI.image = content.value(forKey: "image") as? String
            let kitchenType = content.value(forKey: "kitchen_type") as? [String]
            storeI.kitchenType = kitchenType?.joined(separator: ", ")
            storeI.time = content.value(forKey: "time") as? String
            storeI.logo = content.value(forKey: "logo") as? String
            storeI.name = content.value(forKey: "name") as? String
            storeI.state = content.value(forKey: "state") as? String
            storeI.municipality = content.value(forKey: "municipality") as? String
            storeI.futureAvailable = content.value(forKey: "future_available") as? Bool
            storeI.activeNotifications = content.value(forKey: "active_notifications") as? Bool
            storeI.advsCategory = content.value(forKey: "advs_category") as? String
            storeI.subadvsCategory = content.value(forKey: "subadvs_category") as? String
            storeI.currentPromotionals = content.value(forKey: "current_promotionals") as? Int
            storeI.rating = content.value(forKey: "rating") as? Int
            storeI.status = content.value(forKey: "status") as? Int
            storeI.roleRevision = content.value(forKey: "role_revision") as? Int
            storeI.noProcessed = content.value(forKey: "no_processed") as? Int
            storeI.firstActivationDate = content.value(forKey: "first_activation_date") as? String
            storeI.billingStartActive = content.value(forKey: "billing_start_active") as? Bool
            storeI.activationDate = content.value(forKey: "activation_date") as? String
            storeI.createdAt = content.value(forKey: "created_at") as? String
            storeI.updatedAt = content.value(forKey: "updated_at") as? String
            storeI.isAdvs = content.value(forKey: "is_advs") as? Bool
            storeI.noProfit = content.value(forKey: "no_profit") as? Bool
            storeI.administrativeEmail = content.value(forKey: "administrative_email") as? String
            storeI.paymentInfo = content.value(forKey: "payment_info") as? Int
            storeI.geographicalArea = content.value(forKey: "geographical_area") as? Int
            storeI.fiscalAddress = content.value(forKey: "fiscal_address") as? Int
            storeI.socialInformation = content.value(forKey: "social_information") as? Int
            let itemi = content.value(forKey: "item_inactive") as? [String]
            storeI.itemInactive = itemi?.joined(separator: ", ")
            let packagei = content.value(forKey: "package_inactive") as? [String]
            storeI.packageInactive = packagei?.joined(separator: ", ")
            let extrai = content.value(forKey: "extra_inactive") as? [String]
            storeI.extraInactive = extrai?.joined(separator: ", ")
            st.append(storeI)
        }
        return st
    }
    
    func makeEstablishmentsAdvs(info: NSArray) -> [EstablishmentsAdvs] {
        var advs = [EstablishmentsAdvs]() //Josue Valdez.- Creamos la lista de objetos a regresar
        
        let p = EstablishmentsAdvs()
        p.id = 0 //Dato dummi
        advs.append(p)
        
        for esAdvs in info {
            let promotional = EstablishmentsAdvs() //Se crea la variable de tipo objecto (objecto: EstablishmentsAdvs)
            var content = NSDictionary()
            content = esAdvs as! NSDictionary
            
            promotional.id = content.value(forKey: "id") as? Int
            promotional.establishmentName = content.value(forKey: "establishment_name") as? String
            promotional.name = content.value(forKey: "name") as? String
            promotional.RFC = content.value(forKey: "RFC") as? String
            promotional.is_month_payment = content.value(forKey: "is_month_payment") as? Bool
            promotional.descriptionEstablishment = content.value(forKey: "category_info") as? String
            advs.append(promotional)
        }
        return advs
    }
    
    func makeAdminPromotionalsAdvs(info: NSDictionary) -> AdminPromotionals {
        let adminPromotionals = AdminPromotionals()
        var imagesAdvs = [Images]()
        var promotions = [Promotionals]()
        
        adminPromotionals.currents = info.value(forKey: "currents") as? Int
        adminPromotionals.total = info.value(forKey: "total") as? Int
        
        let p3 = Promotionals()
        let p = Promotionals()
        p3.headerAdvs = "Header" //Dato dummi
        promotions.append(p3)
        p.headerAdvs = "PromoTitle" //Dato dummi
        promotions.append(p)
        
        for promos in (info.value(forKey: "promotions") as? NSArray)! {
            let promotional = Promotionals()
            var content = NSDictionary()
            content = promos as! NSDictionary
            
            promotional.headerAdvs = "Promo" //Dato dummi
            promotional.id = content.value(forKey: "id") as? Int
            promotional.establishment_id = content.value(forKey: "establishment_id") as? Int
            promotional.image_key = content.value(forKey: "image_key") as? Int
            promotional.deleted = content.value(forKey: "deleted") as? Bool
            promotional.editable = content.value(forKey: "editable") as? Bool
            promotional.name = content.value(forKey: "name") as? String
            promotional.isNews = false
            promotional.descriptionAdvs = content.value(forKey: "description") as? String
            promotional.start_date = content.value(forKey: "start_date") as? String
            promotional.end_date = content.value(forKey: "end_date") as? String
            promotional.image = content.value(forKey: "image") as? String
            promotional.isCustomImage = content.value(forKey: "is_custom_image") as? Bool
            promotions.append(promotional)
        }
        
        let p1 = Promotionals()
        p1.headerAdvs = "NewsTitle" //Dato dummi
        promotions.append(p1)
        
        for newsIter in (info.value(forKey: "news") as? NSArray)! {
            let newsObject = Promotionals()
            var content = NSDictionary()
            content = newsIter as! NSDictionary
            
            newsObject.headerAdvs = "News" //Dato dummi
            newsObject.id = content.value(forKey: "id") as? Int
            newsObject.establishment_id = content.value(forKey: "establishment_id") as? Int
            newsObject.image_key = content.value(forKey: "image_key") as? Int
            newsObject.deleted = content.value(forKey: "deleted") as? Bool
            newsObject.editable = content.value(forKey: "editable") as? Bool
            newsObject.name = content.value(forKey: "name") as? String
            newsObject.isNews = true
            newsObject.descriptionAdvs = content.value(forKey: "description") as? String
            newsObject.start_date = content.value(forKey: "start_date") as? String
            newsObject.end_date = content.value(forKey: "end_date") as? String
            newsObject.image = content.value(forKey: "image") as? String
            
            
            promotions.append(newsObject)
        }
        
        for images in (info.value(forKey: "images") as? NSArray)! {
            let imagenes = Images()
            var content = NSDictionary()
            content = images as! NSDictionary
            
            imagenes.id = content.value(forKey: "id") as? Int
            imagenes.image = content.value(forKey: "image") as? String
            imagenes.isSelected = false
            imagesAdvs.append(imagenes)
        }
        
        adminPromotionals.images = imagesAdvs
        adminPromotionals.promotions = promotions
        
        return adminPromotionals
    }
    
    func getPromotionalsAdvs(client_id: Int){
        let url = Constants.baseURL + Constants.establishment + Constants.get_last_adminpromotionals + "\(client_id)/"
        print(url)
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response) in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    print(response.value as! NSObject)
                    self.delegate.didSuccessGetMyPromos?(info: self.makeAdminPromotionalsAdvs(info: response.value as! NSDictionary))
                    break
                case 400:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                    break
                case 404:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                    break
                case 500:
                    self.delegate.didFailGetEstablishmentsAdvs!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                default:
                    break
                }
                break
            case .failure(let error):
                print(error)
                self.delegate.didFailGetEstablishmentsAdvs!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                break
            }
        })
    }
    
    func deletePromotionalById(promotional_id: String){
        let url = Constants.baseURL + Constants.establishment + Constants.delete_admin_promotional + "\(promotional_id)/"
        let promoId = promotional_id
        
        print(url)
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response) in
            switch Int((response.response?.statusCode)!){
            case 200:
                self.delegate.didSuccessDeletePromotional!(id: Int(promoId)!)
                break
            case 400:
                self.delegate.didFailDeletePromotional!(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                break
            case 404:
                self.delegate.didFailDeletePromotional!(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                break
            case 500:
                self.delegate.didFailDeletePromotional!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
            default:
                break
            }
        })
    }
    
    func getTotalPickPal(){
        let url = Constants.baseURL + Constants.establishment + Constants.count_establishment
        print(url)
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            (response) in
            switch Int((response.response?.statusCode ?? 500)!){
            case 200:
                let total = (response.value as! NSDictionary).value(forKey: "total_pickpal") as! Int
                self.delegate.didSuccessGetPickPalCount?(totalPickPal: total)
                break
            case 400:
                self.delegate.didFailGetPickPalCount?(error: "Error", subtitle: "No podemos procesar tusolicitud.")
                break
            case 403:
                self.delegate.didFailGetPickPalCount?(error: "Error", subtitle: "Lo sentimos, has excedido el número de notificaciones mensuales")
                break
            case 404:
                self.delegate.didFailGetPickPalCount!(error: "Error", subtitle: "No existe el cliente actual.")
                break
            case 500:
                self.delegate.didFailGetPickPalCount!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
            default:
                break
            }
        })
    }
    
    
    func savePromoNews(name: String, description: String, start_date: String, end_date:String, image_id: Int, establishment_id: Int, isUpDate:Bool, idPromotional: String, isNews: Bool, imageFilePath: String){
        
        let fileUrl =  URL(fileURLWithPath: imageFilePath)
        print("fileUrl ",fileUrl)
        var url = ""
        if isUpDate {
            url = Constants.baseURL + Constants.establishment + Constants.update_admin_promotional
        }else{
            url = Constants.baseURL + Constants.establishment + Constants.add_admin_promotional
        }
        print(url)
        
        let type = isNews ? 2 : 1
        let parameter : [String: Any]
        
        if imageFilePath != "" {
                     print("url " + url)
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(fileUrl, withName: "image_buffer")
                multipartFormData.append("\(establishment_id)".data(using: .utf8, allowLossyConversion: false)!, withName: "establishment_id")
                multipartFormData.append("\(name)".data(using: .utf8, allowLossyConversion: false)!, withName: "name")
                multipartFormData.append(description.data(using: .utf8, allowLossyConversion: false)!, withName: "description")
                multipartFormData.append("\(start_date)".data(using: .utf8, allowLossyConversion: false)!, withName: "start_date")
                multipartFormData.append("\(end_date)".data(using: .utf8, allowLossyConversion: false)!, withName: "end_date")
                multipartFormData.append("\(type)".data(using: .utf8, allowLossyConversion: false)!, withName: "type")
               
            }, to: url , encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: {progress in
                        print("Porcentaje de subida �" + progress.fractionCompleted.description)
                        UploadingView.changeMessage(newMessage: Int(progress.fractionCompleted * 100).description + " %")
                        if(progress.fractionCompleted == 1.0){
                                            UploadingView.changeMessage(newMessage: "¡Hecho!")
                        }
                    })
                    upload.responseJSON { response in
                        //                    if let avatar = (response.value as? NSDictionary)?.value(forKey: "avatar") as? String{
                        //                        self.delegate?.didSuccessUploadPhoto!(avatar: avatar)
                        self.delegate.didSuccessSavePromo!()
                        //                    }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    self.delegate.didFailSavePromo!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                }
            })
        }else{
            
            print("url " + url)
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append("\(idPromotional)".data(using: .utf8, allowLossyConversion: false)!, withName: "pk")
                multipartFormData.append("\(imageFilePath)".data(using: .utf8, allowLossyConversion: false)!, withName: "image_buffer")
                multipartFormData.append("\(establishment_id)".data(using: .utf8, allowLossyConversion: false)!, withName: "establishment_id")
                multipartFormData.append("\(name)".data(using: .utf8, allowLossyConversion: false)!, withName: "name")
                multipartFormData.append("\(description)".data(using: .utf8, allowLossyConversion: false)!, withName: "description")
                multipartFormData.append("\(start_date)".data(using: .utf8, allowLossyConversion: false)!, withName: "start_date")
                multipartFormData.append("\(end_date)".data(using: .utf8, allowLossyConversion: false)!, withName: "end_date")
                multipartFormData.append("\(type)".data(using: .utf8, allowLossyConversion: false)!, withName: "type")
                multipartFormData.append("\(image_id)".data(using: .utf8, allowLossyConversion: false)!, withName: "image_id")
               
            }, to: url , encodingCompletion: { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: {progress in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseJSON { response in

                        switch Int((response.response?.statusCode)!){
                        case 200:
                            self.delegate.didSuccessSavePromo!()
                            
                            break
                        case 400:
                            self.delegate.didFailSavePromo!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 404:
                            self.delegate?.didFailSavePromo!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailSavePromo!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            self.delegate?.didFailSavePromo!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        }
                        
                       
                       
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    self.delegate.didFailSavePromo!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                }
            })
            
            
        }
    }
    
    func upDateDataset(dataset: [Promotionals], PromotionalId: Int) -> [Promotionals] {
        for object in dataset {
            if object.id == PromotionalId {
                object.deleted = true
            }
        }
        
        return dataset
    }
    
    func makePromoSingleEstablishment (info: NSDictionary) -> PromoSingleEstablishment {
        //        print(info)
        let st = PromoSingleEstablishment()
        let address = EstablishementAddress()
        _ = [Promotional]()
        var gallery = [String]()
        var operation_schedule = [ScheduleItem]()
        var exception_schedule = [ScheduleItem]()
        
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        address.street = temp_address.value(forKey: "street") as? String
        address.colony = temp_address.value(forKey: "colony") as? String
        address.city = temp_address.value(forKey: "city") as? String
        address.state = temp_address.value(forKey: "state") as? String
        address.country = temp_address.value(forKey: "country") as? String
        address.zip_code = temp_address.value(forKey: "zip_code") as? String
        let location = (info).value(forKey: "location") as? NSDictionary
        st.latitude = location?.value(forKey: "latitude") as? Double
        st.longitude = location?.value(forKey: "longitude") as? Double
        st.id = (info).value(forKey: "id") as? Int
        st.address = address
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            let social = SocialInformation()
            social.paymentAllow = (socialInfo).value(forKey: "payment_allow") as? String
            social.averagePrice = (socialInfo).value(forKey: "average_price") as? String
            social.petFriendly = (socialInfo).value(forKey: "average_price") as? Bool
            social.availableWifi = (socialInfo).value(forKey: "available_wifi") as? Bool
            social.availableAlc = (socialInfo).value(forKey: "available_alc") as? Bool
            social.areas = (socialInfo).value(forKey: "areas") as? String
            social.availableKids = (info).value(forKey: "available_kids") as? Bool
            social.environment = (socialInfo).value(forKey: "environment") as? String
            social.music = (socialInfo).value(forKey: "music") as? String
            social.availableSmoke = socialInfo.value(forKey: "available_smoke") as? Bool
            social.availableDelivery = (socialInfo).value(forKey: "available_delivery") as? Bool
            social.amenities = (socialInfo).value(forKey: "amenities") as? String
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            st.socialnformation = social
        }
        
        for image in (info).value(forKey: "establishment_gallery") as! NSArray{
            gallery.append(image as! String)
        }
        st.gallery = gallery
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as! String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as! String
            x.day = (item as! NSDictionary).value(forKey: "day") as! String
            operation_schedule.append(x)
        }
        for itemExp in (info).value(forKey: "exception_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (itemExp as! NSDictionary).value(forKey: "opening_hour") as! String
            x.closing_hour = (itemExp as! NSDictionary).value(forKey: "closing_hour") as! String
            x.day = (itemExp as! NSDictionary).value(forKey: "day") as! String
            exception_schedule.append(x)
        }
        st.operation_schedule = operation_schedule
        st.exceptionSchedule = exception_schedule
        st.name = (info).value(forKey: "establishment_name") as! String
        st.descripcion = (info).value(forKey: "description") as! String
        if let logo = (info).value(forKey: "logo") as? String {
            st.logo = logo
        }
        st.phone = (info).value(forKey: "phone") as! String
        st.noProfit = (info).value(forKey: "no_profit") as! Bool
        st.advsCategorry = (info).value(forKey: "advs_category") as! String
        st.subadvsCategory = (info).value(forKey: "subadvs_category") as! String
        st.activeNotifications = (info).value(forKey: "active_notifications") as! Bool
        st.promotionals = makePromotional(info: (info).value(forKey: "promotionals") as! NSArray)
        return st
    }
    
    
    func makeSingleAdvsEstab (info: NSDictionary) -> SingleEstabAdvs {
        //        print(info)
        let st = SingleEstabAdvs()
        let address = EstablishementAddress()
        let promotionals = [Promotional]()
        var gallery = [String]()
        var operation_schedule = [ScheduleItem]()
        var exception_schedule = [ScheduleItem]()
        
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        address.street = temp_address.value(forKey: "street") as! String
        address.colony = temp_address.value(forKey: "colony") as! String
        address.city = temp_address.value(forKey: "city") as! String
        address.state = temp_address.value(forKey: "state") as! String
        address.country = temp_address.value(forKey: "country") as! String
        address.zip_code = temp_address.value(forKey: "zip_code") as! String
        let location = (info).value(forKey: "location") as! NSDictionary
        st.latitude = location.value(forKey: "latitude") as! Double
        st.longitude = location.value(forKey: "longitude") as! Double
        st.id = (info).value(forKey: "id") as! Int
        st.address = address
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            let social = SocialInformation()
            social.paymentAllow = (socialInfo).value(forKey: "payment_allow") as? String
            social.averagePrice = (socialInfo).value(forKey: "average_price") as? String
            social.petFriendly = (socialInfo).value(forKey: "average_price") as? Bool
            social.availableWifi = (socialInfo).value(forKey: "available_wifi") as? Bool
            social.availableAlc = (socialInfo).value(forKey: "available_alc") as? Bool
            social.areas = (socialInfo).value(forKey: "areas") as? String
            social.availableKids = (info).value(forKey: "available_kids") as? Bool
            social.environment = (socialInfo).value(forKey: "environment") as? String
            social.music = (socialInfo).value(forKey: "music") as? String
            social.availableSmoke = socialInfo.value(forKey: "available_smoke") as? Bool
            social.availableDelivery = (socialInfo).value(forKey: "available_delivery") as? Bool
            social.amenities = (socialInfo).value(forKey: "amenities") as? String
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            st.socialnformation = social
        }
        
        for image in (info).value(forKey: "establishment_gallery") as! NSArray{
            gallery.append(image as! String)
        }
        st.gallery = gallery
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as! String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as! String
            x.day = (item as! NSDictionary).value(forKey: "day") as! String
            operation_schedule.append(x)
        }
        for itemExp in (info).value(forKey: "exception_schedule") as! NSArray{
            let x = ScheduleItem()
            
            if (itemExp as! NSDictionary).value(forKey: "opening_hour") as? String != nil{
                x.open_hour = (itemExp as! NSDictionary).value(forKey: "opening_hour") as? String
            }
            
            if (itemExp as! NSDictionary).value(forKey: "closing_hour") as? String != nil{
                x.closing_hour = (itemExp as! NSDictionary).value(forKey: "closing_hour") as! String
            }
            
            if (itemExp as! NSDictionary).value(forKey: "day") as? String != nil{
                x.day = (itemExp as! NSDictionary).value(forKey: "day") as! String
            }
            
            exception_schedule.append(x)
        }
        st.operation_schedule = operation_schedule
        
        st.name = (info).value(forKey: "establishment_name") as! String
        st.descripcion = (info).value(forKey: "description") as! String
        if let logo = (info).value(forKey: "logo") as? String {
            st.logo = logo
        }
        st.phone = (info).value(forKey: "phone") as! String
        st.noProfit = (info).value(forKey: "no_profit") as! Bool
        st.advsCategorry = (info).value(forKey: "advs_category") as! String
        st.subadvsCategory = (info).value(forKey: "subadvs_category") as! String
        st.activeNotifications = (info).value(forKey: "active_notifications") as! Bool
        st.promotionals = makePromotional(info: (info).value(forKey: "promotionals") as! NSArray)
        st.news = makeNews(info: (info).value(forKey: "news") as! NSArray)
        return st
    }
    
}
