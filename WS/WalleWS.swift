//
//  WalleWS.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 21/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol walletDelegate: class {
    
    @objc optional func getEstablishmentWallet(listWallet: [infoHeaderWallet])
    @objc optional func didFailGetEstablishmentWallet(error: String, subtitle: String)
    
    @objc optional func didAcceptedTermnsConditions()
    @objc optional func didFailacceptedTermnsConditions(error: String, subtitle: String)
    
    @objc optional func didGetTermnsConditions(textTermnsConditions: String)
    @objc optional func didFailGetTermnsConditions(error: String, subtitle: String)
    
    @objc optional func didGetListFaqs(lisFaqs: [ItemFaqs])
    @objc optional func didFailGetGetListFaqs(error: String, subtitle: String)
    
    @objc optional func didGetListWalletAvailable(listWallet: [infoHeaderWallet])
    @objc optional func didFailListWalletAvailable(error: String, subtitle: String)
    
    @objc optional func didGetInfoWallet(infoWallet: ItemSingelWallet)
    @objc optional func didFailGetInfoWallet(error: String, subtitle: String)
    
    
    @objc optional func didProcessOrder(reward: Double, total:Double, amount: Double, expirationDate: String)
    @objc optional func didFailProcessOrder(error: String, subtitle: String)
    
    @objc optional func didMyTransactionsWallet(listTransactions:[ItemHistoryWallet])
    @objc optional func didFailMyTransactionsWallet(error: String, subtitle: String)
    
    @objc optional func didMyWallet(walletInfo:Wallet, haeaderWallet: infoHeaderWallet)
    @objc optional func didFailMyWallet(error: String, subtitle: String)
    
    @objc optional func didCheckMyCash(walletInfo:MyCash)
    @objc optional func didFailCheckMyCash(error: String, subtitle: String)
    
    
    @objc optional func didGetQRPay(urlQR:String, folio: String, client_id: Int)
    @objc optional func didFailetQRPay(error: String, subtitle: String)
    
    
    
}


class WalletWS: NSObject {
    
    var delegate: walletDelegate?
    
    func getMyWalletList(client_id: Int){
     
        
        let parameters = ["client_id": client_id] as [String : Any]
              
              let request = Constants.baseURL+Constants.wallets+Constants.get_my_wallets
          
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
  
        switch response.result{
        case .success:
                  switch Int((response.response?.statusCode)!){
                      case 200:
                        
                        let  listWallets = self.makeListMyWallet(info: response.value! as! NSArray)
                           
                        self.delegate?.getEstablishmentWallet!(listWallet: listWallets)

                      break
                  case 204:
                          print("204")
                          
                      break
                  case 400:
                          print("400")
                          break
                  case 500:
                          print("500")
                      break
                  default:
                          print("ora si error")
                      break
                  }
              break
            case .failure(let error):
              print(error)
              break
        }
              })
    }
    
    
    
    func getAvailableEstablishmentsWalelt(){
     
        
        let request = Constants.baseURL + Constants.wallets + Constants.get_available_establishments
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            let arrayWallet = self.makeListWalletAvailable(info: (response.value! as! NSArray))
                            
                            self.delegate?.didGetListWalletAvailable!(listWallet: arrayWallet)
                            
                            break
                        case 204:
                            self.delegate?.didFailListWalletAvailable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailListWalletAvailable!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailListWalletAvailable!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailListWalletAvailable!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
    }
    
    
    
    func getInfoSingleEstablishmentsWalelt(idEstablishment: Int){
     
        
        let request = Constants.baseURL + Constants.wallets + Constants.get_single_establishment_wallet+"\(idEstablishment)/\(UserDefaults.standard.object(forKey: "client_id") as! Int)/"
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            let arrayWallet = self.makeInfoWallet(info: (response.value! as! NSDictionary))
                            
                            self.delegate?.didGetInfoWallet!(infoWallet: arrayWallet)
                            
                            break
                        case 204:
                            self.delegate?.didFailGetInfoWallet!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailGetInfoWallet!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailGetInfoWallet!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailGetInfoWallet!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
    }
    
    
    
    func processOrderWallet (establishment_id: Int,client_id: Int, amount: Double, payment_method: String ){
        
        let request = Constants.baseURL + Constants.wallets + Constants.register_order_wallet_oppa
        
        
        let  temp = ["establishment": establishment_id, "client": client_id, "amount": amount] as [String : Any]
        let  temp2 = ["payment_method":payment_method, "device_session_id": "\(UIDevice.current.identifierForVendor!)"] as [String : Any]
  
        let parameters = [
            "order": temp,
            "payments":temp2
            ] as [String: Any]
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                print("paso")
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                       
                        let reward = (response.value! as! NSDictionary).value(forKey: "reward") as! Double
                        
                       let total = (response.value! as! NSDictionary).value(forKey: "total") as! Double
                        
                        let amount = (response.value! as! NSDictionary).value(forKey: "amount") as! Double
                        
                       let expirationDate = (response.value! as! NSDictionary).value(forKey: "expiration_date") as! String
                        
                        self.delegate?.didProcessOrder!(reward: reward, total:total, amount: amount, expirationDate: expirationDate)
                        
                        
                        break
                    case 400:
                        self.delegate?.didFailProcessOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailProcessOrder!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailProcessOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                        
                    case 503:
                        self.delegate?.didFailProcessOrder!(error: "Utiliza otra tarjeta como método de pago.", subtitle: "Transacción rechazada por tu institución financiera")
                        break
                    default:
                        self.delegate?.didFailProcessOrder!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailProcessOrder!(error: "Error de comunicación", subtitle: "Ocurrió un error, por favor intenta nuevamente")
                    break
                }
            })
        }
    
    func getMyWallet(idEstablishment: Int){
     
        
        let request = Constants.baseURL + Constants.wallets + Constants.get_my_wallet+"\(idEstablishment)/"
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            let wallet = self.makeMyWallet(info: (response.value! as! NSDictionary))
                            let haeaderWallet = self.makeHeaderWallet(info: (response.value! as! NSDictionary))
                            
                            self.delegate?.didMyWallet!(walletInfo: wallet, haeaderWallet: haeaderWallet)
                            
                            break
                        case 204:
                            self.delegate?.didFailMyWallet!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailMyWallet!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailMyWallet!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailMyWallet!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
    }
    
    
    func getMyCash(idEstablishment: Int){
     
        
        let request = Constants.baseURL + Constants.wallets + Constants.check_my_cash+"\(idEstablishment)/\(UserDefaults.standard.object(forKey: "client_id") as! Int)/"
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            let myWallet = self.makeMyCash(info: (response.value! as! NSDictionary))
                            
                            self.delegate?.didCheckMyCash?(walletInfo: myWallet)
                            
                            break
                        case 204:
                            self.delegate?.didFailCheckMyCash!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailCheckMyCash!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailCheckMyCash!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailMyWallet!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
    }
    
    
    func acceptedTermnsConditions(){
        
        let request = Constants.baseURL + Constants.users + Constants.accepted_termns_conditions + "\(UserDefaults.standard.object(forKey: "client_id") as! Int)/"
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                           
                            self.delegate?.didAcceptedTermnsConditions!()
                           
                            break
                        case 204:
                            self.delegate?.didFailacceptedTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailacceptedTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailacceptedTermnsConditions!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailacceptedTermnsConditions!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        
        
    }
    
    
    func getMyTransactions(idWallet: Int, logo: String){
        
        let request = Constants.baseURL + Constants.wallets + Constants.get_my_transactions_from_wallet+"\(idWallet)/"
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:

                            let arrayTransactions = self.makeListMyTransactions(info: (response.value! as! NSArray), logo: logo)
                            
                            self.delegate?.didMyTransactionsWallet?(listTransactions:arrayTransactions)
                            
                           
                            break
                        case 204:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailGetTermnsConditions!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailGetTermnsConditions!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        
        
    }
    
    func getQRCodePay(wallet_pk: Int,amount: Double){
        
        let parameters = ["wallet_pk": wallet_pk, "amount": amount ] as [String : Any]
        
        let request = Constants.baseURL + Constants.wallets + Constants.request_generate_qr_payment   
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:

                       let url = (response.value! as! NSDictionary).value(forKey: "qr_code") as! String
                        let folio = (response.value! as! NSDictionary).value(forKey: "folio") as! String
                        let client_id = (response.value! as! NSDictionary).value(forKey: "client_id") as! Int
                        self.delegate?.didGetQRPay!(urlQR:url, folio: folio, client_id: client_id)
                        
                        break
                        
                    case 403:
                        self.delegate?.didFailetQRPay!(error: "Error", subtitle: "La cantidad no soporta la solicitud de pago.")
                        break
                    case 400:
                        self.delegate?.didFailetQRPay!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailetQRPay!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailetQRPay!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailetQRPay!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailetQRPay!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })

    }
    
    
    
    func getTermnsConditions(){
        
        let request = Constants.baseURL + Constants.about + Constants.terms_and_conditions_wallet
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:

                            self.delegate?.didGetTermnsConditions!(textTermnsConditions: (response.value! as! NSDictionary).value(forKey: "terms_and_conditions") as! String)
                           
                            break
                        case 204:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailGetTermnsConditions!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailGetTermnsConditions!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        
        
    }
    
    
    
    func getListFaqs(){
        
        let request = Constants.baseURL + Constants.about + Constants.faqs_wallet
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:

                            self.delegate?.didGetListFaqs!(lisFaqs:  self.makeFaqs(info: (response.value! as! NSDictionary)))
                           
                            break
                        case 204:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 400:
                            self.delegate?.didFailGetTermnsConditions!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate?.didFailGetTermnsConditions!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate?.didFailGetTermnsConditions!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        
        
    }
    
    
    

}

extension NSObject {
    
    func makeListMyWallet (info: NSArray) -> [infoHeaderWallet] {
        
         var listWallet = [infoHeaderWallet]()
        let listInfo = (info).value(forKey: "wallet_information") as! NSArray
        for list in info {
            
             let wallet = infoHeaderWallet()
            wallet.name = (list as! NSDictionary).value(forKey: "establishment_name")! as! String
            wallet.moneyWallet = (list as! NSDictionary).value(forKey: "money_wallet")! as! Double
            wallet.id = (list as! NSDictionary).value(forKey: "id")! as! Int
            wallet.state = (list as! NSDictionary).value(forKey: "state")! as! String
            wallet.place = (list as! NSDictionary).value(forKey: "place")! as! String
            wallet.img = (list as! NSDictionary).value(forKey: "logo")! as! String
            wallet.businessLine = (list as! NSDictionary).value(forKey: "business_line")! as! String
            wallet.idEstablisment = (list as! NSDictionary).value(forKey: "establishment_id")! as! Int
            
            listWallet.append(wallet)
        }
        
        return listWallet
    }
   
    
    func makeListWalletAvailable (info: NSArray) -> [infoHeaderWallet] {
        
         var listWallet = [infoHeaderWallet]()
        
        for list in info {
            
             let wallet = infoHeaderWallet()
            wallet.name = (list as! NSDictionary).value(forKey: "establishment_name")! as! String
            wallet.moneyWallet = (list as! NSDictionary).value(forKey: "available_money")! as! Double
            wallet.id = (list as! NSDictionary).value(forKey: "id")! as! Int
            wallet.state = (list as! NSDictionary).value(forKey: "state")! as! String
            wallet.place = (list as! NSDictionary).value(forKey: "place")! as! String
            wallet.img = (list as! NSDictionary).value(forKey: "logo")! as! String
            wallet.businessLine = (list as! NSDictionary).value(forKey: "business_line")! as! String
            listWallet.append(wallet)
        }
        
        return listWallet
    }
    
    
    func makeInfoWallet (info: NSDictionary) -> ItemSingelWallet {
        
        let infoWallet = ItemSingelWallet ()
        let address = EstablishementAddress()
        var operation_schedule = [ScheduleItem]()
        
        
        infoWallet.id = (info).value(forKey: "id") as? Int
        infoWallet.name = (info).value(forKey: "name") as? String
        infoWallet.establishmentName = (info).value(forKey: "establishment_name") as? String
        infoWallet.logo = (info).value(forKey: "logo") as? String
        infoWallet.descripcion = (info).value(forKey: "description") as? String
        
        infoWallet.phone = (info).value(forKey: "phone") as? String
        infoWallet.state = (info).value(forKey: "state") as? String
        infoWallet.municipality = (info).value(forKey: "municipality") as? String
        infoWallet.place = (info).value(forKey: "place") as? String
        infoWallet.establishmentCategory = (info).value(forKey: "establishment_category") as? Int
//        infoWallet.kitchenType = (info).value(forKey: "kitchen_type") as? Int
        
        infoWallet.place  = ((info).value(forKey: "place") as! NSDictionary).value(forKey: "name") as? String
        infoWallet.business_area = ((info).value(forKey: "business_area") as! NSDictionary).value(forKey: "name") as? String
        infoWallet.amountAvailable = (info).value(forKey: "amount_available") as? Double
        infoWallet.rewardPercent = (info).value(forKey: "reward_percent") as? Double
        infoWallet.userCurrentAmount = (info).value(forKey: "user_current_amount") as? Double
       
        
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        address.street = temp_address.value(forKey: "street") as? String
        address.colony = temp_address.value(forKey: "colony") as? String
        address.city = temp_address.value(forKey: "city") as? String
        address.state = temp_address.value(forKey: "state") as? String
        address.country = temp_address.value(forKey: "country") as? String
        address.zip_code = temp_address.value(forKey: "zip_code") as? String
        address.noExt  = temp_address.value(forKey: "n_ext") as? String
        address.noInt  = temp_address.value(forKey: "n_int") as? String
        address.referencePlace  = temp_address.value(forKey: "reference_location") as? String

        
        let location = (info).value(forKey: "location") as? NSDictionary
        infoWallet.latitude = location?.value(forKey: "latitude") as? Double
        infoWallet.longitude = location?.value(forKey: "longitude") as? Double
       
        infoWallet.address = address
        
        
        
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            let social = SocialInformation()
            social.paymentAllow = (socialInfo).value(forKey: "payment_methods") as? String
            social.delivery = (socialInfo).value(forKey: "delivery") as? Bool
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            infoWallet.socialnformation = social
        }
        
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as? String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as? String
            x.day = (item as! NSDictionary).value(forKey: "day") as? String
            operation_schedule.append(x)
        }
        
        infoWallet.operation_schedule = operation_schedule
        
       
            
//            faqs.title = (list as! NSDictionary).value(forKey: "title")! as! String
            
        
        
        return infoWallet
    }
    
    
    func makeListMyTransactions (info: NSArray, logo: String) -> [ItemHistoryWallet] {
    
        
        var infoWallet = [ItemHistoryWallet]()
    
        for list in info {
            
             let movement = ItemHistoryWallet()
            movement.id = (list as! NSDictionary).value(forKey: "id")! as! Int
            movement.reference = (list as! NSDictionary).value(forKey: "reference")! as! String
            movement.createdAt = (list as! NSDictionary).value(forKey: "created_at")! as! String
            movement.timeAt = (list as! NSDictionary).value(forKey: "time_at")! as! String
            movement.establishmentName = (list as! NSDictionary).value(forKey: "establishment_name")! as! String
            movement.concept = (list as! NSDictionary).value(forKey: "concept")! as! String
            movement.conceptInt = (list as! NSDictionary).value(forKey: "concept_int")! as! Int
            movement.amount = (list as! NSDictionary).value(forKey: "amount")! as! String
            movement.percentReward = (list as! NSDictionary).value(forKey: "percent_reward")! as! Double
            movement.reward = (list as! NSDictionary).value(forKey: "reward")! as! String
            movement.total = (list as! NSDictionary).value(forKey: "total")! as! String
            movement.logo = logo
            
            // El concepInt si es 1 es compra
            
           // amount rewRD
            infoWallet.append(movement)
        }
        return infoWallet
    
    }
    
    
    
    
    func makeFaqs (info: NSDictionary) -> [ItemFaqs] {
        
         var listFaqs = [ItemFaqs]()
        let listInfo = (info).value(forKey: "list_faqs") as! NSArray
        for list in listInfo {
            
             let faqs = ItemFaqs()
            faqs.title = (list as! NSDictionary).value(forKey: "title")! as! String
            faqs.answer = (list as! NSDictionary).value(forKey: "answer")! as! String
           
            listFaqs.append(faqs)
        }
        
        return listFaqs
    }
    
    
    
    func makeMyWallet (info: NSDictionary) -> Wallet {
        
        let wallet = Wallet()
        let walletDicctionary = (info).value(forKey: "wallet") as! NSDictionary
       
            
        
        wallet.id = walletDicctionary.value(forKey: "id")! as! Int
        wallet.available_money_to_buy = walletDicctionary.value(forKey: "available_money_to_buy")! as! Double
        wallet.expiration_date = walletDicctionary.value(forKey: "expiration_date")! as! String
        wallet.reference = walletDicctionary.value(forKey: "reference")! as! String
        wallet.money_wallet = walletDicctionary.value(forKey: "money_wallet")! as! Double
           
  
        
        return wallet
    }
    
    func makeHeaderWallet (info: NSDictionary) -> infoHeaderWallet {
        
        let haader = infoHeaderWallet()
        let walletDicctionary = (info).value(forKey: "establishment") as! NSDictionary
       
            
        haader.idEstablisment = walletDicctionary.value(forKey: "id")! as! Int
        haader.img = walletDicctionary.value(forKey: "logo")! as! String
        haader.place = walletDicctionary.value(forKey: "place")! as! String
        haader.state = walletDicctionary.value(forKey: "state")! as! String
        haader.name =  walletDicctionary.value(forKey: "establishment_name")! as! String
        haader.businessLine  =  walletDicctionary.value(forKey: "business_line")! as! String
        haader.backGroundColor = UIColor(red: 0.62, green: 0.73, blue: 0.23, alpha: 1.00)
  
        
        return haader
    }
    
    
    
   
    
    func makeMyCash(info: NSDictionary) -> MyCash {
        
        
        let wallet = MyCash()
        
        wallet.availablePickpalCash = info.value(forKey: "available_pickpal_cash")! as! Bool
        wallet.availableToBuy = info.value(forKey: "available_to_buy")! as! Bool
        wallet.rewardPercent = info.value(forKey: "reward_percent")! as! Double
        wallet.amountAvailable = info.value(forKey: "available_pickpal_cash")! as! Double
        wallet.userCurrentAmount  = info.value(forKey: "user_current_amount")! as! Double
        
        return wallet
    }
    
    
    
    
}

