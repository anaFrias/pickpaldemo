//
//  GoogleServices.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 29/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

@objc protocol googleWSDelegate: class{
    @objc optional func didSuccessgetNearbyPlaces(nearbyPlace: [Places])
    @objc optional func didFailGetNearby(error: String)
    
    @objc optional func didSuccessGetGeolocation(location: String, state: String)
    @objc optional func didFailGetGeolocation(title: String, subtitle: String)
    
    @objc optional func didSuccessGetInfoStreet(location: CLLocationCoordinate2D)
    @objc optional func didFailGetInfoStreet(title: String, subtitle: String)
}

class GoogleServices: NSObject {
    var delegate : googleWSDelegate?
    private let requestGoogleAPI = "https://maps.googleapis.com/maps/api/place/nearbysearch/"
    private let requestGoogleGeocode = "https://maps.googleapis.com/maps/api/geocode/"
    

    
    func showNearbyPlaces(location: CLLocationCoordinate2D, radius: Double){
        let queryParam = "json?location=\(location.latitude),\(location.longitude)&radius=\(radius)&key=AIzaSyDNvBTwhgWNqavtBmJmOn8X-ZscUaisZvA"
        
        Alamofire.request("\(requestGoogleAPI)" + queryParam, method : .get , parameters : nil, encoding: JSONEncoding.default ,  headers: nil).responseJSON { response in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    let nearby = self.nearbyPlace(info: (response.result.value as! NSDictionary).value(forKey: "results") as! NSArray)
//                    print((response.result.value as! NSDictionary).value(forKey: "results") as! NSArray)
                    self.delegate?.didSuccessgetNearbyPlaces!(nearbyPlace: nearby)
                    break
                case 400...499:
                    self.delegate?.didFailGetNearby!(error: "No podemos procesar tu solicitud.")
                    break
                case 500:
                    self.delegate?.didFailGetNearby!(error: "Ha ocurrido un error, intentalo de nuevo más tarde.")
                    break
                default:
                    self.delegate?.didFailGetNearby!(error: "Ha ocurrido un error, intentalo de nuevo más tarde.")
                    break
                }
                break
            case .failure(let error):
                self.delegate?.didFailGetNearby!(error: "Error de comunicación, asegúrate de estar conectado a internet.")
                print (error)
                break
            }
        }
    }
    
    func getCurrentPlace(location: CLLocationCoordinate2D){
        let queryParam = "json?latlng=\(location.latitude),\(location.longitude)&sensor=true&key=AIzaSyDEZ6vQH4yj8gskmztompn8vYnLnUN04Rk"
        Alamofire.request("\(requestGoogleGeocode)" + queryParam, method : .get , parameters : nil, encoding: JSONEncoding.default ,  headers: nil).responseJSON { response in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    let nearby = self.Geolocation(info: (response.result.value as! NSDictionary).value(forKey: "results") as! NSArray, request: "ciudad").0
                    let state = self.Geolocation(info: (response.result.value as! NSDictionary).value(forKey: "results") as! NSArray, request: "ciudad").1
                    self.delegate?.didSuccessGetGeolocation!(location: nearby, state: state)
                    break
                case 400...499:
                    self.delegate?.didFailGetGeolocation!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                    break
                case 500:
                    self.delegate?.didFailGetGeolocation!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                    break
                default:
                    self.delegate?.didFailGetGeolocation!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                    break
                }
                break
            case .failure(let error):
                self.delegate?.didFailGetGeolocation!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                print (error)
                break
            }
        }
    }
    
    
    func getInfoStreet(location: CLLocationCoordinate2D, address: String){
        
        let queryParam = "json?placeid=\(address)&key=AIzaSyDHV1m4uIvINpUuiI1-1JpVsDQhOvuMRAU"
  
        var url = ("https://maps.googleapis.com/maps/api/place/details/" + queryParam).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        
        Alamofire.request(url, method : .get , parameters : nil, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200:
                    
                    let dicctionary = (response.result.value as! NSDictionary).value(forKey: "result") as! NSDictionary
    
                    let coordinates =  self.getInfoAddress(info: dicctionary)
                    self.delegate?.didSuccessGetInfoStreet?(location: coordinates)
                
                    break
                case 400...499:
                    self.delegate?.didFailGetInfoStreet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                    break
                case 500:
                    self.delegate?.didFailGetInfoStreet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                    break
                default:
                    self.delegate?.didFailGetInfoStreet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                    break
                }
                break
            case .failure(let error):
                self.delegate?.didFailGetInfoStreet!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                print (error)
                break
            }
        }
    }
    
    
    func nearbyPlace(info: NSArray) -> [Places] {
        var places: [Places] = []
        for place in info {
            let types = (place as! NSDictionary).value(forKey: "types") as! [String]
            if types.contains("food") || types.contains("restaurant") {
                let nearby = Places()
                //nearby.placeId = (place as! NSDictionary).value(forKey: "place_id") as! String
                nearby.image = (((place as! NSDictionary).value(forKey: "photos") as? NSArray)?.object(at: 0) as? NSDictionary)?.value(forKey: "photo_reference") as? String
                //nearby.placeName = (place as! NSDictionary).value(forKey: "name") as! String
                //nearby.type = (place as! NSDictionary).value(forKey: "types") as! [String]
//                print((place as! NSDictionary).value(forKey: "types") as! [String])
                places.append(nearby)
            }
        }
        return places
    }
    
    func Geolocation(info: NSArray, request: String) -> (String, String){
        var city = String()
        var country = String()
        for location in info {
            let addrsComponents = (location as! NSDictionary).value(forKey: "address_components") as! NSArray
            for address in addrsComponents {
                let types = (address as! NSDictionary).value(forKey: "types") as! [String]
                let count = (address as! NSDictionary).value(forKey: "long_name") as! String
                let cty = (address as! NSDictionary).value(forKey: "long_name") as! String
                if types.contains("administrative_area_level_3") || types.contains("administrative_area_level_2"){
                    city = cty
                }
                if types.contains("administrative_area_level_1") {
                    country = count
                }
            }
            
        }
        print(city, country)
        return (city, country)
    }
    
    
    func getInfoAddress(info: NSDictionary) -> CLLocationCoordinate2D{

        
        let geometry = info.value(forKey: "geometry") as! NSDictionary
        
        var location = geometry.value(forKey: "location") as! NSDictionary
        
        var   centerMapCoordinate = CLLocationCoordinate2D()
        
        
        centerMapCoordinate.latitude = location.value(forKey: "lat") as! Double
        centerMapCoordinate.longitude = location.value(forKey: "lng") as! Double
        
       
        
        return centerMapCoordinate
    }
}

