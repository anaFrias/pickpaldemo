//
//  PointsWS.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol PointsWSDelegate: class {
    
    @objc optional func didSuccessGetPoints(points: PointsMobile)
    @objc optional func didFailGetPoints(error: String, subtitle: String)
    
    @objc optional func didSuccessRewardsMobile(info: [RewardsMobile])
    @objc optional func didFailRewardsMobile(error: String, subtitle: String)
    
    @objc optional func didSuccessGetSingleRewards(info: SingleReward)
    @objc optional func didFailGetGetSingleRewards(error: String, subtitle: String)
    
    @objc optional func didSuccessCountEstablishmentPoints(number: Int)
    @objc optional func didFailCountEstablishmentPoints(error: String, subtitle: String)
    
    @objc optional func didSuccessComission(info: TransferCommission, whereFrom: String)
    @objc optional func didFailComission(error: String, subtitle: String)
    
    @objc optional func didSuccessSendPoints(user_id: Int, points: Int, phoneToSend: String,
                                             establishmentId: Int, friend: String)
    @objc optional func didFailSendPoints(error: String, subtitle: String)
    
    @objc optional func didSuccessInterchangePoints(info: [InterchangemyPoints])
    @objc optional func didFailInterchangePoints(error: String, subtitle: String)
    
    @objc optional func didSuccessGetPendingExchange(info: [PendingExchange])
    @objc optional func didFailGetGetPendingExchange(error: String, subtitle: String)
    
    @objc optional func didSuccessCancelInterchange()
    @objc optional func didFailCancelInterchange(error: String, subtitle: String)
    
    @objc optional func didSuccessviewMyavailableEstablishmentsPoints(info: [RewardsMobile])
    @objc optional func didFailMyavailableEstablishmentsPoints(error: String, subtitle: String)
    
    @objc optional func didSuccessAllEstablishmentAround(info: [RewardsMobile])
    @objc optional func didFailAllEstablishmentAround(error: String, subtitle: String)
    
    
    @objc optional func didSuccessStatusRequestRxchange(dataExchan: TransferDetails)
    @objc optional func didFailStatusRequestRxchange(error: String, subtitle: String)
    
    @objc optional func didSuccessAcceptRequestExchange(total_transactions: Int)
    @objc optional func didFailAcceptRequestExchange(error: String, subtitle: String)

    

    
    /*
     @objc optional func didSuccessGetGuideEstablshmentsById(info: [EstablishmentsGuide])
     @objc optional func didFailGetGuideEstablshmentsById(error: String, subtitle: String)
     
     @objc optional func didSuccessGetGuideCategories(info: [GuideCategories])
     @objc optional func didFailGetGuideCategories(error: String, subtitle: String)
     
     @objc optional func didSuccessSingleEstablishmentGuide(info: SingleEstablishment)
     @objc optional func didFailSingleEstablishmentGuide(error: String, subtitle: String)*/
    
}

public class PointsWS: NSObject{
    
    var delegate: PointsWSDelegate!
    
    func CountEstablishmentPoints() {
        let request = Constants.baseURL + Constants.establishment + Constants.countpoints
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let total = (response.value as! NSDictionary).value(forKey: "total_pickpal") as! Int
                        self.delegate.didSuccessCountEstablishmentPoints?(number: total)
                        break
                    case 400:
                        self.delegate?.didFailCountEstablishmentPoints?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCountEstablishmentPoints?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailCountEstablishmentPoints?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCountEstablishmentPoints?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailCountEstablishmentPoints?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getTotalPoinsMobile(client_id: Int) {
        //let request = Constants.baseURL + Constants.users + Constants.status_profile + String(client_id)
        let request = Constants.baseURL + Constants.users + Constants.status_profile + String(client_id) + "/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let pointsInfo = self.makePointsMobile(info: response.value as! NSDictionary)
                        self.delegate.didSuccessGetPoints?(points: pointsInfo)
                        break
                    case 400:
                        self.delegate?.didFailGetPoints?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetPoints?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetPoints?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetPoints?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetPoints?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
   // func getRewards(client_id: Int, state_code: String){
    func getRewards(client_id: Int){
        //let request = Constants.baseURL + Constants.users + Constants.status_profile + String(client_id)
        let request = Constants.baseURL + Constants.users + Constants.get_rewards + String(client_id) + "/"
      //  let request = Constants.baseURL + Constants.users + Constants.get_rewards + String(client_id) + "/" +  String(state_code)
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let rewards = self.makeRewardsMobile(info: response.value as! NSDictionary)
                        self.delegate.didSuccessRewardsMobile?(info: rewards)
                        break
                    case 400:
                        self.delegate?.didFailRewardsMobile?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailRewardsMobile?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailRewardsMobile?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRewardsMobile?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRewardsMobile?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getSingleRewards(client_id: String, establishmenId: String){
        let request = Constants.baseURL + Constants.establishment + Constants.view_single_reward_establishment + client_id + "/" + establishmenId + "/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let reward = self.makeSingleRewardsMobile(info: response.value as! NSDictionary)
                        self.delegate.didSuccessGetSingleRewards?(info: reward)
                        break
                    case 400:
                        self.delegate?.didFailGetGetSingleRewards?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetGetSingleRewards?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetGetSingleRewards?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetGetSingleRewards?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetGetSingleRewards?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getPriceMoves(from: String){
        let request = Constants.baseURL + Constants.transfer_commission
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let reward = self.makeTransferCommission(info: response.value as! NSDictionary)
                        self.delegate.didSuccessComission?(info: reward, whereFrom: from)
                        break
                    case 400:
                        self.delegate?.didFailComission?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailComission?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailComission?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailComission?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailComission?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func sendPointsToFriend(user_id: Int, points: String, phoneToSend: String, establishmentId: Int, friend: String, tax: Int){
        let doublePoint = Double(points) ?? 0
        let intPoint = Int(doublePoint)
        let parameters = [
            "client_id":user_id,
            "points": intPoint ,
            "phone": phoneToSend,
            "establishment_id": establishmentId,
            "name": friend,
            "tax": tax
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.transfer_point_user)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print("Hola")
                        self.delegate?.didSuccessSendPoints?(user_id: user_id, points: intPoint , phoneToSend: phoneToSend, establishmentId: establishmentId, friend: friend)
                        break
                    case 400:
                        print("Hola")
                        self.delegate?.didFailSendPoints?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        print("Hola")
                        self.delegate?.didFailSendPoints?(error: "Ha ocurrido un error", subtitle: "Ha ocurrido un error, intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    print("Hola")
                    self.delegate?.didFailSendPoints?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func sendPointsToInterchange(user_id: Int, points: Int, establishmentId: Int, establishments_destination: [Int], tax: Int){
        let parameters = [
            "client_id":user_id,
            "points": points ,
            "establishment_origin": establishmentId,
            "establishments_destination": establishments_destination,
            "tax": tax
            ] as [String: Any]
        let url = "\(Constants.baseURL)\(Constants.exchange_points_estableshment)"
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let info = self.makeInterchange(info: response.value as! NSDictionary)
                        self.delegate.didSuccessInterchangePoints?(info: info)
                        break
                    case 400:
                        print("Hola")
                        self.delegate?.didFailInterchangePoints?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        print("Hola")
                        self.delegate?.didFailInterchangePoints?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        print("Hola")
                        self.delegate?.didFailInterchangePoints?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    }
                    break
                case .failure( _):
                    print("Hola")
                    self.delegate?.didFailInterchangePoints?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getPendigExchange(user_id: String) {
        let request = Constants.baseURL + Constants.exchange_list_pending + user_id
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let data = self.makePendingExchange(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetPendingExchange?(info: data)
                        break
                    case 400:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetGetPendingExchange?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func cancelInterchange(excharge_pk: Int) {
        let parameters = [
            "excharge_pk": excharge_pk
            ] as [String: Any]
        let request = Constants.baseURL + Constants.cancelled_exchange
        print(request)
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessCancelInterchange?()
                        break
                    case 400:
                        self.delegate?.didFailCancelInterchange?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCancelInterchange?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailCancelInterchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCancelInterchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailCancelInterchange?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getAllEstablishmentAround() {
        let request = Constants.baseURL + Constants.view_all_establishment_service_points// + 
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let rewards = self.makeRewardsMobileTwo(info: response.value as! NSArray)
                        self.delegate.didSuccessAllEstablishmentAround?(info: rewards)
                        break
                    case 400:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetGetPendingExchange?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    func getUserEstablishmentsPoints() {
        let request = Constants.baseURL + Constants.view_myavailable_establishments_points + String(UserDefaults.standard.object(forKey: "client_id") as! Int)
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let rewards = self.makeUserEstablishmentsPoints(info: response.value as! NSArray)
                        self.delegate.didSuccessviewMyavailableEstablishmentsPoints?(info: rewards)
                        break
                    case 400:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetGetPendingExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetGetPendingExchange?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    
    func getStatusRequestRxchange(client_id: Int, establishment_from: Int, request_id: Int) {
        let request = Constants.baseURL + Constants.get_status_request_exchange
        let parameters = [
                   "client_id": client_id,
                   "establishment_from": establishment_from,
                   "request_id": request_id
                   ] as [String: Any]
        
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                              print("Direccion response", response)
                           //  print("Direccion Guardada",(response.value as! JSONEncoder))
                        
                        
                        let info = self.makeDataExchange(info: response.value as! NSDictionary)
                        
                        self.delegate.didSuccessStatusRequestRxchange?(dataExchan: info)
//                              let jsonText = response.value
//
//                              var dictonary:NSDictionary?
//
//                              if let data = jsonText!.data(using: String.Encoding.utf8) {
//
//                                         do {
//                                             dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
//
//                                             if let myDictionary = dictonary {
//                                                let info = self.makeDataExchange(info: myDictionary)
//
//                                                self.delegate.didSuccessStatusRequestRxchange?(dataExchan: info)
//
//                                             }
//                                         } catch let error as NSError {
//                                             print(error)
//                                         }
//                                     }
                         break
                     case 400:
                         self.delegate?.didFailStatusRequestRxchange?(error: "Error", subtitle: "Intentalo de nuevo más tarde.")
                         break
                     case 404:
                         self.delegate?.didFailStatusRequestRxchange?(error: "Error", subtitle: "El usuario no existe")
                         break
                         
                         case 405:
                        // self.delegate?.didFailStatusRequestRxchange?(error: "Lo sentimos", subtitle: (response.value as! NSDictionary).value(forKey: "message") as! String)
                          self.delegate?.didFailStatusRequestRxchange?(error: "Lo sentimos", subtitle: "La solicitud ya no se encuentra disponible. Recuerda que si necesitas puntos, puedes crear tu propia solicitud de intercambio.")
                             
                         break
                     case 500:
                         self.delegate?.didFailStatusRequestRxchange?(error: "Ha ocurrido un error", subtitle: "Intentalo de nuevo más tarde.")
                         break
                     default:
                         self.delegate?.didFailStatusRequestRxchange?(error: "Ha ocurrido un error", subtitle: "Intentalo de nuevo más tarde.")
                         break
                     }
                 break
               case .failure(let error):
                 print(error)
                 break
           }
        })
     }
            

    
    func acceptRequestExchange(client_id: Int, establishment_from: Int, request_id: Int, establishmentto_id: Int, amount: Int ) {
        let request = Constants.baseURL + Constants.accept_request_exchange
        
        let parameters = [
                   "client_id": client_id,
                   "establishmentfrom_id": establishmentto_id,
                   "establishmentto_id": establishment_from,
                   "amount": amount,
                   "request_id": request_id
                   ] as [String: Any]
        
        
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate.didSuccessAcceptRequestExchange?(total_transactions: (response.value as! NSDictionary).value(forKey: "total_transactions") as! Int)
                        break
                    case 400:
                        self.delegate?.didFailAcceptRequestExchange?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailAcceptRequestExchange?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailAcceptRequestExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailAcceptRequestExchange?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailAcceptRequestExchange?(error: "Error al intercambiar", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    
    
    
    
}

extension PointsWS{
    
    func makePointsMobile(info: NSObject) -> PointsMobile {
        let itm = PointsMobile()
        itm.total_points = info.value(forKey: "total_points") as? Double
        itm.available_advs = info.value(forKey: "available_advs") as? Bool
        itm.total_money = info.value(forKey: "total_money") as? Double
        
        return itm
    }
    
    func makeRewardsMobile(info: NSObject) -> [RewardsMobile]{
        var establishments = [RewardsMobile]()
        for item in (info).value(forKey: "rewards_mobile") as! NSArray{
            let reward = RewardsMobile()
            
            reward.establishment_id =  (item as AnyObject).value(forKey: "establishment_id") as? Int
            reward.total_money = (item as AnyObject).value(forKey: "total_money") as? Double
            reward.total_points = (item as AnyObject).value(forKey: "total_points") as? Double
            reward.establishment_name = (item as AnyObject).value(forKey: "establishment_name") as? String
            reward.place = (item as AnyObject).value(forKey: "place") as? String
            reward.state = (item as AnyObject).value(forKey: "state") as? String
            reward.logo = (item as AnyObject).value(forKey: "logo") as? String
            reward.available = ((item as AnyObject).value(forKey: "available") as? Bool) ?? false
           
            establishments.append(reward)
        }
        
        return establishments
    }
    
    func makeRewardsMobileTwo(info: NSArray) -> [RewardsMobile]{
        var establishments = [RewardsMobile]()
        for item in (info){
            let reward = RewardsMobile()
            
            reward.establishment_id =  (item as AnyObject).value(forKey: "id") as? Int
            reward.logo = (item as AnyObject).value(forKey: "logo") as? String
            reward.state = (item as AnyObject).value(forKey: "state") as? String
            reward.place = (item as AnyObject).value(forKey: "place") as? String
            reward.establishment_name = (item as AnyObject).value(forKey: "establishment_name") as? String
            reward.total_money = 0.0
            reward.total_points = 0.0
            establishments.append(reward)
        }
        
        return establishments
    }
    
    func makeUserEstablishmentsPoints(info: NSArray) -> [RewardsMobile]{
        var establishments = [RewardsMobile]()
        for item in (info){
            let reward = RewardsMobile()
            
            reward.establishment_id =  (item as AnyObject).value(forKey: "id") as? Int
            reward.logo = (item as AnyObject).value(forKey: "logo") as? String
            reward.state = (item as AnyObject).value(forKey: "state") as? String
            reward.place = (item as AnyObject).value(forKey: "place") as? String
            reward.establishment_name = (item as AnyObject).value(forKey: "establishment_name") as? String
            reward.total_money = 0.0
            reward.total_points = (item as AnyObject).value(forKey: "points") as? Double
            establishments.append(reward)
        }
        
        return establishments
    }
    
    
    
    func makePendingExchange(info: NSArray) -> [PendingExchange]{
        var establishments = [PendingExchange]()
        for item in (info) {
            let reward = PendingExchange()
            
            reward.establishment_end_two =  (item as AnyObject).value(forKey: "establishment_end_two") as? String
            reward.establecimient_origin = (item as AnyObject).value(forKey: "establecimient_origin") as? String
            reward.establishment_end_three = (item as AnyObject).value(forKey: "establishment_end_three") as? String
            reward.date = (item as AnyObject).value(forKey: "created_at") as? String
            reward.establishment_end_one = (item as AnyObject).value(forKey: "establishment_end_one") as? String
            reward.concepInterchange = (item as AnyObject).value(forKey: "concept") as? String
            reward.current_balance = (item as AnyObject).value(forKey: "current_balance") as? Double
            reward.id = (item as AnyObject).value(forKey: "id") as? Int
            reward.amount = (item as AnyObject).value(forKey: "amount") as? Double
            reward.tax = (item as AnyObject).value(forKey: "tax") as? Double
            reward.tax_total = (item as AnyObject).value(forKey: "tax_money") as? Double
            reward.exchange_from =  (item as AnyObject).value(forKey: "exchange_from") as? String
            reward.exchange_to =  (item as AnyObject).value(forKey: "exchange_to") as? [String]
            reward.address =  (item as AnyObject).value(forKey: "address") as? String
            reward.exchange_to_address_array = (item as! NSDictionary).value(forKey: "exchange_to_address_array") as? [String]
            
            reward.inprocess_exchange =  (item as AnyObject).value(forKey: "inprocess_exchange") as? Bool
            reward.pending_transfer =  (item as AnyObject).value(forKey: "pending_transfer") as? Bool
            reward.leftAmount = (item as AnyObject).value(forKey: "left_amount") as? Double
            reward.time_at = (item as AnyObject).value(forKey: "time_at") as? String
            reward.next_balance =  (item as AnyObject).value(forKey: "next_balance") as? Double
            reward.money_next_balance =  (item as AnyObject).value(forKey: "money_next_balance") as? Double
            
            reward.left_amount =  (item as AnyObject).value(forKey: "left_amount") as? Double
            reward.left_amount_money =  (item as AnyObject).value(forKey: "left_amount_money") as? Double
            reward.total = (item as AnyObject).value(forKey: "total") as? Double
    
            reward.reference = (item as AnyObject).value(forKey: "reference") as? String
            
            if let modifiers = (item as! NSDictionary).value(forKey: "exchange_details") as? NSArray {
                    var modi = [ExchangeDetails]()
                    for modifi in modifiers {
                        let mod = ExchangeDetails()
                        mod.id = (modifi as! NSDictionary).value(forKey: "id") as? Int
                        mod.amount = ((modifi as! NSDictionary).value(forKey: "amount") as! Int)
                        mod.establishmentidFrom = ((modifi as! NSDictionary).value(forKey: "establishment_from") as! String)
                        mod.establishmentidTo = ((modifi as! NSDictionary).value(forKey: "establishment_to") as! String)
                        mod.isCancel = ((modifi as! NSDictionary).value(forKey: "is_cancelled") as! Bool)
                        modi.append(mod)
                    }
                    reward.exchange_details = modi
                }
            
            establishments.append(reward)
        }
        
        return establishments
    }
    
    func makeTransferCommission(info: NSObject) -> TransferCommission{
        let comission = TransferCommission()
        
        comission.comision_transferencia = (info).value(forKey: "comision_transferencia") as? Double
        comission.comision_intercambio = (info).value(forKey: "comision_intercambio") as? Double
        comission.tipo_cambio = (info).value(forKey: "tipo_cambio") as? Double
        
        return comission
    }
    
    func makeInterchange(info: NSObject) -> [InterchangemyPoints]{
        var comission = [InterchangemyPoints]()
        
        let number = (info).value(forKey: "total_transactions") as! Int
        print(number)
        let data = (info).value(forKey: "detail_transactions") as! NSArray
        if data.count > 0{
            for item in (info).value(forKey: "detail_transactions") as! NSArray{
                let reward = InterchangemyPoints()
                
                reward.from_est =  (item as AnyObject).value(forKey: "from_est") as? String
                reward.client =  (item as AnyObject).value(forKey: "client") as? Int
                reward.to_estpk =  (item as AnyObject).value(forKey: "to_estpk") as? Int
                reward.to_est =  (item as AnyObject).value(forKey: "to_est") as? String
                reward.amount =  (item as AnyObject).value(forKey: "amount") as? Double
                reward.conversionA =  (item as AnyObject).value(forKey: "conversionA") as? Double
                reward.conversionB =  (item as AnyObject).value(forKey: "conversionB") as? Double
                
                comission.append(reward)
            }
        }
        
        return comission
    }
    
    func makeSingleRewardsMobile(info: NSObject) -> SingleReward{
        let establishment = SingleReward()
        
        let address = EstablishementAddress()
        let social = SocialInformation()
        var pickpalService = [String]()
        var kitchenType = [String]()
        var estabGallery = [String]()
        var operation_schedule = [ScheduleItem]()
        var historyList = [HistoryRewards]()
        
        
        let place = (info).value(forKey: "place") as! NSDictionary
        if let estabCat = (info).value(forKey: "establishment_category") as? NSDictionary{
            establishment.establishmentCategory = estabCat.value(forKey: "name") as? String
        }
        //let estabCat = (info).value(forKey: "establishment_category") as! NSDictionary
        let location = (info).value(forKey: "location") as! NSDictionary
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        
        for servicePickPal in (info).value(forKey: "pickpal_services") as! NSArray{
            pickpalService.append(servicePickPal as! String)
        }
        
        for kitchen in (info).value(forKey: "kitchen_type") as! NSArray{
            kitchenType.append(kitchen as! String)
        }
        
        for gallery in (info).value(forKey: "establishment_gallery") as! NSArray{
            estabGallery.append(gallery as! String)
        }
        
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as? String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as? String
            x.day = (item as! NSDictionary).value(forKey: "day") as? String
            operation_schedule.append(x)
        }
        
        for item in (info).value(forKey: "historial") as! NSArray{
            let x = HistoryRewards()
            x.id = (item as! NSDictionary).value(forKey: "id") as? Int
            x.created_at = (item as! NSDictionary).value(forKey: "created_at") as? String
            x.concept = (item as! NSDictionary).value(forKey: "concept") as? String
            x.concept_int = (item as! NSDictionary).value(forKey: "concept_int") as? Int
            x.establishment_name = (item as! NSDictionary).value(forKey: "establishment_name") as? String
            x.total = (item as! NSDictionary).value(forKey: "total") as? Double
            x.next_balance = (item as! NSDictionary).value(forKey: "next_balance") as? Double
            
            x.amount = (item as! NSDictionary).value(forKey: "amount") as? Double
            x.amount_money = (item as! NSDictionary).value(forKey: "amount_money") as? Double
            x.tax = (item as! NSDictionary).value(forKey: "tax") as? Double
            x.tax_money = (item as! NSDictionary).value(forKey: "tax_money") as? Double
            x.status = (item as! NSDictionary).value(forKey: "status") as? Int
            x.status_label = (item as! NSDictionary).value(forKey: "status_label") as? String
            
            x.transfer_to = (item as! NSDictionary).value(forKey: "transfer_to") as? String
            x.transfer_from = (item as! NSDictionary).value(forKey: "transfer_from") as? String
            x.exchange_from = (item as! NSDictionary).value(forKey: "exchange_from") as? String
            x.exchange_to = (item as! NSDictionary).value(forKey: "exchange_to") as? [String]
            x.exchange_to_address_array = (item as! NSDictionary).value(forKey: "exchange_to_address_array") as? [String]
            
            x.status_exchange = (item as! NSDictionary).value(forKey: "status_exchange") as? Int
            x.left_amount = (item as! NSDictionary).value(forKey: "left_amount") as? Double
            x.pending_transfer = (item as! NSDictionary).value(forKey: "pending_transfer") as? Bool
            
            x.money_next_balance = (item as! NSDictionary).value(forKey: "money_next_balance") as? Double
            x.left_amount_money = (item as! NSDictionary).value(forKey: "left_amount_money") as? Double
            
            
            x.next_balance = (item as! NSDictionary).value(forKey: "next_balance") as? Double
            x.time_at = (item as! NSDictionary).value(forKey: "time_at") as? String
            x.reference = (item as! NSDictionary).value(forKey: "reference") as? String
            
            if let str_tax = (item as! NSDictionary).value(forKey: "str_tax") as? String {
                    x.str_tax = str_tax
            }
            
            if let address = (item as! NSDictionary).value(forKey: "address") as? String {
                    x.address = address
            }
                
                if let total_money = (item as! NSDictionary).value(forKey: "total_money") as? Double {
                    x.total_money = total_money
            }
            if let is_cancelled = (item as! NSDictionary).value(forKey: "is_cancelled") as? Bool {
                x.is_cancelled =  is_cancelled
        }
                

        if let modifiers = (item as! NSDictionary).value(forKey: "exchange_details") as? NSArray {
                    var modi = [ExchangeDetails]()
                    for modifi in modifiers {
                        let mod = ExchangeDetails()
                        mod.id = (modifi as! NSDictionary).value(forKey: "id") as? Int
                        mod.amount = ((modifi as! NSDictionary).value(forKey: "amount") as! Int)
                        mod.establishmentidFrom = ((modifi as! NSDictionary).value(forKey: "establishment_from") as! String)
                        mod.establishmentidTo = ((modifi as! NSDictionary).value(forKey: "establishment_to") as! String)
                        mod.isCancel = ((modifi as! NSDictionary).value(forKey: "is_cancelled") as! Bool)
                        modi.append(mod)
                    }
                    x.exchange_details = modi
                }
            
            
            historyList.append(x)
        }
        establishment.history = historyList
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            social.delivery = (socialInfo).value(forKey: "delivery") as? Bool
            social.payment_methods = (socialInfo).value(forKey: "payment_methods") as? String
        }
        
        //Insercion de datos
        establishment.id = (info as AnyObject).value(forKey: "id") as? Int
        establishment.name = (info as AnyObject).value(forKey: "name") as? String
        establishment.logo = (info as AnyObject).value(forKey: "image") as? String
        establishment.logo = (info as AnyObject).value(forKey: "logo") as? String
        establishment.descrip = (info as AnyObject).value(forKey: "description") as? String
        //Address
        address.street = temp_address.value(forKey: "street") as? String
        address.colony = temp_address.value(forKey: "colony") as? String
        address.city = temp_address.value(forKey: "city") as? String
        address.state = temp_address.value(forKey: "state") as? String
        address.country = temp_address.value(forKey: "country") as? String
        address.zip_code = temp_address.value(forKey: "zip_code") as? String
        if let noInt = temp_address.value(forKey: "n_int") as? String {
            address.noInt = noInt
        }else{
            address.noInt = ""
        }
        
        if let noExt = temp_address.value(forKey: "n_ext") as? String {
            address.noExt = noExt
        }else{
            address.noExt = ""
        }
        
        if let ref = temp_address.value(forKey: "reference_location") as? String {
            establishment.reference_location = ref
        }else{
            establishment.reference_location = ""
        }
        /**************/
        establishment.address = address
        establishment.phone = (info as AnyObject).value(forKey: "phone") as? String
        establishment.state = (info as AnyObject).value(forKey: "state") as? String
        establishment.municipality = (info as AnyObject).value(forKey: "municipality") as? String
        establishment.place = place.value(forKey: "name") as? String
        
        establishment.kitchenType = kitchenType
        establishment.pickpal_services = pickpalService
        establishment.servicePlus = (info as AnyObject).value(forKey: "service_plus") as? Bool
        establishment.latitude = location.value(forKey: "latitude") as? Double
        establishment.longitude = location.value(forKey: "longitude") as? Double
        establishment.establishment_gallery = estabGallery
        establishment.operation_schedule = operation_schedule
        establishment.socialInfo = social
        establishment.totalPoints = (info as AnyObject).value(forKey: "total_points") as? Double
        establishment.validity_points_firts = (info as AnyObject).value(forKey: "validity_points_firts") as? String
        establishment.validity_points_last = (info as AnyObject).value(forKey: "validity_points_last") as? String
        establishment.total_money = (info as AnyObject).value(forKey: "total_money") as? Double
        
        establishment.total_points_firts = (info as AnyObject).value(forKey: "total_points_firts") as? Int
        establishment.total_points_second = (info as AnyObject).value(forKey: "total_points_second") as? Int
        
        return establishment
    }
    
    
    func makeDataExchange(info: NSDictionary) -> TransferDetails {
        
        let itm = TransferDetails()
        itm.aboutOther = info.value(forKey: "about_other") as? String
        itm.totalToExchange = info.value(forKey: "total_to_exchange") as? Int
        
        
        itm.AboutMe = info.value(forKey: "about_me") as? String
        
        
        if let fromInfo = (info).value(forKey: "establishment_from") as? NSDictionary {
            itm.establishmentFromName = fromInfo.value(forKey: "name") as? String
            itm.establishmentFromStreet = fromInfo.value(forKey: "address") as? String
           
        }
        
        if let toInfo = (info).value(forKey: "establishment_to") as? NSDictionary {
            itm.establishmentTomName = toInfo.value(forKey: "name") as? String
            itm.establishmentTomStreet = toInfo.value(forKey: "address") as? String
           
        }
        
        
        return itm
    }
    
}
