//
//  EstablishmentWS.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 03/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol establishmentDelegate: class {
    @objc optional func didSuccessGetAppStore(tableInfo: [TableInfo])
    @objc optional func didFailGetAppStore(error: String, subtitle: String)
    
    @objc optional func didSuccessGetSyncDescriptive(tableInfo: [DescriptiveEstablishment])
    @objc optional func didFailGetSyncDescriptive(error: String, subtitle: String)
    
    @objc optional func didSuccessGetEstablshmentsById(info: [Establishment])
    @objc optional func didFailGetEstablshmentsById(error: String, subtitle: String)
    
    @objc optional func didSuccessGetFastEstablshments(info: [Establishment])
    @objc optional func didFailGetFastEstablshments(error: String, subtitle: String)
    
    @objc optional func didSuccessGetSingleEstablishment(info: SingleEstablishment)
    @objc optional func didFailGetSingleEstablishment(error: String, subtitle: String)
    
    @objc optional func didSuccessGetAllTypeKitchen(info: [Kitchens])
    @objc optional func didFailGetAllTypeKitchen(error: String, subtitle: String)
    
    @objc optional func didSuccessGetEvaluation(questions: Questions)
    @objc optional func didFailGetEvaluation(error: String, subtitle: String)
    
    @objc optional func didSuccessSetEvaluation()
    @objc optional func didFailSetEvaluation(error: String, subtitle: String)
    
    @objc optional func didSuccessSetEvaluationNote()
    @objc optional func didFailSetEvaluationNote(error: String, subtitle: String)
    
    @objc optional func didSuccessSetEvaluationCategories()
    @objc optional func didFailSetEvaluationCategories(error: String, subtitle: String)
    
    @objc optional func didSuccessGetNewsFeedHome(news: [News])
    @objc optional func didFailGetNewsFeedHome(error: String, subtitle: String)
    
    @objc optional func didSuccessGetAllTypeSingleKitchen(info: [Kitchens])
    @objc optional func didFailGetAllTypeSingleKitchen(error: String, subtitle: String)
    
    @objc optional func didSuccessGetViewEstablishmentAround(info: [Int])
    @objc optional func didFailGetViewEstablishmentAround(error: String, subtitle: String)
    
    @objc optional func didSuccessGetFastEstablshments1(ids: [Int])
    
    @objc optional func didSuccessGetInvoicing(invoicingInfo: Invoicing)
    @objc optional func didFailGetInvoicing(error: String, subtitle: String)
    
    @objc optional func didSuccessViewMyReward(reward: MyRewardEstablishment)
    @objc optional func didFailViewMyReward(error: String, subtitle: String)
    
    @objc optional func didSuccessTypeOrders(reward: [String])
    @objc optional func didFailTypeOrders(error: String, subtitle: String)
}

class EstablishmentWS: NSObject {
    
    var delegate: establishmentDelegate!
    
    func getAppStore(){
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_sync_establishment)", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let storeInfo = self.makeTableAppStore(info: (response.value as! NSArray))
                        self.delegate.didSuccessGetAppStore!(tableInfo: storeInfo)
                        break
                    case 204:
                        self.delegate.didFailGetAppStore!(error: "204", subtitle: "")
                        break
                    case 400:
                        self.delegate.didFailGetAppStore!(error: "400", subtitle: "")
                        break
                    case 500:
                        self.delegate.didFailGetAppStore!(error: "500", subtitle: "")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetAppStore!(error: "Internet", subtitle: "")
                    break
                }
            })
    }
    
    func getSyncDescriptive(){
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_sync_descriptive_establishments)", method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let storeInfo = self.makeDescriptive(info: (response.value as! NSArray))
                        self.delegate.didSuccessGetSyncDescriptive!(tableInfo: storeInfo)
                        break
                    case 204:
                        self.delegate.didFailGetSyncDescriptive!(error: "204", subtitle: "")
                        break
                    case 400:
                        self.delegate.didFailGetSyncDescriptive!(error: "400", subtitle: "")
                        break
                    case 500:
                        self.delegate.didFailGetSyncDescriptive!(error: "500", subtitle: "")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetSyncDescriptive!(error: "Internet", subtitle: "")
                    break
                }
            })
    }
    func getEstablishmentsById(ids: [Int], order_id:Int = 1){
        let parameters = [
            "ids": ids,
            "order_id":order_id
            ] as [String : Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_establishments_by_ids)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishments = self.makeEstablishments(info: (response.value as! NSArray))
//                        print((response.value as! NSArray))
                        self.delegate.didSuccessGetEstablshmentsById!(info: establishments)
                        break
                    case 204:
                        self.delegate.didFailGetEstablshmentsById!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetEstablshmentsById!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetEstablshmentsById!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetEstablshmentsById!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewEstablishmentOrdertypes(state_code: String, district: String, business_line_id: Int, inside_id: Int){
        
            let parameters = [
                "state_code": state_code,
                "district":district,
                "business_line_id": business_line_id,
                "inside_id": inside_id
                ] as [String : Any]
        
            Alamofire.request("\(Constants.baseURL)\(Constants.view_establishment_ordertypes)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
    //                print(response)
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                            print((response.value as! NSObject))
                            let types = self.makeTypesOrders(info: (response.value as! NSDictionary))
                            print(types)
                            self.delegate.didSuccessTypeOrders?(reward: types)
                            break
                        case 204:
                            self.delegate.didFailTypeOrders?(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                            
                            break
                        case 400:
                            self.delegate.didFailTypeOrders?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate.didFailTypeOrders?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate.didFailTypeOrders?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        }
    
    
    func getFastEstablishments(state: String, municipality: String){
        let parameters = [
            "state": state,
            "municipality": municipality
        ]
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_fast_establishments)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response.value)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishments = self.makeEstablishments(info: (response.value as! NSArray))
                        self.delegate.didSuccessGetFastEstablshments!(info: establishments)
                        break
                    case 204:
                        self.delegate.didFailGetFastEstablshments!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetFastEstablshments!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetFastEstablshments!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetFastEstablshments!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getFastEstablishments2(latitud: Double, longitud: Double){
        let parameters = [
            "latitud": latitud,
            "longitud": longitud
        ]
        Alamofire.request("\(Constants.baseURL)\(Constants.establishment)\(Constants.get_fast_establishments2)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response.value)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        if let ids = (response.value as! NSDictionary).value(forKey: "ids") as? [Int] {
                            self.delegate.didSuccessGetFastEstablshments1!(ids: ids)
                        }else{
                            self.delegate.didFailGetFastEstablshments!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        }
                        
                        break
                    case 204:
                        self.delegate.didFailGetFastEstablshments!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetFastEstablshments!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetFastEstablshments!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetFastEstablshments!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getSingleEstablisment(establishment_id: Int) {
        print(establishment_id)
        let request = Constants.baseURL + Constants.establishment + Constants.get_single_establishment + "\(establishment_id)/"
        print(request)
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishment = self.makeSingleEstablishment(info: response.value as! NSDictionary)
                        self.delegate?.didSuccessGetSingleEstablishment!(info: establishment)
                        break
                    case 400:
                        self.delegate?.didFailGetSingleEstablishment!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetSingleEstablishment!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetSingleEstablishment!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailGetSingleEstablishment!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getAllTypeKitchen(codeState: String) {
        let request = Constants.baseURL + Constants.establishment + Constants.get_all_type_kitchen + "\(codeState.stripingDiacritics)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let kitchens = self.makeKitchen(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetAllTypeKitchen!(info: kitchens)
                        break
                    case 204:
                        self.delegate?.didFailGetAllTypeKitchen!(error: "204", subtitle: "204")
                    case 400:
                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetAllTypeKitchen!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailGetAllTypeKitchen!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewEstablishmentAround(latitude: Double, longitude: Double, codeState: String, order_type: String) {
        let parameters = [
            "latitud": latitude,
            "longitud": longitude,
            "state_code": codeState.stripingDiacritics,
            "order_type": order_type
        ] as [String:Any]
        let request = Constants.baseURL + Constants.establishment + Constants.view_establishment_around
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                                print(response)
                switch response.result{
                case .success:
//                                        debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        if let ids = (response.value as! NSDictionary).value(forKey: "ids") as? [Int] {
                            self.delegate?.didSuccessGetViewEstablishmentAround!(info: ids)
                        }else{
                            self.delegate?.didFailGetViewEstablishmentAround!(error: "empty", subtitle: "empty")
                        }
                        
                        break
                    case 400:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
//                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
//                        self.delegate?.didFailGetAllTypeKitchen!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
//                        self.delegate?.didFailGetAllTypeKitchen!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetViewEstablishmentAround!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
//                    self.delegate?.didFailGetAllTypeKitchen!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getAllTypeKitchenSingle() {
        let request = Constants.baseURL + Constants.establishment + Constants.get_all_type_kitchen_single
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let kitchens = self.makeSingleKitchen(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetAllTypeSingleKitchen!(info: kitchens)
                        break
                    case 204:
                        self.delegate?.didFailGetAllTypeSingleKitchen!(error: "204", subtitle: "204")
                    case 400:
                        self.delegate?.didFailGetAllTypeSingleKitchen!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetAllTypeSingleKitchen!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetAllTypeSingleKitchen!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetAllTypeSingleKitchen!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetAllTypeSingleKitchen!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func GetEvaluation(order_id: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.get_evaluation + "\(order_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let questions = self.makeQuestions(info: (response.value as! NSDictionary).value(forKey: "questions") as! NSArray)
                        let quest = Questions()
                        quest.questions = questions
                        quest.establishment_id = (response.value as! NSDictionary).value(forKey: "establishment_id") as! Int
                        quest.img = (response.value as! NSDictionary).value(forKey: "image") as! String
                        self.delegate?.didSuccessGetEvaluation!(questions: quest)
                        break
                    case 400:
                        self.delegate?.didFailGetEvaluation!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetEvaluation!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetEvaluation!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    
    func SetEvaluation(client_id: Int, establishment_id: Int, evaluation_answer_id: Int, answer: Int, order_id: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.set_evaluation
        let parameters = [
            "evaluation_id": evaluation_answer_id,
            "establishment_id": establishment_id,
            "client_id": client_id,
            "answer": answer,
            "order_id": order_id,
            "skip": 0
            ] as [String: Any]
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetEvaluation!()
                        break
                    case 400:
                        self.delegate?.didFailSetEvaluation!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSetEvaluation!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailSetEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetEvaluation!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func SetEvaluationItems(client_id: Int, establishment_id: Int, evaluation_answer_id: Int, data: [Dictionary<String,Any>], order_id: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.set_evaluation_items
        let parameters = [
            "evaluation_id": evaluation_answer_id,
            "establishment_id": establishment_id,
            "client_id": client_id,
            "data": data,
            "skip": 0
            ] as [String: Any]
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetEvaluation!()
                        break
                    case 400:
                        self.delegate?.didFailSetEvaluation!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSetEvaluation!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailSetEvaluation!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetEvaluation!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func SetEvaluationNote(client_id: Int, establishment_id: Int, evaluation_answer_id: Int, note: String, order_id: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.set_evaluation
        let parameters = [
            "evaluation_id": evaluation_answer_id,
            "establishment_id": establishment_id,
            "client_id": client_id,
            "comment": note,
            "order_id": order_id,
            "skip": 0
            ] as [String: Any]
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
//                print(response)
                switch response.result{
                case .success:
//                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetEvaluationNote!()
                        break
                    case 400:
                        self.delegate?.didFailSetEvaluationNote!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSetEvaluationNote!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailSetEvaluationNote!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetEvaluationNote!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func SetEvaluationCategories(client_id: Int, establishment_id: Int, evaluation_answer_id: Int, data: [Dictionary<String,Any>], notes: String="", order_id: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.set_evaluation_categories
        let parameters = [
            "evaluation_id": evaluation_answer_id,
            "establishment_id": establishment_id,
            "client_id": client_id,
            "data": data,
            "order_id": order_id,
            "comment": notes
            ] as [String: Any]
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
//                                print(response)
                switch response.result{
                case .success:
//                                        debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetEvaluationCategories!()
                        break
                    case 400:
                        self.delegate?.didFailSetEvaluationCategories!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSetEvaluationCategories!(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailSetEvaluationCategories!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetEvaluationCategories!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func getNewsFeeds() {
        let request = Constants.baseURL + Constants.generals + Constants.news_feed_home
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let news = self.makeNewsHome(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetNewsFeedHome!(news: news)
                        break
                    case 400:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetNewsFeedHome!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getNewsFeedsHomeNew(state: String) {
        let request = Constants.baseURL + Constants.generals + Constants.news_feed_home_new_new + "\(state)"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let news = self.makeNewsHome(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetNewsFeedHome!(news: news)
                        break
                    case 400:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetNewsFeedHome!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func getInvoicingByPickpal(establishmentId: Int) {
        let request = Constants.baseURL + Constants.establishment + Constants.get_invoicing_by_pickpal + "\(establishmentId)/"
        
       // print("invoicing URL", request)
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print(response.value)
                        let invoicing = self.makeInvoicing(info: response.value as! NSDictionary)
                        self.delegate?.didSuccessGetInvoicing!(invoicingInfo: invoicing)
                        break
                    case 400:
                        self.delegate?.didFailGetInvoicing!(error: "Error", subtitle: "No podemos procesar tu solicitud")
                        break
                    case 404:
                        self.delegate?.didFailGetInvoicing!(error: "Error", subtitle: "No podemos procesar tu solicitud")
                        break
                    case 500:
                        self.delegate?.didFailGetInvoicing!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetInvoicing!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetInvoicing!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getNewsFeedsNew(state: String) {
        let request = Constants.baseURL + Constants.generals + Constants.news_feed_home_new_new + "\(state)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let news = self.makeNewsHome(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetNewsFeedHome!(news: news)
                        break
                    case 400:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetNewsFeedHome!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetNewsFeedHome!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewMyPointEstablishment(userId: String, establishmentId: String) {
        let request = Constants.baseURL + Constants.users + Constants.viewMyPoints + userId + "/" + establishmentId
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let rew = self.makeMyReward(info: response.value as! NSDictionary)
                        self.delegate.didSuccessViewMyReward?(reward: rew)
                        break
                    case 400:
                        self.delegate?.didFailViewMyReward!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailViewMyReward!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailViewMyReward!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailViewMyReward!(error: "Ha ocurrido un error", subtitle: "Inténtalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailViewMyReward!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
}
extension NSObject {
    func makeTypesOrders (info: NSDictionary) -> [String] {
        var typesOrders = [String]()
        
        for data in (info).value(forKey: "orders") as! NSArray{
            if data as! String == "SM"{
                typesOrders.append("Servicio a Mesa")
            }
            
            if data as! String == "RB"{
                typesOrders.append("Recoger en Barra")
            }
            
            if data as! String == "RS"{
                typesOrders.append("Recoger en Sucursal")
            }
            
            if data as! String == "SD"{
                typesOrders.append("Servicio a Domicilio")
            }
            
            
        }
        
        return typesOrders
    }

    func makeTableAppStore (info: NSArray) -> [TableInfo] {
        var ti = [TableInfo]()
        for storeInfo in info {
            let storeI = TableInfo()
            if let location = (storeInfo as! NSDictionary).value(forKey: "location") as? NSDictionary {
                storeI.latitude  = location.value(forKey: "latitude") as! Double
                storeI.longitude = location.value(forKey: "longitude") as! Double
            }else{
                storeI.latitude  = 0.0
                storeI.longitude = 0.0
            }
            storeI.municipality = (storeInfo as! NSDictionary).value(forKey: "municipality") as! String
            storeI.state = (storeInfo as! NSDictionary).value(forKey: "state") as! String
            storeI.establishment_category = (storeInfo as! NSDictionary).value(forKey: "establishment_category") as! String
            storeI.place = (storeInfo as! NSDictionary).value(forKey: "place") as! String
            storeI.establishment_name = (storeInfo as! NSDictionary).value(forKey: "establishment_name") as! String
            storeI.reference_establishment_id = (storeInfo as! NSDictionary).value(forKey: "reference_establishment_id") as! Int
            storeI.version_cks = (storeInfo as! NSDictionary).value(forKey: "version_cks") as! Int
            ti.append(storeI)
        }
        return ti
    }
    
    func makeDescriptive (info: NSArray) -> [DescriptiveEstablishment] {
        var de = [DescriptiveEstablishment]()
        for storeInfo in info {
            let storeI = DescriptiveEstablishment()
            if let location = (storeInfo as! NSDictionary).value(forKey: "location") as? NSDictionary {
                storeI.latitude  = location.value(forKey: "latitude") as! Double
                storeI.longitude = location.value(forKey: "longitude") as! Double
            }else{
                storeI.latitude  = 23.60408679937719
                storeI.longitude = -102.5035889935411
            }
            storeI.type_kitchen = (storeInfo as! NSDictionary).value(forKey: "type_kitchen") as! String
            storeI.reference_stablishment_id = (storeInfo as! NSDictionary).value(forKey: "reference_establishment_id") as! Int
            storeI.version_cks = (storeInfo as! NSDictionary).value(forKey: "version_cks") as! Int
            de.append(storeI)
        }
        return de
    }
    
    func makeEstablishments (info: NSArray) -> [Establishment] {
        var st = [Establishment]()
    
        for storeInfo in info {
            let storeI = Establishment()
            var category = NSDictionary()
            var pickpalService = [String]()
            var order_types = [String]()
            
            if let cat = (storeInfo as! NSDictionary).value(forKey: "establishment_category") as? NSDictionary {
                category = cat
            }else{
                category = ["name":""]
            }
            let businessArea = (storeInfo as! NSDictionary).value(forKey: "business_area") as! NSDictionary
            storeI.business_area = businessArea.value(forKey: "name") as? String
            let place = (storeInfo as! NSDictionary).value(forKey: "place") as! NSDictionary
            storeI.category  = category.value(forKey: "name") as? String
            storeI.place = place.value(forKey: "name") as? String
            
            storeI.id = (storeInfo as! NSDictionary).value(forKey: "id") as? Int
            storeI.is_open = (storeInfo as! NSDictionary).value(forKey: "is_open") as? Bool
            storeI.name = (storeInfo as! NSDictionary).value(forKey: "name") as? String
            storeI.future_available = (storeInfo as! NSDictionary).value(forKey: "future_available") as? Bool
            if let logo = (storeInfo as! NSDictionary).value(forKey: "logo") as? String {
                storeI.logo = logo
            }
            if let logo = (storeInfo as! NSDictionary).value(forKey: "image") as? String {
                storeI.smallImg = logo
            }
            for servicePickPal in (storeInfo as! NSDictionary).value(forKey: "pickpal_services") as! NSArray{
                pickpalService.append(servicePickPal as! String)
            }
            
            for order_type in (storeInfo as! NSDictionary).value(forKey: "order_types") as! NSArray{
                order_types.append(order_type as! String)
            }
            
            storeI.servicePlus = (storeInfo as! NSDictionary).value(forKey: "service_plus") as? Bool
            storeI.pickpal_services = pickpalService
            storeI.order_types = order_types
            storeI.time = (storeInfo as! NSDictionary).value(forKey: "time") as? String
            storeI.rating = (storeInfo as! NSDictionary).value(forKey: "rating") as? Int
            st.append(storeI)
        }
        return st
    }
    
    func makeQuestions (info: NSArray) -> [Question] {
        var questions = [Question]()
        for element in info {
            let question = Question()
            if let items = (element as! NSDictionary).value(forKey: "products") as? [String] {
                question.items = items
            }
            if let categories = (element as! NSDictionary).value(forKey: "categories") as? [Int] {
                question.categories = categories
            }
            if let labels = (element as! NSDictionary).value(forKey: "labels") as? [String] {
                question.labels = labels
            }
            question.id = (element as! NSDictionary).value(forKey: "id") as! Int
            question.question = (element as! NSDictionary).value(forKey: "question") as! String
            //question.img = (element as! NSDictionary).value(forKey: "image") as! String
            questions.append(question)
        }
        return questions
    }
    
    func makeSingleEstablishment (info: NSDictionary) -> SingleEstablishment {
//        print(info)
        let st = SingleEstablishment()
        let address = EstablishementAddress()
        var gallery = [String]()
        var operation_schedule = [ScheduleItem]()
        var pickpalService = [String]()
        var order_types = [String]()
        if let category = (info).value(forKey: "establishment_category") as? NSDictionary {
            st.category  = category.value(forKey: "name") as! String
        }
        
        for servicePickPal in (info).value(forKey: "pickpal_services") as! NSArray{
            pickpalService.append(servicePickPal as! String)
        }
        st.pickpal_services = pickpalService
        
        for types in (info).value(forKey: "order_types") as! NSArray{
            order_types.append(types as! String)
        }
        st.order_types = order_types
        st.service_plus = (info).value(forKey: "service_plus") as? Bool
        //let category = (info).value(forKey: "establishment_category") as! NSDictionary
        let place = (info).value(forKey: "place") as! NSDictionary
        if let business_area = (info).value(forKey: "business_area") as? NSDictionary{
            st.busines = business_area.value(forKey: "name") as! String
        }else{
            st.busines = ""
        }
        
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        address.street = temp_address.value(forKey: "street") as! String
        address.colony = temp_address.value(forKey: "colony") as! String
        address.city = temp_address.value(forKey: "city") as! String
        address.state = temp_address.value(forKey: "state") as! String
        address.country = temp_address.value(forKey: "country") as! String
        address.zip_code = temp_address.value(forKey: "zip_code") as! String
        
        
        if let referenceLocation = temp_address.value(forKey: "reference_location") as? String {
        
            address.referencePlace =  referenceLocation
            
        }else{
            
            address.referencePlace = ""
            
        }
        
        let location = (info).value(forKey: "location") as! NSDictionary
        st.latitude = location.value(forKey: "latitude") as! Double
        st.longitude = location.value(forKey: "longitude") as! Double
       
        let social = SocialInformation()
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            social.delivery = (socialInfo).value(forKey: "delivery") as? Bool
            social.payment_methods = (socialInfo).value(forKey: "payment_methods") as? String
        }
        st.socialInfo = social
        st.generic_service = (info).value(forKey: "generic_service") as? Bool
        st.place = place.value(forKey: "name") as! String
        st.id = (info).value(forKey: "id") as! Int
        let kitchenType = info.value(forKey: "kitchen_type") as! [String]
//        print(kitchenType.joined(separator: ", "))
        st.kitchen_type = kitchenType.joined(separator: ", ")
        st.address = address
        for image in (info).value(forKey: "establishment_gallery") as! NSArray{
            gallery.append(image as! String)
        }
        st.gallery = gallery
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as! String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as! String
            x.day = (item as! NSDictionary).value(forKey: "day") as! String
            operation_schedule.append(x)
        }
        st.operation_schedule = operation_schedule
        st.company_name = (info).value(forKey: "name") as! String
        st.name = (info).value(forKey: "establishment_name") as! String
        st.descripcion = (info).value(forKey: "description") as! String
        if let logo = (info).value(forKey: "logo") as? String {
            st.logo = logo
        }
        st.phone = (info).value(forKey: "phone") as! String
        st.rating = (info).value(forKey: "rating") as! Int
        let menu = info.value(forKey: "menu") as! NSDictionary
        let menu2 = Menu()
        var categories = [Categories]()
        
        for cat in menu.value(forKey: "categories") as? NSArray ?? NSArray() {
            let category = Categories()
            print(cat)
            category.name = (cat as! NSDictionary).value(forKey: "name") as! String
//            category.include_alcoholic = ((cat as! NSDictionary).value(forKey: "include_alcoholic") as! Bool)
            var itms = [Items]()
            for item in (cat as! NSDictionary).value(forKey: "items") as! NSArray {
                let itm = Items()
                itm.id = ((item as! NSDictionary).value(forKey: "id") as! Int)
                itm.images = ((item as! NSDictionary).value(forKey: "images") as! [String])
                itm.name = ((item as! NSDictionary).value(forKey: "name") as! String)
                itm.descr = ((item as! NSDictionary).value(forKey: "description") as! String)
                itm.price = ((item as! NSDictionary).value(forKey: "price") as! Double)
                itm.category = ((cat as! NSDictionary).value(forKey: "name") as! String)
                itm.is_package = ((item as! NSDictionary).value(forKey: "is_package") as! Bool)
                itm.prep_time = ((item as! NSDictionary).value(forKey: "preparation_time") as! Int)
                itm.prep_time_max = ((item as! NSDictionary).value(forKey: "preparation_time_max") as! Int)
                //itm.rating_neg = ((item as! NSDictionary).value(forKey: "rating_negative") as! Int)
                //itm.rating_pos = ((item as! NSDictionary).value(forKey: "rating_positive") as! Int)
                itm.limit_extra = ((item as! NSDictionary).value(forKey: "limit_extra") as! Int)
                itm.in_stock = ((item as! NSDictionary).value(forKey: "in_stock") as! Bool)
                itm.is_alcoholic = ((item as! NSDictionary).value(forKey: "is_alcoholic") as! Bool)
                if let season = (item as! NSDictionary).value(forKey: "season") as? Int {
                    itm.season = season
                }else{
                    itm.season = 0
                }
                var comp = [Complements]()
                if let complements = (item as! NSDictionary).value(forKey: "complements") as? NSArray{
                    if complements.count > 0 {
                        let compl = Complements()
                        compl.complement_name = "Extras"
                        var comp_prop = [complement_prop]()
                        for comple in complements {
                            let compl_prop = complement_prop()
                            compl_prop.complement_name = (comple as! NSDictionary).value(forKey: "complement_name") as! String
                            compl_prop.id = (comple as! NSDictionary).value(forKey: "id_extra") as! Int
                            compl_prop.value = (comple as! NSDictionary).value(forKey: "price") as! String
                            compl_prop.in_stock = (comple as! NSDictionary).value(forKey: "in_stock") as! Bool
                            compl_prop.is_active = (comple as! NSDictionary).value(forKey: "is_active") as? Bool
                            comp_prop.append(compl_prop)
                        }
                        compl.complements = comp_prop
                        comp.append(compl) 
                        
                    }
                    itm.complements = comp
                }

                if let modifiers = (item as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                    var modi = [Modifiers]()
                    for modifi in modifiers {
//                        print(modifi)
                        let mod = Modifiers()
                        mod.modifiersName = ((modifi as! NSDictionary).value(forKey: "category") as! String)
                        mod.max_required = ((modifi as! NSDictionary).value(forKey: "max") as! Int)
                        mod.requiredInfo = ((modifi as! NSDictionary).value(forKey: "required") as! Bool)
                        if let modifiers_inside = (modifi as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            var mod_prop = [modifier_prop]()
                            for modi2 in modifiers_inside {
                                let modifiers_prop = modifier_prop()
                                modifiers_prop.modifier_name = ((modi2 as! NSDictionary).value(forKey: "modifier_name") as! String)
                                modifiers_prop.id = ((modi2 as! NSDictionary).value(forKey: "id") as! Int)
                                modifiers_prop.value = ((modi2 as! NSDictionary).value(forKey: "price") as! Double)
                                modifiers_prop.is_active = ((modi2 as! NSDictionary).value(forKey: "is_active") as! Bool)
                                mod_prop.append(modifiers_prop)
                            }
                            mod.modifiers = mod_prop
                        }
                       modi.append(mod)
                    }
                    itm.modifiers = modi
                }
                itms.append(itm)
                category.items = itms
            }
            categories.append(category)
        }
        menu2.categories = categories
        st.menu = menu2
        return st
    }
    
    func makeKitchen (info: NSArray) -> [Kitchens] {
        var kitch = [Kitchens]()
        for kitchen in info {
            let kit = Kitchens()
            kit.image = (kitchen as! NSDictionary).value(forKey: "image") as! String
            kit.name = (kitchen as! NSDictionary).value(forKey: "name") as! String
            kit.hide = (kitchen as! NSDictionary).value(forKey: "hide") as! Bool
            kitch.append(kit)
        }
        return kitch
    }
    
    func makeSingleKitchen (info: NSArray) -> [Kitchens] {
        var kitch = [Kitchens]()
        for kitchen in info {
            let kit = Kitchens()
            kit.image = (kitchen as! NSDictionary).value(forKey: "image") as! String
            kit.name = (kitchen as! NSDictionary).value(forKey: "name") as! String
            kitch.append(kit)
        }
        return kitch
    }
    
    func makeNewsHome (info: NSArray) -> [News]{
        var nws = [News]()
        for z in info {
            let new = News()
            if let id = (z as! NSDictionary).value(forKey: "id") as? Int {
                new.id = id
            }
            if let title = (z  as! NSDictionary).value(forKey: "title") as? String {
                new.title = title
            }
            if let descrp = (z  as! NSDictionary).value(forKey: "content") as? String {
                new.content = descrp
            }
            if let image = (z  as! NSDictionary).value(forKey: "image") as? String {
                new.image = image
            }
            if let big_image = (z  as! NSDictionary).value(forKey: "big_image") as? String {
                new.big_image = big_image
            }
            if let category = (z  as! NSDictionary).value(forKey: "category") as? String {
                new.category = category
            }
            nws.append(new)
        }
        return nws
    }
    func makeInvoicing (info: NSDictionary) -> Invoicing{
        let invo = Invoicing()
        invo.order_mode = (info.value(forKey: "order_mode") as! [Int])
        invo.allow_points_movile = (info.value(forKey: "allow_points_movile") as! Bool)
        invo.payment_str = (info.value(forKey: "payment_str") as! String)
        invo.invoicing_by_pickpal = (info.value(forKey: "invoicing_by_pickpal") as! Bool)
        invo.establishment_id = (info.value(forKey: "establishment_id") as! Int)
        invo.available_service  = (info.value(forKey: "order_types") as! [String])
        invo.order_payments  = (info.value(forKey: "order_payments") as! NSDictionary)
        invo.minimum_billing  = (info.value(forKey: "minimum_billing") as! Double)
        
        let invoSched = ScheduleOrders()
        let schedule_orders = info.value(forKey: "schedule_orders") as! NSDictionary
        invoSched.finish_hour = (schedule_orders.value(forKey: "finish_hour") as! String)
        invoSched.order_type = (schedule_orders.value(forKey: "order_type") as! Int)
        invoSched.is_active = (schedule_orders.value(forKey: "is_active") as! Bool)
        invoSched.open_date = (schedule_orders.value(forKey: "open_date") as! String)
        invoSched.finish_date = (schedule_orders.value(forKey: "finish_date") as! String)
        invoSched.pickpal_service_cost = (schedule_orders.value(forKey: "pickpal_service") as? NSNumber )?.floatValue
        
        
        
        
        invo.schedule_orders = invoSched
        invo.payment_methods = (info.value(forKey: "payment_methods") as! Int)
        invo.carry_mode = (info.value(forKey: "carry_mode") as! Int)
        invo.default_reward = (info.value(forKey: "default_reward") as! Int)
        if let  num_tables = (info.value(forKey: "number_tables") as? Int) {
            invo.number_tables = num_tables
        }else{
            invo.number_tables = 0
        }
        return invo
    }
    
    
    func makeMyReward (info: NSDictionary) -> MyRewardEstablishment{
        let reward = MyRewardEstablishment()
        
        reward.establishmentId = (info.value(forKey: "establishment_id") as! Int)
        reward.tipoCambio = (info.value(forKey: "tipo_cambio") as! Double)
        reward.totalMoney = (info.value(forKey: "total_money") as! Double)
        reward.totalPoints = (info.value(forKey: "total_points") as! Double)
        reward.establishmentName = (info.value(forKey: "establishment_name") as! String)
        reward.minExchange = (info.value(forKey: "min_exchange") as! Double)
        return reward
    }
}

extension String {
    var stripingDiacritics: String {
        applyingTransform(.stripDiacritics, reverse: false)!
    }
}
