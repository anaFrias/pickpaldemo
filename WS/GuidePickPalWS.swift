//
//  GuidePickPalWS.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/20/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol GuideDelegate: class {
    
    @objc optional func didSuccessCountEstablishmentGuide(number: Int)
    @objc optional func didFailCountEstablishmentGuide(error: String, subtitle: String)
    
    @objc optional func didSuccessGetViewEstablishmentAround(info: [IdsKm], ids: [Int])
    @objc optional func didFailGetViewEstablishmentAround(error: String, subtitle: String)
    
    @objc optional func didSuccessGetGuideEstablshmentsById(info: [EstablishmentsGuide])
    @objc optional func didFailGetGuideEstablshmentsById(error: String, subtitle: String)
    
    @objc optional func didSuccessGetGuideCategories(info: [GuideCategories])
    @objc optional func didFailGetGuideCategories(error: String, subtitle: String)
    
    @objc optional func didSuccessSingleEstablishmentGuide(info: SingleEstablishment)
    @objc optional func didFailSingleEstablishmentGuide(error: String, subtitle: String)
    
}

public class GuidePickPalWS: NSObject{
    
    var delegate: GuideDelegate!
    
    func CountEstablishmentGuide() {
        let request = Constants.baseURL + Constants.establishment + Constants.count_estab_guide
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let total = (response.value as! NSDictionary).value(forKey: "total_pickpal") as! Int
                        self.delegate.didSuccessCountEstablishmentGuide?(number: total)
                        break
                    case 400:
                        self.delegate?.didFailCountEstablishmentGuide?(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailCountEstablishmentGuide?(error: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailCountEstablishmentGuide?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailCountEstablishmentGuide?(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailCountEstablishmentGuide?(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewEstablishmentAroundGuide(latitude: Double, longitude: Double, codeState: String) {
        let parameters = [
            "latitud": latitude,
            "longitud": longitude,
            "state_code": codeState.stripingDiacritics
            ] as [String:Any]
        let request = Constants.baseURL + Constants.establishment + Constants.view_establishment_around_guide
        print(request)
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                print("paso")
                print(response)
                switch response.result{
                case .success:
                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessGetViewEstablishmentAround!(info: self.makeIdsKms(info: response.value as! NSDictionary).0, ids: self.makeIdsKms(info: response.value as! NSDictionary).1)
                        /*if let ids = (response.value as! NSDictionary).value(forKey: "ids") as? [Int] {
                         self.delegate?.didSuccessGetViewEstablishmentAround!(info: self.makeIdsKms(info: response.value as! NSDictionary))
                         }else{
                         self.delegate?.didFailGetViewEstablishmentAround!(error: "empty", subtitle: "empty")
                         }*/
                        
                        break
                    case 400:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetViewEstablishmentAround!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    
                    self.delegate?.didFailGetViewEstablishmentAround!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getGuideEstablishmentsById(ids: [Int], client_id:Int){
        let parameters = [
            "ids": ids,
            "order_id": false,
            "client_id": client_id
            ] as [String : Any]
        let url = "\(Constants.baseURL)\(Constants.establishment)\(Constants.get_establishments_by_ids_guide)"
        print(parameters)
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishments = self.makeEstablishmentsGuide(info: (response.value as! NSArray))
                        print((response.value as! NSArray))
                        self.delegate.didSuccessGetGuideEstablshmentsById!(info: establishments)
                        break
                    case 204:
                        self.delegate.didFailGetGuideEstablshmentsById!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetGuideEstablshmentsById!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetGuideEstablshmentsById!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetGuideEstablshmentsById!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getCategoriesGuide(codeState: String) {
        let request = Constants.baseURL + Constants.establishment + Constants.get_all_business_line_state + "\(codeState.stripingDiacritics)/"
        print(request)
        Alamofire.request(request, method: .get, parameters: nil, encoding:  JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let establishments = self.makeCategories(info: (response.value as! NSArray))
                        print((response.value as! NSArray))
                        self.delegate.didSuccessGetGuideCategories!(info: establishments)
                        break
                    case 204:
                        self.delegate.didFailGetGuideCategories!(error: "No hay establecimientos", subtitle: "para los filtros seleccionados")
                        break
                    case 400:
                        self.delegate.didFailGetGuideCategories!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate.didFailGetGuideCategories!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate.didFailGetGuideCategories!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewSingleEstablishment(establishmentID: Int){
        let url = "\(Constants.baseURL)\(Constants.establishment)\(Constants.view_single_guide_establishment)\(establishmentID)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print(response)
                        let single = self.makeSingleEstablishmentGuide(info: (response.value as! NSDictionary))
                        print(single)
                        self.delegate.didSuccessSingleEstablishmentGuide!(info: single)
                        break
                    case 400:
                        self.delegate.didFailSingleEstablishmentGuide!(error: "Error", subtitle: "Error en la petición")
                        break
                    case 404:
                        self.delegate.didFailSingleEstablishmentGuide!(error: "Sin coincidencias", subtitle: "establecimiento o cliente no encontrado")
                        break
                    default:
                        self.delegate.didFailSingleEstablishmentGuide!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            })
        
    }
    
}

//Mark.- Funciones para leer informacion de cada json
extension GuidePickPalWS{
    
    func makeEstablishmentsGuide(info: NSArray) -> [EstablishmentsGuide] {
        var st = [EstablishmentsGuide]()
        
        for storeInfo in info {
            let storeI = EstablishmentsGuide()
            var pickpalService = [String]()
            var foodTypes = [String]()
            
            let place = (storeInfo as! NSDictionary).value(forKey: "place") as! NSDictionary
            let bussinesArea = (storeInfo as! NSDictionary).value(forKey: "business_area") as! NSDictionary
            storeI.business_area = bussinesArea.value(forKey: "name") as? String
            storeI.place = place.value(forKey: "name") as? String
            storeI.name = (storeInfo as! NSDictionary).value(forKey: "name") as? String
            storeI.state = (storeInfo as! NSDictionary).value(forKey: "state") as? String
            storeI.municipality = (storeInfo as! NSDictionary).value(forKey: "municipality") as? String
            storeI.service_plus = (storeInfo as! NSDictionary).value(forKey: "service_plus") as? Bool
            storeI.id = (storeInfo as! NSDictionary).value(forKey: "id") as? Int
            
            if let logo = (storeInfo as! NSDictionary).value(forKey: "image") as? String {
                storeI.image = logo
            }
            
            if let logo = (storeInfo as! NSDictionary).value(forKey: "logo") as? String {
                storeI.logo = logo
            }
            
            for servicePickPal in (storeInfo as! NSDictionary).value(forKey: "pickpal_services") as! NSArray{
                pickpalService.append(servicePickPal as! String)
            }
            
            for foodType in (storeInfo as! NSDictionary).value(forKey: "food_types") as! NSArray{
                foodTypes.append(foodType as! String)
            }
            
            storeI.foodTypes = foodTypes
            storeI.pickpal_services = pickpalService
            st.append(storeI)
        }
        return st
    }
    
    func makeCategories(info: NSArray) -> [GuideCategories] {
        var cat = [GuideCategories]()
        
        for guideCategory in info{
            let guideCat = GuideCategories()
            
            guideCat.name = (guideCategory as! NSDictionary).value(forKey: "name") as? String
            guideCat.image = (guideCategory as! NSDictionary).value(forKey: "image") as? String
            guideCat.hide = (guideCategory as! NSDictionary).value(forKey: "hide") as? Bool
            
            cat.append(guideCat)
        }
        
        return cat
    }
    
    func makeIdsKms(info: NSObject) -> ([IdsKm], [Int]){
        var st = [IdsKm]()
        
        let twoKms = (info).value(forKey: "2kms") as? [Int] ?? NSArray() as! [Int]
        
        let fiveKms = (info).value(forKey: "5kms") as? [Int] ?? NSArray() as! [Int]
        
        let tenKms = (info).value(forKey: "10kms") as? [Int] ?? NSArray() as! [Int]
        
        let state = (info).value(forKey: "state") as? [Int] ?? NSArray() as! [Int]
        
        let ids = (info).value(forKey: "ids") as? [Int] ?? NSArray() as! [Int]
        
        for id in ids{
            let idsTwoKmList = IdsKm()
            for two in twoKms{
                if id == two {
                    idsTwoKmList.id = id
                    idsTwoKmList.Km = "2 km"
                    st.append(idsTwoKmList)
                }
            }
        }
        
        for id in ids{
            let idsFiveKmList = IdsKm()
            for five in fiveKms{
                if id == five {
                    idsFiveKmList.id = id
                    idsFiveKmList.Km = "5 km"
                    st.append(idsFiveKmList)
                }
            }
        }
        
        for id in ids{
            let idsTenKmList = IdsKm()
            for ten in tenKms{
                if id == ten {
                    idsTenKmList.id = id
                    idsTenKmList.Km = "10 km"
                    st.append(idsTenKmList)
                }
            }
        }
        
        for id in state{
            let idsState = IdsKm()
            for state in state{
                if id == state {
                    idsState.id = id
                    idsState.Km = "> 10 km"
                    st.append(idsState)
                }
            }
            
        }
        
        return (st, ids)
    }
    
    func makeSingleEstablishmentGuide (info: NSDictionary) -> SingleEstablishment {
        //        print(info)
        let st = SingleEstablishment()
        let address = EstablishementAddress()
        var gallery = [String]()
        var operation_schedule = [ScheduleItem]()
        var pickpalService = [String]()
        
        if let category = (info).value(forKey: "establishment_category") as? NSDictionary {
            st.category  = category.value(forKey: "name") as! String
        }
        //let category = (info).value(forKey: "establishment_category") as! NSDictionary
        let place = (info).value(forKey: "place") as! NSDictionary
        if let business_area = (info).value(forKey: "business_area") as? NSDictionary{
            st.busines = business_area.value(forKey: "name") as! String
        }else{
            st.busines = ""
        }
        
        let temp_address = (info).value(forKey: "address") as! NSDictionary
        address.street = temp_address.value(forKey: "street") as! String
        address.colony = temp_address.value(forKey: "colony") as! String
        address.city = temp_address.value(forKey: "city") as! String
        address.state = temp_address.value(forKey: "state") as! String
        address.country = temp_address.value(forKey: "country") as! String
        address.zip_code = temp_address.value(forKey: "zip_code") as! String
        address.referencePlace = temp_address.value(forKey: "reference_location") as! String
        
        if let noInt = temp_address.value(forKey: "n_int") as? String {
            address.noInt = noInt
        }else{
            address.noInt = ""
        }
        
        if let noExt = temp_address.value(forKey: "n_ext") as? String {
            address.noExt = noExt
        }else{
            address.noExt = ""
        }
        
        if let ref = temp_address.value(forKey: "reference_location") as? String {
            st.reference_location = ref
        }else{
            st.reference_location = ""
        }
        
        let location = (info).value(forKey: "location") as! NSDictionary
        st.latitude = location.value(forKey: "latitude") as! Double
        st.longitude = location.value(forKey: "longitude") as! Double
        
        st.place = place.value(forKey: "name") as! String
        st.id = (info).value(forKey: "id") as! Int
        let kitchenType = info.value(forKey: "kitchen_type") as! [String]
        //        print(kitchenType.joined(separator: ", "))
        st.kitchen_type = kitchenType.joined(separator: ", ")
        st.address = address
        for image in (info).value(forKey: "establishment_gallery") as! NSArray{
            gallery.append(image as! String)
        }
        st.gallery = gallery
        for item in (info).value(forKey: "operation_schedule") as! NSArray{
            let x = ScheduleItem()
            x.open_hour = (item as! NSDictionary).value(forKey: "opening_hour") as! String
            x.closing_hour = (item as! NSDictionary).value(forKey: "closing_hour") as! String
            x.day = (item as! NSDictionary).value(forKey: "day") as! String
            operation_schedule.append(x)
        }
        st.operation_schedule = operation_schedule
        st.company_name = (info).value(forKey: "name") as! String
        st.name = (info).value(forKey: "name") as! String
        st.descripcion = (info).value(forKey: "description") as! String
        if let logo = (info).value(forKey: "logo") as? String {
            st.logo = logo
        }
        st.phone = (info).value(forKey: "phone") as! String
        for servicePickPal in (info).value(forKey: "pickpal_services") as! NSArray{
            pickpalService.append(servicePickPal as! String)
        }
        st.pickpal_services = pickpalService
        let menu = info.value(forKey: "menu") as! NSDictionary
        let menu2 = Menu()
        var categories = [Categories]()
        if let socialInfo = (info).value(forKey: "social_information") as? NSDictionary {
            let social = SocialInformation()
            social.siteURL = (socialInfo).value(forKey: "site_url") as? String
            social.instagramURL = (socialInfo).value(forKey: "instagram_url") as? String
            social.facebookURL = (socialInfo).value(forKey: "facebook_url") as? String
            social.twitterURL = (socialInfo).value(forKey: "twitter_url") as? String
            social.delivery = (socialInfo).value(forKey: "delivery") as? Bool
            social.payment_methods = (socialInfo).value(forKey: "payment_methods") as? String
            st.socialInfo = social
        }
        
        for cat in menu.value(forKey: "categories") as? NSArray ?? NSArray() {
            let category = Categories()
            print(cat)
            category.name = (cat as! NSDictionary).value(forKey: "name") as! String
            //            category.include_alcoholic = ((cat as! NSDictionary).value(forKey: "include_alcoholic") as! Bool)
            var itms = [Items]()
            for item in (cat as! NSDictionary).value(forKey: "items") as! NSArray {
                let itm = Items()
                itm.id = ((item as! NSDictionary).value(forKey: "id") as! Int)
                itm.images = ((item as! NSDictionary).value(forKey: "images") as! [String])
                itm.name = ((item as! NSDictionary).value(forKey: "name") as! String)
                itm.descr = ((item as! NSDictionary).value(forKey: "description") as! String)
                itm.price = ((item as! NSDictionary).value(forKey: "price") as! Double)
                itm.category = ((cat as! NSDictionary).value(forKey: "name") as! String)
                itm.is_package = ((item as! NSDictionary).value(forKey: "is_package") as! Bool)
                
                var comp = [Complements]()
                if let complements = (item as! NSDictionary).value(forKey: "complements") as? NSArray{
                    if complements.count > 0 {
                        let compl = Complements()
                        compl.complement_name = "Extras"
                        var comp_prop = [complement_prop]()
                        for comple in complements {
                            let compl_prop = complement_prop()
                            compl_prop.complement_name = (comple as! NSDictionary).value(forKey: "complement_name") as! String
                            compl_prop.id = (comple as! NSDictionary).value(forKey: "id_extra") as! Int
                            compl_prop.value = (comple as! NSDictionary).value(forKey: "price") as! String
                            compl_prop.in_stock = (comple as! NSDictionary).value(forKey: "in_stock") as! Bool
                            compl_prop.is_active = (comple as! NSDictionary).value(forKey: "is_active") as? Bool
                            comp_prop.append(compl_prop)
                        }
                        compl.complements = comp_prop
                        comp.append(compl)
                        
                    }
                    itm.complements = comp
                }
                
                if let modifiers = (item as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                    var modi = [Modifiers]()
                    for modifi in modifiers {
                        //                        print(modifi)
                        let mod = Modifiers()
                        mod.modifiersName = ((modifi as! NSDictionary).value(forKey: "category") as! String)
                        mod.max_required = ((modifi as! NSDictionary).value(forKey: "max") as! Int)
                        mod.requiredInfo = ((modifi as! NSDictionary).value(forKey: "required") as! Bool)
                        if let modifiers_inside = (modifi as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            var mod_prop = [modifier_prop]()
                            for modi2 in modifiers_inside {
                                let modifiers_prop = modifier_prop()
                                modifiers_prop.modifier_name = ((modi2 as! NSDictionary).value(forKey: "modifier_name") as! String)
                                modifiers_prop.id = ((modi2 as! NSDictionary).value(forKey: "id") as! Int)
                                modifiers_prop.value = ((modi2 as! NSDictionary).value(forKey: "price") as! Double)
                                modifiers_prop.is_active = ((modi2 as! NSDictionary).value(forKey: "is_active") as! Bool)
                                mod_prop.append(modifiers_prop)
                            }
                            mod.modifiers = mod_prop
                        }
                        modi.append(mod)
                    }
                    itm.modifiers = modi
                }
                itms.append(itm)
                category.items = itms
            }
            categories.append(category)
        }
        menu2.categories = categories
        st.menu = menu2
        return st
    }
}
