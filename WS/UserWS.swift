//
//  UserWS.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Alamofire
@objc protocol userDelegate: class {
    @objc optional func didSuccessLogin(is_register: Bool, client_id: Int, complete_info: Bool, phone: Bool, code: Bool)
    @objc optional func didFailLogin(title: String, statusCode: Int, subtitle: String)
    
    @objc optional func didSuccessLoginWithMail(avatar: String, email: String, first_name: String, id: Int, news_feed: Int)
    @objc optional func didSuccessLoginWithMailTwo(avatar: String, email: String, first_name: String, id: Int, news_feed: Int, is_register: Bool, complete_info: Bool, age: Int)
    @objc optional func didFailLoginWithMail(title: String, statusCode: Int, subtitle: String)
    
    @objc optional func didSuccessValidatePhone()
    @objc optional func didFailValidatePhone(title: String, subtitle: String)
    
    @objc optional func didSuccessValidatePhoneExist()
    @objc optional func didFailValidatePhoneExist(title: String, subtitle: String)
    
    @objc optional func didSuccessResendCode()
    @objc optional func didFailResendCode(title: String, subtitle: String)
    
    @objc optional func didSuccessResetPassword()
    @objc optional func didFailResetPassword(title: String, subtitle: String)
    
    @objc optional func didSuccessHelp()
    @objc optional func didFailHelp(title: String, subtitle: String)
    
    @objc optional func didSuccessDeleteAddress()
    @objc optional func didFailDeleteAddress(title: String, subtitle: String)
    
    @objc optional func didSuccessSendCode(email: String, first_name: String, newsFeed: Int, id: Int, complete_info: Bool)
    @objc optional func didFailSendCode(title: String, subtitle: String)
    
    @objc optional func didSuccessGetAbout(email: String, phone_number: String, address: String)
    @objc optional func didFailGetAbout(title: String, subtitle: String)
    
    @objc optional func didSuccessViewProfile(profile: UserProfile)
    @objc optional func didFailViewProfile(title: String, subtitle: String)
    
    @objc optional func didSuccessUpdateProfile()
    @objc optional func didFailUpdateProfile(title: String, subtitle: String)
    
    @objc optional func didSuccessValidateEmail()
    @objc optional func didFailValidateEmail(title: String, subtitle: String)
    
    @objc optional func didSuccessRegisterDeviceNotifications()
    @objc optional func didFailRegisterDeviceNotifications(title: String, subtitle: String)
    
    @objc optional func didSuccessRegister(client_id: Int)
    @objc optional func didFailRegister(title: String, subtitle: String)
    
    @objc optional func didSuccessGetTerms(terms: String)
    @objc optional func didFailGetTerms(title: String, subtitle: String)
    
    @objc optional func didSuccessGetSliders(terms: [Sliders])
    @objc optional func didFailGetSliders(title: String, subtitle: String)
    
    @objc optional func didSuccessUploadPhoto(avatar: String)
    @objc optional func didFailUploadPhoto(title: String, subtitle: String)
    
    @objc optional func didSuccessMyOrders(orders: [Orders])
    @objc optional func didFailMyOrders(title: String, subtitle: String)
    
    @objc optional func didSuccessGetNews(news: [News])
    @objc optional func didFailGetNews(title: String, subtitle: String)
    
    @objc optional func didSuccessGetPickPalComision(comision: Double)
    @objc optional func didFailGetPickPalComision(title: String, subtitle: String)
    
    @objc optional func didSuccessSetNewsFeedLog()
    @objc optional func didFailSetNewsFeedLog(title: String, subtitle: String)
    
    @objc optional func didSuccessGetNewsFeedLog(newsFeed: Int)
    @objc optional func didFailGetNewsFeedLog(title: String, subtitle: String)
    
    @objc optional func didSuccessGetUrlInvoice(url: String, message: String)
    @objc optional func didFailGetUrlInvoice(title: String, subtitle: String)
    
    @objc optional func didSuccessRegisterHelp()
    @objc optional func didFailRegisterHelp(title: String, subtitle: String)
    
    @objc optional func didSuccessRetryBillingGet(rfc: String, email: String, full_name: String, zip_code: String, tax_regime_key: Int)
    @objc optional func didFailRetryBillingGet(title: String, subtitle: String)
    
    @objc optional func didSuccessRetryBillingSet()
    @objc optional func didFailRetryBillingSet(title: String, subtitle: String)
    
    @objc optional func didSuccessGetInStates(states: [String])
    @objc optional func didFailGetInStates(title: String, subtitle: String)
    
    @objc optional func didSuccessGetLastState()
    @objc optional func didFailGetLastState(title: String, subtitle: String)
    
    @objc optional func didSuccessIsAppUpdated(isUpdated: Bool)
    @objc optional func didFailIsAppUpdated(title: String, subtitle: String)
    
    @objc optional func didSuccessStatusProfile(avalidableAdvs: StatusProfile)
    @objc optional func didFailStatusProfile(title: String, subtitle: String)
    
    @objc optional func didSuccessViewMyPoints(viewMyPoints: [ViewMyPoints])
    @objc optional func didFailViewMyPoints(title: String, subtitle: String)
    
    @objc optional func didSuccessDeleteAccount()
    @objc optional func didFailDeleteAccount(title: String, subtitle: String)
    
    @objc optional func didSuccessGetTaskRegimen(regimenList: [TaxRegimenDTO])
    @objc optional func didFailGetTaskRegimen(title: String, subtitle: String)
    
}
class UserWS: NSObject {
    var delegate: userDelegate?
    
    
    func login (email: String, first_name: String, last_name: String, token: String, id: String, record_medium: Int)  {
        var parameters = [String: Any]()
        let headers = [
            "Content-Type":"application/json"
        ]
        
        if record_medium == 3 {
            parameters = [
                "token": token,
                "id":id,
                "record_medium": record_medium,
                "first_name": first_name,
                "last_name": last_name
            ]
        }else{
            parameters = [
                "token": token,
                "id":id,
                "record_medium": record_medium,
                "email": email,
                "first_name": first_name,
                "last_name": last_name
            ]
        }
        
        Alamofire.request("\(Constants.baseURL)"+"\(Constants.login)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let statusCode: Int = response.response?.statusCode ?? 0
            switch response.result {
            case .success:
                switch statusCode {
                case 200:
                    let is_Registered = (response.value as! NSDictionary).value(forKey: "is_register") as! Bool
                    if let client_id = (response.value as! NSDictionary).value(forKey: "client_id") as? Int {
                        if let complete_info = (response.value as! NSDictionary).value(forKey: "client_id") as? Bool {
                            self.delegate?.didSuccessLogin!(is_register: is_Registered, client_id: client_id, complete_info: complete_info, phone: false, code: true)
                        }else{
                            self.delegate?.didSuccessLogin!(is_register: is_Registered, client_id: client_id, complete_info: true, phone: false, code: true)
                        }
                    }else{
                        if let complete_info = (response.value as! NSDictionary).value(forKey: "client_id") as? Bool {
                            self.delegate?.didSuccessLogin!(is_register: is_Registered, client_id: (response.value as! NSDictionary).value(forKey: "id") as! Int, complete_info: complete_info, phone: false, code: true)
                        }else{
                            self.delegate?.didSuccessLogin!(is_register: is_Registered, client_id: (response.value as! NSDictionary).value(forKey: "id") as! Int, complete_info: true, phone: false, code: true)
                        }
                    }
                    
                    break
                case 404:
                    self.delegate?.didFailLogin!(title: "Usuario", statusCode: 404, subtitle: "no encontrado")
                    break
                case 401:
                    self.delegate?.didFailLogin!(title: "Usuario", statusCode: 401, subtitle: "no activo")
                    break
                case 500:
                    self.delegate?.didFailLogin!(title: "Error", statusCode: 500, subtitle: "No se puede procesar tu solicitud")
                    break
                case 302:
                    self.delegate?.didFailLogin!(title: "Usuario existente", statusCode: 302, subtitle: "no es cliente")
                default:
                    break
                }
                break
            case .failure(let error):
                if statusCode == 302 {
                    self.delegate?.didFailLogin!(title: "Usuario existente", statusCode: 302, subtitle: "no es cliente")
                }else{
                    self.delegate?.didFailLogin!(title: "Error de comunicación", statusCode: 302, subtitle: "Asegúrate de estar conectado a internet.")
                }
                print (error)
                break
            }
        }
    }
    
    func validatePhone(client_id: Int, phone: String){
        let parameters = [
            "client_id": client_id,
            "phone":phone
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.validate_phone)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessValidatePhone!()
                        break
                    case 404:
                        self.delegate?.didFailValidatePhone!(title: "Error" , subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 401:
                        self.delegate?.didFailValidatePhone!(title: "Error", subtitle: "Número de teléfono no válido")
                        break
                    case 500:
                        self.delegate?.didFailValidatePhone!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailValidatePhone!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func registerDeviceNotifications(client_pk: Int, token: String){
        let parameters = [
            "client_pk": client_pk,
            "token":token
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.notifications)\(Constants.register_device)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRegisterDeviceNotifications!()
                        break
                    case 404:
                        self.delegate?.didFailRegisterDeviceNotifications!(title: "Error" , subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 401:
                        self.delegate?.didFailRegisterDeviceNotifications!(title: "Error", subtitle: "Número de teléfono no válido")
                        break
                    case 500:
                        self.delegate?.didFailRegisterDeviceNotifications!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRegisterDeviceNotifications!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func validatePhoneExist(phone: String){
        let parameters = [
            "phone":phone
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.validate_phone_exist)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessValidatePhoneExist!()
                        break
                    case 302:
                        self.delegate?.didFailValidatePhoneExist!(title: "", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailValidatePhoneExist!(title: "", subtitle: "")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailValidatePhoneExist!(title: "", subtitle: "")
                    break
                }
            })
    }
    
    func resendCode(client_id: Int, is_login: Int){
        let parameters = [
            "client_id":client_id,
            "is_login":is_login
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.resend_code)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessResendCode!()
                        break
                    case 404:
                        self.delegate?.didFailResendCode!(title: "Error", subtitle: "No podemos procesar tu solicitud..")
                        break
                    case 500:
                        self.delegate?.didFailResendCode!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde..")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailResendCode!(title: "Error de comunicación, asegúrate de estar conectado a internet..", subtitle: "")
                    break
                }
            })
    }
    
    func validate_phone_code(client_id: Int, code: Int, is_login: Bool){
        let parameters = [
            "client_id":client_id,
            "code": code,
            "is_login": is_login
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.validate_phone_code)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        if let email = (response.value as! NSDictionary).value(forKey: "email") as? String,  let fn = (response.value as! NSDictionary).value(forKey: "first_name") as? String, let news_feed = (response.value as! NSDictionary).value(forKey: "news_feed") as? Int, let id = (response.value as! NSDictionary).value(forKey: "id") as? Int {
                            self.delegate?.didSuccessSendCode!(email: email, first_name: fn, newsFeed: news_feed, id: id, complete_info: true)
                        }else{
                            if let id = (response.value as! NSDictionary).value(forKey: "id") as? Int {
                                if let first_name = (response.value as! NSDictionary).value(forKey: "first_name") as? String {
                                    self.delegate?.didSuccessSendCode!(email: "", first_name: first_name, newsFeed: 0, id: id, complete_info: true)
                                }else{
                                    self.delegate?.didSuccessSendCode!(email: "", first_name: "", newsFeed: 0, id: id, complete_info: true)
                                }
                                
                            }else {
                                let id = (response.value as! NSDictionary).value(forKey: "client_id") as! Int
                                if let complete_info = (response.value as! NSDictionary).value(forKey: "complete_info") as? Bool {
                                    if let first_name = (response.value as! NSDictionary).value(forKey: "first_name") as? String {
                                        self.delegate?.didSuccessSendCode!(email: "", first_name: first_name, newsFeed: 0, id: id, complete_info: complete_info)
                                    }else{
                                        self.delegate?.didSuccessSendCode!(email: "", first_name: "", newsFeed: 0, id: id, complete_info: complete_info)
                                    }
                                    
                                }
                            }
                            
                        }
                        break
                    case 400:
                        self.delegate?.didFailSendCode!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSendCode!(title: "El código no es correcto", subtitle: "intenta de nuevo")
                        break
                    case 500:
                        self.delegate?.didFailSendCode!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSendCode!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewProfile(client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.validate_profile + "\(client_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let profile = self.makeProfile(profile: response.value as! NSDictionary)
                        self.delegate?.didSuccessViewProfile!(profile: profile)
                        break
                    case 400:
                        self.delegate?.didFailViewProfile!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailViewProfile!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailViewProfile!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailViewProfile!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getTaxRegimenList() {
        let request = Constants.baseURL + Constants.users + Constants.get_tax_regimen_list
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let taskList = self.makeRegimen(regimenList: response.value as! NSArray)
                        self.delegate?.didSuccessGetTaskRegimen!(regimenList: taskList)
                        break
                    case 400:
                        self.delegate?.didFailGetTaskRegimen!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetTaskRegimen!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetTaskRegimen!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetTaskRegimen!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func updateProfile(client_id: Int, first_name: String, last_name: String, gender: String, birth_date: String, zip_code: String, email: String, is_register: Int, phone:String = "", food_preference:String = ""){
        let parameters = [
            "client_id":client_id,
            "first_name": first_name,
            "last_name": last_name,
            "gender": gender,
            "birth_date": birth_date,
            "zip_code":zip_code,
            "email": email,
            "is_register": is_register,
            "phone": phone,
            "is_complete": true,
            "food_preference": food_preference
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.update_profile)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessUpdateProfile!()
                        break
                    case 400:
                        self.delegate?.didFailUpdateProfile!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailUpdateProfile!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailUpdateProfile!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailUpdateProfile!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailUpdateProfile!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func ValidateEmail(email: String){
        let parameters = [
            "email":email
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.validate_email)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessValidateEmail!()
                        break
                    case 400:
                        self.delegate?.didFailValidateEmail!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 302:
                        self.delegate?.didFailValidateEmail!(title: "Correo electrónico", subtitle: "ya existente")
                        break
                    case 500:
                        self.delegate?.didFailValidateEmail!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailValidateEmail!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func GetTerms() {
        let request = Constants.baseURL + Constants.about + Constants.terms_and_conditions
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        
                        self.delegate?.didSuccessGetTerms!(terms: (response.value as! NSDictionary).value(forKey: "terms_and_conditions") as! String)
                        break
                    case 400:
                        self.delegate?.didFailGetTerms!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetTerms!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetTerms!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetTerms!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func RegisterEmail(password: String, email: String, phone: String){
        let parameters = [
            "password":password,
            "email":email,
            "phone":phone
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.register)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let client_id = (response.value as! NSDictionary).value(forKey: "client_id") as! Int
                        self.delegate?.didSuccessRegister!(client_id: client_id)
                        break
                    case 400:
                        self.delegate?.didFailRegister!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 302:
                        self.delegate?.didFailRegister!(title: "El correo electrónico", subtitle: "que estás tratando de registrar ya existe")
                        break
                    case 500:
                        self.delegate?.didFailRegister!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRegister!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRegister!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func LoginPhone(phone: String){
        let parameters = [
            "phone":phone
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.login)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let client_id = (response.value as! NSDictionary).value(forKey: "client_id") as! Int
                        self.delegate?.didSuccessLogin!(is_register: false, client_id: client_id, complete_info: false, phone: false, code: true)
                        break
                    case 401:
                        self.delegate?.didFailLogin!(title: "Error", statusCode: 401, subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailLogin!(title: "Error", statusCode: 404, subtitle: "Usuario no existente")
                        break
                    case 500:
                        self.delegate?.didFailLogin!(title: "Ha ocurrido un error", statusCode: 500, subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailLogin!(title: "Ha ocurrido un error", statusCode: 500, subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailLogin!(title: "Error de comunicación", statusCode: 9, subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func LoginWithMail(email: String, password: String){
        let parameters = [
            "email":email,
            "password": password
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.login_manual)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                print((response.response?.statusCode))
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        var avtr = String()
                        var mail = String()
                        var fst_nm = String()
                        var client_id = Int()
                        var newsFeed = Int()
                        var completeInfo = true
                        var isRegister = Bool()
                        var ageUser = Int()
                        if let avatar = (response.value as! NSDictionary).value(forKey: "avatar") as? String {
                            avtr = avatar
                        }
                        if let email = (response.value as! NSDictionary).value(forKey: "email") as? String {
                            mail = email
                        }
                        
                        if let first_name = (response.value as! NSDictionary).value(forKey: "first_name") as? String {
                            fst_nm = first_name
                        }
                        if let id = (response.value as! NSDictionary).value(forKey: "id") as? Int {
                            client_id = id
                        }
                        if let nwfdd = (response.value as! NSDictionary).value(forKey: "news_feed") as? Int {
                            newsFeed = nwfdd
                        }
                        if let id = (response.value as! NSDictionary).value(forKey: "client_id") as? Int {
                            client_id = id
                        }
                        if let complete_info = (response.value as! NSDictionary).value(forKey: "complete_info") as? Bool {
                            completeInfo = complete_info
                        }
                        if let is_register = (response.value as! NSDictionary).value(forKey: "is_register") as? Bool {
                            isRegister = is_register
                        }
                        if let age = (response.value as! NSDictionary).value(forKey: "age") as? Int {
                            ageUser = age
                        }
                        self.delegate?.didSuccessLoginWithMailTwo?(avatar: avtr, email: mail, first_name: fst_nm, id: client_id, news_feed: newsFeed, is_register: isRegister, complete_info: completeInfo, age: ageUser)
                        break
                    case 401:
                        self.delegate?.didFailLoginWithMail!(title: "Error", statusCode: 401, subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailLoginWithMail!(title: "Error", statusCode: 404, subtitle: "Correo electrónico no valido, ya existente")
                        break
                    case 500:
                        self.delegate?.didFailLoginWithMail!(title: "Ha ocurrido un error", statusCode: 500, subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    if let status = (response.response?.statusCode) {
                        if status == 401 {
                            self.delegate?.didFailLoginWithMail!(title: "No válido", statusCode: 9, subtitle: "Usuario y/o contraseña incorrectos.")
                        }else if status == 404 {
                            self.delegate?.didFailLoginWithMail!(title: "Error", statusCode: 404, subtitle: "Usuario y/o contraseña incorrectos.")
                        }
                    }else{
                        self.delegate?.didFailLoginWithMail!(title: "Error de comunicación", statusCode: 9, subtitle: "Asegúrate de estar conectado a internet.")
                    }
                    
                    break
                }
            })
    }
    
    func welcomeSliders() {
        let request = Constants.baseURL + Constants.about + Constants.welcome_sliders
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let sliders = self.makeSliders(sliders: response.value as! NSArray)
                        self.delegate?.didSuccessGetSliders!(terms: sliders)
                        break
                    case 400:
                        self.delegate?.didFailGetSliders!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetSliders!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetSliders!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetSliders!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetSliders!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func updateAvatar(imageFilePath: String, client_id: String){
        let fileUrl = URL(fileURLWithPath: imageFilePath)
        let request: String = Constants.baseURL + Constants.users + Constants.update_avatar
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(fileUrl, withName: "avatar")
            multipartFormData.append(client_id.data(using: .utf8, allowLossyConversion: false)!, withName: "client_id")
        }, to: request , encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: {progress in
                    UploadingView.changeMessage(newMessage: Int(progress.fractionCompleted * 100).description + " %")
                    if(progress.fractionCompleted == 1.0){
                        UploadingView.changeMessage(newMessage: "¡Hecho!")
                    }
                })
                upload.responseJSON { response in
                    if let avatar = (response.value as? NSDictionary)?.value(forKey: "avatar") as? String{
                        self.delegate?.didSuccessUploadPhoto!(avatar: avatar)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.delegate?.didFailUploadPhoto!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
            }
        })
        
    }
    
    func ResetPassword(email: String){
        let parameters = [
            "email":email
            ] as [String: String]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.reset_password)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessResetPassword!()
                        break
                    case 400:
                        self.delegate?.didFailResetPassword!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailResetPassword!(title: "Error", subtitle: "Usuario no existente")
                        break
                    case 302:
                        self.delegate?.didFailResetPassword!(title: "Error", subtitle: "Correo Electrónico no valido")
                        break
                    case 500:
                        self.delegate?.didFailResetPassword!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailResetPassword!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func Help(message: String, client_id: Int){
        let parameters = [
            "message":message,
            "client_id": client_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.about)\(Constants.help)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessHelp!()
                        break
                    case 400:
                        self.delegate?.didFailHelp!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailHelp!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func StablishmentHelp(message: String, client_id: Int, establishment_id: Int, order_id: Int){
        let parameters = [
            "message":message,
            "client_id": client_id,
            "establishment_id":establishment_id,
            "order_id": order_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.about)\(Constants.establishment_help)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessHelp!()
                        break
                    case 400:
                        self.delegate?.didFailHelp!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailHelp!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func DeleteAddress(billing_address_id: Int){
        let parameters = [
            "billing_address_id": billing_address_id
            ] as [String: Any]
        Alamofire.request("\(Constants.baseURL)\(Constants.users)\(Constants.delete_address)", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessDeleteAddress!()
                        break
                    case 400:
                        self.delegate?.didFailDeleteAddress!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailDeleteAddress!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailDeleteAddress!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func get_about(){
        let request = Constants.baseURL + Constants.about + Constants.about_pickpal
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessGetAbout!(email: ((response.value as! NSDictionary).value(forKey: "email") as? String)!, phone_number: ((response.value as! NSDictionary).value(forKey: "phone") as? String)!, address: ((response.value as! NSDictionary).value(forKey: "address") as? String)!)
                        break
                    case 400:
                        self.delegate?.didFailGetAbout!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetAbout!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetAbout!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetAbout!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewmyOrders(client_id: Int){
        let request = Constants.baseURL + Constants.users + Constants.view_my_orders + "\(client_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let orders = self.makeOrders(orders: response.value as! NSArray)
                        self.delegate?.didSuccessMyOrders!(orders: orders)
                        break
                    case 204:
                        self.delegate?.didFailMyOrders!(title: "204", subtitle: "")
                        break
                    case 400:
                        self.delegate?.didFailMyOrders!(title: "No podemos procesar tu solicitud.", subtitle: "")
                        break
                    case 404:
                        self.delegate?.didFailMyOrders!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailMyOrders!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailMyOrders!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func RetryBilligGet(order_id: Int){
        let request = Constants.baseURL + Constants.users + Constants.retry_billig_sat + "\(order_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        var rfc = String()
                        var email = String()
                        var full_name = String()
                        var zip_code = String()
                        var cfdiKey = Int()
                        if let RFC = (response.value as? NSDictionary)?.value(forKey: "rfc") as? String {
                            rfc = RFC
                        }
                        if let full = (response.value as? NSDictionary)?.value(forKey: "full_name") as? String {
                            full_name = full
                        }
                        if let EMAIL = (response.value as? NSDictionary)?.value(forKey: "email_send") as? String {
                            email = EMAIL
                        }
                        
                        if let ZIP_CODE = (response.value as? NSDictionary)?.value(forKey: "zip_code") as? String {
                            zip_code = ZIP_CODE
                        }
                        
                        if let TAX_REGIME = (response.value as? NSDictionary)?.value(forKey: "tax_regime") as? Int {
                            cfdiKey = TAX_REGIME
                        }
                        self.delegate?.didSuccessRetryBillingGet!(rfc: rfc, email: email, full_name: full_name, zip_code: zip_code, tax_regime_key: cfdiKey)
                        break
                    case 204:
                        self.delegate?.didFailRetryBillingGet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 400:
                        self.delegate?.didFailRetryBillingGet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailRetryBillingGet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailRetryBillingGet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRetryBillingGet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRetryBillingGet!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func RetryBilligSet(order_id: Int, RFC: String, name: String, email: String, zipCode: String, taxRegimen: Int){
        let parameters = [
            "tax_regime" : taxRegimen,
            "email_send" : email,
            "full_name" : name,
            "rfc" : RFC,
            "zip_code" : zipCode
        ] as [String:Any]
        let request = Constants.baseURL + Constants.users + Constants.retry_billig_sat + "\(order_id)/"
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRetryBillingSet!()
                        break
                    case 204:
                        self.delegate?.didFailRetryBillingSet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 400:
                        self.delegate?.didFailRetryBillingSet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailRetryBillingSet!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailRetryBillingSet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailRetryBillingSet!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRetryBillingSet!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func getNews(){
        let request = Constants.baseURL + Constants.generals + Constants.news_feet
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let news = self.makeNews(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetNews!(news: news)
                        break
                    case 400:
                        self.delegate?.didFailGetNews!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNews!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetNews!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetNews!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getNewsNew(state: String){
        let request = Constants.baseURL + Constants.generals + Constants.news_feed_new + "\(state)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let news = self.makeNews(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetNews!(news: news)
                        break
                    case 204:
                        self.delegate?.didFailGetNews!(title: "204", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 400:
                        self.delegate?.didFailGetNews!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNews!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetNews!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetNews!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    //                    if Int((response.response?.statusCode)!) != 204 {
                    self.delegate?.didFailGetNews!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    //                    }
                    
                    break
                }
            })
    }
    
    func getPickPalComision() {
        let request = Constants.baseURL + Constants.about + Constants.pickpal_commission
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessGetPickPalComision!(comision: (response.value as! NSDictionary).value(forKey: "commission") as! Double)
                        break
                    case 400:
                        self.delegate?.didFailGetPickPalComision!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetPickPalComision!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetPickPalComision!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetPickPalComision!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func setReadNewsFeedLog(client_id: Int, last_id_news_feed: Int) {
        let parameters =  [
            "client_id": client_id,
            "last_id_news_feed": last_id_news_feed
        ]
        let request = Constants.baseURL + Constants.users + Constants.set_read_news_feed_log
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessSetNewsFeedLog!()
                        break
                    case 400:
                        self.delegate?.didFailSetNewsFeedLog!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailSetNewsFeedLog!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailSetNewsFeedLog!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailSetNewsFeedLog!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSetNewsFeedLog!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func getUnreadNewsFeedLog(client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.get_unread_news_feed_log + "\(client_id)/"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessGetNewsFeedLog!(newsFeed: (response.value as! NSDictionary).value(forKey: "news_feed") as! Int)
                        break
                    case 400:
                        self.delegate?.didFailGetNewsFeedLog!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetNewsFeedLog!(title: "El usuario no existe", subtitle: "")
                        break
                    case 500:
                        self.delegate?.didFailGetNewsFeedLog!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetNewsFeedLog!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetNewsFeedLog!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    func getUrlInvoice(order_id: Int) {
        let parameters =  [
            "order_id": order_id
        ]
        let request = Constants.baseURL + Constants.orders + Constants.get_url_invoice
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                //                print(response)
                switch response.result{
                case .success:
                    //                    debugPrint(response)
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        if let url = (response.value as! NSDictionary).value(forKey: "url") as? String {
                            if let message = (response.value as! NSDictionary).value(forKey: "message") as? String {
                                self.delegate?.didSuccessGetUrlInvoice!(url: url, message: message)
                            }else{
                                self.delegate?.didSuccessGetUrlInvoice!(url: "", message: "Tu factura aun no esta disponible")
                            }
                        }else{
                            self.delegate?.didSuccessGetUrlInvoice!(url: "", message: "Tu factura aun no esta disponible")
                        }
                        
                        break
                    case 400:
                        self.delegate?.didFailGetUrlInvoice!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetUrlInvoice!(title: "Error", subtitle: "El usuario no existe")
                        break
                    case 500:
                        self.delegate?.didFailGetUrlInvoice!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetUrlInvoice!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func registerHelp(client_id: Int, descriptionText: String, category: Int, order: Int, file:String = "", codigo:String=""){
        //        let parameters = [
        //            "client":client_id,
        //            "description": descriptionText,
        //            "category": category,
        //            "order": order,
        //            "file": file,
        //            ] as [String: Any]
        let request = "\(Constants.baseURL)\(Constants.orders)\(Constants.register_help)"
        let fileUrl = URL(fileURLWithPath: file)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(fileUrl, withName: "file")
            multipartFormData.append("\(client_id)".data(using: .utf8, allowLossyConversion: false)!, withName: "client")
            multipartFormData.append(descriptionText.data(using: .utf8, allowLossyConversion: false)!, withName: "description")
            multipartFormData.append("\(category)".data(using: .utf8, allowLossyConversion: false)!, withName: "category")
            multipartFormData.append("\(order)".data(using: .utf8, allowLossyConversion: false)!, withName: "order")
        }, to: request , encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: {progress in
                    //                    UploadingView.changeMessage(newMessage: Int(progress.fractionCompleted * 100).description + " %")
                    if(progress.fractionCompleted == 1.0){
                        //                        UploadingView.changeMessage(newMessage: "¡Hecho!")
                    }
                })
                upload.responseJSON { response in
                    //                    if let avatar = (response.value as? NSDictionary)?.value(forKey: "avatar") as? String{
                    //                        self.delegate?.didSuccessUploadPhoto!(avatar: avatar)
                    self.delegate?.didSuccessRegisterHelp!()
                    //                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
            }
        })
        //        Alamofire.request(, method: .post, parameters: parameters, encoding: JSONEncoding.default)
        //            .responseString(completionHandler: { (response) in
        //                //                print(response)
        //                switch response.result{
        //                case .success:
        //                    switch Int((response.response?.statusCode)!){
        //                    case 200:
        //                        self.delegate?.didSuccessRegisterHelp!()
        //                        break
        //                    case 400:
        //                        self.delegate?.didFailRegisterHelp!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
        //                        break
        //                    case 404:
        //                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
        //                        break
        //                    case 302:
        //                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
        //                        break
        //                    case 500:
        //                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
        //                        break
        //                    default:
        //                        break
        //                    }
        //                    break
        //                case .failure( _):
        //                    self.delegate?.didFailRegisterHelp!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
        //                    break
        //                }
        //            })
    }
    func registerHelp2(client_id: Int, descriptionText: String, category: Int, order: Int, file:String = ""){
        let parameters = [
            "client":client_id,
            "description": descriptionText,
            "category": category,
            "order": order,
            "file": file,
            ] as [String: Any]
        let request = "\(Constants.baseURL)\(Constants.orders)\(Constants.register_help)"
        
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessRegisterHelp!()
                        break
                    case 400:
                        self.delegate?.didFailRegisterHelp!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 302:
                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    case 500:
                        self.delegate?.didFailRegisterHelp!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailRegisterHelp!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    func getInStates() {
        let request = Constants.baseURL + Constants.generals + Constants.get_in_states
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let cities = self.makeStates(info: response.value as! NSArray)
                        self.delegate?.didSuccessGetInStates!(states: cities)
                        break
                    case 400:
                        self.delegate?.didFailGetInStates!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetInStates!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetInStates!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetInStates!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetInStates!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getLastState(state: String, client_id:Int) {
        let parameters = [
            "client_pk":client_id,
            "state": state
            ] as [String: Any]
        let request = Constants.baseURL + Constants.notifications + Constants.register_last_state
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessGetLastState!()
                        break
                    case 400:
                        self.delegate?.didFailGetLastState!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailGetLastState!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailGetLastState!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailGetLastState!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailGetLastState!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func getStatusProfile(client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.status_profile + String(client_id)
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        let availdable_advs = self.makeStatusPerfil(info: response.value as! NSDictionary)
//                            (response.value as! NSDictionary).value(forKey: "available_advs") as! Bool
                        self.delegate?.didSuccessStatusProfile?(avalidableAdvs: availdable_advs)
                        
                        break
                    case 400:
                        self.delegate?.didFailStatusProfile!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailStatusProfile!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailStatusProfile!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailStatusProfile!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailGetLastState!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
    }
    
    func viewMyPoints(client_id: Int) {
        let request = Constants.baseURL + Constants.users + Constants.view_my_points + String(client_id)
        print(request)
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print(response)
                        let viewMyPoints = self.makeViewMyPoints(info: response.value as! NSArray)
                        self.delegate?.didSuccessViewMyPoints?(viewMyPoints: viewMyPoints)
                        
                        break
                    case 400:
                        self.delegate?.didFailViewMyPoints!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailViewMyPoints!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailViewMyPoints!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailViewMyPoints!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailViewMyPoints!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    func deleteAccountUser(client_id:Int) {
        let parameters = [
            "client_id":client_id,
            ] as [String: Any]
        let request = Constants.baseURL + Constants.delete_account
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseData(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSuccessDeleteAccount!()
                        break
                    case 400:
                        self.delegate?.didFailDeleteAccount!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 404:
                        self.delegate?.didFailDeleteAccount!(title: "Error", subtitle: "No podemos procesar tu solicitud.")
                        break
                    case 500:
                        self.delegate?.didFailDeleteAccount!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    default:
                        self.delegate?.didFailDeleteAccount!(title: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    self.delegate?.didFailDeleteAccount!(title: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                    break
                }
            })
        
    }
    
    
    func isUpdateAvailable() {
        let info = Bundle.main.infoDictionary
        let currentVersion = info?["CFBundleShortVersionString"] as? String
        let identifier = info?["CFBundleIdentifier"] as? String
        let request = "http://itunes.apple.com/lookup?bundleId=\(identifier!)"
        //        let request = "http://itunes.apple.com/lookup?bundleId=iw.digital.PickPal"
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON(completionHandler: { (response) in
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        print((response.value as! NSDictionary).value(forKey: "results") as? NSArray)
                        if ((response.value as! NSDictionary).value(forKey: "results") as? NSArray)!.count > 0 {
                            if let version = (((response.value as! NSDictionary).value(forKey: "results") as? NSArray)?.object(at: 0) as? NSDictionary)?.value(forKeyPath: "version") as? String{
                                if currentVersion != version {
                                    self.delegate?.didSuccessIsAppUpdated?(isUpdated: false)
                                }else{
                                    self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                                }
                                print(version)
                            }else{
                                self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                            }
                        }else{
                            self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                        }
                        break
                    case 400:
                        self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                        break
                    case 404:
                        self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                        break
                    case 500:
                        self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                        break
                    default:
                        self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didSuccessIsAppUpdated?(isUpdated: true)
                    break
                }
            })
    }
}

extension NSObject {
    func makeProfile (profile: NSDictionary) -> UserProfile{
        let userProfile = UserProfile()
        //        print(profile)
        if let birthday = profile.value(forKey: "birth_date") as? String {
            userProfile.birthday = birthday
        }
        
        if let availableAdvs = profile.value(forKey: "available_advs") as? Bool{
            userProfile.availableAdvs = Bool(availableAdvs as NSNumber)
        }
        
        if let email = profile.value(forKey: "email") as? String {
            userProfile.email = email
        }
        
        if let name = profile.value(forKey: "full_name") as? String {
            userProfile.name = name
        }
        
        if let zipCode = profile.value(forKey: "zip_code") as? Int {
            userProfile.zipCode = zipCode
        }
        
        if let gender = profile.value(forKey: "gender") as? String {
            userProfile.genre = gender
        }
        if let avatar = profile.value(forKey: "avatar") as? String {
            userProfile.avatar = avatar
        }
        if let phone = profile.value(forKey: "phone") as? String {
            userProfile.phone = phone
        }
        return userProfile
    }
    
    func makeSliders (sliders: NSArray) -> [Sliders]{
        var slds = [Sliders]()
        for slide in sliders {
            let sld = Sliders()
            if let title = (slide as! NSDictionary).value(forKey: "title") as? String {
                sld.title = title
            }
            
            if let descrp = (slide as! NSDictionary).value(forKey: "desription") as? String {
                sld.description_title = descrp
            }
            
            if let image = (slide as! NSDictionary).value(forKey: "image") as? String {
                sld.image = image
            }
            slds.append(sld)
        }
        return slds
    }
    
    
    func makeRegimen(regimenList: NSArray) -> [TaxRegimenDTO]{
        var taskRegimenList = [TaxRegimenDTO]()
        for regimen in regimenList {
            let trl = TaxRegimenDTO()
            
            if let title = (regimen as! NSDictionary).value(forKey: "value") as? String {
                trl.value = title
            }
            
            if let key = (regimen as! NSDictionary).value(forKey: "key") as? Int {
                trl.key = key
            }
            
            taskRegimenList.append(trl)
        }
        return taskRegimenList
    }
    
    func makeNews (info: NSArray) -> [News]{
        var nws = [News]()
        for z in info {
            let new = News()
            if let id = (z as! NSDictionary).value(forKey: "id") as? Int {
                new.id = id
            }
            if let title = (z  as! NSDictionary).value(forKey: "title") as? String {
                new.title = title
            }
            if let descrp = (z  as! NSDictionary).value(forKey: "content") as? String {
                new.content = descrp
            }
            if let image = (z  as! NSDictionary).value(forKey: "image") as? String {
                new.image = image
            }
            if let big_image = (z  as! NSDictionary).value(forKey: "big_image") as? String {
                new.big_image = big_image
            }
            if let category = (z  as! NSDictionary).value(forKey: "category") as? String {
                new.category = category
            }
            nws.append(new)
        }
        return nws
    }
    
    func makeOrders (orders: NSArray) -> [Orders]{
        var slds = [Orders]()
        for ord in orders {
            //            print(ord as! NSDictionary)
            let sld = Orders()
            if let referencia = (ord as! NSDictionary).value(forKey: "folio") as? String {
                sld.referencia = referencia
            }
            if let daily_folio = (ord as! NSDictionary).value(forKey: "daily_folio") as? String {
                sld.daily_folio = daily_folio
            }
            if let payment_method = (ord as! NSDictionary).value(forKey: "payment_method") as? String {
                sld.payment_method = payment_method
            }
            if let in_site = (ord as! NSDictionary).value(forKey: "in_site") as? Bool {
                sld.in_site = in_site
            }
            if let in_bar = (ord as! NSDictionary).value(forKey: "in_bar") as? Bool {
                sld.in_bar = in_bar
            }
            if let is_billed = (ord as! NSDictionary).value(forKey: "is_billed") as? Bool {
                sld.is_billed = is_billed
            }else{
                sld.is_billed = false
            }
            
            if let fecha = (ord as! NSDictionary).value(forKey: "register_datetime") as? String {
                sld.fecha = fecha
            }
            if let comercio = (ord as! NSDictionary).value(forKey: "establishment") as? String {
                sld.comercio = comercio
            }
            if let verificador = (ord as! NSDictionary).value(forKey: "verification_code") as? String {
                sld.verificador = verificador
            }
            if let propina = (ord as! NSDictionary).value(forKey: "pickpal_reward") as? String {
                sld.propina = propina
            }
            if let isScheculed = (ord as! NSDictionary).value(forKey: "is_schedule") as? Bool {
                sld.is_scheduled = isScheculed
            }else{
                sld.is_scheduled = false
            }
            if let num_table = (ord as! NSDictionary).value(forKey: "number_table") as? Int {
                sld.number_table = num_table
            }else{
                sld.number_table = 0
            }
            if let is_delivered = (ord as! NSDictionary).value(forKey: "delivered") as? Bool {
                sld.delivered = is_delivered
            }
            if let refund = (ord as! NSDictionary).value(forKey: "refund") as? Bool {
                sld.refund = refund
            }
            
            
            if let is_refund = (ord as! NSDictionary).value(forKey: "is_refund") as? Bool {
                sld.is_refund = is_refund
            }
            
            if let expired_refund = (ord as! NSDictionary).value(forKey: "expired_refund") as? Bool {
                sld.expired_refund = expired_refund
            }
            
            
            if let subtotal = (ord as! NSDictionary).value(forKey: "subtotal") as? String {
                sld.subtotal = subtotal
            }
            
            if let puntos = (ord as! NSDictionary).value(forKey: "points") as? Double {
                sld.points = puntos
            }
            
            if let cash_money = (ord as! NSDictionary).value(forKey: "pickpal_cash_money") as? String {
                sld.pickpal_cash_money = Double (cash_money)
            }
            
            if let puntosmoney = (ord as! NSDictionary).value(forKey: "money_points") as? String {
                sld.moneyPoints = puntosmoney
            }else{
                sld.moneyPoints = "0"
            }
            
            if let allowPoints = (ord as! NSDictionary).value(forKey: "allow_points_movile") as? Bool {
                sld.allow_points_movile = allowPoints
            }
            
            if let new_points = (ord as! NSDictionary).value(forKey: "new_points") as? Double {
                sld.new_points = new_points
            }
            
            if let new_points_money = (ord as! NSDictionary).value(forKey: "new_points_money") as? Double {
                sld.new_points_money = new_points_money
            }
            
            
            if let total_card = (ord as! NSDictionary).value(forKey: "total_card") as? Double {
                sld.total_card = total_card
            }
            if let total_cash = (ord as! NSDictionary).value(forKey: "total_cash") as? Double {
                sld.total_cash = total_cash
            }
            if let total_to_pay = (ord as! NSDictionary).value(forKey: "total_to_pay") as? Double {
                sld.total_to_pay = total_to_pay
            }
            if let total_done_payment = (ord as! NSDictionary).value(forKey: "total_done_payment") as? Double {
                sld.total_done_payment = total_done_payment
            }
            
            
            if let total = (ord as! NSDictionary).value(forKey: "total") as? String {
                if let pickpal_service = (ord as! NSDictionary).value(forKey: "pickpal_service") as? String {
                    sld.total = "\(Double(total)! + Double(pickpal_service)!)"
                }
            }
            if let pickpal_service = (ord as! NSDictionary).value(forKey: "pickpal_service") as? String {
                sld.pickpal_service = pickpal_service
            }
            if let status_display = (ord as! NSDictionary).value(forKey: "get_status_display") as? String {
                sld.status_display = status_display
            }
            if let img = (ord as! NSDictionary).value(forKey: "establishment_image") as? String {
                sld.img = img
            }
            
            
            if let refund_date = (ord as! NSDictionary).value(forKey: "refund_date") as? String {
                sld.refundDate = refund_date
            }
            
            if let category = (ord as! NSDictionary).value(forKey: "establishment_category") as? String {
                sld.category = category
            }
            if let place = (ord as! NSDictionary).value(forKey: "establishment_place") as? String {
                sld.place = place
            }
            if let rating = (ord as! NSDictionary).value(forKey: "establishment_rating") as? Int {
                sld.rating = rating
            }
            if let stablishment_id = (ord as! NSDictionary).value(forKey: "establishment_id") as? Int {
                sld.stablishment_id = stablishment_id
            }
            if let order_id = (ord as! NSDictionary).value(forKey: "order_id") as? Int {
                sld.order_id = order_id
            }
            if let card = (ord as! NSDictionary).value(forKey: "last4") as? String {
                sld.card = card
            }else{
                sld.card = nil
            }
            if let factura_status = (ord as! NSDictionary).value(forKey: "status_invoice") as? String {
                sld.status_invoice = factura_status
            }
            
            if let delivery_address = (ord as! NSDictionary).value(forKey: "delivery_address") as? String {
                sld.delivery_address = delivery_address
            }
            
            if let shipping_cost = (ord as! NSDictionary).value(forKey: "shipping_cost") as? String {
                sld.shippingCost = Double(shipping_cost)
            }
            
            if let confirmed = (ord as! NSDictionary).value(forKey: "confirmed") as? Bool {
                sld.confirmed = confirmed
            }
            
            
            
            
            if let type_order = (ord as! NSDictionary).value(forKey: "type_order") as? String {
                sld.type_order = type_order
            }
            
            if let folio_order = (ord as! NSDictionary).value(forKey: "folio") as? String {
                sld.folio_order = folio_order
            }
            
            var itemsOrders = [ItemOrders]()
            if let orderInf = ((ord as! NSDictionary).value(forKey: "order_description") as? NSArray) {
                sld.order_info = orderInf
            }
            for item in ((ord as! NSDictionary).value(forKey: "order_description") as? NSArray)!{
                let i = ItemOrders()
                i.cuantiti = (item as! NSDictionary).value(forKey: "quantity") as? Int
                i.name = (item as! NSDictionary).value(forKey: "menu_item_name") as? String
                i.total = (item as! NSDictionary).value(forKey: "price") as? String
                itemsOrders.append(i)
            }
            sld.itemsOrders = itemsOrders
            slds.append(sld)
        }
        return slds
    }
    
    func makeStates(info: NSArray) -> [String]{
        var cities = [String]()
        for state in info {
            cities.append((state as! NSDictionary).value(forKey: "name") as! String)
        }
        return cities
    }
    
    func makeViewMyPoints (info: NSArray) -> [ViewMyPoints]{
        var nws = [ViewMyPoints]()
        var details = [ExchangeDetails]()
        for z in info {
            let new = ViewMyPoints()
            
            if let id = (z as! NSDictionary).value(forKey: "id") as? Int {
                new.id = id
            }
            
            if let concepInt = (z as! NSDictionary).value(forKey: "concept_int") as? Int {
                new.concep_int = concepInt
            }
            
            if let cAt = (z as! NSDictionary).value(forKey: "created_at") as? String {
                new.created_at = cAt
            }
            
            if let conp = (z as! NSDictionary).value(forKey: "concept") as? String {
                new.concep = conp
            }
            
            if let eName = (z as! NSDictionary).value(forKey: "establishment_name") as? String {
                new.establishment_name = eName
            }
            
            if let name_from = (z as! NSDictionary).value(forKey: "name_from") as? String {
                new.name_from = name_from
            }
            
            if let phone_client = (z as! NSDictionary).value(forKey: "phone_client") as? String {
                new.phone_client = phone_client
            }
            
            if let to = (z as! NSDictionary).value(forKey: "transfer_to") as? String {
                new.transfer_to = to
            }
            
            if let from = (z as! NSDictionary).value(forKey: "transfer_from") as? String {
                new.transfer_from = from
            }
            
            if let amount = (z as! NSDictionary).value(forKey: "amount") as? Double {
                new.amount = amount
            }
            
            if let nextB = (z as! NSDictionary).value(forKey: "next_balance") as? Double {
                new.next_balance = nextB
            }
            
            if let total = (z as! NSDictionary).value(forKey: "total") as? Double {
                new.total = total
            }
            
            if let establishment_from = (z as! NSDictionary).value(forKey: "establishment_name") as? String {
                new.establishment_from = establishment_from
            }
            
            if let moneyAmount = (z as! NSDictionary).value(forKey: "amount_money") as? Double {
                new.money_amount = moneyAmount
            }
            
            if let moneyNextBalance = (z as! NSDictionary).value(forKey: "money_next_balance") as? Double {
                new.money_next_balance = moneyNextBalance
            }
            
            if let tax = (z as! NSDictionary).value(forKey: "tax") as? Double {
                new.tax = tax
            }
            
            if let tax_money = (z as! NSDictionary).value(forKey: "tax_money") as? Double {
                new.tax_money = tax_money
            }
            
            if let exchange_to = (z as! NSDictionary).value(forKey: "exchange_to") as? [String] {
                new.exchange_to = exchange_to
            }
            
            if let exchange_to_address_array = (z as! NSDictionary).value(forKey: "exchange_to_address_array") as? [String] {
                new.exchange_to_address_array = exchange_to_address_array
            }
            
            
            if let address = (z as! NSDictionary).value(forKey: "address") as? String {
                new.address = address
            }
            
            
            if let exchange_from = (z as! NSDictionary).value(forKey: "exchange_from") as? String {
                new.exchange_from = exchange_from
            }
            
            if let status_label = (z as! NSDictionary).value(forKey: "status_label") as? String {
                new.status_label = status_label
            }
            
            if let time_at = (z as! NSDictionary).value(forKey: "time_at") as? String {
                new.time_at = time_at
            }
            
            if let reference = (z as! NSDictionary).value(forKey: "reference") as? String {
                new.reference = reference
            }
            
            if let str_tax = (z as! NSDictionary).value(forKey: "str_tax") as? String {
                new.str_tax = str_tax
            }
            
            if let total_money = (z as! NSDictionary).value(forKey: "total_money") as? Double {
                new.total_money = total_money
            }
            
            
            if let left_amount = (z as! NSDictionary).value(forKey: "left_amount") as? Double {
                new.left_amount = left_amount
            }
            
            if let left_amount_money = (z as! NSDictionary).value(forKey: "left_amount_money") as? Double {
                new.left_amount_money = left_amount_money
            }
            

            if let is_cancelled = (z as! NSDictionary).value(forKey: "is_cancelled") as? Bool {
                new.is_cancelled = is_cancelled
            }
            if let modifiers = (z as! NSDictionary).value(forKey: "exchange_details") as? NSArray {
                var modi = [ExchangeDetails]()
                for modifi in modifiers {
                    let mod = ExchangeDetails()
                    mod.id = (modifi as! NSDictionary).value(forKey: "id") as? Int
                    mod.amount = ((modifi as! NSDictionary).value(forKey: "amount") as! Int)
                    mod.establishmentidFrom = ((modifi as! NSDictionary).value(forKey: "establishment_from") as! String)
                    mod.establishmentidTo = ((modifi as! NSDictionary).value(forKey: "establishment_to") as! String)
                    mod.isCancel = ((modifi as! NSDictionary).value(forKey: "is_cancelled") as! Bool)
                    modi.append(mod)
                }
                new.exchange_details = modi
            }
             
            nws.append(new)
        }
        return nws
    }
    
    
    
    func makeStatusPerfil(info: NSDictionary) -> StatusProfile{
        
      
        
        var statusProfile = StatusProfile()
        
        statusProfile.availableAdvs =  info.value(forKey: "available_advs") as! Bool
        statusProfile.totalMoney = info.value(forKey: "total_points") as! Double
        statusProfile.totalPoints = info.value(forKey: "total_money") as! Double
        statusProfile.walletAcceptedTerms = info.value(forKey: "wallet_accepted_terms") as! Bool
        statusProfile.walletTermnsConditions =  info.value(forKey: "wallet_termns_conditions") as! String
        
        
        return statusProfile
    }
    
}
