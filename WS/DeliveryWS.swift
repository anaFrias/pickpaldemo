//
//  DeliveryWS.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 08/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import Foundation
import Alamofire

@objc protocol deliveryDelegate: class {
    
    @objc optional func getAddressUser(addressCatalog: [Address])
    @objc optional func failGetAddress(error: String, subtitle: String)
    
    @objc optional func saveAddressUser(addressId: Int)
    @objc optional func failSaveAddress(error: String, subtitle: String)
    
    
    @objc optional func getShoppingCost(shoppingCost: Double, timeDelivery: Int)
    @objc optional func failShoppingCost(error: String, subtitle: String)
    
    
    @objc optional func updateAddressUser()
    @objc optional func failUpdateddress(error: String, subtitle: String)
    
    
    @objc optional func deleteAddressUser()
    @objc optional func failDeleteAddress(error: String, subtitle: String)
    
}

class DeliveryWS: NSObject {
    
    var delegate: deliveryDelegate!
    
    func getAddresList(client_id: Int){
     
        let request = Constants.baseURL + Constants.users + Constants.view_address_delivery + "\(client_id)/"
        
       // let request = Constants.baseURL + Constants.users + Constants.view_address_delivery + "126/"
          print("Lista de direcciones request", request)
        
        Alamofire.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default)
                .responseJSON(completionHandler: { (response) in
  
                    switch response.result{
                    case .success:
                        switch Int((response.response?.statusCode)!){
                        case 200:
                            print("Response ", response.value!)
                            
                             let arrayAddress = self.makeListAddress(info: (response.value! as! NSDictionary).value(forKey: "delivery_address") as! NSArray)
                            
                             print("Convertido ",arrayAddress)
                            
                           self.delegate.getAddressUser!(addressCatalog: arrayAddress)
                           
                            break
                        case 204:
                            self.delegate.failGetAddress!(error: "No hay direcciones guardadas", subtitle: "Aún no haz registrado ninguna dirección")
                            break
                        case 400:
                            self.delegate.failGetAddress!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                            break
                        case 500:
                            self.delegate.failGetAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                            break
                        default:
                            break
                        }
                        break
                    case .failure(let error):
                        print(error)
                        self.delegate.failGetAddress!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                        break
                    }
                })
        }
    

    func setAddres(address: Address,client_id: Int){
            let parameters = [
                "client_id": client_id,
                    "address_delivery_id":0,
                    "street": address.street ?? "",
                    "colony": address.colony ?? "",
                    "municipality": address.city ?? "",
                    "interior_number":address.numeroInterior ?? "",
                    "outdoor_number":address.numeroExterior ?? "",
                    "state":address.state ?? "",
                    "zip_code":address.zip_code ?? "",
                    "latitud": String( address.latitude ?? 0.0),
                    "longitud":String(address.longitude ?? 0.0),
                    "reference_location":address.referencia ?? "",
                    "street_one":address.calleReferencia1 ?? "",
                    "street_two":address.calleReferencia2 ?? ""
            ] as [String : Any]
        print("parameters ", parameters)
        let request = Constants.baseURL+Constants.users+Constants.new_or_update_address_delivery
        print("Guardar Direccion", request)
    
        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseString(completionHandler: { (response) in
            
   // Alamofire.request(request, method: .post, parameters: nil, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
           
            
      switch response.result{
        case .success:
            switch Int((response.response?.statusCode)!){
                case 200:
                     print("Direccion response", response)
                  //  print("Direccion Guardada",(response.value as! JSONEncoder))
                     let jsonText = response.value

                     var dictonary:NSDictionary?
                            
                     if let data = jsonText!.data(using: String.Encoding.utf8) {
                                
                                do {
                                    dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject] as NSDictionary?
                                
                                    if let myDictionary = dictonary {
                                        print("id Address: \(myDictionary["address_delivery_id"]!)")
                                        
                                        let idAddress = myDictionary["address_delivery_id"]!
                                        
                                        self.delegate.saveAddressUser!(addressId: idAddress as! Int)
                                         
                                    }
                                } catch let error as NSError {
                                    print(error)
                                }
                            }
                break
            case 204:
                    print("204")
                break
            case 400:
                    print("400")
                    break
            case 500:
                    print("500")
                break
            default:
                    print("ora si error")
                break
            }
        break
      case .failure(let error):
        print(error)
        break
  }
        })
        }
    
    func updateAddress(address: Address, client_id: Int) {
        
        let parameters = [
                      "client_id": client_id,
                      "address_delivery_id": address.pk ?? "",
                          "street": address.street ?? "",
                          "colony": address.colony ?? "",
                          "municipality": address.city ?? "",
                          "interior_number":address.numeroInterior ?? "",
                          "outdoor_number":address.numeroExterior ?? "",
                          "state":address.state ?? "",
                          "zip_code":address.zip_code ?? "",
                          "latitud": String( address.latitude ?? 0.0),
                          "longitud":String(address.longitude ?? 0.0),
                          "reference_location":address.referencia ?? "",
                          "street_one":address.calleReferencia1 ?? "",
                          "street_two":address.calleReferencia2 ?? ""
                  ] as [String : Any]
              print("parameters ", parameters)
              let request = Constants.baseURL+Constants.users+Constants.new_or_update_address_delivery
              print( "Direccion Actualizada ", request)
          
              Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseString(completionHandler: { (response) in
                  print("Actualizada Direccion response", request)
                  
            switch response.result{
              case .success:
                  switch Int((response.response?.statusCode)!){
                      case 200:
                          print("Direccion Actualizada")
                          self.delegate.updateAddressUser!()
                      break
                  case 204:
                          print("204")
                          
                          
                      break
                  case 400:
                          print("400")
                          break
                  case 500:
                          print("500")
                      break
                  default:
                          print("ora si error")
                      break
                  }
              break
            case .failure(let error):
              print(error)
              break
        }
              })
        
    }
    
    
    func deleteAddress(address: Address){
        
        let parameters = [ "address_delivery_id": address.pk ?? "" ] as [String : Any]
        
         
        let request = Constants.baseURL+Constants.users+Constants.delete_address_delivery
               
            Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
        
                print("Code deleta Address ",Int((response.response?.statusCode)!))
                
                      switch Int((response.response?.statusCode)!){
                              case 200:
                                 
    
                                  self.delegate.deleteAddressUser!()
                                  
                                
                                 
                                  break
                                
                              case 204:
                                self.delegate.failDeleteAddress!(error: "Error", subtitle: "No se pudo eliminar la dirección seleccionada")
                                  break
                              case 400:
                                self.delegate.failDeleteAddress!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                                  break
                                
                              case 500:
                                  self.delegate.failDeleteAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                                  break
                              default:
                                
                                self.delegate.failDeleteAddress!(error: "Ha ocurrido un error", subtitle: "intentalo de nuevo más tarde.")
                                
                                  break
                              }
                              
                     
                      })
    }
    
    
    func getDeliveryCost(addressDelivery: String,establishment_id: Int){
       
              let parameters = [
                "id_address_delivery": addressDelivery,
                "establishment_id":establishment_id,
                      
                  ] as [String : Any]
              print("parameters Costo ", parameters)
              let request = Constants.baseURL+Constants.establishment+Constants.get_price_delivery
           

        Alamofire.request(request, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
                
            //print("Response ", response.value!)
                      switch response.result{
                      case .success:
                          switch Int((response.response?.statusCode)!){
                          case 200:
                              print("Response ", response.value!)
    
                              let shopping_cost = (response.value! as! NSDictionary).value(forKey: "shipping_cost")! as! Double
                              let time_delivery = (response.value! as! NSDictionary).value(forKey: "time_delivery")! as! Int
                                  
                              
                              print("Costo de envio ", shopping_cost)
                              self.delegate.getShoppingCost?(shoppingCost: shopping_cost, timeDelivery: time_delivery)
                             
                              break
                          case 204:
                            self.delegate.failShoppingCost!(error: "No pudimos obtener información", subtitle: "Parece que se pudo obtener el costo de envío")
                              break
                          case 400:
                              self.delegate.failShoppingCost!(error: "Error", subtitle: "No podemos procesar tu solicitud.")
                              break
                            
                          case 406:
                            
                             let mesagge = (response.value! as! NSDictionary).value(forKey: "message")! as! String
                            
                            self.delegate.failShoppingCost!(error: "No hay cobertura en tu zona", subtitle: mesagge)
                            break
                          case 500:
                              self.delegate.failShoppingCost!(error: "Ha ocurrido un error", subtitle: "Intentalo de nuevo más tarde.")
                              break
                          default:
                              break
                          }
                          break
                      case .failure(let error):
                          print(error)
                          self.delegate.failShoppingCost!(error: "Error de comunicación", subtitle: "Asegúrate de estar conectado a internet.")
                          break
                      }
                  })
          }
    
    
    
    
}
    

extension NSObject {
    
    func makeListAddress (info: NSArray) -> [Address] {
        
         var listAddress = [Address]()
       
        for list in info {
            
             let address = Address()
            address.pk = (list as! NSDictionary).value(forKey: "pk")! as! Int
            address.street = (list as! NSDictionary).value(forKey: "street")! as! String
            address.colony = (list as! NSDictionary).value(forKey: "colony")! as! String
            
            address.city = (list as! NSDictionary).value(forKey: "municipality")! as! String
            address.numeroInterior = (list as! NSDictionary).value(forKey: "n_int")! as! String
            address.numeroExterior = (list as! NSDictionary).value(forKey: "n_ext")! as! String
            address.state = (list as! NSDictionary).value(forKey: "state")! as! String
            //address.pais = (list as! NSDictionary).value(forKey: "country") as! String
            address.zip_code = (list as! NSDictionary).value(forKey: "zip_code")! as! String
            address.referencia = (list as! NSDictionary).value(forKey: "reference_location")! as! String
            address.latitude = (list as! NSDictionary).value(forKey: "lat")! as! Double
            address.longitude = (list as! NSDictionary).value(forKey: "lng")! as! Double
            address.calleReferencia1 = (list as! NSDictionary).value(forKey: "reference_street_one") as? String ?? ""
             address.calleReferencia2 = (list as! NSDictionary).value(forKey: "reference_street_two") as? String ?? ""
            print("Info ", address.street)
            
            listAddress.append(address)
        }
        
        return listAddress
    }
    
    
     func makeIdAddress (info: NSArray) -> Int {
        var idAddres = Int()
        
        for list in info {
            
         idAddres   = (list as! NSDictionary).value(forKey: "pk")! as! Int
        }
        
        return idAddres
    }
    
}
