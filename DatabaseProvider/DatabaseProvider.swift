//
//  DatabaseProvider.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 04/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

public class DatabaseProvider {
    class var sharedInstance: DatabaseSQL {
        struct Static {
            static let instance: DatabaseSQL = DatabaseSQL()
        }
        return Static.instance
    }
}
