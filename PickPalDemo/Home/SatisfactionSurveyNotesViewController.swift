//
//  SatisfactionSurveyNotesViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SatisfactionSurveyNotesViewController: UIViewController, establishmentDelegate {

    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var note: UITextView!
    
    var establishmentWS = EstablishmentWS()
    var questionsArray = [Question]()
    var establishment_id = Int()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var order_id = Int()
    var position = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        establishmentWS.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didSuccessSetEvaluationNote() {
        let xib = SurveyThanksViewController()
        xib.order_id = order_id
        xib.modalTransitionStyle = .crossDissolve
        xib.modalPresentationStyle = .overCurrentContext
        self.note.text = ""
        self.present(xib, animated: true, completion: nil)
    }
    
    func didFailSetEvaluationNote(error: String, subtitle: String) {
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            newVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        let alert = UIAlertController(title: "PickPal", message: "No podemos comunicarnos con PickPal", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func omitir(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
               
               newVC.modalPresentationStyle = .fullScreen
               let navController = UINavigationController(rootViewController: newVC)
               navController.modalTransitionStyle = .crossDissolve
               navController.modalPresentationStyle = .fullScreen
               //                    navController.modalPresentationStyle = .custom
               self.present(navController, animated: true, completion: nil)
    }
    //1401053712
    @IBAction func continuar(_ sender: Any) {
        establishmentWS.SetEvaluationNote(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[2].id, note: note.text, order_id: order_id)
                
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
