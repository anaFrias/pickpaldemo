//
//  SingleEstablishmentHelpViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import OneSignal

class SingleEstablishmentHelpViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var nombreEstablecimiento = String()
    var category = String()
    var hourEntregado = String()
    var totalPedido = String()
    var imagenEstablecimiento = String()
    var myorder = Orders()
    var isFromPromos:Bool = false
    
    var questions = ["Mi pedido nunca me lo entregaron", "Mi pedido tiene productos faltantes o incorrectos", "No se aplicó el código promocional", "Tuve un problema con la factura de mi pedido", "Tengo un problema con el pago de un pedido", "Tuve un problema diferente con mi pedido"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
}

extension SingleEstablishmentHelpViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentInfoCell") as! cellEstablishmentTableViewCell
            cell.selectionStyle = .none
            cell.nombreEst.text = nombreEstablecimiento
            cell.nombreEstablecimiento.text = nombreEstablecimiento
            cell.category.text = category
            cell.total.text = totalPedido
            cell.dateLabel.text = hourEntregado
            if imagenEstablecimiento != "" {
                Nuke.loadImage(with: URL(string: imagenEstablecimiento)!, into: cell.imageEstablishment)
            }
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleFAQ")!
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell") as! FAQuestionsTableViewCell
            cell.questionLabel.text = questions[indexPath.row - 2]
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 1 {
            if indexPath.row == 2{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "NoDelivered") as! NoDeliveredViewController
                newVC.nombreComercio = nombreEstablecimiento
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 3 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "faltantePedido") as! FaltantesAyudaViewController
                newVC.orderId = myorder.order_id
                newVC.numberOfProducts = myorder.itemsOrders
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 4 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "promotionalCodeView") as! PromotionalCodeHelpViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 5 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "troubleFactura") as! TroubleFacturaHelpViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 19919919 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "refundTrouble") as! RefundTroubleHelpViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 6 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "payTrouble") as! PayTroubleHelpViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else if indexPath.row == 7 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "otherProblems") as! OtherTroubleViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "otherProblems") as! OtherTroubleViewController
                newVC.orderId = myorder.order_id
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }
        
    }
}
