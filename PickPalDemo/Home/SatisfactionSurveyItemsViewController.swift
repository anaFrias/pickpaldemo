//
//  SatisfactionSurveyItemsViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class SatisfactionSurveyItemsViewController: UIViewController, establishmentDelegate, changeLikeStatus {
    
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var question: UILabel!
    
    var questionsArray = [Question]()
    var establishment_id = Int()
    var imge = String()
    var establishmentWS = EstablishmentWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    //    let order_id = UserDefaults.standard.object(forKey: "order_id") as! Int
    var question_id = Int()
    var order_id = Int()
    
    var rate = 0
    var notes = false
    var items = [String]()
    var flags = [Bool]()
    var position = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        establishmentWS.delegate = self
        img.layer.cornerRadius = 57.5
        img.clipsToBounds = true
        img.layer.masksToBounds = true
        question.text = questionsArray[position].question
        if imge != "" {
            Nuke.loadImage(with: URL(string: imge)!, into: img)
        }
        
        question_id = questionsArray[position].id
        for _ in items{
            flags.append(true);
        }
    }
    func changeLike(position: Int, status: Bool) {
        if flags[position] != status{
            flags[position] = status
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func didSuccessSetEvaluation() {
        LoadingOverlay.shared.hideOverlayView()
        UserDefaults.standard.removeObject(forKey: "survey")
        //        if notes{
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyNotes") as! SatisfactionSurveyNotesViewController
        newVC.questionsArray = questionsArray
        newVC.establishment_id = establishment_id
        newVC.order_id = order_id
        newVC.position = position + 1
        self.navigationController?.pushViewController(newVC, animated: true)
        
        //        }else{
        //            let xib = SurveyThanksViewController()
        //            xib.modalTransitionStyle = .crossDissolve
        //            xib.modalPresentationStyle = .overCurrentContext
        //            xib.order_id = order_id
        //            self.present(xib, animated: true, completion: nil)
        //        }
        
    }
    
    func didFailSetEvaluation(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        let alert = UIAlertController(title: "PickPal", message: "No podemos comunicarnos con PickPal", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func `continue`(_ sender: Any) {
        var i = 0
        var array =  [Dictionary<String,Any>]()
        for e in items{
            let dictionary = ["name": e, "is_like": flags[i]] as [String : Any]
            array.append(dictionary)
            i += 1
        }
        LoadingOverlay.shared.showOverlay(view: self.view)
        establishmentWS.SetEvaluationItems(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[position].id, data: array, order_id: order_id)
    }
    
    @IBAction func omitir(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyNotes") as! SatisfactionSurveyNotesViewController
        newVC.questionsArray = questionsArray
        newVC.establishment_id = establishment_id
        newVC.order_id = order_id
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension SatisfactionSurveyItemsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "evaluation") as! EvaluationTableViewCell
        cell.txt.text = items[indexPath.row]
        cell.position = indexPath.row
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

