//
//  EditProfileViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import Firebase

class EditProfileViewController: UIViewController, setBirthdayDelegate, getDate, getImageDelegate {

    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var mailText: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblMailUser: UILabel!
    
    var date = String()
    var image = UIImage()
    var userws = UserWS()
    var imageString = String()
    var full_name = String()
    var f_name = String()
    var l_name = String()
    var phone = String()
    var mail = String()
    var maiLocale = String()
    var gender = String()
    var zipCode = String()
    var succeedInfo = false
    var formattedPhoneNumber = String()
    var localePhone = String()
    var wrongPhone = CGFloat(0)
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var defaults = UserDefaults.standard
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.viewProfile(client_id: client_id)
        // Do any additional setup after loading the view.
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
    }

    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setBirthday(){
        let datePicker = DatePickerViewController(nibName: "DatePickerViewController", bundle: nil)
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.modalTransitionStyle = .coverVertical
        datePicker.delegate = self
        self.present(datePicker, animated: true, completion: nil)

    }
    func didSuccessViewProfile(profile: UserProfile) {
        LoadingOverlay.shared.hideOverlayView()
        date = profile.birthday!
        imageString = profile.avatar!
        full_name = profile.name
        phone = profile.phone!
        mail = profile.email!
        maiLocale = profile.email!
        localePhone = profile.phone!
        gender = profile.genre!
        if let zp  = profile.zipCode {
            zipCode = "\(profile.zipCode!)"
        }
        localePhone = profile.phone!
        tableView.reloadData()
        UserDefaults.standard.set(imageString, forKey: "avatar")
    }
    func didFailViewProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if title != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    func sendDate(date: String, age: Int) {
        self.date = date
        UserDefaults.standard.set(age, forKey: "age")
        tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
    }
    func didSuccessValidateEmail() {
        
    }
    func didFailValidateEmail(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let indexpath = IndexPath(row: 1, section: 0)
        let cell = tableView.cellForRow(at: indexpath) as! UserInfoTableViewCell
        cell.wrong_icon.alpha = 1
        cell.mail_wrong_text.alpha = 1
        cell.mail_wrong_text.text = "Correo ya existente"
    }
    func getImage() {
        self.showActionSheet()
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func sendInfo(first_name: String, last_name: String, mail: String, phone: String, gender: String, birthday: String, zipCode: String) {
        if first_name == "", last_name == "", mail == "", phone == "", gender == "", birthday == "" {
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else{

            self.mail = mail
            self.phone = phone
            self.f_name = first_name
            self.l_name = last_name
            self.gender = gender
            self.date = birthday
            self.zipCode = zipCode
            userws.updateProfile(client_id: client_id, first_name: first_name, last_name: last_name, gender: gender, birth_date: birthday, zip_code: zipCode, email: mail, is_register: 2, phone: phone)

        }
       
    }
    func validatePhone(correctPhone: Bool, phone: String, change: Bool) {
        if correctPhone, change {
            succeedInfo = true
        }else{
            succeedInfo = false
        }
    }
    func validateEmail(succededInfo: Bool, change: Bool) {
        if succededInfo, change {
            succeedInfo = true
        }else{
            succeedInfo = false
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}
extension EditProfileViewController: userDelegate {

    func didSuccessUploadPhoto(avatar: String) {
        UploadingView.hide()
        imageString = avatar
        UserDefaults.standard.set(imageString, forKey: "avatar")
        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        print(avatar)
    }
    func didFailUploadPhoto(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessUpdateProfile() {
        LoadingOverlay.shared.hideOverlayView()
        if succeedInfo {
            Database.database().reference().removeAllObservers()
            UserDefaults.standard.removeObject(forKey: "first_name")
            UserDefaults.standard.removeObject(forKey: "client_id")
            UserDefaults.standard.removeObject(forKey: "is_register")
            UserDefaults.standard.removeObject(forKey: "phone")
            UserDefaults.standard.removeObject(forKey: "complete_info")
            UserDefaults.standard.removeObject(forKey: "login")
            UserDefaults.standard.removeObject(forKey: "phone_verificaton")
            UserDefaults.standard.removeObject(forKey: "screen")
            UserDefaults.standard.removeObject(forKey: "storyboard")
            UserDefaults.standard.removeObject(forKey: "avatar")
            UserDefaults.standard.removeObject(forKey: "age")
            let db = try! Connection("\(path)/orders.db")
            try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
            print(self.navigationController?.viewControllers)
            let alert = UIAlertController(title: "PickPal", message: "Por tu seguridad cerraremos tu sesión para que accedas con tus nuevos datos", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: { (action) in
                for view in (self.navigationController?.viewControllers)! {
                    if view.isKind(of: ViewController.self) {
                        self.navigationController?.popToRootViewController(animated: true)
                    }else{
                        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                        let mvc = storyBoard.instantiateInitialViewController()
                        mvc?.modalTransitionStyle = .crossDissolve
                        self.present(mvc!, animated: true, completion: {
                        })
                    }
                }
            }))
            self.present(alert, animated: true, completion: {
            })
            
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailUpdateProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
}
//MARK: Get image from camera roll & Photo library
extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cámara", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Carrete", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openGallery()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let picker = UIImagePickerController()
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }else {
            self.openGallery()
        }
    }
    
    func openGallery() {
        var pickerGallery = UIImagePickerController()
        pickerGallery.delegate = self
        pickerGallery.sourceType = .photoLibrary
        pickerGallery.allowsEditing = true
        self.present(pickerGallery, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        LoadingOverlay.shared.showOverlayWithMessage(view: self.view, message: "")
        let uuid = UUID().uuidString
        let myImageName = "imagen" + uuid + ".png"
        let imagePath = fileInDocumentsDirectory(myImageName)
        _ = saveImage((info[UIImagePickerControllerOriginalImage] as? UIImage)!, path: imagePath)
        if let client_id = UserDefaults.standard.object(forKey: "client_id") as? Int {
            userws.updateAvatar(imageFilePath: "\(path)/" + myImageName, client_id: "\(client_id)")
        }
        print("\(path)/" + myImageName)
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
        
    }
    
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "image", for: indexPath) as! ImagePhotoTableViewCell
            cell.delegate = self
            if imageString != "" {
                Nuke.loadImage(with: URL(string: imageString)!, into: cell.imageUser)
//                Nuke.loadImage(with: URL(string: "https://pickpal-assets-demos.s3.us-east-2.amazonaws.com/media/public/users/avatars/WALLPAPER+MOBILE+(1).png")!, into: cell.imageUser)
//                cell.buttonImage.alpha = 0
                cell.imageUser.alpha = 1
            }else{
                cell.buttonImage.alpha = 1
                cell.imageUser.alpha = 0
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "info", for: indexPath) as! UserInfoTableViewCell
            cell.delegate = self
            if full_name != "" {
                cell.FullName.text = full_name
                cell.FullName.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            }else{
                cell.FullName.placeholder = "Nombre Completo"
            }
            
            if phone != "" {
                formattedPhoneNumber = Extensions.format(phoneNumber: phone)!
                cell.phoneNumber.text = formattedPhoneNumber
                cell.phone = phone
                cell.phoneLocal = localePhone
                cell.phoneNumber.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            }else{
                cell.phoneNumber.placeholder = "Teléfono"
            }
            
            if mail != "" {
                cell.email.text = mail
                cell.emailLocal = mail
                cell.email.visibility = .gone
                cell.lblUserEmail.text = mail
                cell.email.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
                cell.email.isEnabled = false
            }else{
                cell.email.placeholder = "Correo Electrónica"
            }
            
            if zipCode != "" {
                cell.zipCodeLabel.text = zipCode
                cell.zipCodeLabel.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            }else{
                cell.zipCodeLabel.placeholder = "Código Postal"
            }
            
            if gender != "" {
                if gender == "F" {
                    cell.gender = "F"
                    cell.genre.text = "Femenino"
                }else{
                    cell.gender = "M"
                    cell.genre.text = "Masculino"
                }
                cell.genre.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            }else{
                cell.genre.text = "Sexo"
                cell.genre.textColor = UIColor.init(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)
            }
            
            if date != "" {
                cell.birthday.text = date
                cell.birthday.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            }else{
                cell.birthday.text = "Fecha de Nacimiento"
                cell.birthday.textColor = UIColor.init(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 115
        }else{
            return 410
        }
    }
}
extension EditProfileViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text?.count)!>0{
            if let formatedphone = Extensions.format(phoneNumber: textField.text!) {
                formattedPhoneNumber = formatedphone
            }else{
                formattedPhoneNumber = textField.text!
            }
            if phone != formattedPhoneNumber {
                wrongPhone = 1
            }
            phone = textField.text!
            tableView.reloadData()
        }else{
            wrongPhone = 1
            tableView.reloadData()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10 // Bool
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
}
