//
//  ProfileAddAddressViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import iOSDropDown

class ProfileAddAddressViewController: UIViewController, paymentDelegate, userDelegate {
    @IBOutlet weak var Rfc: UITextField!
    
    @IBOutlet weak var nombre_completo: UITextField!
    
    @IBOutlet weak var Email: UITextField!
//    unused
    @IBOutlet weak var NoExterior: UITextField!
    @IBOutlet weak var NoInterior: UITextField!
    @IBOutlet weak var Colonia: UITextField!
    @IBOutlet weak var EntidadFederativa: UITextField!
    @IBOutlet weak var Zip: UITextField!
    @IBOutlet weak var Calle: UITextField!
    
    @IBOutlet weak var RFCButton: UIButton!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    @IBOutlet weak var indicatorRFC: UIImageView!
    @IBOutlet weak var indicatorRazon: UIImageView!
    @IBOutlet weak var indicatorMail: UIImageView!
    
    @IBOutlet weak var RFCwrongText: UILabel!
    @IBOutlet weak var MailWrongText: UILabel!
    
    @IBOutlet weak var numPedidos: UIView!
    
    //factura 4.0
    //Código Postal
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var indicatorZipCode: UIImageView!
    @IBOutlet weak var zipCodeButton: UIButton!
    
    //CFDI
    @IBOutlet weak var cfdiIndicator: UIImageView!
    @IBOutlet weak var cfdiFieldDropDown: DropDown!
    @IBOutlet weak var cfdiButton: UIButton!
    
    var paymentWS = PaymentWS()
    var usersWs = UserWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var isUpdate = false
    var addressId = 0
    var billing_address_id = 0
    var billingAddresses = [BillingAddress]()
    var info = BillingAddress()
    var isFromPromos:Bool = false
    var taxRegimenId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentWS.delegate = self
        usersWs.delegate = self
        Rfc.delegate = self
        Email.delegate = self
        nombre_completo.delegate = self
        zipCodeField.delegate = self
        cfdiFieldDropDown.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        if(isUpdate){
            if info.id! == 0 {
                billing_address_id = info.id
            }else{
                Rfc.text = info.RFC
                nombre_completo.text = info.full_name
                Email.text = info.email_send
                billing_address_id = info.id
            }
            
            
            self.zipCodeField.text = info.zip_code
//            paymentWS.getSingleAddress(address_id: addressId, client_id: client_id)
//            LoadingOverlay.shared.showOverlay(view: self.view)
        }
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
         Rfc.addTarget(self, action: #selector(uppercaseRFC(_:)), for: .editingChanged)
        
        requestTaskRegimen()
    }
    
    func findTaxRegime(taxRegime: Int, regimenList: [TaxRegimenDTO]) -> String{
        var tr = ""
        for data in regimenList{
            if taxRegime == data.key{
                tr = data.value
                break
            }
        }
        
        return tr
    }
    
    func requestTaskRegimen(){
        LoadingOverlay.shared.showOverlay(view: self.view)
        usersWs.getTaxRegimenList()
    }
    
    func didSuccessGetTaskRegimen(regimenList: [TaxRegimenDTO]) {
        LoadingOverlay.shared.hideOverlayView()
        
        if isUpdate{
            self.taxRegimenId = info.tax_regime ?? 0
            self.cfdiFieldDropDown.text = findTaxRegime(taxRegime: info.tax_regime ?? 0, regimenList: regimenList)
        }
        var taxNames = [String]()
        for data in regimenList{
            taxNames.append(data.value)
        }
        cfdiFieldDropDown.optionArray = taxNames
        
        cfdiFieldDropDown.didSelect{(selectedText , index ,id) in
            self.taxRegimenId = regimenList[index].key
        }
    }
    
    func didFailGetTaskRegimen(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    @IBAction func responderField(_ sender: UIButton) {
        if sender == RFCButton {
            Rfc.becomeFirstResponder()
        }else if sender == nameButton {
            nombre_completo.becomeFirstResponder()
        }else if sender == emailButton{
            Email.becomeFirstResponder()
        }else if sender == cfdiButton{
            cfdiFieldDropDown.becomeFirstResponder()
        }else{
            zipCodeField.becomeFirstResponder()
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == Rfc {
            indicatorRFC.alpha = 0
        }else if textField == nombre_completo {
            indicatorRazon.alpha = 0
        }else if textField == Email{
            indicatorMail.alpha = 0
        }else if textField == zipCodeField{
            indicatorZipCode.alpha = 0
        }else{
            cfdiIndicator.alpha = 0
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == Rfc {
            if textField.text != "", textField != nil {
                indicatorRFC.alpha = 1
                if isValidRFC(testStr: textField.text!) {
                    indicatorRFC.image = UIImage(named: "correctIcon")
                    RFCwrongText.alpha = 0
                }else{
                    indicatorRFC.image = UIImage(named: "redClose")
                    RFCwrongText.alpha = 1
                }
            }else{
                indicatorRFC.alpha = 1
                indicatorRFC.image = UIImage(named: "redClose")
            }
            
        }else if textField == nombre_completo {
            if textField.text == "" {
                indicatorRazon.alpha = 1
                indicatorRazon.image = UIImage(named: "redClose")
            }
            
        }else if textField == zipCodeField{
            if textField.text == "" {
                indicatorZipCode.alpha = 1
                indicatorZipCode.image = UIImage(named: "redClose")
            }
        }else if textField == cfdiFieldDropDown{
            if textField.text != "" {
                cfdiIndicator.alpha = 0
            }else{
                indicatorRFC.alpha = 1
                indicatorRFC.image = UIImage(named: "redClose")
            }
        }else{
            if textField.text == "" {
                indicatorMail.alpha = 1
                indicatorMail.image = UIImage(named: "redClose")
               
            }else{
                if !isValidEmail(testStr: textField.text) {
                    indicatorMail.alpha = 1
                    MailWrongText.alpha = 1
                    indicatorMail.image = UIImage(named: "redClose")
                }else{
                    indicatorMail.alpha = 1
                    MailWrongText.alpha = 0
                    indicatorMail.image = UIImage(named: "correctIcon")
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func SetBillingAddress(_ sender: Any) {
        if Rfc.text != "", Rfc != nil {
            indicatorRFC.alpha = 1
            if isValidRFC(testStr: Rfc.text!) {
                indicatorRFC.image = UIImage(named: "correctIcon")
            }else{
                indicatorRFC.image = UIImage(named: "redClose")
            }
        }else{
            indicatorRFC.alpha = 1
            indicatorRFC.image = UIImage(named: "redClose")
        }
        if nombre_completo.text == "" {
            indicatorRazon.alpha = 1
            indicatorRazon.image = UIImage(named: "redClose")
        }
        if Email.text == "" {
            indicatorMail.alpha = 1
            indicatorMail.image = UIImage(named: "redClose")
            
        }else{
            if !isValidEmail(testStr: Email.text) {
                indicatorMail.alpha = 1
                indicatorMail.image = UIImage(named: "redClose")
            }else{
                indicatorMail.alpha = 1
                indicatorMail.image = UIImage(named: "correctIcon")
            }
        }
        
        if zipCodeField.text == ""{
            indicatorZipCode.alpha = 1
            indicatorZipCode.image = UIImage(named: "redClose")
        }
        
        if cfdiFieldDropDown.text == ""{
            cfdiIndicator.alpha = 1
            cfdiIndicator.image = UIImage(named: "redClose")
        }
        
        if(Rfc.text != "" && nombre_completo.text != "" && Email.text != "" && zipCodeField.text != "" && self.taxRegimenId != 0){
            if !checkAddress(addresses: billingAddresses, currentRFC: Rfc.text!.uppercased()) || info.full_name != nombre_completo.text! {
                if isValidRFC(testStr: Rfc.text!.uppercased()) {
                    if isValidEmail(testStr: Email.text!) {
                        if self.taxRegimenId != 0{
                            if(!isUpdate){
                                paymentWS.setBillingAddress(
                                    client_id: client_id, billing_address_id: 0, RFC: Rfc.text!, nombre_completo: nombre_completo.text!,
                                    email: Email.text!, tax_regime: self.taxRegimenId, zip_Code: zipCodeField.text!)
                            }else{
                                paymentWS.setBillingAddress(
                                    client_id: client_id, billing_address_id: billing_address_id, RFC: Rfc.text!, nombre_completo: nombre_completo.text!,
                                    email: Email.text!, tax_regime: self.taxRegimenId, zip_Code: zipCodeField.text!)
                            }
                        }else{
                            cfdiIndicator.alpha = 1
                        }
                    }else{
                        MailWrongText.alpha = 1
                    }
                    
                }else{
                    RFCwrongText.alpha = 1
                }
            }else{
                RFCwrongText.alpha = 1
                RFCwrongText.text  = "El RFC ya está registrado"
            }
            
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func validateZipCode(input: String?) -> Bool {
        guard let input = input else { return false }

        return NSPredicate(format: "SELF MATCHES %@", "^\\d{5}(?:[-\\s]?\\d{4})?$")
            .evaluate(with: input.uppercased())
    }
    
    func checkAddress(addresses: [BillingAddress], currentRFC: String) -> Bool {
        var flags = [Bool]()
        if addresses.count == 0 {
            flags.append(false)
        }
        for address in addresses {
            if address.RFC == currentRFC {
                if info.email_send != Email.text || info.full_name != nombre_completo.text || info.zip_code != zipCodeField.text || info.tax_regime != self.taxRegimenId{
                    flags.append(false)
                }else{
                    flags.append(true)
                }
            }else{
                flags.append(false)
            }
        }
        if flags.contains(true) {
            return true
        }else{
            return false
        }
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSuccessSetBillingAddress(billing_address_id: Int) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didFailSetBillingAddress(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessGetSingleBillingAddress(info: BillingAddress) {
        LoadingOverlay.shared.hideOverlayView()
//        Rfc.text = info.RFC
//        Calle.text = info.street
//        NoExterior.text = info.outdoor_number
//        NoInterior.text = info.interior_number
//        Colonia.text = info.colony
//        EntidadFederativa.text = info.state
//        Zip.text = info.zip_code
        if info.id! == 0 {
            billing_address_id = info.id
        }else{
            Rfc.text = info.RFC
            nombre_completo.text = info.full_name
            Email.text = info.email_send
            billing_address_id = info.id
        }
    }
    func didFailGetBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func didFailGetSingleBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func isValidRFC(testStr:String) -> Bool {
        guard testStr != nil else { return false }

        let result = matches(for: "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[0-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\\d]{3})?$", in: testStr)
        if result.count > 0 {
            if result[0] == testStr {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
        
    }
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension ProfileAddAddressViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == Rfc {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }else{
            return true
        }
//        let lowercaseCharRange: NSRange = (string as NSString).rangeOfCharacter(from: CharacterSet.lowercaseLetters)
        
//        if Int(lowercaseCharRange.location) != NSNotFound {
//            textField.text = (textField.text as NSString?)?.replacingCharacters(in: range, with: string.uppercased())
//            return false
//        }
        
        
         // Bool
    }
    @objc func uppercaseRFC (_ textField: UITextField) {
        Rfc.text = textField.text?.uppercased()
    }
}
