//
//  AlertInfoRefundDetails.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 03/08/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertInfoRefundDetails: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNumberOrder: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalAproxToRefund: UILabel!
    @IBOutlet weak var lblFailPercent: UILabel!
    @IBOutlet weak var lblSeconTitle: UILabel!
    @IBOutlet weak var lblTotalPoint: UILabel!
    @IBOutlet weak var lblTotalCash: UILabel!
    @IBOutlet weak var lblTotalPaymentMethod: UILabel!
    @IBOutlet weak var lblTotalToRefund: UILabel!
    
    @IBOutlet weak var lblTitleExtra: UILabel!
    @IBOutlet weak var lblTitlePoints: UILabel!
    @IBOutlet weak var lblTitleCash: UILabel!
    
    
    var  refundDetails = RefundDetails()
    var totalReembolso = 0.0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        containerView.clipsToBounds = false
        containerView.layer.masksToBounds = false
        containerView.layer.cornerRadius = 15
        
        
        //TITULOS
        lblTitle.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: "Detalles del Reembolso", ziseFont: 19.0, colorText: 0)
        lblSeconTitle.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: "Detalles del Reembolso", ziseFont: 17.0, colorText: 0)
        
        //VALORES
        lblNumberOrder.attributedText = CustomsFuncs.getAttributedString(title1: "Orden #\(refundDetails.order_number!)", title2: "", ziseFont: 19.0, colorText: 0)
        lblSubTotal.text = CustomsFuncs.getFomatMoney(currentMoney: refundDetails.subtotal)
       
        
        lblTotal.text = CustomsFuncs.getFomatMoney(currentMoney: refundDetails.total)
        
        lblFailPercent.text = "\(refundDetails.fail_percent!)%"
        
        lblTotalAproxToRefund.text = CustomsFuncs.getFomatMoney(currentMoney: refundDetails.total_aprox_to_refund)
        
        //Detalles del Reembolso
        if refundDetails.refund_points != nil && refundDetails.refund_points > 0{
            lblTotalPoint.text = "\(refundDetails.refund_points!)pts/ \(CustomsFuncs.getFomatMoney(currentMoney: refundDetails.refund_points_money))"
            
            totalReembolso += refundDetails.refund_points_money!
            
        }else{
            
            lblTotalPoint.visibility = .gone
            lblTitlePoints.visibility = .gone
        }
        
        if refundDetails.refund_pickpal_cash != nil && refundDetails.refund_pickpal_cash > 0{
            lblTotalCash.text = CustomsFuncs.getFomatMoney(currentMoney: refundDetails.refund_pickpal_cash)
            
            totalReembolso += refundDetails.refund_pickpal_cash
        }else{
            
            lblTotalCash.visibility = .gone
            lblTitleCash.visibility = .gone
        }
       
        if refundDetails.refund_extra != nil && refundDetails.refund_extra > 0{
            
            lblTitleExtra.text = "Adicional (\(refundDetails.refund_extra_method!)):"
            
            lblTotalPaymentMethod.text = CustomsFuncs.getFomatMoney(currentMoney: refundDetails.refund_extra)
            
            totalReembolso += refundDetails.refund_extra
            
        }else{
            
            lblTotalPaymentMethod.visibility = .gone
            lblTitleExtra.visibility = .gone
            
        }
        
        lblTotalToRefund.text = CustomsFuncs.getFomatMoney(currentMoney: totalReembolso)
        
        


    }
    @IBAction func btnCloseAlertinfo(_ sender: UIButton) {
         
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
