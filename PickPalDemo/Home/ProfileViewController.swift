//
//  ProfileViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class ProfileViewController: UIViewController, userDelegate {
    
    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var tableView: UITableView!
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var user = UserProfile()
    var userws = UserWS()
    var first_time = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.viewProfile(client_id: client_id)
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidosBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didSuccessViewProfile(profile: UserProfile) {
        LoadingOverlay.shared.hideOverlayView()
        user = profile
        tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        if !first_time{
            userws.viewProfile(client_id: client_id)
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didFailViewProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if title != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    
    @IBAction func edit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
        if let name = user.name {
            newVC.full_name = user.name
        }
        
        if user.phone != nil && user.phone != ""{
            newVC.phone = user.phone!
        }
        if let email = user.email{
            newVC.mail = email
        }
        
        if let gender = user.genre {
            newVC.gender = gender
        }
        
        if user.zipCode != nil{
             newVC.zipCode = "\(user.zipCode!)"
        }else{
             newVC.zipCode = ""
        }
        if let birthday = user.birthday {
            newVC.date = birthday
        }
        
        first_time = false
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImage") as! ProfileImageTableViewCell
            if user.avatar != nil && user.avatar != ""{
                Nuke.loadImage(with: URL(string: user.avatar!)!, into: cell.img)
                cell.img.layer.cornerRadius = 70
            }
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDivisor")!
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileItem") as! ProfileItemTableViewCell
            var fullName = String()
            var fullNameArr = [String]()
            var firstName = String()
            var lastName = String()
            if user.name != nil && user.name != ""{
                fullName = user.name
                fullNameArr = fullName.components(separatedBy: " ")
                firstName = fullNameArr[0]
                lastName = (fullNameArr.count > 1 ? fullNameArr[1] : nil)!
            }
            cell.item1Title.text = "NOMBRE"
            cell.item1Txt.text = firstName
            cell.item2Title.text = "APELLIDOS"
            cell.item2Txt.text = lastName
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePhone") as! ProfilePhoneTableViewCell
            if user.phone != nil {
                let formattedPhoneNumber = Extensions.format(phoneNumber: user.phone!)
                cell.txt.text = formattedPhoneNumber!
            }
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileItem") as! ProfileItemTableViewCell
            cell.item1Title.text = "CORREO  ELECTRONICO"
            cell.item1Txt.text = user.email
            cell.item2Title.text = "CODIGO POSTAL"
            if user.zipCode != nil{
                cell.item2Txt.text = "\(user.zipCode ?? 1)"
            }else{
                cell.item2Txt.text = ""
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileItem") as! ProfileItemTableViewCell
            cell.item1Title.text = "SEXO"
            if user.genre == "M"{
                cell.item1Txt.text = "Masculino"
            }else if user.genre == "F"{
                cell.item1Txt.text = "Femenino"
            }else{
                cell.item1Txt.text = user.genre
            }
            cell.item2Title.text = "FECHA DE NACIMIENTO"
            cell.item2Txt.text = user.birthday
            cell.divisor.alpha = 0
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 180
        }else if indexPath.row == 1 {
            return 26
        }else if indexPath.row == 3 {
            return 60
        }else{
            return 70
        }
    }
}
