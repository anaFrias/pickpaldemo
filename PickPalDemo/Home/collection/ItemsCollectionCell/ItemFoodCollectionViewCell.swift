//
//  ItemFoodCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ItemFoodCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var currentIndicator: UIView!
}
