//
//  TypeServicesCollectionViewCell.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 24/08/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TypeServicesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconType: UIImageView!
}
