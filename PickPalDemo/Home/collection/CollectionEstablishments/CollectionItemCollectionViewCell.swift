//
//  CollectionItemCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class CollectionItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var nearbyLabel: UILabel!
    @IBOutlet weak var kitchenName: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
}
