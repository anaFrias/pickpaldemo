//
//  SliderItemCollectionViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 02/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SliderItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var fadedImage: UIImageView!
    @IBOutlet weak var bluredImage: UIImageView!
    
}
