//
//  NearbyPlacesCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 29/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class NearbyPlacesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imagePlace: UIImageView!
    
}
