//
//  CollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 07/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol openModalDelegate {
    func openModal(place: Bool, category: Bool, city: Bool, bussinesLine: Bool, type: Bool)
}

class CollectionViewCell: UICollectionViewCell {
    var delegate: openModalDelegate!
    var place: Bool!
    var category: Bool!
    var city: Bool!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBAction func selector(_ sender: Any) {
        self.delegate.openModal(place: place, category: category, city: city, bussinesLine: false, type: false)
    }
}
