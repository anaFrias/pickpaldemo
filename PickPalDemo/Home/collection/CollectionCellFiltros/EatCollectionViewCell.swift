//
//  EatCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 08/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol openSingleDelegate {
    func openSingle(municipio: String, categoria: String, lugar: String)
}
class EatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSearchEat: UILabel!
    
    var delegate: openSingleDelegate!
    var municipio: String!
    var categoria: String!
    var lugar: String!
    
    @IBAction func selector(_ sender: Any) {
        self.delegate.openSingle(municipio: municipio, categoria: categoria, lugar: lugar)
    }
}
