//
//  SatisfactionSurvey1ViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class SatisfactionSurvey1ViewController: UIViewController, establishmentDelegate {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star5: UIButton!
    
    var questionsArray = [Question]()
    var establishment_id = Int()
    var imge = String()
    var establishmentWS = EstablishmentWS()
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    //let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as! Int
    var order_id = Int()
    var question_id = Int()
    var rate = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        establishmentWS.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        //establishmentWS.GenerateEvaluation(client_id: client_id, order_id: 73, establishment_id: 3)
        establishmentWS.GetEvaluation(order_id: order_id)
        img.layer.cornerRadius = 57.5
        img.clipsToBounds = true
        img.layer.masksToBounds = true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSuccessSetEvaluation() {
        LoadingOverlay.shared.hideOverlayView()
        if rate <= 3 {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "newSurveys") as! NewSurveysViewController
            newVC.questionsArray = questionsArray
            newVC.establishment_id = establishment_id
            newVC.order_id = order_id
            newVC.image = imge
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
            newVC.questionsArray = questionsArray
            newVC.establishment_id = establishment_id
            newVC.imge = imge
            newVC.order_id = order_id
            if let questions = questionsArray[2].items{
                newVC.items = questions
                newVC.position = 2
            }else if let products = questionsArray[1].items{
                newVC.items = products
                newVC.position = 1
            }
            
            if rate <= 3{
                newVC.notes = true
            }
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    
    func didFailSetEvaluation(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
//        print(error, subtitle)
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }
        let alert = UIAlertController(title: "PickPal", message: "No podemos comunicarnos con PickPal", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func didSuccessGetEvaluation(questions: Questions) {
        LoadingOverlay.shared.hideOverlayView()
        question.text = questions.questions[0].question
        if questions.img != "" {
            Nuke.loadImage(with: URL(string: questions.img)!, into: img)
        }
        question_id = questions.questions[0].id
        questionsArray = questions.questions
        imge = questions.img
        establishment_id = questions.establishment_id
    }
    
    func didFailGetEvaluation(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }
        let alert = UIAlertController(title: "PickPal", message: "No podemos comunicarnos con PickPal", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func paintStars(){
        switch rate
        {
        case 0:
            star1.setImage(UIImage(named: "starUnselected"), for: .normal)
            star2.setImage(UIImage(named: "starUnselected"), for: .normal)
            star3.setImage(UIImage(named: "starUnselected"), for: .normal)
            star4.setImage(UIImage(named: "starUnselected"), for: .normal)
            star5.setImage(UIImage(named: "starUnselected"), for: .normal)
        case 1:
            star1.setImage(UIImage(named: "starSelected"), for: .normal)
            star2.setImage(UIImage(named: "starUnselected"), for: .normal)
            star3.setImage(UIImage(named: "starUnselected"), for: .normal)
            star4.setImage(UIImage(named: "starUnselected"), for: .normal)
            star5.setImage(UIImage(named: "starUnselected"), for: .normal)
            
        case 2:
            star1.setImage(UIImage(named: "starSelected"), for: .normal)
            star2.setImage(UIImage(named: "starSelected"), for: .normal)
            star3.setImage(UIImage(named: "starUnselected"), for: .normal)
            star4.setImage(UIImage(named: "starUnselected"), for: .normal)
            star5.setImage(UIImage(named: "starUnselected"), for: .normal)
            
        case 3:
            star1.setImage(UIImage(named: "starSelected"), for: .normal)
            star2.setImage(UIImage(named: "starSelected"), for: .normal)
            star3.setImage(UIImage(named: "starSelected"), for: .normal)
            star4.setImage(UIImage(named: "starUnselected"), for: .normal)
            star5.setImage(UIImage(named: "starUnselected"), for: .normal)
            
        case 4:
            star1.setImage(UIImage(named: "starSelected"), for: .normal)
            star2.setImage(UIImage(named: "starSelected"), for: .normal)
            star3.setImage(UIImage(named: "starSelected"), for: .normal)
            star4.setImage(UIImage(named: "starSelected"), for: .normal)
            star5.setImage(UIImage(named: "starUnselected"), for: .normal)
            
        case 5:
            star1.setImage(UIImage(named: "starSelected"), for: .normal)
            star2.setImage(UIImage(named: "starSelected"), for: .normal)
            star3.setImage(UIImage(named: "starSelected"), for: .normal)
            star4.setImage(UIImage(named: "starSelected"), for: .normal)
            star5.setImage(UIImage(named: "starSelected"), for: .normal)
            
        default:
            return
        }
    }
    
    @IBAction func clickStar5(_ sender: Any) {
        rate = 5
        paintStars()
    }
    
    @IBAction func clickStar4(_ sender: Any) {
        rate = 4
        paintStars()
    }
    
    @IBAction func clickStar3(_ sender: Any) {
        rate = 3
        paintStars()
    }
    
    @IBAction func clickStar2(_ sender: Any) {
        rate = 2
        paintStars()
    }
    
    @IBAction func clickStar1(_ sender: Any) {
        rate = 1
        paintStars()
    }
    
    @IBAction func `continue`(_ sender: Any) {
        if rate != 0{
            LoadingOverlay.shared.showOverlay(view: self.view)
            guard questionsArray.count > 0 else {
                establishmentWS.SetEvaluation(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: 1, answer: rate, order_id: self.order_id)
                return
            }
            establishmentWS.SetEvaluation(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[0].id, answer: rate, order_id: self.order_id)
            
        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.items = questionsArray[1].items
//            newVC.notes = true
//            newVC.establishment_id = establishment_id
//            newVC.imge = imge
//            self.navigationController?.pushViewController(newVC, animated: true)
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = "Campo faltante"
            newXIB.subTitleMessageString = "Para continuar porfavor rellena todos los campos solicitados. Gracias"
            present(newXIB, animated: true, completion: nil)
        }
    }
    @IBAction func omitir(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
        newVC.questionsArray = questionsArray
        newVC.items = questionsArray[1].items
        newVC.notes = true
        newVC.order_id = order_id
        newVC.establishment_id = establishment_id
        newVC.imge = imge
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}
