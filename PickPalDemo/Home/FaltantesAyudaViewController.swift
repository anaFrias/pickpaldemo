//
//  FaltantesAyudaViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/21/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class FaltantesAyudaViewController: UIViewController, FaltantesProtocol, userDelegate, missingProductDelegate, closeModalDelegate {

    @IBOutlet weak var tableView: UITableView!
    var numberOfProducts = [ItemOrders]()
    var stuff = [String]()
    var strings = ["Falta parte del producto", "Otra cosa"]
    var results = [String]()
    var orderId = Int()
    var infoSend = String()
    var userws = UserWS()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        stuff.append("faltantes")
        for _ in 0..<numberOfProducts.count {
            stuff.append("items")
        }
        stuff.append("itemsHard")
        stuff.append("itemsHard")
        stuff.append("sendInfo")
        // Do any additional setup after loading the view.
    }
    func selectRow(indexPath: IndexPath) {
        tableView(tableView, didSelectRowAt: indexPath)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    func sendText(text: String) {
        if infoSend != "" {
            let stringRepresentation = infoSend + " Comentario: " + text
            let clientId = UserDefaults.standard.object(forKey: "client_id") as! Int
            userws.registerHelp2(client_id: clientId, descriptionText: stringRepresentation, category: 4, order: orderId)
        }else{
            let alert = UIAlertController(title: "Campo faltante", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func didSuccessRegisterHelp() {
        let newXIB = SuccedMessageHelpViewController(nibName: "SuccedMessageHelpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true) {
            
        }
    }
    func didFailRegisterHelp(title: String, subtitle: String) {
        let error = ErrorViewController()
        error.modalTransitionStyle = .crossDissolve
        error.modalPresentationStyle = .overCurrentContext
        error.errorMessageString = title
        error.subTitleMessageString = subtitle
        self.present(error, animated: true, completion: nil)
    }
}
extension FaltantesAyudaViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + (numberOfProducts.count + 2)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if stuff[indexPath.row] == "faltantes"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "celFaltantes", for: indexPath) as! helpMissedHeadTableViewCell
            cell.selectionStyle = .none
            if let name = UserDefaults.standard.object(forKey: "first_name") as? String {
                cell.nameUser.text = name
            }
            return cell
        }else if stuff[indexPath.row] == "items"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProductosFaltantes", for: indexPath) as! MissingProductTableViewCell
            cell.indexPath = indexPath
            cell.delegate = self
            cell.selectionStyle = .none
            cell.productTitle.text = numberOfProducts[indexPath.row - 1].name
            return cell
            
        }else if stuff[indexPath.row] == "itemsHard"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProductosFaltantes", for: indexPath) as! MissingProductTableViewCell
            cell.selectionStyle = .none
            cell.indexPath = indexPath
            cell.delegate = self
            cell.productTitle.text = strings[indexPath.row - (numberOfProducts.count + 1)]
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "detallesFaltantes", for: indexPath) as! sendInfoMissingTableViewCell
            cell.selectionStyle = .none
//            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if stuff[indexPath.row] == "items" || stuff[indexPath.row] == "itemsHard" {
            let currentCell = tableView.cellForRow(at: indexPath) as! MissingProductTableViewCell
            if currentCell.selectedButton.isSelected {
                currentCell.selectedButton.isSelected = false
                let index = results.index(of: currentCell.productTitle.text!) as! Int
                results.remove(at: index)
            }else{
              currentCell.selectedButton.isSelected = true
                results.append(currentCell.productTitle.text!)
                
            }
            infoSend = results.joined(separator: ", ")
            print(infoSend)
        }
//        if stuff[indexPath.row] == "itemsHard" {
//            let currentCell = tableView.cellForRow(at: indexPath)
//        }
    }
}
