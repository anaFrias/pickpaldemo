

//
//  HomeViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 02/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//
import UIKit
import CoreLocation
import GooglePlaces
import UserNotifications
import Firebase
import OneSignal
import Nuke
import RevealingSplashView
import SQLite

var showListo = true

class HomeViewController: UIViewController, setValuesDelegate, establishmentDelegate/*, cellActionsDelegate*/, CLLocationManagerDelegate, googleWSDelegate, ordersDelegate, categoriesSelection, userDelegate, selectedSearchValue, hurryActionsDelegate, nearbyActionsDelegate, showCurrentOrder, reorderDelegate, UNUserNotificationCenterDelegate, NewFiltersCriteriasDelegate {
    
    var ws = EstablishmentWS()
    var gws = GoogleServices()
    var orderws = OrdersWS()
    var userws = UserWS()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewMyOrder: UIView!
    @IBOutlet weak var viewNewsFeed: UIView!
    @IBOutlet weak var newsFeedTotal: UILabel!
    @IBOutlet weak var viewCarrito: UIView!
    @IBOutlet weak var carritoTotal: UILabel!
    @IBOutlet weak var carritoCuantity: UILabel!
    @IBOutlet weak var imgPickPalToolbar: UIImageView!
    
    //    numero de pedidos
    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var pedidoLabel: UILabel!
    
    
    @IBOutlet weak var newsFeedButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var viewNoCities: UIView!
    
    @IBOutlet weak var collectionVIew: UICollectionView!
    var totalPrice = 0.00
    var cantidad = Int()
    var valueInfo = String()
    var valuePlace = String()
    var valueCat = String()
    var valueState = String()
    var ids = [Int]()
    var valuesItems = [KitchensComplete]()
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    var homePush = true
    var neews = false
    //    para la facturación
    var factura = false
    var orderIdFactura = 0
    //    ----------
    var ref: DatabaseReference!
    var refSearch: DatabaseReference!
    var orderRef: DatabaseReference!
    var operationRef: DatabaseReference!
    var refSearchQry: DatabaseQuery!
    var handle: DatabaseHandle!
    //    arrays with locations
    var cities = [String]()
    var municipalties = [String]()
    var direction = String()
    let center = UNUserNotificationCenter.current()
    
    //    Location variables
    var locationManager = CLLocationManager()
    var myCurrentLocation:CLLocationCoordinate2D!
    var latitudes = [Double]()
    var longitudes = [Double]()
    
    var placesClient: GMSPlacesClient!
    var placeString = String()
    var places = [Places]()
    var status = String()
    var defaults = UserDefaults.standard
    var goOrderReady = Bool()
    var kitchenIds = [[Int]]()
    //    breadcrum values
    var disctrit = [String]()
    var categories = [String]()
    var locations = [String]()
    var totalEstablishments = [Establishment]()
    var totalNewsFeed = [News]()
    var structure = [String]()
    var index = [Int]()
    var noCloseEstablishments = Bool()
    var showSplash = Bool()
    var estadosDisponibles = [String]()
    var establishmentsID = [Int]()
    var openPedidos = Bool()
    var orderIdExpand = Int()
    var valueStateAround = String()
    var valueInfoAround = String()
    var openNotif = [Bool]()
    var stateComplete = String()
    var isUpdatedVersion = Bool()
    
    var isServiceInEst = Bool()
    var isServiceInBar = Bool()
    var isServiceInTable = Bool()
    var isServiceInHome = Bool()
    
    var midBussinessLine = String()
    var midInside = String()
    var mTypeOrderSelect = "Tipo de Pedido"
    var mTypeOrderCode = String()
    var mListTypeOrders = [String]()
    var filtetSelection = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        if showSplash {
            let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "singlelogo")!,iconInitialSize: CGSize(width: 139, height: 135), backgroundColor: UIColor(red:220/255, green:25/255, blue:53/255, alpha:1.0))
            //Adds the revealing splash view as a sub view
            self.view.addSubview(revealingSplashView)
            revealingSplashView.startAnimation(){
                self.orderws.getOrdersQuiz(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            }
        }else{
            self.orderws.getOrdersQuiz(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
        //        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        //        print(appVersion, "app version")
        DatabaseFunctions.shared.createDB()
        OneSignal.setSubscription(true)
        registerDeviceNotifications()
        
        if locationManager.location == nil {
            initlocationManager()
            
        }
        
        if let mun = UserDefaults.standard.object(forKey: "municipalty") as? String {
        //    valueInfo = mun
            valueInfo =   mun.elementsEqual("Santiago de Querétaro") ? "Querétaro" : mun

        }else{
            valueInfo = "Estado"
        }
        
        valuePlace = "Ubicación"
        
        if let mun = UserDefaults.standard.object(forKey: "category") as? String {
            valueCat = mun
        }else{
            valueCat = "Giro de negocio"
        }
        
        if let st = UserDefaults.standard.object(forKey: "state") as? String {
            valueState = st
        }else{
            valueState = ""
        }
        
        ref = Database.database().reference().child("establishments")
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        typeServiceSelecction(itemSelection:"")
        
        viewNewsFeed.layer.cornerRadius = viewNewsFeed.frame.height / 2
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.refresh(_:)), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.factura(_:)), name: NSNotification.Name(rawValue: "factura"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.openPedido(_:)), name: NSNotification.Name(rawValue: "openPedido"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.reorderOrder(_:)), name: NSNotification.Name(rawValue: "reorderOrder"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.openHomePoints(_:)), name: NSNotification.Name(rawValue: "openHomePoints"), object: nil)
        
        if neews{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
        if factura{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "verificarFacturacion") as! VerificarFacturacionViewController
            newVC.orderId = orderIdFactura
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
        if openPedidos{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.orderId = orderIdExpand
            newVC.pedidos = DatabaseFunctions.shared.getPedidos()
            newVC.readyDesplegar = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
        structure = ["grettings", "filter" ,"closer"]
        
//        fatalError()
        
    }
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
//            let db = try! Connection("\(path)/orders.db")
//            try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
//            self.viewCarrito.alpha = 0
            
        }
        DatabaseFunctions.shared.createDB()
        orderws.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
    }
    func didSuccessGetLastOrder(orders: [MultipleOrders]) {
        //        MARK: Sacar los arreglos de pedidos
        LoadingOverlay.shared.hideOverlayView()
        
        //Oscar: Limpar la BD local para actualizarla segun el WS
        DatabaseFunctions.shared.deleteAllItems()
        
        if orders.count > 0 {
        numPedidos.alpha = 1
        pedidoLabel.text = "\(orders.count)"
        }
        
        for (index, order) in orders.enumerated() {
        //    print("Orden Home 0: \(String(describing:  order.order_id))  bandera: \(order.to_refund)  fecha  \(order.register_date)")
            let reference = Database.database().reference().child("orders").child("-" + order.establishment_id!.description).child(order.register_date_key!).child("-" + order.order_id.description)
            print(reference)
            reference.observe(.value, with: { (snapshot) -> Void in
                //                Se agregan variables de status y se redirige a pantalla correspondiente
                if !(snapshot.value is NSNull) {
                    let valueOrder = (snapshot.value as! NSDictionary)
                    //                    print(valueOrder)
                    //                    self.showListo.append(false)
                    let statusInt = valueOrder.value(forKey: "status_client") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusInt2 = valueOrder.value(forKey: "status") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusClient = (Constants.status as NSDictionary).value(forKey: "\(statusInt)") as! String
                    let statusController = (Constants.statusControllers as! NSDictionary).value(forKey: "\(statusInt2)") as! String
                    if let screen = UserDefaults.standard.object(forKey: "screen") as? String, let storyboard = UserDefaults.standard.object(forKey: "storyboard") as? String{
                        if screen != "Home" {
                            if !self.homePush {
                                if screen == "Carrito" {
                                    if self.isUpdatedVersion {
                                        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
                                        let newVC = storyboard.instantiateViewController(withIdentifier: screen)
                                        self.navigationController?.pushViewController(newVC, animated: true)
                                    }else{
                                        self.updateAlert()
                                    }
                                }else{
                                    if statusClient == "in_process"{
                                        if self.isUpdatedVersion {
                                            let storyboard = UIStoryboard(name: storyboard, bundle: nil)
                                            let newVC = storyboard.instantiateViewController(withIdentifier: screen)
                                            self.navigationController?.pushViewController(newVC, animated: true)
                                        }else{
                                            self.updateAlert()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //                Se recupera el detalle del pedido
                    var dictionaryInfoString = String()
                    var products = NSArray()
                    if valueOrder.value(forKey: "products") != nil {
                        products = valueOrder.value(forKey: "products") as! NSArray
                    }
                    
                    var dictionaryInfo = [Dictionary<String,Any>]()
                    var dictionaryModifiersTotal = [Dictionary<String,Any>]()
                    var dictionaryComplementsTotal = [Dictionary<String,Any>]()
                    var i = 0
                    var y = 0
                    var totalPrice = 0.00
                    for product in products {
                        //                        print(product)
                        var modifiers = NSArray()
                        var modifiersS = String()
                        var modifiers_price = NSArray()
                        var modifiers_priceS = String()
                        var modifiers_id = NSArray()
                        var modifiers_idS = String()
                        var modifiers_cat = NSArray()
                        var modifiers_catS = String()
                        
                        var complements = NSArray()
                        var complementsS = String()
                        var complements_price = NSArray()
                        var complement_priceS = String()
                        var complements_id = NSArray()
                        var complements_idS = String()
                        var complements_cat = NSArray()
                        var complements_catS = String()
                        
                        
                        if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            modifiers = mod
                        }else{
                            modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
                        }
                        
                        if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                            modifiers_price = mod_price
                        }else{
                            modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
                        }
                        
                        if let mod_id = (product as! NSDictionary).value(forKey: "modifier_ids") as? NSArray {
                            modifiers_id = mod_id
                        }else{
                            modifiers_idS = (product as! NSDictionary).value(forKey: "modifier_ids") as! String
                        }
                        
                        if let mod_cat = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                            modifiers_cat = mod_cat
                        }else{
                            modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
                        }
                        
                        if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                            complements = comp
                        }else{
                            complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
                        }
                        
                        if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                            complements_price = comp_price
                        }else{
                            complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
                        }
                        
                        if let comp_id = (product as! NSDictionary).value(forKey: "complement_ids") as? NSArray {
                            complements_id = comp_id
                        }else{
                            complements_idS = (product as! NSDictionary).value(forKey: "complement_ids") as! String
                        }
                        
                        if let comp_cat = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                            complements_cat = comp_cat
                        }else{
                            complements_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
                        }
                        
                        if modifiers.count > 0 || complements.count > 0 {
                            var modifP = [Double]()
                            var modifId = [Int]()
                            var modifCat = [String]()
                            var compP = [Double]()
                            var compN = [String]()
                            var compC = [Int]()
                            var compId = [Int]()
                            var compCat = [String]()
                            for j in 0..<modifiers.count {
                                modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                                modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
                                modifCat.append(modifiers_cat.object(at: j) as! String)
                            }
                            var aux = 1
                            var index = -1
                            for z in 0..<complements.count {
                                if !compN.contains(complements[z] as! String) {
                                    compN.append(complements[z] as! String)
                                    compC.append(1)
                                    aux = 1
                                    compP.append(Double(complements_price[z] as! String)!)
                                    compId.append(Int(complements_id[z] as! String) ?? 0)
                                    compCat.append(complements_cat[z] as! String)
                                    index += 1
                                }else{
                                    aux += 1
                                    compC[index] = aux
                                    compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                                    compId[index] = (Int(complements_id[z] as! String) ?? 0)
                                    compCat[index] = (complements_cat[z] as! String)
                                }
                            }
                            
                            let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_id": modifId, "modifier_category":modifCat] as [String : Any]
                            dictionaryModifiersTotal.append(dictionaryModifiers)
                            let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_id": compId, "complement_category":compCat] as [String : Any]
                            dictionaryComplementsTotal.append(dictionaryComplements)
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": true, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                            y += 1
                        }else{
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": false, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_id": complements_idS, "complement_category": complements_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_id": modifiers_idS, "modifier_category": modifiers_catS]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                        }
                        totalPrice += (product as! NSDictionary).value(forKey: "price") as! Double
                        i += 1
                    }
                    var qrCode = String()
                    if let qr = valueOrder.value(forKey: "qr") as? String {
                        qrCode = qr
                    }
                    var last4 = String()
                    if let lastDig = valueOrder.value(forKey: "last4") as? String {
                        last4 = lastDig
                    }else{
                        last4 = ""
                    }
                    var flagReady = Bool()
                    if !DatabaseFunctions.shared.getReadyFlag(order_id: order.order_id) {
                        flagReady = false
                    }else{
                        flagReady = true
                    }
             //        print("Orden 1 Home: \(String(describing:  order.order_id)) \(order.to_refund)  ")
                    let sendNotifications =   DatabaseFunctions.shared.getStatusNotifications(idOrder: order.order_id)
                    DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date:order.register_date, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusClient, qr: qrCode, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusController, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag: flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address, shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String (order.time_delivery), to_refund: order.to_refund, days_elapsed: order.days_elapsed, allow_process_nopayment: order.allow_process_nopayment, est_delivery_hour: order.est_delivery_hour, est_ready_hour: order.est_ready_hour, is_generic: order.is_generic, confirmed:valueOrder.value(forKey: "confirmed") as! Bool, confirm_hour: valueOrder.value(forKey: "confirm_hour") as! String, sendNotifications: sendNotifications, notified: valueOrder.value(forKey: "notified") as? Bool ?? false, pickpal_cash_money:valueOrder.value(forKey: "pickpal_cash_money") as? Double ?? 0, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false, total_card: order.total_card, total_cash: order.total_cash, total_to_pay: order.total_to_pay, total_done_payment: order.total_done_payment,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0)
                                       
                    
                                       
                                       
                    
                    if let isRefunded = valueOrder.value(forKey: "is_refunded") as? Bool {
                        if isRefunded {
                            DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                        }
                    }
                    if statusClient == "expired" {
                        //                        self.numPedidos.alpha = 0
                        
                       
                        DatabaseFunctions.shared.deleteItem()
                        DatabaseFunctions.shared.deleteDeliveredOrders()
                    }else{
                        self.numPedidos.alpha = 1
                        self.defaults.set("Home", forKey: "screen")
                        self.defaults.set("Home", forKey: "storyboard")
                        if statusClient == "delivered" ||  statusClient == "canceled" || statusClient == "expired" || statusClient == "refund"{
                            //                            self.numPedidos.alpha = 0
                            //                            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                            DatabaseFunctions.shared.deleteItem()
                            DatabaseFunctions.shared.deleteDeliveredOrders()
                            //                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                        }
                        if statusClient == "ready"{
                            
                            if DatabaseFunctions.shared.getReadyFlag(order_id: order.order_id) {
                                let readyView = OrdenListaViewController()
                                readyView.modalTransitionStyle = .crossDissolve
                                readyView.modalPresentationStyle = .overCurrentContext
                                if let in_site = valueOrder.value(forKey: "in_site") as? Bool {
                                    if in_site {
                                        readyView.subtitle1 = "Tu pedido de"
                                        readyView.subtitle2 = "será entregado a tu mesa \(valueOrder.value(forKey: "number_table") as! Int)"
                                    }else{
                                        readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                                        readyView.subtitle2 = "y a disfrutar!"
                                    }
                                }else{
                                    readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                                    readyView.subtitle2 = "y a disfrutar!"
                                    
                                    
                                    if order.order_type == "SD"{
                                        readyView.subtitle1 = "¡Tu pedido está en camino!"
                                        readyView.subtitle2 = ""
                                        
                                    }
                                    
                                }
                                readyView.establishName = order.establishment_name
                                readyView.orderId = order.order_id
                                readyView.delegate = self
                                
                               print("Orden 2: \(String(describing: valueOrder.value(forKey: "order_number") as! String)) \(order.to_refund)  ")
                                
                                let sendNotifications =   DatabaseFunctions.shared.getStatusNotifications(idOrder: order.order_id)
                                self.present(readyView, animated: true, completion: nil)
                                DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date:order.register_date, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusClient, qr: qrCode, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusController, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag: flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address, shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String (order.time_delivery), to_refund: order.to_refund, days_elapsed: order.days_elapsed, allow_process_nopayment: order.allow_process_nopayment, est_delivery_hour: order.est_delivery_hour, est_ready_hour: order.est_ready_hour, is_generic: order.is_generic, confirmed:valueOrder.value(forKey: "confirmed") as! Bool, confirm_hour: valueOrder.value(forKey: "confirm_hour") as! String, sendNotifications: sendNotifications, notified: valueOrder.value(forKey: "notified") as? Bool ?? false, pickpal_cash_money:valueOrder.value(forKey: "pickpal_cash_money") as? Double ?? 0, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false, total_card: order.total_card, total_cash: order.total_cash, total_to_pay: order.total_to_pay, total_done_payment: order.total_done_payment ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                                                   
                                                   
                                
                                
                                
                            }
                            //                            if showListo  {
                            //                                let readyView = OrdenListaViewController()
                            //                                readyView.modalTransitionStyle = .crossDissolve
                            //                                readyView.modalPresentationStyle = .overCurrentContext
                            //                                if let in_site = valueOrder.value(forKey: "in_site") as? Bool {
                            //                                    if in_site {
                            //                                        readyView.subtitle1 = "Tu pedido de"
                            //                                        readyView.subtitle2 = "será entregado a tu mesa \(valueOrder.value(forKey: "number_table") as! Int)"
                            //                                    }else{
                            //                                        readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                            //                                        readyView.subtitle2 = "y a disfrutar!"
                            //                                    }
                            //                                }else{
                            //                                    readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                            //                                    readyView.subtitle2 = "y a disfrutar!"
                            //                                }
                            //                                readyView.establishName = order.establishment_name
                            //                                readyView.orderId = order.order_id
                            //                                readyView.delegate = self
                            //                                self.present(readyView, animated: true, completion: nil)
                            //                                showListo = false
                            //                            }
                            //                            else{
                            //
                            //                            }
                            
                            
                            //                            let data = ["establishment_name": order.establishment_name, "order_id": order.order_id, "order_ready": true] as [String : Any]
                            //                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self, userInfo: data)
                        }
                        if statusClient == "to_pay" && (order.order_type == "SM" || order.order_type == "RB") {
                            let registerDay = valueOrder.value(forKey: "register_date") as! String
                            let registerHour = valueOrder.value(forKey: "register_hour") as! String
                            let estimated = registerDay + " " + registerHour
                            //        print(estimated)
                            let form = DateFormatter()
                            form.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //                            form.locale = Locale(identifier: "UTC")
                            if form.date(from: estimated) != nil {
                                let dateRegister = form.date(from: estimated)
                                let currentDay = Date()
                                let timeInterval = currentDay.timeIntervalSince(dateRegister!)
                                //                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                               // self.orderws.retryOrder(order_id: order_id)
                                if (timeInterval > 780) {
                                    self.orderws.changeOrderStatus(order_id: order.order_id, status: 7)
                                   // DatabaseFunctions.shared.deleteDueOrder(order_id: order.order_id)
                                }
                            }
                            
                        }
                        if let isRefunded = valueOrder.value(forKey: "is_refunded") as? Bool {
                            if isRefunded {
                                DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                            }
                            
                        }
                    }
                }
                
            })
        }
    }
    
    @IBAction func goCarrito(_ sender: Any) {
        ws.viewMyPointEstablishment(userId: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmentId: String(DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)))
    }
    
    func didFailGetLastOrder(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if subtitle == "404" {
            DatabaseFunctions.shared.deleteAllItems()
        }
    }
    
    func didSuccessGetOrdersQuiz(items: [OrderQuizItem]) {
        if items.count > 0 {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm" // "a" prints "pm" or "am"
            let hourString = formatter.string(from: Date())
            
            let dayformatter = DateFormatter()
            dayformatter.dateFormat = "yyyy-MM-dd"
            
            let dayString = dayformatter.string(from: Date())
            let inFormatter = DateFormatter()
            inFormatter.dateFormat = "HH:mm"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            var dateOrder = Date()
            //            if let dO = dateFormatter.date(from: dayString + "T" + (items.first?.delivery_at)! + "+0000") {
            if dateFormatter.date(from: dayString + "T" + (items.first?.delivery_at)! + "+0000") != nil {
                dateOrder = dateFormatter.date(from: dayString + "T" + (items.first?.delivery_at)! + "+0000")!
            }
            //            }
            var dateLocal = Date()
            //            if let dL = dateFormatter.date(from: dayString + "T" + hourString + ":00+0000") {
            if dateFormatter.date(from: dayString + "T" + hourString + ":00+0000") != nil {
                dateLocal = dateFormatter.date(from: dayString + "T" + hourString + ":00+0000")!
            }
            
            //            }
            
            //            print(dateLocal)
            let timeInterval = dateLocal.timeIntervalSince(dateOrder)
            //if (timeInterval > 600) {
                let xib = SurveyViewController()
                xib.nombreEstablecimiento = (items.first?.establishment)!
                xib.order_id = (items.first?.id)!
                xib.modalTransitionStyle = .crossDissolve
                xib.modalPresentationStyle = .overCurrentContext
                self.present(xib, animated: true, completion: {
                    //                    self.numPedidos.alpha = 0
                    //                self.viewMyOrder.alpha = 0
                })
                
            //}
        }
    }
    func didFailGetOrdersQuiz(error: String, subtitle: String) {
        print(error)
    }
    @objc func refresh (_ notification: NSNotification) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    @objc func factura (_ notification: NSNotification) {
        //        print((((notification.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as! Int)
        let orderId = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as! Int
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "verificarFacturacion") as! VerificarFacturacionViewController
        newVC.orderId = orderId
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @objc func reorderOrder(_ notification: NSNotification) {
        let reorder = ReOrderViewController()
        reorder.delegate = self
        reorder.modalTransitionStyle = .crossDissolve
        reorder.modalPresentationStyle = .overCurrentContext
        reorder.order_id = (notification.userInfo! as NSDictionary).value(forKey: "order_id") as! Int//(response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
        reorder.establishment_id = (notification.userInfo! as NSDictionary).value(forKey: "establishment_id") as! String
        reorder.register_date = (notification.userInfo! as NSDictionary).value(forKey: "register_date") as! String
        reorder.nombreComercio = (notification.userInfo! as NSDictionary).value(forKey: "establishment_name") as! String
        self.present(reorder, animated: true, completion: nil)
        
    }
    
    @objc func openHomePoints(_ notification: NSNotification) {
        
        
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.pushExchange = (notification.userInfo! as NSDictionary).value(forKey: "dataPusExchange") as! PushExchange
        newVC.isPushExchange = true
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)

//        let ponits = HomePointsViewController()
//        ponits.pushExchange = (notification.userInfo! as NSDictionary).value(forKey: "dataPusExchange") as! PushExchange
//        ponits.isPushExchange = true
//        self.present(ponits, animated: true, completion: nil)
        
        
    }
    
    func reorder(order_id: Int, is_canceled: Bool) {
        if is_canceled {
            //Oscar Cancelar pedido
            self.orderws.changeOrderStatus(order_id:order_id, status: 8)
                    DatabaseFunctions.shared.deleteDueOrder(order_id: order_id)
            UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)"])
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)+"])
            })
        }else{
            
             self.orderws.retryOrder(order_id: order_id)
            
        }
        
    }
    @objc func openPedido (_ notification: NSNotification) {
        
        if let orderId = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int {
            if let status_code = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "status_order ") as? Int {
                if status_code == 5 || status_code == 8 {
                    if let history = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "historial") as? Bool {
                        if history {
                            for view in (self.navigationController?.viewControllers)! {
                                print(view, "VIEW CONTROLLERS")
                            }
                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
                            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                            newVC.currentOrderId = orderId
                            newVC.pedidos = DatabaseFunctions.shared.getPedidos()
                            newVC.readyDesplegar = false
                            newVC.showPedido = false
                            newVC.showHistorial = true
                            self.navigationController?.pushViewController(newVC, animated: true)
                        }
                    }
                }else{
                    if let tableNumber = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "number_table") as? Int {
                        if tableNumber > 0 {
                            let readyView = OrdenListaViewController()
                            readyView.modalTransitionStyle = .crossDissolve
                            readyView.modalPresentationStyle = .overCurrentContext
                            readyView.establishName = DatabaseFunctions.shared.getEstablishmentNameByOrderId(order_id: orderId)
                            readyView.orderId = orderId
                            readyView.isNotification = true
                            readyView.delegate = self
                            readyView.subtitle1 = "Tu pedido de"
                            readyView.subtitle2 = "será entregado a tu mesa \(tableNumber)"
                            self.present(readyView, animated: true, completion: nil)
                            showListo = false
                        }else{
                            let readyView = OrdenListaViewController()
                            readyView.modalTransitionStyle = .crossDissolve
                            readyView.modalPresentationStyle = .overCurrentContext
                            readyView.establishName = DatabaseFunctions.shared.getEstablishmentNameByOrderId(order_id: orderId)
                            readyView.orderId = orderId
                            readyView.isNotification = true
                            readyView.delegate = self
                            readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                            readyView.subtitle2 = "y a disfrutar!"
                            self.present(readyView, animated: true, completion: nil)
                            showListo = false
                        }
                    }else{
                        let readyView = OrdenListaViewController()
                        readyView.modalTransitionStyle = .crossDissolve
                        readyView.modalPresentationStyle = .overCurrentContext
                        readyView.establishName = DatabaseFunctions.shared.getEstablishmentNameByOrderId(order_id: orderId)
                        readyView.orderId = orderId
                        readyView.isNotification = true
                        readyView.delegate = self
                        readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                        readyView.subtitle2 = "y a disfrutar!"
                        self.present(readyView, animated: true, completion: nil)
                        showListo = false
                    }
                    
                }
            }else{
                let readyView = OrdenListaViewController()
                readyView.modalTransitionStyle = .crossDissolve
                readyView.modalPresentationStyle = .overCurrentContext
                readyView.establishName = DatabaseFunctions.shared.getEstablishmentNameByOrderId(order_id: orderId)
                readyView.orderId = orderId
                readyView.isNotification = true
                readyView.delegate = self
                self.present(readyView, animated: true, completion: nil)
                showListo = false
            }
        }
    }
    func didSuccessRetryOrder(order_id: Int) {
       print("Tipo Orden BD Home ", DatabaseFunctions.shared.getOrderIdType(order_id: order_id))
        
         print("Tipo Orden BD Home  id \(DatabaseFunctions.shared.getOrderIdType(order_id: order_id)) tipo \(DatabaseFunctions.shared.getOrderIdType(order_id: order_id)) NopaymentOrder \(DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id)) ")
    if DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay" && DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id) && (DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB") {
        LoadingOverlay.shared.hideOverlayView()
        //        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
                //            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                print("15 minutes has passed")
                print(order_id)
               /* if DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay" && DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id) && (DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB") {*/
                    
                    //Oscar Cancelar orden
                    self.orderws.changeOrderStatus(order_id:order_id, status: 7)
                //}
            })
            
        }
        
        
        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 300, repeats: false)
            //   let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content1 = UNMutableNotificationContent()
        let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content1.title = "¡Ve a pagar tu Pedido a \(establecimiento)!"
        content1.body = "Paga tu pedido mostrando tu código QR, tienes 10 minutos. ¡Corre! haz clic aquí para ver los detalles..."
        //        content1.sound = UNNotificationSound.default()
        content1.sound = UNNotificationSound(named: "pedidolisto.wav")
        content1.userInfo = ["establishment_name": establecimiento, "order_id": order_id, "notification_on": false]
        let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
        center.delegate = self
        center.add(request1, withCompletionHandler: nil)
        
       
        
        let trigge3 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        //                let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content3 = UNMutableNotificationContent()
        let establecimiento1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content3.title = "¡Ve a pagar tu Pedido a \(establecimiento1)!"
        content3.body = "Paga tu pedido mostrando tu código QR, tienes 5 minutos. ¡Corre! haz clic aquí para ver los detalles..."
        content3.sound = UNNotificationSound.default()
        content3.userInfo = ["establishment_name": establecimiento1, "order_id": order_id, "notification_on": false]
        content3.sound = UNNotificationSound(named: "pedidolisto.wav")
        let request3 = UNNotificationRequest(identifier: "\(order_id)-", content: content3, trigger: trigge3)
        center.delegate = self
        center.add(request3, withCompletionHandler: nil)
        
        let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let establishment_id1 = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        let cDate = Date()
        let format = DateFormatter()
        format.dateFormat = "dd_MM_yyyy"
        let currentDate = format.string(from: cDate)
        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
        //        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let content2 = UNMutableNotificationContent()
        content2.title = "Pedido cancelado"
        //        content2.body = "Lo sentimos, el tiempo para pagar tu pedido en \(establecimiento) ha expirado."
        content2.body = "Haz click aquí si deseas ordenar nuevamente TU PEDIDO de \(establecimiento)"
        content2.sound = UNNotificationSound(named: "pedidolisto.wav")
        content2.userInfo = ["establishment_name": establishment_name1, "order_id": order_id, "notification_on": true, "establishment_id": "-\(establishment_id1)", "register_date": currentDate]
        let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
        content2.categoryIdentifier = "caducado"
        center.delegate = self
        center.add(request2, withCompletionHandler: nil)
        
        UNUserNotificationCenter.current().add(request1) { (error) in
            if let error = error {
                print("Se ha producido un error request1: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request2) { (error) in
            if let error = error {
                print("Se ha producido un error request2: \(error)")
            }
        }
        
       /* UNUserNotificationCenter.current().add(request3) { (error) in
            if let error = error {
                print("Se ha producido un error request3: \(error)")
            }*/
        }
        
    
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.showPedido = true
        newVC.currentOrderId = order_id
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func didFailRetryOrder(error: String, subtitle: String) {
        print(error)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        if let notifictionon = response.notification.request.content.userInfo as? NSDictionary {
            if let notif = notifictionon.value(forKey: "notification_on") as? Bool {
                if notif {
                    let reorder = ReOrderViewController()
                    reorder.delegate = self
                    reorder.modalTransitionStyle = .crossDissolve
                    reorder.modalPresentationStyle = .overCurrentContext
                    reorder.order_id = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
                    reorder.establishment_id = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "establishment_id") as! String
                    reorder.register_date = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "register_date") as! String
                    reorder.nombreComercio = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "establishment_name") as! String
                    self.present(reorder, animated: true, completion: nil)
                }else{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                    newVC.readyDesplegar = true
                    newVC.currentOrderId = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
            }else{
                if let isFactura = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "is_factura") as? Bool {
                    if isFactura {
                        let order_id = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "verificarFacturacion") as! VerificarFacturacionViewController
                        newVC.orderId = order_id!
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                    
                }else{
                    if let order_id = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int {
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                        newVC.currentOrderId = order_id
                        newVC.showPedido = true
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                }
                
                
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let order_done = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).1
        let order_elements = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        let encoded = order_elements.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
        
        if !order_done && order_elements != "" {
            viewCarrito.alpha = 1
        }else{
            viewCarrito.alpha = 0
        }
        if let tempElements = dictionary as? [Dictionary<String,Any>] {
            var tempPrice = Double()
            var tempCant = Int()
            //            print(tempElements)
            for temp in tempElements {
                let double = temp["price"] as! Double
                tempPrice += double
                let cants = temp["quantity"] as! Int
                tempCant += cants
                if let arrayCan = temp["modifier_prices"] as? NSArray {
                    for arr in arrayCan {
                        let price_modif = Double(arr as! String)
                        tempPrice += price_modif!
                    }
                }else{
                    if let arrayM = temp["modifiers"] as? NSArray {
                        //                        print(arrayM)
                        for modif in arrayM {
                            if let array = modif as? NSDictionary {
                                if let arr = array.value(forKey: "modifier_price") as? NSArray {
                                    for tempM in arr {
                                        let price_modif = tempM as! Double
                                        tempPrice += price_modif
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                
                if let arrayC = temp["complements"] as? NSDictionary {
                    if let arr = arrayC.value(forKey: "complement_price") as? NSArray {
                        for tempC in arr {
                            //                            print(tempC, "temp Complements")
                            let price_comp = tempC as! Double
                            tempPrice += price_comp
                        }
                        
                    }
                }
            }
            if tempPrice > 0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: Double(tempPrice) as NSNumber) {
                    carritoTotal.text = "\(formattedTipAmount)"
                }
                //                totalLabel.text = "$\(tempPrice)0"
                carritoCuantity.text = "\(tempCant)"
                totalPrice = tempPrice
                cantidad = tempCant
            }else{
                viewCarrito.alpha = 0
                totalPrice = 0
                cantidad  = 0
            }
        }
        
        ws.delegate = self
        orderws.delegate = self
        userws.delegate = self
        gws.delegate = self
        userws.isUpdateAvailable()
        orderws.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        //        LoadingOverlay.shared.showOverlay(view: self.view)
        let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
        userws.getUnreadNewsFeedLog(client_id: client_id)
        userws.getInStates()
        
        self.navigationController?.isNavigationBarHidden = true
        //        gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            let clocation = locationManager.location
            if clocation?.coordinate.latitude == nil{
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                    //                    locationManager.requestAlwaysAuthorization()
                    locationManager.requestWhenInUseAuthorization()
                }
            }else{
                userLat = (clocation?.coordinate.latitude)!
                userLng = (clocation?.coordinate.longitude)!
               
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
            }
        }
        
        placesClient = GMSPlacesClient.shared()
        //        MARK: get kitchen
        operationRef = Database.database().reference().child("operations")
        handle = operationRef.observe(.value, with: { (snapshot) -> Void in
            self.loadSlider()
            //            MARK: Descomentar si es necesario
            //            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showView(_:)), name: NSNotification.Name(rawValue: "showView"), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.deletePedido(_:)), name: NSNotification.Name(rawValue: "deletePedido"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.goReady(_:)), name: NSNotification.Name(rawValue: "goReady"), object: nil)
        //        isUpdateAvailable()
    }
    func registerDeviceNotifications(){
        if let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId {
            print(userId)
            userws.registerDeviceNotifications(client_pk: UserDefaults.standard.object(forKey: "client_id") as! Int, token: userId)
        }
        
    }
    func didSuccessGetInStates(states: [String]) {
        //        print(states)
        //        let loadingHome = LOADINGOVERViewController()
        //        loadingHome.dismiss(animated: true, completion: nil)
        estadosDisponibles = states
        collectionVIew.reloadData()
    }
    func didFailGetInStates(title: String, subtitle: String) {
        //        let loadingHome = LOADINGOVERViewController()
        //        loadingHome.dismiss(animated: true, completion: nil)
    }
    func didSuccessRegisterDeviceNotifications() {
        OneSignal.setSubscription(true)
        //        print("success on register notification")
    }
    func didFailRegisterDeviceNotifications(title: String, subtitle: String) {
        //        print(title)
    }
    override func viewWillDisappear(_ animated: Bool) {
        //        ref.removeAllObservers()
        if operationRef != nil {
            operationRef.removeObserver(withHandle: handle)
        }
    }
    func didSuccessGetNewsFeedLog(newsFeed: Int) {
        if newsFeed > 0 {
            //            newsFeedTotal.text = "\(newsFeed)"
            newsFeedTotal.text = "\(1)"
            viewNewsFeed.alpha = 1
        }else{
            viewNewsFeed.alpha = 0
        }
        
    }
    func didFailGetNewsFeedLog(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    func didSuccessChangeOrderStatus() {
             //   UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    func didFailChangeOrderStatus(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    @objc func showView (_ notification: NSNotification) {
        self.numPedidos.alpha = 1
        //        print("se ha creado una orden")
    }
    
    func homeInfo(news: [News], establishments: [Establishment])  {
        var newsCounter = 0
        var indxEst = 0
        var indxNF = 0
        guard !structure.isEmpty else {
            structure.removeAll()
            return
        }
        
        //        MARK: Se pidio que se quitara pero se va a conservar
        //        if noCloseEstablishments {
        //            structure = ["grettings", "filter", "closer"]
        //            index = [0, 0, 0]
        //        }else{
        viewNoCities.alpha = 0
        structure = ["grettings", "filter","closer", "hurry"]
        index = [0, 0, 0, 0]
        //        }
        if establishments.count < 5 {
            for _ in 0..<establishments.count {
                if newsCounter != news.count {
                    structure.append("establishment")
                    index.append(indxEst)
                    indxEst += 1
                    
                    structure.append("news")
                    index.append(indxNF)
                    indxNF += 1
                    
                    newsCounter += 1
                }else{
                    structure.append("establishment")
                    index.append(indxEst)
                    indxEst += 1
                }
            }
        }else{
            for i in 0..<establishments.count  {
                if newsCounter != news.count {
                    if ((i+1)%5 == 0 && i != 0) {
                        structure.append("establishment")
                        index.append(indxEst)
                        indxEst += 1
                        
                        structure.append("news")
                        index.append(indxNF)
                        indxNF += 1
                        
                        newsCounter += 1
                    }else{
                        structure.append("establishment")
                        index.append(indxEst)
                        indxEst += 1
                    }
                }else{
                    structure.append("establishment")
                    index.append(indxEst)
                    indxEst += 1
                }
            }
        }
        
        
        if valuesItems.count > 0 {
            structure.append("title")
            structure.append("categorias")
            index += [0, 0]
        }
        totalEstablishments = establishments
        totalNewsFeed  = news
        tableView.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    @objc func goReady (_ notification: NSNotification) {
        //        print("orden lista")
        //        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
    }
    
    func showOrder(orderId: Int) {
        for view in (self.navigationController?.viewControllers)! {
            print(view, "VIEW CONTROLLERS")
        }
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.currentOrderId = orderId
        newVC.pedidos = DatabaseFunctions.shared.getPedidos()
        newVC.readyDesplegar = true
        newVC.showPedido = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func newsFeed(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
        
    }
    @IBAction func backToHome(_ sender: UIButton) {
        UserDefaults.standard.set("menu", forKey: "place")
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }

            self.restoreSesion()
    
            
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func settings(_ sender: Any) {
        if isUpdatedVersion {
            if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
                if !invited {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                    let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    })
                    let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                        self.restoreSesion()
                    })
                    alert.addAction(aceptar)
                    alert.addAction(irA)
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }else{
            self.updateAlert()
        }
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    @IBAction func pedidos(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    func didSuccessGetGeolocation(location: String, state: String) {
        LoadingOverlay.shared.hideOverlayView()
        self.stateComplete = state
        let clocation = locationManager.location
        let geoCoder = CLGeocoder()
        let location2 = CLLocation(latitude: userLat, longitude: userLng)
        //        MAARK: Por sis i o por si no
        if let mun = UserDefaults.standard.object(forKey: "municipalty") as? String {
          //  valueInfo = mun
            
          valueInfo =   mun.elementsEqual("Santiago de Querétaro") ? "Querétaro" : mun
        }else{
            valueInfo = "Estado"
        }
        
        valuePlace = "Ubicación"
        
        
        if let mun = UserDefaults.standard.object(forKey: "category") as? String {
            valueCat = mun
        }else{
            valueCat = "Giro de negocio"
        }
        
        if let st = UserDefaults.standard.object(forKey: "state") as? String {
            valueState = st
        }else{
            valueState = ""
        }
        geoCoder.reverseGeocodeLocation(location2, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            if let placeMark = placemarks?[0] {
                // Location name
                if let locationName = placeMark.location {
                    //                    print(locationName)
                }
                // Street address
                if let street = placeMark.thoroughfare {
                    //                    print(street)
                }
                // City
                //                if UserDefaults.standard.object(forKey: "state") == nil{
                if let state = placeMark.administrativeArea {
                    self.valueStateAround = state
                    if let st = UserDefaults.standard.object(forKey: "state") as? String {
                        self.valueState = st
                    }else{
                        self.valueState = state
                    }
                    //                        self.ws.viewEstablishmentAround(latitude: self.userLat, longitude: self.userLng, codeState: self.valueState)
                    self.loadSlider()
                }
                // City
                if let city = placeMark.subAdministrativeArea {
                    self.valueInfoAround = location
                    if let loc = UserDefaults.standard.object(forKey: "municipalty") as? String {
                        //self.valueInfo = loc
                          self.valueInfo =  loc.elementsEqual("Santiago de Querétaro") ? "Querétaro" : loc
                        
                    }else{
                      //  self.valueInfo = location
                        
                       self.valueInfo =  location.elementsEqual("Santiago de Querétaro") ? "Querétaro" : location
                        
                    }
                }
                // Colonia
                if let colonia = placeMark.subLocality {
                    self.placeString = colonia
                    self.direction = colonia
                    //                    MARK: Descomentar si es necesario
                    //                    self.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                    //                    self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
                }
                // Zip code
                if let zip = placeMark.isoCountryCode {
                    //                    print(zip)
                }
                // Country
                if let country = placeMark.country {
                    //                    print(country)
                }
            }
            let referenceEstablishment = Database.database().reference().child("establishments").child(self.valueState)//.child(self.valueInfo)
            referenceEstablishment.observe(.value, with: { (snapshot) -> Void in
                self.loadSlider()
                //                MARK: Descomentar si es necesario
                //                self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
            })
            //            MARK: self.valueState
            self.ws.getAllTypeKitchen(codeState: self.valueState.stripingDiacritics)
            let bar = self.stateComplete.folding(options: .diacriticInsensitive, locale: .current)
            if UserDefaults.standard.object(forKey: "client_id") as! Int != nil {
                
                self.userws.getLastState(state: bar, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
                
            }
            
        })
    }
    func didSuccessGetLastState() {
        
    }
    func didFailGetLastState(title: String, subtitle: String) {
        print(title, subtitle)
    }
    func didFailGetGeolocation(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        //        let loadingHome = LOADINGOVERViewController()
        //        loadingHome.dismiss(animated: true, completion: nil)
        
    }
    func didSuccessgetNearbyPlaces(nearbyPlace: [Places]) {
        self.places = nearbyPlace
        //        MARK: Descomentar si es necesario
        //        tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
    }
    
    func didSuccessGetEstablshmentsById(info: [Establishment]) {
        totalEstablishments = info
        viewNoCities.alpha = 0
        //        print(structure)
        //        MARK: Descomentar si es necesario
        //        self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        //        self.ws.getNewsFeeds()
        let bar = self.stateComplete.folding(options: .diacriticInsensitive, locale: .current)
        self.ws.getNewsFeedsNew(state: bar)
        
    }
    func didFailGetEstablshmentsById(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didSuccessGetPendingOrder(id: Int) {
        //        viewMyOrder.alpha = 1
        
    }
    func didFailGetPendingOrder(error: String, statusCode: Int, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    func loadSlider(){
        print("around")
        print(self.userLat)
        print(self.userLng)
        print(self.valueStateAround)
        self.ws.viewEstablishmentAround(latitude: self.userLat, longitude: self.userLng, codeState: self.valueStateAround.stripingDiacritics, order_type: "0")
        //self.ws.viewEstablishmentAround(latitude: 20.8388633,   longitude: -100.9411175, codeState: "QRO",                 order_type: "0")
        
    }
    func didSuccessGetNewsFeedHome(news: [News]) -> Void {
        var newsVar =  news
        var tempArray = [News]()
        while !newsVar.isEmpty {
            let i = Int(arc4random_uniform(UInt32(newsVar.count)))
            let obj = newsVar.remove(at: i)
            tempArray.append(obj)
        }
        homeInfo(news: tempArray, establishments: totalEstablishments)
    }
    
    func didFailSetNewsFeedLog(title: String, subtitle: String) {
        
    }
    func didFailGetNewsFeedHome(error: String, subtitle: String) {
        homeInfo(news: [], establishments: totalEstablishments)
    }
    func selectedSearchId(id: Int) {
        if isUpdatedVersion {
            var tempids = [Int]()
            tempids.append(id)
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
            nwVC.ids = tempids
            nwVC.userLat = self.userLat
            nwVC.userLng = self.userLng
            nwVC.valueCity = self.valueInfo
            nwVC.valueCategory = self.valueCat
            nwVC.valuePlace = self.valuePlace
            nwVC.valueState = self.valueState
            self.defaults.set(self.ids, forKey: "ids_establishment")
            self.navigationController?.pushViewController(nwVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    
    func nearbyTapAction(id: Int, name: String, indexPath: IndexPath) {
        if isUpdatedVersion {
            var tempids = [Int]()
            tempids.append(id)
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = id
            newVC.municipioStr = self.disctrit[indexPath.row]
            newVC.categoriaStr = self.categories[indexPath.row]
            newVC.lugarString = self.locations[indexPath.row]
            newVC.establString = name
            if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool{
                if !order_done {
                    defaults.set(id, forKey: "establishment_id")
                }
            }else{
                defaults.set(id, forKey: "establishment_id")
            }
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    @IBAction func search(_ sender: Any) {
        if isUpdatedVersion {
            let newXIB = SearchViewController(nibName: "SearchViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.delegate = self
            newXIB.isFromPromos = false
            newXIB.state = self.valueState
            newXIB.municipality = self.valueInfo
            present(newXIB, animated: true, completion: nil)
        }else{
            self.updateAlert()
        }
    }
    
    func closeModal() {
        newsFeedButton.alpha = 1
        searchButton.alpha = 1
        homeButton.alpha = 1
        //        viewNewsFeed.alpha = 1
    }
    
    func openModalAndSingle(modal: Bool, place: Bool, category: Bool, city: Bool, bussinesLine: Bool, type: Bool) {
        if isUpdatedVersion {
            if modal {
                newsFeedButton.alpha = 0
                searchButton.alpha = 0
                viewNewsFeed.alpha = 0
                homeButton.alpha = 0
                if type {//Estado / Municipio
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Tipo de Pedido"
                    newXIB.municipio =  valueInfo
                    newXIB.categoria = valueCat
                    newXIB.isTypeOrders = type
                    newXIB.places = false
                    newXIB.delegate = self
                    newXIB.typeOrdersList = self.mListTypeOrders
                    newXIB.state = valueState
                    newXIB.idBussinessLine = midBussinessLine
                    present(newXIB, animated: true, completion: nil)
                }else if place {//Estado / Municipio
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Ubicación"
                    newXIB.municipio =  valueInfo
                    newXIB.categoria = valueCat
                    newXIB.places = false
                    newXIB.delegate = self
                    newXIB.state = valueState
                    newXIB.idBussinessLine = midBussinessLine
                    present(newXIB, animated: true, completion: nil)
                }else if category {
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Giro de negocio"
                    newXIB.municipio = valueInfo
                    newXIB.categoria = "none"
                    newXIB.places = false
                    newXIB.categories = true
                    newXIB.delegate = self
                    newXIB.state = valueState
                    present(newXIB, animated: true, completion: nil)
                }else if bussinesLine {//Giro de negocio
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Giro de negocio"
                    newXIB.municipio = valueInfo
                    newXIB.categoria = "none"
                    newXIB.places = false
                    newXIB.categories = true
                    newXIB.delegate = self
                    newXIB.state = valueState
//                    newXIB.from = "guide"
                    present(newXIB, animated: true, completion: nil)
                }else{
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Estado"
                    newXIB.municipio = "none"
                    newXIB.categoria = "none"
                    newXIB.places = true
                    newXIB.delegate = self
                    newXIB.state = valueState
                    present(newXIB, animated: true, completion: nil)
                }
            }else{
                if valueCat != "Giro de negocio" && valuePlace != "Ubicación" {
                    print(ref)
                    ref.observe( .value, with: { (snapshot) -> Void in
                        self.ids.removeAll()
                        if !(snapshot.value is NSNull) {
                            if let stateQuery = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                                let state = self.valueInfo.elementsEqual("Santiago de Querétaro") ? "Querétaro" : self.valueInfo
                                if let singleState = (stateQuery.value(forKey: state) as? NSDictionary)  {
                                    for item in singleState.allValues as NSArray {
                                        print(item)
                                        /*let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                        self.ids.append(ids)*/
                                        
                                        if self.mTypeOrderCode != ""{
                                            for orderTypes in (item as! NSDictionary).value(forKey: "order_types") as? NSArray ?? NSArray(){
                                                if orderTypes as! String == self.mTypeOrderCode{ //Buscamos por tipo de orden
                                                    self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                                }
                                            }
                                        }else if self.midInside != "" || self.midInside != "0"{
                                            if (item as! NSDictionary).value(forKey: "inside_id") as? Int == Int(self.midInside){
                                                self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                            }
                                            
                                        }else if self.midBussinessLine != "" || self.midBussinessLine != "0" {
                                            if (item as! NSDictionary).value(forKey: "business_line_id") as? Int == Int(self.midBussinessLine){
                                                self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                            }
                                        }
                                    }
                                    
                                    let uniqueVals = self.uniq(source: self.ids)
                                    print(uniqueVals)
                                    
                                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                    let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                                    nwVC.ids = uniqueVals
                                    nwVC.userLat = self.userLat
                                    nwVC.userLng = self.userLng
                                    nwVC.valueCity = self.valueInfo
                                    nwVC.valueCategory = self.valueCat
                                    nwVC.valuePlace = self.valuePlace
                                    nwVC.valueState = self.valueState
                                    nwVC.typeKitchen = false
                                    self.navigationController?.pushViewController(nwVC, animated: true)
                                }
                            }
                        }
                    })
                }else{
                    //                print(ref)
                    ref.observe( .value, with: { (snapshot) -> Void in
                        self.ids.removeAll()
                        if !(snapshot.value is NSNull) {
                            if let stateQuery = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                                if let singleState = (stateQuery.value(forKey: self.valueInfo) as? NSDictionary)  {
                                    for item in singleState.allValues as NSArray {
                                        let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                        self.ids.append(ids)
                                        //                                    print(self.ids)
                                    }
                                    
                                }
                            }
                        }
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                        nwVC.ids = self.ids
                        nwVC.valueCity = self.valueInfo
                        nwVC.valueCategory = self.valueCat
                        nwVC.valuePlace = self.valuePlace
                        nwVC.valueState = self.valueState
                        nwVC.userLat = self.userLat
                        nwVC.userLng = self.userLng
                        nwVC.typeKitchen = false
                        //                    self.defaults.set(self.ids, forKey: "ids_establishment")
                        self.navigationController?.pushViewController(nwVC, animated: true)
                        //                    self.ws.getEstablishmentsById(ids: self.ids)
                        
                    })
                }
                //            print("gotonextPage")
                
            }
        }else{
            self.updateAlert()
        }
        
    }
    
    
    
    func didSuccessGetAllTypeKitchen(info: [Kitchens]) {
        LoadingOverlay.shared.hideOverlayView()
        ref.observe( .value, with: { (snapshot) -> Void in
            var temp = [KitchensComplete]()
            var tempIdsArray = [[Int]]()
            for v in info{
                var cont = 0
                var tempIds = [Int]()
                
                if self.valueState != "" && self.valueInfo != "Estado" {
                    if !(snapshot.value is NSNull) {
                        if let state_establishments = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                            //                            print(state_establishments)
                            //                            if let all_establishments = state_establishments.value(forKey: self.valueInfo) as? NSDictionary{
                            let all_establishments = state_establishments.allValues as! NSArray
                            for establishment in  all_establishments {
                                if  let es = establishment as? NSDictionary {
                                    if let establish = (establishment as! NSDictionary).allKeys as? NSArray {
                                        for establo in establish {
                                            //                                            print((establishment as! NSDictionary).value(forKey: establo as! String))
                                            let establishments = (establishment as! NSDictionary).value(forKey: establo as! String)
                                            print(establishments)
                                            if let type_kitchen = (establishments as! NSDictionary).value(forKey: "business_line_name") as? String {
                                                let longitud = ((establishments as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "longitude") as! CLLocationDegrees
                                                let latitud = ((establishments as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "latitude") as! CLLocationDegrees
                                                if type_kitchen.contains(v.name) {
                                                    if self.getRatio(latitud: self.userLat, Longitud: self.userLng, Latitud2: latitud, Longitud2: longitud, km: 2) {
                                                        cont += 1
                                                        tempIds.append(((establishments as! NSDictionary).value(forKey: "id") as! Int))
                                                    }else{
                                                        tempIds.append(((establishments as! NSDictionary).value(forKey: "id") as! Int))
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                let tempItem = KitchensComplete()
                tempItem.image = v.image
                tempItem.name = v.name
                tempItem.hide = v.hide
                tempItem.nearby = cont
                tempItem.ids = tempIds
                temp.append(tempItem)
                tempIdsArray.append(tempIds)
            }
            self.kitchenIds = tempIdsArray
            self.valuesItems = temp
            self.tableView.reloadData()
        })
    }
    
    @IBAction func orderStatus(_ sender: Any) {
        //        if status == "3" {
        //            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        //            let newVC = storyboard.instantiateViewController(withIdentifier: "OrderDetail") as! OrderDetailViewController
        //            self.navigationController?.pushViewController(newVC, animated: true)
        //        }else{
        //            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        //            let newVC = storyboard.instantiateViewController(withIdentifier: "OrderDetail") as! OrderDetailViewController
        //            self.navigationController?.pushViewController(newVC, animated: true)
        //        }
        
    }
    func selectedCategory(index: IndexPath) {
        if valuesItems[index.item].nearby > 0 {
            if isUpdatedVersion {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                nwVC.ids = valuesItems[index.item].ids
                nwVC.nameTypeKitchen = valuesItems[index.item].name
                nwVC.typeKitchen = true
                nwVC.userLat = self.userLat
                nwVC.userLng = self.userLng
                defaults.set(valuesItems[index.item].ids, forKey: "ids_establishment")
                self.navigationController?.pushViewController(nwVC, animated: true)
            }else{
                self.updateAlert()
            }
        }else{
            if isUpdatedVersion {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                nwVC.ids = kitchenIds[index.item]
                nwVC.nameTypeKitchen = valuesItems[index.item].name
                nwVC.typeKitchen = true
                nwVC.userLat = self.userLat
                nwVC.userLng = self.userLng
                defaults.set(valuesItems[index.item].ids, forKey: "ids_establishment")
                self.navigationController?.pushViewController(nwVC, animated: true)
            }else{
                self.updateAlert()
            }
        }
    }
    func didFailGetAllTypeKitchen(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if error != "204" {
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }else{
            valuesItems = []
            places = []
            self.tableView.reloadData()
        }
    }
    func setValue(value: String, place: Bool, category: Bool, state: String, isType: Bool, idBussinessLine: String) {
        newsFeedButton.alpha = 1
        searchButton.alpha = 1
        homeButton.alpha = 1
        viewNewsFeed.alpha = 0
        
        if category{
            midBussinessLine = idBussinessLine
        }else{
            midInside = idBussinessLine
        }
        
        if isType{
            mTypeOrderSelect = value
            switch value {
            case "Servicio a Mesa":
                mTypeOrderCode = "SM"
                break
                
            case "Recoger en Barra":
                mTypeOrderCode = "RB"
                break
                
            case "Recoger en Sucursal":
                mTypeOrderCode = "RS"
                break
            default:
                mTypeOrderCode = "SD"
            }
        }else if place {
            valueInfo = value
            valueState = state
            //            UserDefaults.standard.set(valueState, forKey: "state")
            //            UserDefaults.standard.set(valueInfo, forKey: "municipalty")
        }else if category {
            valueCat = value
            //            UserDefaults.standard.set(valueCat, forKey: "category")
        }else{
            valuePlace = value
            //            UserDefaults.standard.set(valuePlace, forKey: "place")
        }
        
        //        MARK: Descomentar si es necesario
        //        tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        loadSlider()
        //        MARK: get kitchen / valueState
        ws.getAllTypeKitchen(codeState: valueState.stripingDiacritics)
    }
    func didSuccessGetViewEstablishmentAround(info: [Int]) {
        //        establishmentsID = info
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        if info.count > 0 {
            print("ids")
            print(info)
            self.ws.getEstablishmentsById(ids: info)
        }else{
           
            self.totalEstablishments.removeAll()
            self.totalNewsFeed.removeAll()
            self.noCloseEstablishments = true
            self.viewNoCities.alpha = 1
            structure = ["grettings", "filter","closer", "hurry", "establishment"]
            self.index = [0, 0, 0, 0, 0]
            
            tableView.reloadData()
        }
        
        //        loadSlider()
    }
    func didFailGetViewEstablishmentAround(error: String, subtitle: String) {
        if error == "empty" {
            self.totalEstablishments.removeAll()
            self.totalNewsFeed.removeAll()
            self.noCloseEstablishments = true
            self.viewNoCities.alpha = 1
            self.structure = ["grettings", "filter", "closer"]
            self.index = [0, 0, 0]
        }else{
            self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        }
    }
    
    func OnSelectInEstablishment(typeAction: String, isSelected: Bool) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.view.isUserInteractionEnabled = false
        switch typeAction {
        case "RS":
            isServiceInEst = true
            isServiceInBar = false
            isServiceInTable = false
            isServiceInHome = false
            break
            
        case "RB":
            isServiceInEst = false
            isServiceInBar = true
            isServiceInTable = false
            isServiceInHome = false
            break
            
        case "SM":
            isServiceInEst = false
            isServiceInBar = false
            isServiceInTable = true
            isServiceInHome = false
            break
            
        case "SD":
            isServiceInEst = false
            isServiceInBar = false
            isServiceInTable = false
            isServiceInHome = true
            break
        default:
            isServiceInEst = false
            isServiceInBar = false
            isServiceInTable = false
            isServiceInHome = false
        }
        //self.ws.viewEstablishmentAround(latitude: 20.64701472426902, longitude: -100.4290414514649, codeState: "QRO", order_type: typeAction/*self.valueStateAround*/)
        
        self.ws.viewEstablishmentAround(latitude: self.userLat, longitude: self.userLng, codeState: self.valueStateAround.stripingDiacritics, order_type: typeAction)
        
        self.filtetSelection = typeAction
        typeServiceSelecction(itemSelection:typeAction)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getRatio(latitud: CLLocationDegrees, Longitud: CLLocationDegrees, Latitud2: CLLocationDegrees, Longitud2: CLLocationDegrees, km: Int) -> Bool{
        let coordinate1 = CLLocation(latitude: latitud, longitude: Longitud)
        let coordinate2 = CLLocation(latitude: Latitud2, longitude: Longitud2)
        
        let distance = coordinate1.distance(from: coordinate2)
        switch km {
        case 2:
            if distance <= 2000 {
                return true
            }else{
                return false
            }
        case 5:
            if distance <= 5000 {
                return true
            }else{
                return false
            }
        case 10:
            if distance <= 10000 {
                return true
            }else{
                return false
            }
        default:
            if distance <= 2000 {
                return true
            }else{
                return false
            }
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myCurrentLocation = CLLocationCoordinate2D(latitude: (locations.last?.coordinate.latitude)!, longitude: (locations.last?.coordinate.longitude)!)
        locationManager.stopUpdatingLocation()
        
        //        getRatio()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //Busqueda por criterios
    func ShowStateCriterias(){
        self.openModalAndSingle(modal: true, place: false, category: false, city: true, bussinesLine: false, type: false)
    }
    
    func ShowBussinesLineCriterias(){
        self.openModalAndSingle(modal: true, place: false, category: false, city: false, bussinesLine: true, type: false)
    }
    
    func ShowLocationCriterias(){
        self.openModalAndSingle(modal: true, place: true, category: false, city: false, bussinesLine: false, type: false)
        
    }
    
    func ShowTypeCriterias(){
       // self.ws.viewEstablishmentOrdertypes(state_code: valueState, district: valueInfo, business_line_id: Int(midBussinessLine) ?? 1, inside_id: Int(midInside) ?? 0)
        self.ws.viewEstablishmentOrdertypes(state_code: valueState, district: valueInfo, business_line_id: Int(midBussinessLine) ?? 0, inside_id: Int(midInside) ?? 0)
    }
    
    func SearchByNewCriterias(){
        self.openModalAndSingle(modal: false, place: false, category: false, city: false, bussinesLine: false, type: false)
    }
    
    func didSuccessTypeOrders(reward: [String]) {
        self.mListTypeOrders = reward
        self.openModalAndSingle(modal: true, place: false, category: false, city: false, bussinesLine: false, type: true)
    }
    
    func didFailTypeOrders(error: String, subtitle: String) {
        print(error)
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource, singleEstDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if valuesItems.count == 0 {
        //            return totalEstablishments.count + 4
        //        }else{
        //            return totalEstablishments.count + 6
        //        }
        return structure.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if structure[indexPath.row] == "grettings" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "grettingsCell", for: indexPath) as! WelcomeTableViewCell
            cell.selectionStyle = .none
            let hour = Calendar.current.component(.hour, from: Date())
            if hour < 12 {
                cell.grettingsLabel.text = "¡Buenos días "
            }else if hour < 19 {
                cell.grettingsLabel.text = "¡Buenas tardes "
            }else{
                cell.grettingsLabel.text = "¡Buenas noches "
            }
            cell.nameLabel.text = (UserDefaults.standard.object(forKey: "first_name") as? String) ?? "" + "!"
            if let image = UserDefaults.standard.object(forKey: "avatar") as? String, image != "" {
                Nuke.loadImage(with: URL(string: image)!, into: cell.imageUser)
            }
            if placeString == "" && valueInfoAround == "" && valueStateAround == "" {
                cell.locationLabel.isHidden = true
                cell.youAreAtLabel.textAlignment = .center
            }else{
                if placeString == "" && valueInfoAround == "" {
                    cell.locationLabel.isHidden = true
                    cell.youAreAtLabel.textAlignment = .center
                }else{
                    cell.locationLabel.isHidden = false
                    cell.youAreAtLabel.textAlignment = .right
                }
                
            }
            print(valueStateAround)
            if placeString == "" && valueInfoAround == "" {
                cell.locationLabel.text = " " + valueStateAround
            }else{
                if placeString == "" {
                    cell.locationLabel.text = " " + valueInfoAround + ", " + valueStateAround
                }else{
                    if valueInfoAround == "" {
                        cell.locationLabel.text = " " + placeString + ", " + valueStateAround
                    }else{
                        cell.locationLabel.text = " " /*+ placeString + ", "*/ + valueInfoAround + ", " + valueStateAround
                    }
                }
            }
            
            return cell
        }else if structure[indexPath.row] == "filter" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewfiltersCell") as! NewFiltersCriteriasTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.lblCity.text = valueInfo
            cell.lblBussinessLine.text = valueCat
            cell.lblLocation.text = valuePlace
            cell.lblType.text = mTypeOrderSelect
            /*cell.selectionStyle = .none
             cell.delegate = self
             cell.valueInfo = valueInfo
             cell.valueCat = valueCat
             cell.valuePlace = valuePlace
             cell.collectionViewMenu.reloadData()*/
            return cell
        }else if structure[indexPath.row] == "hurry" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "hurryCell") as! HurryTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            
            /*cell.isServiceInEst = isServiceInEst
             cell.isServiceInBar = isServiceInBar
             cell.isServiceInTable = isServiceInTable
             cell.isServiceInHome = isServiceInHome*/
            return cell
        }else if structure[indexPath.row] == "closer" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nearbyPlaces") as! NearbyCellTableViewCell
            cell.selectionStyle = .none
            //            MARK: se pidió que se quitara pero conservar
            //            if noCloseEstablishments {
            //                cell.queRicoTitle.text = "No encontramos establecimientos cerca de ti. Como no queremos que te quedes con hambre, te mostramos otros lugares en \(valueInfo)."
            //            }else{
            cell.queRicoTitle.attributedText = CustomsFuncs.getAttributedString(title1: "¡Estos son los lugares cercanos a ti!\n", title2: "¿Qué necesitas comprar?", colorText: 0)
            //            }
            return cell
        }else if structure[indexPath.row] == "title" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell")!
            cell.selectionStyle = .none
            return cell
        }else if structure[indexPath.row] == "categorias" {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "categoriesCell") as? CollectionEstablishmentTableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.values = valuesItems
            cell.collectionView.reloadData()
            cell.delegate = self
            return cell
        }else if structure[indexPath.row] == "news" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsfeed") as! NewsFeedTableViewCell
            cell.selectionStyle = .none
            cell.content.text = totalNewsFeed[index[indexPath.row]].content
            cell.title.text = totalNewsFeed[index[indexPath.row]].title
            cell.category.text = totalNewsFeed[index[indexPath.row]].category
            if(totalNewsFeed[index[indexPath.row]].image != ""){
                Nuke.loadImage(with: URL(string: totalNewsFeed[index[indexPath.row]].image)!, into: cell.img)
                Nuke.loadImage(with: URL(string: totalNewsFeed[index[indexPath.row]].image)!, into: cell.blur)
            }
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "establishments", for: indexPath) as! EstablishmentsTableViewCell
            if structure[indexPath.row] == "establishment" {
                cell.contentView.layer.cornerRadius = 3
                cell.contentView.clipsToBounds = true
                
                if totalEstablishments.count > 0 {
                cell.establishmentName.text = totalEstablishments[index[indexPath.row]].name
                
                let icons = CustomsFuncs.getIconsServicePickPal(data: totalEstablishments[index[indexPath.row]].pickpal_services)
                switch icons.count {
                //switch totalEstablishments[index[indexPath.row]].pickpal_services.count {
                case 4:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = false
                    cell.btnPoints.isHidden = false
                    
                    cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                    cell.btnPromos.image = UIImage(named: icons[1])
                    cell.btnGuia.image = UIImage(named: icons[2])
                    cell.btnPoints.image = UIImage(named: icons[3])
                    break
                case 3:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = false
                    cell.btnPoints.isHidden = true
                    cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                    cell.btnPromos.image = UIImage(named: icons[1])
                    cell.btnGuia.image = UIImage(named: icons[2])
                    break
                    
                case 2:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                    cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
//                    cell.btnPromos.image = UIImage(named: icons[1])
                    break
                    
                case 1:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = true
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                  //  cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                    break
                    
                default:
                    cell.btnFoodAndDrinks.isHidden = true
                    cell.btnPromos.isHidden = true
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                    break
                }
                
                let iconsTyOrder = CustomsFuncs.getIconsOrderTypesPickPal(data: totalEstablishments[index[indexPath.row]].order_types)
                switch iconsTyOrder.count {
                case 4:
                    cell.imgServiceEst.isHidden = false
                    cell.imgServiceBar.isHidden = false
                    cell.imgServiceTable.isHidden = false
                    cell.imgServiceHome.isHidden = false
                    
                    cell.imgServiceEst.image = UIImage(named: iconsTyOrder[0])
                    cell.imgServiceBar.image = UIImage(named: iconsTyOrder[1])
                    cell.imgServiceTable.image = UIImage(named: iconsTyOrder[2])
                    cell.imgServiceHome.image = UIImage(named: iconsTyOrder[3])
                    break
                case 3:
                    cell.imgServiceEst.isHidden = true
                    cell.imgServiceBar.isHidden = false
                    cell.imgServiceTable.isHidden = false
                    cell.imgServiceHome.isHidden = false
                    
                    cell.imgServiceBar.image = UIImage(named: iconsTyOrder[0])
                    cell.imgServiceTable.image = UIImage(named: iconsTyOrder[1])
                    cell.imgServiceHome.image = UIImage(named: iconsTyOrder[2])
                    break
                    
                case 2:
                    cell.imgServiceEst.isHidden = true
                    cell.imgServiceBar.isHidden = true
                    cell.imgServiceTable.isHidden = false
                    cell.imgServiceHome.isHidden = false
                    
                    cell.imgServiceTable.image = UIImage(named: iconsTyOrder[0])
                    cell.imgServiceHome.image = UIImage(named: iconsTyOrder[1])
                    break
                    
                case 1:
                    cell.imgServiceEst.isHidden = false
                    cell.imgServiceBar.isHidden = true
                    cell.imgServiceTable.isHidden = true
                    cell.imgServiceHome.isHidden = true
                    
                    cell.imgServiceEst.image = UIImage(named: iconsTyOrder[0])
                    break
                    
                default:
                    cell.imgServiceEst.isHidden = true
                    cell.imgServiceBar.isHidden = true
                    cell.imgServiceTable.isHidden = true
                    cell.imgServiceHome.isHidden = true
                    break
                }
                
                
                if totalEstablishments[index[indexPath.row]].future_available! {
                    if let logo = totalEstablishments[index[indexPath.row]].logo, logo != "" {
                        Nuke.loadImage(with: URL(string: logo)!, into: cell.establishmentImage)
                        Nuke.loadImage(with: URL(string: logo)!, into: cell.bluredImage)
                    }else{
                        cell.establishmentImage.image = UIImage(named: "comingSoon")
                        cell.bluredImage.image = UIImage(named:"comingSoon")
                    }
                    
                }else{
                    if let logo = totalEstablishments[index[indexPath.row]].logo, logo != "" {
                        Nuke.loadImage(with: URL(string: logo)!, into: cell.establishmentImage)
                        Nuke.loadImage(with: URL(string: logo)!, into: cell.bluredImage)
                    }else{
                        cell.establishmentImage.image = UIImage(named: "placeholderImageRed")
                        cell.bluredImage.image = UIImage(named:"placeholderImageRed")
                    }
                }
                
                cell.subtitleLabel.text = totalEstablishments[index[indexPath.row]].business_area + " - " + totalEstablishments[index[indexPath.row]].place
                cell.txtTime.text = totalEstablishments[index[indexPath.row]].time + " min"
                cell.viewTime.layer.cornerRadius = 10
                cell.viewTime.clipsToBounds = true
                cell.viewTime.layer.masksToBounds = true
                cell.viewTime.layer.shadowColor = UIColor.black.cgColor
                cell.viewTime.layer.shadowOffset = CGSize(width: 0, height: 2)
                cell.viewTime.layer.shadowOpacity = 0.24
                cell.viewTime.layer.shadowRadius = CGFloat(2)
                cell.homeDelegateFlag = true
                cell.delegate = self
                cell.indexPath = indexPath
                if totalEstablishments[index[indexPath.row]].is_open {
                    cell.statusLabel.text = "Abierto"
                    cell.viewTime.alpha = 1
                    cell.noAvaliable.alpha = 0
                    cell.buttonGoSingle.isEnabled = true
                }else{
                    if totalEstablishments[index[indexPath.row]].future_available! {
                        cell.statusLabel.text = "Próximamente"
                        cell.viewTime.alpha = 0
                        if let logo = totalEstablishments[index[indexPath.row]].logo, logo != "" {
                            cell.noAvaliable.alpha = 1
                            cell.iconCerrado.image = UIImage(named: "proximamente")
                        }else{
                            cell.noAvaliable.alpha = 0
                        }
                        cell.buttonGoSingle.isEnabled = false
                    }else{
                        cell.viewTime.alpha = 1
                        cell.statusLabel.text = "Cerrado"
                        cell.noAvaliable.alpha = 1
                        cell.iconCerrado.image = UIImage(named: "iconClosed")
                        cell.buttonGoSingle.isEnabled = false
                    }
                    
                }
                cell.rate(rating: totalEstablishments[index[indexPath.row]].rating)
               
                    cell.noDisponible.alpha = 0
                    cell.establecimiento.alpha = 1
                    cell.sinResultados.alpha = 0
                   
                }else{
                    
                    cell.noDisponible.alpha = 0
                    cell.establecimiento.alpha = 0
                    cell.sinResultados.alpha = 1
                   
                }
//                cell.noDisponible.alpha = 0
                
               
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.row == 0 {
        if structure[indexPath.row] == "grettings" {
            return 113
            //        }else if indexPath.row == 1 {
        }else if structure[indexPath.row] == "filter" {
            return 60
            //        }else if indexPath.row == 2 {
        }else if structure[indexPath.row] == "hurry" {
            return 120//38
            //        }else if indexPath.row == 3 {
        }else if structure[indexPath.row] == "closer" {
            return 56
            //        }else if indexPath.row == totalEstablishments.count + 4 {
        }else if structure[indexPath.row] == "title" {
            return 30
            //        }else if indexPath.row == totalEstablishments.count + 5 {
        }else if structure[indexPath.row] == "categorias" {
            return 392
        }else if structure[indexPath.row] == "news" {
            return 139
        }else{
            return 280
        }
    }
    func goSingleEstablishment(indexPath: IndexPath) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = totalEstablishments[index[indexPath.row]].id
            newVC.municipioStr = valueInfo
            newVC.categoriaStr = totalEstablishments[index[indexPath.row]].category
            newVC.lugarString = totalEstablishments[index[indexPath.row]].place
            newVC.establString = totalEstablishments[index[indexPath.row]].name
            newVC.giroNegocio = totalEstablishments[index[indexPath.row]].business_area
            newVC.pickpalServices = totalEstablishments[index[indexPath.row]].pickpal_services
            newVC.service_available = totalEstablishments[index[indexPath.row]].order_types
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
        
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return estadosDisponibles.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nombreCities", for: indexPath)as! EstadosDisponiblesCollectionViewCell
        cell.estadoLabel.text = estadosDisponibles[indexPath.item]
        return cell
    }
}

extension HomeViewController{
    
    func didSuccessViewMyReward(reward: MyRewardEstablishment) {
        let handle = ref.observe(.value) { (snapshot) in
            if !(snapshot.value is NSNull) {
                if self.isUpdatedVersion {
                    let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
                    let value = snapshot.value as! NSDictionary
                    if let state = value.value(forKey: self.valueState) as? NSDictionary {
                        if let district = state.value(forKey: self.valueInfo) as? NSDictionary {
                            if let inside = district.value(forKey: "\(DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))") as? NSDictionary{
                                newVC.municipio = (inside.value(forKey: "district") as! String)
                                newVC.lugar = (inside.value(forKey: "inside") as! String)
                                newVC.categoria = (inside.value(forKey: "category") as! String)
                            }
                        }
                    }
                    newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    newVC.esablishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newVC.mMyReward = reward
                    newVC.subtotal = self.totalPrice
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    self.updateAlert()
                }
            }
        }
        if ref != nil {
            ref.removeObserver(withHandle: handle)
        }
    }
    
    func didFailViewMyReward(error: String, subtitle: String) {
        
    }
    
    
    func typeServiceSelecction(itemSelection:String){
        
        let preferences = UserDefaults.standard

        let currentLevelKey = "itemSelecteKey"

        let currentLevel = itemSelection
        
        preferences.set(itemSelection, forKey: currentLevelKey)

        //  Save to disk
        let didSave = preferences.synchronize()

        if !didSave {
            //  Couldn't save (I've never seen this happen in real world testing)
            
            print("No se pudo guardar")
            
        }
        
//        fatalError()
         print("Guardando seleccion ", itemSelection)
        
    }
    
}

extension HomeViewController{
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}


extension HomeViewController{
    
    func initlocationManager(){
        
        let authorizationStatus = CLLocationManager.authorizationStatus()

        if (authorizationStatus == CLAuthorizationStatus.denied) {
            getLocation()
        }else{
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.navigationController?.isNavigationBarHidden = true
           
        }
        
    }

    func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                let alertController = UIAlertController (title: "¡Oh no!", message: "¡Necesitamos tu ubicación para encontrar los comercios cercanos a ti!", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Configuración", style: .cancel) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                                
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (_) -> Void in
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                  
                }
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
            }
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                if CLLocationManager.authorizationStatus() == .denied  {
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                    
                }
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("autorizado")
            }
            
        } else {
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("autorizado")
            manager.requestAlwaysAuthorization()
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
            break
        case .denied, .notDetermined, .restricted:
            print("denegado")
           
        default:
            break
        }
    }
}




