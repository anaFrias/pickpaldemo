//
//  RegisterNewEstablishmentViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/27/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class RegisterNewEstablishmentViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var values = ["", "Datos de Contacto", "Nombre Completo", "Número Celular", "Correo Electrónico", "Datos de tu Negocio", "Nombre Comercial", "Dirección Comercial", "Número de Sucursales", "Elige un tipo de comida", "Número de Pedidos Semanales"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RegisterNewEstablishmentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerTitle", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 11 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sendInfoEst", for: indexPath) as! sendButtonEstablishmentTableViewCell
            cell.selectionStyle = .none
            return cell
        } else if indexPath.row == 1 || indexPath.row == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sectionTItle", for: indexPath) as! sectionLabelTableViewCell
            cell.sectionLabel.text = values[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "inputsDropCell", for: indexPath) as! inputDropEstablishmentTableViewCell
            cell.textField.placeholder = values[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "inputsCell", for: indexPath) as! inputEstablishmentCellTableViewCell
            cell.textField.placeholder = values[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    }
}
