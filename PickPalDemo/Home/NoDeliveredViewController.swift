//
//  NoDeliveredViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class NoDeliveredViewController: UIViewController, UITextViewDelegate, userDelegate, closeModalDelegate {

    @IBOutlet weak var detalleComercio: UILabel!
    @IBOutlet weak var nombreDeUsuario: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var placeholder: UILabel!
    var nombreComercio = String()
    var userws = UserWS()
    var orderId = Int()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        if let userName = UserDefaults.standard.object(forKey: "first_name") as? String {
            nombreDeUsuario.text = userName
        }
        detalleComercio.text = "¿Cómo podemos ayudarte con tu pedido de \(nombreComercio)?"
        textView.layer.cornerRadius = 4.5
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 0.5).cgColor
        textView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendInfo(_ sender: Any) {
        let clientId = UserDefaults.standard.object(forKey: "client_id") as! Int
        if textView.text != "" {
            userws.registerHelp2(client_id: clientId, descriptionText: textView.text, category: 2, order: orderId)
        }else{
            let alert = UIAlertController(title: "Campo faltante", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func didSuccessRegisterHelp() {
        let newXIB = SuccedMessageHelpViewController(nibName: "SuccedMessageHelpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true) {
            
        }
    }
    func didFailRegisterHelp(title: String, subtitle: String) {
        let error = ErrorViewController()
        error.modalTransitionStyle = .crossDissolve
        error.modalPresentationStyle = .overCurrentContext
        error.errorMessageString = title
        error.subTitleMessageString = subtitle
        self.present(error, animated: true, completion: nil)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholder.alpha = 0
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            placeholder.alpha = 1
        }else{
            placeholder.alpha = 0
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
