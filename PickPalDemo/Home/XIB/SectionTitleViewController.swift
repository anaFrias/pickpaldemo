//
//  SectionTitleViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 17/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SectionTitleViewController: UIViewController {

    @IBOutlet weak var nameCat: UILabel!
    var nameCategory = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        nameCat.text = nameCategory
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
