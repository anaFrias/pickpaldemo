//
//  AlcoholicDrinksDisclaimerViewController.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 9/19/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
protocol isAlcoholicDelegate {
    func orderBelong(indexPath: IndexPath, name: String, cants: Int, price: Double, totalItemPrice: Double, totalItemCant: Int, id: Int, isDelegate: Bool, isPackage: Bool)
    func confirtNewOrder(indexPath: IndexPath, name: String, cants: Int, price: Double, totalItemPrice: Double, totalItemCant: Int, id: Int, isDelegate: Bool, isPackage: Bool)
    func orderCancel(eraseAll: Bool)
}
class AlcoholicDrinksDisclaimerViewController: UIViewController {

    @IBOutlet weak var menor18: UIButton!
    @IBOutlet weak var mayor18: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var nombreComercio: UILabel!
    var defaults = UserDefaults.standard
    var delegate: isAlcoholicDelegate!
    var index = IndexPath()
    var name = String()
    var price = Double()
    var cant = Int()
    var totalItemPrice = Double()
    var totalItemCant = Int()
    var idItm = Int()
    var isPackage = Bool()
    var nombreDelComercio = String()
    var establishment_id = Int()
//    var notShowin
    override func viewDidLoad() {
        super.viewDidLoad()

        viewContainer.layer.cornerRadius = 5
        menor18.layer.borderColor = UIColor.init(red: 203, green: 29, blue: 62, alpha: 1).cgColor
        menor18.layer.borderWidth = 1
        menor18.layer.cornerRadius = 5
        mayor18.layer.cornerRadius = 5
        
        nombreComercio.text = nombreDelComercio
        // Do any additional setup after loading the view.
    }

    @IBAction func soyMenorBtn(_ sender: Any) {
//        self.delegate.orderCancel(eraseAll: false)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func soyMayorBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != 0 && DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != self.establishment_id {
            
            self.delegate.confirtNewOrder(indexPath: index, name: name, cants: cant, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: idItm, isDelegate: false, isPackage: isPackage)
          
        }else{
        
        self.delegate.orderBelong(indexPath: index, name: name, cants: cant, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: idItm, isDelegate: false, isPackage: isPackage)
            
        }
       
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
