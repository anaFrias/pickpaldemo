//
//  CellEmptySearchTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 11/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class CellEmptySearchTableViewCell: UITableViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var searchArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
