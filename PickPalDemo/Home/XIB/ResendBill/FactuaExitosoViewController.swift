//
//  FactuaExitosoViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/18/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol facturaEnviadaDelegate {
    func facturaEnviada()
}

class FactuaExitosoViewController: UIViewController {

    @IBOutlet weak var viewLayer: UIView!
    @IBOutlet weak var nameUser: UILabel!
    var delegate: facturaEnviadaDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewLayer.layer.cornerRadius = 10
        if let name = UserDefaults.standard.object(forKey: "first_name") as? String {
            nameUser.text = name
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func continuarBtn(_ sender: Any) {
        self.delegate.facturaEnviada()
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
