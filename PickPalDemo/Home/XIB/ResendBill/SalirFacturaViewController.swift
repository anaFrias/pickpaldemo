//
//  SalirFacturaViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/26/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol salirDelegate {
    func salirFactura()
}
class SalirFacturaViewController: UIViewController {
    
    @IBOutlet weak var nameUser: UILabel!
    var delegate: salirDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let userName = UserDefaults.standard.object(forKey: "first_name") as? String {
            nameUser.text = userName
        }
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func yesAction(_ sender: Any) {
        self.delegate.salirFactura()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
