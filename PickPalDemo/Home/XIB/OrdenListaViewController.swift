//
//  OrdenListaViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 10/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol showCurrentOrder {
    func showOrder(orderId: Int)
}

class OrdenListaViewController: UIViewController {

    @IBOutlet weak var container: UIView!
    
    @IBOutlet weak var stablishment_name: UILabel!
    var ttl = String()
    var desc = String()
    var code = String()
    var identifier = String()
    var establishName = String()
    var delegate: showCurrentOrder!
    var orderId = Int()
    var isNotification = Bool()
    var subtitle1 = String()
    var subtitle2 = String()
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var title2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.cornerRadius = 10
        container.layer.masksToBounds = true
        stablishment_name.text = establishName
        title1.text = subtitle1
        title2.text = subtitle2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ready(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            self.delegate.showOrder(orderId: self.orderId)
        })
    }
    
    @IBAction func close(_ sender: Any) {
        if isNotification {
            self.delegate.showOrder(orderId: self.orderId)
        }
    
        self.dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
