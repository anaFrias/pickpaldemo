//
//  SearchViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 27/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Firebase
protocol selectedSearchValue {
    func selectedSearchId(id: Int)
}
class SearchViewController: UIViewController {
    
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    var value = [String]()
    var valueAux2 = [String]()
    var ValueIds = [Int]()
    var mListEstablishments = [RewardsMobile]()
    var delegate: selectedSearchValue!
    var ref: DatabaseReference!
    var state = String()
    var municipality = String()
    var counter = 1
    var handler: DatabaseHandle!
    var noRelated = Bool()
    var from = String()
    
    /*Variables para swichear entre food and drinks y promos*/
    var isFromPromos = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        input.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        if self.from == Constants.PICKPAL_PUNTOS{
            print(Constants.PICKPAL_PUNTOS)
            input.placeholder = "Busca por nombre comercial"
        }else if self.from == "guide" {
            ref = Database.database().reference().child("establishments" + from).child(state)
        }else{
            ref = Database.database().reference().child("establishments" + (from.isEmpty ? "" : from + "s")).child(state)//.child(municipality)
        }
        
        tableView.register(UINib(nibName: "CellFilterAdvsTableViewCell", bundle: nil), forCellReuseIdentifier: "filterAdvs")
        tableView.register(UINib(nibName: "CellEmptySearchTableViewCell", bundle: nil), forCellReuseIdentifier: "cellEmptySearch")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        loadStablishments(filter: textField.text!)
    }
    @IBAction func closeModal(_ sender: Any) {
        if ref != nil {
            if handler != nil {
                ref.removeObserver(withHandle: handler)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadStablishments(filter: String){
        var items = [String]()
        var itemsAux2 = [String]()
        var ids = [Int]()
        value.removeAll()
        var kitchenTypes = ""
        
        if filter != ""{
            if from == Constants.PICKPAL_PUNTOS{
                for data in mListEstablishments{
                    if data.establishment_name.lowercased().hasPrefix(filter.lowercased()){
                        
                        items.append(data.state + " - " + data.establishment_name)
                        ids.append(data.establishment_id)
                        itemsAux2.append(data.place)
                        self.value = items
                        self.valueAux2 = itemsAux2
                        self.ValueIds = ids
                        self.counter = 0
                        self.tableView.reloadData()
                        
                    }
                }
            }else{
                handler = ref.observe(.childAdded, with: { (snapshot) -> Void in
                    if !(snapshot.value is NSNull) {
                        if let values = (snapshot.value as! NSDictionary).allKeys as? [String] {
                            //                        print(values)
                            for value in values {
                                if let val = (snapshot.value as! NSDictionary).value(forKey: value) as? NSDictionary {
                                    //                                print(val)
                                    if !self.from.isEmpty {
                                        if let kitchenArray = val.value(forKey: "category") as? NSObject {
                                            kitchenTypes = kitchenArray as! String
                                            print(kitchenTypes)
                                        }
                                    }else{
                                        if let kitchenArray = val.value(forKey: (self.from.isEmpty) ?  "business_line_name" : "category") as? String {
                                         //   kitchenTypes = kitchenArray.object(at: 0) as! String
                                            kitchenTypes = kitchenArray
                                            print("kitchenTypes ",kitchenTypes)
                                            
                                        }
                                    }
                                    
                                    let name = val.value(forKey: "name") as! String
                                    print(name)
                                    if name.lowercased().hasPrefix(filter.lowercased()) || kitchenTypes.lowercased().hasPrefix(filter.lowercased()){
                                        self.ValueIds.removeAll()
                                        print(snapshot)
                                        items.append("\(((snapshot.value as! NSDictionary).value(forKey: value) as! NSDictionary).value(forKey: "name") as! String)")
                                        switch self.from {
                                        case "adv":
                                            itemsAux2.append("\(((snapshot.value as! NSDictionary).value(forKey: value) as! NSDictionary).value(forKey: "subcategory") as! String)")
                                            break
                                        case "guide":
                                            itemsAux2.append("\(((snapshot.value as! NSDictionary).value(forKey: value) as! NSDictionary).value(forKey: "business_line_name") as! String)")
                                            break
                                            
                                        default:
                                            itemsAux2.append("\(((snapshot.value as! NSDictionary).value(forKey: value) as! NSDictionary).value(forKey: "inside_name") as? String ?? "")")
                                            break
                                        }
                                        
                                        ids.append(((snapshot.value as! NSDictionary).value(forKey: value) as! NSDictionary).value(forKey: "id") as! Int)
                                        self.value = items
                                        self.valueAux2 = itemsAux2
                                        self.ValueIds = ids
                                        self.counter = 0
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                        }
                    }
                })
            }
            if from != Constants.PICKPAL_PUNTOS{
                self.ValueIds.removeAll()
            }
            
            self.counter = 1
            self.noRelated = true
            self.tableView.reloadData()
        }else{
            ValueIds.removeAll()
            counter = 1
            noRelated = false
            self.tableView.reloadData()
        }
    }
}
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return value.count + counter
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if value.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellEmptySearch", for: indexPath) as! CellEmptySearchTableViewCell
            if noRelated{
                cell.logo.image = UIImage(named: "brandRed")
                cell.searchArrow.alpha = 0
                cell.label1.text = "No se encontraron resultados"
                cell.label2.isHidden = false
                cell.label2.text = from == Constants.PICKPAL_PUNTOS ? "Busca por nombre comercial" : "Intenta buscar por Nombre comercial o por Categoría de comida."
            }else{
                cell.logo.image = UIImage(named: "pickpal_logo_verde")
                cell.searchArrow.alpha = 1
                if from == Constants.PICKPAL_PUNTOS{
                        cell.label1.text = "¡Compra en los comercios afiliados, gana puntos móviles y redimelos, transfiérelos o intercambialos!"
                    cell.label2.isHidden = true
                }else{
                    if isFromPromos{
                        cell.label1.text = "¡Encuentra tu comercio, aprovecha la promoción y ahorra dinero!"
                    }else{
                        
                        if from == "guide"{
                            
                            cell.label1.text = "Encuentra tu comercio y conoce su información. Si están afiliados a Carta Digital Plus podrás conocer de antemano los productos y precios de lo que vende."
                            
                        }else{
                            
                            cell.label1.text = "¡Encuentra tu comercio favorito, pide y paga más rápido sin hacer filas o esperar a que te atiendan!"
                            
                        }
                       
                    }
                    cell.label2.text = "Este es el camino correcto"
                }
                
            }
            cell.isUserInteractionEnabled = false
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "filterAdvs", for: indexPath) as! CellFilterAdvsTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.selectionStyle = .none
            if from == Constants.PICKPAL_PUNTOS{
                if indexPath.row < value.count{
                    cell.lblTitle.text = value[indexPath.row]
                    cell.lblSubtitle.text = valueAux2[indexPath.row]
                }else{
                    cell.isHidden = true
                }
            }else{
                cell.lblTitle.text = "\(state) - \(value[indexPath.row])"
                cell.lblSubtitle.text = valueAux2[indexPath.row]
            }
            
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ref != nil {
            ref.removeObserver(withHandle: handler)
        }
        delegate.selectedSearchId(id: ValueIds[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
}
