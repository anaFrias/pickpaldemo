//
//  SurveyViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 13/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class SurveyViewController: UIViewController, userDelegate, ordersDelegate {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var stablishment_name: UILabel!
    
    var userws = UserWS()
    var orderWs = OrdersWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var nombreEstablecimiento  = String()
    var order_id = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.cornerRadius = 10
        container.layer.masksToBounds = true
        img.layer.cornerRadius = img.frame.height / 2
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        userws.delegate = self
        orderWs.delegate = self
        if let first_name = UserDefaults.standard.object(forKey: "first_name") as? String {
            name.text = first_name
        }
        stablishment_name.text = nombreEstablecimiento
        userws.viewProfile(client_id: client_id)
    }
    @IBAction func notNow(_ sender: Any) {
//        UserDefaults.standard.removeObject(forKey: "statusItem")
//        UserDefaults.standard.removeObject(forKey: "order_id")
//        UserDefaults.standard.removeSuite(named: "stb_name")
        orderWs.SkipEvaluation(order_id: order_id)
        self.dismiss(animated: true, completion: nil)
    }
    func didSuccessSkipEvaluation() {
        self.dismiss(animated: true, completion: nil)
    }
    func didFailSkipEvaluation(error: String, subtitle: String) {
        self.dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSuccessViewProfile(profile: UserProfile) {
        if profile.avatar != nil && profile.avatar != ""{
            Nuke.loadImage(with: URL(string: profile.avatar!)!, into: img)
        }
    }
    func didFailViewProfile(title: String, subtitle: String) {
        print(title)
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func contestar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurvey") as! SatisfactionSurvey1ViewController
        
        newVC.modalPresentationStyle = .fullScreen
        newVC.order_id = order_id
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        navController.modalPresentationStyle = .fullScreen
        //                    navController.modalPresentationStyle = .custom
        self.present(navController, animated: true, completion: nil)

        
        /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurvey") as! SatisfactionSurvey1ViewController
        newVC.order_id = order_id
        let navController = UINavigationController(rootViewController: newVC)
        newVC.modalPresentationStyle = .fullScreen
        navController.modalTransitionStyle = .crossDissolve
        self.present(navController, animated: true, completion: nil)*/
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
