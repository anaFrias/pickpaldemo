//
//  ItemSectionCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 17/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ItemSectionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var currentItem: UIView!
    @IBOutlet weak var nameCategory: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
