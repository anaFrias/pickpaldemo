//
//  LOADINGOVERViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/24/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class LOADINGOVERViewController: UIViewController {

    @IBOutlet weak var gifImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       gifImage.loadGif(name: "WindowsLoading")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
            self.dismiss(animated: true, completion: {
            })
        })
        // Do any additional setup after loading the view.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
