//
//  CancelCart2ViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/10/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol cancelCartV2Delegate {
    func seeRefund(index: IndexPath, order_id: Int)
}

class CancelCart2ViewController: UIViewController {

    
    @IBOutlet weak var losentimosLbl: UILabel!
    @IBOutlet weak var viewOwn: UIView!
    
    var nameEstablishment = String()
    var index = IndexPath()
    var delegate: cancelCartV2Delegate!
    var order_id = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOwn.layer.cornerRadius = 10
        losentimosLbl.text = "Lo sentimos, algunos de los ingredientes de los platillos y/o bebidas que ordenaste de \(nameEstablishment) están agotados."
        // Do any additional setup after loading the view.
    }

    @IBAction func viewDetail(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.seeRefund(index: index, order_id: self.order_id)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
