//
//  ProductosAgotadosTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/10/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class ProductosAgotadosTableViewCell: UITableViewCell {

    @IBOutlet weak var nameModifier: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var producto: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
