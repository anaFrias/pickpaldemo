//
//  CancelCartViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/10/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol cancelCartCardDelegate {
    func seeRefund(index: IndexPath, order_id: Int)
    func goToEstablishment(id: Int,name: String)
    func gotoHome()
}

class CancelCartViewController: UIViewController {
//nombre de ir a...
    
    @IBOutlet weak var viewCancelado: UIView!
    @IBOutlet weak var establishmentName: UIButton!
    @IBOutlet weak var sorryLabel: UILabel!
    
    var nameEstablishment = String()
    var idEstablishment = Int()
    var order_id = Int()
    var delegate: cancelCartCardDelegate!
    var index = IndexPath()
    override func viewDidLoad() {
        super.viewDidLoad()

        viewCancelado.layer.cornerRadius = 10
        sorryLabel.text = "Lo sentimos algunos de los ingredientes de los platillos y/o bebidas que ordenaste de \(nameEstablishment) están agotados."
//        establishmentName.setTitle("Ir a \(nameEstablishment)", for: .normal)
        // Do any additional setup after loading the view.
    }

    @IBAction func refundDetail(_ sender: Any) {
        self.delegate.seeRefund(index: index, order_id: self.order_id)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func seeNearbyStb(_ sender: Any) {
        self.delegate.goToEstablishment(id: idEstablishment, name: nameEstablishment)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func gotoEstablishment(_ sender: Any) {
        self.delegate.gotoHome()
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
