//
//  DeleteCartViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/10/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol deleteCarritoDelegate {
    func deleteCarrito(showView: Bool, stb_id: Int, municipio: String, categoria: String, lugar: String, estbName: String, stayInCart: Bool)
}

class DeleteCartViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var viewELiminar: UIView!
    @IBOutlet weak var tuCarritoSera: UILabel!
    
    var textString = String()
    var titleString = String()
    var delegate: deleteCarritoDelegate!
    var idProduct = [Int]()
    var isComplement = false
    var establishment_id = Int()
    var municipio = String()
    var categoria = String()
    var lugar = String()
    var estName = String()
    var showView = Bool()
    var stayInCart = Bool()
    var isCombo = [Bool]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewELiminar.layer.cornerRadius = 10
        label.text = textString
        tuCarritoSera.text = titleString
        let order_elements = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        let encoded = order_elements.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
//        print(dictionary as? [Dictionary<String,Any>])
        if isComplement {
            if let order = dictionary as? [Dictionary<String,Any>] {
                var ord = order
                for (n, orden) in ord.enumerated() {
                    var index = Int()
                    for (ind, id) in self.idProduct.enumerated() {
                        if let complements = orden["complements"] as? NSDictionary {
                            if let comp_id = complements.value(forKey: "complement_id") as? NSArray {
                                print(comp_id)
                                if comp_id.contains(id) {
                                    let item_id = orden["id"] as! Int
                                    index = ord.index(where: {dict -> Bool in
                                        return dict.contains(where: {_ in dict["id"] as! Int == item_id})
                                    }) ?? 0
                                }
                            }
                        }else{
                            index = 100000000
                        }
                        if let modifiers = orden["modifiers"] as? NSArray {
                            for modifier in modifiers {
                                if let modif_id = (modifier as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                                    print(modif_id)
                                    if modif_id.contains(id) {
                                        let item_id = orden["id"] as! Int
                                        print(item_id)
                                        //
                                        index = ord.index(where: {dict -> Bool in
                                            return dict.contains(where: {_ in dict["id"] as! Int == item_id})
                                        }) ?? 0
                                    }
                                }
                            }
                        }else{
                            if index != 100000000 {
                                index = 100000000
                            }
                        }
                    }
                    print(ord, index)
                    if index != 100000000 {
                        if index != nil {
                             ord.remove(at: index)
                        }
                       
                    }
                    if ord.count > 0 {
                        //                    Insertar de nuevo -_-
                        let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                        let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                        DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: ord, establishment_id: establishmentId, notes: notes)
                        tuCarritoSera.text = "Tu carrito será actualizado"
                        stayInCart = true
                    }else {
                        DatabaseFunctions.shared.deleteItem()
                        tuCarritoSera.text = "Tu carrito será eliminado"
                        stayInCart = false
                        
                    }

                }
                
            }
        }else{
            if let order = dictionary as? [Dictionary<String,Any>] {
                var ord = order
                for (ind, id) in self.idProduct.enumerated() {
                    var index = Int()
                    if isCombo[ind] {
                        index = ord.index(where: {dict -> Bool in
                            return dict.contains(where: {_ in dict["id"] as! Int == id && dict["is_package"] as! Bool})
                        }) ?? 0
                    }else{
                        index = ord.index(where: {dict -> Bool in
                            return dict.contains(where: {_ in dict["id"] as! Int == id})
                        }) ?? 0
                    }
                    
                    
                    if index != nil {
                        ord.remove(at: index)
                    }
                    if ord.count > 0 {
                        //                    Insertar de nuevo -_-
                        let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                        let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                        DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: ord, establishment_id: establishmentId, notes: notes)
                        tuCarritoSera.text = "Tu carrito será actualizado"
                        stayInCart = true
                    }else {
                        DatabaseFunctions.shared.deleteItem()
                        tuCarritoSera.text = "Tu carrito será eliminado"
                        stayInCart = false
                        
                    }
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func dismissDropCart(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        self.delegate.deleteCarrito(showView: self.showView, stb_id: self.establishment_id, municipio: self.municipio, categoria: self.categoria, lugar: self.lugar, estbName: self.estName, stayInCart: self.stayInCart)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
