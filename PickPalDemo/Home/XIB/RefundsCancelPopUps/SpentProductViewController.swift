//
//  SpentProductViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/10/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol sentToDelegate {
    func sentCarrito(idEstablishment: Int, estbName: String, category: String, place: String, munic: String)
    func sentSingleEstablishment(idEstablishment: Int, estbName: String, category: String, place: String, munic: String)
}

class SpentProductViewController: UIViewController {

    @IBOutlet weak var viewNoProcesado: UIView!
    @IBOutlet weak var tableView: UITableView!
    var nameProducts = [String]()
    var ids = [Int]()
    var establishmentName = String()
    var delegate: sentToDelegate!
    var cat = String()
    var mun = String()
    var pla = String()
    var noItems = false
    var estalishmentId = Int()
    var isComplement = false
    var idItems = [Int]()
    var categories = [String]()
    var nameItem = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "headerProcesadoTableViewCell", bundle: nil), forCellReuseIdentifier: "titleHeader")
        tableView.register(UINib(nibName: "titleAgotadosTableViewCell", bundle: nil), forCellReuseIdentifier: "titleAgotados")
        tableView.register(UINib(nibName: "ProductosAgotadosTableViewCell", bundle: nil), forCellReuseIdentifier: "productosCell")
        viewNoProcesado.layer.cornerRadius = 10
        print(ids, nameProducts)
        let order_elements = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        let encoded = order_elements.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
        if let order = dictionary as? [Dictionary<String,Any>] {
            var ord = [Dictionary<String,Any>]()
            ord = order
            if isComplement {
                for orden in ord {
                    var index = Int()
                    let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    for (i, id) in self.ids.enumerated() {
                        print(id)
                        self.establishmentName = establishmentName
                        self.estalishmentId = establishmentId
                        if let complements = orden["complements"] as? NSDictionary {
                            if let comp_id = complements.value(forKey: "complement_id") as? NSArray {
                                print(comp_id)
                                if comp_id.contains(id) {
                                    let item_id = orden["id"] as! Int
                                    index = ord.index(where: {dict -> Bool in
                                        return dict.contains(where: {_ in dict["id"] as! Int == item_id})
                                    }) ?? 0
                                }
                            }
                        }else{
                            index = 100000000
                        }
                        
                        if let modifiers = orden["modifiers"] as? NSArray {
                            for modifier in modifiers {
                                if let modif_id = (modifier as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                                    print(modifier)
                                    if modif_id.contains(id) {
                                        let item_id = orden["id"] as! Int
                                        print(item_id)
                                        categories.append((modifier as! NSDictionary).value(forKey: "category") as! String)
                                        index = ord.index(where: {dict -> Bool in
                                            return dict.contains(where: {_ in dict["id"] as! Int == item_id})
                                        }) ?? 0
                                        
                                    }
                                }
                            }
                            print(orden)
                            if idItems.count > 0 {
                                if orden["id"] as! Int == idItems[i] {
                                    nameItem.append(orden["name"] as! String)
                                    
                                }
                            }
                            
                        }else{
                            if index != 100000000 {
                                index = 100000000
                            }
                        }
                    }
                    
                    if index != 100000000 {
                        if index != nil {
                            ord.remove(at: index)
                        }
                    }
                    if ord.count > 0 {
                        //                    Insertar de nuevo -_-
                        let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                        DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: ord, establishment_id: establishmentId, notes: notes)
                        self.noItems = false
                    }else {
                        DatabaseFunctions.shared.deleteItem()
                        self.noItems = true
                    }
                    print(categories, nameItem)
                }
                
            }else{
                for id in self.ids {
                    print(id)
                    let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    self.establishmentName = establishmentName
                    self.estalishmentId = establishmentId
                    
                    let index = ord.index(where: {dict -> Bool in
                        return dict.contains(where: {_ in dict["id"] as! Int == id})
                    })
                    if index != nil {
                        ord.remove(at: index!)
                    }
                    if ord.count > 0 {
                        //                    Insertar de nuevo -_-
                        let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                        DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: ord, establishment_id: establishmentId, notes: notes)
                        self.noItems = false
                    }else {
                        DatabaseFunctions.shared.deleteItem()
                        self.noItems = true
                    }
                }
            }
            
        }
        // Do any additional setup after loading the view.
    }


    @IBAction func addProducts(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.sentSingleEstablishment(idEstablishment: self.estalishmentId, estbName: self.establishmentName, category: cat, place: pla, munic: mun)
    }
    @IBAction func verCarrito(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if noItems {
            self.delegate.sentSingleEstablishment(idEstablishment: self.estalishmentId, estbName: self.establishmentName, category: cat, place: pla, munic: mun)
        }else{
            self.delegate.sentCarrito(idEstablishment: self.estalishmentId, estbName: self.establishmentName, category: cat, place: pla, munic: mun)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
extension SpentProductViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + nameProducts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleHeader", for: indexPath) as! headerProcesadoTableViewCell
            if isComplement {
                cell.titleStb.text = "A \(establishmentName) se le han agotado algunos ingredientes de los siguientes platillos"
            }else{
                cell.titleStb.text = "A \(establishmentName) se le han agotado algunos platillos y/o bebidas"
            }
            
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleAgotados", for: indexPath)
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "productosCell", for: indexPath) as! ProductosAgotadosTableViewCell
            cell.producto.text = nameItem[indexPath.row - 2]
            if idItems.count > 0 {
                cell.categoryLbl.text = categories[indexPath.row - 2] + ": " + nameProducts[indexPath.row - 2]
//                cell.nameModifier.text = nameProducts[indexPath.row - 2]
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 183
        }else if indexPath.row == 1{
            return 32.5
        }else{
            if idItems.count > 0 {
                return 50
            }else{
                return 34
            }
        }
    }
}
