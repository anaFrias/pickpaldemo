//
//  PedidoCanceladoViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/23/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class PedidoCanceladoViewController: UIViewController {

    @IBOutlet weak var haAgotadoLbl: UILabel!
    var textLabel = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        haAgotadoLbl.text = textLabel
        // Do any additional setup after loading the view.
    }


    @IBAction func aceptarBt(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
