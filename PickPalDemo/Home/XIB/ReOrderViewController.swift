//
//  ReOrderViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/30/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Firebase
protocol reorderDelegate {
    func reorder(order_id: Int, is_canceled: Bool)
}

class ReOrderViewController: UIViewController {
    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var pedidoDetailLabel: UILabel!
    
    @IBOutlet weak var nombreComercioLabel: UILabel!
    
    @IBOutlet weak var pedidoDetailText: UITextView!
    var order_id = Int()
    var establishment_id = String()
    var register_date = String()
    var order_elements = [Dictionary<String,Any>]()
    var nombreComercio = String()
    var delegate: reorderDelegate!
    var reference: DatabaseReference!
    var handler: DatabaseHandle!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainer.layer.cornerRadius = 5
        let reference = Database.database().reference().child("orders").child(establishment_id).child(register_date).child("-" + order_id.description)
       handler = reference.observe(.value) { (snapshot) in
            if !(snapshot.value is NSNull) {
                let valueOrder = (snapshot.value as! NSDictionary)
                if valueOrder.value(forKey: "products") != nil {
                    self.order_elements = self.convertValueDictionary(products: valueOrder.value(forKey: "products") as! NSArray)
                }
                var string = String()
                var i = 0
                for order in self.order_elements {
                    if i < 4 {
                        let cant = (order["quantity"] as! Int)
                        let name = order["name"] as! String
                        let str = "\(cant) " + name + "\r\n"
                        string.append(str)
                    }
                    i += 1
                }
                self.pedidoDetailText.text = string
            }
        }
//        order_elements = DatabaseFunctions.shared.getSinglePedido(order_id: order_id)
        
        nombreComercioLabel.text = nombreComercio
        // Do any additional setup after loading the view.
    }
    func convertValueDictionary (products: NSArray) -> [Dictionary<String,Any>] {
        var dictionaryInfo = [Dictionary<String,Any>]()
        var dictionaryModifiersTotal = [Dictionary<String,Any>]()
        var dictionaryComplementsTotal = [Dictionary<String,Any>]()
        var i = 0
        var y = 0
        var totalPrice = 0.00
        for product in products {
            //                        print(product)
            var modifiers = NSArray()
            var modifiersS = String()
            var modifiers_price = NSArray()
            var modifiers_priceS = String()
            var modifiers_id = NSArray()
            var modifiers_idS = String()
            var modifiers_cat = NSArray()
            var modifiers_catS = String()
            
            var complements = NSArray()
            var complementsS = String()
            var complements_price = NSArray()
            var complement_priceS = String()
            var complements_id = NSArray()
            var complements_idS = String()
            var complements_cat = NSArray()
            var complements_catS = String()
            
            if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                modifiers = mod
            }else{
                modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
            }
            
            if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                modifiers_price = mod_price
            }else{
                modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
            }
            
            if let mod_id = (product as! NSDictionary).value(forKey: "modifier_ids") as? NSArray {
                modifiers_id = mod_id
            }else{
                modifiers_idS = (product as! NSDictionary).value(forKey: "modifier_ids") as! String
            }
            
            if let mod_cat = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                modifiers_cat = mod_cat
            }else{
                modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
            }
            
            if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                complements = comp
            }else{
                complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
            }
            
            if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                complements_price = comp_price
            }else{
                complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
            }
            
            if let comp_id = (product as! NSDictionary).value(forKey: "complement_ids") as? NSArray {
                complements_id = comp_id
            }else{
                complements_idS = (product as! NSDictionary).value(forKey: "complement_ids") as! String
            }
            
            if let comp_cat = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                complements_cat = comp_cat
            }else{
                complements_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
            }
            
            if modifiers.count > 0 || complements.count > 0 {
                var modifP = [Double]()
                var modifId = [Int]()
                var modifCat = [String]()
                var compP = [Double]()
                var compN = [String]()
                var compC = [Int]()
                var compId = [Int]()
                var compCat = [String]()
                for j in 0..<modifiers.count {
                    modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                    modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
                    modifCat.append(modifiers_cat.object(at: j) as! String)
                }
                var aux = 1
                var index = -1
                for z in 0..<complements.count {
                    if !compN.contains(complements[z] as! String) {
                        compN.append(complements[z] as! String)
                        compC.append(1)
                        aux = 1
                        compP.append(Double(complements_price[z] as! String)!)
                        compId.append(Int(complements_id[z] as! String) ?? 0)
                        compCat.append(complements_cat[z] as! String)
                        index += 1
                    }else{
                        aux += 1
                        compC[index] = aux
                        compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                        compId[index] = (Int(complements_id[z] as! String) ?? 0)
                        compCat[index] = (complements_cat[z] as! String)
                    }
                }
                
                let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_id": modifId, "modifier_category":modifCat] as [String : Any]
                dictionaryModifiersTotal.append(dictionaryModifiers)
                let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_id": compId, "complement_category":compCat] as [String : Any]
                dictionaryComplementsTotal.append(dictionaryComplements)
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": true, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                dictionaryInfo.append(dictionary)
                y += 1
            }else{
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": false, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_id": complements_idS, "complement_category": complements_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_id": modifiers_idS, "modifier_category": modifiers_catS]] as [String : Any]
                dictionaryInfo.append(dictionary)
            }
            totalPrice += (product as! NSDictionary).value(forKey: "price") as! Double
            i += 1
        }
        return dictionaryInfo
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if reference != nil {
            reference.removeObserver(withHandle: handler)
        }
    }
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reorderSction(_ sender: Any) {
        self.delegate.reorder(order_id: self.order_id, is_canceled: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noAction(_ sender: Any) {
        self.delegate.reorder(order_id: self.order_id, is_canceled: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
