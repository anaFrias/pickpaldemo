//
//  SurveyThanksViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 17/08/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class SurveyThanksViewController: UIViewController, userDelegate, ordersDelegate {

    @IBOutlet weak var container: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var order_id = Int()
    
    var userws = UserWS()
    var orderWs = OrdersWS()
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.cornerRadius = 10
        container.layer.masksToBounds = true
        img.layer.cornerRadius = img.frame.height / 2
        img.clipsToBounds = true
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        userws.delegate = self
        orderWs.delegate = self
        if let first_name = UserDefaults.standard.object(forKey: "first_name") as? String {
            name.text = first_name
        }
        userws.viewProfile(client_id: client_id)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func didSuccessViewProfile(profile: UserProfile) {
        if profile.avatar != nil && profile.avatar != ""{
            Nuke.loadImage(with: URL(string: profile.avatar!)!, into: img)
        }
    }
    func didFailViewProfile(title: String, subtitle: String) {
        print(title)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSuccessSkipEvaluation() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        
        newVC.modalPresentationStyle = .fullScreen
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        navController.modalPresentationStyle = .fullScreen
        //                    navController.modalPresentationStyle = .custom
        self.present(navController, animated: true, completion: nil)
    }
    func didFailSkipEvaluation(error: String, subtitle: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        
        newVC.modalPresentationStyle = .fullScreen
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        navController.modalPresentationStyle = .fullScreen
        //                    navController.modalPresentationStyle = .custom
        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func ready(_ sender: Any) {
        orderWs.SkipEvaluation(order_id: order_id)
        orderWs.SkipEvaluation(order_id: order_id)
    }
}
