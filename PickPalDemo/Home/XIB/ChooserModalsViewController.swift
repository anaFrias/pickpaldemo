
//
//  ChooserModalsViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 04/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import SQLite
import Firebase

protocol setValuesDelegate {
    func setValue(value: String, place: Bool, category: Bool, state: String, isType: Bool, idBussinessLine: String)
    func closeModal()
}
class ChooserModalsViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageBlured: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var value = [String]()
    var valueIds = [Int]()
    var valueIdsInside = [Int]()
    var valueStates = [String]()
    var states = false
    var titleString = String()
    var typeOrdersList = [String]()
    var delegate: setValuesDelegate!
    var places = Bool()
    var categories = Bool()
    var isTypeOrders = Bool()
    var municipio: String!
    var categoria: String!
    var idBussinessLine: String!
    var refLocations: DatabaseReference!
    var refLocations2: DatabaseReference!
    var refCategories: DatabaseReference!
    var refInsides: DatabaseQuery!
    var state = String()
    var from = String()
    
    var ubicationoptions = [String : Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refLocations = Database.database().reference().child("locations" + (from.isEmpty ? "" : from))
        if from == "guide"{
            refCategories = Database.database().reference().child("businesslinesguide")
        }else{
//            refCategories = Database.database().reference().child("categories" + (from.isEmpty ? "2" : from+"s"))
            refCategories = Database.database().reference().child("businesslines")
        }
        
        if from == "adv"{
            
            refCategories = Database.database().reference().child("categoriesadvs")
            
        }
        
        print("se viene "+from)
        print("Hola \(state) \(municipio!)")
        if isTypeOrders{
            self.value = typeOrdersList
            self.tableView.reloadData()
        }else if places {
            refLocations.observe(.value, with: { (snapshot) -> Void in
                if !(snapshot.value is NSNull) {
                    print("****",snapshot.value as? NSDictionary)
                    if let all = (snapshot.value as! NSDictionary).value(forKey: "all") as? NSArray  {
                        var states = [String]()
                        var ctis = [String]()
                        for item in all {
                            if !(item is NSNull) {
                                var city_2 = String()
                                var code = String()
                                if let city = (item as! NSDictionary).value(forKey: "name") as? String {
                                    city_2 = city
                                }
                                if let cti = (item as! NSDictionary).value(forKey: "code") as? String {
                                    code = cti
                                }
                                states.append(city_2)
                                ctis.append(code)
                            }
                            
                        }
                        let sorted = states.sorted()
                        let sortedCities = ctis.sorted()
                        self.value = sorted
                        self.valueStates = sortedCities
                        self.states = true
                        self.tableView.reloadData()
                    }else{
                        if let all2 = (snapshot.value as! NSDictionary).value(forKey: "all") as? NSDictionary {
                            if let values = all2.allValues as? NSArray {
                                print(values)
                                var states = [String]()
                                var ctis = [String]()
                                for item in values {
                                    if !(item is NSNull) {
                                        states.append((item as! NSDictionary).value(forKey: "name") as! String)
                                        ctis.append((item as! NSDictionary).value(forKey: "code") as! String)
                                    }
                                }
                                
                                let sorted = states.sorted()
                                let sortedCities = ctis.sorted()
                                self.value = sorted
                                self.valueStates = sortedCities
                                self.states = true
                                self.tableView.reloadData()
                                
                            }
                        }
                    }
                    
                }
            })
        }else{
            if categories {
                if municipio != "", municipio != nil {
                    //  valueIds.removeAll()
                    refCategories.observe(.value, with: { (snapshot) -> Void in
                        if !(snapshot.value is NSNull) {
                            if let value = (snapshot.value as! NSDictionary).value(forKey: self.state) as? NSDictionary {
                                if let municipalty = value.value(forKey: self.municipio!)  {
                                    switch self.from {
                                    case "adv":
                                        let sortCat = (municipalty as! [String]).removingDuplicates().sorted()
                                        self.value = sortCat
                                        break
                                        
                                        
                                    case "guide":
                                        print("Valor: ",municipalty )
                                        
                                       
//                                        var array = [String]()
//                                        for value in (municipalty as? NSArray)! {
//                                            array += value as! [String]
//                                        }
//                                        print(array.removingDuplicates())
//                                        let sortCat = array.removingDuplicates().sorted()
                                        self.value = municipalty as! [String]
//                                        self.value = ["Necesito" , "Decirte", "Que te estare", "Esperando", "Por si existe", "Amor"]
                                        
                                        self.value = self.value.sorted { $0.description < $1.description }
                                        
                                        
                                        
                                        break
                                    default:
                                        let sortCat = (municipalty as! NSArray)
                                        for i in 0..<sortCat.count{
                                            let value = sortCat[i]
                                            self.value.append((value as! NSDictionary).value(forKey: "name") as! String)
                                            self.valueIds.append((value as! NSDictionary).value(forKey: "id") as! Int)
                                            print("refCategories: ", self.refCategories)
//                                            self.value.append(sortCat[i] as! String)
//                                            self.valueIds.append(i as! Int)
                                        }
                                    }
                                }else{
                                    var array = [String]()
                                    for value in (value.allValues as? NSArray)! {
                                        array += value as! [String]
                                    }
                                    print(array.removingDuplicates())
                                    let sortCat = array.removingDuplicates().sorted()
                                    self.value = sortCat
                                }
                                
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            }else{
                if municipio != "", municipio != nil, categoria != "Categoría", categoria != nil {
                    refInsides = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs")).child(state).child(municipio.replacingOccurrences(of: ".", with: "")).queryOrdered(byChild: "category")
                    var places = [String]()
                    if self.categoria == "Categoría" {
                        refInsides?.observe(.childAdded, with: { (snapshot) -> Void in
                            if !(snapshot.value is NSNull) {
                                places.append((snapshot.value as! NSDictionary).value(forKey: "name") as! String)
                                let sortPlaces = places.sorted()
                                self.value = sortPlaces
                                self.tableView.reloadData()
                            }
                        })
                    }else{
                        let refInsd: DatabaseQuery!
                        switch self.from {
                        case "adv":
                            
                            refInsd = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs")).child(state).child(municipio.replacingOccurrences(of: ".", with: "")).queryOrdered(byChild: "category").queryEqual(toValue: self.categoria)
                            break
                        default:
                               
                            if idBussinessLine.isEmpty{
                                
                                refInsd = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs")).child(state).child(municipio.replacingOccurrences(of: ".", with: ""))
                                
                            }else{
                                
                                refInsd = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs")).child(state).child(municipio.replacingOccurrences(of: ".", with: "")).child("-\(String(describing: idBussinessLine!))")//.queryOrdered(byChild: "business_line_name").queryEqual(toValue: self.categoria.elementsEqual("Negocio") ? "Restaurantes":self.categoria)
                            }
                            
                            
                            ///FUNCIONA
                            
                            
                            
                            //   refInsd = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs")).child(state).child(municipio.replacingOccurrences(of: ".", with: "")).child("-\(String(describing: idBussinessLine.isEmpty ? "1":idBussinessLine))").queryOrdered(byChild: "business_line_name").queryEqual(toValue: self.categoria.elementsEqual("Negocio") ? "Restaurantes":self.categoria )
                            break
                        }
                        
                        print("refInsd: ",refInsd)
                        
                        if idBussinessLine.isEmpty{
                            
                            refInsd.observe(.childAdded, with: { (snapshot) -> Void in
                                if !(snapshot.value is NSNull) {
                                    if let values = (snapshot.value as? NSArray){
                                    
                                    print("Values ",values)
                                    var index = 0
                                    for value in values {
                                      
                                        let val = (snapshot.value as! NSArray)
                                        let nameLocation = val.value(forKey: "name") as! [String]
                                         let idLocation = val.value(forKey: "id") as! [Int]
                                        print("nameLocation ",nameLocation)
                                        self.ubicationoptions[nameLocation[index]] = idLocation [index]
                                        
                                        if !places.contains(nameLocation[index] as! String){
                                        self.valueIdsInside.append(idLocation [index])
                                        places.append(nameLocation [index])
                                            
                                        }
                                        index += 1
                                        
                                    }
                                    
                                    let sortPlaces = places.sorted()
                                    self.value = sortPlaces
                                    self.tableView.reloadData()
                                    
                                    
                                }
                                }
                                
                            })
                            
                        }else{
                            
                            refInsd.observe(.childAdded, with: {(snapshot) -> Void in
                                if !(snapshot.value is NSNull) {
                                    
                                    places.append((snapshot.value as! NSDictionary).value(forKey: "name") as! String)
                                    
                                    
                                    
                                    if self.from != "adv"{
                                        self.valueIdsInside.append((snapshot.value as! NSDictionary).value(forKey: "id") as! Int)
                                        self.ubicationoptions[(snapshot.value as! NSDictionary).value(forKey: "name") as! String] = (snapshot.value as! NSDictionary).value(forKey: "id") as! Int
                                        
                                       
                                    }
                                    let sortPlaces = places.sorted()
                                    self.value = sortPlaces
                                    
                                   
                                    self.tableView.reloadData()
                                }else{
                                    
                                    print("Esta vacio")
                                    
                                }
                            })
                            
                            
                        }
                    }
                }else{
                    if state != "", state != nil {
                        //                    EN BASE A EL ESTADO Y MUNICIPIO
                        refInsides = Database.database().reference().child((from.isEmpty ? "insides" : "subcatadvs"))
                        refInsides.observe(.value, with: {(snapshot) -> Void in
                            if !(snapshot.value is NSNull) {
                                if let state = (snapshot.value as! NSDictionary).value(forKey: self.state) as? NSDictionary {
                                    if let municipalty = state.value(forKey: self.municipio!) as? NSDictionary {
                                        var places = [String]()
                                        for (index, value) in (municipalty.allValues as! NSArray).enumerated() {
                                            print(value)
                                            places.append((value as! NSDictionary).value(forKey: "name") as! String)
                                        }
                                        print(places)
                                        let sortPlaces = places.removingDuplicates().sorted()
                                        self.value = sortPlaces
                                        self.tableView.reloadData()
                                    }else{
                                        var keys = [String]()
                                        var places = [String]()
                                        print((state.allValues as! NSArray) .count)
                                        for (index, value) in (state.allValues as! NSArray).enumerated() {
                                            keys += (value as! NSDictionary).allKeys as! [String]
                                            print(keys)
                                            let values = ((state as! NSDictionary).allValues as? NSArray)?.object(at: 0) as! NSDictionary
                                            print(values)
                                            let namePlaces = (values.allValues as! NSArray)
                                            for (i, val) in namePlaces.enumerated() {
                                                places.append((val as! NSDictionary).value(forKey: "name") as! String)
                                            }
                                        }
                                        let sortPlaces = places.removingDuplicates().sorted()
                                        self.value = sortPlaces
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                        })
                    }
                }
            }
            
        }
        
        titleLabel.text = titleString
        
        let lightBlur = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: lightBlur)
        blurEffectView.frame = self.imageBlured.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.imageBlured.addSubview(blurEffectView)
    }
    
    func setValues (state: String) {
        self.tableView.contentOffset.y = 0.0
        titleLabel.text = "Municipio / Delegación"
        refLocations.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                print(snapshot.value as! NSDictionary)
                if let singleState = (snapshot.value as! NSDictionary).value(forKey: state) as? NSArray {
                    var municipalty = [String]()
                    for item in singleState {
                        
                        if let city = item as? String {
                            municipalty.append(city)
                        }
                        
                    }
                    let sortCities = municipalty.sorted()
                    self.value = sortCities
                    self.states = false
                    
                    self.tableView.reloadData()
                }else{
                    if let singleState = (snapshot.value as! NSDictionary).value(forKey: state) as? NSDictionary {
                        if let municipalty = singleState.allValues as? [String] {
                            let sortCities = municipalty.sorted()
                            self.value = sortCities
                            self.states = false
                            self.tableView.reloadData()
                        }
                        
                    }
                }
            }
        })
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        //        refLocations.removeAllObservers()
        //        refCategories.removeAllObservers()
        //        refInsides.removeAllObservers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.closeModal()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}

extension ChooserModalsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return value.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = .none
        cell.textLabel?.text = value[indexPath.row]
        cell.textLabel?.font = UIFont(name: "Aller", size: 16)
        cell.textLabel?.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if states {
            let cell = tableView.cellForRow(at: indexPath)!
            cell.textLabel?.font = UIFont(name: "Aller-Black", size: 17)
            cell.setSelected(false, animated: true)
            print("Values :",value[indexPath.row])
            print("valueStates: ",valueStates[indexPath.row])
            self.setValues(state: valueStates[indexPath.row])
            self.state = valueStates[indexPath.row]
        }else{
            switch self.from {
            case "adv", "guide":
                var bussiness = "0"
                if valueIds.count > indexPath.row {
                    bussiness = String(valueIds[indexPath.row])
                }
                delegate.setValue(value: value[indexPath.row], place: places, category: categories, state: self.state, isType: isTypeOrders, idBussinessLine: bussiness)
                //delegate.setValue(value: value[indexPath.row], place: places, category: categories, state: self.state)
                break
            default:
                if categories || titleString == "Ubicación" {
                    
                
                
                    delegate.setValue(value: value[indexPath.row], place: places, category: categories, state: self.state, isType: isTypeOrders,
                                      idBussinessLine: categories ? String(valueIds[indexPath.row]) : String(ubicationoptions[value[indexPath.row]]!))//String(valueIdsInside[indexPath.row]))
                }else{
                    delegate.setValue(value: value[indexPath.row], place: places, category: categories, state: self.state, isType: isTypeOrders, idBussinessLine: "0")
                }
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

extension Dictionary where Value: Equatable {
    func allKeys(forValue val: Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}
