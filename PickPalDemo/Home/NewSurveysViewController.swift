//
//  NewSurveysViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/27/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class NewSurveysViewController: UIViewController, establishmentDelegate, buttonsProtocol, continueButtonDelegate {

    @IBOutlet weak var aspectLabel: UILabel!
    @IBOutlet weak var questionSurvey: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var superHappyBtn: UIButton!
    @IBOutlet weak var happierBtn: UIButton!
    @IBOutlet weak var happyBtn: UIButton!
    @IBOutlet weak var neutralBtn: UIButton!
    @IBOutlet weak var sadBtn: UIButton!
    @IBOutlet weak var superSadBtn: UIButton!
    
    var questionsArray = [Question]()
    var establishment_id = Int()
    var image = String()
    var order_id = Int()
    var counter = 0
    var categories = [Int]()
    var labels = [String]()
    var answers  = [Int]()
    
    var catAnswers = [Int]()
    var establishmentws = EstablishmentWS()
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var ans = [Int]()
    var cat = [Int]()
    var index = [0,1,0,2,0,3,0,4]
    var nota = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        establishmentws.delegate = self
        // Do any additional setup after loading the view.
        questionSurvey.text = questionsArray[1].question
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        Nuke.loadImage(with: URL(string: image)!, into: imageView)
        for i in 0..<questionsArray.count {
            if let cat = questionsArray[i].categories {
                categories = cat
            }
            if let lab = questionsArray[i].labels {
                labels = lab
            }
        }
    }
    
    @IBAction func omitir(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyNotes") as! SatisfactionSurveyNotesViewController
        newVC.questionsArray = questionsArray
        newVC.establishment_id = establishment_id
        newVC.order_id = order_id
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func buttonsInfo(value: Int, type: Int) {
        if ans.count < 4 {
            if !cat.contains(type) {
                ans.append(value)
                cat.append(type)
            }
        }
    }
    func continueButton() {
        
        if /*ans.count == 4*/ ans.count == labels.count {
            var dictionary = [Dictionary<String,Any>]()
            for (index, valor) in ans.enumerated() {
                let dic = ["category": valor, "value": cat[index]]
                dictionary.append(dic)
            }
//            print(dictionary)
            if let notaPickpal = UserDefaults.standard.object(forKey: "nota_comercio") as? String {
                establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary, notes: notaPickpal, order_id: order_id)
            }else{
                establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary, order_id: order_id)
            }
        }
    }
    
    @IBAction func agregarNota(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "NotaAdicional") as! NotaAdicionalViewController
        newVC.image = image
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func superSad(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(1)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
        
    }
    
    @IBAction func sad(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(2)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
    }
    
    @IBAction func neutral(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(3)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
    }
    
    @IBAction func happy(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(4)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
    }
    
    @IBAction func happier(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(5)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
    }
    
    @IBAction func superHappy(_ sender: Any) {
//        if counter < labels.count {
//            answers.append(6)
//            catAnswers.append(categories[counter])
//            aspectLabel.text = labels[counter]
//            counter += 1
//        }else{
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
//            newVC.questionsArray = questionsArray
//            newVC.establishment_id = establishment_id
//            newVC.imge = image
//            newVC.order_id = order_id
//            newVC.items = questionsArray[2].items
//            newVC.notes = true
//            self.navigationController?.pushViewController(newVC, animated: true)
//            let dictionary = ["category": catAnswers, "value": answers]
//
//            establishmentws.SetEvaluationCategories(client_id: client_id, establishment_id: establishment_id, evaluation_answer_id: questionsArray[1].id, data: dictionary)
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func didSuccessSetEvaluationCategories() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SatisfactionSurveyItems") as! SatisfactionSurveyItemsViewController
        newVC.questionsArray = questionsArray
        newVC.establishment_id = establishment_id
        newVC.imge = image
        newVC.order_id = order_id
        newVC.items = questionsArray[2].items
        newVC.notes = true
        UserDefaults.standard.removeObject(forKey: "nota_comercio")
        self.navigationController?.pushViewController(newVC, animated: true)

    }
    func didFailSetEvaluationCategories(error: String, subtitle: String) {
        print(error)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension NewSurveysViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return labels.count
        return (labels.count * 2) + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        if indexPath.row % 2 == 0 {
            if /*indexPath.row == 8*/ indexPath.row + 1 == (labels.count * 2) + 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "continueButton", for: indexPath) as! continueNewSurveyTableViewCell
                cell.delegate = self
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "surveyQuestions", for: indexPath) as! surveyAnswersTableViewCell
                
                if (indexPath.row/2) < labels.count{
                    cell.questionLbl.text = "\((indexPath.row/2) + 1).- " + labels[indexPath.row/2]
                }else{
                    cell.questionLbl.text = ""
                }
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "surveyAnswers", for: indexPath) as! surveyQuestionsTableViewCell
            cell.category = categories[indexPath.row - index[indexPath.row]]
            cell.delegate = self
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row % 2 == 0 {
            if /*indexPath.row == 8*/ indexPath.row + 1 == (labels.count * 2) + 1{
                return 74
            }else{
                return 29
            }
            
        }else{
            return 53
        }
    }
}
