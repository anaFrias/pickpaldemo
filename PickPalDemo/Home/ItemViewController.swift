//
//  ItemViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import Firebase

class ItemViewController: UIViewController, ordersDelegate {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var numberItems: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
//    Passin vars
    var orderws = OrdersWS()
    var complements = [Complements]()
    var modifiers = [Modifiers]()
    var itemNameString: String!
    var price: String!
    var cant: Int!
    var imageItem: String!
    var totalItems = [Int]()
    var dictionaryModifiers = Dictionary<String,Any>()
    var dictionaryComplements = Dictionary<String,Any>()
    var itemsInsert = [String]()
    var total = Int()
    var doublePrice = Double()
    var prics = [Double]()
    var cants = [Int]()
    var namss = [String]()
    var idEstablishment = Int()
    var idItem = Int()
    var descriptionText = String()
//    Helped arrays
    var selectedCells = [[Bool]]()
    var selected = [[Bool]]()
    var itemsPerExtras = [[Int]]()
    var itemsExtras = [[Int]]()
    var itms = Int()
    var selectedModifiers = [[Int]]()
    var selectedM = [[Bool]]()
    var requested = [[Bool]]()
    
    //modifiers/complements arrays
    var modificadores = [[String]]()
    var modificadores_precio = [[Double]]()
    
    var complementos = [[String]]()
    var complementos_precio = [[Double]]()
    var complementos_cant = [[Int]]()
    
//    Modifiers/complements
    var mod = [String]()
    var com = [String]()
    var mod_pri = [Double]()
    var com_pri = [Double]()
    var com_cant = [Int]()
    var mod_id = [Int]()
    
    var itemsAdded = [Int]()
    var defaults = UserDefaults.standard
    var totalsWithComplements = Double()
    var totalsWithModifiers = Double()
    var modifiersCount = Int()
    var complementsCount = Int()
    var dictionaryInfo = [Dictionary<String,Any>]()
    var dictionaryModifiersTotal = [Dictionary<String,Any>]()
    var dictionaryComplementsTotal = [Dictionary<String,Any>]()
    
    var dictionaryModifiersRequired = [Dictionary<String,Any>]()
    
    var initialArraysString = [[String]]()
    var initialArraysInt = [[Int]]()
    var initialArraysDouble = [[Double]]()
    var initialArraysIds = [[Int]]()
    
    var initialArraysModifString = [[String]]()
    var initialArraysModifDouble = [[Double]]()
    var initialArraysModifIds = [[Int]]()
    
    var modifiersSelection = [Int]()
    var dictionarySelections = [Int]()
    var dictionaryComplementSelections = [Int]()
    //    MARK: neededVariables
    var notes = String()
    var establishmentName = String()
    var isPackage = Bool()
    var limit_extra = Int()
    var extrasCounter = [[Int]]()
    
    var handle2: DatabaseHandle!
    var handle3: DatabaseHandle!
    
    var extrasObserve: DatabaseReference!
    var modifiersObserve: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       orderws.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        
        modifiersCount = modificadores.count
        complementsCount = complementos.count
        if imageItem != nil, imageItem != "" {
            Nuke.loadImage(with: URL(string: imageItem)!, into: itemImage)
        }

        itemName.text = itemNameString
        itemPrice.text = price + "0"
        numberItems.text = "\(cant!)"
        LoadingOverlay.shared.showOverlay(view: self.view)
        setArrays()
        setNumberOfItemsSelected(numItems: total)
//        orderws.viewSingleProduct(product_id: idItem, establishment_id: idEstablishment)
        inactivateExtrasObserve()
//        inactivateModifiersObserve()
        
    }
    func inactivateExtrasObserve() {
        extrasObserve = Database.database().reference().child("inactive_extras").child("-\(idEstablishment)")
        print(extrasObserve)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle2 = extrasObserve.observe(.value) { (snapshot) in
            self.inactivateModifiersObserve()

        }
    }
    func inactivateModifiersObserve() {
        modifiersObserve = Database.database().reference().child("inactive_modifiers").child("-\(idEstablishment)")
        print(modifiersObserve)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle3 = modifiersObserve.observe(.value) { (snapshot) in
            self.setArrays()
            self.totalsWithComplements = 0
            self.totalsWithModifiers = 0
            self.itemsAdded.append(1)
            self.itemsInsert.removeAll()
            self.modifiersSelection.removeAll()
            self.totalItems.removeAll()
            self.selectedCells.removeAll()
            self.requested.removeAll()
            self.selectedModifiers.removeAll()
            self.itemsPerExtras.removeAll()
            self.extrasCounter.removeAll()
            self.initialArraysString.removeAll()
            self.initialArraysInt.removeAll()
            self.initialArraysDouble.removeAll()
            self.initialArraysIds.removeAll()
            self.initialArraysModifString.removeAll()
            self.initialArraysModifDouble.removeAll()
            self.initialArraysModifIds.removeAll()
            self.startArrays(numItems: self.total)
            self.orderws.viewSingleProduct(product_id: self.idItem, establishment_id: self.idEstablishment)
            //            print(snapshot.value as! NSDictionary)
        }
    }
    func didSuccessViewSingleProduct(item: Items) {
        modifiers = item.modifiers!
        complements = item.complements!
//        modifiersCount = modificadores.count
//        complementsCount = complementos.count
//        if imageItem != nil, imageItem != "" {
//            Nuke.loadImage(with: URL(string: imageItem)!, into: itemImage)
//        }
//
//        itemName.text = itemNameString
//        itemPrice.text = price + "0"
//        numberItems.text = "\(cant!)"
//        LoadingOverlay.shared.showOverlay(view: self.view)
//        setArrays()
//        setNumberOfItemsSelected(numItems: total)
//        inactivateExtrasObserve()
        tableView.reloadData()
    }
    
    func didFailViewSingleProduct(error: String, subtitle: String) {
        print(error, subtitle)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if extrasObserve != nil {
            extrasObserve.removeObserver(withHandle: handle2)
        }
        if modifiersObserve != nil {
            modifiersObserve.removeObserver(withHandle: handle3)
        }
    }
    func setArrays() {
        if com.count > 0 {
            com.removeAll()
            com_pri.removeAll()
            com_cant.removeAll()
        }
        if complementos.count > 0  {
            complementos.removeAll()
            complementos_precio.removeAll()
            complementos_cant.removeAll()
        }
        
        if modificadores.count > 0 {
            modificadores.removeAll()
            modificadores_precio.removeAll()
        }
        
        for _ in 0..<(total + complementsCount) {
            complementos.append([])
            complementos_precio.append([])
            complementos_cant.append([])
        }
        
        for _ in 0..<(total + modifiersCount) {
            modificadores.append([])
            modificadores_precio.append([])
        }
        //        MARK: Se estaban inicializando estos arreglos
//        for i in 0..<modifiers.count {
//            for _ in 0..<modifiers[i].modifiers.count {
//                mod.append("")
//                mod_pri.append(0.00)
//                mod_id.append(0)
//            }
//        }
        

        for _ in 0..<total {
            for _ in 0..<modifiers.count{
                dictionaryModifiersTotal.append([:])
                dictionaryModifiersRequired.append([:])
            }
            dictionaryComplementsTotal.append([:])
        }
    }
    func setNumberOfItemsSelected(numItems: Int) {
        for j in 0..<modifiers.count {
            
            var selectedFlags = [Bool]()
            var selectedModif = [Int]()
            for i in 0..<modifiers[j].modifiers.count {
                //                MARK: Ya no se va a preseleccionar
//                if modifiers[j].requiredInfo {
//                    if i == 0 {
//                        selectedFlags.append(true)
//                    }else{
//                        selectedFlags.append(false)
//                    }
//
//                } else{
                selectedModif.append(i)
                selectedFlags.append(false)
//                }
               
            }
            selected.append(selectedFlags)
//            selectedM.append(j)
        }
       
        for j in 0..<complements.count {
            var itmsExt = [Int]()
            for _ in 0..<complements[j].complements.count {
                itmsExt.append(0)
            }
            itemsExtras.append(itmsExt)
        }
        for _ in 0..<total {
            itemsAdded.append(0)
        }
        
        for i in 0..<modifiers.count {
//            var requested = [Bool]()
//            for j in 0..<modifiers[i].modifiers.count {
//                if modifiers[i].requiredInfo
//                requested.append(false)
//            }
            
        }
        
        
        startArrays(numItems: numItems)
        LoadingOverlay.shared.hideOverlayView()
    }
    func startArrays(numItems: Int) {
        if numItems == 0 {
            self.navigationController?.popViewController(animated: true)
        }
        if numItems <= 1 {
            //        Se agregan items para mostrarlos en la tabla y sea más fácil su lectura
            itemsInsert.append("separator")
            itemsInsert.append("instructions")
            
            modifiersSelection.append(0)
            modifiersSelection.append(0)
            
            dictionarySelections.append(0)
            dictionarySelections.append(0)
            
            dictionaryComplementSelections.append(0)
            dictionaryComplementSelections.append(0)
            
            selectedCells.append([])
            selectedCells.append([])
            
            requested.append([])
            requested.append([])
            
            selectedModifiers.append([])
            selectedModifiers.append([])
            
            itemsPerExtras.append([])
            itemsPerExtras.append([])
            
            extrasCounter.append([])
            extrasCounter.append([])
            
            initialArraysString.append([])
            initialArraysString.append([])
            
            initialArraysInt.append([])
            initialArraysInt.append([])
            
            initialArraysDouble.append([])
            initialArraysDouble.append([])
            
            initialArraysIds.append([])
            initialArraysIds.append([])
            
            initialArraysModifString.append([])
            initialArraysModifString.append([])
            
            initialArraysModifDouble.append([])
            initialArraysModifDouble.append([])
            
            initialArraysModifIds.append([])
            initialArraysModifIds.append([])
            
            if modifiers.count > 0{
                for i in 0..<modifiers.count {
                    itemsInsert.append("modificador")
                    modifiersSelection.append(i)
                    dictionarySelections.append(i)
                    dictionaryComplementSelections.append(0)
                    selectedCells.append(selected[i])
//                    requested.append(selectedM[i])
                    selectedModifiers.append([i])
                    itemsPerExtras.append([])
                    extrasCounter.append([])
                    initialArraysString.append([])
                    initialArraysInt.append([])
                    initialArraysDouble.append([])
                    initialArraysIds.append([])
                    //                    MARK: ya no se usaran los modificadores por default
                    initialArraysModifString.append([])
                    initialArraysModifIds.append([])
                    initialArraysModifDouble.append([])
//                    let modificador = modifiers[i].modifiers[0].modifier_name
//                    initialArraysModifString.append([modificador!])
                    //                    MARK: precio modificadores por default
//                    let precioModificador = Double(modifiers[i].modifiers[0].value)
//                    totalsWithModifiers += (precioModificador * Double(total))
//                    totalsWithModifiers += 0
                    //                    MARK: Se agrega al array, pero con precio 0 porque ya no estarán pre-seleccionado
//                    initialArraysModifDouble.append([precioModificador])
//                    initialArraysModifDouble.append([0])
                    //                    ids de modificadores por default
//                    initialArraysModifIds.append([modifiers[i].modifiers[0].id])
                }
            }
            if complements.count > 0 {
                itemsInsert.append("extra")
                modifiersSelection.append(0)
                dictionarySelections.append(0)
                dictionaryComplementSelections.append(0)
                selectedCells.append([])
                requested.append([])
                selectedModifiers.append([])
                itemsPerExtras.append(itemsExtras[0])
                extrasCounter.append(itemsExtras[0])
                initialArraysString.append([])
                initialArraysInt.append([])
                initialArraysDouble.append([])
                initialArraysIds.append([])
                initialArraysModifString.append([])
                initialArraysModifDouble.append([])
                initialArraysModifIds.append([])
            }
            itemsInsert.append("adicional")
            itemsInsert.append("anadir")
            
            modifiersSelection.append(0)
            modifiersSelection.append(0)
            
            dictionarySelections.append(0)
            dictionarySelections.append(0)
            
            dictionaryComplementSelections.append(0)
            dictionaryComplementSelections.append(0)
            
            selectedCells.append([])
            selectedCells.append([])
            
            requested.append([])
            requested.append([])
            
            selectedModifiers.append([])
            selectedModifiers.append([])
            
            itemsPerExtras.append([])
            itemsPerExtras.append([])
            
            extrasCounter.append([])
            extrasCounter.append([])
            
            initialArraysString.append([])
            initialArraysString.append([])
            
            initialArraysInt.append([])
            initialArraysInt.append([])
            
            initialArraysDouble.append([])
            initialArraysDouble.append([])
            
            initialArraysIds.append([])
            initialArraysIds.append([])
            
            initialArraysModifString.append([])
            initialArraysModifString.append([])
            
            initialArraysModifDouble.append([])
            initialArraysModifDouble.append([])
            
            initialArraysModifIds.append([])
            initialArraysModifIds.append([])
            
            numberItems.text = "\(total)"
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
                itemPrice.text = "\(formattedTipAmount)"
            }
            for i in 0..<modifiers.count {
                
                let dictionary = ["modifier_name": initialArraysModifString[2+i], "modifier_price": initialArraysModifDouble[2+i], "modifier_id": initialArraysModifIds[2+i], "category": modifiers[i].modifiersName!] as [String : Any]
                dictionaryModifiersTotal[i] = dictionary
                dictionaryModifiersRequired[i] = ["required": false]
            }
        }else{
            //            MARK: else
            itemsInsert.append("separator")
            itemsInsert.append("instructions")
            
            modifiersSelection.append(0)
            modifiersSelection.append(0)
            
            dictionarySelections.append(0)
            dictionarySelections.append(0)
            
            dictionaryComplementSelections.append(0)
            dictionaryComplementSelections.append(0)
            
            totalItems.append(0)
            totalItems.append(0)
            
            selectedCells.removeAll()
            selectedCells.append([])
            selectedCells.append([])
            
            requested.removeAll()
            requested.append([])
            requested.append([])
            
            selectedModifiers.removeAll()
            selectedModifiers.append([])
            selectedModifiers.append([])
            
            itemsPerExtras.removeAll()
            itemsPerExtras.append([])
            itemsPerExtras.append([])
            
            extrasCounter.removeAll()
            extrasCounter.append([])
            extrasCounter.append([])
            
            initialArraysString.removeAll()
            initialArraysString.append([])
            initialArraysString.append([])
            
            initialArraysInt.removeAll()
            initialArraysInt.append([])
            initialArraysInt.append([])
            
            initialArraysDouble.removeAll()
            initialArraysDouble.append([])
            initialArraysDouble.append([])
            
            initialArraysIds.removeAll()
            initialArraysIds.append([])
            initialArraysIds.append([])
            
            initialArraysModifString.removeAll()
            initialArraysModifString.append([])
            initialArraysModifString.append([])
            
            initialArraysModifDouble.removeAll()
            initialArraysModifDouble.append([])
            initialArraysModifDouble.append([])
            
            initialArraysModifIds.removeAll()
            initialArraysModifIds.append([])
            initialArraysModifIds.append([])
            
            var x = 0
            for i in 0..<total {
                if i == 0 {
                    itemsInsert.append("item")
                    modifiersSelection.append(0)
                    dictionarySelections.append(0)
                    dictionaryComplementSelections.append(0)
                    totalItems.append(i+1)
                    selectedCells.append([])
                    requested.append([])
                    selectedModifiers.append([])
                    itemsPerExtras.append([])
                    extrasCounter.append([])
                    initialArraysString.append([])
                    initialArraysInt.append([])
                    initialArraysDouble.append([])
                    initialArraysIds.append([])
                    initialArraysModifString.append([])
                    initialArraysModifDouble.append([])
                    initialArraysModifIds.append([])
                    if modifiers.count > 0 {
                        var arr = [Int]()
                        for j in 0..<modifiers.count {
                            arr.append(j)
                            itemsInsert.append("modificador")
                            modifiersSelection.append(j)
                            dictionarySelections.append(x)
                            dictionaryComplementSelections.append(0)
                            totalItems.append(0)
                            selectedCells.append(selected[j])
                            selectedModifiers.append([j])
                            itemsPerExtras.append([])
                            extrasCounter.append([])
                            initialArraysString.append([])
                            initialArraysInt.append([])
                            initialArraysDouble.append([])
                            initialArraysIds.append([])
                            
                            //                            MARK: Ya no se van a preseleccionar
                            initialArraysModifString.append([])
                            initialArraysModifIds.append([])
                            initialArraysModifDouble.append([])
//                            let modificador = modifiers[j].modifiers[0].modifier_name
//                            initialArraysModifString.append([modificador!])
                            //                            MARK: ya no se va a preseleccionar
//                            let precioModificador = Double(modifiers[j].modifiers[0].value)
//                            totalsWithModifiers += (precioModificador * Double(total))
//                            initialArraysModifDouble.append([precioModificador])
//                            initialArraysModifIds.append([modifiers[j].modifiers[0].id])
                            x += 1
                        }
                    }
                    if complements.count > 0 {
                        itemsInsert.append("extra")
                        modifiersSelection.append(0)
                        dictionarySelections.append(0)
                        dictionaryComplementSelections.append(i)
                        totalItems.append(0)
                        selectedCells.append([])
                        requested.append([])
                        selectedModifiers.append([])
                        itemsPerExtras.append(itemsExtras[0])
                        extrasCounter.append(itemsExtras[0])
                        initialArraysString.append([])
                        initialArraysInt.append([])
                        initialArraysDouble.append([])
                        initialArraysIds.append([])
                        initialArraysModifString.append([])
                        initialArraysModifDouble.append([])
                        initialArraysModifIds.append([])
                    }
                }else{
                    itemsInsert.append("separator")
                    itemsInsert.append("item")
                    
                    modifiersSelection.append(0)
                    modifiersSelection.append(0)
                    
                    dictionarySelections.append(0)
                    dictionarySelections.append(0)
                    
                    dictionaryComplementSelections.append(0)
                    dictionaryComplementSelections.append(0)
                    
                    totalItems.append(0)
                    totalItems.append(i+1)
                    
                    selectedCells.append([])
                    selectedCells.append([])
                    
                    requested.append([])
                    requested.append([])
                    
                    selectedModifiers.append([])
                    selectedModifiers.append([])
                    
                    itemsPerExtras.append([])
                    itemsPerExtras.append([])
                    
                    extrasCounter.append([])
                    extrasCounter.append([])
                    
                    initialArraysString.append([])
                    initialArraysString.append([])
                    
                    initialArraysInt.append([])
                    initialArraysInt.append([])
                    
                    initialArraysDouble.append([])
                    initialArraysDouble.append([])
                    
                    initialArraysIds.append([])
                    initialArraysIds.append([])
                    
                    initialArraysModifString.append([])
                    initialArraysModifString.append([])
                    
                    initialArraysModifDouble.append([])
                    initialArraysModifDouble.append([])
                    
                    initialArraysModifIds.append([])
                    initialArraysModifIds.append([])
                    
                    if modifiers.count > 0 {
                        for j in 0..<modifiers.count {
                            itemsInsert.append("modificador")
                            modifiersSelection.append(j)
//                            print(x)
                            dictionarySelections.append(x)
                            dictionaryComplementSelections.append(0)
                            totalItems.append(0)
                            selectedCells.append(selected[j])
                            requested.append(selected[j])
                            selectedModifiers.append([j])
                            itemsPerExtras.append([])
                            extrasCounter.append([])
                            initialArraysString.append([])
                            initialArraysInt.append([])
                            initialArraysDouble.append([])
                            initialArraysIds.append([])
                            //                            MARK: ya no se va a preseleccionar
                            initialArraysModifString.append([])
                            initialArraysModifIds.append([])
                            initialArraysModifDouble.append([])
//                            let modificador = modifiers[j].modifiers[0].modifier_name
//                            initialArraysModifString.append([modificador!])
                            
//                            let precioModificador = Double(modifiers[j].modifiers[0].value)
//                            totalsWithModifiers += (precioModificador * Double(total))
//                            initialArraysModifDouble.append([precioModificador])
//                            initialArraysModifIds.append([modifiers[j].modifiers[0].id])
                            x += 1
                        }
                    }
                    if complements.count > 0 {
                        itemsInsert.append("extra")
                        modifiersSelection.append(0)
                        dictionarySelections.append(0)
                        dictionaryComplementSelections.append(i)
                        totalItems.append(0)
                        selectedCells.append([])
                        requested.append([])
                        selectedModifiers.append([])
                        itemsPerExtras.append(itemsExtras[0])
                        extrasCounter.append(itemsExtras[0])
                        initialArraysString.append([])
                        initialArraysInt.append([])
                        initialArraysDouble.append([])
                        initialArraysIds.append([])
                        initialArraysModifString.append([])
                        initialArraysModifDouble.append([])
                        initialArraysModifIds.append([])
                    }
                    
                }
                
            }
            itemsInsert.append("adicional")
            itemsInsert.append("anadir")
            
            modifiersSelection.append(0)
            modifiersSelection.append(0)
            
            dictionarySelections.append(0)
            dictionarySelections.append(0)
            
            dictionaryComplementSelections.append(0)
            dictionaryComplementSelections.append(0)
            
            totalItems.append(0)
            totalItems.append(0)
            
            selectedCells.append([])
            selectedCells.append([])
            
            requested.append([])
            requested.append([])
            
            selectedModifiers.append([])
            selectedModifiers.append([])
            
            itemsPerExtras.append([])
            itemsPerExtras.append([])
            
            extrasCounter.append([])
            extrasCounter.append([])
            
            initialArraysString.append([])
            initialArraysString.append([])
            
            initialArraysInt.append([])
            initialArraysInt.append([])
            
            initialArraysDouble.append([])
            initialArraysDouble.append([])
            
            initialArraysIds.append([])
            initialArraysIds.append([])
            
            initialArraysModifString.append([])
            initialArraysModifString.append([])
            
            initialArraysModifDouble.append([])
            initialArraysModifDouble.append([])
            
            initialArraysModifIds.append([])
            initialArraysModifIds.append([])
            
            numberItems.text = "\(total)"
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
                itemPrice.text = "\(formattedTipAmount)"
            }
            //itemPrice.text =  "$\(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers)0"
            var y = 0
            for _ in 0..<total {
                for j in 0..<modifiers.count {
                    let dictionary = ["modifier_name": initialArraysModifString[3+j], "modifier_price": initialArraysModifDouble[3+j], "modifier_id": initialArraysModifIds[3+j], "category": modifiers[j].modifiersName!] as [String : Any]
                    dictionaryModifiersTotal[y] = dictionary
                    dictionaryModifiersRequired[y] = ["required": false]
                    y += 1
                }
            }
        }
    }
    @IBAction func minusAction(_ sender: Any) {
        if total > 0 {
            itemsAdded.removeLast()
            total -= 1
            setArrays()
            itemsInsert.removeAll()
            dictionarySelections.removeAll()
            modifiersSelection.removeAll()
            totalItems.removeAll()
            selectedCells.removeAll()
            requested.removeAll()
            selectedModifiers.removeAll()
            itemsPerExtras.removeAll()
            extrasCounter.removeAll()
            initialArraysString.removeAll()
            initialArraysInt.removeAll()
            initialArraysDouble.removeAll()
            initialArraysIds.removeAll()
            initialArraysModifString.removeAll()
            initialArraysModifDouble.removeAll()
            initialArraysModifIds.removeAll()
            startArrays(numItems: total)
            tableView.reloadData()
        }
        
        
    }
    @IBAction func moreAction(_ sender: Any) {
        total += 1
        setArrays()
        totalsWithComplements = 0
        totalsWithModifiers = 0
        itemsAdded.append(1)
        itemsInsert.removeAll()
        modifiersSelection.removeAll()
        totalItems.removeAll()
        selectedCells.removeAll()
        requested.removeAll()
        selectedModifiers.removeAll()
        itemsPerExtras.removeAll()
        extrasCounter.removeAll()
        initialArraysString.removeAll()
        initialArraysInt.removeAll()
        initialArraysDouble.removeAll()
        initialArraysIds.removeAll()
        initialArraysModifString.removeAll()
        initialArraysModifDouble.removeAll()
        initialArraysModifIds.removeAll()
        startArrays(numItems: total)
        tableView.reloadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ItemViewController: modifiersDelegate, extrasDelegate, addOrderDelegate, closeModalProtocol {
    func getModifiers(index: IndexPath, modifier: String, price: Double, indexButton: Int, id: Int, selection: Int, modifierIndex: Int) {
        //        MARK: Para agregarlos a los arreglos de arreglos
//        let mod1 = [modifier]
//        let mod_pri1 = [price]
//        let mod_id = [id]
        
        print(selection)
        if !selectedCells[modifierIndex][indexButton] {
            if initialArraysModifDouble[index.row].count < modifiers[selectedModifiers[modifierIndex][0]].max_required {
                if !initialArraysModifString[index.row].contains(modifier), !initialArraysModifIds[index.row].contains(id) {
                    initialArraysModifString[index.row].append(modifier)
                    initialArraysModifDouble[index.row].append(price)
                    initialArraysModifIds[index.row].append(id)
                    selectedCells[index.row][indexButton] = true
                }
                if modifiers[selectedModifiers[modifierIndex][0]].requiredInfo {
                    dictionaryModifiersRequired[selection] = ["required": true]
                }
//                print(dictionaryModifiersRequired[0], selectedModifiers[modifierIndex][0])
//                selectedM.append(required)
//                initialArraysModifString[index.row] = mod
//                initialArraysModifDouble[index.row] = mod_pri
//                initialArraysModifIds[index.row] = mod_id
                
            }
        }else{
            if initialArraysModifString[index.row].contains(modifier), initialArraysModifIds[index.row].contains(id) {
                let ind = initialArraysModifString[index.row].index(of: modifier)
                initialArraysModifString[index.row].remove(at: ind!)
                initialArraysModifDouble[index.row].remove(at: ind!)
                initialArraysModifIds[index.row].remove(at: ind!)
                selectedCells[index.row][indexButton] = false
            }
            if modifiers[selectedModifiers[modifierIndex][0]].requiredInfo {
                dictionaryModifiersRequired[selection] = ["required": false]
            }
//            initialArraysModifString[index.row] = mod
//            initialArraysModifDouble[index.row] = mod_pri
//            initialArraysModifIds[index.row] = mod_id
//            let dictionaryModifiers = ["modifier_name": initialArraysModifString[index.row], "modifier_price": initialArraysModifDouble[index.row], "modifier_id": initialArraysModifIds[index.row], "category": modifiers[modifiersSelection[index.row]].modifiersName!] as [String : Any]
//            dictionaryModifiersTotal[selection] = dictionaryModifiers
//            totalsWithModifiers = (Double(1)*price)
//            totalsWithModifiers = 0
//
//            selectedCells[index.row][indexButton] = false
//            print(initialArraysModifDouble)
//            print(totalsWithModifiers)
//            let formatter = NumberFormatter()
//            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
//            formatter.numberStyle = .currency
//
//            if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
//                itemPrice.text = "\(formattedTipAmount)"
//            }
            
        }
//            selectedCells[index.row][indexButton] = true
//            for i in 0..<modifiers[modifiersSelection[modifierIndex]].modifiers.count {
//                if i != indexButton {
//                    selectedCells[index.row][i] = false
//                }
//            }
        let dictionaryModifiers = ["modifier_name": initialArraysModifString[index.row], "modifier_price": initialArraysModifDouble[index.row], "modifier_id": initialArraysModifIds[index.row], "category": modifiers[modifiersSelection[index.row]].modifiersName!] as [String : Any]
        dictionaryModifiersTotal[selection] = dictionaryModifiers
        totalsWithModifiers = (Double(1)*price)
        totalsWithModifiers = 0
        
        
        for i in 0..<initialArraysModifDouble.count {
            for j in 0..<initialArraysModifDouble[i].count {
                totalsWithModifiers += initialArraysModifDouble[i][j]
            }
        }
//        print(totalsWithModifiers)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        
        if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
            itemPrice.text = "\(formattedTipAmount)"
        }

        tableView.reloadRows(at: [index], with: .fade)
    }
    
    func addItem(index: IndexPath, i: Int, name: String, price: Double, cant: Int, id: Int, selection: Int) {
//        print(limit_extra)
        extrasCounter[index.row][i] = 1
//        print(extrasCounter[index.row].reduce(0,+))
        if extrasCounter[index.row].reduce(0,+) <= limit_extra {
//            extrasCounter[index.row][i] = 1
//            print(extrasCounter)
            let items = cant + 1
            //        if items <= limit_extra {
            itemsPerExtras[index.row][i] = items
            
            if !initialArraysString[index.row].contains(name) {
                initialArraysString[index.row].append(name)
                initialArraysInt[index.row].append(items)
                initialArraysDouble[index.row].append(Double(items)*price)
                initialArraysIds[index.row].append(id)
            }else{
                initialArraysString[index.row][initialArraysString[index.row].index(of: name)!] = name
                initialArraysInt[index.row][initialArraysString[index.row].index(of: name)!] = items
                initialArraysDouble[index.row][initialArraysString[index.row].index(of: name)!] = Double(items)*price
                initialArraysIds[index.row][initialArraysString[index.row].index(of: name)!] = id
            }
            
            let dictionaryComplements = ["complement_name": initialArraysString[index.row], "complement_price": initialArraysDouble[index.row], "complement_cant": initialArraysInt[index.row], "complement_id": initialArraysIds[index.row], "category":"EXTRAS"] as [String : Any]
            dictionaryComplementsTotal[selection] = dictionaryComplements
//            print(dictionaryComplementsTotal)
            totalsWithComplements += (Double(1)*price)
            let totals = totalsWithComplements + (Double(total)*doublePrice)
            tableView.reloadRows(at: [index], with: .fade)
            //        tableView.reloadData()
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
                itemPrice.text = "\(formattedTipAmount)"
            }
        }
        
//        }
        
        //itemPrice.text =  "$\(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers)0"
    }
    func removeItem(index: IndexPath, i: Int, name: String, price: Double, cant: Int, id: Int, selection: Int){
        for z in 0..<itemsPerExtras.count {
            for y in 0..<itemsPerExtras[z].count {
                if itemsPerExtras[z][y] == 0 {
                    extrasCounter[z][y] = 0
                }
            }
        }
//        print(extrasCounter)
        let items = cant - 1
        if items >= 0 {
            itemsPerExtras[index.row][i] = items
        }
        if items > 0 {
            initialArraysIds[index.row][initialArraysString[index.row].index(of: name)!] = id
            initialArraysInt[index.row][initialArraysString[index.row].index(of: name)!] = items
            initialArraysDouble[index.row][initialArraysString[index.row].index(of: name)!] = Double(items)*price
            initialArraysString[index.row][initialArraysString[index.row].index(of: name)!] = name
        }else{
            if items == 0{
                initialArraysIds[index.row].remove(at: initialArraysString[index.row].index(of: name)!)
                initialArraysInt[index.row].remove(at: initialArraysString[index.row].index(of: name)!)
                initialArraysDouble[index.row].remove(at: initialArraysString[index.row].index(of: name)!)
                initialArraysString[index.row].remove(at: initialArraysString[index.row].index(of: name)!)
            }
        }
        
        let dictionaryComplements = ["complement_name": initialArraysString[index.row], "complement_price": initialArraysDouble[index.row], "complement_cant": initialArraysInt[index.row], "complement_id": initialArraysIds[index.row], "category": /*modifiers[modifiersSelection[index.row]].modifiersName*/"EXTRAS"] as [String : Any]
        dictionaryComplementsTotal[selection] = dictionaryComplements
        
//        print(totalsWithComplements)
        if totalsWithComplements >= (Double(1)*price) {
            totalsWithComplements -= (Double(1)*price)
            
            let totals = totalsWithComplements  + (Double(total)*doublePrice)
//            if totals <= (doublePrice * Double(total)) {
//                totalsWithComplements = (doublePrice * Double(total))
//            }
            tableView.reloadRows(at: [index], with: .fade)
            //        tableView.reloadData()
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if totals >= (doublePrice * Double(total)){
                if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
                    itemPrice.text = "\(formattedTipAmount)"
                }
                
            }
        }
        
        if itemsPerExtras[index.row][i] == 0 {
            extrasCounter[index.row][i] = 0
        }
//        print(extrasCounter, "remove")
        //itemPrice.text =  "$\(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers)0"
    }
    func addOrder() {
        //        MARK: Create
        var x = 0
        var arrai = [Bool]()
        var arraitemp = [Bool]()
        for i in 0..<total {
            var modifiersArray = [Dictionary<String,Any>]()
            var modifiersArrayRequired = [Dictionary<String,Any>]()
            for j in 0..<modifiers.count {
                modifiersArray.append(dictionaryModifiersTotal[x])
                modifiersArrayRequired.append(dictionaryModifiersRequired[x])
                x += 1
                print(modifiers[j].requiredInfo)
                if modifiers[j].requiredInfo {
                    arrai.append(true)
                }
                if (modifiersArrayRequired[j]["required"]) as! Bool {
                    arraitemp.append(true)
                }
            }
            
            let dictionary = ["name": itemNameString, "quantity": 1, "price": doublePrice, "complex": true, "complements": dictionaryComplementsTotal[i], "modifiers": modifiersArray, "id": idItem, "is_package": isPackage,"price_product": doublePrice, "image": imageItem ] as [String : Any]
            dictionaryInfo.append(dictionary)
        }
        if arrai.count == arraitemp.count {
            DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: dictionaryInfo, establishment_id: idEstablishment, notes: notes)
            self.navigationController?.popViewController(animated: true)

        }else{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.subTitleMessageString = "Por favor, escoge un producto obligatorio para poder continuar"
            newXIB.errorMessageString = "Producto obligatorio"
            present(newXIB, animated: true, completion: nil)
            dictionaryInfo.removeAll()
        }
//        print(dictionaryInfo)
//        DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: dictionaryInfo, establishment_id: idEstablishment, notes: notes)
        //        MARK: Alerta de agregado. Ya no se usa
//        let newXIB = AlertSuccesViewController(nibName: "AlertSuccesViewController", bundle: nil)
//        if let name = UserDefaults.standard.object(forKey: "first_name") as? String {
//            newXIB.titleString = "\(name), tu producto se ha añadido exitosamente a tu orden"
//        }else{
//            newXIB.titleString = "Tu producto se ha añadido exitosamente a tu orden"
//        }
//        newXIB.delegate = self
//        newXIB.modalTransitionStyle = .crossDissolve
//        newXIB.modalPresentationStyle = .overCurrentContext
//        present(newXIB, animated: true, completion: {
////            self.defaults.set(self.idEstablishment, forKey: "establishment_id")
//        })
//        self.navigationController?.popViewController(animated: true)
    }
    func goCheckout() {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ItemViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsInsert.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if itemsInsert[indexPath.row] == "separator" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "divider", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }else if itemsInsert[indexPath.row] == "instructions"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "instructionsCell", for: indexPath) as! InstructionTableViewCell
            cell.selectionStyle = .none
            if let name = UserDefaults.standard.object(forKey: "first_name") as? String {
                cell.infoLabel.text = descriptionText
            }else{
                cell.infoLabel.text = descriptionText
                
            }
            
            return cell
        }else if itemsInsert[indexPath.row] == "item"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! itemCellTableViewCell
            cell.selectionStyle = .none
            cell.itemName.text = itemNameString
            cell.itemImage.clipsToBounds = true
            cell.itemImage.layer.cornerRadius = 5
            if imageItem != nil, imageItem != "" {
                Nuke.loadImage(with: URL(string: imageItem)!, into: cell.itemImage)
            }
            cell.itemPrice.text = price + "0"
            cell.numberItem.text = "\(totalItems[indexPath.row])."
            return cell
        }else if itemsInsert[indexPath.row] == "adicional"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "aditional", for: indexPath) as! AdditionalInfoTableViewCell
            cell.selectionStyle = .none
            cell.notes.delegate = self
            return cell
        }else if itemsInsert[indexPath.row] == "anadir"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "addOrder", for: indexPath) as! AddToMyOrderTableViewCell
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers) as NSNumber) {
                cell.total.text = "\(formattedTipAmount)"
            }

            //cell.total.text = "$\(totalsWithComplements + (Double(total)*doublePrice) + totalsWithModifiers)0"
            cell.delegate = self
            return cell
        }else if itemsInsert[indexPath.row] == "extra"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "sectionExtras", for: indexPath) as! extrasTableViewCell
            cell.selectionStyle = .none
            cell.complements = complements[modifiersSelection[indexPath.row]].complements
            cell.extrasTitle = complements[modifiersSelection[indexPath.row]].complement_name
            cell.totalItemsSelected = itemsPerExtras[indexPath.row]
            cell.selectedComplement = dictionaryComplementSelections[indexPath.row]
            cell.delegate = self
            cell.indexPath = indexPath
            cell.itemExtras = limit_extra
            cell.tableView.reloadData()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "sectionItemCell", for: indexPath) as! ComplementsTableViewCell
            cell.selectionStyle = .none
            cell.modifiers = modifiers[modifiersSelection[indexPath.row]].modifiers
            cell.modifier_name = modifiers[modifiersSelection[indexPath.row]].modifiersName
            cell.is_required = modifiers[modifiersSelection[indexPath.row]].requiredInfo
            cell.maxToChoose = modifiers[modifiersSelection[indexPath.row]].max_required
            cell.indexPath = indexPath
            cell.selectedFlags = selectedCells[indexPath.row]
            cell.numComplement = dictionarySelections[indexPath.row]
            cell.delegate = self
            cell.tableView.reloadData()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if itemsInsert[indexPath.row] == "separator" {
            return 29
        }else if itemsInsert[indexPath.row] == "instructions" {
            return 120
        }else if itemsInsert[indexPath.row] == "item" {
            return 112
        }else if itemsInsert[indexPath.row] == "adicional"{
            return 180
        }else if itemsInsert[indexPath.row] == "anadir" {
            return 101
        }else if itemsInsert[indexPath.row] == "extra"{
            return CGFloat((44*(complements[0].complements.count + 1)) + 20)
        }else{
            return CGFloat((44*(modifiers[modifiersSelection[indexPath.row]].modifiers.count + 1)) + 20)
        }
    }
}
extension ItemViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        var string = String()
        if textField.text != "" {
            let not = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes) + itemNameString + ": "
//            if not != "" {
                string += not + " " + textField.text! + " "
//            }else{
//                string += textField.text! + " "
//            }
        }
        notes = string
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 40 // Bool
    }
}
