//
//  ProfileSingleCardViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ProfileSingleCardViewController: UIViewController, paymentDelegate, getDateCard, UITextFieldDelegate {

    
    @IBOutlet weak var buttonText: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var vencimiento: UILabel!
    @IBOutlet weak var cvv: UITextField!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var btn_Action: UIButton!
    @IBOutlet weak var vencimientoIncorrecto: UILabel!
    
//    WRONG ICONS
    @IBOutlet weak var wrongVencimiento: UIImageView!
    @IBOutlet weak var wrongDVV: UIImageView!
    
    @IBOutlet weak var ImageTarjeta: UIImageView!

    @IBOutlet weak var numPedido: UIView!
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var payment_source_id = String()
    var card_name = String()
    var card_number = String()
    var paymentWS = PaymentWS()
    var is_editing = false
    var Month = String()
    var Year = String()
    var brand = String()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = card_name
        number.text = card_number
        paymentWS.delegate = self
        editView.alpha = 0
        cvv.delegate = self
        //OPENPAY
        if brand.uppercased() == "MASTERCARD"{
            ImageTarjeta.image = UIImage(named: "iconMC")
        }else if brand.uppercased() == "AMERICAN_EXPRESS"{
            ImageTarjeta.image = UIImage(named: "iconAE")
        }else{
            ImageTarjeta.image = UIImage(named: "visaCard")
        }
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedido.alpha = 1
            }else{
                numPedido.alpha = 0
            }
        }else{
            numPedido.alpha = 0
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            wrongDVV.alpha = 1
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        wrongDVV.alpha = 0
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = Int()
        if brand == "MC" || brand == "VISA" {
            maxLength = 3
        }else{
            maxLength = 4
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendDate(month: String, year: String) {
        let date = Date()
        let currentCalendar = Calendar.current
        let currentMonth = currentCalendar.component(.month, from: date)
        let currentYear = currentCalendar.component(.year, from: date)
        
        if Int(year)! == currentYear {
            if Int(month)! <= currentMonth {
                wrongVencimiento.alpha = 1
                vencimientoIncorrecto.alpha = 1
            }else{
                wrongVencimiento.alpha = 0
                vencimientoIncorrecto.alpha = 0
            }
        }else{
            wrongVencimiento.alpha = 0
            vencimientoIncorrecto.alpha = 0
        }
        
        Month = month
        Year = year
        vencimiento.text = month + "/" + year
        vencimiento.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
    }
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
       if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func vencimientoSelector(_ sender: Any) {
        let datePicker = CardDatePickerViewController(nibName: "CardDatePickerViewController", bundle: nil)
        datePicker.delegate = self
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.modalTransitionStyle = .coverVertical
        self.present(datePicker, animated: true, completion: nil)
    }
    
    @IBAction func editCard(_ sender: Any) {
        if !is_editing{
            editView.alpha = 1
//            btn_Action.setImage(UIImage(named: "backgroundBtn"), for: .normal)
            btn_Action.setBackgroundImage(UIImage(named: "backgroundBtn"), for: .normal)
//            btn_Action.titleLabel?.text = "  Guardar"
            buttonText.text = "Guardar"
            is_editing = true
        }else{
            editView.alpha = 0
//            btn_Action.setImage(UIImage(named: "backgroundBtn"), for: .normal)
            btn_Action.setBackgroundImage(UIImage(named: "backgroundBtn"), for: .normal)
//            btn_Action.titleLabel?.text = "  Borrar"
            buttonText.text = "Borrar"
            is_editing = false
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSuccessUpdateCard() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didFailUpdateCard(error: String, subtitle: String) {
        btn_Action.setBackgroundImage(UIImage(named: "backgroundBtn"), for: .normal)
        btn_Action.titleLabel?.text = "Continuar"
        is_editing = true
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessRemoveCard() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didFailRemoveCard(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func deleteCard(_ sender: Any) {
        if is_editing{
            if vencimiento.text != "Vencimiento *" && cvv.text != ""{
                paymentWS.updateCard(client_id: client_id, payment_source_id: payment_source_id, month: Month, year: Year, name: card_name)
            }else{
                if vencimiento.text == "Vencimiento *" {
                    wrongVencimiento.alpha = 1
                }else{
                    wrongVencimiento.alpha = 0
                }
                if cvv.text == "" {
                    wrongDVV.alpha = 1
                }
                let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            paymentWS.deleteCard(client_id: client_id, payment_source_id: payment_source_id)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
