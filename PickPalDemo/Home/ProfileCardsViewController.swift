//
//  ProfileCardsViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import OpenpayKit

class ProfileCardsViewController: UIViewController, paymentDelegate {
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var cards = [Card]()
    var paymentWS = PaymentWS()
    var first_time = false
    var isFromPromos:Bool = false
    
    @IBOutlet weak var copyTarjetas: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numPedidos: UIView!
    
//    Openpay
    //OpenPay
    var openpay : Openpay!
    var sessionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        paymentWS.delegate = self
        paymentWS.getUserCards(client_id: client_id)
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
//        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey: Constants.API_KEY, isProductionMode: false, isDebug: false)
    }
    
//
//    func successSessionID(sessionID: String) {
//        print("SessionID: \(sessionID)")
//        self.sessionID = sessionID
//        //        LoadingOverlay.shared.showOverlay(view: self.view)
////        openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
//    }
//
//    func failSessionID(error: NSError) {
//        LoadingOverlay.shared.hideOverlayView()
//        print("\(error.code) - \(error.localizedDescription)")
//    }
//    func successCard() {
////        openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
//
//    }
//
//    func failCard(error: NSError) {
//        print("\(error.code) - \(error.localizedDescription)")
//    }
    
//    func successToken(token: OPToken) {
//        print("TokenID: \(token.id)")
//        LoadingOverlay.shared.showOverlay(view: self.view)
//        paymentWS.addNewCardOppa(client_id: client_id, token_card: token.id, session_id: self.sessionID)
//    }
    func didSuccessAddCard() {
        paymentWS.getUserCards(client_id: client_id)
    }
    func didFailAddCard(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let alert = UIAlertController(title: "PickPal", message: "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
//    func failToken(error: NSError) {
//        print("\(error.code) - \(error.localizedDescription)")
//    }
    @IBAction func settings(_ sender: Any) {
//        openpay = nil
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
//        openpay = nil
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    @IBAction func pedidosButton(_ sender: Any) {
//        openpay = nil
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
//        openpay = nil
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        if !first_time{
            paymentWS.getUserCards(client_id: client_id)
            LoadingOverlay.shared.showOverlay(view: self.view)
        }
    }
    
    func didSuccessGetCards(cards: [Card]) {
        LoadingOverlay.shared.hideOverlayView()
        self.cards = cards
        tableView.reloadData()
    }
    
    func didFailGetCards(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if error != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }else{
            self.cards.removeAll()
            tableView.reloadData()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension ProfileCardsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == cards.count{
//            openpay = nil
//            CONEKTA
//            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCardViewController
//            first_time = false
//            newVC.cards = self.cards
//            self.navigationController?.pushViewController(newVC, animated: true)
            
            addCardOpenPay()
            
//            OPENPAY
//            self.openpay.loadCardForm(in: self, successFunction: self.successCard, failureFunction: self.failCard, formTitle: "Openpay")
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileSingleCard") as! ProfileSingleCardViewController
            newVC.card_name = cards[indexPath.row].name
            newVC.card_number = cards[indexPath.row].card
            newVC.payment_source_id = cards[indexPath.row].payment_source_id
            newVC.brand = cards[indexPath.row].brand 
            newVC.isFromPromos = self.isFromPromos
            first_time = false
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < cards.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilecardItem", for: indexPath) as! ProfileCardItemTableViewCell
            cell.txt.text = cards[indexPath.row].card
//            OPENPAY
//            if cards[indexPath.row].brand.uppercased() == "MASTERCARD"{
//                cell.IMG.image = UIImage(named: "iconMC")
//            }else if cards[indexPath.row].brand.uppercased() == "AMERICAN_EXPRESS"{
//                cell.IMG.image = UIImage(named: "iconAE")
//            }else{
//                cell.IMG.image = UIImage(named: "visaCard")
//            }
//            CONEKTA
            if cards[indexPath.row].brand == "MC"{
                cell.IMG.image = UIImage(named: "iconMC")
            }else if cards[indexPath.row].brand == "AMERICAN_EXPRESS"{
                cell.IMG.image = UIImage(named: "iconAE")
            }else{
                cell.IMG.image = UIImage(named: "visaCard")
            }

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddCard", for: indexPath)
//            if cards.count >= 3 {
//                cell.isUserInteractionEnabled = false
//            }else{
//                cell.isUserInteractionEnabled = true
//            }
            return cell
        }
    }
    
    
}

extension ProfileCardsViewController: ordersDelegate {
    
    func addCardOpenPay() {
        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey:  Constants.API_KEY, isProductionMode: false, isDebug: false)
        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Openpay")
    }

    func successSessionID(sessionID: String) {
            print("SessionID: \(sessionID)")
            self.sessionID = sessionID
            //        LoadingOverlay.shared.showOverlay(view: self.view)
            openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
        }
        
        func failSessionID(error: NSError) {
            LoadingOverlay.shared.hideOverlayView()
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
                
            }
        }
        func successCard() {
            openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
            
        }
        
        func failCard(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }
        
        func successToken(token: OPToken) {
            print("TokenID: \(token.id)")
            DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: self.view)
                self.paymentWS.addNewCardOppa(client_id:  self.client_id, token_card: token.id, session_id: self.sessionID)
            }
        }
    

        
        func failToken(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }

}
