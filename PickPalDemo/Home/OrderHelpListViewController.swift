//
//  OrderHelpListViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class OrderHelpListViewController: UIViewController, userDelegate {

    @IBOutlet weak var tableView: UITableView!
    var userws = UserWS()
    var myorders = [Orders]()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        if let client_id = UserDefaults.standard.object(forKey: "client_id") as? Int {
            userws.viewmyOrders(client_id: client_id)
        }
        // Do any additional setup after loading the view.
    }
    func didSuccessMyOrders(orders: [Orders]) {
        LoadingOverlay.shared.hideOverlayView()
        self.myorders = orders
        self.tableView.reloadData()
    }
    func didFailMyOrders(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let alert = UIAlertController(title: "No hay pedidos", message: "Actualmente no tienes pedidos para ayuda", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        print(title)
    }
    @IBAction func homeButton(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func pedidosButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func settingsButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OrderHelpListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myorders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentCell") as! EstablishmentHelpTableViewCell
        cell.selectionStyle = .none
        cell.deliveredTime.text = myorders[indexPath.row].fecha
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        var subtotal = Double()
        for i in 0..<myorders[indexPath.row].itemsOrders.count {
            subtotal += Double(myorders[indexPath.row].itemsOrders[i].total)!
        }
        let total = subtotal + Double(myorders[indexPath.row].pickpal_service)!
        if let formattedTipAmount = formatter.string(from: total as NSNumber) {
            cell.totalLabel.text = "\(formattedTipAmount)"
        }
//        cell.totalLabel.text = "$" + myorders[indexPath.row].total + "0"
        cell.nameEstablishment.text = myorders[indexPath.row].comercio
        cell.nameEstablishmentCard.text = myorders[indexPath.row].comercio
        if myorders[indexPath.row].img != "" {
            Nuke.loadImage(with: URL(string: myorders[indexPath.row].img)!, into: cell.imageEstablishment)
        }
        cell.locationLabel.text = myorders[indexPath.row].place + " - " + myorders[indexPath.row].category
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleHelp") as! SingleEstablishmentHelpViewController
        newVC.category = myorders[indexPath.row].place + " - " + myorders[indexPath.row].category
        newVC.hourEntregado = myorders[indexPath.row].fecha
        newVC.nombreEstablecimiento = myorders[indexPath.row].comercio
        newVC.myorder = myorders[indexPath.row]
        newVC.isFromPromos = isFromPromos
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        var subtotal = Double()
        for i in 0..<myorders[indexPath.row].itemsOrders.count {
            subtotal += Double(myorders[indexPath.row].itemsOrders[i].total)!
        }
        let total = subtotal + Double(myorders[indexPath.row].pickpal_service)!
        if let formattedTipAmount = formatter.string(from: total as NSNumber) {
             newVC.totalPedido = "\(formattedTipAmount)"
        }
        newVC.imagenEstablecimiento = myorders[indexPath.row].img
        self.navigationController?.pushViewController(newVC, animated: true)
       
    }
}
