//
//  ProfileAddressViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ProfileAddressViewController: UIViewController, paymentDelegate, EditProfileAddress, userDelegate {
    
    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var tableView: UITableView!
    var address = [BillingAddress]()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var paymentWS = PaymentWS()
    var userWS = UserWS()
    var flags = [Bool]()
    var first_time = false
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentWS.delegate = self
        userWS.delegate = self
        paymentWS.getBillingAddress(client_id: client_id)
        LoadingOverlay.shared.showOverlay(view: self.view)
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !first_time{
            paymentWS.getBillingAddress(client_id: client_id)
            LoadingOverlay.shared.showOverlay(view: self.view)
        }
    }
    @IBAction func settings(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               newVC.isFromPromos = isFromPromos
               self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    
    @IBAction func pedidosButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func didSuccessGetBillingAddress(info: [BillingAddress]) {
        LoadingOverlay.shared.hideOverlayView()
        address = info
        for _ in info{
            flags.append(false)
        }
        tableView.reloadData()
    }
    
    func didFailGetBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if error != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
        
    }
    func didSuccessDeleteAddress() {
        LoadingOverlay.shared.showOverlay(view: self.view)
        paymentWS.getBillingAddress(client_id: client_id)
    }
    func didFailDeleteAddress(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func clickDeleteAddress(indexPath: IndexPath) {
        let okAction = UIAlertAction(title: "Eliminar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.userWS.DeleteAddress(billing_address_id: self.address[indexPath.row].id)
            self.address.removeAll()
            self.tableView.reloadData()
        }
        let cancel = UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default) {
            UIAlertAction in
        }
        let alert = UIAlertController(title: "PickPal", message: "¿Desas eliminar estos datos de facturación?", preferredStyle: .alert)
        alert.addAction(cancel)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    func clickEditAddress(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileAddAddress") as! ProfileAddAddressViewController
        newVC.isUpdate = true
        newVC.addressId = address[indexPath.row].id
        newVC.billingAddresses = address
        newVC.info = address[indexPath.row]
        newVC.isFromPromos = isFromPromos
        first_time = false
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
extension ProfileAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return address.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == address.count ){
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileAddAddress") as! ProfileAddAddressViewController
            first_time = false
//            newVC.info = address[indexPath.row]
            newVC.billingAddresses = address
            newVC.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == address.count ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddAddress", for: indexPath)
//            if address.count >= 3{
//                cell.isUserInteractionEnabled = false
//            }else{
//                cell.isUserInteractionEnabled = true
//            }
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddress", for: indexPath) as! ProfileAddressTableViewCell
            cell.RFC.text = address[indexPath.row].RFC
//            cell.direccion.text = address[indexPath.row].colony + " " + address[indexPath.row].street + " #" + address[indexPath.row].interior_number
            cell.indexPath = indexPath
            cell.delegate = self
            return cell

        }
        
    }
}

