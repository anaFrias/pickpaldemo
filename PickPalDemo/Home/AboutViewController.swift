//
//  AboutViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 07/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController, userDelegate {

    @IBOutlet weak var numPedido: UIView!
    @IBOutlet weak var direccion: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var isFromPromos:Bool = false
    
    var userWS = UserWS()
//    var options = ["Términos y Condiciones", "Políticas de Cupones y Promociones", "Políticas de Privacidad", "", "Califícanos en App Store/PlayStore"]
     var options = ["Términos y Condiciones de uso", "Aviso y políticas de Privacidad", "", "Más información", "Califícanos en App Store/PlayStore"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userWS.delegate = self
        userWS.get_about()
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedido.alpha = 1
            }else{
                numPedido.alpha = 0
            }
        }else{
            numPedido.alpha = 0
        }
    }

    @IBAction func webSiteBtn(_ sender: Any) {
        guard let url = URL(string: "http://www.pickpal.com/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSuccessGetAbout(email: String, phone_number: String, address: String) {
        mail.text = email
        phone.text = phone_number
        direccion.text = address
    }
    func didFailGetAbout(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    @IBAction func settings(_ sender: Any) {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               newVC.isFromPromos = isFromPromos
               self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }

    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension AboutViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            return 75
        }else{
            return 31
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectedCells", for: indexPath) as! AboutCellTableViewCell
        
        
        cell.titleAbout.text = options[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
//            self.navigationController?.pushViewController(vc, animated: true)
            self.present(vc, animated: true, completion: nil)
        }
        if indexPath.row == 1 {
            guard let url = URL(string: "https://pickpal.com/privacy_policy.pdf") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        if indexPath.row == 2 {
            
        }
        if indexPath.row == 3 {
            guard let url = URL(string: "https://pickpal.com/contacto/") else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        if indexPath.row == 4 {
            rateApp(appId: "1378364730")//1367186101 Kulttiva //1378364730 PickPal
        }
    }
    fileprivate func rateApp(appId: String) {
        openUrl("itms-apps://itunes.apple.com/app/" + appId)
    }
    fileprivate func openUrl(_ urlString:String) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
