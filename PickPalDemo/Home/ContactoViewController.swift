//
//  ContactoViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ContactoViewController: UIViewController, userDelegate {

    
    @IBOutlet weak var numPedido: UIView!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var txtContacto: UITextView!
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    let userWS = UserWS()
    var establishment_id = Int()
    var order_id = Int()
    var establishment = false
    var comercio = ""
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userWS.delegate = self
//        if let first_name = UserDefaults.standard.object(forKey: "first_name") as? String {
//            if comercio != ""{
//                titulo.text = "¿Cómo podemos ayudarte " + first_name + " en tu pedido de " + comercio + "?"
//            }else{
//                titulo.text = "¿Cómo podemos ayudarte " + first_name + "?"
//            }
//
//        }else{
//            if comercio != ""{
//                titulo.text = "¿Cómo podemos ayudarte en tu pedido de " + comercio + "?"
//            }else{
//                titulo.text = "¿Cómo podemos ayudarte?"
//            }
//
//        }
        if let userName = UserDefaults.standard.object(forKey: "first_name") as? String {
            titulo.text = "\(userName) escríbenos tu comentario y/o sugerencia..."
        }else{
            titulo.text = "Escríbenos tu comentario y/o sugerencia..."
        }
        
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedido.alpha = 1
            }else{
                numPedido.alpha = 0
            }
        }else{
            numPedido.alpha = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               newVC.isFromPromos = isFromPromos
               self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func didSuccessHelp() {
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popViewController(animated: true)
        }
        let alert = UIAlertController(title: "¡Tu mensaje ha sido Enviado!", message: "\(String(describing: UserDefaults.standard.object(forKey: "first_name")!)), gracias por compartirnos tu opinión y hacer que PickPal sea mejor cada día.", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func didFailHelp(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func call(_ sender: Any) {
        if txtContacto.text != ""{
            if establishment{
                userWS.StablishmentHelp(message:txtContacto.text, client_id: client_id, establishment_id: establishment_id, order_id: order_id)
            }else{
                userWS.Help(message: txtContacto.text, client_id: client_id)
            }
            
        }else{
            let alert = UIAlertController(title: "PickPal", message: "Ingresa el comentario que deseas enviar", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
