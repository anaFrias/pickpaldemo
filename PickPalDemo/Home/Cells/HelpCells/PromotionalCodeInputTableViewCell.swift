//
//  PromotionalCodeInputTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol inputPromoCodeDelegate {
    func inputPromoCode(text: String)
}
class PromotionalCodeInputTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var inputText: UITextField!
    var delegate: inputPromoCodeDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        inputText.delegate = self
        // Initialization code
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate.inputPromoCode(text: textField.text!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
