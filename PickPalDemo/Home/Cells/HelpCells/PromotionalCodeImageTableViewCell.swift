//
//  PromotionalCodeImageTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol openImagePickerDelegate {
    func imagePicker(isPromotional: Bool)
}

class PromotionalCodeImageTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imagePicker: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewRounded: UIView!
    var delegate: openImagePickerDelegate!
    var isPromotion = Bool()
    override func awakeFromNib() {
        super.awakeFromNib()
        viewRounded.layer.cornerRadius = 4.5
        viewRounded.layer.borderWidth = 0.5
        viewRounded.layer.borderColor = UIColor.init(red: 173/255, green: 180/255, blue: 185/255, alpha: 1.0).cgColor
        // Initialization code
    }

    @IBAction func takePicture(_ sender: Any) {
        self.delegate.imagePicker(isPromotional: isPromotion)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
