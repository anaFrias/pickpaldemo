//
//  RefundSendInfoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol sendInfoRefundsDelegate {
    func sendInfoRefunds(text: String)
}

class RefundSendInfoTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var placeholder: UILabel!
    @IBOutlet weak var textView: UITextView!
    var delegate: sendInfoRefundsDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.delegate = self
        textView.layer.borderColor = UIColor.init(red: 173/255, green: 180/255, blue: 185/255, alpha: 1.0).cgColor
        textView.layer.cornerRadius = 4.5
        textView.layer.borderWidth = 0.5
        // Initialization code
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" {
            placeholder.alpha = 0
        }else{
            placeholder.alpha = 1
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholder.alpha = 0
    }
    @IBAction func sendInfo(_ sender: Any) {
        if textView.text != "" {
            self.delegate.sendInfoRefunds(text: textView.text!)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
