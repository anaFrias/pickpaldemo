//
//  sendInfoMissingTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/21/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol FaltantesProtocol {
    func sendText(text: String)
}

class sendInfoMissingTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var placeholder: UILabel!
    @IBOutlet weak var textView: UITextView!
    var delegate: FaltantesProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textView.layer.borderColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1.0).cgColor
        textView.layer.borderWidth = 1.0
        textView.layer.cornerRadius = 4.5
        textView.delegate = self
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholder.alpha = 0
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" {
            placeholder.alpha = 0
        }else{
            placeholder.alpha = 1
        }
    }
    @IBAction func sendInfo(_ sender: Any) {
//        if textView.text != "" {
            self.delegate.sendText(text: textView.text)
//        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
