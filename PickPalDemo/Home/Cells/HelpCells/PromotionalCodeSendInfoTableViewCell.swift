//
//  PromotionalCodeSendInfoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol saveInfoDelegate {
    func saveInfo(text: String)
}

class PromotionalCodeSendInfoTableViewCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var placeholder: UILabel!
    @IBOutlet weak var textView: UITextView!
    var delegate: saveInfoDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textView.delegate = self
        textView.layer.cornerRadius = 4.5
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.init(red: 173/255, green: 180/255, blue: 185/255, alpha: 1.0).cgColor
    }
    @IBAction func sendInfo(_ sender: Any) {
//        if textView.text != "" {
            self.delegate.saveInfo(text: textView.text)
//        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" {
            placeholder.alpha = 0
        }else{
            placeholder.alpha = 1
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholder.alpha = 0
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
