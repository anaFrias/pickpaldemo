//
//  FAQuestionsTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class FAQuestionsTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
