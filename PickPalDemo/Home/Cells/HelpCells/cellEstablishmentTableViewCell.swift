//
//  cellEstablishmentTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class cellEstablishmentTableViewCell: UITableViewCell {

    @IBOutlet weak var imageEstablishment: UIImageView!
    @IBOutlet weak var nombreEstablecimiento: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var nombreEst: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var total: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
