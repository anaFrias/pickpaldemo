//
//  EstablishmentHelpTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/20/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class EstablishmentHelpTableViewCell: UITableViewCell {

    @IBOutlet weak var deliveredTime: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var nameEstablishmentCard: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var nameEstablishment: UILabel!
    @IBOutlet weak var imageEstablishment: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
