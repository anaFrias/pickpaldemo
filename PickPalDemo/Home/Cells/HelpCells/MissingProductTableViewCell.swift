//
//  MissingProductTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/21/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol missingProductDelegate {
    func selectRow(indexPath: IndexPath)
}
class MissingProductTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedButton: UIButton!
    @IBOutlet weak var productTitle: UILabel!
    var delegate: missingProductDelegate!
    var indexPath = IndexPath()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func selectStuff(_ sender: Any) {
        if indexPath != nil {
            self.delegate.selectRow(indexPath: indexPath)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
