//
//  PagadosTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 10/29/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol porPagarCellDelegate {
    func desplegar(indexPath: IndexPath)
}

class PagadosTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pedidoPagadoLbl: UILabel!
    @IBOutlet weak var totales: UILabel!
    @IBOutlet weak var servicioPickpal: UILabel!
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var numPedido: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var hour_pagado: UILabel!
    @IBOutlet weak var establishmentName: UILabel!
    @IBOutlet weak var viewCellConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCustomerService: UIButton!
    
    @IBOutlet weak var lblTextTotal: UILabel!
    @IBOutlet weak var tiempoEntrega: UILabel!
    @IBOutlet weak var titleNotas: UILabel!
    @IBOutlet weak var lblTituloEstatus: UILabel!
    
    
    @IBOutlet weak var folioPdido: UILabel!
    
    
    @IBOutlet weak var titleServicioDomicilio: UILabel!
    @IBOutlet weak var viewPedidosHeight: NSLayoutConstraint!
    
    @IBOutlet weak var horaConfirmarPagado: UILabel!
    @IBOutlet weak var tituloConfirmarPedido: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var horaListo: UILabel!
    @IBOutlet weak var horaProceso: UILabel!
    @IBOutlet weak var horaPagado: UILabel!
    @IBOutlet weak var horaCreado: UILabel!
    
    @IBOutlet weak var pedidoListoTitle: UILabel!
    @IBOutlet weak var pedidoEnProcesoTitle: UILabel!
    @IBOutlet weak var peidoPagadoTitle: UILabel!
    
    @IBOutlet weak var barraListoConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var horaEstimadaRedHeader: UILabel!
    
    @IBOutlet weak var pedidoEnLabel: UILabel!
    @IBOutlet weak var recogeQRLabel: UILabel!
    @IBOutlet weak var horaEstimadaLabel: UILabel!
    @IBOutlet weak var veARecogerLabel: UILabel!
    
    @IBOutlet weak var viewPedidosPagados: UIView!
    
    @IBOutlet weak var viewPagadosContainer: UIView!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    
    @IBOutlet weak var propinaLabel: UILabel!
    
    @IBOutlet weak var propina: UILabel!
    @IBOutlet weak var lblPointsUsed: UILabel!
    
    @IBOutlet weak var titlePointsUsed: UILabel!
    @IBOutlet weak var servicioPickpalText: UILabel!
    
    @IBOutlet weak var lblAddressSend: UILabel!
    @IBOutlet weak var lblAddressSendValue: UILabel!
    
    @IBOutlet weak var lblConditionsOrder: UILabel!
    @IBOutlet weak var btnActionConditionsOrder: UIButton!
    
    @IBOutlet weak var servicioADomicilio: UILabel!
    @IBOutlet weak var ctlTopLabelConditions: NSLayoutConstraint!
    @IBOutlet weak var ctlTopBtnConditions: NSLayoutConstraint!
    @IBOutlet weak var ctlTopResponsabiity: NSLayoutConstraint!
    
    // Wallet
    
    @IBOutlet weak var textPickPalcash: UILabel!
    @IBOutlet weak var totalPickpalCash: UILabel!
    @IBOutlet weak var totalPaymentMethod: UILabel!
    @IBOutlet weak var textPaymentMethod: UILabel!
    @IBOutlet weak var lblTitleCard: UILabel!
    @IBOutlet weak var lblAmountCard: UILabel!
    
    @IBOutlet weak var lblTitleTotalPagado: UILabel!
    @IBOutlet weak var lblTotalPagado: UILabel!
    @IBOutlet weak var containerPagos: UIView!
    
    
   
    
    var modifier_price = [Double]()
    var modifier_cant = [Int]()
    var modifier_name = [String]()
    var modifier_cat = [String]()
    var isModifier = [Bool]()
    var name: String!
    var elements = [Dictionary<String,Any>]()
    var hasModCom = Bool()
    var modComp = [Int]()
    var delegate: porPagarCellDelegate!
    var indexPath = IndexPath()
    
    var dinamicHeight: CGFloat = 25
    var dinamicHeightAxu: CGFloat = 0
    
    @IBOutlet weak var dropdown: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        print(viewPedidosHeight)
        tableView.delegate = self
        tableView.dataSource = self
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func desplegar(_ sender: Any) {
        self.delegate.desplegar(indexPath: indexPath)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modifier_name.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print("Tamaño celda: \(dinamicHeight)")
        return dinamicHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !isModifier[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subItemItem", for: indexPath) as! PedidosActualesListaTableViewCell
            cell.pedidoLabel.text = modifier_name[indexPath.row]
            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.precioLabel.text = "\(formattedTipAmount)"
                }
//                cell.precioLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.precioLabel.text = "      "
            }
            
            
            dinamicHeight = cell.pedidoLabel.frame.height + 4
//            dinamicHeightAxu += dinamicHeight
            tableViewHeight.constant = dinamicHeight * CGFloat(modifier_name.count)
            print("Tamaño Tabla: \(dinamicHeightAxu)")
            tableView.beginUpdates()
            tableView.endUpdates()
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "complementsModifiers", for: indexPath) as! ModifCompleTableViewCell
            if modifier_cat[indexPath.row] == "" {
                cell.compModLabel.text = "Producto: " + modifier_name[indexPath.row]
            }else{
                cell.compModLabel.text = modifier_cat[indexPath.row] + ": " + modifier_name[indexPath.row]
            }
            
//            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            cell.cantidadLabel.text = "      "
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.totalLabel.text = "\(formattedTipAmount)"
                }
//                cell.totalLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.totalLabel.text = "      "
            }
            
            return cell
        }
    }

}
