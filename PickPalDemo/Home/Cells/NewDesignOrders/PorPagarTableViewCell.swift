//
//  PorPagarTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 10/29/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol cancelOrderDelegate {
    func cancelOrder(order_id: Int)
    func desplegarAction(indexPath:IndexPath)
}

class PorPagarTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleNotas: UILabel!
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var totalTable: UILabel!
    @IBOutlet weak var pickPalService: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderNumber: UILabel!
    @IBOutlet weak var qrImage: UIImageView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var establishment_name: UILabel!
    @IBOutlet weak var horaRecibido: UILabel!
    @IBOutlet weak var lblPayAlert: UILabel!
    @IBOutlet weak var lblTextTotal: UILabel!
    @IBOutlet weak var containerPagos: UIView!
    
    @IBOutlet weak var lblTotalCash: UILabel!
    @IBOutlet weak var lblTextCash: UILabel!
    @IBOutlet weak var labelRecibidoPorPagar: UILabel!
    @IBOutlet weak var labelFolioCompleto: UILabel!
    @IBOutlet weak var porPagarEnProceso: UILabel!
    @IBOutlet weak var cancelarPedidoBtn: UIButton!
    @IBOutlet weak var horaPorPagarListo: UILabel!
    @IBOutlet weak var porPagarListo: UILabel!
    @IBOutlet weak var labelTextQR: UILabel!
    @IBOutlet weak var costoEnvio: UILabel!
    @IBOutlet weak var labelHoraPorPagarEnproceso: UILabel!
    @IBOutlet weak var buttonConditions: UIButton!
    
    @IBOutlet weak var btnServicioAlCliente: UIButton!
    @IBOutlet weak var tituloConfirmarPedidoPorPagar: UILabel!
    @IBOutlet weak var horaConfirmarPedidoPorPagar: UILabel!
    @IBOutlet weak var labelMensajePropina: UILabel!
    @IBOutlet weak var mensajePropina: UILabel!
    @IBOutlet weak var propinaLabel: UILabel!
    @IBOutlet weak var tilePropina: UILabel!
    @IBOutlet weak var titleCostoEnvio: UILabel!
    @IBOutlet weak var labelTitleToPay: UILabel!
    @IBOutlet weak var titleAddres: UILabel!
    @IBOutlet weak var addresDelivery: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var modeLabel: UILabel!
    
    @IBOutlet weak var titlePonitsUsed: UILabel!
    @IBOutlet weak var lblPointsUsed: UILabel!
    
    @IBOutlet weak var ctlHeightCatMod: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewPedidoConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewMiPedido: UIView!
    
    @IBOutlet weak var viewCellConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewPedidosConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewLarge: UIView!
    
    @IBOutlet weak var notes: UILabel!
    
    @IBOutlet weak var lblSendOrder: UILabel!
    @IBOutlet weak var lblSendOrderValue: UILabel!
    
    @IBOutlet weak var btnConditionOrders: UIButton!
    @IBOutlet weak var lblConditionOrders: UILabel!
    
    @IBOutlet weak var ctlTopBtnConditions: NSLayoutConstraint!
    @IBOutlet weak var ctlTopLblConditions: NSLayoutConstraint!
    
    @IBOutlet weak var ctlTopSendOrderLabel: NSLayoutConstraint!
    @IBOutlet weak var ctlToSendOrderValueLabel: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblTitleTotalPagado: UILabel!
    @IBOutlet weak var lblTotalPagado: UILabel!
    @IBOutlet weak var lblTitleRestantePagar: UILabel!
    @IBOutlet weak var lblTotalRestantePagar: UILabel!
    
    
    
    var modifier_price = [Double]()
    var modifier_cant = [Int]()
    var modifier_name = [String]()
    var modifier_cat = [String]()
    var isModifier = [Bool]()
    var name: String!
    var elements = [Dictionary<String,Any>]()
    var hasModCom = Bool()
    var modComp = [Int]()
    var delegate: cancelOrderDelegate!
    var order_id = Int()
    var indexPath = IndexPath()
    
    var dinamicHeight: CGFloat = 25
    var dinamicHeightAxu: CGFloat = 0
    
    @IBOutlet weak var tienes15minutos: UILabel!
    @IBOutlet weak var dropdown: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        
        cancelarPedidoBtn.layer.cornerRadius = cancelarPedidoBtn.frame.height / 2
        cancelarPedidoBtn.layer.borderWidth = 0.8
        cancelarPedidoBtn.layer.borderColor = UIColor.init(red: 139/255, green: 149/255, blue: 156/255, alpha: 1).cgColor
//        print(tableView.frame.size.height)
//        print(viewMiPedido.frame.size.height)
//        print(viewCellConstraint.constant)
        
        // Initialization code
    }

    @IBAction func cancelOrder(_ sender: Any) {
        self.delegate.cancelOrder(order_id: self.order_id)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func desplegar(_ sender: Any) {
        self.delegate.desplegarAction(indexPath: indexPath)
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modifier_name.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dinamicHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var heightDinamic = 0
            for _ in modifier_name {
                heightDinamic += 25
            }
        ctlHeightCatMod.constant = CGFloat(heightDinamic)
        
        if !isModifier[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subItemItem", for: indexPath) as! PedidosActualesListaTableViewCell
            cell.pedidoLabel.text = modifier_name[indexPath.row]
            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.precioLabel.text = "\(formattedTipAmount)"
                }
//                cell.precioLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.precioLabel.text = ""
            }
            
            dinamicHeight = cell.pedidoLabel.frame.height + 4
            dinamicHeightAxu = dinamicHeight
            tableView.beginUpdates()
            tableView.endUpdates()
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "complementsModifiers", for: indexPath) as! ModifCompleTableViewCell
            if modifier_cat[indexPath.row] != "" {
                cell.compModLabel.text = modifier_cat[indexPath.row] + ": " + modifier_name[indexPath.row]
            }else{
                cell.compModLabel.text = "Producto: " + modifier_name[indexPath.row]
            }
        
//            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            cell.cantidadLabel.text = ""
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.totalLabel.text = "\(formattedTipAmount)"
                }
//                cell.totalLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.totalLabel.text = ""
            }
            
            return cell
        }
    }
    
}
