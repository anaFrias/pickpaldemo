//
//  ReembolsadosTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 10/29/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol openHelpDelegate {
    func openHelp(order_id: Int)
    func desplegar(indexPath: IndexPath)
}

class ReembolsadosTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var establishmentTitle: UILabel!
    
    @IBOutlet weak var viewCell: UIView!
    @IBOutlet weak var viewTotalConstraint: NSLayoutConstraint!
    @IBOutlet weak var qrSample: UIImageView!
    @IBOutlet weak var totalOrder: UILabel!
    @IBOutlet weak var numPedido: UILabel!
    @IBOutlet weak var buttonExpand: UIButton!
    
    @IBOutlet weak var pedidosView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var servicioLabel: UILabel!
    
    @IBOutlet weak var totalesLabel: UILabel!
    @IBOutlet weak var horaPagado: UILabel!
    @IBOutlet weak var horaProceso: UILabel!
    @IBOutlet weak var horaCancelado: UILabel!
    
    
    @IBOutlet weak var tableViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var pedidosViewConstraint: NSLayoutConstraint!

    
    @IBOutlet weak var muestraCodigo: UILabel!
    @IBOutlet weak var fechaLimiteLabel: UILabel!
    @IBOutlet weak var tienesHastaView: UIView!
    @IBOutlet weak var helpButton: UIButton!
    
//    VIEWS TO CHANGE
    @IBOutlet weak var pedidoRembolsoTitleRed: UILabel!
    
    @IBOutlet weak var instructionsBelow: UILabel!
    
    var modifier_price = [Double]()
    var modifier_cant = [Int]()
    var modifier_name = [String]()
    var isModifier = [Bool]()
    var name: String!
    var elements = [Dictionary<String,Any>]()
    var hasModCom = Bool()
    var modComp = [Int]()
    var delegate: openHelpDelegate!
    var indexPath = IndexPath()
    var orderId = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonExpand(_ sender: Any) {
        self.delegate.desplegar(indexPath: indexPath)
    }
    
    @IBAction func buttonHelp(_ sender: Any) {
        self.delegate.openHelp(order_id: orderId)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modifier_name.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !isModifier[indexPath.row] {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subItemItem", for: indexPath) as! PedidosActualesListaTableViewCell
            cell.pedidoLabel.text = modifier_name[indexPath.row]
            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.precioLabel.text = "\(formattedTipAmount)"
                }
//                cell.precioLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.precioLabel.text = "      "
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "complementsModifiers", for: indexPath) as! ModifCompleTableViewCell
            cell.compModLabel.text = modifier_name[indexPath.row]
//            cell.cantidadLabel.text = "\(modifier_cant[indexPath.row])"
            cell.cantidadLabel.text = "      "
            if modifier_price[indexPath.row] != 0.0 {
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: modifier_price[indexPath.row] as NSNumber) {
                    cell.totalLabel.text = "\(formattedTipAmount)"
                }
//                cell.totalLabel.text = "$\(modifier_price[indexPath.row])0"
            }else{
                cell.totalLabel.text = "      "
            }
            
            return cell
        }
    }
}
