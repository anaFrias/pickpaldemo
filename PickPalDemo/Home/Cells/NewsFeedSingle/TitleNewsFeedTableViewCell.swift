//
//  TitleNewsFeedTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/4/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class TitleNewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var title1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
