//
//  SettingsResumeTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SettingsResumeTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        img.layer.cornerRadius = 70
        img.clipsToBounds = true
        img.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
