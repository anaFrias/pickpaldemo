//
//  ProfileAddressTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol EditProfileAddress {
    func clickEditAddress(indexPath: IndexPath)
    func clickDeleteAddress(indexPath: IndexPath)
}
class ProfileAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var RFC: UILabel!
    @IBOutlet weak var direccion: UILabel!
    var delegate: EditProfileAddress!
    var indexPath: IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func edit(_ sender: Any) {
        self.delegate.clickEditAddress(indexPath: indexPath)
    }
    
    @IBAction func deleteAddress(_ sender: Any) {
        self.delegate.clickDeleteAddress(indexPath: indexPath)
    }
    
}
