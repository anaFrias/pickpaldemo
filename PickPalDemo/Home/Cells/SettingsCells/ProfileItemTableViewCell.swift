//
//  ProfileItemTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ProfileItemTableViewCell: UITableViewCell {

    @IBOutlet weak var item1Title: UILabel!
    @IBOutlet weak var item1Txt: UILabel!
    @IBOutlet weak var item2Title: UILabel!
    @IBOutlet weak var item2Txt: UILabel!
    @IBOutlet weak var divisor: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
