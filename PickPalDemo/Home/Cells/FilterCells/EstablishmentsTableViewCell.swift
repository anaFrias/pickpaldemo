//
//  EstablishmentsTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol singleEstDelegate {
    func goSingleEstablishment(indexPath: IndexPath)
}

class EstablishmentsTableViewCell: UITableViewCell {

    @IBOutlet weak var noAvaliable: UIView!
    @IBOutlet weak var establishmentImage: UIImageView!
    @IBOutlet weak var establishmentName: UILabel!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star1: UIImageView!
    
    @IBOutlet weak var bluredImage: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var noDisponible: UIView!
    @IBOutlet weak var establecimiento: UIView!
    @IBOutlet weak var txtTime: UILabel!
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var buttonGoSingle: UIButton!
    
    @IBOutlet weak var iconCerrado: UIImageView!
    
    //pedidos disponibles
    @IBOutlet weak var imgServiceEst: UIImageView!
    @IBOutlet weak var imgServiceBar: UIImageView!
    @IBOutlet weak var imgServiceTable: UIImageView!
    @IBOutlet weak var imgServiceHome: UIImageView!
    
    
    @IBOutlet weak var sinResultados: UIView!
    @IBOutlet weak var btnFoodAndDrinks: UIImageView!
    @IBOutlet weak var btnPromos: UIImageView!
    @IBOutlet weak var btnGuia: UIImageView!
    @IBOutlet weak var btnPoints: UIImageView!
    
    var indexPath = IndexPath()
    var homeDelegateFlag = Bool()
    var delegate: singleEstDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let lightBlur = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: lightBlur)
        blurEffectView.frame = bluredImage.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        bluredImage.addSubview(blurEffectView)
        
        bluredImage.clipsToBounds = true
        if homeDelegateFlag {
            buttonGoSingle.alpha = 1
        }
    }

    func rate (rating: Int) {
        if rating == 5 {
            star1.image = UIImage(named: "iconStarFilled")
            star2.image = UIImage(named: "iconStarFilled")
            star3.image = UIImage(named: "iconStarFilled")
            star4.image = UIImage(named: "iconStarFilled")
            star5.image = UIImage(named: "iconStarFilled")
        }else if rating == 4 {
            star1.image = UIImage(named: "iconStarFilled")
            star2.image = UIImage(named: "iconStarFilled")
            star3.image = UIImage(named: "iconStarFilled")
            star4.image = UIImage(named: "iconStarFilled")
            star5.image = UIImage(named: "iconStarEmpty")
        }else if rating == 3 {
            star1.image = UIImage(named: "iconStarFilled")
            star2.image = UIImage(named: "iconStarFilled")
            star3.image = UIImage(named: "iconStarFilled")
            star4.image = UIImage(named: "iconStarEmpty")
            star5.image = UIImage(named: "iconStarEmpty")
        }else if rating == 2 {
            star1.image = UIImage(named: "iconStarFilled")
            star2.image = UIImage(named: "iconStarFilled")
            star3.image = UIImage(named: "iconStarEmpty")
            star4.image = UIImage(named: "iconStarEmpty")
            star5.image = UIImage(named: "iconStarEmpty")
        }else if rating == 1 {
            star1.image = UIImage(named: "iconStarFilled")
            star2.image = UIImage(named: "iconStarEmpty")
            star3.image = UIImage(named: "iconStarEmpty")
            star4.image = UIImage(named: "iconStarEmpty")
            star5.image = UIImage(named: "iconStarEmpty")
        }else{
            star1.image = UIImage(named: "iconStarEmpty")
            star2.image = UIImage(named: "iconStarEmpty")
            star3.image = UIImage(named: "iconStarEmpty")
            star4.image = UIImage(named: "iconStarEmpty")
            star5.image = UIImage(named: "iconStarEmpty")
        }
    }
    
//    @IBAction func goSingle(_ sender: Any) {
//    }
    
    @IBAction func goSingle(_ sender: Any) {
        guard let del = delegate else{
            return
        }
        del.goSingleEstablishment(indexPath: indexPath)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none

        // Configure the view for the selected state
    }

}
