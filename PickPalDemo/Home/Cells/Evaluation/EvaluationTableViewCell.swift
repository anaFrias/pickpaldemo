//
//  EvaluationTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol changeLikeStatus {
    func changeLike(position: Int, status: Bool)
}
class EvaluationTableViewCell: UITableViewCell {

    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    var delegate: changeLikeStatus!
    var position: Int!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func dislike(_ sender: Any) {
        btnLike.setImage(UIImage(named: "likeUnselected"), for: UIControlState.normal)
        btnDislike.setImage(UIImage(named: "dislikeSelected"), for: UIControlState.normal)
        delegate.changeLike(position: position, status: false)
    }
    @IBAction func like(_ sender: Any) {
        btnLike.setImage(UIImage(named: "likeSelected"), for: UIControlState.normal)
        btnDislike.setImage(UIImage(named: "dislikeUnselected"), for: UIControlState.normal)
        delegate.changeLike(position: position, status: true)
    }
}
