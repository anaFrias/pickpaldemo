//
//  ItemsCollectionTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ItemsCollectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    var items = [Categories]()
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        // Initialization code
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "itm", for: indexPath) as! ItemFoodCollectionViewCell
        item.itemTitle.text = items[indexPath.item].name
        if indexPath.item == 0 {
            item.currentIndicator.alpha = 1
        }else{
            item.currentIndicator.alpha = 0
        }
        return item
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as! ItemFoodCollectionViewCell
        item.currentIndicator.alpha = 1
        
        for i in 0..<items.count {
            if i != indexPath.item {
                let item = collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as! ItemFoodCollectionViewCell
                item.currentIndicator.alpha = 0
            }
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
