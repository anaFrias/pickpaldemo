//
//  InfoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import GoogleMaps

class InfoTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var DescriptionLabel: UILabel!
    @IBOutlet weak var DescriptionText: UILabel!
    @IBOutlet weak var scheduleInfo: UILabel!
    @IBOutlet weak var scheduleTitle: UILabel!
    @IBOutlet weak var categoryDescr: UILabel!
    @IBOutlet weak var categoryTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let camera = GMSCameraPosition.camera(withLatitude: 20.6535815, longitude: -100.4296307, zoom: 6.0)
        let viewMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        print(viewMap)
        viewMap.isMyLocationEnabled = true

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = viewMap
        
        mapView = viewMap
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
