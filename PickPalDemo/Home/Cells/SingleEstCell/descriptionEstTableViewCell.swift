//
//  descriptionEstTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 3/4/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class descriptionEstTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescrip: UILabel!
        @IBOutlet weak var lblServicioADomicilio: UILabel!
        @IBOutlet weak var lblMetodoPago: UILabel!
        @IBOutlet weak var lblHorario: UILabel!
        @IBOutlet weak var lblCategory: UILabel!
        @IBOutlet weak var lblDireccion: UILabel!
        @IBOutlet weak var lblRedesSociales: UILabel!
        @IBOutlet weak var btnWeb: UIButton!
        @IBOutlet weak var btnFb: UIButton!
        @IBOutlet weak var btnInsta: UIButton!
        @IBOutlet weak var btnTwitter: UIButton!
        @IBOutlet weak var stackSocialMedia: UIStackView!
        
    @IBOutlet weak var labelTelefono: UILabel!
    var singleInfo: SocialInformation!
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        
        @IBAction func goWeb(_ sender: Any) {
            self.launchUrl(url: singleInfo.siteURL)
        }
        
        @IBAction func goInstagran(_ sender: Any) {
            self.launchUrl(url: singleInfo.instagramURL)
        }
        
        @IBAction func goFacebook(_ sender: Any) {
            self.launchUrl(url: singleInfo.facebookURL)
        }
        
        @IBAction func goTwitter(_ sender: Any) {
            self.launchUrl(url: singleInfo.twitterURL)
        }
        
        
        
        func launchUrl(url: String){
            if let reviewURL = URL(string: url), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        }

    }
