//
//  SingleItemTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol singleItemDelegate {
    func sendItem(cantIndividual: Int, totalItemPrice: Double, totalItemCant: Int, name: String, index: IndexPath, price: Double, id: Int, isPackage: Bool)
    func addItem(price: Double, cant: Int, name: String, indexPath: IndexPath, id: Int, isPackage: Bool)
    func deleteItem(price: Double, cant: Int, name: String, indexPath: IndexPath, id: Int, isPackage: Bool)
    func zoomIn(indexPath: IndexPath)
}
class SingleItemTableViewCell: UITableViewCell {

    @IBOutlet weak var disableBtn: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var numberOfItems: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var cantPedidos: UIView!
    @IBOutlet weak var cantPedidosLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var outOfStockView: UIView!
    @IBOutlet weak var masBtn: UIButton!
    @IBOutlet weak var menosBtn: UIButton!
    
    
    var cant = 0
    var priceString = 0.00
    var delegate: singleItemDelegate!
    var indexPath: IndexPath!
    var name: String!
    var totalPrice = Double()
    var totalCant = Int()
    var idItem = Int()
    var isPackage = Bool()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func plusAction(_ sender: Any) {
        self.delegate.addItem(price: priceString, cant: cant, name: name, indexPath: indexPath, id: idItem, isPackage: isPackage)
    }
    
    @IBAction func minusAction(_ sender: Any) {
        if cant != 0{
            self.delegate.deleteItem(price: priceString, cant: cant, name: name, indexPath: indexPath, id: idItem, isPackage: isPackage)
        }
        
    }
    
    @IBAction func zoomIn(_ sender: Any) {
        self.delegate.zoomIn(indexPath: indexPath)
    }
    
    @IBAction func addButton(_ sender: Any) {
        totalPrice = (priceString * Double(cant))
        totalCant = cant
        self.delegate.sendItem(cantIndividual: cant, totalItemPrice: totalPrice, totalItemCant: totalCant, name: name, index: indexPath, price: priceString, id: idItem, isPackage: isPackage)
    }
}
