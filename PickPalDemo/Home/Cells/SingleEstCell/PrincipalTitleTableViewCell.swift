//
//  PrincipalTitleTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol infoDelegate {
    func openInfo(open: String, indexPath: IndexPath)
}

class PrincipalTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var nameEstablish: UILabel!
    @IBOutlet weak var logo: UIImageView!
    var indexPath = IndexPath()
    var delegate: infoDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func showInfo(_ sender: Any) {
        self.delegate.openInfo(open: "info", indexPath: indexPath)
    }
    
    
    @IBAction func showLocation(_ sender: Any) {
    }
}
