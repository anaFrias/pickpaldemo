//
//  AddressSettingsCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 23/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol actionsAddresDelegate {
    func editAddres()
    func deleteAddress()
}

class AddressSettingsCollectionViewCell: TableViewCell {
    
    @IBOutlet weak var viewDireccion: UIView!
    var delegate: actionsAddresDelegate!
    @IBOutlet weak var viewBottom: UIStackView!
    
    @IBOutlet weak var iconAddress: UIImageView!
    @IBOutlet weak var addresLabel: UILabel!
    
    @IBOutlet weak var backgraundCell: UIView!
    
    @IBAction func editAddressBtn(_ sender: Any) {
        
     //   delegate.editAddres()
        
       // viewDireccion.layer.cornerRadius = 15
        
        viewDireccion.clipsToBounds = false
        viewDireccion.layer.masksToBounds = false
        viewDireccion.layer.cornerRadius = 5
        
    }
    
    
    @IBOutlet weak var buttonEditAddress: UIButton!
    
    @IBOutlet weak var buttonDeleteAddress: UIButton!
    
    
    @IBAction func deleteAddressBtn(_ sender: Any) {
        
        print("")
        delegate.deleteAddress()
        
    }
    
    
        
}
