//
//  sectionLabelTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/27/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class sectionLabelTableViewCell: UITableViewCell {

    @IBOutlet weak var sectionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
