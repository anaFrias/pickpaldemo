//
//  ModifCompleTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/5/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ModifCompleTableViewCell: UITableViewCell {

    @IBOutlet weak var compModLabel: UILabel!
    @IBOutlet weak var cantidadLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
