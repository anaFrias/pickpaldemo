//
//  complementModifierTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 2/26/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class complementModifierTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var priceItem: UILabel!
    @IBOutlet weak var nameItem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
