//
//  itemSingleItemPedidoTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 08/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class itemSingleItemPedidoTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
