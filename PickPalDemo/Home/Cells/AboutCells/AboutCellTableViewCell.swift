//
//  AboutCellTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/3/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class AboutCellTableViewCell: UITableViewCell {

    @IBOutlet weak var titleAbout: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
