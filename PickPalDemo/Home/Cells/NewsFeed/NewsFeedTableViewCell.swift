//
//  NewsFeedTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 13/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var blur: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var category: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        img.layer.cornerRadius = 5
        img.clipsToBounds = true
        img.layer.masksToBounds = true
        blur.layer.cornerRadius = 5
        blur.clipsToBounds = true
        blur.layer.masksToBounds = true
        let lightBlur = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: lightBlur)
        blurEffectView.frame = blur.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blur.addSubview(blurEffectView)
        
        blur.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
