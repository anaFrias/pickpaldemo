//
//  NewsFeedBigTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 14/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class NewsFeedBigTableViewCell: UITableViewCell {

    @IBOutlet weak var blur: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var category: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let lightBlur = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: lightBlur)
        blurEffectView.frame = blur.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blur.addSubview(blurEffectView)
        
        blur.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
