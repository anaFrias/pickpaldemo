//
//  EstadosDisponiblesCollectionViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/21/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

class EstadosDisponiblesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var estadoLabel: UILabel!
}
