//
//  continueNewSurveyTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/9/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol continueButtonDelegate {
    func continueButton()
}

class continueNewSurveyTableViewCell: UITableViewCell {

    var delegate: continueButtonDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func continuarBtn(_ sender: Any) {
        self.delegate.continueButton()
    }
}
