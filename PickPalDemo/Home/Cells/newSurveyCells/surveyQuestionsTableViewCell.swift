//
//  surveyQuestionsTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/7/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol buttonsProtocol {
    func buttonsInfo(value: Int, type: Int)
}

class surveyQuestionsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var superHappy: UIButton!
    @IBOutlet weak var happier: UIButton!
    @IBOutlet weak var happy: UIButton!
    @IBOutlet weak var neutral: UIButton!
    @IBOutlet weak var sad: UIButton!
    @IBOutlet weak var superSad: UIButton!
    var delegate: buttonsProtocol!
    var category = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func superSadBtn(_ sender: Any) {
        if superSad.isSelected {
            superSad.isSelected = false
        }else{
            superSad.isSelected = true
            sad.isSelected = false
            neutral.isSelected = false
            happy.isSelected = false
            happier.isSelected = false
            superHappy.isSelected = false
            self.delegate.buttonsInfo(value: 1, type: category)
        }
    }
    @IBAction func sadBtn(_ sender: Any) {
        if sad.isSelected {
            sad.isSelected = false
        }else{
            sad.isSelected = true
            superSad.isSelected = false
            neutral.isSelected = false
            happy.isSelected = false
            happier.isSelected = false
            superHappy.isSelected = false
            self.delegate.buttonsInfo(value: 2, type: category)
        }
    }
    @IBAction func neutralBtn(_ sender: Any) {
        if neutral.isSelected {
            neutral.isSelected = false
        }else{
            neutral.isSelected = true
            superSad.isSelected = false
            sad.isSelected = false
            happy.isSelected = false
            happier.isSelected = false
            superHappy.isSelected = false
            self.delegate.buttonsInfo(value: 3, type: category)
        }
    }
    @IBAction func happyBtn(_ sender: Any) {
        if happy.isSelected {
            happy.isSelected = false
        }else{
            happy.isSelected = true
            superSad.isSelected = false
            sad.isSelected = false
            neutral.isSelected = false
            happier.isSelected = false
            superHappy.isSelected = false
            self.delegate.buttonsInfo(value: 4, type: category)
        }
    }
    @IBAction func happierBtn(_ sender: Any) {
        if happier.isSelected {
            happier.isSelected = false
        }else{
            happier.isSelected = true
            superSad.isSelected = false
            sad.isSelected = false
            neutral.isSelected = false
            happy.isSelected = false
            superHappy.isSelected = false
            self.delegate.buttonsInfo(value: 5, type: category)
        }
    }
    @IBAction func superHappyBtn(_ sender: Any) {
        if superHappy.isSelected {
            superHappy.isSelected = false
        }else{
            superHappy.isSelected = true
            superSad.isSelected = false
            sad.isSelected = false
            neutral.isSelected = false
            happy.isSelected = false
            happier.isSelected = false
            self.delegate.buttonsInfo(value: 6, type: category)
        }
    }
    
}
