//
//  CollectionEstablishmentTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
protocol categoriesSelection {
    func selectedCategory(index: IndexPath)
}
class CollectionEstablishmentTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var values = [KitchensComplete]()
    var delegate: categoriesSelection!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func prepareForReuse() {
        collectionView.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate.selectedCategory(index: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return values.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KitchenType", for: indexPath) as! CollectionItemCollectionViewCell
        cell.viewContainer.layer.cornerRadius = 5
        cell.itemImage.layer.cornerRadius = 5
        cell.viewContainer.clipsToBounds = true
        cell.itemImage.clipsToBounds = true
        if let array = values[indexPath.row].image{
            if values[indexPath.row].image == ""{
                cell.itemImage.image = UIImage(named: "placeholder_items")
            }else{
                Nuke.loadImage(with: URL(string: values[indexPath.row].image)!, into: cell.itemImage)
            }
        }else{
            cell.itemImage.image = UIImage(named: "placeholder_items")
        }
        
        cell.kitchenName.text = values[indexPath.row].name
        if(values[indexPath.row].nearby == 0){
            cell.nearbyLabel.alpha = 0
        }else{
          //  cell.nearbyLabel.alpha = 1
            cell.nearbyLabel.text = "(\(values[indexPath.row].nearby!) cerca de ti)"
             cell.viewContainer.alpha = 0
        }
        if let hide = values[indexPath.row].hide{
            
            if(hide) {
               
                cell.viewContainer.alpha = 0.45
                cell.isUserInteractionEnabled = false
                
            }else{
               
                cell.viewContainer.alpha = 0
                cell.isUserInteractionEnabled = true
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 112.5
        let height: CGFloat = 112.5
        return CGSize(width: width, height: height)
        
        
    }
    
}
