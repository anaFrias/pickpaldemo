//
//  ImagePhotoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol getImageDelegate {
    func getImage()
}
class ImagePhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var buttonImage: UIButton!
    var delegate: getImageDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageUser.layer.cornerRadius = imageUser.frame.height / 2
        imageUser.clipsToBounds = true
    }

    @IBAction func uploadImage(_ sender: Any) {
        self.delegate.getImage()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
