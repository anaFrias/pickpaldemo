//
//  UserInfoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol setBirthdayDelegate {
    func setBirthday()
    func sendInfo(first_name: String, last_name: String, mail: String, phone: String, gender: String, birthday: String, zipCode: String)
    func validatePhone(correctPhone: Bool, phone: String, change: Bool)
    func validateEmail(succededInfo: Bool, change: Bool)
}
class UserInfoTableViewCell: UITableViewCell, UITextFieldDelegate, userDelegate {

    @IBOutlet weak var heightMascButton: NSLayoutConstraint!
    @IBOutlet weak var heightFemButton: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var wrong_phone: UIImageView!
    @IBOutlet weak var wrong_name: UIImageView!
    @IBOutlet weak var mail_wrong_text: UILabel!
    @IBOutlet weak var wrong_icon: UIImageView!
    @IBOutlet weak var wrongGender: UIImageView!
    @IBOutlet weak var wrongBirthday: UIImageView!
    @IBOutlet weak var phoneWrongText: UILabel!
    
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var masc: UIButton!
    @IBOutlet weak var fem: UIButton!
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var FullName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var zipCodeLabel: UITextField!
    
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var zipCodeButton: UIButton!
    
    @IBOutlet weak var name_view: UIView!
    @IBOutlet weak var mail_view: UIView!
    @IBOutlet weak var phone_view: UIView!
    @IBOutlet weak var gender_view: UIView!
    @IBOutlet weak var birthday_view: UIView!
    @IBOutlet weak var zp_view: UIView!

    @IBOutlet weak var btnGender: UIButton!
    
    var gender = String()
    var date = String()
    var birthdate = String()
    var phone = String()
    var phoneLocal = String()
    var emailLocal = String()
    var delegate: setBirthdayDelegate!
    var showGenderFlag = false
    var userws = UserWS()
    var succedMail = true
    var succedPhone = true
    var hasChange = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.heightMascButton.constant = 0
        self.heightFemButton.constant = 0
        self.heightConstraint.constant = 0
        self.fem.alpha = 0
        self.masc.alpha = 0
        date = "Fecha de Nacimiento"
        birthday.text = date
        phoneNumber.delegate = self
        FullName.delegate = self
        email.delegate = self
        userws.delegate = self
        
    }
    @IBAction func genreButton(_ sender: Any) {
        if showGenderFlag {
            self.masc.alpha = 0
            self.fem.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.layoutIfNeeded()
                self.heightConstraint.constant = 0
                self.heightFemButton.constant = 0
                self.heightMascButton.constant = 0
                
            }) { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                    self.showGenderFlag = false
                })
            }
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.layoutIfNeeded()
                self.heightConstraint.constant = 52
                self.heightFemButton.constant = 26
                self.heightMascButton.constant = 26
                self.masc.alpha = 1
                self.fem.alpha = 1
            }) { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                    self.showGenderFlag = true
                })
            }
        }
        
    }
    @IBAction func femAction(_ sender: Any) {
        self.masc.alpha = 0
        self.fem.alpha = 0
        wrongGender.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
            self.heightConstraint.constant = 0
            self.heightFemButton.constant = 0
            self.heightMascButton.constant = 0
            
        }) { (finished) in
            self.genre.text = "Femenino"
            self.gender = "F"
            self.genre.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            UIView.animate(withDuration: 0.2, animations: {
                self.downArrow.transform = CGAffineTransform.identity
            })
        }
        
    }
    
    @IBAction func saveProfile(_ sender: Any) {
        if FullName.text != "", email.text != "", genre.text != "Sexo", birthday.text != "Fecha de Nacimiento" {
            if succedPhone, succedMail {
                let str = FullName.text!
                if let range: Range<String.Index> = str.range(of: " ") {
                    let last_name = str.substring(from: range.upperBound)
                    let substring_fn = str.substring(to: range.upperBound)
                    let first_name = substring_fn.trimmingCharacters(in: .whitespaces)
                    self.delegate.sendInfo(first_name: first_name, last_name: last_name, mail: email.text!, phone: phone, gender: gender, birthday: birthday.text!, zipCode: zipCodeLabel.text!)
                }else {
                    self.delegate.sendInfo(first_name: FullName.text!, last_name: "", mail: email.text!, phone: phone, gender: gender, birthday: birthday.text!, zipCode: zipCodeLabel.text!)
                }
            }else{
//                self.delegate.sendInfo(first_name: "", last_name: "", mail: "", phone: "", gender: "", birthday: "", zipCode: "")
            }
            
        }else{
            if genre.text == "Sexo" {
                wrongGender.alpha = 1
            }
            if birthday.text == "Fecha de Nacimiento" {
                wrongBirthday.alpha = 1
            }
            if FullName.text == "" {
                wrong_name.alpha = 1
            }
            
            if email.text == "" {
                wrong_icon.alpha = 1
            }
            if phoneNumber.text == "" {
                wrong_phone.alpha = 1
            }
            self.delegate.sendInfo(first_name: "", last_name: "", mail: "", phone: "", gender: "", birthday: "", zipCode: "")
        }
    
    }
    @IBAction func birthdayButton(_ sender: Any) {
        wrongBirthday.alpha = 0
        self.delegate.setBirthday()
    }
    
    @IBAction func mascAction(_ sender: Any) {
        self.masc.alpha = 0
        self.fem.alpha = 0
        wrongGender.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
            self.heightConstraint.constant = 0
            self.heightFemButton.constant = 0
            self.heightMascButton.constant = 0
            
        }) { (finished) in
            self.genre.text = "Masculino"
            self.gender = "M"
            self.genre.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            UIView.animate(withDuration: 0.2, animations: {
                self.downArrow.transform = CGAffineTransform.identity
            })
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10 // Bool
        }else{
            return true
        }

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneNumber {
            if phoneNumber.text != ""{
                if Extensions.format(phoneNumber: phoneLocal)  == phoneNumber.text {
                    wrong_phone.alpha = 0
                    phoneWrongText.alpha = 0
                    succedPhone = true
                    hasChange = false
                }else{
                    if phoneLocal == phoneNumber.text! {
                        succedPhone = true
                        hasChange = false
                        phoneWrongText.alpha = 0
                        wrong_phone.alpha = 0
                    }else{
                        if (phoneNumber.text?.count)! == 10 {
                            phone = phoneNumber.text!
                            userws.validatePhoneExist(phone: phoneNumber.text!)
                        }else{
                            succedPhone = false
                            hasChange = false
                            wrong_phone.alpha = 1
                            phoneWrongText.text = "El teléfono celular proporcionado no es válido"
                            phoneWrongText.alpha = 1
                        }
                    }
                    
                }
                
            }else{
                succedPhone = false
                hasChange = false
                wrong_phone.alpha = 1
            }
            if let formattedPhoneNumber = Extensions.format(phoneNumber: textField.text!) {
                textField.text = formattedPhoneNumber
            }
        }

        if textField == email {
            if textField.text == "" {
                wrong_icon.alpha = 1
            }else{
                if emailLocal == textField.text! {
                    succedMail = true
                    wrong_icon.alpha = 0
                    mail_wrong_text.alpha = 0
                }else{
                    if isValidEmail(testStr: textField.text!) {
                        userws.ValidateEmail(email: textField.text!)
                    }else{
                        wrong_icon.alpha = 1
                        mail_wrong_text.text = "El correo proporcionado no es válido"
                        mail_wrong_text.alpha = 1
                    }
                }
                
            }
        }
        if textField == FullName {
            if textField.text == "" {
                wrong_name.alpha = 1
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == email {
            wrong_icon.alpha = 0
        }
        
        if textField == phoneNumber {
            wrong_phone.alpha = 0
            textField.text = ""
        }
        
        if textField == FullName {
            wrong_name.alpha = 0
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func didSuccessValidateEmail() {
        succedMail = true
        wrong_icon.alpha = 0
        mail_wrong_text.alpha = 0
        self.delegate.validateEmail(succededInfo: true, change: true)
        
    }
    func didFailValidateEmail(title: String, subtitle: String) {
        succedMail = false
        wrong_icon.alpha = 1
        mail_wrong_text.text = "El correo proporcionado ya existe"
        mail_wrong_text.alpha = 1
        self.delegate.validateEmail(succededInfo: false, change: false)
    }
    func didSuccessValidatePhoneExist() {
        succedPhone = true
        hasChange = true
        wrong_phone.alpha = 0
        phoneWrongText.alpha = 0
        print(phoneNumber.text!)
        self.delegate.validatePhone(correctPhone: true, phone: phone, change: hasChange)
    }
    func didFailValidatePhoneExist(title: String, subtitle: String) {
        succedPhone = false
        hasChange = false
        wrong_phone.alpha = 1
        phoneWrongText.text = "El teléfono celular ya existe"
        phoneWrongText.alpha = 1
        self.delegate.validatePhone(correctPhone: false, phone: phoneNumber.text!, change: hasChange)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func tapInto(_ sender: UIButton) {
        if sender == nameButton {
            FullName.becomeFirstResponder()
        }else if sender == mailButton {
            email.becomeFirstResponder()
        }else if sender == phoneButton {
            phoneNumber.becomeFirstResponder()
        }else{
            zipCodeLabel.becomeFirstResponder()
        }
    }
    
}
