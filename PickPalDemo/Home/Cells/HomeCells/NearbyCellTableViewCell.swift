//
//  NearbyCellTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 29/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
protocol nearbyActionsDelegate {
    func nearbyTapAction(id: Int, name: String, indexPath: IndexPath)
}
class NearbyCellTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    @IBOutlet weak var divider: UIView!
    @IBOutlet weak var direction: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var loSentimos: UILabel!
    @IBOutlet weak var sadLogo: UIImageView!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var dots: UIPageControl!
    @IBOutlet weak var slider: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var noNearby: UIView!
    
    @IBOutlet weak var queRicoTitle: UILabel!
    
    @IBOutlet weak var fadedImage: UIImageView!
    var delegate: nearbyActionsDelegate!
    var images = [String]()
    var viewFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var nearbyPlaces = [Places]()
    var txtDireccion = String()
    override func awakeFromNib() {
        super.awakeFromNib()
        images.removeAll()
//        for p in nearbyPlaces{
//            images.append(p.image!)
//            print(p.time)
//        }
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        collectionView.contentOffset.x = 0
//        generateSlider()
//        direction.text = txtDireccion
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func generateSlider(){
        if nearbyPlaces.count == 0{
            noNearby.alpha = 1
            //dots.alpha = 1
            direction.alpha = 1
            subtitle.alpha = 1
            loSentimos.alpha = 1
            sadLogo.alpha = 1
            emptyImage.alpha = 1
            divider.alpha = 1
            collectionView.alpha = 0
        }else{
            direction.alpha = 0
            subtitle.alpha = 0
            loSentimos.alpha = 0
            sadLogo.alpha = 0
            emptyImage.alpha = 0
            divider.alpha = 0
            collectionView.alpha = 1
            noNearby.alpha = 0
        }
        //var width = 0
        
        /*if images.count > 0 {
            for index in 0...images.count - 1{
                viewFrame.origin.x = slider.frame.size.width * CGFloat(index)
                viewFrame.size = slider.frame.size
                
                let view = UIView(frame: viewFrame)
                view.bounds.size.height = slider.frame.size.height
                //            view.backgroundColor = UIColor.red
                //            let image = UIImageView(image: images[index])
                var image = UIImageView()
                Nuke.loadImage(with: URL(string: images[index])!, into: image)
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: slider.frame.size)
                view.clipsToBounds = true
                
                
                slider.addSubview(image)
                //imageView.image = images[index]
                
                width += Int(slider.frame.width)
                
                self.slider.addSubview(view)
            }
            
            slider.contentSize = CGSize(width: (slider.frame.size.width * CGFloat(images.count)), height: slider.frame.size.height)
            
            slider.delegate = self
        }*/
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.nearbyTapAction(id: nearbyPlaces[indexPath.row].placeId, name: nearbyPlaces[indexPath.row].placeName, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return nearbyPlaces.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "home_slider_item", for: indexPath) as! SliderItemCollectionViewCell
//        cell.logoContainer.layer.cornerRadius = 22
        cell.img.layer.cornerRadius = 7
//        cell.logoContainer.clipsToBounds = true
        cell.img.clipsToBounds = true
        cell.fadedImage.layer.cornerRadius = 7
        cell.fadedImage.clipsToBounds = true
        let lightBlur = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: lightBlur)
        blurEffectView.frame = cell.bluredImage.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        cell.bluredImage.addSubview(blurEffectView)
        cell.bluredImage.clipsToBounds = true
        cell.bluredImage.layer.cornerRadius = 7
        if nearbyPlaces[indexPath.row].image != "" {
            Nuke.loadImage(with: URL(string: nearbyPlaces[indexPath.row].image!)!, into: cell.bluredImage)
            Nuke.loadImage(with: URL(string: nearbyPlaces[indexPath.row].image!)!, into: cell.img)
        }
    
        
//        if nearbyPlaces[indexPath.row].logo != ""{
//             Nuke.loadImage(with: URL(string: nearbyPlaces[indexPath.row].logo!)!, into: cell.logo)
//        }
        cell.time.text = nearbyPlaces[indexPath.row].time + " min"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 112.5
        let height: CGFloat = 112.5
        return CGSize(width: width, height: height)
        
        
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
//        dots.currentPage = Int(pageNumber)
//        dots.currentPageIndicatorTintColor = UIColor.white
//    }
}
