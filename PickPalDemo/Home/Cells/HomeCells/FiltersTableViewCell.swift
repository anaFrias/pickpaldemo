//
//  FiltersTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 04/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol cellActionsDelegate {
    func openModalAndSingle(modal: Bool, place: Bool, category: Bool, city: Bool)
}

class FiltersTableViewCell: UITableViewCell,  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, openModalDelegate, openSingleDelegate {
    
    @IBOutlet weak var collectionViewMenu: UICollectionView!

    @IBOutlet weak var nearbyText: UILabel!
    var delegate: cellActionsDelegate!
    var valueInfo = "Municipio"
    var valueCat = "Categoría"
    var valuePlace = "Ubicación"
    var defaults = UserDefaults.standard
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewMenu.delegate = self
        collectionViewMenu.dataSource = self
        // Initialization code
    }

    func openSingle(municipio: String, categoria: String, lugar: String) {
        self.delegate.openModalAndSingle(modal: false, place: false, category: false, city: false)
    }
    
    func openModal(place: Bool, category: Bool, city: Bool, bussinesLine: Bool, type: Bool) {
        self.delegate.openModalAndSingle(modal: true, place: place, category: category, city: city)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionViewMenu.dequeueReusableCell(withReuseIdentifier: "municipio", for: indexPath) as! CollectionViewCell
            cell.delegate = self
            cell.place = false
            cell.category = false
            cell.city = true
            cell.cityLabel.text = valueInfo
            return cell
        }else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "separator2", for: indexPath)
            return cell
        }else if indexPath.row == 2 {
            let cell = collectionViewMenu.dequeueReusableCell(withReuseIdentifier: "categoria", for: indexPath) as! CollectionViewCell
            cell.delegate = self
            cell.place = false
            cell.category = true
            cell.city = false
            cell.cityLabel.text = valueCat
            return cell
        }else if indexPath.item == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "separator3", for: indexPath)
            return cell
        }else if indexPath.item == 4 {
            let cell = collectionViewMenu.dequeueReusableCell(withReuseIdentifier: "lugar", for: indexPath) as! CollectionViewCell
            cell.delegate = self
            cell.place = true
            cell.category = false
            cell.city = false
            cell.cityLabel.text = valuePlace
            return cell
        }else{
            let cell = collectionViewMenu.dequeueReusableCell(withReuseIdentifier: "acomer", for: indexPath) as! EatCollectionViewCell
            if valueInfo != "" && valueCat != "" && valuePlace != ""{
                cell.municipio = valueInfo
                cell.categoria = valueCat
                cell.lugar = valuePlace
                cell.delegate = self
            }
            return cell
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            valueInfo = "Entidad Federativa"
//            valueCat = "Categoría"
//            valuePlace = "Lugar"
//            defaults.removeObject(forKey: "municipalty")
//            defaults.removeObject(forKey: "place")
//            defaults.removeObject(forKey: "category")
//            collectionView.reloadData()
//        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        /*if indexPath.item == 0 {
            return CGSize(width: 33, height: 27.5)
        }else */if indexPath.item == 1 {
            return CGSize(width: 1, height: 27.5)
        }else if indexPath.item == 3 {
            return CGSize(width: 1, height: 27.5)
        }/*else if indexPath.item == 5 {
            return CGSize(width: 1, height: 27.5)
        }*/else{
//            let width = (collectionView.frame.width - 36) / 4
            let width = (collectionView.frame.width - 4) / 4
            return CGSize(width: width, height: 27.5)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


