//
//  HurryTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 27/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol hurryActionsDelegate {
    func OnSelectInEstablishment(typeAction: String, isSelected: Bool)
}

class HurryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var viewContainerInEstablishment: UIView!
    @IBOutlet weak var viewContainerInBar: UIView!
    @IBOutlet weak var viewContainerInTable: UIView!
    @IBOutlet weak var viewContainerInHome: UIView!
    
    @IBOutlet weak var lblInEstablishment: UILabel!
    @IBOutlet weak var lblInBar: UILabel!
    @IBOutlet weak var lblInTable: UILabel!
    @IBOutlet weak var lblInHome: UILabel!
    
    @IBOutlet weak var imgInEstablishment: UIImageView!
    @IBOutlet weak var imgInBar: UIImageView!
    @IBOutlet weak var imgInTable: UIImageView!
    @IBOutlet weak var imgInHome: UIImageView!
    
    var isServiceInEst = Bool()
    var isServiceInBar = Bool()
    var isServiceInTable = Bool()
    var isServiceInHome = Bool()
    
    var delegate: hurryActionsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowOpacity = 0.24
        view.layer.shadowRadius = CGFloat(2)
        view.layer.cornerRadius = 5
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func OnGetInEstablishment(_ sender: Any) {
        if !isServiceInEst{
            isServiceInEst = true
            self.delegate.OnSelectInEstablishment(typeAction: "RS", isSelected: true)
            viewContainerInEstablishment.backgroundColor = UIColor.rgb(red: 37, green: 210, blue: 115)
            lblInEstablishment.textColor = .white
            setTintColor(image: "ic_Shop", contentImageView: imgInEstablishment, color: UIColor.rgb(red: 255, green: 255, blue: 255))
            disableServiceHome()
            disableServiceTable()
            disableServiceBar()
        }else{
            self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
            disableServiceEstablishment()
        }
    }
    
    @IBAction func OnGetInBar(_ sender: Any) {
        if !isServiceInBar{
            isServiceInBar = true
            self.delegate.OnSelectInEstablishment(typeAction: "RB", isSelected: true)
            viewContainerInBar.backgroundColor = UIColor.rgb(red: 37, green: 210, blue: 115)
            lblInBar.textColor = .white
            setTintColor(image: "ic_getInBar", contentImageView: imgInBar, color: UIColor.rgb(red: 255, green: 255, blue: 255))
            disableServiceHome()
            disableServiceTable()
            disableServiceEstablishment()
        }else{
            self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
            disableServiceBar()
        }
    }
    
    @IBAction func OnServiceTable(_ sender: Any) {
        if !isServiceInTable{
            isServiceInTable = true
            self.delegate.OnSelectInEstablishment(typeAction: "SM", isSelected: true)
            viewContainerInTable.backgroundColor = UIColor.rgb(red: 37, green: 210, blue: 115)
            lblInTable.textColor = .white
            setTintColor(image: "ic_serviceTable", contentImageView: imgInTable, color: UIColor.rgb(red: 255, green: 255, blue: 255))
            disableServiceHome()
            disableServiceBar()
            disableServiceEstablishment()
        }else{
            self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
            disableServiceTable()
        }
    }
    
    @IBAction func OnServiceHome(_ sender: Any) {
        if !isServiceInHome{
            isServiceInHome = true
            self.delegate.OnSelectInEstablishment(typeAction: "SD", isSelected: true)
            viewContainerInHome.backgroundColor = UIColor.rgb(red: 37, green: 210, blue: 115)
            lblInHome.textColor = .white
            setTintColor(image: "ic_serviceHome", contentImageView: imgInHome, color: UIColor.rgb(red: 255, green: 255, blue: 255))
            disableServiceTable()
            disableServiceBar()
            disableServiceEstablishment()
        }else{
            self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
            disableServiceHome()
        }
    }
    
    func disableServiceHome(){
        //self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
        viewContainerInHome.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255)
        lblInHome.textColor = UIColor.rgb(red: 90, green: 104, blue: 114)
        setTintColor(image: "ic_serviceHome", contentImageView: imgInHome, color: UIColor.rgb(red: 37, green: 210, blue: 115))
        isServiceInHome = false
    }
    
    func disableServiceTable(){
        //self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
        viewContainerInTable.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255)
        lblInTable.textColor = UIColor.rgb(red: 90, green: 104, blue: 114)
        setTintColor(image: "ic_serviceTable", contentImageView: imgInTable, color:  UIColor.rgb(red: 37, green: 210, blue: 115))
        isServiceInTable = false
    }
    
    func disableServiceBar(){
        //self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
        viewContainerInBar.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255)
        lblInBar.textColor = UIColor.rgb(red: 90, green: 104, blue: 114)
        setTintColor(image: "ic_getInBar", contentImageView: imgInBar, color: UIColor.rgb(red: 37, green: 210, blue: 115))
        isServiceInBar = false
    }
    
    func disableServiceEstablishment(){
        //self.delegate.OnSelectInEstablishment(typeAction: "0", isSelected: false)
        viewContainerInEstablishment.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255)
        lblInEstablishment.textColor = UIColor.rgb(red: 90, green: 104, blue: 114)
        setTintColor(image: "ic_Shop", contentImageView: imgInEstablishment, color: UIColor.rgb(red: 37, green: 210, blue: 115))
        isServiceInEst = false
    }
    
    func setTintColor(image: String, contentImageView: UIImageView, color: UIColor){
        if let myImage = UIImage(named: image) {
            let tintableImage = myImage.withRenderingMode(.alwaysTemplate)
            contentImageView.image = tintableImage
            contentImageView.tintColor = color
        }
    }
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}
