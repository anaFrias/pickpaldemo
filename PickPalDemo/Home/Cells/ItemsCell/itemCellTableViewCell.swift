//
//  itemCellTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit


class itemCellTableViewCell: UITableViewCell {
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var numberItem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 5
        itemImage.layer.cornerRadius = 5
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
