//
//  InstructionTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class InstructionTableViewCell: UITableViewCell {
    @IBOutlet weak var infoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
