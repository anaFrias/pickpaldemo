//
//  ExtraItemTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol extrasCellDelegate {
    func addItem(index: Int, name: String, price: Double, cant: Int, id: Int)
    func deleteItem(index: Int, name: String, price: Double, cant: Int, id: Int)
}
class ExtraItemTableViewCell: UITableViewCell {

    @IBOutlet weak var numberItems: UILabel!
    @IBOutlet weak var extraProductName: UILabel!
    @IBOutlet weak var extraItemPrice: UILabel!
    
    
    @IBOutlet weak var agotadoImg: UIImageView!
    @IBOutlet weak var menosBtn: UIButton!
    @IBOutlet weak var maasBtn: UIButton!
    
    var delegate: extrasCellDelegate!
    var index = Int()
    var name: String!
    var price: Double!
    var numberOFItems: Int!
    var idComplements = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func addItem(_ sender: Any) {
        delegate.addItem(index: self.index, name: name, price: price, cant: numberOFItems, id: idComplements)
    }
    @IBAction func removeItem(_ sender: Any) {
        delegate.deleteItem(index: self.index, name: name, price: price, cant: numberOFItems, id: idComplements)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
