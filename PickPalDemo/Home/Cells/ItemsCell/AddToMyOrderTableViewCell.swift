//
//  AddToMyOrderTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol addOrderDelegate {
    func addOrder()
}

class AddToMyOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var total: UILabel!
    var delegate: addOrderDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func addOrder(_ sender: Any) {
        self.delegate.addOrder()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
