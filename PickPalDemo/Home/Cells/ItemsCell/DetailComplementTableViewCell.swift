//
//  DetailComplementTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol complementosCellDelegate {
    func getModifiersCell (index: IndexPath, modifier: String, price: Double, indexButton: Int, id: Int, selection: Int, modifierIndex: Int)
}

class DetailComplementTableViewCell: UITableViewCell {
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var complementName: UILabel!
    @IBOutlet weak var complementPrice: UILabel!
    
    @IBOutlet weak var agotadoImage: UIImageView!
    var delegate: complementosCellDelegate!
    var index = IndexPath()
    var modifierCell = String()
    var priceCell = Double()
    var indexButton = Int()
    var id = Int()
    var selection = Int()
    var modifierIndex = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectedButton(_ sender: Any) {
        self.delegate.getModifiersCell(index: index, modifier: modifierCell, price: priceCell, indexButton: indexButton, id: id, selection: selection, modifierIndex: modifierIndex)
    }
    
}
