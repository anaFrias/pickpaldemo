//
//  AdditionalInfoTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class AdditionalInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var notes: UITextField!
    var defaults = UserDefaults.standard
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewContainer.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
