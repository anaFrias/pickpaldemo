//
//  ComplementsTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol modifiersDelegate {
    func getModifiers (index: IndexPath, modifier: String, price: Double, indexButton: Int, id: Int, selection: Int, modifierIndex: Int)
}
class ComplementsTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource, complementosCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    var modifiers = [modifier_prop]()
    var selectedFlags = [Bool]()
    var delegate: modifiersDelegate!
    var modifier_name = String()
    var indexPath = IndexPath()
    var numComplement = Int()
    var is_required = Bool()
    var maxToChoose = Int()
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modifiers.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sectionComplement", for: indexPath) as! SectionComplementTableViewCell
            cell.titleComplement.text = modifier_name
            if is_required {
                cell.requiredImage.alpha = 1
            }else{
                cell.requiredImage.alpha = 0
            }
            cell.maxChoose.text = "Máximo a elegir: \(maxToChoose)"
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailComplement", for: indexPath) as! DetailComplementTableViewCell
            cell.selectionStyle = .none
            cell.index = self.indexPath
            cell.modifierCell = modifiers[indexPath.row - 1].modifier_name
            cell.priceCell = modifiers[indexPath.row - 1].value
            cell.indexButton = indexPath.row - 1
            cell.id = modifiers[indexPath.row - 1].id
            cell.selection = numComplement
            cell.modifierIndex = self.indexPath.row
            cell.delegate = self
            if modifiers[indexPath.row - 1].is_active{
                cell.selectButton.isEnabled = true
                cell.complementName.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.complementPrice.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.agotadoImage.alpha = 0
            }else{
                cell.selectButton.isEnabled = false
                cell.complementName.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.complementPrice.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.agotadoImage.alpha = 1
            }
            //            MARK: Work on it
            cell.complementName.text = modifiers[indexPath.row - 1].modifier_name
            if modifiers[indexPath.row - 1].value == 0.00 {
                 cell.complementPrice.text = ""
            }else{
                 cell.complementPrice.text = "+ $"+modifiers[indexPath.row - 1].value.description + "0"
            }
            cell.selectButton.isSelected = selectedFlags[indexPath.row - 1]
            
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            //            MARK: Work on it
            self.delegate.getModifiers(index: self.indexPath, modifier: modifiers[indexPath.row - 1].modifier_name, price: modifiers[indexPath.row - 1].value, indexButton: indexPath.row - 1, id: modifiers[indexPath.row - 1].id, selection: numComplement, modifierIndex: self.indexPath.row)
        }
        
    }
    func getModifiersCell(index: IndexPath, modifier: String, price: Double, indexButton: Int, id: Int, selection: Int, modifierIndex: Int) {
        self.delegate.getModifiers(index: index, modifier: modifier, price: price, indexButton: indexButton, id: id, selection: selection, modifierIndex: modifierIndex)
    }
   
}
