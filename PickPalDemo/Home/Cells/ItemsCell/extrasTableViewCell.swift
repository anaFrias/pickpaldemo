//
//  extrasTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol extrasDelegate {
    func addItem(index: IndexPath, i: Int, name: String, price: Double, cant: Int, id: Int, selection: Int)
    func removeItem(index: IndexPath, i: Int, name: String, price: Double, cant: Int, id: Int, selection: Int)
}
class extrasTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource, extrasCellDelegate {


    @IBOutlet weak var tableView: UITableView!
    var complements = [complement_prop]()
    var totalItemsSelected = [Int]()
    var indexPath = IndexPath()
    var delegate: extrasDelegate!
    var selectedComplement = Int()
    var itemExtras = Int()
    var in_stock = [Bool]()
    var extrasTitle = String()
    override func awakeFromNib() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.layer.cornerRadius = 5
        super.awakeFromNib()
        // Initialization code
    }

    func addItem(index: Int, name: String, price: Double, cant: Int, id: Int) {
        self.delegate.addItem(index: indexPath, i: index, name: name, price: price, cant: cant, id: id, selection: selectedComplement)
    }
    func deleteItem(index: Int, name: String, price: Double, cant: Int, id: Int) {
        self.delegate.removeItem(index: indexPath, i: index, name: name, price: price, cant: cant, id: id, selection: selectedComplement)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complements.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sectionExtras", for: indexPath) as! SectionComplementTableViewCell
            cell.titleComplement.text = "\(extrasTitle)"
            cell.maxChoose.text = "Máximo a elegir \(itemExtras)"
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "extraItem", for: indexPath) as! ExtraItemTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            //            MARK: WORK ON ITT!!!!
            cell.extraProductName.text = complements[indexPath.row - 1].complement_name
            if complements[indexPath.row - 1].value == "0.00" {
                cell.extraItemPrice.text = ""
            }else{
                cell.extraItemPrice.text = "$"+complements[indexPath.row - 1].value
            }
            
            cell.name = complements[indexPath.row - 1].complement_name
            cell.price = Double(complements[indexPath.row - 1].value)
            cell.numberItems.text = "\(totalItemsSelected[indexPath.row - 1])"
            cell.numberOFItems = totalItemsSelected[indexPath.row - 1]
            cell.idComplements = complements[indexPath.row - 1].id
            cell.index = indexPath.row - 1
            if complements[indexPath.row - 1].in_stock {
                cell.menosBtn.isUserInteractionEnabled = true
                cell.maasBtn.isUserInteractionEnabled = true
                cell.menosBtn.setImage(UIImage(named: "minus"), for: .normal)
                cell.maasBtn.setImage(UIImage(named: "plus"), for: .normal)
                cell.extraProductName.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.extraItemPrice.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.numberItems.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.agotadoImg.alpha = 0
            }else{
                cell.menosBtn.isUserInteractionEnabled = false
                cell.maasBtn.isUserInteractionEnabled = false
                cell.menosBtn.setImage(UIImage(named: "minusGray"), for: .normal)
                cell.maasBtn.setImage(UIImage(named: "plusGray"), for: .normal)
                cell.extraProductName.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.extraItemPrice.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.numberItems.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.agotadoImg.alpha = 1
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }

}
