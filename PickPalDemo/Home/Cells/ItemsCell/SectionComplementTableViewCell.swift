//
//  SectionComplementTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SectionComplementTableViewCell: UITableViewCell {

    @IBOutlet weak var requiredImage: UIImageView!
    @IBOutlet weak var titleComplement: UILabel!
    @IBOutlet weak var maxChoose: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
