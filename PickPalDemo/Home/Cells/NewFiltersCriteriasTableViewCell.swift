//
//  NewFiltersCriteriasTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 6/16/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

@objc protocol NewFiltersCriteriasDelegate: class {
    @objc optional func ShowStateCriterias()
    
    @objc optional func ShowBussinesLineCriterias()
    
    @objc optional func ShowLocationCriterias()
    
    @objc optional func ShowTypeCriterias()
    
    @objc optional func SearchByNewCriterias()
}


class NewFiltersCriteriasTableViewCell: TableViewCell {
    
    @IBOutlet weak var newContainerLeft: UIView!
    @IBOutlet weak var newContainerRight: UIView!
    @IBOutlet weak var stackContainerFilters: UIStackView!
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblBussinessLine: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    var delegate: NewFiltersCriteriasDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        newContainerLeft.clipsToBounds = true
        newContainerLeft.layer.cornerRadius = 18
        newContainerLeft.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        newContainerRight.clipsToBounds = true
        newContainerRight.layer.cornerRadius = 18
        newContainerRight.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        stackContainerFilters.layer.masksToBounds = false
        stackContainerFilters.layer.shadowColor = UIColor.black.cgColor
        stackContainerFilters.layer.shadowOffset = CGSize(width: 0, height: 2)
        stackContainerFilters.layer.shadowOpacity = 0.24
        stackContainerFilters.layer.shadowRadius = CGFloat(2)
        stackContainerFilters.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func onSelectEstate(_ sender: Any) {
        self.delegate.ShowStateCriterias?()
    }
    
    @IBAction func OnSelectBussinesLine(_ sender: Any) {
        self.delegate.ShowBussinesLineCriterias?()
    }
    
    @IBAction func OnSelectLocation(_ sender: Any) {
        self.delegate.ShowLocationCriterias?()
        
    }
    
    @IBAction func OnSelectType(_ sender: Any) {
        self.delegate.ShowTypeCriterias?()
    }
    
    
    @IBAction func OnSearchByNewCriterias(_ sender: Any) {
        self.delegate.SearchByNewCriterias?()
    }
    
}
