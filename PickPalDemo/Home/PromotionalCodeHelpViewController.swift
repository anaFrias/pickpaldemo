//
//  PromotionalCodeHelpViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class PromotionalCodeHelpViewController: UIViewController, openImagePickerDelegate, inputPromoCodeDelegate, saveInfoDelegate, userDelegate, closeModalDelegate {

    @IBOutlet weak var tableView: UITableView!
    var orderId = Int()
    var savedPromotionalCode = String()
    var completeText = String()
    var isPromotionalCode = Bool()
    var userws = UserWS()
    var promoCodeText = String()
    var image = UIImage()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func imagePicker(isPromotional: Bool) {
//        isPromotionalCode = isPromotional
        showActionSheet()
    }
    func inputPromoCode(text: String) {
        promoCodeText = text
    }
    func saveInfo(text: String) {
        if promoCodeText != "" && savedPromotionalCode != "" {
            let clientId = UserDefaults.standard.object(forKey: "client_id") as! Int
            completeText = promoCodeText + " Comentario: " + text
            LoadingOverlay.shared.showOverlay(view: self.view)
            userws.registerHelp(client_id: clientId, descriptionText: completeText, category: 3, order: orderId, file: savedPromotionalCode)
        }else{
            let alert = UIAlertController(title: "Campo faltante", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func didSuccessRegisterHelp() {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = SuccedMessageHelpViewController(nibName: "SuccedMessageHelpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true) {
            
        }
    }
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    func didFailRegisterHelp(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let error = ErrorViewController()
        error.modalTransitionStyle = .crossDissolve
        error.modalPresentationStyle = .overCurrentContext
        error.errorMessageString = title
        error.subTitleMessageString = subtitle
        self.present(error, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
extension PromotionalCodeHelpViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoHeaderCell", for: indexPath)
            return cell
            
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "promotionalCodeCell", for: indexPath) as! PromotionalCodeInputTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imagesAttachedCell", for: indexPath) as! PromotionalCodeImageTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            if image != nil {
                cell.imagePicker.alpha = 1
                cell.imagePicker.image = image
            }
//            cell.isPromotion = true
//            cell.descriptionLabel.text = "Adjunta la imágen de donde obtuviste el código promocional"
            return cell
//        }else if indexPath.row == 3 {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "imagesAttachedCell", for: indexPath) as! PromotionalCodeImageTableViewCell
//            cell.selectionStyle = .none
//            cell.isPromotion = false
//            cell.delegate = self
//            cell.descriptionLabel.text = "Adjunta una captura del recibo de tu pedido"
//            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "sendInfoCell", for: indexPath) as! PromotionalCodeSendInfoTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 305
        }else if indexPath.row == 1 {
            return 87
        }else if indexPath.row == 2 {
            return 94
//        }else if indexPath.row == 3 {
//            return 94
        }else{
            return 205
        }
    }
}
//MARK: Get image from camera roll & Photo library
extension PromotionalCodeHelpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cámara", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Carrete", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openGallery()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let picker = UIImagePickerController()
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.showsCameraControls = true
            self.present(picker, animated: true, completion: nil)
        }else {
            self.openGallery()
        }
    }
    
    func openGallery() {
        var pickerGallery = UIImagePickerController()
        pickerGallery.delegate = self
        pickerGallery.sourceType = .photoLibrary
        self.present(pickerGallery, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        LoadingOverlay.shared.showOverlayWithMessage(view: self.view, message: "Subiendo..")
//        if image {
            image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            let uuid = UUID().uuidString
            let myImageName = "imagen" + uuid + ".png"
            let imagePath = fileInDocumentsDirectory(myImageName)
            _ = saveImage((info[UIImagePickerControllerOriginalImage] as? UIImage)!, path: imagePath)
            //        print("\(path)/" + myImageName)
            //        if isPromotionalCode {
            savedPromotionalCode = "\(path)/" + myImageName
            //        }else{
            //            savedCheck = "\(path)/" + myImageName
            //        }
            print(imagePath)
            self.dismiss(animated: true, completion: nil)
//        }

    }

    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
        
    }
    
}
