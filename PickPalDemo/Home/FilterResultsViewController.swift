//
//  FilterResultsViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 08/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import Firebase
import CoreLocation
import OneSignal

class FilterResultsViewController: UIViewController, openModalDelegate, openSingleDelegate,
setValuesDelegate, establishmentDelegate, PromosDelegate, businessSingleDelegate, GuideDelegate, EstablishmentGuideDelegate {
    
    func goToSingle(position: Int) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        promws.viewSingleAdvsEstablishment(establishmentID: promoValues[position].id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
    }
    
    func suscribeNotifications(position: Int) {
        print("notificationes", promoValues[position].activeNotifications)
        promws.businessSuscription(establishmentID: promoValues[position].id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int, suscribe: !promoValues[position].activeNotifications, position: position)
    }
    
    func didSuccessSuscribre(position: Int, isSuscribe: Bool) {
        print(promoValues[position].activeNotifications.description)
        promoValues[position].activeNotifications = !promoValues[position].activeNotifications
        print(promoValues[position].activeNotifications.description)
        tableView.reloadRows(at: [IndexPath(row: position, section: 0)], with: .automatic)
        if isSuscribe{
            showToast(message: "Alertas activadas para " + promoValues[position].name)
        }else{
            showToast(message: "Alertas desactivadas para " + promoValues[position].name)
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    //@IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categoryNameView: UIView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var viewContentButtonsGuide: UIView!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    //    Numero de pedidos
    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var imgPickPalToolbarLogo: UIImageView!
    
    @IBOutlet weak var newContainerLeft: UIView!
    @IBOutlet weak var newContainerRight: UIView!
    @IBOutlet weak var stackContainerFilters: UIStackView!
    @IBOutlet weak var lblTextGuia: UILabel!
    
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblBussinessLine: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var containerNewFilters: UIView!
    
    @IBOutlet weak var stackGuide: UIStackView!
    
    @IBOutlet weak var containerTypeOrder: UIView!
    var ids = [Int]()
    var mListTypeOrders = [String]()
    var valueState = String()
    var valueCity = String()
    var valueCategory = String()
    var valuePlace = String()
    var ws = EstablishmentWS()
    var guideWS = GuidePickPalWS()
    var values = [Establishment]()
    var valuesaux = [EstablishmentsGuide]()
    var promoValues = [PromoEstablishment]()
    var valuesItems = [KitchensComplete]()
    var defaults = UserDefaults.standard
    var ref: DatabaseReference!
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    var typeKitchen = Bool()
    var nameTypeKitchen = String()
    var totalEstablishments = 1
    var kitchenIds = [[Int]]()
    var errorFlag = Bool()
    var from = String()
    var promws = PromosWS()
    var isFromPromos = false
    var isFromGuide = false
    var mIdList = [IdsKm]()
    
    var midBussinessLine = String()
    var midInside = String()
    var mTypeOrderSelect = "Tipo de pedido"
    var mTypeOrderCode = String()
    
    var valueCityAux = ""
    var valueCategoryAux = ""
    var valuePlaceAux = ""
    var valueTypeAux = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        promws.delegate = self
        guideWS.delegate = self
        
        newContainerLeft.clipsToBounds = true
        newContainerLeft.layer.cornerRadius = 18
        newContainerLeft.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        newContainerRight.clipsToBounds = true
        newContainerRight.layer.cornerRadius = 18
        newContainerRight.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        stackContainerFilters.layer.masksToBounds = false
        stackContainerFilters.layer.shadowColor = UIColor.black.cgColor
        stackContainerFilters.layer.shadowOffset = CGSize(width: 0, height: 2)
        stackContainerFilters.layer.shadowOpacity = 0.24
        stackContainerFilters.layer.shadowRadius = CGFloat(2)
        stackContainerFilters.layer.cornerRadius = 5
        
        viewContentButtonsGuide.clipsToBounds = true
        viewContentButtonsGuide.layer.cornerRadius = 18
        viewContentButtonsGuide.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        

        print(ids)
        
        lblCity.text = valueCity
        lblBussinessLine.text = valueCategory
        lblLocation.text = valuePlace
        lblType.text = mTypeOrderSelect
        lblState.text = valueCityAux
        lblCategory.text = valueCategoryAux
        
        self.navigationController?.isNavigationBarHidden = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        
        
        valueCityAux = valueCity
        valueCategoryAux = valueCategory
        valuePlaceAux = valuePlace
        valueTypeAux = mTypeOrderSelect
          
       
        
        if isFromPromos{
            imgPickPalToolbarLogo.image = UIImage(named: "pickpalPromo")
            containerTypeOrder.visibility = .invisible
            
            
        }else{
            imgPickPalToolbarLogo.image = UIImage(named: "toolBarLogo")
        }
        
        if valueCity == "" && valueCategory == "" && valuePlace == ""  && valueState == ""{
            if let valueM = UserDefaults.standard.object(forKey: "municipalty") as? String {
                valueCity = valueM
            }
            if let valueC = UserDefaults.standard.object(forKey: "category") as? String {
                valueCategory = valueC
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "place") as? String {
                valuePlace = valueP
            }
            if let valueS = UserDefaults.standard.object(forKey: "state") as? String {
                valueState = valueS
            }
        }
        if typeKitchen {
            categoryNameView.alpha = 1
            categoryNameLabel.text = nameTypeKitchen
            containerNewFilters.alpha = 0
        }else{
            categoryNameView.alpha = 0
            containerNewFilters.alpha = 1
        }
    
        
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
        lblTextGuia.visibility = .gone
        if (from.isEmpty) {
            ref = Database.database().reference().child("establishments")
            ws.getEstablishmentsById(ids: ids, order_id: 0)
            viewContentButtonsGuide.alpha = 0
        } else if from == "guide" {
            //collectionView.alpha = 0
            lblTextGuia.visibility = .visible
            containerNewFilters.isHidden = true
            viewContentButtonsGuide.alpha = 1
            ref = Database.database().reference().child("establishmentsguide")
            self.guideWS.viewEstablishmentAroundGuide(latitude: self.userLat, longitude: self.userLng, codeState: valueState.stripingDiacritics)
           
//            guideWS.getGuideEstablishmentsById(ids: ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }else{
            viewContentButtonsGuide.alpha = 0
            ref = Database.database().reference().child("establishmentsadvs")
            promws.getEstablishmentsById(ids: ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
        //        MARK: GET TIME KITCHEN
        ws.getAllTypeKitchen(codeState: valueState.stripingDiacritics)
        // Do any additional setup after loading the view.
        lblCity.text = valueCity
        lblBussinessLine.text = valueCategory
        lblLocation.text = valuePlace
        lblType.text = mTypeOrderSelect
        lblState.text = valueCityAux
        lblCategory.text = valueCategoryAux
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        ref.removeAllObservers()
    }
    override func viewWillAppear(_ animated: Bool) {
        defaults.removeObject(forKey: "screen")
        defaults.removeObject(forKey: "storyboard")
    }
    
    //Inicia Busqueda por criterios
    @IBAction func OnOpenEstateFilter(_ sender: Any) {
        self.openModal(place: false, category: false, city: true, bussinesLine: false, type: false)
    }
    
    @IBAction func OnOpenBussinesLineFilter(_ sender: Any) {
        self.openModal(place: false, category: false, city: false, bussinesLine: true, type: false)
    }
    
    @IBAction func OnOpenInsideFilter(_ sender: Any) {
        self.openModal(place: true, category: false, city: false, bussinesLine: false, type: false)
    }
    
    @IBAction func OnOpenOrderTypeFilter(_ sender: Any) {
        self.ws.viewEstablishmentOrdertypes(state_code: valueState, district: valueCity, business_line_id: Int(midBussinessLine) ?? 0, inside_id: Int(midInside) ?? 0)
    }
    
    @IBAction func OnSearchFilter(_ sender: Any) {
        
        if valueCityAux != valueCity || valueCategoryAux != valueCategory || valuePlaceAux != valuePlace ||  valueTypeAux != mTypeOrderSelect{
        self.openSingle(municipio: valueCity, categoria: valueCategory, lugar: valuePlace)
            valueCityAux = valueCity
           valueCategoryAux = valueCategory
           valuePlaceAux = valuePlace
           valueTypeAux = mTypeOrderSelect
        }else{
            print("Nel mijo")
        }
        //self.openModalAndSingle(modal: false, place: false, category: false, city: false, bussinesLine: false, type: false)
    }
    
    func didSuccessTypeOrders(reward: [String]) {
        self.mListTypeOrders = reward
        self.openModal(place: false, category: false, city: false, bussinesLine: false, type: true)
    }
    
    func didFailTypeOrders(error: String, subtitle: String) {
        print(error)
    }
    
    //Termina Busqueda por criterios
    
    func getIconsServicePickPal (data: [String]) -> [String]{
        var icons = [String]()
        for service1 in data{
            if service1 == "POD" {//Pickpal Order guide_icon_food_and_drinks
                icons.append("guide_icon_food_and_drinks")
            }
        }
        
        for service2 in data{
            if service2 == "PPR" {//Pickpal Promos guide_icon
                icons.append("guide_icon_promos")
            }
        }
        
        for service3 in data{
            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
                icons.append("guide_icon")
            }
        }
        
        for service4 in data{
            if service4 == "PPM" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("guide_icon_points")
            }
        }
        
        return icons
    }
    
    func didSuccessGetGuideEstablshmentsById(info: [EstablishmentsGuide]) {
        LoadingOverlay.shared.hideOverlayView()
        self.valuesaux = info
        totalEstablishments = info.count
        for id in mIdList{
            for estab in valuesaux{
                if id.id == estab.id {
                    estab.kms = id.Km
                }
            }
        }
        errorFlag = false
        self.tableView.reloadData()
    }
    
    @IBAction func showMunicipality(_ sender: Any) {
        self.openModal(place: false, category: false, city: true, bussinesLine: false, type: false)
    }
    
    @IBAction func showCategory(_ sender: Any) {
        self.openModal(place: false, category: true, city: false, bussinesLine: false, type: false)
    }
    
    @IBAction func BuscarFilter(_ sender: Any) {
        if valueCity != "" && valueCategory != "" && valuePlace != ""{
            self.openSingle(municipio: valueCity, categoria: valueCategory, lugar: valuePlace)
        }
    }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-100, width: 300, height: 45))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func didSuccessGetAllTypeKitchen(info: [Kitchens]) {
        ref.observe( .value, with: { (snapshot) -> Void in
            var temp = [KitchensComplete]()
            var tempIdsArray = [[Int]]()
            for v in info{
                var cont = 0
                var tempIds = [Int]()
                if !(snapshot.value is NSNull) {
                    if let state = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                        if let all_establishments = (state.value(forKey: self.valueCity) as? NSDictionary)  {
                            for establishments in all_establishments.allValues as NSArray {
                                if let type_kitchen = (establishments as! NSDictionary).value(forKey: "kitchen_types") as? [String] {
                                    let longitud = ((establishments as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "longitude") as!CLLocationDegrees
                                    let latitud = ((establishments as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "latitude") as! CLLocationDegrees
                                    if type_kitchen.contains(v.name) {
                                        if self.getRatio(latitud: self.userLat, Longitud: self.userLng, Latitud2: latitud, Longitud2: longitud) {
                                            cont += 1
                                            tempIds.append(((establishments as! NSDictionary).value(forKey: "id") as! Int))
                                        }else{
                                            tempIds.append(((establishments as! NSDictionary).value(forKey: "id") as! Int))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                let tempItem = KitchensComplete()
                tempItem.image = v.image
                tempItem.name = v.name
                tempItem.nearby = cont
                tempItem.ids = tempIds
                temp.append(tempItem)
                tempIdsArray.append(tempIds)
            }
            self.kitchenIds = tempIdsArray
            self.valuesItems = temp
            self.tableView.reloadData()
        })
    }
    func getRatio(latitud: CLLocationDegrees, Longitud: CLLocationDegrees, Latitud2: CLLocationDegrees, Longitud2: CLLocationDegrees) -> Bool{
        let coordinate🕕 = CLLocation(latitude: latitud, longitude: Longitud)
        let coordinate🕛 = CLLocation(latitude: Latitud2, longitude: Longitud2)
        
        let distance = coordinate🕕.distance(from: coordinate🕛)
        if distance <= 2000 {
            return true
        }else{
            return false
        }
        
    }
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = isFromPromos
                newVC.isFromGuide = self.isFromGuide
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = isFromPromos
            newVC.isFromGuide = self.isFromGuide
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        newVC.isFromGuide = isFromGuide
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            if isFromGuide{
                let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
                let navController = UINavigationController(rootViewController: newVC)
                navController.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }
            
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func didFailGetAllTypeKitchen(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        //        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        //        newXIB.modalTransitionStyle = .crossDissolve
        //        newXIB.modalPresentationStyle = .overCurrentContext
        //        newXIB.errorMessageString = error
        //        newXIB.subTitleMessageString = subtitle
        //        present(newXIB, animated: true, completion: nil)
    }
    
    //didSuccessSingleAdvs
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs) {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        newVC.singleInfo = info
        newVC.id = info.id
        newVC.municipioStr = info.address.city
        newVC.categoriaStr = info.address.colony
        newVC.lugarString = info.advsCategorry
        newVC.establString = info.name
        /*if let logoimg = establishment.logo{
         print(logoimg)
         newVC.logoString = logoimg
         }*/
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didSuccessSingleEstablishment(establishment: PromoSingleEstablishment) {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        //newVC.singleInfo = establishment
        newVC.id = establishment.id
        newVC.municipioStr = establishment.address.city
        newVC.categoriaStr = establishment.address.colony
        newVC.lugarString = establishment.advsCategorry
        newVC.establString = establishment.name
        /*if let logoimg = establishment.logo{
         print(logoimg)
         newVC.logoString = logoimg
         }*/
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func didSuccessGetEstablshmentsById(info: [Establishment]) {
        LoadingOverlay.shared.hideOverlayView()
        self.values = info
        totalEstablishments = info.count
        errorFlag = false
        self.tableView.reloadData()
    }
    
    func didSuccessGetPromoEstablshmentsById(info: [PromoEstablishment]) {
        LoadingOverlay.shared.hideOverlayView()
        self.promoValues = info
        totalEstablishments = info.count
        errorFlag = false
        self.tableView.reloadData()
    }
    
    func didFailGetEstablshmentsById(error: String, subtitle: String) {
        
        LoadingOverlay.shared.hideOverlayView()
        totalEstablishments = 1
        errorFlag = true
        self.values.removeAll()
        self.promoValues.removeAll()
        tableView.reloadData()
        //        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        //        newXIB.modalTransitionStyle = .crossDissolve
        //        newXIB.modalPresentationStyle = .overCurrentContext
        //        newXIB.errorMessageString = error
        //        present(newXIB, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openSingle(municipio: String, categoria: String, lugar: String) {
        let node = from == "guide" ? valueCity : valuePlace
        if valueCategory != "Negocio" && node != "Ubicación" {
            print(ref)
            ref.observe(.value, with: { (snapshot) -> Void in
                LoadingOverlay.shared.showOverlay(view: self.view)
                self.ids.removeAll()
                if let state = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary {
                    if let singleState = state.value(forKey: self.valueCity) as? NSDictionary {
                        print(singleState)
                        for item in singleState.allValues as NSArray {
                            if self.mTypeOrderCode != ""{
                                for orderTypes in (item as! NSDictionary).value(forKey: "order_types") as? NSArray ?? NSArray(){
                                    if orderTypes as! String == self.mTypeOrderCode{ //Buscamos por tipo de orden
                                        self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                    }
                                }
                            }else if self.midInside != "" || self.midInside != "0"{
                                if (item as! NSDictionary).value(forKey: "inside_id") as? Int == Int(self.midInside){
                                    self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                }
                                
                            }else if self.midBussinessLine != "" || self.midBussinessLine != "0" {
                                if (item as! NSDictionary).value(forKey: "business_line_id") as? Int == Int(self.midBussinessLine){
                                    self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                }
                            }
                            
                            /*let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                            var categories = ""
                            if self.from == "guide"{
                                categories = (item as! NSDictionary).value(forKey: "business") as! String
                            }else{
                                categories = (item as! NSDictionary).value(forKey: "category") as! String
                            }
                            
                            var newNode = ""
                            switch self.from {
                            case "guide":
                                newNode = "business"
                                break
                            case "adv":
                                newNode = "subcategory"
                                break
                            default:
                                newNode = "inside"
                                break
                            }
                            
                            let inside = (item as! NSDictionary).value(forKey: newNode) as! String
                            if categories == self.valueCategory {
                                if inside == self.valuePlace {
                                    self.ids.append(ids)
                                }else{
                                    if self.from == "guide"{
                                        self.ids.append(ids)
                                    }
                                }
                            }*/
                            
                            print(self.ids)
                        }
                        
                    }else{
                        self.ids = []
                    }
                    
                }
                LoadingOverlay.shared.hideOverlayView()
                self.tableView.contentOffset.y = 0
                
                switch self.from {
                case "guide":
                    self.guideWS.getGuideEstablishmentsById(ids: self.ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
                    break
                case "adv":
                    self.promws.getEstablishmentsById(ids: self.ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
                    break
                default:
                    self.ws.getEstablishmentsById(ids: self.ids, order_id: 0)
                    break
                }
                //self.ws.getEstablishmentsById(ids: self.ids)
                self.tableView.reloadData()
            })
        }else{
            ref.observe( .value, with: { (snapshot) -> Void in
                LoadingOverlay.shared.showOverlay(view: self.view)
                self.ids.removeAll()
                if let stateQuery = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                    let state = self.valueCity.elementsEqual("Santiago de Querétaro") ? "Querétaro" : self.valueCity
                    if let singleState = (stateQuery.value(forKey: state) as? NSDictionary)  {
                        print(singleState)
                        
                        for item in singleState.allValues as NSArray {
                            let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                            self.ids.append(ids)
                            print(self.ids)
                        }
                        
                    }
                }else{
                    self.ids = []
                }
                LoadingOverlay.shared.hideOverlayView()
                self.tableView.contentOffset.y = 0
                if (self.from.isEmpty) {
                    self.ws.getEstablishmentsById(ids: self.ids, order_id: 0)
                } else {
                    self.promws.getEstablishmentsById(ids: self.ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
                }
                self.tableView.reloadData()
            })
        }
    }
    
    func setValue(value: String, place: Bool, category: Bool, state: String, isType: Bool, idBussinessLine: String) {
        backButton.alpha = 1
        if category{
            midBussinessLine = idBussinessLine
        }else{
            midInside = idBussinessLine
        }
        
        if isType{
            mTypeOrderSelect = value
            lblType.text = mTypeOrderSelect
            switch value {
            case "Servicio a Mesa":
                mTypeOrderCode = "SM"
                break
                
            case "Recoger en Barra":
                mTypeOrderCode = "RB"
                break
                
            case "Recoger en Sucursal":
                mTypeOrderCode = "RS"
                break
            default:
                mTypeOrderCode = "SD"
            }
        }else if place {
            valueCity = value
            valueState = state
            lblState.text = valueCity
            lblCity.text = valueCity
        }else if category {
            valueCategory = value
            lblCategory.text = valueCategory
            lblBussinessLine.text = valueCategory
        }else{
            valuePlace = value
            lblLocation.text = valuePlace
        }
        tableView.reloadData()
    }
    func closeModal() {
        backButton.alpha = 1
    }
    
    func openModal(place: Bool, category: Bool, city: Bool, bussinesLine: Bool, type: Bool) {
        backButton.alpha = 0
        if type {//Estado / Municipio
            let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.titleString = "Tipo de pedido"
            newXIB.municipio =  valueCity
            newXIB.categoria = valueCategory
            newXIB.isTypeOrders = type
            newXIB.places = false
            newXIB.delegate = self
            newXIB.typeOrdersList = self.mListTypeOrders
            newXIB.state = valueState
            newXIB.idBussinessLine = midBussinessLine
            present(newXIB, animated: true, completion: nil)
        }else if place {//Estado / Municipio
            let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.titleString = (from.isEmpty ? "Ubicación" : "Subcategoría")
            newXIB.municipio =  valueCity
            newXIB.state = valueState
            newXIB.categoria = valueCategory
            newXIB.places = false
            newXIB.delegate = self
            newXIB.from = self.from
            newXIB.idBussinessLine = midBussinessLine
            present(newXIB, animated: true, completion: nil)
        }else if category {
            let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.titleString = "Giro de Negocio"
            newXIB.municipio = valueCity
            newXIB.categoria = "none"
            newXIB.places = false
            newXIB.categories = true
            newXIB.delegate = self
            if from == "guide"{
                newXIB.from = "guide"
            }
  
            newXIB.state = valueState
            present(newXIB, animated: true, completion: nil)
        }else if bussinesLine {//Giro de negocio
            let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.titleString = "Giro de Negocio"
            newXIB.municipio = valueCity
            newXIB.categoria = "none"
            newXIB.places = false
            newXIB.categories = true
            newXIB.delegate = self
            newXIB.state = valueState
            if from == "guide"{
                newXIB.from = "guide"
            }
  
            present(newXIB, animated: true, completion: nil)
        }else{
            let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.titleString = "Estado"
            newXIB.municipio = "none"
            newXIB.categoria = "none"
            newXIB.places = true
            newXIB.delegate = self
            newXIB.state = valueState
            present(newXIB, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //didFailGetGuideEstablshmentsById
    func didFailGetGuideEstablshmentsById(error: String, subtitle: String) {
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension FilterResultsViewController: categoriesSelection {
    func selectedCategory(index: IndexPath) {
        if valuesItems[index.item].nearby > 0 {
            LoadingOverlay.shared.showOverlay(view: self.view)
            ids = valuesItems[index.item].ids
            categoryNameLabel.text = valuesItems[index.item].name
            categoryNameView.alpha = 1
            //collectionView.alpha = 0
            print(valuesItems[index.item].ids)
            tableView.contentOffset.y = 0
            if (self.from.isEmpty) {
                self.ws.getEstablishmentsById(ids: self.ids, order_id: 0)
            } else {
                self.promws.getEstablishmentsById(ids: self.ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            }
        }else{
            LoadingOverlay.shared.showOverlay(view: self.view)
            ids = kitchenIds[index.item]
            categoryNameLabel.text = valuesItems[index.item].name
            categoryNameView.alpha = 1
            //collectionView.alpha = 0
            print(valuesItems[index.item].ids)
            tableView.contentOffset.y = 0
            if (self.from.isEmpty) {
                self.ws.getEstablishmentsById(ids: self.ids, order_id: 0)
            } else {
                self.promws.getEstablishmentsById(ids: self.ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            }
        }
    }
}
extension FilterResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("values" + values.count.description)
        print("promos" + promoValues.count.description)
        if from == "guide" {
            return valuesaux.count
        }else{
            if values.count > 0 {
                return values.count //+ 2
            } else if promoValues.count > 0 {
                return promoValues.count
            }else{
                return 0 //+ 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == totalEstablishments {
            if from == "guide" {
                return UITableViewCell()
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "categoriesTitle")!
                cell.selectionStyle = .none
                return cell
            }
        }else if indexPath.row == (totalEstablishments + 1) {
            if from == "guide" {
                return UITableViewCell()
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "collection") as! CollectionEstablishmentTableViewCell
                cell.values = valuesItems
                cell.selectionStyle = .none
                cell.delegate = self
                return cell
            }
        }else{
            if self.from == "guide" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentsGuide") as! EstablishmentGuideTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                cell.mId = valuesaux[indexPath.row].id
                cell.mBussinesArea = valuesaux[indexPath.row].business_area
                cell.mPlace = valuesaux[indexPath.row].place
                cell.lblNameEstab.text = valuesaux[indexPath.row].name
                cell.lblGuidePlus.text = valuesaux[indexPath.row].service_plus ? "Guía Plus |" + valuesaux[indexPath.row].kms : "Guía Básica |" +  valuesaux[indexPath.row].kms
                cell.lblDescription.text = valuesaux[indexPath.row].place + " - " + valuesaux[indexPath.row].business_area
                if let image = valuesaux[indexPath.row].image, image != "" {
                    Nuke.loadImage(with: URL(string: image)!, into: cell.imgEstablishment)
                }
                
                if valuesaux[indexPath.row].pickpal_services.count > 0  {
                    let icons = getIconsServicePickPal(data: valuesaux[indexPath.row].pickpal_services)
                    switch icons.count {
                    case 4:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = false
                        cell.btnPoints.isHidden = false
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
                        cell.btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                        break
                    case 3:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = false
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
                        break
                        
                    case 2:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        break
                        
                    case 1:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = true
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        break
                        
                    default:
                        cell.btnFoodAndDrinks.isHidden = true
                        cell.btnPromos.isHidden = true
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        break
                    }
                }else{
                    cell.viewContentButtons.isHidden = true
                }
                cell.btnFoodAndDrinks.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                cell.btnPromos.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                cell.btnGuidePickPal.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                cell.btnPoints.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                return cell
            }else{
                if (self.from != "adv"){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "establishments", for: indexPath) as! EstablishmentsTableViewCell
                    cell.selectionStyle = .none
                    
                    if values.count > 0 {
                        let icons = CustomsFuncs.getIconsServicePickPal(data: values[indexPath.row].pickpal_services)
                        switch values[indexPath.row].pickpal_services.count - 1 {
                        case 4:
                            cell.btnFoodAndDrinks.isHidden = false
                            cell.btnPromos.isHidden = false
                            cell.btnGuia.isHidden = false
                            cell.btnPoints.isHidden = false
                            
                            cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                            cell.btnPromos.image = UIImage(named: icons[1])
                            cell.btnGuia.image = UIImage(named: icons[2])
                            cell.btnPoints.image = UIImage(named: icons[3])
                            break
                        case 3:
                            cell.btnFoodAndDrinks.isHidden = false
                            cell.btnPromos.isHidden = false
                            cell.btnGuia.isHidden = false
                            cell.btnPoints.isHidden = true
                            cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                            cell.btnPromos.image = UIImage(named: icons[1])
                            cell.btnGuia.image = UIImage(named: icons[2])
                            break
                            
                        case 2:
                            cell.btnFoodAndDrinks.isHidden = false
                            cell.btnPromos.isHidden = false
                            cell.btnGuia.isHidden = true
                            cell.btnPoints.isHidden = true
                            cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                            cell.btnPromos.image = UIImage(named: icons[1])
                            break
                            
                        case 1:
                            cell.btnFoodAndDrinks.isHidden = false
                            cell.btnPromos.isHidden = true
                            cell.btnGuia.isHidden = true
                            cell.btnPoints.isHidden = true
                            cell.btnFoodAndDrinks.image = UIImage(named: icons[0])
                            break
                            
                        default:
                            cell.btnFoodAndDrinks.isHidden = true
                            cell.btnPromos.isHidden = true
                            cell.btnGuia.isHidden = true
                            cell.btnPoints.isHidden = true
                            break
                        }
                        
                        let iconsTyOrder = CustomsFuncs.getIconsOrderTypesPickPal(data: values[indexPath.row].order_types)
                        switch iconsTyOrder.count {
                        case 4:
                            cell.imgServiceEst.isHidden = false
                            cell.imgServiceBar.isHidden = false
                            cell.imgServiceTable.isHidden = false
                            cell.imgServiceHome.isHidden = false
                            
                            cell.imgServiceEst.image = UIImage(named: iconsTyOrder[0])
                            cell.imgServiceBar.image = UIImage(named: iconsTyOrder[1])
                            cell.imgServiceTable.image = UIImage(named: iconsTyOrder[2])
                            cell.imgServiceHome.image = UIImage(named: iconsTyOrder[3])
                            break
                        case 3:
                            cell.imgServiceEst.isHidden = true
                            cell.imgServiceBar.isHidden = false
                            cell.imgServiceTable.isHidden = false
                            cell.imgServiceHome.isHidden = false
                            
                            cell.imgServiceBar.image = UIImage(named: iconsTyOrder[0])
                            cell.imgServiceTable.image = UIImage(named: iconsTyOrder[1])
                            cell.imgServiceHome.image = UIImage(named: iconsTyOrder[2])
                            break
                            
                        case 2:
                            cell.imgServiceEst.isHidden = true
                            cell.imgServiceBar.isHidden = true
                            cell.imgServiceTable.isHidden = false
                            cell.imgServiceHome.isHidden = false
                            
                            cell.imgServiceTable.image = UIImage(named: iconsTyOrder[0])
                            cell.imgServiceHome.image = UIImage(named: iconsTyOrder[1])
                            break
                            
                        case 1:
                            cell.imgServiceEst.isHidden = false
                            cell.imgServiceBar.isHidden = true
                            cell.imgServiceTable.isHidden = true
                            cell.imgServiceHome.isHidden = true
                            
                            cell.imgServiceEst.image = UIImage(named: iconsTyOrder[0])
                            break
                            
                        default:
                            cell.imgServiceEst.isHidden = true
                            cell.imgServiceBar.isHidden = true
                            cell.imgServiceTable.isHidden = true
                            cell.imgServiceHome.isHidden = true
                            break
                        }
                        
                        
                        
                        cell.establishmentName.text = values[indexPath.row].name
                        if values[indexPath.row].future_available! {
                            cell.establishmentImage.image = UIImage(named: "comingSoon")
                            cell.bluredImage.image = UIImage(named:"comingSoon")
                        }else{
                            if let logo = values[indexPath.row].logo, logo != "" {
                                Nuke.loadImage(with: URL(string: logo)!, into: cell.establishmentImage)
                                Nuke.loadImage(with: URL(string: logo)!, into: cell.bluredImage)
                            }else{
                                cell.establishmentImage.image = UIImage(named: "placeholderImageRed")
                                cell.bluredImage.image = UIImage(named:"placeholderImageRed")
                            }
                        }
                        cell.subtitleLabel.text = values[indexPath.row].place + " - " + values[indexPath.row].category
                        cell.txtTime.text = values[indexPath.row].time + " min"
                        cell.viewTime.layer.cornerRadius = 10
                        cell.viewTime.clipsToBounds = true
                        cell.viewTime.layer.masksToBounds = true
                        cell.viewTime.layer.shadowColor = UIColor.black.cgColor
                        cell.viewTime.layer.shadowOffset = CGSize(width: 0, height: 2)
                        cell.viewTime.layer.shadowOpacity = 0.24
                        cell.viewTime.layer.shadowRadius = CGFloat(2)
                        if values[indexPath.row].is_open {
                            cell.statusLabel.text = "Abierto"
                            cell.noAvaliable.alpha = 0
                            cell.isUserInteractionEnabled = true
                        }else{
                            if values[indexPath.row].future_available! {
                                cell.statusLabel.text = "Próximamente"
                                cell.noAvaliable.alpha = 0
                                cell.isUserInteractionEnabled = false
                            }else{
                                cell.statusLabel.text = "Cerrado"
                                cell.noAvaliable.alpha = 1
                                cell.isUserInteractionEnabled = false
                            }
                            
                        }
                        
                        cell.rate(rating: values[indexPath.row].rating)
                        cell.noDisponible.alpha = 0
                        cell.establecimiento.alpha = 1
                    }else if promoValues.count > 0 {
                        cell.establishmentName.text = promoValues[indexPath.row].name
                        if promoValues[indexPath.row].futureAvailable! {
                            cell.establishmentImage.image = UIImage(named: "comingSoon")
                            cell.bluredImage.image = UIImage(named:"comingSoon")
                        }else{
                            if let logo = promoValues[indexPath.row].logo, logo != "" {
                                Nuke.loadImage(with: URL(string: logo)!, into: cell.establishmentImage)
                                Nuke.loadImage(with: URL(string: logo)!, into: cell.bluredImage)
                            }else{
                                cell.establishmentImage.image = UIImage(named: "placeholderImageRed")
                                cell.bluredImage.image = UIImage(named:"placeholderImageRed")
                            }
                        }
                        cell.subtitleLabel.text =  promoValues[indexPath.row].establishmentCategory
                        cell.txtTime.text = promoValues[indexPath.row].time + " min"
                        cell.viewTime.layer.cornerRadius = 10
                        cell.viewTime.clipsToBounds = true
                        cell.viewTime.layer.masksToBounds = true
                        cell.viewTime.layer.shadowColor = UIColor.black.cgColor
                        cell.viewTime.layer.shadowOffset = CGSize(width: 0, height: 2)
                        cell.viewTime.layer.shadowOpacity = 0.24
                        cell.viewTime.layer.shadowRadius = CGFloat(2)
                        
                        if promoValues[indexPath.row].isOpen || self.from == "adv" {
                            cell.statusLabel.text = "Abierto"
                            cell.noAvaliable.alpha = 0
                            cell.isUserInteractionEnabled = true
                        }else{
                            if promoValues[indexPath.row].futureAvailable! {
                                cell.statusLabel.text = "Próximamente"
                                cell.noAvaliable.alpha = 0
                                cell.isUserInteractionEnabled = false
                            }else{
                                cell.statusLabel.text = "Cerrado"
                                cell.noAvaliable.alpha = 1
                                cell.isUserInteractionEnabled = false
                            }
                            
                        }
                        
                        cell.rate(rating: promoValues[indexPath.row].rating)
                        cell.noDisponible.alpha = 0
                        cell.establecimiento.alpha = 1
                    }else{
                        cell.noDisponible.alpha = 1
                        cell.establecimiento.alpha = 0
                    }
                    return cell
                } else if promoValues.count > 0{
                    //} else if promoValues[indexPath.row].currentPromotionals > 0{
                    //Mark: Views de promos
                    let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentCell") as! BusinessAround
                    if let image = promoValues[indexPath.row].image,image != "" {
                        Nuke.loadImage(with: URL(string: image)!, into: cell.establishmentCover)
                    }
                    cell.selectionStyle = .none
                    cell.position = indexPath.row
                    cell.delegate = self
                    cell.partnerImage.isHidden = !promoValues[indexPath.row].noProfit
                    cell.promoCount.text = promoValues[indexPath.row].currentPromotionals.description
                    cell.businessName.text = promoValues[indexPath.row].name.description
                    cell.businessKind.text = promoValues[indexPath.row].advsCategory + " - " + promoValues[indexPath.row].subadvsCategory
                    cell.establishmentCover.layer.masksToBounds = false
                    cell.establishmentCover.layer.shadowColor = UIColor.black.cgColor
                    cell.establishmentCover.layer.shadowOffset = CGSize(width: 1, height: 0)
                    cell.establishmentCover.layer.shadowRadius = CGFloat(5)
                    cell.establishmentCover.layer.shadowOpacity = 0.24
                    
                    if promoValues[indexPath.row].activeNotifications {
                        cell.iconBell.image = UIImage(named: "iconBell")
                    }
                    return cell
                } else {
                    return UITableViewCell()
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if from == "guide" {
            return 240
        }else{
            if indexPath.row == totalEstablishments {
                return from == "guide" ? 0 : 30
            }else if indexPath.row == (totalEstablishments + 1) {
                return from == "guide" ? 0 : 392
            }else{
                if (self.from != "adv"){
                    return 280
                    //} else if promoValues[indexPath.row].currentPromotionals > 0 {
                } else if promoValues.count > 0 {
                    return 235
                } else {
                    return 0
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == totalEstablishments {
            
        }else if indexPath.row == (totalEstablishments + 1) {
            
        }else{
            if !errorFlag {
                if from == "guide"{
                    let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment_guide") as! SingleGuideCollectionViewCell
                    newVC.id = valuesaux[indexPath.row].id
                    newVC.municipioStr = valueCity
                    newVC.categoriaStr = valuesaux[indexPath.row].business_area
                    newVC.lugarString = valuesaux[indexPath.row].place
                    newVC.establString = ""
                    newVC.isServicePlus = valuesaux[indexPath.row].service_plus
                    if let km = valuesaux[indexPath.row].kms{
                        newVC.kilometers = valuesaux[indexPath.row].kms
                    }
                    
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else if from.isEmpty{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
                    newVC.id = (values.count > 0) ? values[indexPath.row].id : promoValues[indexPath.row].id
                    newVC.municipioStr = valueCity
                    newVC.categoriaStr = (values.count > 0) ? values[indexPath.row].category : promoValues[indexPath.row].establishmentCategory
                    newVC.lugarString = (values.count > 0) ? values[indexPath.row].place : promoValues[indexPath.row].place
                    newVC.establString = (values.count > 0) ? values[indexPath.row].name : promoValues[indexPath.row].name
                    self.navigationController?.pushViewController(newVC, animated: true)
                    //                if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool{
                    //                    if !order_done {
                    //                        defaults.set(values[indexPath.row].id, forKey: "establishment_id")
                    //                    }
                    //                }else{
                    //                    defaults.set(values[indexPath.row].id, forKey: "establishment_id")
                    //                }
                } else {
                    //Mark: Agregar flujo para que vaya a BusinessViewController
                }
            }
            
        }
    }
    
}


extension FilterResultsViewController: AlertAdsDelegate {
    
    func didSuccessGetViewEstablishmentAround(info: [IdsKm], ids: [Int]) {
        
        print(info)
        self.mIdList = info
        if info.count > 0 {
            guideWS.getGuideEstablishmentsById(ids: ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }else{
            LoadingOverlay.shared.hideOverlayView()
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "No se encontraron establecimientos, vuelve a intentarlo más tarde"
            newXIB.mTitle = "PickPal Carta Digital"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromHome = true
            newXIB.valueCall = 1
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
        
            
        }
    }
    
}

/*extension FilterResultsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
 func numberOfSections(in collectionView: UICollectionView) -> Int {
 return 1
 }
 
 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 return 6
 }
 
 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 if indexPath.item == 0 {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "municipio", for: indexPath) as! CollectionViewCell
 cell.delegate = self
 cell.place = false
 cell.category = false
 cell.city = true
 cell.cityLabel.text = valueCity
 return cell
 }else if indexPath.item == 1 {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "separator2", for: indexPath)
 return cell
 }else if indexPath.item == 2 {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoria", for: indexPath) as! CollectionViewCell
 cell.delegate = self
 cell.place = false
 cell.category = true
 cell.city = false
 cell.cityLabel.text = valueCategory
 return cell
 }else if indexPath.item == 3 {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "separator3", for: indexPath)
 return cell
 }else if indexPath.item == 4 {
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "lugar", for: indexPath) as! CollectionViewCell
 cell.delegate = self
 cell.place = true
 cell.category = false
 cell.city = false
 cell.cityLabel.text = valuePlace
 
 return cell
 
 }else{
 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "acomer", for: indexPath) as! EatCollectionViewCell
 cell.lblSearchEat.text = from == "guide" ? "¡Buscar!" : "¡A comer!"
 if valueCity != "" && valueCategory != "" && valuePlace != ""{
 cell.municipio = valueCity
 cell.categoria = valueCategory
 cell.lugar = valuePlace
 cell.delegate = self
 }
 return cell
 }
 
 }
 
 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
 if indexPath.item == 0 || indexPath.item == 2{
 if from == "guide" {
 return CGSize(width: (view.frame.width + 15) / 3 , height: 27.5)
 }else{
 return CGSize(width: view.frame.width / 4 , height: 27.5)
 }
 }else if indexPath.item == 1 {
 return CGSize(width: 1, height: 27.5)
 }else if indexPath.item == 3 {
 if from == "guide"{
 return CGSize(width: 0, height: 0)
 }else{
 let width = (collectionView.frame.width - 10) / 4
 return CGSize(width: 1, height: 27.5)
 }
 }else if indexPath.item == 4{
 if from == "guide"{
 return CGSize(width: 0, height: 0)
 }else{
 let width = (collectionView.frame.width - 10) / 4
 return CGSize(width: width, height: 27.5)
 }
 }else{
 let width = (collectionView.frame.width - 10) / 4
 return CGSize(width: width, height: 27.5)
 }
 }
 
 override var preferredStatusBarStyle: UIStatusBarStyle {
 return .lightContent
 }
 }*/
