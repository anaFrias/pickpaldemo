//
//  MyAddressesViewController.swift
//  PickPalTesting
//
//  Copyright © 2020 Oscar Marinez. All rights reserved.
//

import UIKit
import GoogleMaps


class MyAddressesViewController: UIViewController,deliveryDelegate, actionsAddresDelegate, CLLocationManagerDelegate,  UITableViewDelegate, UITableViewDataSource {

    
    

    @IBOutlet weak var tableMyAddress: UITableView!
    @IBOutlet weak var collectionViewAddress: UICollectionView!
    var isFromPromos:Bool = false
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var deliveryDelegate = DeliveryWS()
    var arrayAddress = [Address]()
    var addressSelect = Address()
    var addressDefault = Address()
    var locationManager = CLLocationManager()
    var itemSelect = Int ()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableMyAddress.delegate = self
        tableMyAddress.dataSource = self
        
        deliveryDelegate.delegate = self
        
        deliveryDelegate.getAddresList(client_id: client_id)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
           deliveryDelegate.getAddresList(client_id: client_id)
        
       }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        addressDefault.latitude = locValue.latitude
        addressDefault.longitude = locValue.longitude
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func getAddressUser(addressCatalog: [Address]){

        
        itemSelect = addressCatalog.count + 1
        self.arrayAddress = addressCatalog
        tableMyAddress.reloadData()
    }

    
    func failGetAddress(error: String, subtitle: String){
        
         print("ERROR Lista direcciones ", error)
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
      /*  let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)*/
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressSettingsCollectionViewCell;
       
        cell.iconAddress.clipsToBounds = false
        cell.iconAddress.layer.masksToBounds = true
        cell.iconAddress.layer.cornerRadius = 8
        
        cell.delegate = self
        cell.buttonEditAddress.tag = indexPath.item
        cell.buttonEditAddress.addTarget(self, action: #selector(self.editarDireccion), for: .touchUpInside)
        cell.buttonDeleteAddress.tag = indexPath.item
        cell.buttonDeleteAddress.addTarget(self, action: #selector(self.eliminarDireccion), for: .touchUpInside)
        
        if arrayAddress[indexPath.row].numeroExterior! != ""{
            
            if arrayAddress[indexPath.row].numeroInterior! != ""{
                
                cell.addresLabel.text = "# \(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), no. Int. \(arrayAddress[indexPath.row].numeroInterior!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
                
            }else{
                
                cell.addresLabel.text = "# \(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
            }
            
        }else{
            
            if arrayAddress[indexPath.row].numeroInterior! != ""{
                
                cell.addresLabel.text = "\(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), no. Int. \(arrayAddress[indexPath.row].numeroInterior!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
            }else{
                
                cell.addresLabel.text = "\(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
            }
        }
        
        
        return cell;
    }
    

    
    
    
    @objc func editAddres(){
        
        
        print("Editar la direccion" , addressSelect.street!)
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
         let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
        newV.isEditAddrress = true
        newV.typeAddress = "edit"
        newV.direccion = addressSelect
        newV.from = "MyAddress"
        newV.titleSectionText = "Dirección Registrada"
        self.navigationController?.pushViewController(newV, animated: true)
        
    }
    
    
    @objc func editarDireccion(_ sender: UIButton){
        
        addressSelect = arrayAddress[sender.tag]
        
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
         let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
        newV.isEditAddrress = true
        newV.typeAddress = "edit"
        newV.direccion = addressSelect
        newV.from = "MyAddress"
        newV.titleSectionText = "Dirección Registrada"
        self.navigationController?.pushViewController(newV, animated: true)
        
    }
   
    func deleteAddress(){
        
         print("Eliminar la direccion", addressSelect.street! )
        alertConfirmation()
        //deliveryDelegate.deleteAddress(address: addressSelect)
        
    }
    
    @objc func eliminarDireccion(_ sender: UIButton){
    addressSelect = arrayAddress[sender.tag]
    print("Eliminar la direccion", addressSelect.street! )
    alertConfirmation()
    
    }
    
    @IBAction func editAddress(_ sender: Any) {
        
        print("Editar la direccion" , addressSelect.street!)
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
         let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
        newV.isEditAddrress = true
        newV.typeAddress = "edit"
        newV.direccion = addressSelect
        newV.from = "MyAddress"
        newV.titleSectionText = "Dirección Registrada"
        self.navigationController?.pushViewController(newV, animated: true)
        
        
    }
    
    func deleteAddressUser(){
        
        deliveryDelegate.getAddresList(client_id: client_id)
        arrayAddress.removeAll()
        tableMyAddress.reloadData()
        
    }
    
    func failDeleteAddress(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
        
       
    }
    
    
    
    @IBAction func btn_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btn_home(_ sender: Any) {
        
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    
    @IBAction func home(_ sender: Any) {
        
        print("Nuevo home")
        
    }
    @IBAction func bnt_pedidos(_ sender: Any) {
        
        print("botpn home")
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
               newVC.isFromPromos = isFromPromos
               self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    
    @IBAction func btn_settings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               newVC.isFromPromos = isFromPromos
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func addAddress(_ sender: Any) {
        
        
//              let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
//              let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
//                newV.titleSectionText = "Agregar una nueva dirección"
//                newV.from = "MyAddress"
//                newV.editAddress = addressDefault
//                newV.actionAddress = "newAddress"
//                newV.isEditAddrress = true
        
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newV = storyboar.instantiateViewController(withIdentifier: "DireccionesRegistradas") as! MailingAddressViewController
        newV.from = "Edit"
        
              self.navigationController?.pushViewController(newV, animated: true)
             
        
    }
    
    
    func alertConfirmation(){
        
        
        let refreshAlert = UIAlertController(title: addressSelect.street!, message: "¿Deseas eliminar esta dirección registrada?", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "Eliminar", style: .default, handler: { (action: UIAlertAction!) in
            self.deliveryDelegate.deleteAddress(address: self.addressSelect)
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            refreshAlert.dismiss(animated: true, completion: nil)
        }))

        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
    }
  
    
    
}
