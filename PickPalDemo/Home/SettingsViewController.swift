//
//  SettingsViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 04/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import OneSignal

class SettingsViewController: UIViewController, userDelegate {
    
    var userws = UserWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var user = UserProfile()
    var first_time = true
    //Josue Valdez.- Se agrega bandera para saber de donde ha sido llamada la pantalla, isFromPromos de tipo booleano solo se envia
    //solo se envia el parametro en HomePromosViewController como true, si no se envia un parametro por defecto sera false
    var isFromPromos:Bool = false
    var isFromGuide: Bool = false
    var whereFrom: String = ""
    var isUpdatedVersion = Bool()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var numPedidos: UIView!
    
    @IBOutlet weak var vewPedidos: UIView!
    @IBOutlet weak var logNewsFeed: UILabel!
    @IBOutlet weak var imgLogoToolbar: UIImageView!
    @IBOutlet weak var imgNewsOrExit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.viewProfile(client_id: client_id)
        userws.getUnreadNewsFeedLog(client_id: client_id)
        vewPedidos.layer.cornerRadius = vewPedidos.frame.height / 2
        userws.isUpdateAvailable()
        
        switch whereFrom {
        case Constants.PICKPAL_PUNTOS:
            imgLogoToolbar.image = UIImage(named: "pickpalGuia")
            imgNewsOrExit.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
            vewPedidos.alpha = 0
        default:
            if isFromGuide{
                imgLogoToolbar.image = UIImage(named: "ic_header_guide")
                imgNewsOrExit.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
                vewPedidos.alpha = 0
            }else{
                if isFromPromos{
                    imgLogoToolbar.image = UIImage(named: "pickpalPromo")
                    imgNewsOrExit.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
                    vewPedidos.alpha = 0
                }else{
                    imgLogoToolbar.image = UIImage(named: "toolBarLogo")
                    imgNewsOrExit.setImage(UIImage(named: "iconNewsWhite") , for: UIControlState.normal)
                }
            }
        }
    }
    
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
        }
    }
    
    func didSuccessGetNewsFeedLog(newsFeed: Int) {
        if newsFeed == 0 {
            vewPedidos.alpha = 0
        }else{
            vewPedidos.alpha = 1
            logNewsFeed.text = "\(1)"
        }
    }
    func didFailGetNewsFeedLog(title: String, subtitle: String) {
        vewPedidos.alpha = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        userws.viewProfile(client_id: client_id)
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
    }
    
    func didSuccessDeleteAccount() {
        LoadingOverlay.shared.hideOverlayView()
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "is_register")
        UserDefaults.standard.removeObject(forKey: "phone")
        UserDefaults.standard.removeObject(forKey: "complete_info")
        UserDefaults.standard.removeObject(forKey: "login")
        UserDefaults.standard.removeObject(forKey: "phone_verificaton")
        UserDefaults.standard.removeObject(forKey: "screen")
        UserDefaults.standard.removeObject(forKey: "storyboard")
        UserDefaults.standard.removeObject(forKey: "municipalty")
        UserDefaults.standard.removeObject(forKey: "state")
        UserDefaults.standard.removeObject(forKey: "category")
        UserDefaults.standard.removeObject(forKey: "place")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func didFailDeleteAccount(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    
    //Josue Valdez.- isFromPromos determina la funcion que hara el boton
    @IBAction func nesFeed(_ sender: Any) {
        if isFromPromos || isFromGuide || self.whereFrom == Constants.PICKPAL_PUNTOS{
            UserDefaults.standard.set("menu", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    //Josue Valdez.- isFromPromos determina la funcion que hara el boton
    @IBAction func settings(_ sender: Any) {
        if !isFromPromos || !isFromGuide || self.whereFrom != Constants.PICKPAL_PUNTOS{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func pedidos(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.isFromPromos = isFromPromos
            newVC.isFromGuide = isFromGuide
            newVC.whereFrom = whereFrom
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    //Josue Valdez.- si la pantalla fue abierta desde pickpalpromos redigira a homepromos
    @IBAction func home(_ sender: Any) {
        switch whereFrom {
        case Constants.PICKPAL_PUNTOS:
            let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
            
        case Constants.PICKPAL_WALLET:
            
            
                let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
                self.navigationController?.pushViewController(newVC, animated: true)

            
            
        default:
            if !isFromPromos {
                if isFromGuide{
                    let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
                    let navController = UINavigationController(rootViewController: newVC)
                    navController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
                
            }else{
                UserDefaults.standard.set("promos", forKey: "place")
                let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
                let navController = UINavigationController(rootViewController: newVC)
                navController.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }
    }
    
    func didSuccessViewProfile(profile: UserProfile) {
        LoadingOverlay.shared.hideOverlayView()
        user = profile
        tableView.reloadData()
    }
    
    func didFailViewProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if title != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    
    func showDeleteAccountDialog(){
        let alert = UIAlertController(title: "Eliminar cuenta", message: "¿Estás seguro? Todos los pedidos, facturas e información de tu cuenta serán eliminada. Esta acción no se puede revertir. \n\nAl eliminar la cuenta se cerrará tu sesión.", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .destructive) { (alert) in
            LoadingOverlay.shared.showOverlay(view: self.view)
            self.userws.deleteAccountUser(client_id: self.client_id)
        }
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Josue valdez.- Si availableAdvs es null o false no se mostrará la opcion "Mis promociones, si es true podremos mostrarla y
    //determina cuantos items vamos a mostrar dentro del tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user.availableAdvs != nil && !user.availableAdvs! {
            print("primero numberOfRowsInSection")
            return 11
        } else if user.availableAdvs != nil && user.availableAdvs! {
            print("Segundo numberOfRowsInSection")
            return 12
        }else{
            print("Segundo numberOfRowsInSection")
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if user.availableAdvs != nil && !user.availableAdvs! {
            
            switch indexPath.row {
            case 1:
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
                first_time = false
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
                
            
            case 2:
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Address") as! MyAddressesViewController
             
               self.navigationController?.pushViewController(newVC, animated: true)
            
            
            
            case 3:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                   let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileCards") as! ProfileCardsViewController
                       newVC.isFromPromos = isFromPromos
                               newVC.first_time = true
                               self.navigationController?.pushViewController(newVC, animated: true)
                
            case 4:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileAdrdess") as! ProfileAddressViewController
                newVC.isFromPromos = isFromPromos
                newVC.first_time = true
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 5:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "OrdersList") as! OrderHelpListViewController
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 6:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "About") as! AboutViewController
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
                
                
            case 7:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                               let newVC = storyboard.instantiateViewController(withIdentifier: "Contacto") as! ContactoViewController
                               newVC.isFromPromos = isFromPromos
                               self.navigationController?.pushViewController(newVC, animated: true)
                
            case 8:
                
                guard let url = URL(string: "http://www.pickpal.com/admin/request_owner/") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                    
            case 9:
                
                let alert = UIAlertController(title: "PickPal", message: "Al cerrar sesión si tienes pedidos activos no recibiras notificaciones", preferredStyle: .alert)
                               let action = UIAlertAction(title: "Aceptar", style: .destructive) { (alert) in
                                   UserDefaults.standard.removeObject(forKey: "first_name")
                                   UserDefaults.standard.removeObject(forKey: "client_id")
                                   UserDefaults.standard.removeObject(forKey: "is_register")
                                   UserDefaults.standard.removeObject(forKey: "phone")
                                   UserDefaults.standard.removeObject(forKey: "complete_info")
                                   UserDefaults.standard.removeObject(forKey: "login")
                                   UserDefaults.standard.removeObject(forKey: "phone_verificaton")
                                   UserDefaults.standard.removeObject(forKey: "screen")
                                   UserDefaults.standard.removeObject(forKey: "storyboard")
                                   UserDefaults.standard.removeObject(forKey: "municipalty")
                                   UserDefaults.standard.removeObject(forKey: "state")
                                   UserDefaults.standard.removeObject(forKey: "category")
                                   UserDefaults.standard.removeObject(forKey: "place")
                                   UserDefaults.standard.removeObject(forKey: "age")
                                   let db = try! Connection("\(path)/orders.db")
                                   try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
                                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                   let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
                                   vc.closeSession = true
                                   OneSignal.setSubscription(false)
                                   self.navigationController?.pushViewController(vc,animated: true)
                               }
                               alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
                               alert.addAction(action)
                               self.present(alert, animated: true, completion: nil)
            case 10:
                self.showDeleteAccountDialog()
                
            default:
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
    
            }
            
            
         } else if user.availableAdvs != nil && user.availableAdvs! {
            
            
            switch indexPath.row {
            case 1:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
                newVC.isFromPromos = isFromPromos
                first_time = false
                self.navigationController?.pushViewController(newVC, animated: true)
                
                case 2:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Address") as! MyAddressesViewController
                 
                   self.navigationController?.pushViewController(newVC, animated: true)
                
                
            case 3:
                let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                               let newVC = storyboard.instantiateViewController(withIdentifier: "MyEstabsAdvs") as! MyEstabsAdvsViewController
                               newVC.isFromPromos = isFromPromos
                               self.navigationController?.pushViewController(newVC, animated: true)
            case 4:
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileCards") as! ProfileCardsViewController
                newVC.isFromPromos = isFromPromos
                newVC.first_time = true
                self.navigationController?.pushViewController(newVC, animated: true)
            case 5:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileAdrdess") as! ProfileAddressViewController
                newVC.isFromPromos = isFromPromos
                newVC.first_time = true
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 6:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "OrdersList") as! OrderHelpListViewController
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 7:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                               let newVC = storyboard.instantiateViewController(withIdentifier: "About") as! AboutViewController
                               newVC.isFromPromos = isFromPromos
                               self.navigationController?.pushViewController(newVC, animated: true)
                
            case 8:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Contacto") as! ContactoViewController
                newVC.isFromPromos = isFromPromos
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 9:
                
                //            let storyboard = UIStoryboard(name: "Home", bundle: nil)
                //            let newVC = storyboard.instantiateViewController(withIdentifier: "registerNew") as! RegisterNewEstablishmentViewController
                //            self.navigationController?.pushViewController(newVC, animated: true)
                //            pickpal.com/admin/request_owner
                guard let url = URL(string: "http://www.pickpal.com/admin/request_owner/") else {
                    return //be safe
                }
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
                
                
            case 10:
                
                
                let alert = UIAlertController(title: "PickPal", message: "Al cerrar sesión si tienes pedidos activos no recibiras notificaciones", preferredStyle: .alert)
                let action = UIAlertAction(title: "Aceptar", style: .destructive) { (alert) in
                    UserDefaults.standard.removeObject(forKey: "first_name")
                    UserDefaults.standard.removeObject(forKey: "client_id")
                    UserDefaults.standard.removeObject(forKey: "is_register")
                    UserDefaults.standard.removeObject(forKey: "phone")
                    UserDefaults.standard.removeObject(forKey: "complete_info")
                    UserDefaults.standard.removeObject(forKey: "login")
                    UserDefaults.standard.removeObject(forKey: "phone_verificaton")
                    UserDefaults.standard.removeObject(forKey: "screen")
                    UserDefaults.standard.removeObject(forKey: "storyboard")
                    UserDefaults.standard.removeObject(forKey: "municipalty")
                    UserDefaults.standard.removeObject(forKey: "state")
                    UserDefaults.standard.removeObject(forKey: "category")
                    UserDefaults.standard.removeObject(forKey: "place")
                    UserDefaults.standard.removeObject(forKey: "age")
                    let db = try! Connection("\(path)/orders.db")
                    try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
                    vc.closeSession = true
                    OneSignal.setSubscription(false)
                    self.navigationController?.pushViewController(vc,animated: true)
                }
                alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            
            case 11:
                self.showDeleteAccountDialog()
                
            default:
                 let storyboard = UIStoryboard(name: "Home", bundle: nil)
                               let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
            }
            
        }else{
            
            switch indexPath.row {
            case 1:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
                first_time = false
                self.navigationController?.pushViewController(newVC, animated: true)
                
                
                case 2:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                   let newVC = storyboard.instantiateViewController(withIdentifier: "Address") as! MyAddressesViewController
                 
                   self.navigationController?.pushViewController(newVC, animated: true)

                
            case 3:
                
                let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                                              let newVC = storyboard.instantiateViewController(withIdentifier: "MyEstabsAdvs") as! MyEstabsAdvsViewController
                                              newVC.isFromPromos = isFromPromos
                                              self.navigationController?.pushViewController(newVC, animated: true)
                
            case 4:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "ProfileCards") as! ProfileCardsViewController
                newVC.isFromPromos = isFromPromos
                newVC.first_time = true
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 5:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                               let newVC = storyboard.instantiateViewController(withIdentifier: "OrdersList") as! OrderHelpListViewController
                               self.navigationController?.pushViewController(newVC, animated: true)
                
            case 6:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "About") as! AboutViewController
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 7:
                
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Contacto") as! ContactoViewController
                self.navigationController?.pushViewController(newVC, animated: true)
                
            case 8:
                
                //            let storyboard = UIStoryboard(name: "Home", bundle: nil)
                               //            let newVC = storyboard.instantiateViewController(withIdentifier: "registerNew") as! RegisterNewEstablishmentViewController
                               //            self.navigationController?.pushViewController(newVC, animated: true)
                               //            pickpal.com/admin/request_owner
                               guard let url = URL(string: "http://www.pickpal.com/admin/request_owner/") else {
                                   return //be safe
                               }
                               
                               if #available(iOS 10.0, *) {
                                   UIApplication.shared.open(url, options: [:], completionHandler: nil)
                               } else {
                                   UIApplication.shared.openURL(url)
                               }
                
            case 9:
                
                let alert = UIAlertController(title: "PickPal", message: "Al cerrar sesión si tienes pedidos activos no recibiras notificaciones", preferredStyle: .alert)
                let action = UIAlertAction(title: "Aceptar", style: .destructive) { (alert) in
                    UserDefaults.standard.removeObject(forKey: "first_name")
                    UserDefaults.standard.removeObject(forKey: "client_id")
                    UserDefaults.standard.removeObject(forKey: "is_register")
                    UserDefaults.standard.removeObject(forKey: "phone")
                    UserDefaults.standard.removeObject(forKey: "complete_info")
                    UserDefaults.standard.removeObject(forKey: "login")
                    UserDefaults.standard.removeObject(forKey: "phone_verificaton")
                    UserDefaults.standard.removeObject(forKey: "screen")
                    UserDefaults.standard.removeObject(forKey: "storyboard")
                    UserDefaults.standard.removeObject(forKey: "municipalty")
                    UserDefaults.standard.removeObject(forKey: "state")
                    UserDefaults.standard.removeObject(forKey: "category")
                    UserDefaults.standard.removeObject(forKey: "place")
                    UserDefaults.standard.removeObject(forKey: "age")
                    let db = try! Connection("\(path)/orders.db")
                    try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
                    vc.closeSession = true
                    OneSignal.setSubscription(false)
                    self.navigationController?.pushViewController(vc,animated: true)
                }
                alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                
            case 10:
                self.showDeleteAccountDialog()
                
            default:
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "edit_profile") as! EditProfileViewController
            }
            
            
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if user.availableAdvs != nil && !user.availableAdvs! {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsResume") as! SettingsResumeTableViewCell
                cell.userName.text = user.name
                cell.userEmail.text = user.email
                if user.phone != nil {
                    cell.userPhone.text = Extensions.format(phoneNumber: user.phone!)
                }
                if user.avatar != nil && user.avatar != ""{
                    Nuke.loadImage(with: URL(string: user.avatar!)!, into: cell.img)
                    cell.img.layer.cornerRadius = 70
                }
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                 cell.name.text = "Mi Perfil"
                 cell.icon.image = UIImage(named: "iconProfile")
                return cell
            }else if indexPath.row == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Domicilios Registrados"
                cell.icon.image = UIImage(named: "iconDirecciones")
                
                return cell
            }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Tarjetas Bancarias Registradas"
                cell.icon.image = UIImage(named: "iconTarjetas")
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.icon.image = UIImage(named: "iconFacturacion")
                cell.name.text = "Datos de Facturación"
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Ayuda"
                cell.icon.image = UIImage(named: "helpIcon")
                return cell
            }else if indexPath.row == 6{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Acerca de PickPal"
                cell.icon.image = UIImage(named: "iconAcerca")
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Contacto PickPal"
                cell.icon.image = UIImage(named: "iconContacto")
                return cell
            }else if indexPath.row == 8{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Registra tu negocio"
                cell.icon.image = UIImage(named: "iconBuilding")
                return cell
            }else if indexPath.row == 9{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Cerrar Sesión"
                cell.icon.image = UIImage(named: "iconCloseSession")
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Eliminar cuenta"
                cell.icon.image = UIImage(named: "deleteUser")
                return cell
            }
        } else if user.availableAdvs != nil && user.availableAdvs! {
           
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsResume") as! SettingsResumeTableViewCell
                cell.userName.text = user.name
                cell.userEmail.text = user.email
                if user.phone != nil {
                    cell.userPhone.text = Extensions.format(phoneNumber: user.phone!)
                }
                if user.avatar != nil && user.avatar != ""{
                    Nuke.loadImage(with: URL(string: user.avatar!)!, into: cell.img)
                    cell.img.layer.cornerRadius = 70
                }
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Mi Perfil"
                cell.icon.image = UIImage(named: "iconProfile")
                return cell
            }else  if indexPath.row == 2{
                           let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                           cell.name.text = "Domicilios Registrados"
                           cell.icon.image = UIImage(named: "iconDirecciones")
                           
                           return cell
           }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "PickPal Notipromos"
                cell.icon.image = UIImage(named: "myPromos")
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Tarjetas Bancarias Registradas"
                cell.icon.image = UIImage(named: "iconTarjetas")
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.icon.image = UIImage(named: "iconFacturacion")
                cell.name.text = "Datos de Facturación"
                return cell
            }else if indexPath.row == 6{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Ayuda"
                cell.icon.image = UIImage(named: "helpIcon")
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Acerca de PickPal"
                cell.icon.image = UIImage(named: "iconAcerca")
                return cell
            }else if indexPath.row == 8{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Contacto PickPal"
                cell.icon.image = UIImage(named: "iconContacto")
                return cell
            }else if indexPath.row == 9{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Registra tu negocio"
                cell.icon.image = UIImage(named: "iconBuilding")
                return cell
            }else if indexPath.row == 10{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Cerrar Sesión"
                cell.icon.image = UIImage(named: "iconCloseSession")
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Eliminar cuenta"
                cell.icon.image = UIImage(named: "deleteUser")
                return cell
            }
        }else{
            if indexPath.row == 0 {
              
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsResume") as! SettingsResumeTableViewCell
                cell.userName.text = user.name
                cell.userEmail.text = user.email
                if user.phone != nil {
                    cell.userPhone.text = Extensions.format(phoneNumber: user.phone!)
                }
                if user.avatar != nil && user.avatar != ""{
                    Nuke.loadImage(with: URL(string: user.avatar!)!, into: cell.img)
                    cell.img.layer.cornerRadius = 70
                }
                return cell
            }else if indexPath.row == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Mi Perfil"
                cell.icon.image = UIImage(named: "iconProfile")
                return cell
            }else if indexPath.row == 2{
               let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
               cell.name.text = "Domicilios Registrados"
               cell.icon.image = UIImage(named: "iconDirecciones")
                               
               return cell
               }else if indexPath.row == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Tarjetas Bancarias Registradas"
                cell.icon.image = UIImage(named: "iconTarjetas")
                return cell
            }else if indexPath.row == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.icon.image = UIImage(named: "iconFacturacion")
                cell.name.text = "Datos de Facturación"
                return cell
            }else if indexPath.row == 5{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Ayuda"
                cell.icon.image = UIImage(named: "helpIcon")
                return cell
            }else if indexPath.row == 6{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Acerca de PickPal"
                cell.icon.image = UIImage(named: "iconAcerca")
                return cell
            }else if indexPath.row == 7{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Contacto PickPal"
                cell.icon.image = UIImage(named: "iconContacto")
                return cell
            }else if indexPath.row == 8{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Registra tu negocio"
                cell.icon.image = UIImage(named: "iconBuilding")
                return cell
            }else if indexPath.row == 9{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Cerrar Sesión"
                cell.icon.image = UIImage(named: "iconCloseSession")
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell") as! SettingsCellTableViewCell
                cell.name.text = "Eliminar cuenta"
                cell.icon.image = UIImage(named: "deleteUser")
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }else{
            return 50
        }
    }
}

