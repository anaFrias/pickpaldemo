//
//  PedidosAnterioresViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 07/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import UserNotifications
import Firebase
import SQLite
import OneSignal

class PedidosAnterioresViewController: UIViewController, userDelegate, pushViewPedidos, ordersDelegate, showCurrentOrder, cancelOrderDelegate, porPagarCellDelegate, cancelCartCardDelegate, cancelCartV2Delegate, openHelpDelegate, AlertAdsDelegate, UNUserNotificationCenterDelegate, CustomerServiceDelegate{
    
    @IBOutlet weak var btnActual: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderTableView: UITableView!
    
    //    Pedidos indicador
    @IBOutlet weak var viewPedidos: UIView!
    @IBOutlet weak var labelPedidos: UILabel!
    
    @IBOutlet weak var nombreEstablecimiento: UILabel!
    @IBOutlet weak var historial: UIButton!
    @IBOutlet weak var tableContainer: UIView!
    @IBOutlet weak var noCurrent: UIView!
    @IBOutlet weak var generarFacturaLabel: UILabel!
    
    @IBOutlet weak var imgLogoToolbar: UIImageView!
    @IBOutlet weak var btnNewsFeed: UIButton!
    @IBOutlet weak var numPedido: UIView!
    @IBOutlet weak var folioPedido: UILabel!
    
    var myorders = [Orders]()
    var userWS = UserWS()
    var ordersWS = OrdersWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var order_id = Int()
    var customerServiceWS = CustomerServiceWS()
    
    
    @IBOutlet weak var viewEstablecimiento: UIView!
    //    VARS current order
    var dinamicHeight: CGFloat = 70
    var heightRefund: Int = 0
    var dinamicHeightMinimizada = [[CGFloat]]()
    var Minimizada : CGFloat = 60
    var order_elements = [Dictionary<String, Any>]()
    var pricesFinal =  [Double]()
    var subtotal = 0.00
    var total = 0.00
    var defaults = UserDefaults.standard
    var statusLabel = String()
    var arrays = [String]()
    var status = String()
    var ws = OrdersWS()
    var statusCode = Int()
    var comission = Double()
    var dateString = String()
    var values = [[String]]()
    var headerName:[String] = ["Pedidos por pagar", "Pedidos pagados", "Pedidos por reembolso"]
    var pedidos = [[Pedidos]]()
    var openFlag = [[Bool]]()
    var indexPath = IndexPath()
    var orders = [Orders]()
    var allItemsOrder = [[String]]()
    var notifications = Int()
    var arrayNotifications = [Int]()
    var isFromPromos:Bool = false
    var isFromGuide:Bool = false
    var whereFrom: String = ""
    var orderServiceClient = Pedidos()
    var ultimoPedido = Int()
    var orderId = Int()
    
    var seconds = 5
    var timer = Timer()
    var orderIdRefundDetails = Int()
    var sizeTotales : CGFloat = 0
    
    @IBOutlet weak var totalOrdersLabel: UILabel!
    
    @IBOutlet weak var viewNews: UIView!
    @IBOutlet weak var newsLabel: UILabel!
    
    var showPedido = false
    var readyDesplegar = false
    var desplegarCaducado = false
    var referenceItems: DatabaseReference!
    var handleItems: DatabaseHandle!
    
    var referenceModifiers: DatabaseReference!
    var handleModifiers: DatabaseHandle!
    
    var referenceExtras: DatabaseReference!
    var handleExtras: DatabaseHandle!
    var itemSelectType = ""
    var itemSelectIsPay = Bool()
    var center = UNUserNotificationCenter.current()
    
    var currentOrderId = Int()
    var showHistorial = false
    var activateNotifications = Bool()
    var numeroPedidos = 0
    var goToServiceClient = false
    var pedidoEntregado = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        orderTableView.dataSource = self
        orderTableView.delegate = self
        userWS.delegate = self
        ordersWS.delegate = self
        customerServiceWS.delegate = self
        viewNews.layer.cornerRadius = viewNews.frame.height / 2
        loadDateOrden()
        NotificationCenter.default.addObserver(self, selector: #selector(PedidosAnterioresViewController.deletePedido(_:)), name: NSNotification.Name(rawValue: "deletePedido"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PedidosAnterioresViewController.updateStatus(_:)), name: NSNotification.Name(rawValue: "updateStatus"), object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(PedidosAnterioresViewController.updateStatus(_:), name: NSNotification.Name(rawValue: "updateStatus"), object: nil))
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        userWS.getPickPalComision()
        
        userWS.viewmyOrders(client_id: client_id)
        //        print(pedidos.count, "viewDidLoad")
        ordersWS.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        self.navigationController?.isNavigationBarHidden = true
        tableView.flashScrollIndicators()
        if let order_id_temp = UserDefaults.standard.object(forKey: "order_id") as? Int {
            order_id = order_id_temp
        }
        self.view.isUserInteractionEnabled = false
        LoadingOverlay.shared.showOverlay(view: self.view)
        
        userWS.getUnreadNewsFeedLog(client_id: client_id)
        showCurrentOrder()
        let center = UNUserNotificationCenter.current()
        center.getPendingNotificationRequests(completionHandler: { requests in
            for request in requests {
                print(request, "notificaciones pendiente")
            }
        })
        
        switch whereFrom {
        case Constants.PICKPAL_PUNTOS:
            imgLogoToolbar.image = UIImage(named: "pickpalGuia")
            btnNewsFeed.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
        default:
            if isFromGuide{
                imgLogoToolbar.image = UIImage(named: "ic_header_guide")
                btnNewsFeed.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
            }else{
                if isFromPromos{
                    imgLogoToolbar.image = UIImage(named: "pickpalPromo")
                    btnNewsFeed.setImage(UIImage(named: "cambiarhome") , for: UIControlState.normal)
                }else{
                    imgLogoToolbar.image = UIImage(named: "toolBarLogo")
                    btnNewsFeed.setImage(UIImage(named: "iconNewsWhite") , for: UIControlState.normal)
                }
            }
        }
        
    }
    
    
    @IBAction func alertCondicionesPagados(_ sender: Any) {
        
        
        
       
        
    }
    
    @IBAction func btnRefundDetails(_ sender: Any) {
        
   
        
    }
    
    
    func loadDateOrden() {
        print("Orden consultando")
        ordersWS.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
    }
    
    
    @IBAction func concionesPorPagar(_ sender: UIButton) {
        
        
        
        switch itemSelectType {
            
        case "RS":
            
            let newXIB = AlertConditionsTwoViewController(nibName: "AlertConditionsTwoViewController", bundle: nil)
            
            newXIB.primerTexto = "1. Debes de pagar tu pedido una vez notificado. De lo contario se cancelará."
            newXIB.segundoTexto = ""
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
        case "SD":
            
            let newXIB = AlertConditionsTwoViewController(nibName: "AlertConditionsTwoViewController", bundle: nil)
            
            newXIB.primerTexto = "1. Paga tu pedido al repartidor una vez recibido."
            newXIB.segundoTexto = ""
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
            
            
        default:
            print("Sin opciones")
        }
        
    }
    
    @IBAction func conditiosOrdersToPay(_ sender: Any) {
        
        
        print("Sin opciones")
        
        
    }
    @IBAction func btnCustomerService(_ sender: Any) {
        
        
        
        print("Hola esta es la nueva funsion")
        if goToServiceClient{
            LoadingOverlay.shared.showOverlay(view: self.view)
            self.view.isUserInteractionEnabled = false
            
            customerServiceWS.getSingleServiceClient(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, establishment_id: orderServiceClient.establishment_id, number_table: orderServiceClient.number_table)
            
            
        }else{
            
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "Una vez que tu pedido este en estatus \"en proceso\" podrás utilizar las funciones de \"Llamar Mesero\" y \"Pedir Cuenta\"."
            newXIB.mTitle = "Error"
            newXIB.mToHome = false
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.valueCall = 2
            newXIB.isFromAdd = false
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
        }
        
    }
    
    
    
    func getSingleCustomerService(infoSingle: MySingleServiceClient ){
        
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleCustumerService") as! SingleCustumerServiceViewController
        newVC.infoSingle = infoSingle
        newVC.numeroPedidos = numeroPedidos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    func didFailGetSingleCustomerService(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        print("Error: \(error)")
        
    }
    
    
    @IBAction func OpenConditionsOrders(_ sender: Any) {
        
        
        switch itemSelectType {
        case "RS":
            
            let newXIB = OrderConditionsViewController(nibName: "OrderConditionsViewController", bundle: nil)
            newXIB.primertexto = "1.Recoge tu pedido una vez notificado que está listo o dentro de los siguientes 5 días hábiles a partir de tu compra."
            newXIB.segundoTexto = "2. En caso contrario, tendrás 10 días naturales para pasar a recoger tu pedido si es que se sigue teniendo en existencia o tendrás oportunidad de reembolsar tu compra. El reembolso se efectuará por el subtotal de tu compra menos una penalidad."
            newXIB.tercertexto = "3. Cualquier aclaración sobre un pedido que no pueda resolver el comercio, por favor comunícate con nosotros ingresando desde la apliacion al botón de Ajustes> Ayuda> selecciona el pedido con el que necesites ayuda y rellena el formulario."
            
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
            
        case "SD":
            let newXIB = OrderConditionsViewController(nibName: "OrderConditionsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
            
            
        default:
            print("Sin opciones")
        }
        
    }
    
    
    
    
    
    func openHelp(order_id: Int) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "refundTrouble") as! RefundTroubleHelpViewController
        newVC.orderId = order_id
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    func didSuccessGetNewsFeedLog(newsFeed: Int) {
        if newsFeed == 0 {
            viewNews.alpha = 0
        }else{
            viewNews.alpha = 1
            newsLabel.text = "\(1)"
        }
    }
    func didFailGetNewsFeedLog(title: String, subtitle: String) {
        viewNews.alpha = 0
    }
    
    func showCurrentOrder() {
        if !showHistorial {
            pedidos = DatabaseFunctions.shared.getPedidos()
            if pedidos.count > 0 {
                if pedidos.count > 2 {
                    labelPedidos.text = "\(pedidos[0].count + pedidos[1].count + pedidos[2].count)"
                    totalOrdersLabel.text = "\(pedidos[0].count + pedidos[1].count + pedidos[2].count)"
                }else if pedidos.count > 1{
                    labelPedidos.text = "\(pedidos[0].count + pedidos[1].count)"
                    totalOrdersLabel.text = "\(pedidos[0].count + pedidos[1].count)"
                }else{
                    labelPedidos.text = "\(pedidos[0].count)"
                    totalOrdersLabel.text = "\(pedidos[0].count)"
                }
                openFlag.removeAll()
                dinamicHeightMinimizada.removeAll()
                for i in 0..<pedidos.count {
                    var flags = [Bool]()
                    var heightMini = [CGFloat]()
                    for j in 0..<pedidos[i].count {
                        flags.append(false)
                        heightMini.append(0)
                    }
                    openFlag.append(flags)
                    dinamicHeightMinimizada.append(heightMini)
                }
                
                tableView.alpha = 0
                orderTableView.alpha = 1
                numPedido.alpha = 1
               
                viewPedidos.alpha = pedidos.count > 0 ? 1:0
                
                //                viewEstablecimiento.alpha = 1
                //                CURRENT ORDER
                btnActual.setTitleColor(UIColor.white, for: .normal)
                btnActual.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
                viewPedidos.backgroundColor = UIColor.white
                labelPedidos.textColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
                historial.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
                historial.backgroundColor = UIColor.white
                //            NotificationCenter.default.addObserver(self, selector: #selector(OrderDetailViewController.updateStatus(_:)), name: NSNotification.Name(rawValue: "updateStatus"), object: nil)
                //            print(pedidos)
                getInactiveItems()
                getInactiveExtras()
                getInactiveModifiers()
                self.orderTableView.reloadData()
                if readyDesplegar {
                    //                showPedidoDesplegado(order_id: orderId)
                    showPedido = true
                }
                if desplegarCaducado {
                    showPedidoCaducar()
                }
                
            }else{
                showPedido = false
                orderTableView.alpha = 0
                viewPedidos.alpha = 0
                numPedido.alpha = 0
                //            viewEstablecimiento.alpha = 0
                tableView.alpha = 1
                tableView.reloadData()
                btnActual.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
                btnActual.backgroundColor = UIColor.white
                viewPedidos.backgroundColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
                labelPedidos.textColor = UIColor.white
                historial.setTitleColor(UIColor.white, for: .normal)
                historial.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
                LoadingOverlay.shared.showOverlay(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self.userWS.viewmyOrders(client_id: self.client_id)
                }
                
                
            }
        }else{
            showPedido = false
            orderTableView.alpha = 0
            viewPedidos.alpha = 0
            numPedido.alpha = 0
            tableView.alpha = 1
            tableView.reloadData()
            btnActual.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
            btnActual.backgroundColor = UIColor.white
            viewPedidos.backgroundColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
            labelPedidos.textColor = UIColor.white
            historial.setTitleColor(UIColor.white, for: .normal)
            historial.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
            LoadingOverlay.shared.showOverlay(view: self.view)
            userWS.viewmyOrders(client_id: client_id)
        }
        
        
    }
    func setArray()  {
        arrays.append("header")
        arrays.append("cantPrice")
        for i in 0..<order_elements.count {
            arrays.append("itemPedidos")
        }
        arrays.append("noContent")
        arrays.append("subtotal")
        arrays.append("pickpalService")
        arrays.append("total")
        arrays.append("empty")
        arrays.append("estimated")
        arrays.append("status")
    }
    
    @IBAction func newsFeed(_ sender: Any) {
        if isFromGuide || isFromPromos || whereFrom == Constants.PICKPAL_PUNTOS {
            UserDefaults.standard.set("menu", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
        
        /*if !isFromPromos {
         let storyboard = UIStoryboard(name: "Home", bundle: nil)
         let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
         self.navigationController?.pushViewController(newVC, animated: true)
         }else{
         UserDefaults.standard.set("menu", forKey: "place")
         let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
         let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
         self.navigationController?.pushViewController(newVC, animated: true)
         }*/
    }
    
    func getInactiveItems() {
        
        pedidos = DatabaseFunctions.shared.getPedidos()
        //        let idItems = getItemsId()
        referenceItems = Database.database().reference().child("inactive_items")
        print(referenceItems)
        handleItems = referenceItems.observe(.value) { (snapshot) in
            for (ind, pedido) in self.pedidos.enumerated() {
                var ids = [Int]()
                let idItems = self.getItemsId().0
                let idCombos = self.getItemsId().1
                for (index, pe) in pedido.enumerated() {
                    ids.append(pe.order_id)
                    if !(snapshot.value is NSNull) {
                        
                        var array = [Int]()
                        var arrayC = [Bool]()
                        
                        //                        print(snapshot.value as! NSDictionary)
                        if let dictionary = snapshot.value as? NSDictionary {
                            if let establishment = (snapshot.value as! NSDictionary).value(forKey: "-" + pe.establishment_id.description) as? NSDictionary {
                                
                                if let est = (establishment.value(forKey: "items") as? NSDictionary) {
                                    let values = est.allKeys as? NSArray
                                    //                            for number in (snapshot.value as! NSDictionary).allKeys as NSArray {
                                    print(values as? NSArray)
                                    for number in values as! NSArray {
                                        if let num = number as? String {
                                            array.append(Int(num)!)
                                            if let comb = est.value(forKey: "\(number)") as? NSDictionary {
                                                if let isCombo = comb.value(forKey: "is_combo") as? Bool {
                                                    arrayC.append(isCombo)
                                                }
                                            }
                                        }
                                        
                                    }
                                    print(array)
                                    print(arrayC)
                                }
                                
                                if let est = (establishment.value(forKey: "items") as? NSDictionary) {
                                    let values = est.allKeys as? NSArray
                                    if  idItems.count > 0 {
                                        if idItems[ind].count > 0 {
                                            for i in 0..<idItems[ind][index].count {
                                                if array.contains(idItems[ind][index][i]) && idCombos[ind][index][i] {
                                                    if pe.statusItem == "to_pay" {
                                                        self.ws.changeOrderStatus(order_id: ids[index], status: 8)
                                                        let cancel = PedidoCanceladoViewController(nibName: "PedidoCanceladoViewController", bundle: nil)
                                                        cancel.modalTransitionStyle = .coverVertical
                                                        cancel.modalPresentationStyle = .overCurrentContext
                                                        cancel.textLabel = "\(pe.establishment_name!) ha agotado algunos platillos y/o bebidas"
                                                        self.present(cancel, animated: true, completion: nil)
                                                        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                                                            //                            print(nofitications)
                                                            let orderid = ids[index]
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                                                        })
                                                        
                                                    }else if pe.statusController == "new" {
                                                    
                                                    }
                                                    print("inactivate")
                                                }else if array.contains(idItems[ind][index][i]){
                                                    if pe.statusItem == "to_pay" {
                                                        self.ws.changeOrderStatus(order_id: ids[index], status: 8)
                                                        let cancel = PedidoCanceladoViewController(nibName: "PedidoCanceladoViewController", bundle: nil)
                                                        cancel.modalTransitionStyle = .coverVertical
                                                        cancel.modalPresentationStyle = .overCurrentContext
                                                        cancel.textLabel = "\(pe.establishment_name!) ha agotado algunos platillos y/o bebidas"
                                                        self.present(cancel, animated: true, completion: nil)
                                                        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                                                            //                            print(nofitications)
                                                            let orderid = ids[index]
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                                                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                                                        })
                                                        
                                                    }else if pe.statusController == "new" {
                                                       
                                                    }
                                                }
                                            }
                                            
                                        }
                                        
                                    }
                                }
                                
                            }
                        }
                        
                    }
                }
            }
        }
        
    }
    
    func getInactiveExtras() {
        pedidos = DatabaseFunctions.shared.getPedidos()
        referenceExtras = Database.database().reference().child("inactive_extras")
        handleExtras = referenceExtras.observe(.value) { (snapshot) in
            for (ind, pedido) in self.pedidos.enumerated() {
                var ids = [Int]()
                let idItems = self.getExtrasId()
                for (index, pe) in pedido.enumerated() {
                    ids.append(pe.order_id)
                    if !(snapshot.value is NSNull) {
                        var array = [Int]()
                        if let dictionary = snapshot.value as? NSDictionary {
                            if let establishment = (snapshot.value as! NSDictionary).value(forKey: "-"+pe.establishment_id.description) as? NSDictionary {
                                print(establishment)
                                print(establishment.allKeys as? NSArray)
                                let values = establishment.allKeys as! NSArray
                                for number in values  {
                                    if let num = number as? String {
                                        array.append(Int(num)!)
                                    }
                                }
                                print(array)
                                if  idItems.count > 0 {
                                    if idItems[ind].count > 0 {
                                        for i in 0..<idItems[ind][index].count {
                                            if array.contains(idItems[ind][index][i]) {
                                                if pe.statusItem == "to_pay" {
                                                    self.ws.changeOrderStatus(order_id: ids[index], status: 8)
                                                    let cancel = PedidoCanceladoViewController(nibName: "PedidoCanceladoViewController", bundle: nil)
                                                    cancel.modalTransitionStyle = .coverVertical
                                                    cancel.modalPresentationStyle = .overCurrentContext
                                                    cancel.textLabel = "\(pe.establishment_name!) ha agotado algunos platillos y/o bebidas"
                                                    self.present(cancel, animated: true, completion: nil)
                                                    UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                                                        let orderid = ids[index]
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                                                    })
                                                    
                                                }
                                                print("inactivate")
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                                
                            }
                        }
                        
                    }
                }
            }
        }
        
    }
    
    func getInactiveModifiers() {
        pedidos = DatabaseFunctions.shared.getPedidos()
        referenceModifiers = Database.database().reference().child("inactive_modifiers")
        print(referenceModifiers)
        handleModifiers = referenceModifiers.observe(.value) { (snapshot) in
            for (ind, pedido) in self.pedidos.enumerated() {
                var ids = [Int]()
                let idItems = self.getExtrasId()
                for (index, pe) in pedido.enumerated() {
                    ids.append(pe.order_id)
                    if !(snapshot.value is NSNull) {
                        var array = [Int]()
                        if let dictionary = snapshot.value as? NSDictionary {
                            if let establishment = (snapshot.value as! NSDictionary).value(forKey: "-"+pe.establishment_id.description) as? NSDictionary {
                                print(establishment)
                                print(establishment.allKeys as? NSArray)
                                let values = establishment.allKeys as! NSArray
                                for number in values  {
                                    if let num = number as? String {
                                        array.append(Int(num)!)
                                    }
                                }
                                print(array)
                                if  idItems.count > 0 {
                                    if idItems[ind].count > 0 {
                                        for i in 0..<idItems[ind][index].count {
                                            if array.contains(-idItems[ind][index][i]) {
                                                if pe.statusItem == "to_pay" {
                                                    self.ws.changeOrderStatus(order_id: ids[index], status: 8)
                                                    let cancel = PedidoCanceladoViewController(nibName: "PedidoCanceladoViewController", bundle: nil)
                                                    cancel.modalTransitionStyle = .coverVertical
                                                    cancel.modalPresentationStyle = .overCurrentContext
                                                    cancel.textLabel = "\(pe.establishment_name!) ha agotado algunos platillos y/o bebidas"
                                                    self.present(cancel, animated: true, completion: nil)
                                                    UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                                                        let orderid = ids[index]
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                                                        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                                                    })
                                                    
                                                }
                                                print("inactivate")
                                            }
                                        }
                                        
                                    }
                                    
                                }
                                
                                
                            }
                        }
                        
                    }
                }
            }
        }
        
    }
    func getItemsId() -> ([[[Int]]], [[[Bool]]])  {
        var itmSection = [[[Int]]]()
        var itmSectionCombo = [[[Bool]]]()
        for pedido in pedidos {
            var itm = [[Int]]()
            var combo = [[Bool]]()
            for pe in pedido {
                if let order_elements = pe.order_elements {
                    var itm2 = [Int]()
                    var combo2 = [Bool]()
                    for o_e in order_elements {
                        if let integer = (o_e as! NSDictionary).value(forKey: "id") as? String {
                            itm2.append(Int(integer)!)
                            if let isCombo = (o_e as! NSDictionary).value(forKey: "is_package") as? Bool {
                                combo2.append(isCombo)
                            }else{
                                combo2.append(false)
                            }
                        }
                    }
                    itm.append(itm2)
                    combo.append(combo2)
                }
                
            }
            itmSection.append(itm)
            itmSectionCombo.append(combo)
        }
        return (itmSection, itmSectionCombo)
    }
    func getExtrasId() -> ([[[Int]]])  {
        var itmSection = [[[Int]]]()
        for pedido in pedidos {
            var itm = [[Int]]()
            for pe in pedido {
                if let order_elements = pe.order_elements {
                    var itm2 = [Int]()
                    for o_e in order_elements {
                        if let modifiers = (o_e as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            for modifier in modifiers {
                                print(modifier)
                                if let mod_id = (modifier as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                                    for modificadorID in mod_id {
                                        itm2.append(modificadorID as! Int)
                                    }
                                }
                            }
                        }
                        
                        if let complements = (o_e as! NSDictionary).value(forKey: "complements") as? NSDictionary {
                            print(complements)
                            if let compl_id = (complements as! NSDictionary).value(forKey: "complement_id") as? NSArray {
                                for complementoId in compl_id {
                                    itm2.append(complementoId as! Int)
                                }
                            }
                            
                        }
                        print((o_e as! NSDictionary).value(forKey: "modifiers"))
                    }
                    itm.append(itm2)
                }
                
            }
            itmSection.append(itm)
        }
        return (itmSection)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if referenceItems != nil {
            referenceItems.removeObserver(withHandle: handleItems)
        }
        
    }
    
    func seeRefund(index: IndexPath, order_id: Int) {
        self.showPedidoDesplegado(order_id: order_id)
        print("desplegar reembolso")
    }
    func goToEstablishment(id: Int, name: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let nwVC = storyboard.instantiateViewController(withIdentifier: "customResults") as! CustomResultsViewController
        if let city = UserDefaults.standard.object(forKey: "municipalty") as? String {
            nwVC.valueCity = city
        }
        if let state = UserDefaults.standard.object(forKey: "state") as? String {
            nwVC.valueState = state
        }
        self.navigationController?.pushViewController(nwVC, animated: true)
        
    }
    func getStablishmentInfo() {
        
    }
    func gotoHome() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func cancelOrder(order_id: Int) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.ordersWS.changeOrderStatus(order_id:order_id, status: 8)
        DatabaseFunctions.shared.deleteDueOrder(order_id: order_id)
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)"])
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)+"])
        })
        userWS.viewmyOrders(client_id: client_id)
    }
    func calculateprice(prices: [Double], discount: Double){
        subtotal = 0.00
        total = 0.00
        for i in 0..<prices.count{
            subtotal += prices[i] * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double)
        }
        total = subtotal + comission
        total = total - discount
        orderTableView.reloadData()
    }
    func didSuccessChangeOrderStatus(){
        LoadingOverlay.shared.hideOverlayView()
        self.pedidos = DatabaseFunctions.shared.getPedidos()
        //        print(pedidos.count,  "change order status")
        if pedidos.count > 0 {
            self.orderTableView.reloadData()
        }
    }
    func didFailChangeOrderStatus(error: String, subtitle: String) {
        
    }
    @objc func deletePedido(_ notification: NSNotification) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
    }
    @objc func updateStatus(_ userInfo: NSNotification) {
        
        if let info = userInfo.userInfo as? NSDictionary{
            if (info.value(forKey: "order_ready") as? Bool) != nil {
                let readyView = OrdenListaViewController()
                readyView.modalTransitionStyle = .crossDissolve
                readyView.modalPresentationStyle = .overCurrentContext
                readyView.establishName = (UserDefaults.standard.object(forKey: "establishment_name") as? String)!
                readyView.orderId = (UserDefaults.standard.object(forKey: "order_id") as? Int)!
                readyView.delegate = self
                self.present(readyView, animated: true, completion: nil)
            }
            ordersWS.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
    }
    
    
    @objc func openDialogReady(userInfo: NSNotification) {
        
        
        print("Abrir alert: \(userInfo)")
        
       
    }
    
    
    func openDialogOrderReady(idOrder: Int,establishment_name: String ){
      
        
            let readyView = OrdenListaViewController()
            readyView.modalTransitionStyle = .crossDissolve
            readyView.modalPresentationStyle = .overCurrentContext
            readyView.establishName = establishment_name
            readyView.orderId = idOrder
            readyView.delegate = self
            self.present(readyView, animated: true, completion: nil)
        
        
    }
    
    func didSuccessGetLastOrder(orders: [MultipleOrders]) {
        
        numeroPedidos = orders.count
        
        if orders.count != 0 {
        ultimoPedido = orders[0].order_id
        }
        print("Ultimo pedido \(ultimoPedido)")
        viewPedidos.alpha = pedidos.count > 0 ? 1:0
        LoadingOverlay.shared.hideOverlayView()
       
        
//        labelPedidos.text = "\(pedidos.count)"
//        totalOrdersLabel.text = "\(pedidos.count)"
        labelPedidos.text = "\(orders.count)"
        totalOrdersLabel.text = "\(orders.count)"
        pedidos = DatabaseFunctions.shared.getPedidos()
        openFlag.removeAll()
        dinamicHeightMinimizada.removeAll()
        for i in 0..<pedidos.count {
            var flags = [Bool]()
            var heightMini = [CGFloat]()
            for j in 0..<pedidos[i].count {
                flags.append(false)
                heightMini.append(0)
            }
            openFlag.append(flags)
            dinamicHeightMinimizada.append(heightMini)
        }
        //        print(openFlag)
        var prepTime = [Int]()
        for (index, order) in orders.enumerated() {
            
            
            let reference = Database.database().reference().child("orders").child("-" + order.establishment_id!.description).child(order.register_date_key!).child("-" + order.order_id.description)
            //            print(reference)
            reference.observe(.value, with: { (snapshot) -> Void in
                if !(snapshot.value is NSNull){
                    let valueOrder = (snapshot.value as! NSDictionary)
                    if self.showPedido {
                        //                        self.showPedidoDesplegado(order_id: order.order_id)
                        print(self.currentOrderId, order.order_id)
                        if self.currentOrderId == order.order_id {
                            //                            self.tableView(self.orderTableView, didSelectRowAt:IndexPath(row: 0, section: 0))
                            self.showPedidoDesplegado(order_id: order.order_id)
                            self.currentOrderId = 0
                            self.showPedido = false
                            //                            print(self.currentOrderId, order.order_id)
                            //                            print("showPedido")
                        }
                    }
                    
                    let statusInt = valueOrder.value(forKey: "status_client") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusInt2 = valueOrder.value(forKey: "status") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusEnum = (Constants.status as! NSDictionary).value(forKey: "\(statusInt)") as! String
                    let statusCont = (Constants.statusControllers as! NSDictionary).value(forKey: "\(statusInt2)") as! String
                    //                Se recupera el detalle del pedido
                    var dictionaryInfoString = String()
                    let products = valueOrder.value(forKey: "products") as! NSArray//(snapshot.value as! NSDictionary).value(forKey: "products") as! NSArray
                    var dictionaryInfo = [Dictionary<String,Any>]()
                    var dictionaryModifiersTotal = [Dictionary<String,Any>]()
                    var dictionaryComplementsTotal = [Dictionary<String,Any>]()
                    var i = 0
                    var y = 0
                    var totalPrice = 0.00
                    for product in products {
                        print(product)
                        var modifiers = NSArray()
                        var modifiersS = String()
                        var modifiers_price = NSArray()
                        var modifiers_priceS = String()
                        var modifiers_id = NSArray()
                        var modifiers_idS = String()
                        var modifiers_cat = NSArray()
                        var modifiers_catS = String()
                        
                        var complements = NSArray()
                        var complementsS = String()
                        var complements_price = NSArray()
                        var complement_priceS = String()
                        var complements_id = NSArray()
                        var complements_idS = String()
                        var complements_cat = NSArray()
                        var complements_catS = String()
                        
                        if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            modifiers = mod
                        }else{
                            modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
                        }
                        
                        if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                            modifiers_price = mod_price
                        }else{
                            modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
                        }
                        
                        if let mod_id = (product as! NSDictionary).value(forKey: "modifier_ids") as? NSArray {
                            modifiers_id = mod_id
                        }else{
                            modifiers_idS = (product as! NSDictionary).value(forKey: "modifier_ids") as! String
                        }
                        
                        if let mod_cat = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                            modifiers_cat = mod_cat
                        }else{
                            modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
                        }
                        
                        if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                            complements = comp
                        }else{
                            complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
                        }
                        
                        if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                            complements_price = comp_price
                        }else{
                            complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
                        }
                        
                        if let comp_id = (product as! NSDictionary).value(forKey: "complement_ids") as? NSArray {
                            complements_id = comp_id
                        }else{
                            complements_idS = (product as! NSDictionary).value(forKey: "complement_ids") as! String
                        }
                        
                        if let comp_cat = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                            complements_cat = comp_cat
                        }else{
                            complements_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
                        }
                        
                        if modifiers.count > 0 || complements.count > 0 {
                            var modifP = [Double]()
                            var modifId = [Int]()
                            var modifCat = [String]()
                            var compP = [Double]()
                            var compN = [String]()
                            var compC = [Int]()
                            var compId = [Int]()
                            var compCat = [String]()
                            for j in 0..<modifiers.count {
                                modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                                modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
                                modifCat.append(modifiers_cat.object(at: j) as! String)
                            }
                            var aux = 1
                            var index = -1
                            for z in 0..<complements.count {
                                if !compN.contains(complements[z] as! String) {
                                    compN.append(complements[z] as! String)
                                    compC.append(1)
                                    aux = 1
                                    compP.append(Double(complements_price[z] as! String)!)
                                    compId.append(Int(complements_id[z] as! String) ?? 0)
                                    compCat.append(complements_cat[z] as! String)
                                    index += 1
                                }else{
                                    aux += 1
                                    compC[index] = aux
                                    compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                                    compId[index] = (Int(complements_id[z] as! String) ?? 0)
                                    compCat[index] = (complements_cat[z] as! String)
                                }
                            }
                            
                            let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_id": modifId, "modifier_category":modifCat] as [String : Any]
                            dictionaryModifiersTotal.append(dictionaryModifiers)
                            let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_id": compId, "complement_category":compCat] as [String : Any]
                            dictionaryComplementsTotal.append(dictionaryComplements)
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": true, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                            y += 1
                        }else{
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": false, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_id": complements_idS, "complement_category": complements_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_id": modifiers_idS, "modifier_category": modifiers_catS]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                        }
                        totalPrice += (product as! NSDictionary).value(forKey: "price") as! Double
                        i += 1
                    }
                    let qr = valueOrder.value(forKey: "qr") as! String
                    var prepTimeInt = valueOrder.value(forKey: "full_preparation_time") as! Int
                    if statusEnum == "delayed" {
                        prepTimeInt = (valueOrder.value(forKey: "full_preparation_time") as! Int + 5)
                        //                        prepTimeInt = (valueOrder.value(forKey: "full_preparation_time") as! Int + 2)
                        let currentDate = Date()
                        let estimated = (valueOrder.value(forKey: "register_date") as! String) + " " + (valueOrder.value(forKey: "register_hour") as! String)
                        //        print(estimated)
                        let form = DateFormatter()
                        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let datePay = form.date(from: estimated)
                        let calendar = Calendar.current
                        var date = calendar.date(byAdding:.minute, value: prepTimeInt, to: datePay!)!
                        
                        while currentDate > date {
                            //                            print("p")
                            prepTimeInt += (valueOrder.value(forKey: "full_preparation_time") as! Int + 5)
                            date = calendar.date(byAdding:.minute, value: prepTimeInt, to: datePay!)!
                        }
                    }
                    if statusCont == "delivery_delayed" {
                        prepTimeInt = (valueOrder.value(forKey: "full_preparation_time") as! Int + 2)
                        let currentDate = Date()
                        let estimated = (valueOrder.value(forKey: "register_date") as! String) + " " + (valueOrder.value(forKey: "register_hour") as! String)
                        let form = DateFormatter()
                        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let datePay = form.date(from: estimated)
                        let calendar = Calendar.current
                        var date = calendar.date(byAdding:.minute, value: prepTimeInt, to: datePay!)!
                        //                        while currentDate > date {
                        //                            prepTimeInt += (valueOrder.value(forKey: "full_preparation_time") as! Int + 5)
                        //                            date = calendar.date(byAdding:.minute, value: prepTimeInt, to: datePay!)!
                        //                        }
                    }
                    //                    if prepTime.contains(prepTimeInt) {
                    prepTime.append(prepTimeInt)
                    //                    }
                    
                    //                    print(prepTime)
                    var last4 = String()
                    if let lastDig = valueOrder.value(forKey: "last4") as? String {
                        last4 = lastDig
                    }else{
                        last4 = ""
                    }
                    //                    if statusEnum == "refund" {
                    //                        let refund = Date()
                    //                        let format = DateFormatter()
                    //                        format.dateFormat = "HH:mm:ss"
                    //                        let myDate = format.string(from: refund)
                    ////                        print(myDate)
                    //
                    //                        DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: prepTimeInt, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice + (valueOrder.value(forKey: "pickpal_service") as! Double), order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusEnum, qr: qr, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4)
                    //                    }else{
                    var flagReady = Bool()
                    if !DatabaseFunctions.shared.getReadyFlag(order_id: order.order_id) {
                        flagReady = false
                    }else{
                        flagReady = true
                    }
                    
                    
                    
                    
                    /* DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: prepTimeInt, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice + (valueOrder.value(forKey: "pickpal_service") as! Double), order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusEnum, qr: qr, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusCont, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag: flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address ?? "", shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: (valueOrder.value(forKey: "time_delivery") as! NSNumber).stringValue , to_refund: order.to_refund!)*/
                    //                    }
                    
                    
                    let sendNotifications =   DatabaseFunctions.shared.getStatusNotifications(idOrder: order.order_id)
                    print("Sea actulizo firebase: \(order.order_id)")
                    
                    
                    if valueOrder.value(forKey: "confirmed")as? Bool ?? false{
                    self.defaults.setValue(order.establishment_name, forKey: "establishment_name")
                    self.defaults.setValue(order.establishment_id, forKey: "establishment_id")
                    self.defaults.setValue(order.order_id, forKey: "order_id")
                    
                    
                        
                    }
                    
                   
                    DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: prepTimeInt, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice + (valueOrder.value(forKey: "pickpal_service") as! Double), order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: order.register_date, process_time: valueOrder.value(forKey: "process_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusEnum, qr: qr, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusCont, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag    : flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address ?? "", shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery:String( order.time_delivery) , to_refund: order.to_refund!, days_elapsed: order.days_elapsed,allow_process_nopayment: order.allow_process_nopayment, est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? "" , est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: order.is_generic ,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "", sendNotifications: sendNotifications, notified: valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: valueOrder.value(forKey: "pickpal_cash_money") as? Double ?? 0.0, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: order.two_devices ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false, total_card: order.total_card, total_cash: order.total_cash, total_to_pay: order.total_to_pay, total_done_payment: order.total_done_payment,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                    
        
                    
                    self.pedidos = DatabaseFunctions.shared.getPedidos()
                    //                    print(self.pedidos.count, "pedidos dentro del success last order")
                    self.openFlag.removeAll()
                    self.dinamicHeightMinimizada.removeAll()
                    for i in 0..<self.pedidos.count {
                        var flags = [Bool]()
                        var heightMini = [CGFloat]()
                        for _ in 0..<self.pedidos[i].count {
                            flags.append(false)
                            heightMini.append(0)
                        }
                        self.openFlag.append(flags)
                        self.dinamicHeightMinimizada.append(heightMini)
                    }
                    //                    print("statusEnum", statusEnum)
                    self.orderTableView.scrollsToTop = true
                    if self.pedidos.count > 0 {
                        self.orderTableView.reloadData()
                    }
                    //                    print(self.pedidos)
                    if statusEnum == "delivered" || statusEnum == "expired" || statusEnum == "canceled" || statusEnum == "refund"{
                        DatabaseFunctions.shared.deleteItem()
                        DatabaseFunctions.shared.deleteDeliveredOrders()
                        DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                        self.viewPedidos.alpha = 0
                        self.numPedido.alpha = 0
                        
                        self.showCurrentOrder()
                        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                            //                            print(nofitications)
                            let orderid = order.order_id!
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                        })
                        
                        if statusEnum == "delivered" && self.pedidos.count > 0 {
                            self.ordersWS.getRewardsWins(order_id: order.order_id!)
//                            self.userWS.viewmyOrders(client_id: self.client_id)
                            print("Hola ya se entrego")
                        }else{
                            
                            if statusEnum == "delivered" && self.pedidoEntregado{
//                                self.userWS.viewmyOrders(client_id: self.client_id)
                                self.ordersWS.getRewardsWins(order_id: order.order_id!)
                                self.pedidoEntregado = false
                            }
                            
                        }
//                        self.userWS.viewmyOrders(client_id: self.client_id)
                    }
                    if let isRefunded = valueOrder.value(forKey: "is_refunded") as? Bool {
                        if isRefunded {
                            DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                            //                            self.pedidos = DatabaseFunctions.shared.getPedidos()
                            self.showCurrentOrder()
                        }
                    }
                    if statusEnum != "to_pay" {
                        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                            let orderid = order.order_id!
                            print(nofitications, orderid)
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)"])
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)+"])
                            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(orderid)-"])
                        })
                    }
                    if statusEnum == "to_pay" && self.activateNotifications {
                        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                            let orderid = order.order_id!
                            //                            print(nofitications, orderid)
                            self.notifications = nofitications.count
                            print(self.notifications, "numero de notificaciones")
                        })
                    }
                }
            })
        }
        
        tableView.reloadData()
    }
    
    
    //Falta regresar el establecimiento
    func didSuccessGetWinsOrder(wins: WindsRewards) {
        
        if wins.points ?? 0 > 0{
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "Tu compra en \(wins.establishment_name ?? "") generó  \(CustomsFuncs.numberFormat(value: wins.points ?? 0)) pts. /\(CustomsFuncs.getFomatMoney(currentMoney: Double(wins.money_points))) fueron abonados exitosamente "
            newXIB.mTitle = UserDefaults.standard.object(forKey: "first_name") as? String ?? ""
            newXIB.mToHome = false
            newXIB.mTitleButton = "Listo"
            newXIB.mId = 0
            newXIB.valueCall = 0
            newXIB.isFromAdd = false
            newXIB.delegate = self
            newXIB.isShowPoints = true
            self.present(newXIB, animated: true, completion: nil)
        }
    }
    
    func didFailGetWinsOrder(error: String, subtitle: String) {
        
    }
    
    func deletePromoById(id: Int) {
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        
    }
    
    /*deletePromoById(id: Int)
     @objc optional func closeDialogAndExit*/
    
    func showPedidoCaducar() {
        for i in 0..<pedidos.count {
            for j in 0..<pedidos[i].count {
                //                print(pedidos[i][j].statusItem, order_id)
                //                print(i,j)
                //                print(orderTableView.cellForRow(at: IndexPath(row: j, section: i)))
                if pedidos[i][j].statusItem == "to_pay" {
                    self.orderTableView.scrollsToTop = true
                    self.tableView(self.orderTableView, didSelectRowAt: IndexPath(row: j, section: i))
                    print("show pedido caducar")
                }
            }
        }
    }
    func showPedidoDesplegado(order_id: Int)  {
        pedidos = DatabaseFunctions.shared.getPedidos()
        for i in 0..<pedidos.count {
            for j in 0..<pedidos[i].count {
                if pedidos[i][j].order_id! == order_id {
                    print(orderTableView.cellForRow(at: IndexPath(row: j, section: i)),"cell")
                    if orderTableView.cellForRow(at: IndexPath(row: j, section: i)) == nil {
                        orderTableView.reloadData()
                    }
                    self.tableView.scrollsToTop = true
                    self.tableView(self.orderTableView, didSelectRowAt: IndexPath(row: j, section: i))
                    print("showpedido desplegado", IndexPath(row: j, section: i) )
                }
            }
        }
    }
    
    func getOrderFromMyOrders(products: NSArray) -> [Dictionary<String,Any>]{
        //        print(products)
        var dictionaryInfo = [Dictionary<String,Any>]()
        var dictionaryModifiersTotal = [Dictionary<String,Any>]()
        var dictionaryComplementsTotal = [Dictionary<String,Any>]()
        var i = 0
        var y = 0
        for product in products {
            //                        print(product)
            var modifiers = NSArray()
            var modifiersS = String()
            var modifiers_price = NSArray()
            var modifiers_priceS = String()
            var modifiers_cat = NSArray()
            var modifiers_catS = String()
            
            var complements = NSArray()
            var complementsS = String()
            var complements_price = NSArray()
            var complement_priceS = String()
            var complement_cat = NSArray()
            var complement_catS = String()
            
            if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                modifiers = mod
            }else{
                modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
            }
            
            if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                modifiers_price = mod_price
            }else{
                modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
            }
            
            if let mod_category = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                modifiers_cat = mod_category
            }else{
                modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
            }
            
            if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                complements = comp
            }else{
                complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
            }
            
            if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                complements_price = comp_price
            }else{
                complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
            }
            
            if let comp_category = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                complement_cat = comp_category
            }else{
                complement_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
            }
            
            if modifiers.count > 0 || complements.count > 0 {
                var modifP = [Double]()
                var modifCa = [String]()
                var compP = [Double]()
                var compN = [String]()
                var compC = [Int]()
                var compCa = [String]()
                for j in 0..<modifiers.count {
                    modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                    modifCa.append(modifiers_cat.object(at: j) as! String)
                }
                var aux = 1
                var index = -1
                for z in 0..<complements.count {
                    if !compN.contains(complements[z] as! String) {
                        compN.append(complements[z] as! String)
                        compC.append(1)
                        aux = 1
                        compP.append(Double(complements_price[z] as! String)!)
                        if complement_cat.count > 0{
                            compCa.append(complement_cat[z] as! String)
                        }else{
                            compCa.append("")
                        }
                        index += 1
                    }else{
                        aux += 1
                        compC[index] = aux
                        compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                        if complement_cat.count > 0{
                            compCa[index] = complement_cat[z] as! String
                        }else{
                            compCa.append("")
                        }
                    }
                }
                
                let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_category": modifCa] as [String : Any]
                dictionaryModifiersTotal.append(dictionaryModifiers)
                let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_category":compCa] as [String : Any]
                dictionaryComplementsTotal.append(dictionaryComplements)
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "menu_item_name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! String, "complex": true, "id":(product as! NSDictionary).value(forKey: "reference_id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                dictionaryInfo.append(dictionary)
                y += 1
            }else{
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "menu_item_name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! String, "complex": false,  "id":(product as! NSDictionary).value(forKey: "reference_id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_category":complement_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_category": modifiers_catS]] as [String : Any]
                dictionaryInfo.append(dictionary)
            }
            i += 1
        }
        return dictionaryInfo
    }
    //    MARK: this
    func makeOrdersObject (dictionary:  [Dictionary<String, Any>]) {
        var tempNameArray = [String]()
        var tempCantArray = [Int]()
        var tempPriceArray = [Double]()
        var tempModifier = [Bool]()
        for i in 0..<dictionary.count {
            var priceModifiers = Double()
            var priceComplements = Double()
            if let hasModifiers = (dictionary[i] as NSDictionary).value(forKey: "complex") as? Bool {
                if hasModifiers {
                    if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                        tempNameArray.append(name)
                        tempPriceArray.append(price)
                        tempCantArray.append(quantity)
                        tempModifier.append(false)
                        tempNameArray.append(name)
                        tempPriceArray.append(Double(i))
                        tempCantArray.append(quantity)
                        tempModifier.append(true)
                    }
                }else{
                    if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                        tempNameArray.append(name)
                        tempPriceArray.append(price)
                        tempCantArray.append(quantity)
                        tempModifier.append(false)
                    }
                }
                
            }else{
                if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                    tempNameArray.append(name)
                    tempPriceArray.append(price)
                    tempCantArray.append(quantity)
                    tempModifier.append(false)
                }
            }
            
            if let complements = ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as? [String]{
                //                        if let comp = complements.value(forKey: "complement_price") as? [Double] {
                tempPriceArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]//complements.value(forKey: "complement_price") as! [Double]
                tempCantArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_cant") as! [Int]//complements.value(forKey: "complement_cant") as! [Int]
                tempNameArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as! [String]//complements.value(forKey: "complement_name") as! [String]
                for i in 0..<(((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).count {
                    tempModifier.append(true)
                }
                priceComplements = (((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).reduce(0, +)
                //                        }
            }
            if let modifiers = (dictionary[i] as NSDictionary).value(forKey: "modifiers") as? NSArray {
                for modi in modifiers{
                    tempNameArray += (modi as! NSDictionary).value(forKey: "modifier_name") as! [String]
                    tempPriceArray += (modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]
                    for i in 0..<((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).count {
                        tempCantArray.append(1)
                        tempModifier.append(true)
                    }
                    priceModifiers = ((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).reduce(0, +)
                }
            }
            if let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? Double {
                for prices in tempPriceArray {
                    let indice = tempPriceArray.index(of: Double(i))
                    if indice != nil {
                        tempPriceArray[indice!] = price - priceModifiers - priceComplements
                    }
                }
            }
            
        }
        //                print(order_ele)
        //        cell.modifier_name = tempNameArray
        //        cell.modifier_cant =  tempCantArray
        //        cell.modifier_price = tempPriceArray
        //        cell.isModifier = tempModifier
    }
    
    func showOrder(orderId: Int) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.currentOrderId = orderId
        newVC.pedidos = DatabaseFunctions.shared.getPedidos()
        newVC.readyDesplegar = true
        newVC.showPedido = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func didFailGetLastOrder(error: String, subtitle: String) {
        print(error, "didFailGetLastOrder")
        //        print(tableView.alpha)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = isFromPromos
                newVC.isFromGuide = self.isFromGuide
                newVC.whereFrom = self.whereFrom
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = isFromPromos
            newVC.isFromGuide = self.isFromGuide
            newVC.whereFrom = self.whereFrom
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    @IBAction func home(_ sender: Any) {
        if whereFrom == Constants.PICKPAL_PUNTOS{
            let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            if !isFromPromos {
                if isFromGuide{
                    let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
                    let navController = UINavigationController(rootViewController: newVC)
                    navController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    if self.whereFrom == Constants.PICKPAL_WALLET {
                        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
                        self.navigationController?.pushViewController(newVC, animated: true)

                    }else{
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                }
                
            }else{
                
                    UserDefaults.standard.set("promos", forKey: "place")
                    let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
                    let navController = UINavigationController(rootViewController: newVC)
                    navController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.pushViewController(newVC, animated: true)
                
            }
        }
    }
    
    func downloadFact(order_id: Int) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        userWS.getUrlInvoice(order_id: order_id)
        
    }
    
    func push(order_id: Int, stablishment_id: Int, comercioTxt: String, myorder: Orders, total: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleHelp") as! SingleEstablishmentHelpViewController
        newVC.myorder = myorder
        newVC.nombreEstablecimiento = comercioTxt
        newVC.category = myorder.category
        newVC.hourEntregado = myorder.fecha
        newVC.totalPedido = total
        newVC.imagenEstablecimiento = myorder.img
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func historial(_ sender: Any) {
        //                MARK: Nuevo diseño
        if self.myorders.count > 0 {
            tableContainer.alpha = 1
            orderTableView.alpha = 0
            tableView.alpha = 1
            noCurrent.alpha = 0
        }else{
            tableContainer.alpha = 0
            noCurrent.alpha = 1
        }
        btnActual.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
        btnActual.backgroundColor = UIColor.white
        viewPedidos.backgroundColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
        labelPedidos.textColor = UIColor.white
        historial.setTitleColor(UIColor.white, for: .normal)
        historial.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
    }
    @IBAction func Actual(_ sender: Any) {
        if pedidos.count > 0 {
            btnActual.setTitleColor(UIColor.white, for: .normal)
            btnActual.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
            viewPedidos.backgroundColor = UIColor.white
            labelPedidos.textColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
            historial.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
            historial.backgroundColor = UIColor.white
            noCurrent.alpha = 0
            tableContainer.alpha = 1
            orderTableView.alpha = 1
            tableView.alpha = 0
            orderTableView.reloadData()
        }else{
            btnActual.setTitleColor(UIColor.white, for: .normal)
            btnActual.backgroundColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
            viewPedidos.backgroundColor = UIColor.white
            labelPedidos.textColor = UIColor.init(red: 38/255, green: 210/255, blue: 115/255, alpha: 1)
            historial.setTitleColor(UIColor.init(red: 53/255, green: 53/255, blue: 53/255, alpha: 1), for: .normal)
            historial.backgroundColor = UIColor.white
            noCurrent.alpha = 1
            tableContainer.alpha = 0
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func desplegarAction(indexPath: IndexPath) {
        tableView(orderTableView, didSelectRowAt: indexPath)
        print("desplegarAction")
    }
    func desplegar(indexPath: IndexPath) {
        tableView(orderTableView, didSelectRowAt: indexPath)
        print("deslegar ")
    }
    func didSuccessGetUrlInvoice(url: String, message: String) {
        LoadingOverlay.shared.hideOverlayView()
        if url != ""{
            guard let url = URL(string: url) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }else{
            
            let alert = UIAlertController(title: "PickPal", message: "Tu factura aún no está disponible", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func didFailGetUrlInvoice(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func didSuccessGetPendingOrder(id: Int) {
        LoadingOverlay.shared.hideOverlayView()
        order_id = id
    }
    func didFailGetPendingOrder(error: String, statusCode: Int, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        self.statusCode = statusCode
        if statusCode == 204{
            
        }else{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    func didSuccessMyOrders(orders: [Orders]) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        self.orders = orders
        myorders = orders
        if !showHistorial {
            if pedidos.count > 0 {
                tableContainer.alpha = 1
                noCurrent.alpha = 0
                tableView.alpha = 0
            }else{
                if orders.count > 0 {
                    noCurrent.alpha = 0
                    tableContainer.alpha = 1
                    tableView.alpha = 1
                    myorders = orders
                    print("Ultima Orden: \(myorders[0].order_id)")
                }else{
                    noCurrent.alpha = 1
                    tableContainer.alpha = 0
                    tableView.alpha = 1
                }
            }
            for _ in 0..<myorders.count {
                allItemsOrder.append([])
            }
            
            tableView.reloadData()
        }else{
            if orders.count > 0 {
                noCurrent.alpha = 0
                tableContainer.alpha = 1
                tableView.alpha = 1
                myorders = orders
            }else{
                noCurrent.alpha = 1
                tableContainer.alpha = 0
                tableView.alpha = 1
            }
            
            for _ in 0..<myorders.count {
                allItemsOrder.append([])
            }
            
            tableView.reloadData()
        }
        
    }
    
    func didFailMyOrders(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        if title != "204"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }else{
            if pedidos.count == 0 {
                noCurrent.alpha = 1
                tableContainer.alpha = 0
            }
        }
    }
    
    func updateDataFromFirebase(establishment_id: Int, register_date: String, order_id: Int, isCash: Bool) {
        let reference = Database.database().reference().child("orders").child(establishment_id.description).child(register_date).child(order_id.description)
        reference.observe(.value, with: { (snapshot) -> Void in
            let valueInfo = snapshot.value as! NSDictionary
            let statusInt = valueInfo.value(forKey: "status_client") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
            let statusEnum = (Constants.status as NSDictionary).value(forKey: "\(statusInt)") as! String
            DatabaseFunctions.shared.updateStuff(registerDate: register_date, orderId: order_id, statusItem: statusEnum, payTime: valueInfo.value(forKey: "register_hour") as! String, processTime: valueInfo.value(forKey: "register_hour") as! String, readyTime: valueInfo.value(forKey: "ready_hour") as! String, isCash: isCash)
        })
    }
}
extension PedidosAnterioresViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == orderTableView {
            //            print(pedidos.count, "pedidos en el number of section")
            return pedidos.count
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return myorders.count
        }else{
            //            print(pedidos[section].count, "pedidos en el number of rows")
            if pedidos.count > 0 {
                return pedidos[section].count
            }else{
                return 0
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        print(tableView)
        if tableView == self.tableView {
            //            print("tableView pedidos anteriores")
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemPedidos") as! ItemPedidoTableViewCell
            cell.order = myorders[indexPath.row]
            cell.fecha.text = myorders[indexPath.row].fecha
            cell.referencia.text = myorders[indexPath.row].referencia
            cell.comercio.text = myorders[indexPath.row].comercio
            cell.payment_method.text = myorders[indexPath.row].payment_method
            
            //cell.puntosLabel.text = myorders[indexPath.row].
            //            cell.numFolio.text = myorders[indexPath.row].daily_folio
            //SM - servicio a Mesa, RB - Recoger en Barra, SD - Servicio a Domicilio, RS - Recoger en Sucursal
            
            cell.lblAddressSendOrderValue.visibility = .gone
            cell.lblAddressSendOrder.visibility = .gone
            cell.shipping_cost.visibility = .gone
            cell.titleShippingCost.visibility = .gone
            cell.ctlDivisor.constant = 60
            cell.ctlBottomTotal.constant = 60
            cell.heightContainerPagos.constant = 35
            cell.heightContainerSubtotal.constant = 75
            
            var sizePagos = CGFloat(25)
            var sizeSubtotal = CGFloat(25)
            
            sizeTotales = sizePagos + sizeSubtotal
            
            //MOSTRAR Y OCULTAR PAGOS
            
            //MOSTAMOS EL SUBTOTAL
            
            
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US")
            formatter.numberStyle = .currency
            var subtotal = Double()
            for i in 0..<myorders[indexPath.row].itemsOrders.count {
                if myorders[indexPath.row].itemsOrders[i].total != nil {
                    subtotal += Double(myorders[indexPath.row].itemsOrders[i].total)!
                }
            }
            
            let money = Double(myorders[indexPath.row].moneyPoints) ?? 0
            let total = subtotal + Double(myorders[indexPath.row].pickpal_service)! + Double(myorders[indexPath.row].propina)! + myorders[indexPath.row].shippingCost ?? 0
            
            if let formattedTipAmount = formatter.string(from: total as NSNumber) {
                cell.total.text = "\(formattedTipAmount)"
                cell.totalString = "\(formattedTipAmount)"
            }
            
            let totalLessPoint = (total - money) - myorders[indexPath.row].pickpal_cash_money
        

            //MOSTRAMOS U OCULTAMOS LOS PUNTOS UTILIZADOS
            
            
            if  myorders[indexPath.row].points == 0 {
                
                cell.lblValuePuntos.visibility = .gone
                cell.titlePoints.visibility = .gone
            }else{
                cell.lblValuePuntos.text = "-\(CustomsFuncs.numberFormat(value:myorders[indexPath.row].points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(myorders[indexPath.row].moneyPoints) as! Double))"
                cell.lblValuePuntos.visibility = .visible
                cell.titlePoints.visibility = .visible
                cell.heightContainerPagos.constant += sizePagos
                sizeTotales += sizePagos
            }
            
            
            //MOSTRAMOS OCULTAMOS EL TOTAL DE CASH UTILIZADO
            if myorders[indexPath.row].pickpal_cash_money != nil {
                if myorders[indexPath.row].pickpal_cash_money > 0{
                    
                    cell.textoTotalPickpalCash.visibility = .visible
                    cell.totalPickPalCash.visibility = .visible
                    cell.heightContainerPagos.constant += sizePagos
                    sizeTotales += sizePagos
                    if totalLessPoint > 0 {
                        cell.viewDivPickpalCash.visibility = .visible
                            
                }else {
                        
                       
                    }
                    cell.totalPickPalCash.text = "\(CustomsFuncs.getFomatMoney(currentMoney:myorders[indexPath.row].pickpal_cash_money))"
                    
                }else{
                    
                    cell.textoTotalPickpalCash.visibility = .gone
                    cell.totalPickPalCash.visibility = .gone
                   
                }
            
            }else{
    
                cell.textoTotalPickpalCash.visibility = .gone
                cell.totalPickPalCash.visibility = .gone
                
            }
            
            //TOTAL PAGADO Y METODO DE PAGO
            cell.lblTipoPAgo.visibility = .visible
            cell.lblCantidadPagada.visibility = .visible
            cell.heightContainerPagos.constant += sizePagos
            sizeTotales += sizePagos
            switch myorders[indexPath.row].payment_method {
            case "Efectivo":
                cell.lblTipoPAgo.text =  myorders[indexPath.row].payment_method
                cell.lblCantidadPagada.text = "\(CustomsFuncs.getFomatMoney(currentMoney:myorders[indexPath.row].total_cash))"
                
            case "Tarjeta":
                cell.lblTipoPAgo.text =  myorders[indexPath.row].payment_method
                cell.lblCantidadPagada.text = "\(CustomsFuncs.getFomatMoney(currentMoney:myorders[indexPath.row].total_card))"
                
            default:
                cell.lblTipoPAgo.visibility = .gone
                cell.lblCantidadPagada.visibility = .gone
                cell.heightContainerPagos.constant -= sizePagos
                sizeTotales = sizeTotales - sizePagos
            }
            
           
            
            
            //TOTAL PAGOS
            
            cell.lblTotalPagado.text = CustomsFuncs.getFomatMoney(currentMoney: myorders[indexPath.row].total_to_pay)
            
            
            
            
            
        
            
            switch myorders[indexPath.row].type_order {
            case "SM": //SM - servicio a Mesa
                //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                cell.lblAddressSendOrderValue.visibility = .gone
                cell.lblAddressSendOrder.visibility = .gone
                cell.shipping_cost.visibility = .gone
                cell.titleShippingCost.visibility = .gone
                cell.propinaLabel.visibility = .visible
                cell.propinaValue.visibility = .visible
                cell.eat_type.text = "Servicio a mesa \(myorders[indexPath.row].number_table.description)"
                break
                
            case "RB"://RB - Recoger en Barra
                //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                cell.lblAddressSendOrderValue.visibility = .gone
                cell.lblAddressSendOrder.visibility = .gone
                cell.shipping_cost.visibility = .gone
                cell.titleShippingCost.visibility = .gone
                cell.propinaLabel.visibility = .visible
                cell.propinaValue.visibility = .visible
                cell.eat_type.text = "Pedido en Barra"
                break
                
            case "RS"://RS - Recoger en Sucursal
                //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                cell.eat_type.text = "Recoger en sucursal"
                cell.lblAddressSendOrderValue.visibility = .gone
                cell.lblAddressSendOrder.visibility = .gone
                cell.shipping_cost.visibility = .gone
                cell.titleShippingCost.visibility = .gone
                cell.propinaLabel.visibility = .gone
                cell.propinaValue.visibility = .gone
                break
            case "SD"://SD - Servicio a Domicilio
                print("/SD - Servicio a Domicilio ",myorders[indexPath.row].delivery_address )
                cell.lblAddressSendOrderValue.visibility = .visible
                cell.lblAddressSendOrder.visibility = .visible
                /*  cell.shipping_cost.isHidden = false
                 cell.titleShippingCost.isHidden = false*/
                cell.propinaLabel.visibility = .visible
                cell.propinaValue.visibility = .visible
                cell.ctlDivisor.constant = 109
                cell.ctlBottomTotal.constant = 109
                cell.eat_type.text = "Servicio a domicilio"
                cell.lblAddressSendOrderValue.text = myorders[indexPath.row].delivery_address
                
                cell.shipping_cost.text =  "$\(myorders[indexPath.row].shippingCost ?? 0.0)"
                
                
            default:
                print("Sin opciones")
                
            }
            cell.btnRefundDetails.visibility = .gone
            if myorders[indexPath.row].allow_points_movile{
                cell.lblAddedPoints.visibility = .visible
                cell.lblUsedPointsInfo.visibility = .visible
                cell.lblAddedPoints.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos abonados: ", title2: "\(CustomsFuncs.numberFormat(value: myorders[indexPath.row].new_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: myorders[indexPath.row].new_points_money ?? 0))", ziseFont: 11.0)
                cell.lblUsedPointsInfo.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos utilizados: ", title2: "-\(CustomsFuncs.numberFormat(value: myorders[indexPath.row].points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(myorders[indexPath.row].moneyPoints)!))", ziseFont: 11.0)
            }else{
                cell.lblAddedPoints.visibility = .gone
                cell.lblUsedPointsInfo.visibility = .gone
            }
            
            //  var puntos = Double(myorders[indexPath.row].moneyPoints!)
            
            
                    
            
            if myorders[indexPath.row].card != nil{
                cell.cardContainer.alpha = 1
                cell.card.text = "XXXX " + myorders[indexPath.row].card
            }else{
                cell.cardContainer.alpha = 0
                
            }

            
            if let formattedTipAmount = formatter.string(from: Double(myorders[indexPath.row].propina)! as NSNumber) {
                let propina = Int(round((Float(myorders[indexPath.row].propina)! / Float(subtotal)) * 100))
                cell.propinaValue.text = "\(propina)%   \(formattedTipAmount)"
            }
            
            cell.subtotalLabel.text = "$\(myorders[indexPath.row].pickpal_service!)"
            
            
            if myorders[indexPath.row].delivered  {
                
                
                if myorders[indexPath.row].payment_method == "Efectivo"{
                    cell.status_display.text = "Pedido entregado/Pagado"
                }else{
                    
                    cell.status_display.text = "Pedido Entregado"
                }
                
                cell.checkPedido.image = UIImage(named: "checkGreen")
                
                cell.shipping_cost.visibility = .visible
                cell.titleShippingCost.visibility = .visible
                cell.shipping_cost.text =  "$\(myorders[indexPath.row].shippingCost ?? 0.0)"
            }else if myorders[indexPath.row].refund {
                //                if myorders[indexPath.row]
                cell.status_display.text = "Pedido No Entregado / Sujeto A Reembolso"
                cell.checkPedido.image = UIImage(named: "cancelIcon")
                
                heightRefund = 0
                
                if  myorders[indexPath.row].is_refund{
                    
                    
                    var attributedTitleString: NSAttributedString!
                    var stringStreet: NSAttributedString!
                    
                    let attributesTitle: [NSAttributedString.Key: Any] = [
                        .foregroundColor: UIColor(red: 0.56, green: 0.60, blue: 0.64, alpha: 1.00),
                        .font: UIFont(name: "Aller-Bold", size: 11.0)!
                    ]
                  
                  let attributesStreet: [NSAttributedString.Key: Any] = [
                      .foregroundColor:UIColor(red: 0.56, green: 0.60, blue: 0.64, alpha: 1.00),
                      .font: UIFont(name: "Aller-Italic", size: 11)!
                  ]
                    
                    let completeText = NSMutableAttributedString(string: "")

                    attributedTitleString = NSAttributedString(string: "Compra: ", attributes: attributesTitle)
                    completeText.append(attributedTitleString)
                    
                    stringStreet = NSAttributedString(string: " \(myorders[indexPath.row].fecha!)\n", attributes: attributesStreet)
                   
                    completeText.append(stringStreet)
                    attributedTitleString = NSAttributedString(string: "Reembolso: ", attributes: attributesTitle)
                    completeText.append(attributedTitleString)
                    
                    var dateString = myorders[indexPath.row].refundDate ?? "2020-01-01" + " 00:00:00"
                    dateString = dateString + " 00:00:00"
                    let formatDate = DateFormatter()
                    formatDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let stringToDate = formatDate.date(from: dateString)
                    let formatString = DateFormatter()
                    formatString.dateFormat = "dd/MM/yyyy"
                    
                    if stringToDate != nil{
                        
                        let dateToString = formatString.string(from: stringToDate!)
                        
                        stringStreet = NSAttributedString(string: "\(dateToString)", attributes: attributesStreet)
                       
                        completeText.append(stringStreet)
                                     
                        cell.fecha.attributedText = completeText
                        
                    }else{
                        
                        stringStreet = NSAttributedString(string: "\(myorders[indexPath.row].refundDate!)", attributes: attributesStreet)
                       
                        completeText.append(stringStreet)
                        
                         
                        cell.fecha.attributedText = completeText
                        
                    }
                    //Aqui
                    cell.btnRefundDetails.visibility = .visible
                    cell.btnRefundDetails.tag = indexPath.row
                    cell.btnRefundDetails.addTarget(self, action: #selector(self.getRefundDetails(_:)), for: .touchUpInside)
                    cell.ctlDivisor.constant = myorders[indexPath.row].type_order == "SD" ? 160 : 90
                    cell.ctlBottomTotal.constant = myorders[indexPath.row].type_order == "SD" ? 160 : 90
                    heightRefund = myorders[indexPath.row].type_order == "SD" ? 60 : 40
                    
                    
                    switch  myorders[indexPath.row].type_order {
                    case "SD":
                        cell.status_display.text = "Pedido No Entregado / Reembolsado"
                        
                    case "RS":
                        cell.status_display.text = "Pedido No Recogido / Reembolsado"
                    default:
                        
                            cell.status_display.text = "Pedido No Recogido / No Entregado"
//                        cell.status_display.text = "Pedido Rechazado / Reembolsado"
                    }
                    
                }
                
            }else{
                cell.status_display.text = "Pedido No Recogido / No Entregado"
                cell.checkPedido.image = UIImage(named: "cancelIcon")
                
                if myorders[indexPath.row].expired_refund  {
                    
                    switch  myorders[indexPath.row].type_order {
                    case "SD":
                        cell.status_display.text = "Pedido No Entregado / Vigencia Expirada"
                        
                    case "RS":
                        cell.status_display.text = "Pedido No Recogido / Vigencia Expirada"
                    default:
                        cell.status_display.text = "Pedido No Recogido / No Entregado"
//                        cell.status_display.text = "Pedido Rechazado / Reembolsado"
                    }
                }
            }
            
            if let logo = myorders[indexPath.row].img, !myorders[indexPath.row].img.isEmpty{
                Nuke.loadImage(with: URL(string: logo), into: cell.img)
            }else{
                Nuke.loadImage(with: URL(string: "https://pickpal-assets.s3.amazonaws.com/static/img/avatar_gray_rect.png"), into: cell.img)
            }
            
            cell.category.text = myorders[indexPath.row].place + " - " + myorders[indexPath.row].category
            cell.itemsOrder = myorders[indexPath.row].itemsOrders
            //            if let formattedTipAmount = formatter.string(from: subtotal as NSNumber) {
            
            if let formattedTipAmount = formatter.string(from: subtotal as! NSNumber) {
                cell.subtotalPrice1.text = formattedTipAmount
            }
            
            //            cell.subtotalLabel.text = "$\(subtotal)0"
            //            print("subtotal", subtotal)
            cell.subtotal = subtotal
            //            cell.subsubtotalPrice1.text =
            //            cell.pickpal_service = myorders[indexPath.row].pickpal_service
                        cell.tableView.reloadData()
            cell.delegate = self
            cell.order_id = myorders[indexPath.row].order_id
            cell.stablishment_id = myorders[indexPath.row].stablishment_id
            cell.eat_type.layer.cornerRadius = 3
            cell.eat_type.clipsToBounds = true
            cell.comercioTxt = myorders[indexPath.row].comercio
            if !myorders[indexPath.row].is_billed{
                cell.btnDownload.isUserInteractionEnabled = false
                //                cell.btnDownload.alpha = 0
                cell.facturaLabel.alpha = 0
                cell.is_blilled.text = "No"
            }else{
                cell.btnDownload.isUserInteractionEnabled = true
                //                cell.btnDownload.alpha = 1
                if myorders[indexPath.row].status_invoice == "Pendiente" {
                    cell.facturaLabel.alpha = 1
                    cell.is_blilled.text = "En proceso"
                }else {
                    cell.facturaLabel.alpha = 0
                    cell.is_blilled.text = myorders[indexPath.row].status_invoice
                }
                
            }
            var dictionary = getOrderFromMyOrders(products: myorders[indexPath.row].order_info)
            var tempNameArray = [String]()
            var tempCantArray = [Int]()
            var tempPriceArray = [Double]()
            var tempModifier = [Bool]()
            var tempCatgory = [String]()
            
            for i in 0..<dictionary.count {
                var priceModifiers = Double()
                var priceComplements = Double()
                if let hasModifiers = (dictionary[i] as NSDictionary).value(forKey: "complex") as? Bool {
                    if hasModifiers {
                        if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? String, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                            tempNameArray.append(name)
                            tempPriceArray.append(Double(price)!)
                            tempCantArray.append(quantity)
                            tempModifier.append(false)
                            tempCatgory.append("")
                            tempNameArray.append(name)
                            tempPriceArray.append(Double(i) + 100000)
                            tempCantArray.append(quantity)
                            tempModifier.append(true)
                            tempCatgory.append("")
                        }
                    }else{
                        if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? String, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                            tempNameArray.append(name)
                            tempPriceArray.append(Double(price)!)
                            tempCantArray.append(quantity)
                            tempModifier.append(false)
                            tempCatgory.append("")
                        }
                    }
                    
                }else{
                    if let name = (dictionary[i] as NSDictionary).value(forKey: "name") as? String, let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? String, let quantity = (dictionary[i] as NSDictionary).value(forKey: "quantity") as? Int{
                        tempNameArray.append(name)
                        tempPriceArray.append(Double(price)!)
                        tempCantArray.append(quantity)
                        tempModifier.append(false)
                        tempCatgory.append("")
                    }
                }
                
                if let complements = ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as? [String]{
                    //                        if let comp = complements.value(forKey: "complement_price") as? [Double] {
                    //                    print(((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double], "complements price")
                    tempPriceArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]//complements.value(forKey: "complement_price") as! [Double]
                    tempCantArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_cant") as! [Int]//complements.value(forKey: "complement_cant") as! [Int]
                    tempNameArray += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as! [String]//complements.value(forKey: "complement_name") as! [String]
                    tempCatgory  += ((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_category") as! [String]
                    for i in 0..<(((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).count {
                        tempModifier.append(true)
                    }
                    priceComplements = (((dictionary[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).reduce(0, +)
                    //                        }
                }
                if let modifiers = (dictionary[i] as NSDictionary).value(forKey: "modifiers") as? NSArray {
                    for modi in modifiers{
                        tempNameArray += (modi as! NSDictionary).value(forKey: "modifier_name") as! [String]
                        tempPriceArray += (modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]
                        tempCatgory += (modi as! NSDictionary).value(forKey: "modifier_category") as! [String]
                        //                        print((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double])
                        for i in 0..<((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).count {
                            tempCantArray.append(1)
                            tempModifier.append(true)
                        }
                        priceModifiers = ((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).reduce(0, +)
                    }
                }
                if let price = (dictionary[i] as NSDictionary).value(forKey: "price") as? String {
                    let priceDouble = Double(price)!
                    for prices in tempPriceArray {
                        let indice = tempPriceArray.index(of: Double(i) + 100000)
                        if indice != nil {
                            tempPriceArray[indice!] = priceDouble - priceModifiers - priceComplements
                        }
                    }
                }
                
            }
            cell.modifier_name = tempNameArray
            cell.modifier_cant =  tempCantArray
            cell.modifier_price = tempPriceArray
            cell.isModifier = tempModifier
            cell.modifier_cat = tempCatgory
            allItemsOrder[indexPath.row] = tempNameArray
            cell.tableView.reloadData()
            switch myorders[indexPath.row].rating {
            case 1:
                cell.star1.image = UIImage(named: "starSelected")
            case 2:
                cell.star1.image = UIImage(named: "starSelected")
                cell.star2.image = UIImage(named: "starSelected")
            case 3:
                cell.star1.image = UIImage(named: "starSelected")
                cell.star2.image = UIImage(named: "starSelected")
                cell.star3.image = UIImage(named: "starSelected")
            case 4:
                cell.star1.image = UIImage(named: "starSelected")
                cell.star2.image = UIImage(named: "starSelected")
                cell.star3.image = UIImage(named: "starSelected")
                cell.star4.image = UIImage(named: "starSelected")
            case 5:
                cell.star1.image = UIImage(named: "starSelected")
                cell.star2.image = UIImage(named: "starSelected")
                cell.star3.image = UIImage(named: "starSelected")
                cell.star4.image = UIImage(named: "starSelected")
                cell.star5.image = UIImage(named: "starSelected")
            default:
                print("default")
            }
            
            if myorders[indexPath.row].shippingCost! != 0{
                
                cell.shipping_cost.visibility = .visible
                cell.titleShippingCost.visibility = .visible
                
                
            }else{
                
                cell.shipping_cost.visibility = .gone
                cell.titleShippingCost.visibility = .gone
                cell.heightContainerSubtotal.constant -= sizeSubtotal
                sizeTotales =  sizeTotales - sizeSubtotal
                
            }
            
            
            if myorders[indexPath.row].refund && !myorders[indexPath.row].confirmed{
                
                cell.status_display.text = "Pedido Rechazado / Reembolsado"
            }else{
                
                heightRefund = 0
            }
            
            
            return cell
        }else{
            
            if pedidos.count > 0 {
                
                if pedidos[indexPath.section][indexPath.row].statusController != ""{
                    
                    // MARK: - Oscar Listado de pedidos
                    if pedidos[indexPath.section][indexPath.row].is_paid{
                        
//                        if pedidos[indexPath.section].count == indexPath.row{
//                            self.indexPath = indexPath
//                        }
                        // MARK: - Oscar Listado Pagados
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "cardTitlePagados", for: indexPath) as! PagadosTableViewCell
                        cell.delegate = self
                        let formatter = NumberFormatter()
                        formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                        formatter.numberStyle = .currency
                        
                        if pedidos[indexPath.section][indexPath.row].order_id == ultimoPedido{
                            self.indexPath = indexPath
//                            cell.establishmentName.lineBreakMode = .byWordWrapping
//                            cell.establishmentName.numberOfLines = 0
                        }else{
                            
                            cell.establishmentName.lineBreakMode = .byWordWrapping
                            cell.establishmentName.numberOfLines = 1
                        }
                      
                        
                        //SERIVCIO AL CLIENTE (LLAMAR MESERO PEDIR CUENTA)
                        
                        // SOLO SI ES SM SE MUESTRA EL BOTON
                        if  pedidos[indexPath.section][indexPath.row].order_type == "SM"{
                            
                            //SI TIENE AVILITADO EL SERVICO A CLIENTE MOSTRAMOS EL BOTON
                            cell.btnCustomerService.visibility = pedidos[indexPath.section][indexPath.row].av_customer_service ? .visible:.gone
                            
                            //ACTIVAMOS O DESACTIVAMOS LA INTERACCION
//                            cell.btnCustomerService.isUserInteractionEnabled = pedidos[indexPath.section][indexPath.row].active_customer_service
                            
                            // SI ESTA ACTIVA CAMBIAMOS LA IMAGEN DEL BOTON
                            if pedidos[indexPath.section][indexPath.row].active_customer_service{
                                
                                
                                cell.btnCustomerService.setImage(UIImage(named: "ic_btn_customer_service.png"),for: .normal)
                            }else{
                                
                                cell.btnCustomerService.setImage(UIImage(named: "ic_btn_customer_service_off.png"),for: .normal)
                                
                            }
                            
                           
                        }else{
                            cell.btnCustomerService.visibility = .gone
                        }
                        

                        
                        if pedidos[indexPath.section][indexPath.row].is_generic{
                            
                            
                            if (pedidos[indexPath.section][indexPath.row].order_type == "RS" || pedidos[indexPath.section][indexPath.row].order_type == "SD") && pedidos[indexPath.section][indexPath.row].is_generic{
                            
                            
                            cell.btnActionConditionsOrder.visibility = .visible
                        }
                            
                        }else{
                            
                            cell.btnActionConditionsOrder.visibility = .gone
                            
                        }

                        
                        cell.indexPath = indexPath
                        cell.selectionStyle = .none
                        
                        cell.lblAddressSendValue.text = pedidos[indexPath.section][indexPath.row].address
                        cell.establishmentName.text = pedidos[indexPath.section][indexPath.row].establishment_name
                        let dateString = pedidos[indexPath.section][indexPath.row].register_date! + " 00:00:00"
                        let formatDate = DateFormatter()
                        formatDate.dateFormat = "dd-MM-yyyy HH:mm:ss"
                        let stringToDate = formatDate.date(from: dateString)
                        let formatString = DateFormatter()
                        formatString.dateFormat = "dd/MM/yyyy"
                        
                        if stringToDate != nil{
                            let dateToString = formatString.string(from: stringToDate!)
                            cell.dateLabel.text = dateToString
                        }else{
                            
                            cell.dateLabel.text = pedidos[indexPath.section][indexPath.row].register_date!
                            
                        }
                        
                       
                        
                        let folio = pedidos[indexPath.section][indexPath.row].folio_complete!
                        var foliFormate = ""
                        var index = 0
                        for char in folio {
                            
                            if index <= 3 {
                                
                                foliFormate += String (char)
                                index = index + 1
                            }else{
                                
                                foliFormate += "-" + String(char)
                                index = 1
                            }
                        }
                        
                        cell.lblAddressSend.visibility = .gone
                        cell.lblAddressSendValue.visibility = .gone
                        cell.folioPdido.text = foliFormate// String(describing:  pedidos[indexPath.section][indexPath.row].folio_complete!)
                        /* cell.ctlTopBtnConditions.constant = 50
                         cell.ctlTopLabelConditions.constant = 58
                         cell.ctlTopResponsabiity.constant = 88*/
                        cell.horaEstimadaLabel.text = "Hora estimada de entrega"
                        switch pedidos[indexPath.section][indexPath.row].order_type {
                        case "SM": //SM - servicio a Mesa
                            //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                            //                        cell.ctlTopResponsabiity.constant = 50
                            //    cell.btnActionConditionsOrder.isHidden = true
                            //       cell.lblConditionsOrder.isHidden = false
                            cell.modeLabel.text = "Servicio a mesa \(pedidos[indexPath.section][indexPath.row].number_table.description)"
                            cell.propina.visibility = .gone
                            cell.propinaLabel.visibility = .gone
                            cell.titleServicioDomicilio.visibility = .gone
                            cell.servicioADomicilio.visibility = .gone
                            break
                            
                        case "RB"://RB - Recoger en Barra
                            //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                            //  cell.ctlTopResponsabiity.constant = 50
                           
                            cell.lblConditionsOrder.visibility = .visible
                            cell.modeLabel.text = "Recoger en Barra"
                            cell.propina.visibility = .visible
                            cell.propinaLabel.visibility = .visible
                            cell.titleServicioDomicilio.visibility = .gone
                            cell.servicioADomicilio.visibility = .gone
                            break
                            
                        case "RS"://RS - Recoger en Sucursal
                            //cell.btnHelp.frame = CGRect(x: 45, y: 580, width: 200, height: 35)
                            cell.modeLabel.text = "Recoger en sucursal"
                            cell.propina .visibility = .gone
                            cell.propinaLabel.visibility = .gone
                            cell.titleServicioDomicilio.visibility = .gone
                            cell.servicioADomicilio.visibility = .gone
                            break
                        default://SD - Servicio a Domicilio
                          
                            cell.lblConditionsOrder.visibility = .visible
                            cell.lblAddressSend.visibility = .visible
                            cell.lblAddressSendValue.visibility = .visible
                            cell.modeLabel.text = "Servicio a domicilio"
                            cell.titleServicioDomicilio.visibility = .visible
                            cell.servicioADomicilio.visibility = .visible
                            cell.propina.visibility = .visible
                            cell.propinaLabel.visibility = .visible
                            cell.lblAddressSendValue.text = pedidos[indexPath.section][indexPath.row].address
                            cell.recogeQRLabel.text = "Proporciona este número de folio al repartidor"
//                            cell.servicioADomicilio.text = "$\(pedidos[indexPath.section][indexPath.row].shipping_cost ?? 0.0)"
                            cell.tiempoEntrega.visibility = .visible
                            cell.tiempoEntrega.text = "Tiempo de entrega estimado:  \(pedidos[indexPath.section][indexPath.row].time_delivery!) min."
                            cell.recogeQRLabel.text = "Proporciona este número de folio al repartidor"
                            if pedidos[indexPath.section][indexPath.row].shipping_cost != 0{
                                
                                cell.servicioADomicilio.visibility = .visible
                                cell.titleServicioDomicilio.visibility = .visible
                                cell.servicioADomicilio.text = "$\(pedidos[indexPath.section][indexPath.row].shipping_cost ?? 0.0)"
                                
                            }else{
                                
                                cell.servicioADomicilio.visibility = .gone
                                cell.titleServicioDomicilio.visibility = .gone
                                
                            }
                            break
                        }
                        
                        
                        if let orderNumber = pedidos[indexPath.section][indexPath.row].order_number {
                            if  orderNumber != "" {
                                cell.numPedido.text = pedidos[indexPath.section][indexPath.row].folio + "/" + orderNumber
                                
                            }else{
                                cell.numPedido.text = pedidos[indexPath.section][indexPath.row].folio
                            }
                            
                        }else{
                            cell.numPedido.text = pedidos[indexPath.section][indexPath.row].folio
                        }
                        
                        cell.lblPointsUsed.text = "-\(CustomsFuncs.numberFormat(value: pedidos[indexPath.section][indexPath.row].points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: pedidos[indexPath.section][indexPath.row].money_puntos ?? 0))"
                        
                        if let formattedTipAmount = formatter.string(from: (pedidos[indexPath.section][indexPath.row].percent) as NSNumber) {
                            if pedidos[indexPath.section][indexPath.row].percent > 0 {
                                let percent = Int(round((Float(pedidos[indexPath.section][indexPath.row].percent ?? 0) / Float(pedidos[indexPath.section][indexPath.row].total ?? 0)) * 100))
                                cell.propina.text = "\(percent)%    \(formattedTipAmount)"
                                cell.propinaLabel.visibility = .visible
                                cell.propina.visibility = .visible
                            }else{
                                
                               
                                cell.propinaLabel.visibility = .gone
                                cell.propina.visibility = .gone
                            }
                           
                        }
                        if let formattedTipAmount = formatter.string(from: pedidos[indexPath.section][indexPath.row].pickpal_comission as NSNumber) {
                        
                        cell.servicioPickpal.text = "\(formattedTipAmount)"
                        }
                        
                        
                        if let formattedTipAmount = formatter.string(from: (pedidos[indexPath.section][indexPath.row].total) as NSNumber) {
                            cell.subtotal.text = "\(formattedTipAmount)"
                        }
                        
                        
                        
                        
                        //MOSTRAR TOTALES Y PAGOS (PEDIDSO PAGADOS)
                        
                        
                        //TOTAL TARJETA MINIMIZADA
                        
                        
                        let subTotal = ( pedidos[indexPath.section][indexPath.row].total + pedidos[indexPath.section][indexPath.row].pickpal_comission + pedidos[indexPath.section][indexPath.row].percent +
                            pedidos[indexPath.section][indexPath.row].shipping_cost)
                        
                        if let formattedTipAmount = formatter.string(from: subTotal as NSNumber) {
                            cell.total.text = "\(formattedTipAmount)"
                        }
                        
                        //TOTAL
                     
                        
                        
                        if let formattedTipAmount = formatter.string(from: subTotal as NSNumber) {
                            cell.totales.text = "\(formattedTipAmount)"
                        }
                        
                        
                        // MOOSTRAR OCULTAR PUNTOS MOVILES
                        
                        if pedidos[indexPath.section][indexPath.row].points > 0{
                            
                            cell.lblPointsUsed.visibility = .visible
                            cell.titlePointsUsed.visibility = .visible
                            
                            cell.lblPointsUsed.text = "-\(CustomsFuncs.numberFormat(value: pedidos[indexPath.section][indexPath.row].points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney:  pedidos[indexPath.section][indexPath.row].money_puntos ?? 0))"
                            
                            
                        }else{
                            cell.lblPointsUsed.visibility = .gone
                            cell.titlePointsUsed.visibility = .gone
                        }
                        
                       
                        
                        
                        //MOSTRAR OCULTAR PICKPAL CASH
    
                        if pedidos[indexPath.section][indexPath.row].pickpal_cash_money > 0{
                                  
                            cell.textPickPalcash.visibility = .visible
                            
                            cell.totalPickpalCash.visibility = .visible
                            
                            cell.totalPickpalCash.text = "\(CustomsFuncs.getFomatMoney(currentMoney: pedidos[indexPath.section][indexPath.row].pickpal_cash_money!))"

                            
                        }else{
                            
                            
                            cell.textPickPalcash.visibility = .gone
                            
                            cell.totalPickpalCash.visibility = .gone
                            
                            
                        }
                        
                        
                        //MOSTRAR OCULTAR TARJETA
                        
                        
                        //Tipo de pago
                        
                       
                        if pedidos[indexPath.section][indexPath.row].payment_method == 3{

                            cell.lblTitleCard.text = "Tarjeta"
                        }
                        
                        if pedidos[indexPath.section][indexPath.row].payment_method == 2{

                            cell.lblTitleCard.text = "Efectivo"
                            
                        }

                        
                        let pagoTarjeta = (subTotal - pedidos[indexPath.section][indexPath.row].pickpal_cash_money) - pedidos[indexPath.section][indexPath.row].money_puntos
                        
                        
                        if  pagoTarjeta > 0{
                            
                            
                            cell.lblAmountCard.visibility = .visible
                            cell.lblTitleCard.visibility = .visible
                            
                            cell.lblAmountCard.text = "\(CustomsFuncs.getFomatMoney(currentMoney: pagoTarjeta))"
                            
                            
                        }else{
                            
                            cell.lblAmountCard.visibility = .gone
                            cell.lblTitleCard.visibility = .gone
                            
                        }
                        
                        
                        //TOTAL PAGADO
                        
                        
                        let totalPedidoPagado = pagoTarjeta + pedidos[indexPath.section][indexPath.row].pickpal_cash_money + pedidos[indexPath.section][indexPath.row].money_puntos
                        
                        cell.lblTotalPagado.text = "\(CustomsFuncs.getFomatMoney(currentMoney: totalPedidoPagado))"
                        
                        
                        
                        
                        
                        
                        //                    cell.subtotal.text = "$\(pedidos[indexPath.section][indexPath.row].total ?? 0)0"
                        //                    cell.servicioPickpal.text = "$\(pedidos[indexPath.section][indexPath.row].pickpal_comission ?? 0)0"
                        //                    cell.totales.text = "$\(pedidos[indexPath.section][indexPath.row].total_after_checkout ?? 0)0"
                        //                cell.gifImage.loadGif(name: "GifProcess")
                        var tempNameArray = [String]()
                        var tempPriceArray = [Double]()
                        var tempCantArray = [Int]()
                        var tempModifier = [Bool]()
                        var tempCategory = [String]()
                        let order_ele = pedidos[indexPath.section][indexPath.row].order_elements!
                        //                print(order_ele, "order_ele")
                        cell.elements = order_ele
                        let pagado = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        //                cell.pagado.text = pagado
                        //                cell.enproceso.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].process_time ?? "00:00:00")
                        //                cell.listo.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].ready_time ?? "00:00:00")
                        
                        cell.horaProceso.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.horaPagado.text = pagado
                        cell.notes.text = pedidos[indexPath.section][indexPath.row].notes
                        
                        if pedidos[indexPath.section][indexPath.row].notes == ""{
                            cell.titleNotas.visibility = .gone
                        }else{
                            cell.titleNotas.visibility = .visible
                        }
                        
                        if DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].ready_time ?? "00:00:00") == "" {
                            if pedidos[indexPath.section][indexPath.row].is_scheduled {
                                cell.horaListo.alpha = 0
                                cell.pedidoListoTitle.alpha = 0
                                cell.horaEstimadaRedHeader.text = "Hora programada de entrega"
                                cell.horaEstimadaRedHeader.textColor = UIColor.init(red: 203/255, green: 29/255, blue: 62/255, alpha: 1)
                                cell.hour_pagado.alpha = 1
                                cell.horaEstimadaLabel.text = "Hora programada de entrega"
                                cell.pedidoPagadoLbl.alpha = 1
                                // cell.pedidoPagadoLbl.text = "Pedido programado"
                            }else{
                                
                                cell.horaListo.alpha = 0
                                cell.pedidoListoTitle.alpha = 0
                                cell.horaEstimadaRedHeader.textColor = UIColor.init(red: 203/255, green: 29/255, blue: 62/255, alpha: 1)
                                cell.hour_pagado.alpha = 1
                                if pedidos[indexPath.section][indexPath.row].in_bar {
                                    cell.horaEstimadaRedHeader.text = "Hora estimada de entrega en barra"
                                    cell.horaEstimadaLabel.text = "Hora estimada de entrega en barra"
                                    //cell.horaEstimadaLabel.text = ""
                                    cell.pedidoPagadoLbl.alpha = 1
                                    //  cell.pedidoPagadoLbl.text = "Pedido en barra"
                                    cell.recogeQRLabel.text = "Recoge tu pedido mostrando este código QR"
                                }else{
                                    cell.pedidoPagadoLbl.alpha = 0
                                    cell.horaEstimadaRedHeader.text = "Hora estimada de entrega"
                                    cell.horaEstimadaLabel.text = "Hora estimada de entrega"
                                    //   cell.recogeQRLabel.text = "Recoge tu pedido mostrando este código QR"
                                }
                            }
                            
                            
                            
                            
                        }else{
                            cell.horaListo.alpha = 1
                            cell.pedidoListoTitle.alpha = 1
                            cell.horaListo.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].ready_time ?? "00:00:00")
                            if pedidos[indexPath.section][indexPath.row].number_table > 0 {
                                cell.horaEstimadaRedHeader.text = "Pedido listo en mesa \(pedidos[indexPath.section][indexPath.row].number_table.description)"
                            }else{
                                cell.horaEstimadaRedHeader.text = "Pedido listo"
                            }
                            cell.horaEstimadaRedHeader.textColor = UIColor.init(red: 28/255, green: 215/255, blue: 134/255, alpha: 1)
                            cell.hour_pagado.alpha = 0
                            //            cell.barraListoConstraint.constant = 0
                            cell.pedidoEnLabel.text = "Pedido listo"
                            cell.recogeQRLabel.alpha = 1
                            
                            cell.horaEstimadaLabel.alpha = 0
                            cell.veARecogerLabel.alpha = 1
                            cell.horaCreado.text = "Pedido listo"
                            
                            if pedidos[indexPath.section][indexPath.row].is_scheduled {
                                cell.pedidoPagadoLbl.alpha = 1
                                // cell.pedidoPagadoLbl.text = "Pedido programado"
                                cell.recogeQRLabel.text = "Recoge tu pedido mostrando este código QR"
                                cell.veARecogerLabel.text = "¡Ve a recoger tu pedido!"
                            }else{
                                if pedidos[indexPath.section][indexPath.row].in_bar {
                                    cell.pedidoPagadoLbl.alpha = 1
                                    cell.pedidoPagadoLbl.text = "Pedido en barra"
                                    cell.recogeQRLabel.text = "Recoge tu pedido mostrando este código QR"
                                    cell.veARecogerLabel.text = "¡Ve a recoger tu pedido!"
                                }else{
                                    cell.pedidoPagadoLbl.alpha = 0
                                    cell.recogeQRLabel.text = "Recoge tu pedido mostrando este código QR"
                                    cell.veARecogerLabel.text = "¡Ve a recoger tu pedido!"
                                }
                                
                            }
                            
                            if pedidos[indexPath.section][indexPath.row].order_type == "SD"{
                                
                                if pedidos[indexPath.section][indexPath.row].statusItem == "ready"{
                                    cell.veARecogerLabel.text = "¡Tu pedido esta en camino!"
                                }else{
                                    cell.veARecogerLabel.text = "¡Paga tu pedido al recibirlo!"
                                }
                                
                                
                                cell.recogeQRLabel.text = "Proporciona este número de folio al repartidor"
                            }
                            
                            
                            if pedidos[indexPath.section][indexPath.row].in_site {
                                cell.veARecogerLabel.text = "Tu pedido ya va en camino y será entregado a tu mesa."
                            }
                            
                            
                            
                            
                        }
                        
                        
                        cell.pedidoPagadoLbl.alpha = 0
                        
                        
                        let estimated = DatabaseFunctions.shared.getEstimated(processHour: pedidos[indexPath.section][indexPath.row].pay_time ?? "", registerDay: pedidos[indexPath.section][indexPath.row].register_date ?? "", prepTime: pedidos[indexPath.section][indexPath.row].estimated_time)
                        
                        cell.hour_pagado.text = pedidos[indexPath.section][indexPath.row].est_delivery_hour != "" ? formatterHour(hour: pedidos[indexPath.section][indexPath.row].est_delivery_hour) : estimated
                        
//                        cell.hour_pagado.text =  DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].est_delivery_hour)
                        
                        cell.horaCreado.text = pedidos[indexPath.section][indexPath.row].est_delivery_hour != "" ? formatterHour(hour: pedidos[indexPath.section][indexPath.row].est_delivery_hour) : estimated
                        //                    print(order_ele)
                        for i in 0..<order_ele.count {
                            var priceModifiers = Double()
                            var priceComplements = Double()
                            if let hasModifiers = (order_ele[i] as NSDictionary).value(forKey: "complex") as? Bool {
                                
                                if hasModifiers {
                                    if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                        tempNameArray.append(name)
                                        tempPriceArray.append(price)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(false)
                                        tempCategory.append("")
                                        tempNameArray.append(name)
                                        tempPriceArray.append(Double(i) + 100000)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(true)
                                        tempCategory.append("")
                                    }
                                }else{
                                    if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                        tempNameArray.append(name)
                                        tempPriceArray.append(price)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(false)
                                        tempCategory.append("")
                                    }
                                }
                                
                            }else{
                                if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                    tempNameArray.append(name)
                                    tempPriceArray.append(price)
                                    tempCantArray.append(quantity)
                                    tempModifier.append(false)
                                    tempCategory.append("")
                                }
                            }
                            
                            if let complements = ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as? [String]{
                                //                        if let comp = complements.value(forKey: "complement_price") as? [Double] {
                                tempPriceArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]//complements.value(forKey: "complement_price") as! [Double]
                                tempCantArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_cant") as! [Int]//complements.value(forKey: "complement_cant") as! [Int]
                                tempNameArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as! [String]//complements.value(forKey: "complement_name") as! [String]
                                tempCategory += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_category") as! [String]
                                for i in 0..<(((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).count {
                                    tempModifier.append(true)
                                }
                                priceComplements = (((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).reduce(0, +)
                                //                            print(priceComplements)
                            }
                            if let modifiers = (order_ele[i] as NSDictionary).value(forKey: "modifiers") as? NSArray {
                                for modi in modifiers{
                                    tempNameArray += (modi as! NSDictionary).value(forKey: "modifier_name") as! [String]
                                    tempPriceArray += (modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]
                                    tempCategory += (modi as! NSDictionary).value(forKey: "modifier_category") as! [String]
                                    for i in 0..<((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).count {
                                        tempCantArray.append(1)
                                        tempModifier.append(true)
                                    }
                                    priceModifiers = ((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).reduce(0, +)
                                    //                                print(priceModifiers)
                                }
                            }
                            if let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double {
                                for prices in tempPriceArray {
                                    let indice = tempPriceArray.index(of: Double(i) + 100000)
                                    if indice != nil {
                                        tempPriceArray[indice!] = price - priceModifiers - priceComplements
                                    }
                                }
                            }
                            
                        }
                        
                        //                print(order_ele)
                        cell.modifier_name = tempNameArray
                        cell.modifier_cant =  tempCantArray
                        cell.modifier_price = tempPriceArray
                        cell.isModifier = tempModifier
                        cell.modifier_cat = tempCategory
                        //                    print(cell.modifier_name.count)
                        
                
                        if pedidos[indexPath.section][indexPath.row].qr != "" {
                            Nuke.loadImage(with: URL(string: pedidos[indexPath.section][indexPath.row].qr)!, into: cell.qrImage)
                        }
                        
                        if pedidos[indexPath.section][indexPath.row].order_type != "RS" && pedidos[indexPath.section][indexPath.row].percent == 0{
                            //   if pedidos[indexPath.section][indexPath.row].percent > 0{
                            cell.pedidoEnLabel.visibility  = .visible
                            cell.pedidoEnLabel.text = "Recuerda que la propina no esta incluida en el pago de tu cuenta actual."
                            cell.pedidoEnLabel.textColor = UIColor(red: 0.82, green: 0.11, blue: 0.23, alpha: 1.00)
                            cell.pedidoEnLabel.font = cell.pedidoEnLabel.font.withSize(10)
                            
                        }else{
                            cell.pedidoEnLabel.visibility  = .gone
                            cell.pedidoEnLabel.textColor = UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00) // #5A6872
                            cell.pedidoEnLabel.font = cell.pedidoEnLabel.font.withSize(16)
                            //tableView.reloa
                        }
                        
                        
                        
                        if pedidos[indexPath.section][indexPath.row].confirmed{// && !pedidos[indexPath.section][indexPath.row].to_refund{
                            
                            cell.horaConfirmarPagado.text =  DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00")
                            cell.tituloConfirmarPedido.text = "Pedido confirmado"
                            
                            cell.horaEstimadaRedHeader.text = "Hora estimada de entrega"
                            cell.hour_pagado.text = pedidos[indexPath.section][indexPath.row].est_delivery_hour != "" ? formatterHour(hour: pedidos[indexPath.section][indexPath.row].est_delivery_hour) : estimated
                                    cell.hour_pagado.visibility = .visible
                            
                            
                        }else{
                            
//                            if !pedidos[indexPath.section][indexPath.row].to_refund{
                                cell.lblTituloEstatus.text = "Pedido por confirmar"
                                cell.horaConfirmarPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                                cell.tituloConfirmarPedido.text = "Pedido por confirmar"
                                
                                cell.horaEstimadaRedHeader.text = "Pedido por confirmar"
                                cell.hour_pagado.text = ""
                                
//                            }
                            
                        }
                        
                        cell.tableView.reloadData()
                       
                        
                    switch pedidos[indexPath.section][indexPath.row].statusController {
                        
                        
                        
                    case "not_confirmed":
                        
                        cell.horaPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.peidoPagadoTitle.text = "Pedido pagado"
                       
                    
                        cell.horaProceso.text = ""
                        cell.pedidoEnProcesoTitle.text = ""
                        cell.horaListo.text = ""
                        cell.pedidoListoTitle.text = ""
                        cell.veARecogerLabel.text = ""
                        cell.lblTituloEstatus.text = "Pedido por confirmar"
                        
                        
                    case "new":
                        
                        cell.horaPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.peidoPagadoTitle.text = "Pedido pagado"
                        if pedidos[indexPath.section][indexPath.row].confirmed{
                            cell.lblTituloEstatus.text = "Pedido en proceso"
                        }else{
                            
                            cell.lblTituloEstatus.text = "Pedido por confirmar"
                        }
                       
    
                        cell.horaProceso.text = ""
                        cell.pedidoEnProcesoTitle.text = ""
                        cell.horaListo.text = ""
                        cell.pedidoListoTitle.text = ""
                        cell.veARecogerLabel.text = ""
                        
                    case "to_pay":
                       
                        cell.horaPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.peidoPagadoTitle.text = "Pedido pagado"
                        cell.horaConfirmarPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00")
                        
                        cell.horaProceso.text = ""
                        cell.pedidoEnProcesoTitle.text = ""
                        cell.horaListo.text = ""
                        cell.pedidoListoTitle.text = ""
                        cell.veARecogerLabel.text = ""
                        
                        
                        
                    case "in_process", "delayed", "delivery_delayed": // en proceso y listo
                        
                        cell.horaPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.peidoPagadoTitle.text = "Pedido pagado"
                        cell.horaProceso.text =  DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].process_time ?? "00:00:00")
                        cell.pedidoEnProcesoTitle.text = "Pedido en proceso"
                        cell.horaListo.text = ""
                        cell.pedidoListoTitle.text = ""
                        cell.lblTituloEstatus.text = "Pedido en proceso"
                        cell.veARecogerLabel.text = ""
                        
//                            if pedidos[indexPath.section][indexPath.row].est_ready_hour != "" {
//                                cell.pedidoListoTitle.alpha = 1
//                                cell.horaListo.alpha = 1
//                                cell.horaListo.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].est_ready_hour ?? "00:00:00")
//                                cell.pedidoListoTitle.text = "Pedido listo"
//                                cell.lblTituloEstatus.text = "Pedido listo"
//                            }
                        
                    case "ready":
                        cell.pedidoListoTitle.alpha = 1
                        cell.horaListo.alpha = 1
                        
                        cell.horaPagado.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                        cell.peidoPagadoTitle.text = "Pedido pagado"
                        
                        cell.horaProceso.text =  DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00")
                        cell.pedidoEnProcesoTitle.text = "Pedido en proceso"
                        
                        if pedidos[indexPath.section][indexPath.row].notified{
                            
                            cell.horaListo.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].ready_time ?? "00:00:00")
                            cell.pedidoListoTitle.text = "Pedido listo"
                            cell.lblTituloEstatus.text = "Pedido listo"
                            if pedidos[indexPath.section][indexPath.row].number_table > 0 {
                                cell.horaEstimadaRedHeader.text = "Pedido listo en mesa \(pedidos[indexPath.section][indexPath.row].number_table.description)"
                            }else{
                                cell.horaEstimadaRedHeader.text = "Pedido listo"
                            }
//                            openAlertOrdenLista(establishName: pedidos[indexPath.section][indexPath.row].establishment_name)
                        }else{
                            
                            cell.horaProceso.text =  DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00")
                            cell.pedidoEnProcesoTitle.text = "Pedido en proceso"
                            cell.horaListo.text = ""
                            cell.pedidoListoTitle.text = ""
                            cell.lblTituloEstatus.text = "Pedido en proceso"
                            
                        }
                        
                        
                        
                    default:
                        
                        cell.horaPagado.text = ""
                        cell.peidoPagadoTitle.text = ""
                        cell.horaProceso.text = ""
                        cell.pedidoEnProcesoTitle.text = ""
                        cell.horaListo.text = ""
                        cell.pedidoListoTitle.text = ""
                            
                        }
                   
                        if  pedidos[indexPath.section][indexPath.row].to_refund{
                                                  
                            cell.hour_pagado.visibility = .gone
                            cell.pedidoEnLabel.visibility = .visible
                            cell.recogeQRLabel.text = "Presenta este Código QR para obtener tu reembolso"
                            cell.pedidoEnLabel.text = "Pedido factible a reembolsar o recoger "
//                            cell.veARecogerLabel.text = "¡Tu pedido ha expirado!"
                            cell.veARecogerLabel.text = ""
                            cell.lblTituloEstatus.text = ""
                            cell.horaEstimadaRedHeader.text = "Pedido factible a reembolsar o recoger"
                            cell.horaEstimadaRedHeader.textColor =  UIColor.init(red: 203/255, green: 29/255, blue: 62/255, alpha: 1)
                            
                            cell.tiempoEntrega.visibility = .visible
                            
                            if pedidos[indexPath.section][indexPath.row].order_type == "SD"{
                                cell.tiempoEntrega.text = "Tienes un período de un día para pedir tu reembolso."
                                
                                
                            } else{
                                cell.tiempoEntrega.text = "Tienes un período de \( pedidos[indexPath.section][indexPath.row].days_elapsed!) días para pedir tu reembolso."
                                
                            }
                            
                            cell.tableView.reloadData()
                        }else{
                            cell.tiempoEntrega.visibility = .gone
                            cell.hour_pagado.visibility = .visible
                            // cell.recogeQRLabel.text = "Presenta este Código QR para obtener tu reembolso"
                            if pedidos[indexPath.section][indexPath.row].order_type != "RS" && pedidos[indexPath.section][indexPath.row].percent != 0{
                         
                            cell.pedidoEnLabel.text = ""
                                cell.pedidoEnLabel.visibility = .gone
                                
                            }
                        }

                        return cell
                        
                    }else{
                        
                        
                        // MARK: - Oscar Listado Por Pagar
                        
                        // if  pedidos[indexPath.section][indexPath.row].statusController != "new"{
                        print("Estatus ", pedidos[indexPath.section][indexPath.row].statusController)
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "cardTitlePagar", for: indexPath) as! PorPagarTableViewCell
                        cell.selectionStyle = .none
                        cell.delegate = self
                        cell.order_id = pedidos[indexPath.section][indexPath.row].order_id
                        cell.buttonConditions.visibility = .gone
                        
                        if pedidos[indexPath.section][indexPath.row].order_id == ultimoPedido{
                            self.indexPath = indexPath

                            
                        }else{
                            cell.establishment_name.lineBreakMode = .byWordWrapping
                            cell.establishment_name.numberOfLines = 1
                        }
                        
                        
                        if pedidos[indexPath.section][indexPath.row].is_generic{
                            
                        if (pedidos[indexPath.section][indexPath.row].order_type == "RS" || pedidos[indexPath.section][indexPath.row].order_type == "SD") {
                            
                            cell.buttonConditions.visibility = .visible
                            
                        
                            
                        }else{
                            
                            cell.buttonConditions.visibility = .gone
                            
                        }
                        }
                        
                        
                        //SERIVCIO AL CLIENTE (LLAMAR MESERO PEDIR CUENTA)
                        
                        // SOLO SI ES SM SE MUESTRA EL BOTON
                        if  pedidos[indexPath.section][indexPath.row].order_type == "SM"{
                            
                            //SI TIENE AVILITADO EL SERVICO A CLIENTE MOSTRAMOS EL BOTON
                            cell.btnServicioAlCliente.visibility = pedidos[indexPath.section][indexPath.row].av_customer_service ? .visible:.gone
                            
                            //ACTIVAMOS O DESACTIVAMOS LA INTERACCION
//                            cell.btnCustomerService.isUserInteractionEnabled = pedidos[indexPath.section][indexPath.row].active_customer_service
                            
                            // SI ESTA ACTIVA CAMBIAMOS LA IMAGEN DEL BOTON
                            if pedidos[indexPath.section][indexPath.row].active_customer_service{
                                
                                
                                cell.btnServicioAlCliente.setImage(UIImage(named: "ic_btn_customer_service.png"),for: .normal)
                            }else{
                                
                                cell.btnServicioAlCliente.setImage(UIImage(named: "ic_btn_customer_service_off.png"),for: .normal)
                                
                            }
                            
                           
                        }else{
                            cell.btnServicioAlCliente.visibility = .gone
                        }
                        
                        
                        
                        //MOSTRAR TOTALES Y PAGOS (PEDIDSO PAGADOS)
                        
                        let subTotal = ( pedidos[indexPath.section][indexPath.row].total + pedidos[indexPath.section][indexPath.row].pickpal_comission + pedidos[indexPath.section][indexPath.row].percent +
                            pedidos[indexPath.section][indexPath.row].shipping_cost)
                        
                        //Total Minimizado
                        
                        
                        cell.totalLabel.text = "\(CustomsFuncs.getFomatMoney(currentMoney: subTotal))"
                        
                        
                        
                        //Subtotal
                        
                       
                        cell.subtotalLabel.text = "\(CustomsFuncs.getFomatMoney(currentMoney: pedidos[indexPath.section][indexPath.row].subtotal ))"
                        
                        cell.totalTable.text = "\(CustomsFuncs.getFomatMoney(currentMoney: subTotal))"
                        
                        
                        //MOSTRAR U OCULTAR CONTENEDOR PAGOS
                        
                        if pedidos[indexPath.section][indexPath.row].points == 0 && pedidos[indexPath.section][indexPath.row].pickpal_cash_money == 0{
                            
                            
                            cell.containerPagos.visibility = .gone
                            
                        }else{
                            
                            cell.containerPagos.visibility = .visible
                        }
                        
                        
                        
                        //PUNTOS MOVILES
                        
                        if pedidos[indexPath.section][indexPath.row].points > 0{
                            
                            cell.lblPointsUsed.visibility = .visible
                            cell.titlePonitsUsed.visibility = .visible
                            
                            cell.lblPointsUsed.text = "-\(CustomsFuncs.numberFormat(value: pedidos[indexPath.section][indexPath.row].points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: pedidos[indexPath.section][indexPath.row].money_puntos ?? 0))"
                            
                            
                        }else{
                            cell.lblPointsUsed.visibility = .gone
                            cell.titlePonitsUsed.visibility = .gone
                        }
                        
                        
                        //PICPAL CASH
                        
                   
                        if pedidos[indexPath.section][indexPath.row].pickpal_cash_money > 0{
                                  
                            cell.lblTotalCash.visibility = .visible
                            cell.lblTextCash.visibility = .visible
                            cell.lblTotalCash.text = "\(CustomsFuncs.getFomatMoney(currentMoney: pedidos[indexPath.section][indexPath.row].pickpal_cash_money!))"
                            
                            
                            
                        }else{
                                
                            cell.lblTotalCash.visibility = .gone
                            cell.lblTextCash.visibility = .gone
                            
                        }
                        
                        
                        // TOTAL PAGADO
                        
                        let totalPagado = pedidos[indexPath.section][indexPath.row].pickpal_cash_money + pedidos[indexPath.section][indexPath.row].money_puntos
                        
                        if pedidos[indexPath.section][indexPath.row].pickpal_cash_money > 0 || pedidos[indexPath.section][indexPath.row].money_puntos > 0 {
                            
                            
                            cell.lblTitleTotalPagado.visibility = .visible
                            cell.lblTotalPagado.visibility = .visible
                            
                            cell.lblTotalPagado.text = "\(CustomsFuncs.getFomatMoney(currentMoney: totalPagado))"
                            
                            
                        }else{
                            
                            cell.lblTitleTotalPagado.visibility = .gone
                            cell.lblTotalPagado.visibility = .gone
 
                        }
                        
                        
                        //TOTAL RESTANTE POR PAGAR
                        
                        
                        if (subTotal - totalPagado) > 0{
                            
                            cell.lblTitleRestantePagar.visibility = .visible
                            cell.lblTotalRestantePagar.visibility = .visible
                            
                            cell.lblTotalRestantePagar.text =  "\(CustomsFuncs.getFomatMoney(currentMoney: (subTotal - totalPagado)))"
                            
                        }else{
                            
                            cell.lblTitleRestantePagar.visibility = .gone
                            cell.lblTotalRestantePagar.visibility = .gone
                            
                        }
                        
                        
                        
                       
                        
                        
                        cell.buttonConditions.tag = indexPath.row
                        cell.buttonConditions.addTarget(self, action: #selector(self.conditionsToPay(_:)), for: .touchUpInside)
                        var folio = pedidos[indexPath.section][indexPath.row].folio_complete!
                        var foliFormate = ""
                        var index = 0
                        for char in folio {
                            
                            if index <= 3 {
                                
                                foliFormate += String (char)
                                index = index + 1
                            }else{
                                
                                foliFormate += "-" + String(char)
                                index = 1
                            }
                        }
                        cell.labelFolioCompleto.text = foliFormate//" \(String(describing:  pedidos[indexPath.section][indexPath.row].folio_complete!))"
                        cell.establishment_name.text = pedidos[indexPath.section][indexPath.row].establishment_name
                        
                        let formatter = NumberFormatter()
                        formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                        formatter.numberStyle = .currency
                        //                    let total = subtotal + Double(myorders[indexPath.row].pickpal_service)!
                        //            if let formattedTipAmount = formatter.string(from: Double(myorders[indexPath.row].total)! as NSNumber) {
                        //                cell.total.text = "\(formattedTipAmount)"
                        //            }String(format: "%.2f", tip)
                        
                        
                        
                        if let formattedTipAmount = formatter.string(from: pedidos[indexPath.section][indexPath.row].pickpal_comission as NSNumber) {
                        
                            cell.pickPalService.text = "\(formattedTipAmount)"
                        }
          
                        
                        var dateString = pedidos[indexPath.section][indexPath.row].register_date ?? "2020-01-01" + " 00:00:00"
                        dateString = dateString + " 00:00:00"
                        let formatDate = DateFormatter()
                        formatDate.dateFormat = "dd-MM-yyyy HH:mm:ss"
                        let stringToDate = formatDate.date(from: dateString)
                        let formatString = DateFormatter()
                        formatString.dateFormat = "dd/MM/yyyy"
                        
                        if stringToDate != nil {
                        let dateToString = formatString.string(from: stringToDate!)
                        cell.dateLabel.text = dateToString
                            
                        }else{
                            
                            cell.dateLabel.text = pedidos[indexPath.section][indexPath.row].register_date!
                            
                        }
                        
                        //cell.lblSendOrder.isHidden = true
                        //cell.lblSendOrderValue.isHidden = true
                        //cell.btnConditionOrders.isHidden = true
                        //cell.lblConditionOrders.isHidden = true
                        cell.addresDelivery.visibility = .gone
                        cell.titleAddres.visibility = .gone
                        switch pedidos[indexPath.section][indexPath.row].order_type {
                        case "RB"://RB - Recoger en Barra //SM - servicio a Mesa
                            cell.modeLabel.text = pedidos[indexPath.section][indexPath.row].order_type == "SM" ? "Servicio a mesa \(pedidos[indexPath.section][indexPath.row].number_table!)" : "Recoger en barra"
                            cell.labelTitleToPay.text = "¡Ve a pagar tu pedido!"
                            cell.labelTextQR.text = "Paga y recoge tu pedido mostrando este Código QR"
                            cell.propinaLabel.visibility = .visible
                            cell.tilePropina.visibility = .visible
                            cell.titleCostoEnvio.visibility = .gone
                            cell.costoEnvio.visibility = .gone
                            
                          
                            
                            break
                            
                        case "SM"://RB - Recoger en Barra //SM - servicio a Mesa
                            cell.modeLabel.text = pedidos[indexPath.section][indexPath.row].order_type == "SM" ? "Servicio a mesa \(pedidos[indexPath.section][indexPath.row].number_table!)" : "Recoger en barra"
                            cell.labelTitleToPay.text = "¡Ve a pagar tu pedido!"
                            
                            if !pedidos[indexPath.section][indexPath.row].allow_process_nopayment && !pedidos[indexPath.section][indexPath.row].two_devices{
                                
                                
                                cell.labelTextQR.text = "Muestrá este código QR al mesero al recibir tu pedido."
                            }else{
                                
                                cell.labelTextQR.text = "Paga tu pedido mostrando este Código QR"
                                
                            }
                            
                          
                            cell.propinaLabel.visibility = .visible
                            cell.tilePropina.visibility = .visible
                            cell.titleCostoEnvio.visibility = .gone
                            cell.costoEnvio.visibility = .gone
                            break
                            
                        case "RS"://RS - Recoger en Sucursal
                            cell.labelTitleToPay.text = "¡Paga tu pedido al recogerlo!"
                            cell.labelTextQR.text = "Paga y recoge tu pedido mostrando este Código QR"
                            cell.modeLabel.text = "Recoger en Sucursal"
                            //cell.btnConditionOrders.isHidden = false
                            //cell.lblConditionOrders.isHidden = false
                            
                            //cell.ctlTopBtnConditions.constant = 8
                            //cell.ctlTopLblConditions.constant = 16
                            cell.propinaLabel.visibility = .gone
                            cell.tilePropina.visibility = .gone
                            cell.tienes15minutos.visibility = .visible
                            cell.tilePropina.visibility = .gone
                            cell.propinaLabel.visibility = .gone
                            cell.titleCostoEnvio.visibility = .gone
                            cell.costoEnvio.visibility = .gone
                            //cell.tienes15minutos.textColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
                            let pagado = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                            cell.tienes15minutos.text = "Hora estimada de entrega: \(pagado)"
                            
                            break
                        case "SD"://SD - Servicio a Domicilio
                            
                            cell.modeLabel.text = "Servicio a Domicilio"
                            cell.addresDelivery.visibility = .visible
                            cell.titleAddres.visibility = .visible
                            cell.propinaLabel.visibility = .visible
                            cell.tilePropina.visibility = .visible
                            cell.addresDelivery.text = pedidos[indexPath.section][indexPath.row].address
                            
//                            cell.labelTitleToPay.text = "¡Paga tu pedido al recibirlo!"
                            
                            if pedidos[indexPath.section][indexPath.row].statusItem == "ready"{
                                cell.labelTitleToPay.text = "¡Paga tu pedido al recibirlo!"
                            }else{
                                cell.labelTitleToPay.text = ""
                            }
                            
                            cell.labelTextQR.text = "Proporciona este número de folio al repartidor"
                            cell.tienes15minutos.visibility = .visible
                            //  cell.tienes15minutos.textColor = UIColor.init(red: 38/255, green: 213/255, blue: 117/255, alpha: 1)
                            let pagado = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                            cell.tienes15minutos.text = "Hora estimada de entrega: \(pagado)" //pedidos[indexPath.section][indexPath.row].statusItem
                            cell.titleCostoEnvio.visibility = .visible
                            cell.costoEnvio.visibility = .visible
                            if pedidos[indexPath.section][indexPath.row].shipping_cost != 0{
                                
                                cell.titleCostoEnvio.visibility = .visible
                                cell.costoEnvio.visibility = .visible
                                cell.costoEnvio.text = "$\(pedidos[indexPath.section][indexPath.row].shipping_cost!)"
                                
                            }else{
                                
                                cell.titleCostoEnvio.visibility = .gone
                                cell.costoEnvio.visibility = .gone
                                
                            }
                            
                            break
                            
                        default:
                            print("Sin opciones")
                            
                        }
                        
                        
                        /*if pedidos[indexPath.section][indexPath.row].in_site {
                         cell.modeLabel.text = "Servicio a mesa \(0)"
                         }else{
                         cell.modeLabel.text = "Para llevar"
                         }*/
                        
                        cell.indexPath = indexPath
                        if pedidos[indexPath.section][indexPath.row].qr != "" {
                            Nuke.loadImage(with: URL(string: pedidos[indexPath.section][indexPath.row].qr)!, into: cell.qrImage)
                        }
                        //                cell.gifImage.loadGif(name: "GifProcess"
                  
                        
                        let totalPedido = (pedidos[indexPath.section][indexPath.row].total > 0 ? pedidos[indexPath.section][indexPath.row].total:pedidos[indexPath.section][indexPath.row].pickpal_cash_money) ?? 0
                        
                        if pedidos[indexPath.section][indexPath.row].percent > 0{
                        if let formattedTipAmount = formatter.string(from: (pedidos[indexPath.section][indexPath.row].percent) as NSNumber) {
                            let percent = Int(round((Float(pedidos[indexPath.section][indexPath.row].percent) / Float(totalPedido)) * 100))
                            cell.propinaLabel.text = "\(percent)%  \(formattedTipAmount)"
                        }
                        }else{
                            
                            cell.propinaLabel.text = ""
                        }
                        
                        if let orderNumber = pedidos[indexPath.section][indexPath.row].order_number {
                            if  orderNumber != "" {
                                cell.orderNumber.text = pedidos[indexPath.section][indexPath.row].folio + "/" + orderNumber
                            }else{
                                
                                cell.orderNumber.text = pedidos[indexPath.section][indexPath.row].folio
                            }
                            
                        }else{
                            cell.orderNumber.text = pedidos[indexPath.section][indexPath.row].folio
                        }
                        cell.notes.text = pedidos[indexPath.section][indexPath.row].notes
                        
                        if pedidos[indexPath.section][indexPath.row].notes == ""{
                            cell.titleNotas.visibility = .gone
                        }else{
                            cell.titleNotas.visibility = .visible
                        }
                        
//                        let pedidoProcesado = pedidos[indexPath.section][indexPath.row].register_date! + " " + pedidos[indexPath.section][indexPath.row].pay_time!
                        
                       
                     
                        if pedidos[indexPath.section][indexPath.row].confirmed {
                            
                            activateNotifications = !pedidos[indexPath.section][indexPath.row].allow_process_nopayment && pedidos[indexPath.section][indexPath.row].order_type != "SD" && pedidos[indexPath.section][indexPath.row].confirmed
                            
                        }else{
                            
                            cell.tienes15minutos.text = "Pedido por Confirmar"
                            
                            activateNotifications = false
                        }
                      
                       
                        
                        if  activateNotifications{
                            let pedidoProcesado = pedidos[indexPath.section][indexPath.row].register_date! + " " + pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00"
//                            sendNotificationsToPay(order_id: pedidos[indexPath.section][indexPath.row].order_id, establishment_name:  pedidos[indexPath.section][indexPath.row].establishment_name)
                            let currentDay = Date() //
                            let formaTTer = DateFormatter()
                            formaTTer.dateFormat = "dd-MM-yyyy HH:mm:ss"
                            formaTTer.locale = Locale(identifier: "es_MX")
                            let dateOrderRegister = formaTTer.date(from: pedidoProcesado) as! Date // 2:25
                            print(dateOrderRegister, "pedido procesado")
                            let calendar = Calendar.current
                           
                            let tenminutes = calendar.date(byAdding:.minute, value: 10, to: dateOrderRegister)! // 2:35 5 minutes left
                            let fiveminutes = calendar.date(byAdding:.minute, value: 5, to: dateOrderRegister)! //2:30 10minutes left
                            print(tenminutes, "pedido procesado")
                            print(fiveminutes, "pedido procesado")
                            print(currentDay, "pedido procesado")
                            
                            let dateNow = Date()
                            var calendarNow = Calendar.current
                            let secons = calendar.date(byAdding:.second, value: 2, to: dateNow)! // 2:35 5 minutes left
                            
                            print(dateNow, "Notificacion now")
                            print(dateOrderRegister, "Notificacion register")
                            print( secons == dateOrderRegister, "Notificacion register")
                            
                       
                            
                            if pedidos[indexPath.section][indexPath.row].sendNotifications{
                                
                                sendNotificationsToPay(order_id: pedidos[indexPath.section][indexPath.row].order_id, establishment_name:  pedidos[indexPath.section][indexPath.row].establishment_name)
                                
                                 DatabaseFunctions.shared.updateStatusNotifications(idOrder: pedidos[indexPath.section][indexPath.row].order_id, sendNotifications: false)
                                
                            }
                            
                            
                            if currentDay < fiveminutes {
                                cell.tienes15minutos.text = "¡Tienes 15 minutos!"
                            }else if currentDay < tenminutes {
                                cell.tienes15minutos.text = "¡Tienes 10 minutos!"
                            }else{
                                cell.tienes15minutos.text = "¡Tienes 5 minutos!"
                            }
                            
                        }else{
               
                            if !pedidos[indexPath.section][indexPath.row].allow_process_nopayment && pedidos[indexPath.section][indexPath.row].order_type != "SD"{
                                
                                cell.tienes15minutos.text = "Pedido por confirmar"
                                
                            }else{
                                
                                let pagado = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                                cell.tienes15minutos.text = "Hora estimada de entrega: \(pagado)"
                            }
                               
                        }
                        
                        var tempNameArray = [String]()
                        var tempPriceArray = [Double]()
                        var tempCantArray = [Int]()
                        var tempModifier = [Bool]()
                        var tempCategory = [String]()
                        let order_ele = pedidos[indexPath.section][indexPath.row].order_elements!
                        cell.elements = order_ele
                        //                    print(order_ele)
                        for i in 0..<order_ele.count {
                            var priceModifiers = Double()
                            var priceComplements = Double()
                            if let hasModifiers = (order_ele[i] as NSDictionary).value(forKey: "complex") as? Bool {
                                if hasModifiers {
                                    if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                        tempNameArray.append(name)
                                        tempPriceArray.append(price)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(false)
                                        tempCategory.append("")
                                        tempNameArray.append(name)
                                        tempPriceArray.append(Double(i) + 100000)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(true)
                                        tempCategory.append("")
                                    }
                                }else{
                                    if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                        tempNameArray.append(name)
                                        tempPriceArray.append(price)
                                        tempCantArray.append(quantity)
                                        tempModifier.append(false)
                                        tempCategory.append("")
                                    }
                                }
                                
                            }else{
                                if let name = (order_ele[i] as NSDictionary).value(forKey: "name") as? String, let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double, let quantity = (order_ele[i] as NSDictionary).value(forKey: "quantity") as? Int{
                                    tempNameArray.append(name)
                                    tempPriceArray.append(price)
                                    tempCantArray.append(quantity)
                                    tempModifier.append(false)
                                    tempCategory.append("")
                                }
                            }
  
                            if let complements = ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as? [String]{
                                //                        if let comp = complements.value(forKey: "complement_price") as? [Double] {
                                tempPriceArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]//complements.value(forKey: "complement_price") as! [Double]
                                tempCantArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_cant") as! [Int]//complements.value(forKey: "complement_cant") as! [Int]
                                tempNameArray += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_name") as! [String]//complements.value(forKey: "complement_name") as! [String]
                                print(order_ele[i] as NSDictionary)
                                tempCategory += ((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_category") as! [String]
                                for i in 0..<(((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).count {
                                    tempModifier.append(true)
                                }
                                priceComplements = (((order_ele[i] as NSDictionary).value(forKey: "complements") as? NSDictionary)?.value(forKey: "complement_price") as! [Double]).reduce(0, +)
                                //                        }
                            }
                            if let modifiers = (order_ele[i] as NSDictionary).value(forKey: "modifiers") as? NSArray {
                                for modi in modifiers{
                                    tempNameArray += (modi as! NSDictionary).value(forKey: "modifier_name") as! [String]
                                    tempPriceArray += (modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]
                                    tempCategory += (modi as! NSDictionary).value(forKey: "modifier_category") as! [String]
                                    for i in 0..<((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).count {
                                        tempCantArray.append(1)
                                        tempModifier.append(true)
                                    }
                                    priceModifiers = ((modi as! NSDictionary).value(forKey: "modifier_price") as! [Double]).reduce(0, +)
                                }
                            }
                            if let price = (order_ele[i] as NSDictionary).value(forKey: "price") as? Double {
                                for prices in tempPriceArray {
                                    let indice = tempPriceArray.index(of: Double(i) + 100000)
                                    if indice != nil {
                                        tempPriceArray[indice!] = price - priceModifiers - priceComplements
                                    }
                                }
                            }
                        }
                        //                print(order_ele)
                        cell.modifier_name = tempNameArray
                        cell.modifier_cant =  tempCantArray
                        cell.modifier_price = tempPriceArray
                        cell.isModifier = tempModifier
                        cell.modifier_cat = tempCategory
                        
                        if  pedidos[indexPath.section][indexPath.row].order_type != "SD"{
                            
                            if pedidos[indexPath.section][indexPath.row].allow_process_nopayment {
                               
                                cell.labelTitleToPay.text = "¡Paga tu pedido al recogerlo!"
                            }else{
                               
                                
                                cell.labelTitleToPay.text = "¡Ve a pagar tu pedido!"
                            }
                            
                            
                        }else{
                            
                            if !pedidos[indexPath.section][indexPath.row].allow_process_nopayment {
                               
                                cell.labelTitleToPay.text = "¡Ve a pagar tu pedido!"
                            }else{
                                
                                cell.labelTitleToPay.text = ""
                                
                            }
                            
                            
                        }
                        
                        //                        if  pedidos[indexPath.section][indexPath.row].order_type != "SD" {
                                                   
                        if !pedidos[indexPath.section][indexPath.row].allow_process_nopayment{
                            
                            cell.lblPayAlert.visibility = .visible
                            cell.cancelarPedidoBtn.visibility = .visible
                                                        
                                                        
                               
                        }else{

                            if !pedidos[indexPath.section][indexPath.row].confirmed && !pedidos[indexPath.section][indexPath.row].two_devices {

                                cell.lblPayAlert.visibility = .visible
                                cell.cancelarPedidoBtn.visibility = .visible
                                

                            }else{

                               
                                
                                cell.lblPayAlert.visibility = .gone
                                cell.cancelarPedidoBtn.visibility = .gone

                            }
                            
                        }
   
                        if pedidos[indexPath.section][indexPath.row].order_type != "RS" && pedidos[indexPath.section][indexPath.row].percent == 0{
                            
                            cell.labelMensajePropina.visibility = .visible
                            
                            
                        }else{
                            
                            cell.labelMensajePropina.visibility = .gone
                            
                        }
                        
                        if pedidos[indexPath.section][indexPath.row].percent != 0{
                            
                            cell.propinaLabel.visibility = .visible
                            cell.tilePropina.visibility = .visible
                            
                        }else{
                            cell.propinaLabel.visibility = .gone
                            cell.tilePropina.visibility = .gone
                            
                        }
                        
                        
                        
                        
                        
                        
                        //Oscar Timer Line Pedido por  Pagar
                        /*
                         horaPagado Pedido Pagado
                         
                         Hora proceso
                         
                         Hora Listo
                         */
                        
                        if pedidos[indexPath.section][indexPath.row].confirmed{
                            
                            cell.horaConfirmarPedidoPorPagar.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour ?? "00:00:00")
                            cell.tituloConfirmarPedidoPorPagar.text = "Pedido confirmado"
                           
                            let pagado = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time ?? "00:00:00")
                            cell.tienes15minutos.text = "Hora estimada de entrega: \(pagado)"
   
                        }else{
                            
                            cell.horaConfirmarPedidoPorPagar.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                            cell.tituloConfirmarPedidoPorPagar.text = "Pedido por confirmar"
                            cell.tienes15minutos.text = "Pedido por confirmar"
                            
                            
                        }
                        
                        cell.tienes15minutos.textColor =  UIColor.init(red: 203/255, green: 29/255, blue: 62/255, alpha: 1)
                        switch pedidos[indexPath.section][indexPath.row].statusController {
                        
                        case "new":

                            cell.horaRecibido.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                            
                            cell.labelHoraPorPagarEnproceso.text = ""
                            cell.porPagarEnProceso.text = ""
                            cell.horaPorPagarListo.text = ""
                            cell.porPagarListo.text = ""
                            
                            
                        case "to_pay":
                            
                            cell.horaRecibido.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                            cell.labelHoraPorPagarEnproceso.text = ""
                            cell.porPagarEnProceso.text = ""
                            cell.horaPorPagarListo.text = ""
                            cell.porPagarListo.text = ""
                            
                            
                        case "in_process", "delayed", "delivery_delayed": // en proceso y listo
                            
                            cell.horaRecibido.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                            cell.labelHoraPorPagarEnproceso.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour!)
                            cell.porPagarEnProceso.text = "Pedido en proceso"
                            cell.horaPorPagarListo.text = ""
                            cell.porPagarListo.text = ""
                            
                           
                            
                        case "ready":
                          
                            cell.horaRecibido.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].pay_time!)
                            cell.labelHoraPorPagarEnproceso.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour!)
                            cell.porPagarEnProceso.text = "Pedido en proceso"
                            
                            if pedidos[indexPath.section][indexPath.row].notified{
                        
                            cell.horaPorPagarListo.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].est_ready_hour!)
                            cell.porPagarListo.text = "Pedido listo"
                            cell.tienes15minutos.textColor = UIColor.init(red: 28/255, green: 215/255, blue: 134/255, alpha: 1)
                                if pedidos[indexPath.section][indexPath.row].number_table > 0 {
                                    cell.tienes15minutos.text = "Pedido listo en mesa \(pedidos[indexPath.section][indexPath.row].number_table.description)"

                                }else{
                                    
                                    cell.tienes15minutos.text = "Pedido listo"
                                }
                                cell.mensajePropina.text = "Pedido Listo"
//                                openAlertOrdenLista(establishName: pedidos[indexPath.section][indexPath.row].establishment_name)
                            }else{
                                
                                cell.labelHoraPorPagarEnproceso.text = DatabaseFunctions.shared.getHourPay(pagado: pedidos[indexPath.section][indexPath.row].confirm_hour!)
                                cell.porPagarEnProceso.text = "Pedido en proceso"
                                cell.horaPorPagarListo.text = ""
                                cell.porPagarListo.text = ""
                                
                                
                            }
                            
                        default:
                            
                            cell.horaRecibido.text = ""
                            cell.labelRecibidoPorPagar.text = ""
                            cell.labelHoraPorPagarEnproceso.text = ""
                            cell.porPagarEnProceso.text = ""
                            cell.horaPorPagarListo.text = ""
                            cell.porPagarListo.text = ""
                            
                        }
                        
                        if pedidos[indexPath.section][indexPath.row].is_paid{
                            cell.labelRecibidoPorPagar.text = "Pedido pagado"
                            
                        }else{
                            if pedidos[indexPath.section][indexPath.row].confirmed{
                                
                            }else{
                                
                            }
                            cell.labelRecibidoPorPagar.text = "Pedido recibido"
                        }
                        
                        cell.selectionStyle = .none
                        

                        
                       
                        
                        return cell
                        
                    }
                    
                }else{
                    return UITableViewCell()
                }
                
            }else{
                
                return UITableViewCell()
            }
            
        }
      
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   
        if tableView == self.tableView {
          
            var heightDelivery = myorders[indexPath.row].type_order == "SD" ? 90 : 40
            heightDelivery  += Int(sizeTotales)
            return CGFloat(440 + (allItemsOrder[indexPath.row].count * 30) + heightDelivery + heightRefund) //Josue Valdez.- Se agregan 90 puntos

        }else{
      
            if self.indexPath == indexPath {
//                print("showpedido dentro: max: \(dinamicHeight) seleccion:  \(indexPath.section) row: \(indexPath.row) openFlag: \(openFlag)")
                return dinamicHeight

            }else{
//                print("showpedido dentro: min: \(dinamicHeightMinimizada) seleccion: \(indexPath.section) row: \(indexPath.row) openFlag: \(openFlag)")
            
                    return 70//dinamicHeightMinimizada[indexPath.section][indexPath.row]
//                return Minimizada
                
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == orderTableView {
            return 49.5
        }else{
            return 0
        }
    }
//    Cambios relizados sse cambio el if, y la forma de mostrar el numeor de pedidos (ambos cambios estan comentados)
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = PedidosHeadersViewController()
        if pedidos.count > 0 {
            if pedidos[section].count > 0 {
                // if pedidos[section][0].statusItem == "to_pay" {
                if !pedidos[section][0].is_paid {
                    header.pedidosString = "Pedidos por pagar"
                    header.pedidosNumber = "\(DatabaseFunctions.shared.getStatus().1.count)"
//                    header.pedidosNumber = "\(pedidos[section].count)"
                    
                    print("Orden por pagar \(DatabaseFunctions.shared.getStatus().1.count)")
//                }else if pedidos[section][0].statusItem == "in_process" || pedidos[section][0].statusItem == "ready"  || pedidos[section][0].statusItem == "delayed" || pedidos[section][0].statusItem == "delivery_delayed" {
                }else   if pedidos[section][0].is_paid {
                    header.pedidosString = "Pedidos pagados"
                    header.pedidosNumber = "\(DatabaseFunctions.shared.getStatus().2.count)"
//                    header.pedidosNumber = "\(pedidos[section].count)"
                    print("Orden pagado \(DatabaseFunctions.shared.getStatus().2.count)")
                }else{
                    header.pedidosString = "Pedidos Por Reembolso"
                    header.pedidosNumber = "\(DatabaseFunctions.shared.getStatus().3.count)"
//                    header.pedidosNumber = "\(pedidos[section].count)"
                    print("Orden en reembolso \(DatabaseFunctions.shared.getStatus().3.count)")
                    
                }
            }
        }
        return header.view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        self.indexPath = indexPath
       
        if tableView == orderTableView {
            tableView.beginUpdates()
            
            itemSelectType = pedidos[indexPath.section][indexPath.row].order_type
            itemSelectIsPay = pedidos[indexPath.section][indexPath.row].is_paid
            
            
            
            goToServiceClient = pedidos[indexPath.section][indexPath.row].active_customer_service
                
                orderServiceClient = pedidos[indexPath.section][indexPath.row]
           
//            print("showpedido en el didSelectRow", indexPath.section,indexPath.row,openFlag, dinamicHeightMinimizada)
            print("showpedido en el didSelectRow: section: \(indexPath.section) row: \(indexPath.row) openFlag: \(openFlag) dinamicHeightMinimizada: \(dinamicHeightMinimizada)")
            if !indexPath.isEmpty {
                if openFlag.count > 0 {
                    //                    print(openFlag.count, pedidos.count, indexPath.section, indexPath.row)
                    if openFlag.count == pedidos.count {
                        if openFlag[indexPath.section].count == pedidos[indexPath.section].count {
                            if !openFlag[indexPath.section][indexPath.row]  {
                                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                                
                                if let cell = tableView.cellForRow(at: indexPath) as? PorPagarTableViewCell {
                                    //                                    print(cell.modifier_name.count * 25, cell.tableViewHeightConstraint.constant, "tamaño tabla en por pagar")
                                    cell.dropdown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
//                                    cell.tableViewHeightConstraint.constant = CGFloat(cell.modifier_name.count * 25)
//                                    cell.tableViewHeightConstraint.constant =   CGFloat( CGFloat(cell.modifier_name.count) * cell.dinamicHeightAxu)
                                    
                                    cell.viewMiPedido.frame.size.height = (360 + cell.tableViewHeightConstraint.constant)
                                    cell.viewLarge.frame.size.height = (917.5 + cell.tableViewHeightConstraint.constant)
                                    dinamicHeight = 60 + (cell.viewCellConstraint.constant - 100)
                                    openFlag[indexPath.section][indexPath.row] = true
//                                    self.indexPath = indexPath
                                   
                                    cell.establishment_name.lineBreakMode = .byWordWrapping
                                    cell.establishment_name.numberOfLines = 0
//                                    dinamicHeightMinimizada[indexPath.section][indexPath.row] = cell.establishment_name.frame.height + 50
                                    
                                    cell.viewCellConstraint.constant = CGFloat(cell.modifier_name.count * 45)
                                    switch pedidos[indexPath.section][indexPath.row].order_type {
                                    case "SM", "RB"://RB - Recoger en Barra //SM - servicio a Mesa
//                                        cell.viewPedidosHeight.constant = 356 + cell.tableViewHeight.constant
                                        cell.viewCellConstraint.constant = 1000 + 140
                                        break
                                        
                                    case "RS"://RS - Recoger en Sucursal
//                                        cell.viewPedidosHeight.constant = 356 + cell.tableViewHeight.constant + 50
                                        cell.viewCellConstraint.constant = 1000 + 140
                                        
                                        break
                                    default://SD - Servicio a Domicilio
//                                        cell.viewPedidosHeight.constant = 356 + cell.tableViewHeight.constant + 140
                                        cell.viewCellConstraint.constant = 1000 + 170
                                        break
                                    }
                                    
                                    dinamicHeight = 110 + (cell.viewCellConstraint.constant - 100) +  CGFloat(cell.modifier_name.count * 25) + 140
                                    cell.tableView.endUpdates()
                                }
                                
                                
                                // MARK: - Oscar Listado de Pagados
                                if let cell = tableView.cellForRow(at: indexPath) as? PagadosTableViewCell {
                                    //                                    print(cell.modifier_name.count * 25, cell.tableViewHeight.constant, "tamaño tabla en pagados")
//                                    cell.dinamicHeightAxu = 0
                                    cell.dropdown.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
//                                    cell.tableViewHeight.constant = cell.dinamicHeightAxu
                                    cell.viewCellConstraint.constant =   CGFloat( CGFloat(cell.modifier_name.count) * 45)
                                    
//                                    cell.tableView.reloadData()
                                    
                                    switch pedidos[indexPath.section][indexPath.row].order_type {
                                    case "SM", "RB"://RB - Recoger en Barra //SM - servicio a Mesa
                                        cell.viewPedidosHeight.constant = 386 + cell.tableViewHeight.constant + 140
                                        cell.viewCellConstraint.constant = 1000 + cell.tableViewHeight.constant + 20  + 140
                                        break
                                        
                                    case "RS"://RS - Recoger en Sucursal
                                        cell.viewPedidosHeight.constant = 386 + cell.tableViewHeight.constant + 50 + 140
                                        cell.viewCellConstraint.constant = 968.5 + cell.tableViewHeight.constant + 70  + 140
                                        
                                        break
                                    default://SD - Servicio a Domicilio
                                        cell.viewPedidosHeight.constant = 386 + cell.tableViewHeight.constant + 140 + 140
                                        cell.viewCellConstraint.constant = 968.5 + cell.tableViewHeight.constant + 110  + 140
                                        break
                                    }
                                    
                                    dinamicHeight = 110 + (cell.viewCellConstraint.constant - 100)
//                                    dinamicHeightMinimizada[indexPath.section][indexPath.row] = cell.establishmentName.frame.height + 50
                                    openFlag[indexPath.section][indexPath.row] = true
                                    cell.establishmentName.lineBreakMode = .byWordWrapping
                                    cell.establishmentName.numberOfLines = 0
//                                    self.indexPath = indexPath
                                    //
                                }
                                if let cell = tableView.cellForRow(at: indexPath) as? ReembolsadosTableViewCell {
                                    //                                    print(cell.modifier_name.count * 25, cell.tableViewConstraint.constant, "tamaño tabla en reembolsos")
                                    cell.buttonExpand.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                                    cell.tableViewConstraint.constant = CGFloat(cell.modifier_name.count * 25)
                                    cell.pedidosViewConstraint.constant = 376 + /*(396 + */cell.tableViewConstraint.constant/*)*/ //CGFloat(cell.modifier_name.count * 10)
                                    cell.viewTotalConstraint.constant = 703 + cell.tableViewConstraint.constant //CGFloat(cell.modifier_name.count * 10)
                                    //                                    print(cell.tableViewConstraint.constant)
                                    dinamicHeight = 60 + (cell.viewTotalConstraint.constant - 100)
                                    openFlag[indexPath.section][indexPath.row] = true
                                    
                                }
                                
                                
                            }else{
                                if let cell = tableView.cellForRow(at: indexPath) as? PorPagarTableViewCell {
                                    cell.dropdown.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                                    cell.establishment_name.lineBreakMode = .byWordWrapping
                                    cell.establishment_name.numberOfLines = 1
                                }else{
                                    print("No es por pagar")
                                }
                                if  let cell = orderTableView.cellForRow(at: indexPath) as? PagadosTableViewCell {
                                    cell.dropdown.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                                    cell.establishmentName.lineBreakMode = .byWordWrapping
                                    cell.establishmentName.numberOfLines = 1
                                }else{
                                    print("No es  Pagado")
                                }
                                if let cell = tableView.cellForRow(at: indexPath) as? ReembolsadosTableViewCell {
                                    cell.buttonExpand.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                                }
                                
                                //Tamaño minimizado
                                if let cell = tableView.cellForRow(at: indexPath) as? PorPagarTableViewCell {
                                    
                                    dinamicHeightMinimizada[indexPath.section][indexPath.row] = cell.establishment_name.frame.height + 50
                                }
                                
                                
                                if let cell = tableView.cellForRow(at: indexPath) as? PagadosTableViewCell {
                                    
                                    dinamicHeightMinimizada[indexPath.section][indexPath.row] = cell.establishmentName.frame.height + 50
                                    
                                }
                                
                                dinamicHeight = 70
                                openFlag[indexPath.section][indexPath.row] = false
                            }
                        }
                    }
                }
                
            }
            
            print("showpedido en el didSelectRow 2: section: \(indexPath.section) row: \(indexPath.row) openFlag: \(openFlag) dinamicHeightMinimizada: \(dinamicHeightMinimizada)")

            tableView.endUpdates()
           
        }else{
            
            self.orderIdRefundDetails = myorders[indexPath.row].order_id
            
        }
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func didSuccessGetViewOrder(status: String) {
        //        print(status)
    }
    func didFailGetViewOrder(error: String, subtitle: String) {
        print(error, "didFailGetViewOrder")
    }
    func didSuccessGetPickPalComision(comision: Double) {
    }
    func didFailGetPickPalComision(title: String, subtitle: String) {
        comission = 0
    }
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
    func formatterHour(hour: String)-> String{
        
        let form = DateFormatter()
        form.dateFormat = "HH:mm:ss"
        let datePay = form.date(from: hour)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm aa"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        var dateString = String()
        if datePay == nil {
            dateString = ""
        }else{
            dateString = formatter.string(from: datePay!)
        }
//        print(dateString, "dateString")
        return dateString
    
    }
    
    @objc func conditionsToPay(_ sender: UIButton){
        switch itemSelectType {
            
        case "RS":
            
            let newXIB = AlertConditionsTwoViewController(nibName: "AlertConditionsTwoViewController", bundle: nil)
            
            newXIB.primerTexto = "1. Debes de pagar tu pedido una vez notificado. De lo contario se cancelará."
            newXIB.segundoTexto = ""
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
        case "SD":
            
            let newXIB = AlertConditionsTwoViewController(nibName: "AlertConditionsTwoViewController", bundle: nil)
            
            newXIB.primerTexto = "1. Paga tu pedido al repartidor una vez recibido."
            newXIB.segundoTexto = ""
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            self.present(newXIB, animated: true, completion: nil)
            
            
            
        default:
            print("Sin opciones")
        }
    }
    
    @objc func getRefundDetails(_ sender: UIButton){
        
        print("Nueva funcion reembolso orden \(myorders[sender.tag].order_id)")
            ordersWS.getRefundDetails(idOrder: myorders[sender.tag].order_id)
    
    }
    
    
     func didSuccessGetRefundDetails(refundDetails:RefundDetails){
        
        print("info remmbolso: \(refundDetails)")
        
        let newXIB = AlertInfoRefundDetails(nibName: "AlertInfoRefundDetails", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.refundDetails = refundDetails
        present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func didFailGetRefundDetails(error: String, subtitle: String){
        
        print("Error al obtener info remmbolso: \(error)")
    }
        
    
    func openAlertOrdenLista(establishName: String){
        
        let readyView = OrdenListaViewController()
        readyView.modalTransitionStyle = .crossDissolve
        readyView.modalPresentationStyle = .overCurrentContext
        readyView.establishName = establishName
        readyView.delegate = self
        self.present(readyView, animated: true, completion: nil)
        
        
    }
    
    func sendNotificationsToPay(order_id: Int, establishment_name: String){
        
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                print("15 minutes has passed")
                print(order_id)
                 if  DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay"  {
                    self.ordersWS.changeOrderStatus(order_id:order_id, status: 7)
                }
            })
            
        }
        
        
        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 300, repeats: false)
        let content1 = UNMutableNotificationContent()
        let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content1.title = "¡Ve a pagar tu Pedido a \(establishment_name)!"
        content1.body = "Paga tu pedido mostrando tu código QR, tienes 10 minutos. ¡Corre! haz clic aquí para ver los detalles..."
        content1.sound = UNNotificationSound.default()
        content1.sound = UNNotificationSound(named: "pedidolisto.wav")
        content1.userInfo = ["establishment_name": establishment_name, "order_id": order_id, "notification_on": false]
        let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
        center.delegate = self
        center.add(request1, withCompletionHandler: nil)
        
        
        let trigge3 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        let content3 = UNMutableNotificationContent()
        
        let establecimiento1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            content3.title = "¡Ve a pagar tu Pedido a \(establishment_name)!"
            content3.body = "Paga tu pedido mostrando tu código QR, tienes 5 minutos. ¡Corre! haz clic aquí para ver los detalles..."
            content3.sound = UNNotificationSound.default()
            content3.userInfo = ["establishment_name": establishment_name, "order_id": order_id, "notification_on": false]
            content3.sound = UNNotificationSound(named: "pedidolisto.wav")
        let request3 = UNNotificationRequest(identifier: "\(order_id)-", content: content3, trigger: trigge3)
        center.delegate = self
       center.add(request3, withCompletionHandler: nil)
        
        let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let establishment_id1 = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        let cDate = Date()
        let format = DateFormatter()
        format.dateFormat = "dd_MM_yyyy"
        let currentDate = format.string(from: cDate)
        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
//        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let content2 = UNMutableNotificationContent()
        content2.title = "Pedido cancelado"
//        content2.body = "Lo sentimos, el tiempo para pagar tu pedido en \(establecimiento) ha expirado."
        content2.body = "Haz click aquí si deseas ordenar nuevamente TU PEDIDO de \(establecimiento)"
        content2.sound = UNNotificationSound(named: "pedidolisto.wav")
        content2.userInfo = ["establishment_name": establishment_name, "order_id": order_id, "notification_on": true, "establishment_id": "-\(establishment_id1)", "register_date": currentDate]
        let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
        content2.categoryIdentifier = "caducado"
        center.delegate = self
        center.add(request2, withCompletionHandler: nil)
        
        
        UNUserNotificationCenter.current().add(request1) { (error) in
            if let error = error {
                print("Se ha producido un error request1: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request2) { (error) in
            if let error = error {
                print("Se ha producido un error request2: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request3) { (error) in
            if let error = error {
                print("Se ha producido un error request3: \(error)")
            }
        }
        
    }
   
}
