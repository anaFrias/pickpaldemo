//
//  NotaAdicionalViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/21/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

class NotaAdicionalViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var campoTexto: UITextView!
    @IBOutlet weak var imageEstablishment: UIImageView!
    var image = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        campoTexto.delegate = self
        imageEstablishment.layer.cornerRadius = imageEstablishment.frame.height / 2
        imageEstablishment.clipsToBounds = true
        campoTexto.text = ""
        imageEstablishment.layer.masksToBounds = true
        Nuke.loadImage(with: URL(string: image)!, into: imageEstablishment)
        campoTexto.layer.cornerRadius = 10
        if let note = UserDefaults.standard.object(forKey: "nota_comercio") as? String {
            campoTexto.text = note
        }

        // Do any additional setup after loading the view.
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 100    // 100 Limit Value
    }
    @IBAction func `continue`(_ sender: Any) {
        UserDefaults.standard.set(campoTexto.text!, forKey: "nota_comercio")
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
