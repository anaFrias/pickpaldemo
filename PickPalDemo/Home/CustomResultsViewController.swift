//
//  CustomResultsViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 28/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import CoreLocation
import SQLite
import OneSignal

class CustomResultsViewController: UIViewController, establishmentDelegate {
    
    @IBOutlet weak var numPedido: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var noContent: UIView!
    var valueState = String()
    var valueCity = String()
    var ws = EstablishmentWS()
    var values = [Establishment]()
    var defaults = UserDefaults.standard
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        if locationManager.location != nil {
            ws.getFastEstablishments2(latitud: /*20.5880600*/(locationManager.location?.coordinate.latitude)!, longitud: /*-100.3880600*/(locationManager.location?.coordinate.longitude)!)
            LoadingOverlay.shared.showOverlay(view: self.view)
        }else{
            values.removeAll()
        }
        topBar.layer.shadowColor = UIColor.gray.cgColor
        topBar.layer.shadowOpacity = 1
        topBar.layer.shadowOffset = CGSize.zero
        topBar.layer.shadowRadius = 5
        noContent.alpha = 0
        tableView.alpha = 1
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedido.alpha = 1
            }else{
                numPedido.alpha = 0
            }
        }else{
            numPedido.alpha = 0
        }
//        ws.getFastEstablishments(state: "QUE", municipality: "Queretaro")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didSuccessGetFastEstablshments(info: [Establishment]) {
        LoadingOverlay.shared.hideOverlayView()
        values = info
        noContent.alpha = 0
        tableView.alpha = 1
        tableView.reloadData()
//        var ids = [Int]()
//        for inf in info {
//            ids.append(inf.id)
//        }
//        print(ids)
//        ws.getEstablishmentsById(ids: ids)
    }
    func didSuccessGetFastEstablshments1(ids: [Int]) {
        if ids.count > 0 {
             ws.getEstablishmentsById(ids: ids)
        }else{
            LoadingOverlay.shared.hideOverlayView()
            noContent.alpha = 1
            tableView.alpha = 0
        }
    }
    func didSuccessGetEstablshmentsById(info: [Establishment]) {
        LoadingOverlay.shared.hideOverlayView()
        values = info
        noContent.alpha = 0
        tableView.alpha = 1
        tableView.reloadData()
    }
    func didFailGetEstablshmentsById(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        noContent.alpha = 1
        tableView.alpha = 0
    }
    func didFailGetFastEstablshments(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        noContent.alpha = 1
        tableView.alpha = 0
    }
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension CustomResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "establishments", for: indexPath) as! EstablishmentsTableViewCell
        cell.establishmentName.text = values[indexPath.row].name
        if values[indexPath.row].future_available! {
            cell.establishmentImage.image = UIImage(named: "comingSoon")
            cell.bluredImage.image = UIImage(named:"comingSoon")
        }else{
            if let logo = values[indexPath.row].logo, logo != "" {
                Nuke.loadImage(with: URL(string: logo)!, into: cell.establishmentImage)
                Nuke.loadImage(with: URL(string: logo)!, into: cell.bluredImage)
            }else{
                cell.establishmentImage.image = UIImage(named: "placeholderImageRed")
                cell.bluredImage.image = UIImage(named:"placeholderImageRed")
            }
        }
        cell.subtitleLabel.text = values[indexPath.row].place + " - " + values[indexPath.row].category
        cell.txtTime.text = values[indexPath.row].time + " min"
        cell.viewTime.layer.cornerRadius = 10
        cell.viewTime.clipsToBounds = true
        cell.viewTime.layer.masksToBounds = true
        cell.viewTime.layer.shadowColor = UIColor.black.cgColor
        cell.viewTime.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.viewTime.layer.shadowOpacity = 0.24
        cell.viewTime.layer.shadowRadius = CGFloat(2)
        if values[indexPath.row].is_open {
            cell.statusLabel.text = "Abierto"
            cell.noAvaliable.alpha = 0
        }else{
            if values[indexPath.row].future_available! {
                cell.statusLabel.text = "Próximamente"
                cell.noAvaliable.alpha = 0
            }else{
                cell.statusLabel.text = "Cerrado"
                cell.noAvaliable.alpha = 1
            }
            
        }
        cell.rate(rating: values[indexPath.row].rating)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 235
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = values[indexPath.row].id
        newVC.municipioStr = valueCity
        newVC.categoriaStr = values[indexPath.row].category
        newVC.lugarString = values[indexPath.row].place
        newVC.establString = values[indexPath.row].name
        defaults.set(values[indexPath.row].id, forKey: "establishment_id")
            //            defaults.set(values[indexPath.row].name, forKey: "establishment_name")
            self.navigationController?.pushViewController(newVC, animated: true)
    }
    
}
