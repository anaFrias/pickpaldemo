//
//  RefundTroubleHelpViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class RefundTroubleHelpViewController: UIViewController, sendInfoRefundsDelegate, userDelegate, closeModalDelegate {

    @IBOutlet weak var tableView: UITableView!
    var orderId = Int()
    var userws = UserWS()
    var isFromPromos:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func sendInfoRefunds(text: String) {
        if text != "" {
            let clientId = UserDefaults.standard.object(forKey: "client_id") as! Int
            userws.registerHelp2(client_id: clientId, descriptionText: text, category: 6, order: orderId)
        }else{
            let alert = UIAlertController(title: "Campo faltante", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func close() {
        self.navigationController?.popViewController(animated: true)
    }

    func didSuccessRegisterHelp() {
        let newXIB = SuccedMessageHelpViewController(nibName: "SuccedMessageHelpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true) {
            
        }
    }
    func didFailRegisterHelp(title: String, subtitle: String) {
        let error = ErrorViewController()
        error.modalTransitionStyle = .crossDissolve
        error.modalPresentationStyle = .overCurrentContext
        error.errorMessageString = title
        error.subTitleMessageString = subtitle
        self.present(error, animated: true, completion: nil)
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
extension RefundTroubleHelpViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ayudaCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoDetalleCell", for: indexPath) as! RefundSendInfoTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
            
        }
    }
}
