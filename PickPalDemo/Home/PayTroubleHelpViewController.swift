//
//  PayTroubleHelpViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/26/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class PayTroubleHelpViewController: UIViewController, UITextViewDelegate, userDelegate, closeModalDelegate {

    @IBOutlet weak var placeholder: UILabel!
    @IBOutlet weak var textView: UITextView!
    var userws = UserWS()
    var rutePathImage = String()
    var orderId = Int()
    var image = UIImage()
    var isFromPromos:Bool = false
    
    @IBOutlet weak var imagenPedido: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        textView.delegate = self
        textView.layer.borderColor = UIColor.init(red: 173/255, green: 180/255, blue: 185/255, alpha: 1.0).cgColor
        textView.layer.cornerRadius = 4.5
        textView.layer.borderWidth = 0.5
        // Do any additional setup after loading the view.
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != "" {
            placeholder.alpha = 0
        }else{
            placeholder.alpha = 1
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
         placeholder.alpha = 0
    }
    @IBAction func sendInfo(_ sender: Any) {
        if rutePathImage != "" || textView.text != "" {
            let clientId = UserDefaults.standard.object(forKey: "client_id") as! Int
            userws.registerHelp2(client_id: clientId, descriptionText: textView.text, category: 1, order: orderId, file: rutePathImage)
        }else{
            let alert = UIAlertController(title: "Campo faltante", message: "Todos los campos son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    func didSuccessRegisterHelp() {
        let newXIB = SuccedMessageHelpViewController(nibName: "SuccedMessageHelpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true) {
            
        }
    }
    
    func didFailRegisterHelp(title: String, subtitle: String) {
        let error = ErrorViewController()
        error.modalTransitionStyle = .crossDissolve
        error.modalPresentationStyle = .overCurrentContext
        error.errorMessageString = title
        error.subTitleMessageString = subtitle
        self.present(error, animated: true, completion: nil)
    }
    
    @IBAction func uploadImage(_ sender: Any) {
        showActionSheet()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PayTroubleHelpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cámara", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Carrete", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openGallery()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let picker = UIImagePickerController()
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            
            self.present(picker, animated: true, completion: nil)
        }else {
            self.openGallery()
        }
    }
    
    func openGallery() {
        let pickerGallery = UIImagePickerController()
        pickerGallery.delegate = self
        pickerGallery.sourceType = .photoLibrary
        self.present(pickerGallery, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //        LoadingOverlay.shared.showOverlayWithMessage(view: self.view, message: "Subiendo..")
        
        image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
        imagenPedido.image = image
        imagenPedido.alpha = 1
        let uuid = UUID().uuidString
        let myImageName = "imagen" + uuid + ".png"
        let imagePath = fileInDocumentsDirectory(myImageName)
        _ = saveImage((info[UIImagePickerControllerOriginalImage] as? UIImage)!, path: imagePath)
        //        print("\(path)/" + myImageName)
        //        if isPromotionalCode {
        rutePathImage = "\(path)/" + myImageName
        //        }else{
        //            savedCheck = "\(path)/" + myImageName
        //        }
        print(imagePath)
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        _ = UIImagePNGRepresentation(image)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
        
    }
    
}
