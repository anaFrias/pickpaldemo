//
//  SingleNewsFeedViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/4/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import OneSignal

class SingleNewsFeedViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var titleNewsFeed = String()
    var image = String()
    var content = String()
    var category = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func homeBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func pedidosBnt(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores")
        newVC.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settingsBtn(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }

}
extension SingleNewsFeedViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as! ImageNewsFeedTableViewCell
            cell.selectionStyle = .none
            if(image != ""){
                Nuke.loadImage(with: URL(string: image)!, into: cell.imageNewsFeed)
            }
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! TitleNewsFeedTableViewCell
            cell.category.text = category
            cell.selectionStyle = .none
            cell.title1.text = titleNewsFeed
            cell.title2.text = titleNewsFeed
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "contentCell", for: indexPath) as! ContentNewsFeedTableViewCell
            cell.titleLabel.text = titleNewsFeed
            cell.selectionStyle = .none
            cell.contentLabel.text = content
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 163
        }else if indexPath.row == 1 {
            return 77
        }else {
            return 320.5
        }
    }
}
