//
//  NewsFeedViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 13/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import OneSignal
import CoreLocation

class NewsFeedViewController: UIViewController, userDelegate, googleWSDelegate, CLLocationManagerDelegate  {

    @IBOutlet weak var numPedidos: UIView!
    @IBOutlet weak var tableView: UITableView!
    var newsArray = [News]()
    var userWS = UserWS()
    var last_id = Int()
    var state = String()
    var isFromPromos: Bool = false
    var isFromGuide: Bool = false
    var locationManager = CLLocationManager()
    var myCurrentLocation:CLLocationCoordinate2D!
    var gws = GoogleServices()
    
    @IBOutlet weak var header: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userWS.delegate = self
        gws.delegate = self
//        userWS.getNews()
//        userWS.getNewsNew(state: state)
        LoadingOverlay.shared.showOverlay(view: self.view)
        header.layer.cornerRadius = 5
        header.clipsToBounds = true
        header.layer.masksToBounds = true
        header.layer.shadowColor = UIColor.black.cgColor
        header.layer.shadowOffset = CGSize(width: 0, height: 2)
        header.layer.shadowOpacity = 0.24
        header.layer.shadowRadius = CGFloat(2)
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done {
                numPedidos.alpha = 1
            }else{
                numPedidos.alpha = 0
            }
        }else{
            numPedidos.alpha = 0
        }
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            let clocation = locationManager.location
            if clocation?.coordinate.latitude == nil{
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                    //                    locationManager.requestAlwaysAuthorization()
                    locationManager.requestWhenInUseAuthorization()
                }
            }else{
                let userLat = (clocation?.coordinate.latitude)!//19.4326//CDMX
                let userLng = (clocation?.coordinate.longitude)!//-99.13330000000002//CDMX
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
            }
        }
    }
    
    func didSuccessGetGeolocation(location: String, state: String) {
        let diacritics = state.folding(options: .diacriticInsensitive, locale: .current)
        userWS.getNewsNew(state: diacritics)
    }
    func didFailGetGeolocation(title: String, subtitle: String) {
        print(title)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = isFromPromos
                newVC.isFromGuide = isFromGuide
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = isFromGuide
            newVC.isFromGuide = isFromGuide
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        UserDefaults.standard.removeObject(forKey: "age")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        newVC.isFromGuide = isFromGuide
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            if isFromGuide{
                let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
                let navController = UINavigationController(rootViewController: newVC)
                navController.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }
            
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSuccessGetNews(news: [News]) {
        LoadingOverlay.shared.hideOverlayView()
        newsArray = news
        if let lastId = newsArray.first?.id {
            last_id = lastId
        }
        if let client_id = UserDefaults.standard.object(forKey: "client_id") as? Int {
            userWS.setReadNewsFeedLog(client_id: client_id, last_id_news_feed: last_id)
        }
        tableView.reloadData()
    }
    func didFailGetNews(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if title != "204" {
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    func didSuccessSetNewsFeedLog() {
        print("done")
    }
    func didFailSetNewsFeedLog(title: String, subtitle: String) {
        print("imposible settear el log")
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension NewsFeedViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsFeedBig") as! NewsFeedBigTableViewCell
            cell.content.text = newsArray[indexPath.row].content
            cell.title.text = newsArray[indexPath.row].title
            cell.category.text = newsArray[indexPath.row].category
            if(newsArray[indexPath.row].big_image != ""){
                Nuke.loadImage(with: URL(string: newsArray[indexPath.row].big_image!)!, into: cell.img)
                Nuke.loadImage(with: URL(string: newsArray[indexPath.row].big_image!)!, into: cell.blur)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsFeed") as! NewsFeedTableViewCell
            cell.content.text = newsArray[indexPath.row].content
            cell.title.text = newsArray[indexPath.row].title
            cell.category.text = newsArray[indexPath.row].category
            if(newsArray[indexPath.row].image != ""){
                Nuke.loadImage(with: URL(string: newsArray[indexPath.row].image!)!, into: cell.img)
                Nuke.loadImage(with: URL(string: newsArray[indexPath.row].image!)!, into: cell.blur)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 246
        }else{
            return 139
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let currentCell = tableView.cellForRow(at: indexPath) as? NewsFeedBigTableViewCell {
            if (currentCell.content.text?.count)! > 170 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "singleNews") as! SingleNewsFeedViewController
                newVC.content = newsArray[indexPath.row].content
                newVC.category = newsArray[indexPath.row].category
                newVC.titleNewsFeed = newsArray[indexPath.row].title
                newVC.image = newsArray[indexPath.row].image
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }else {
            let currentCell = tableView.cellForRow(at: indexPath) as! NewsFeedTableViewCell
            if (currentCell.content.text?.count)! > 170 {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "singleNews") as! SingleNewsFeedViewController
                newVC.content = newsArray[indexPath.row].content
                newVC.category = newsArray[indexPath.row].category
                newVC.titleNewsFeed = newsArray[indexPath.row].title
                newVC.image = newsArray[indexPath.row].image
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }
        
        
    }
}
