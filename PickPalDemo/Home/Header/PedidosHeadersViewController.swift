//
//  PedidosHeadersViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 10/29/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class PedidosHeadersViewController: UIViewController {
    
    @IBOutlet weak var numberOf: UILabel!
    @IBOutlet weak var pedidosLabel: UILabel!
    
    var pedidosString = String()
    var pedidosNumber = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberOf.text = pedidosNumber
        pedidosLabel.text = pedidosString
        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
