//
//  VerificarFacturacionViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/18/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
import iOSDropDown

class VerificarFacturacionViewController: UIViewController, UITextFieldDelegate, userDelegate, facturaEnviadaDelegate, salirDelegate {

    
    @IBOutlet weak var correoTextView: UITextField!
    @IBOutlet weak var razonSocialTextView: UITextField!
    @IBOutlet weak var RFCTextView: UITextField!
    @IBOutlet weak var indicatorRFC: UIImageView!
    @IBOutlet weak var indicatorRazon: UIImageView!
    @IBOutlet weak var indicatorCorreo: UIImageView!
    @IBOutlet weak var tfCFDI: DropDown!
    @IBOutlet weak var tfZipCode: UITextField!
    @IBOutlet weak var indicatorCFDI: UIImageView!
    @IBOutlet weak var indicatorZipCode: UIImageView!
    //    Validation flags
    var isCorrectEmail = Bool()
    var isCorrectName = Bool()
    var isCorrectRFC = Bool()
    var isCorrectZipCode = Bool()
    var isCorrectCFDI = Bool()
    
    var taxRegimenId: Int = 0
    
    var userws = UserWS()
    var orderId = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        RFCTextView.delegate = self
        razonSocialTextView.delegate = self
        correoTextView.delegate = self
        tfZipCode.delegate = self
        tfCFDI.delegate = self
        RFCTextView.addTarget(self, action: #selector(uppercaseRFC(_:)), for: .editingChanged)
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.RetryBilligGet(order_id: orderId)
        // Do any additional setup after loading the view.
    }
    
    func requestTaskRegimen(){
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.getTaxRegimenList()
    }
    
    func didSuccessGetTaskRegimen(regimenList: [TaxRegimenDTO]) {
        LoadingOverlay.shared.hideOverlayView()
        
        self.tfCFDI.text = findTaxRegime(taxRegime: self.taxRegimenId , regimenList: regimenList)
        
        var taxNames = [String]()
        for data in regimenList{
            taxNames.append(data.value)
        }
        tfCFDI.optionArray = taxNames
        
        tfCFDI.didSelect{(selectedText , index ,id) in
            self.taxRegimenId = regimenList[index].key
        }
        
        
        
    }
    
    func findTaxRegime(taxRegime: Int, regimenList: [TaxRegimenDTO]) -> String{
        var tr = ""
        for data in regimenList{
            if taxRegime == data.key{
                tr = data.value
                break
            }
        }
        
        return tr
    }
    
    func didFailGetTaskRegimen(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didSuccessRetryBillingGet(rfc: String, email: String, full_name: String, zip_code: String, tax_regime_key: Int)  {
//        Asignar valores
        LoadingOverlay.shared.hideOverlayView()
        RFCTextView.text = rfc
        razonSocialTextView.text = full_name
        correoTextView.text = email
        if email != "" {
            isCorrectEmail = true
        }
        if full_name != "" {
            isCorrectName = true
        }
        if rfc != "" {
            isCorrectRFC = true
        }
        
        if zip_code != ""{
            isCorrectZipCode = true
        }
        
        if tax_regime_key != 0{
            self.taxRegimenId = tax_regime_key
            isCorrectCFDI = true
            requestTaskRegimen()
        }
    }
    
    func facturaEnviada() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        newVC.showSplash = false
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailRetryBillingGet(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessRetryBillingSet() {
        let newXIB = FactuaExitosoViewController(nibName: "FactuaExitosoViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        present(newXIB, animated: true, completion: nil)
    }
    
    func didFailRetryBillingSet(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfCFDI{
            if taxRegimenId != 0{
                isCorrectCFDI = true
                indicatorCFDI.alpha = 1
                indicatorCFDI.image = UIImage(named: "correctIcon")
            }else{
                isCorrectCFDI = false
                indicatorCFDI.alpha = 1
                indicatorCFDI.image = UIImage(named: "wrongIcon")
            }
        }else if textField == tfZipCode{
            if textField.text != "" {
                isCorrectZipCode = true
                indicatorZipCode.alpha = 1
                indicatorZipCode.image = UIImage(named: "correctIcon")
            }else{
                isCorrectZipCode = false
                indicatorZipCode.alpha = 1
                indicatorZipCode.image = UIImage(named: "wrongIcon")
            }
        }else if textField == RFCTextView {
            if textField.text != "" {
                if isValidRFC(testStr: textField.text!) {
                    isCorrectRFC = true
                    indicatorRFC.alpha = 1
                    indicatorRFC.image = UIImage(named: "correctIcon")
                }else{
                    isCorrectRFC = false
                    indicatorRFC.alpha = 1
                    indicatorRFC.image = UIImage(named: "wrongIcon")
                }
            }else{
                isCorrectRFC = false
                indicatorRFC.alpha = 1
                indicatorRFC.image = UIImage(named: "wrongIcon")
            }
        }else if textField == razonSocialTextView {
            if textField.text != "" {
                isCorrectName = true
                indicatorRazon.alpha = 1
                indicatorRazon.image = UIImage(named: "correctIcon")
            }else{
                isCorrectName = false
                indicatorRazon.alpha = 1
                indicatorRazon.image = UIImage(named: "wrongIcon")
            }
        }else{
            if textField.text != "" {
                if isValidEmail(testStr: textField.text) {
                    isCorrectEmail = true
                    indicatorCorreo.alpha = 1
                    indicatorCorreo.image = UIImage(named: "correctIcon")
                }else{
                    isCorrectEmail = false
                    indicatorCorreo.alpha = 1
                    indicatorCorreo.image = UIImage(named: "wrongIcon")
                }
            }else{
                isCorrectEmail = false
                indicatorCorreo.alpha = 1
                indicatorCorreo.image = UIImage(named: "wrongIcon")
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == tfCFDI {
            indicatorCFDI.alpha = 0
        }else if textField == tfZipCode{
            indicatorZipCode.alpha = 0
        }else if textField == RFCTextView {
            indicatorRFC.alpha = 0
        }else if textField == razonSocialTextView {
            indicatorRazon.alpha = 0
        }else{
            indicatorCorreo.alpha = 0
        }
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == RFCTextView {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }else{
            return true
        }
    }
    
    @objc func uppercaseRFC (_ textField: UITextField) {
        RFCTextView.text = textField.text?.uppercased()
    }
    
    func isValidRFC(testStr:String) -> Bool {
        guard testStr != nil else { return false }
        
        let result = matches(for: "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[0-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\\d]{3})?$", in: testStr)
        if result.count > 0 {
            if result[0] == testStr {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    @IBAction func volverAFacturar(_ sender: Any) {
        if isCorrectEmail && isCorrectName && isCorrectRFC && isCorrectCFDI && isCorrectZipCode{
            userws.RetryBilligSet(
                order_id: orderId,
                RFC: RFCTextView.text!,
                name: razonSocialTextView.text!,
                email: correoTextView.text!,
                zipCode: tfZipCode.text ?? "",
                taxRegimen: self.taxRegimenId)
        }
    }
    
    @IBAction func HomeButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func pedidosButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func backButton(_ sender: Any) {
        let salir = SalirFacturaViewController()
        salir.delegate = self
        salir.modalTransitionStyle = .coverVertical
        salir.modalPresentationStyle = .overCurrentContext
        self.present(salir, animated: true, completion: nil)
    }
    func salirFactura() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
