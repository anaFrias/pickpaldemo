//
//  EstablishmentsViewController.swift
//  PickPalTesting
//
//  Created by josue valdez on 5/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol EstPointDelegate {
    func OnResultEst(ids: [RewardsMobile])
}

class EstablishmentsViewController: UIViewController, EstTransferDelgate, PointsWSDelegate {
    
    @IBOutlet weak var tableEstablishment: UITableView!
    @IBOutlet weak var containerSearchEst: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    
    var multiplesIdsSelected = [RewardsMobile]()
    var mListEstablishments = [RewardsMobile]()
    var mListEstablishmentsFilter = [RewardsMobile]()
    var idsSelected = [RewardsMobile]()
    var mCounter = 0
    var delegate: EstPointDelegate!
    var isOneEst = true
     var pointWs = PointsWS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        containerSearchEst.layer.borderWidth = 0.5
        containerSearchEst.layer.borderColor = UIColor.black.cgColor
        containerSearchEst.layer.cornerRadius = 5
        mListEstablishmentsFilter = mListEstablishments
        
        
     //   pointWs.getUserEstablishmentsPoints();
        
        for data in mListEstablishmentsFilter{
            
            data.isSelect = false
            
        }
        
        for data in mListEstablishmentsFilter{
            if data.isSelect{
                onSelectEst(id: data.establishment_id, select: true)
                break
            }
        }
    }
    
    @IBAction func OnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func OnContinue(_ sender: Any) {
        if idsSelected.count > 0{
            for data in multiplesIdsSelected{
                if idsSelected[0].establishment_id == data.establishment_id {
                   showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "El comercio \(idsSelected[0].establishment_name ?? "") ha sido previamente seleccionado para obtener puntos")
                }
            }
            
//            print(idsSelected)
            self.navigationController?.popViewController(animated: true)
            self.delegate.OnResultEst(ids: idsSelected)
        }else{
            showMessage(title: "PickPal Puntos Móviles", message: "Debes de seleccionar un establecimiento")
        }
    }
    
    func showMessage(title:String, message: String){
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.isFromAdd = true
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func OnTextWatcher(_ sender: Any) {
        mListEstablishmentsFilter.removeAll()
        if tfSearch.text != ""{
            for data in mListEstablishments{
                if data.establishment_name.lowercased().hasPrefix(tfSearch.text?.lowercased() ?? ""){
                    mListEstablishmentsFilter.append(data)
                }
            }
            tableEstablishment.reloadData()
        }else{
            mListEstablishmentsFilter = mListEstablishments
            tableEstablishment.reloadData()
        }
    }
    
    
    func onSelectEst(id: Int, select: Bool) {
        if isOneEst{
            for data in mListEstablishments {
                if id == data.establishment_id {
                    idsSelected.removeAll()
                    data.isSelect = true
                    self.idsSelected.append(data)
                    
                }else{
                    data.isSelect = false
                }
            }
            tableEstablishment.reloadData()
        }else{
            for data in mListEstablishments {
                if id == data.establishment_id{
                    if !select{
                        if mCounter > 0{
                            data.isSelect = false
                            mCounter = mCounter - 1
                            var index = 0
                            for idS in idsSelected{
                                if id == idS.establishment_id{
                                    self.idsSelected.remove(at: index)
                                    break
                                }
                                index += 1
                            }
                            tableEstablishment.reloadData()
                            break
                        }
                    }else{
                        if mCounter <= 2{
                            data.isSelect = true
                            self.idsSelected.append(data)
                            mCounter += 1
                            tableEstablishment.reloadData()
                            break
                        }
                    }
                }
            }
        }
    }
}

extension EstablishmentsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mListEstablishmentsFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = mListEstablishmentsFilter[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "transfer") as! EstTransferTableViewCell
        cell.selectionStyle = .none
        
        cell.delegate = self
        cell.mid = data.establishment_id
        cell.isSelect = data.isSelect
        
        cell.lblName.text = "\(data.state ?? "") - \(data.establishment_name ?? "") | \(data.place ?? "")"
        cell.imgIcon.image = mListEstablishments[indexPath.row].isSelect ? UIImage(named: "fillRoundedCheckBox") : UIImage(named: "emptyRoundedCheckBox")
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

extension EstablishmentsViewController: AlertAdsDelegate{
    
    func deletePromoById(id: Int){
        
    }
    
    func closeDialogAndExit(toHome: Bool){
        
    }
}
