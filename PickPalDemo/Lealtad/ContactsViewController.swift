//
//  ContactsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Contacts

protocol ContactsLealtadDelegate {
    func onContactDelegate(name: String, phone: String)
}

class ContactsViewController: UIViewController, ContactsPointsDelegate, AlertAdsDelegate {
    
    @IBOutlet weak var tableContacts: UITableView!
    @IBOutlet weak var SearchContacs: UISearchBar!
    @IBOutlet weak var viewContainerSearch: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    
    var contactsList = [ContactsTransfer]()
    var contactsListFilter = [ContactsTransfer]()
    var fetchedContacts : [CNContact] = []
    var phoneNumber = ""
    var nameContact = ""
    var delegate: ContactsLealtadDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainerSearch.layer.borderWidth = 0.5
        viewContainerSearch.layer.borderColor = UIColor.black.cgColor
        viewContainerSearch.layer.cornerRadius = 5
        
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            presentSettingsAlert()
            return
        }
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                self.presentSettingsAlert()
                return
            }
            
            let request = CNContactFetchRequest(keysToFetch: [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor])
            do {
                try store.enumerateContacts(with: request) { contact, stop in
                    let name = CNContactFormatter.string(from: contact, style: .fullName)
//                    print(name as Any)
                    
                    for phone in contact.phoneNumbers {
                        var label = phone.label
                        if label != nil {
                            label = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label!)
                        }
//                        print("  ", label ?? "", phone.value.stringValue)
                        
                        let myFriend = ContactsTransfer()
                        myFriend.phone = phone.value.stringValue
                        myFriend.name = name
                        myFriend.isSelect = false
                        self.contactsList.append(myFriend)
                        self.contactsListFilter.append(myFriend)
                    }
                    self.tfSearch.placeholder = "\(self.contactsList.count) contactos"
                }
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func OnTextWatcher(_ sender: Any) {
        contactsListFilter.removeAll()
        if tfSearch.text != ""{
            for data in contactsList{
                if let name = data.name{
                    if name.lowercased().hasPrefix(tfSearch.text?.lowercased() ?? "") || data.phone.lowercased().hasPrefix(tfSearch.text?.lowercased() ?? ""){
                        contactsListFilter.append(data)
                    }
                }else{
                    if data.phone.lowercased().hasPrefix(tfSearch.text?.lowercased() ?? ""){
                        contactsListFilter.append(data)
                    }
                }
                
            }
            tableContacts.reloadData()
        }else{
            contactsListFilter = contactsList
            tableContacts.reloadData()
        }
    }
    
    private func presentSettingsAlert() {
        let settingsURL = URL(string: UIApplicationOpenSettingsURLString)!
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "PickPal", message: "Necesita acceso a tus contactos para transferir puntos moviles", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ir a ajustes", style: .default) { _ in
                UIApplication.shared.openURL(settingsURL)
            })
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel))
            self.present(alert, animated: true)
        }
    }
    
    
    @IBAction func OnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func OnTransferPoints(_ sender: Any) {
        if (phoneNumber == "" && nameContact == "") || phoneNumber.count < 10{
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            if phoneNumber.count < 10{
                newXIB.mMessage = "El número teléfonico seleccionado no tiene un formato válido"
            }else{
                newXIB.mMessage = "Debes de seleccionar un contacto"
            }
            
            newXIB.mTitle = "PickPal Puntos Móviles"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
        }else{
            //Mandar mensaje de ux
            self.navigationController?.popViewController(animated: true)
            delegate.onContactDelegate(name: self.nameContact, phone: self.phoneNumber)
        }
    }
    
    func onSelectUser(number: String, name: String) {
        /*self.phoneNumber = number
        self.nameContact = name
        print(phoneNumber)
        print(nameContact)
        
        for data in contactsList{
            if data.name == name && data.phone == number{
                data.isSelect = true
            }else{
                data.isSelect = false
            }
        }
        
        tableContacts.reloadData()*/
        
        self.navigationController?.popViewController(animated: true)
        delegate.onContactDelegate(name: name, phone: number)
    }
    
    func deletePromoById(id: Int){
        
    }
    
    func closeDialogAndExit(toHome: Bool){
        
    }
}

extension ContactsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsListFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsPoints") as! ContactsPointsTableViewCell
        let data = contactsListFilter[indexPath.row]
        cell.selectionStyle = .none
        cell.delegate = self
        if let name = data.name{
            cell.name = name
        }else{
            cell.name = ""
        }
        
        cell.phone = data.phone
        
        cell.lblName.text = data.name
        cell.lblNumberPhone.text = data.phone
        
        if data.isSelect {
            cell.backgroundColor = .lightGray
        }else{
            cell.backgroundColor = .white
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
