//
//  MultipleEstsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/18/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol MultipleEstsDelegate {
    func OnSetMultipleEst(ids: [RewardsMobile])
}

class MultipleEstsViewController: UIViewController, EstTransferDelgate, PointsWSDelegate, LoadMoreEstDelegate {
    
    @IBOutlet weak var tableEst: UITableView!
    @IBOutlet weak var sbSearch: UISearchBar!
    @IBOutlet weak var containerSearchMultiple: UIView!
    @IBOutlet weak var tfSearch: UITextField!
    
    var mListEstablishments = [RewardsMobile]()
    var mListEstablishmentsFilter = [RewardsMobile]()
    var mListEstablishmentsAllAux = [RewardsMobile]()
    var idsSelected = [RewardsMobile]()
    var mCounter = 0
    var delegate: MultipleEstsDelegate!
    var pointWs = PointsWS()
    var idPrevious = 0
    var valueState = ""
    var isFirstTime: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let reward = RewardsMobile()
        reward.establishment_id = 0
        mListEstablishments.append(reward)
        pointWs.delegate = self
        //Prueba 2
        containerSearchMultiple.layer.borderWidth = 0.5
        containerSearchMultiple.layer.borderColor = UIColor.black.cgColor
        containerSearchMultiple.layer.cornerRadius = 5
        
       // LoadingOverlay.shared.showOverlay(view: self.view)
       // pointWs.getAllEstablishmentAround()
        
        
        var index = 0
        for list in mListEstablishments{
        
            if list.establishment_id == idPrevious{
        
                mListEstablishments.remove(at: index)
            
            }
            
            index += 1
        }
        mListEstablishmentsAllAux = mListEstablishments
        index = 0
        for list in mListEstablishments{
        
            if list.state != nil && list.state != valueState{
        
                mListEstablishments.remove(at: index)
            
            }
            
            index += 1
        }

        mListEstablishments.append(reward)
         mListEstablishmentsFilter = mListEstablishments
        
        mListEstablishmentsFilter = mListEstablishmentsFilter.removingDuplicates()
        
        for data in mListEstablishmentsFilter {
                       
            if data.isSelect{
//            self.idsSelected.append(data)
                
                data.isSelect = false
            }
        }
        
//        mCounter = idsSelected.count
    
    }
    
    @IBAction func onCloseView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onContinue(_ sender: Any) {
        /*var i = 0
        for data in idsSelected{
           if idPrevious == data.establishment_id{
               idsSelected.remove(at: i)
            break
            }
            i+=1
        }*/
        
        if idsSelected.count > 0{
            print(idsSelected)
            self.navigationController?.popViewController(animated: true)
            self.delegate.OnSetMultipleEst(ids: idsSelected)
        }else{
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "Debes de seleccionar al menos un establecimiento"
            newXIB.mTitle = "PickPal Puntos Móviles"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
        }
    }
    
   
    @IBAction func OnTextWatcher(_ sender: Any) {
        mListEstablishmentsFilter.removeAll()
        if tfSearch.text != ""{
            for data in mListEstablishments{
                if let name = data.establishment_name{
                    if name.lowercased().hasPrefix(tfSearch.text?.lowercased() ?? ""){
                        mListEstablishmentsFilter.append(data)
                    }
                }
                
            }
            tableEst.reloadData()
        }else{
            mListEstablishmentsFilter = mListEstablishments
            /*if !isFirstTime{
                mListEstablishmentsFilter.removeLast()
            }*/
            tableEst.reloadData()
        }
    }
    
    func removeEstPrevious(){
//        print(mListEstablishments)
        /*for index in 0...mListEstablishments.count{
            if idPrevious == mListEstablishments[index].establishment_id{
                mListEstablishments.remove(at: index)
                break
            }
        }*/
    }
    
    func onSelectEst(id: Int, select: Bool) {
        for data in mListEstablishmentsFilter {
            if id == data.establishment_id{
                if !select{
                    if mCounter > 0{
                        data.isSelect = false
                        mCounter = mCounter - 1
                        var index = 0
                        for idS in idsSelected{
                            if id == idS.establishment_id{
                                self.idsSelected.remove(at: index)
                                break
                            }
                            index += 1
                        }
                        tableEst.reloadData()
                        break
                    }
                }else{
                    if mCounter <= 2{
                        data.isSelect = true
                        self.idsSelected.append(data)
                        mCounter += 1
                        tableEst.reloadData()
                        break
                    }
                }
            }
        }
    }
    
    func onLoadMore() {
        isFirstTime = false
        
//        mListEstablishmentsAllAux = mListEstablishments
        
        let countEstablishments = mListEstablishmentsAllAux.count - 1 //le descontamos
        for i in 0...countEstablishments{
            if idPrevious == mListEstablishmentsAllAux[i].establishment_id{
                mListEstablishmentsAllAux.remove(at: i)
                break
            }
        }
        
       
        if countEstablishments == mListEstablishmentsAllAux.count {
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "No hay comercios disponibles por el momento en otros estados"
            newXIB.mTitle = "Puntos Móviles"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            mListEstablishmentsFilter.removeLast()
            tableEst.reloadData()
        }else{
            mListEstablishments.removeAll()
            
            for data in mListEstablishmentsAllAux{
                if idPrevious != data.establishment_id{
                    mListEstablishments.append(data)
                }
            }
            prepareList()
            didSuccessRewardsMobile(info: mListEstablishmentsAllAux)
        }
//        LoadingOverlay.shared.showOverlay(view: self.view)
//        pointWs.getAllEstablishmentAround()
    }
    
    func didSuccessRewardsMobile(info: [RewardsMobile]) {
        mListEstablishmentsAllAux = info
        mListEstablishments.removeAll()

        for data in info{
            if  data.state != nil{
            if data.state != self.valueState{
                if idPrevious != data.establishment_id{
                    mListEstablishments.append(data)
                    //onSelectEst(id: data.establishment_id, select: true)
                }
            }
            }
        }
        if mListEstablishments.count < 0{
            
            prepareList()
            
        }else{
            
           
            
            showDialogError(title: "Puntos Móviles", message: "No hay comercios disponibles por el momento en otros estados")
            
        }
        
    }
    
    func showDialogError(title: String, message: String) {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func prepareList(){
        //Seteamos un valor dummy para mostrar los establecimientos de otros estados
        if isFirstTime {
            let dummy = RewardsMobile()
            dummy.establishment_id = 0
            mListEstablishments.append(dummy)
            
        }
        
        removeEstPrevious()
        mListEstablishmentsFilter = mListEstablishments
        tableEst.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailGetGetPendingExchange(error: String, subtitle: String) {
        
    }
}

extension MultipleEstsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mListEstablishmentsFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = mListEstablishmentsFilter[indexPath.row]
        if data.establishment_id != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "transfer") as! EstTransferTableViewCell
            cell.selectionStyle = .none
            
            cell.delegate = self
            cell.mid = data.establishment_id
            cell.isSelect = data.isSelect
            
            cell.lblName.text = "\(data.state ?? "") - \(data.establishment_name ?? "") | \(data.place ?? "")"
            cell.lblName.isEnabled = idPrevious != data.establishment_id
            
            if idPrevious != data.establishment_id{
                cell.imgIcon.image = mListEstablishmentsFilter[indexPath.row].isSelect ? UIImage(named: "fillRoundedCheckBox") : UIImage(named: "emptyRoundedCheckBox")
                cell.btnSelectItem.isUserInteractionEnabled = true
                cell.imgIcon.tintColor = .clear
            }else{
                cell.btnSelectItem.isUserInteractionEnabled = false
                cell.imgIcon.image = UIImage(named: "emptyRoundedCheckBox")
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "load") as! LoadMoreEstTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

extension MultipleEstsViewController: AlertAdsDelegate{
    
    func deletePromoById(id: Int){
        
    }
    
    func closeDialogAndExit(toHome: Bool){
        
    }
}

