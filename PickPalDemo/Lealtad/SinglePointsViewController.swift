//
//  SinglePointsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
import GoogleMaps
import MapKit
import OneSignal
import SQLite

class SinglePointsViewController: UIViewController, PointsWSDelegate, OptionsPointsDelegate, userDelegate, SendPoinsSingleDelegate, AlertAdsDelegate, ContactsLealtadDelegate, TransferInterchangeDelegate, InterchangePointsDelegate, MultipleEstsDelegate, MKMapViewDelegate, PromosDelegate, ServicesDelegate, MapInfoDelegate, establishmentDelegate, MapsCellDelegate{
    
  
    
    func invalidPoints() {
        
    }
    
    @IBOutlet weak var SingleTableView: UITableView!
    
    var structures = [String]()
    var establishment = EstablishmentWS()
    var pointWs = PointsWS()
    var usersWs = UserWS()
    var mEstabId = 0
    var estabName = String()
    var mSingleReward = SingleReward()
    var myHistoryPoints = [HistoryRewards]()
    var listPendingExchange = [PendingExchange]()
    var mTransferComission = TransferCommission()
    var multiplesIdsSelected = [RewardsMobile]()
    var isShowingHistory = false
    var isShowingTransfer = false
    var isShowingInterchange = false
    var isShowingInterchangeInProcess = false
    var isShowingInfo = false
    var isShowingMap = false
    var isCleanFields: Bool = false
    var mRewardsMobile = RewardsMobile()
    
    var mMoneyValueToSend = 0.0
    var textToEstSelect = ""
    var textUserSelected = ""
    var textPhoneSelected = ""
    var textPointsEnter = "0"
    var newMoneyAfterSend = ""
    var newPointsAfterSend = ""
    var taxPoints = 0
    var taxMoneyPoints = 0.0
    var valueState = String()
    
    var textToMultipleEstSelect = "Establecimiento(s)"
    var moneyTaxValue = 0.0
    var valueTaxPoints = 0
    var valueInterchangePoints = 0
    var valueMoneyPointsInterchange = 0.0
    var pointsAfterGiftInterhange = 0.0
    var pointsMoneyAfterGiftInterhange = 0.0
    
    var isEditPhone: Bool = false
    
    var pmws = PromosWS()
    var countList = 0
    var dinamicHeight: CGFloat = 280
     var indexPath = IndexPath()
    var expandedIndexSet : IndexSet = []
    var tipoIntercambio = String()
    var cancelAmount = Double()
    var cancelEstablishment = String()
    var dinamicHeightFinish: CGFloat = 240
    var dinamicHeightGeneral: CGFloat = 220
    var dinamicHeightInProcess: CGFloat = 270
    var dinamicHeightInProcessMin: CGFloat = 270
    override func viewDidLoad() {
        super.viewDidLoad()
        pointWs.delegate = self
        usersWs.delegate = self
        establishment.delegate = self
        pmws.delegate = self
        SingleTableView.delegate = self
        SingleTableView.dataSource = self
        SingleTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        isShowingHistory = true //Se inicia con el historial
        LoadingOverlay.shared.showOverlay(view: self.view)
        pointWs.getSingleRewards(client_id: String(UserDefaults.standard.object(forKey: "client_id") as! Int),
                                 establishmenId: String(mEstabId))
        
        
    }
    
    @IBAction func backHome(_ sender: Any) {
        /*let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
         let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
         self.navigationController?.pushViewController(newVC, animated: true)*/
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goHome(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goLastOrders(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = false
        newVC.whereFrom = Constants.PICKPAL_PUNTOS
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goSettings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = false
                newVC.isFromGuide = false
                newVC.whereFrom = Constants.PICKPAL_PUNTOS
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = false
            newVC.isFromGuide = false
            newVC.whereFrom = Constants.PICKPAL_PUNTOS
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func goToNavigation(acttion: Int){
        switch acttion {
        case 1://FoodAndDrinks
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = self.mSingleReward.id
            newVC.municipioStr = self.mSingleReward.municipality
            newVC.categoriaStr = self.mSingleReward.establishmentCategory
            newVC.lugarString = self.mSingleReward.place
            newVC.establString = ""
            self.navigationController?.pushViewController(newVC, animated: true)
            break
            
        case 2://Promos
            LoadingOverlay.shared.showOverlay(view: self.view)
            pmws.viewSingleAdvsEstablishment(establishmentID: self.mSingleReward.id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
            break
            
        case 3://Guia
            print("hola")
            let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment_guide") as! SingleGuideCollectionViewCell
            newVC.id = self.mSingleReward.id
            newVC.municipioStr = self.mSingleReward.municipality
            newVC.categoriaStr = self.mSingleReward.establishmentCategory
            newVC.lugarString = self.mSingleReward.place
            newVC.establString = ""
            newVC.isServicePlus = mSingleReward.servicePlus
            newVC.kilometers = ""
            self.navigationController?.pushViewController(newVC, animated: true)
            break
        default: //4 default puntos
            print("Ya estamos en el single daahhhhh")
            break
        }
    }
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs) {
        LoadingOverlay.shared.hideOverlayView()
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        newVC.singleInfo = info
        newVC.id = info.id
        newVC.municipioStr = info.address.city
        newVC.categoriaStr = info.address.colony
        newVC.lugarString = info.advsCategorry
        newVC.establString = info.name
        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailSingleAdvs(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    func OnSelectedInfo() {
        
        structures.removeAll()
        structures.append("CardHeader")
        structures.append("titleOptions")
        structures.append("ServicePickpal")
        structures.append("MapInfo")
        structures.append("optionsPoints")
        structures.append("detail")
        //structures.append("map")
        SingleTableView.reloadData()
        
        if isShowingInfo{
            
            OnShowHistory(isShowing: true)
            
        }else{
            
            isShowingHistory = false
            isShowingTransfer = false
            isShowingInterchange = false
            isShowingInterchangeInProcess = false
            isShowingInfo = true
            isShowingMap = false
        }
    
        
       
    }
    
    func OnSelectedMap() {
        
        structures.removeAll()
        structures.append("CardHeader")
        structures.append("titleOptions")
        structures.append("ServicePickpal")
        structures.append("MapInfo")
        structures.append("optionsPoints")
        structures.append("map")
        SingleTableView.reloadData()
        
        if isShowingMap{
            
            OnShowHistory(isShowing: true)
            
            
            
        }else{
            isShowingHistory = false
            isShowingTransfer = false
            isShowingInterchange = false
            isShowingInterchangeInProcess = false
            isShowingInfo = false
            isShowingMap = true
            
        }
        
        
  
        
        
    }
    
    @IBAction func OnOpenContacts(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "contacts") as! ContactsViewController
        //newVC.contacts = contacts
        newVC.delegate = self
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    func OnShowHistory(isShowing: Bool) {
        if isShowingHistory{
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            SingleTableView.reloadData()
            
        }else{
            isShowingHistory = true
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getSingleRewards(client_id: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmenId: String(mEstabId))
            //usersWs.viewMyPoints(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
        
        isShowingInfo = !isShowing
        isShowingMap = false
        isShowingHistory = isShowing
        isShowingTransfer = false
        isShowingInterchangeInProcess  = false
        isShowingInterchange = false
        
        
    }
    
    func OnShowInterchangePonts(isShowing: Bool) {
        
        if isShowingInterchange{
            
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            //structures.append("map")
            SingleTableView.reloadData()
            
        }else{
            
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPriceMoves(from: Constants.typeInterchange)
            
        }
        
        isShowingInfo = !isShowing
        isShowingMap = false
        isShowingHistory = false
        isShowingTransfer = false
        isShowingInterchangeInProcess  = false
        isShowingInterchange = isShowing
        textToEstSelect = "Establecimiento"
        valueTaxPoints = 0
        textUserSelected = ""
        textPhoneSelected = ""
        textPointsEnter = ""
        valueInterchangePoints = 0
        textToMultipleEstSelect = "Establecimiento(s)"
        
    }
    
    func OnShowInterchangeInProces(isShowing: Bool) {
        if isShowingInterchangeInProcess{
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            //structures.append("map")
            SingleTableView.reloadData()
            isShowingInterchangeInProcess = false
        }else{
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPendigExchange(user_id:String( UserDefaults.standard.object(forKey: "client_id") as! Int))
        }
        
        isShowingInfo = !isShowing
        isShowingMap = false
    }
    
    func OnShowTransferPoints(isShowing: Bool) {
        if isShowingTransfer{
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            
            SingleTableView.reloadData()
        }else{
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPriceMoves(from: Constants.typeTranfer)
        }
        
        isShowingInfo = !isShowing
        isShowingMap = false
        isShowingHistory = false
        isShowingInterchange = false
        isShowingInterchangeInProcess  = false
        isShowingTransfer = isShowing
    }
    
    func showMessage(title: String, message: String, valueCall: Int = 1, whatsAppMesaage: Bool = false){
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.isFromHome = true
        newXIB.valueCall = valueCall
        newXIB.isFromAdd = true
        if whatsAppMesaage{
                   newXIB.hiddenBtnWhats = false
                   newXIB.msnWhatsApp = "Recibiste una Transferencia de Puntos Móviles. ¡Felicidades! \(UserDefaults.standard.object(forKey: "first_name") as! String) te acaba de transferir \(textPointsEnter) pts/ $\(mMoneyValueToSend) de \(estabName). ¡Redímelos, Transfiérelos o Intercámbialos!"
            
            openHistory()
        }
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
}

//MARK.- Extension para respuestas del servidor
extension SinglePointsViewController{
    
    func didSuccessComission(info: TransferCommission, whereFrom: String) {
        mTransferComission = info
        structures.removeAll()
        structures.append("CardHeader")
        structures.append("titleOptions")
        structures.append("ServicePickpal")
        structures.append("MapInfo")
        structures.append("optionsPoints")
        structures.append("titles")
        self.isCleanFields = true
        
        if whereFrom == Constants.typeTranfer{
            structures.append("transferPoints")
        }else if whereFrom == Constants.typeInterchange{
            structures.append("InterchangePoints")
        }
        
        SingleTableView.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailComission(error: String, subtitle: String) {
        
    }
    
    func didSuccessGetSingleRewards(info: SingleReward){
        mSingleReward = info
        LoadingOverlay.shared.hideOverlayView()
        if isShowingHistory{
            self.myHistoryPoints = mSingleReward.history
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("titles")
            for _ in mSingleReward.history {
                structures.append("history")
            }
            
            SingleTableView.reloadData()
        }else if isShowingTransfer{
            didSuccessComission(info: mTransferComission, whereFrom: Constants.typeTranfer)
        }else if isShowingInterchange{
            didSuccessComission(info: mTransferComission, whereFrom: Constants.typeInterchange)
        }else{
            isShowingInfo = true
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            SingleTableView.reloadData()
        }
    }
    
    func didFailGetGetSingleRewards(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func changeAmount(pointsToSend: Double, newMoneyAfter: Double, newPointsAfter: Double, moneyValueTosend: Double, taxValue: Int, moneyTax: Double) {
        textPointsEnter = String(pointsToSend)
        newMoneyAfterSend = String(newMoneyAfter)
        newPointsAfterSend = String(newPointsAfter)
        mMoneyValueToSend = moneyValueTosend
        taxPoints = taxValue
        taxMoneyPoints = moneyTax
        if textPointsEnter == "0"{
            SingleTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
        }
    }
    
    func invalidPoints(message: String) {
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: message)
    }

     func invalidPointsTwo(message: String) {
         showMessage(title:  UserDefaults.standard.object(forKey: "first_name") as! String, message: message)
     }
     
    func tranferToFriend() {
        if textPointsEnter != "0" && textPointsEnter != "0.0" && textPointsEnter != "" && textPhoneSelected != ""{
            if isEditPhone{
                if !textPhoneSelected.isPhone(){
                    showMessage(title: "Número Celular inválido", message: "El número celular debe de tener 10 dígitos.")
                    return
                }
            }
           
            tipoIntercambio = "trasnferencia"
            
            establishment.viewMyPointEstablishment(userId: "\(UserDefaults.standard.object(forKey: "client_id") as! Int)", establishmentId: "\(mRewardsMobile.establishment_id!)")
            
        }else{
            showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "Ingresa todos los datos solicitados.")
            
        }
    }
    
    func deletePromoById(id: Int) {
//        OnShowInterchangeInProces(isShowing: true)
        
        switch id {
        case 4,5,6:
            isShowingHistory = false
            openHistory()
        default:
            OnShowInterchangeInProces(isShowing: true)
        }
        
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        
    }
    func openHistory(){
        self.SingleTableView.contentOffset.y = 0
        if isShowingHistory{
            structures.removeAll()
            structures.append("CardHeader")
            structures.append("titleOptions")
            structures.append("ServicePickpal")
            structures.append("MapInfo")
            structures.append("optionsPoints")
            structures.append("detail")
            SingleTableView.reloadData()
            
        }else{
            isShowingHistory = true
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getSingleRewards(client_id: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmenId: String(mEstabId))
            //usersWs.viewMyPoints(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
        
        
        isShowingMap = false
        
        isShowingTransfer = false
        isShowingInterchangeInProcess  = false
        isShowingInterchange = false
    }
    func onContactDelegate(name: String, phone: String) {
        isEditPhone = false
        textUserSelected = name
        textPhoneSelected = phone
        SingleTableView.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .automatic)
    }
    
    func removeSimbols(){
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil)
        
    }
    
    func didSuccessSendPoints(user_id: Int, points: Int, phoneToSend: String, establishmentId: Int, friend: String){
        pointWs.getSingleRewards(client_id: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmenId: String(mEstabId))
        //let user = UserDefaults.standard.object(forKey: "first_name") as! String
        
        let message = "\(points) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mMoneyValueToSend)) fueron transferidos a \(friend) (\(phoneToSend))."
        
        //let message = "Tus puntos de \(mRewardsMobile.establishment_name ?? "") fueron transferidos \(points) pts /\(CustomsFuncs.getFomatMoney(currentMoney: mMoneyValueToSend)) exitosamente a: \(friend) \(phoneToSend)."
        showMessage(title: "Transferencia exitosa", message: message, valueCall: 6, whatsAppMesaage: true)
        
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailSendPoints(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "Ocurrio un problema al enviar tus puntos")
    }
    
    func didSuccessGetPendingExchange(info: [PendingExchange]) {
        isShowingHistory = false
        isShowingTransfer = false
        isShowingInterchangeInProcess = true
        isShowingInterchange = false
        
        listPendingExchange = info
        structures.removeAll()
        structures.append("CardHeader")
        structures.append("titleOptions")
        structures.append("ServicePickpal")
        structures.append("MapInfo")
        structures.append("optionsPoints")
        structures.append("titles")
        for _ in info {
            structures.append("InterchangePointsInProcess")
        }
        SingleTableView.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailGetGetPendingExchange(error: String, subtitle: String) {
        isShowingHistory = false
        isShowingInterchange = false
        isShowingInterchangeInProcess = false
        isShowingTransfer = false
        isShowingInfo = true
        isShowingMap = false
        
        structures.removeAll()
        structures.append("CardHeader")
        structures.append("titleOptions")
        structures.append("ServicePickpal")
        structures.append("MapInfo")
        structures.append("optionsPoints")
        structures.append("detail")
        //structures.append("map")
        SingleTableView.reloadData()
        
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "No tienes intercambios pendientes")
        LoadingOverlay.shared.hideOverlayView()
        
    }
    
    func  onCancelInterchange(id: Int, nameEstablishment: String,amountTransfer : Double ) {
        
        self.cancelAmount = amountTransfer
        self.cancelEstablishment = nameEstablishment
        LoadingOverlay.shared.showOverlay(view: self.view)
        pointWs.cancelInterchange(excharge_pk: id)
    }
    
    func didSuccessCancelInterchange(){
        pointWs.getPendigExchange(user_id:String( UserDefaults.standard.object(forKey: "client_id") as! Int))
        LoadingOverlay.shared.hideOverlayView()
        let user = UserDefaults.standard.object(forKey: "first_name") as! String
        showMessage(title: user, message: "Cancelaste la solicitud. Por lo que se te reembolsaron \(CustomsFuncs.numberFormat(value:self.cancelAmount )) pts. de \(self.cancelEstablishment)",valueCall: 5)
    }
    
    func didFailCancelInterchange(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    //Intercambiar puntos
    func onOpenSingleEstablishment() {
        
    }
    
    func didFailRewardsMobile(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    func didSuccessRewardsMobile(info: [RewardsMobile]) {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "multiples") as! MultipleEstsViewController
        newVC.mListEstablishments = info
        newVC.delegate = self
        newVC.idPrevious = mEstabId
        newVC.valueState = self.valueState
        newVC.isFirstTime = true
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func onOpenMultiplesEstablishments() {
      //  pointWs.getRewards(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
         pointWs.getAllEstablishmentAround()
    }
    
    
    
    func didSuccessAllEstablishmentAround(info: [RewardsMobile]) {
        
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "multiples") as! MultipleEstsViewController
        newVC.mListEstablishments = info
        newVC.delegate = self
        newVC.idPrevious = mEstabId
        newVC.valueState = self.valueState
        newVC.isFirstTime = true
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func onChangeInterchangeEnter(valeu: Double, moneyPoint: Double, taxValue: Int, moneyTax: Double, pointsAfterGift: Double, pointsAfterGiftMoney: Double) {
        print(valeu)
        valueInterchangePoints = Int(valeu)
        valueTaxPoints = taxValue
        moneyTaxValue = moneyTax
        valueMoneyPointsInterchange = moneyPoint
        pointsAfterGiftInterhange = pointsAfterGift
        pointsMoneyAfterGiftInterhange = pointsAfterGiftMoney
    }
    
    func onInitInterchange() {
        print(valueInterchangePoints)
        print(multiplesIdsSelected)
        if valueInterchangePoints != 0 && multiplesIdsSelected.count > 0 {
            
           tipoIntercambio = "intercambio"
           establishment.viewMyPointEstablishment(userId: "\(UserDefaults.standard.object(forKey: "client_id") as! Int)", establishmentId: "\(mRewardsMobile.establishment_id!)")
        }else{
            showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "Ingresa todos los datos solicitados.")
        }
    }
    
    func OnSetMultipleEst(ids: [RewardsMobile]) {
        textToMultipleEstSelect = ""
        multiplesIdsSelected = ids
        print(ids.count)
        var index = 0//tableInterchange.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        for data in ids{
            if index >= ids.count-1{
                textToMultipleEstSelect = textToMultipleEstSelect + data.establishment_name
            }else{
                textToMultipleEstSelect = textToMultipleEstSelect + data.establishment_name + " / "
            }
            index += 1
        }
        
        SingleTableView.reloadRows(at: [IndexPath(row:6, section: 0)], with: .automatic)
    }
    
    func didSuccessInterchangePoints(info: [InterchangemyPoints]) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        textToMultipleEstSelect = "Establecimiento(s)"
        pointWs.getSingleRewards(client_id: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmenId: String(mEstabId))
        if info.count > 0{
            let message = "¡Enhorabuena! Intercambiaste \(valueInterchangePoints.formatnumbers()) pts | \(CustomsFuncs.numberFormat(value: valueMoneyPointsInterchange)) de \(info[0].from_est ?? "") por \(info[0].to_est ?? "")."
            showMessage(title: "¡Tus Puntos Móviles ya se intercambiaron!", message: message, valueCall: 4)
        }else{
            let user = UserDefaults.standard.object(forKey: "first_name") as! String
            showMessage(title: user, message: "PickPal buscará hacer el intercambio de forma automática y te notificaremos cuando quede realizado.", valueCall: 3)
        }
        
        //MARK: - Termina intercambio
        
        
        
        
    }
    
    
    func didFailInterchangePoints(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    func onAddedPhone(phone: String) {
        isEditPhone = phone == "" ? false : true
        textPhoneSelected = phone
        
    }
}

//MARK.- Extension para uitableview/
extension SinglePointsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(structures[indexPath.row]){
            case "MapInfo":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapAndInfo", for: indexPath) as! MapInfoTableViewCell
            cell.delegate = self
            
            if !isShowingMap{
                cell.mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
            }
            
            if !isShowingInfo{
                cell.infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
            }else{
                cell.infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
            }
            
            cell.lblNameStablishment.text = mSingleReward.name
            return cell
            
        case "map":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapTable", for: indexPath) as! MapPointsTableViewCell
            
            let latit = CLLocationDegrees(mSingleReward.latitude)
            let longi = CLLocationDegrees(mSingleReward.longitude)
            
            let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
            cell.mapa.setCamera(camera, animated: true)
            let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
            let annotation = MKPointAnnotation()
            annotation.coordinate = eyeCoordinate
            cell.mapa.addAnnotation(annotation)
            cell.delegate = self
            
            return cell
            
        case "InterchangePointsInProcess":
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchangeProcess", for: indexPath) as! TransferInterchangeTableViewCell
            cell.delegate = self
            let data = listPendingExchange[indexPath.row - 6]
//            cell.isUserInteractionEnabled = false
            cell.mId = data.id
            cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.date, ziseFont: 11.0, colorText: 0)
            cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 11.0, colorText: 0)
            cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concepInterchange, ziseFont: 11.0, colorText: 0)
            
//            cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2: "\(CustomsFuncs.numberFormat(value: data.next_balance)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_next_balance))", ziseFont: 11.0, colorText: 0)
            
            cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2: "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money))", ziseFont: 12.0, colorText: 0)
            
       //     cell.lblCause.attributedText = CustomsFuncs.getAttributedString(title1: "Por puntos de:\n", title2: "\(data.exchange_to ?? "")", ziseFont: 11.0, colorText: 0)
            
            
             var establhistFrom : String!
             var establhistFromStreet : String!
             var currentIndex = 0
             for name in data.exchange_to!{
                         
                         establhistFrom = "\(name)"
                          currentIndex += 1
             }
                     
             for nameStreet in data.exchange_to_address_array!{
                         
                         establhistFromStreet = nameStreet
                         
             }
            
            cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:"\(data.reference!)", ziseFont: 12.0, colorText: 0)
            cell.lblCause.text = ""
            cell.lblToFromUser.attributedText = formatStringAddressProcess(data: data)
            //cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos de:\n", title2: "\(data.exchange_from ?? "")", ziseFont: 11.0, colorText: 0)
//            cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:"\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money))", ziseFont: 12.0, colorText: 0)
            
            cell.lblAmount.visibility = .gone
            cell.lblMovements.visibility = .gone
            cell.imgArrowMovemen.visibility = .gone
            
            if data.tax_total > 0 {
                
            cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisión:\n", title2: "\(CustomsFuncs.numberFormat(value: data.tax ?? 0)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.tax_total))", ziseFont: 11.0, colorText: 0)
                
            }else{
                
                cell.lblPriceTransaction.text = ""
                
            }
            
            
            //Oscar Movimientos
            if data.exchange_details.count > 0{
//                cell.isUserInteractionEnabled = true;
                cell.lblMovements.visibility = .visible
                cell.imgArrowMovemen.visibility = .visible
                
                cell.listMovemenTransfer = data.exchange_details
                cell.collectinTransferPoints.reloadData()
                               
           if expandedIndexSet.contains(indexPath.row) {
                            
                   dinamicHeight = dinamicHeight + CGFloat(data.exchange_details.count * 60)
                   cell.collectinTransferPoints.visibility = .visible
                    cell.collectinTransferPoints.reloadData()
            cell.imgArrowMovemen.image =  UIImage(named: "ic_movement_up")
               //    cell.heightCell.constant = dinamicHeight
           } else {
                             
                dinamicHeight = 280
                cell.collectinTransferPoints.visibility = .gone
               // cell.heightCell.constant = dinamicHeight
           }
            }else{
                
                cell.lblMovements.visibility = .gone
                
                cell.imgArrowMovemen.visibility = .gone
            }
            
            cell.selectionStyle = .none
            return cell
            
        case "CardHeader":
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCardPoints", for: indexPath) as! MyCardPointsTableViewCell
            cell.selectionStyle = .none
            cell.viewCard.layer.cornerRadius = 5
            cell.viewCard.layer.shadowColor = UIColor.black.cgColor
            cell.viewCard.layer.shadowOpacity =  0.30
            cell.viewCard.layer.shadowOffset = .zero
            cell.viewCard.layer.shadowRadius = 5
            
            cell.lblPointsCard.text = CustomsFuncs.numberFormat(value: mSingleReward.totalPoints) + " pts"
            cell.lblMoneyCard.text = CustomsFuncs.getFomatMoney(currentMoney: mSingleReward.total_money ?? 0.0)
            cell.lblStateCard.text = mSingleReward.state
            cell.lblPlaceCard.text = mSingleReward.place
            
           /* cell.lblVingencyInit.isHidden = mSingleReward.total_points_firts > 0 ? false : true
            cell.lblVingencyFinish.isHidden = mSingleReward.total_points_second > 0 ? false : true*/
            
            if mSingleReward.total_points_firts <= 0 && mSingleReward.total_points_second > 0{
                cell.lblOnlyDatePoints.text = mSingleReward.validity_points_last
                cell.lblOnlyDatePoints.isHidden = false
                cell.lblVingencyInit.isHidden = true
                cell.lblVingencyFinish.isHidden = true
            } else if mSingleReward.total_points_second <= 0 && mSingleReward.total_points_firts > 0{
                cell.lblOnlyDatePoints.text = mSingleReward.validity_points_firts
                cell.lblOnlyDatePoints.isHidden = false
                cell.lblVingencyInit.isHidden = true
                cell.lblVingencyFinish.isHidden = true
            }else{
                cell.lblVingencyInit.text = mSingleReward.validity_points_firts
                cell.lblVingencyFinish.text = mSingleReward.validity_points_last
                cell.lblOnlyDatePoints.isHidden = true
            }
            
            if let logo = mSingleReward.logo, logo != "" {
                Nuke.loadImage(with: URL(string: logo)!, into: cell.imgImageCard)
            }
            
            return cell
            
        case "titleOptions":
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleOptionsSelected")!
            cell.selectionStyle = .none
            return cell
            
        case "ServicePickpal":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Services") as! ServicesPickPalTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.singleInfo = mSingleReward
            cell.lblPhoneNumber.text = mSingleReward.phone
            if mSingleReward.pickpal_services != nil {
                let icons = CustomsFuncs.getIconsServicePickPal(data: mSingleReward.pickpal_services)
                switch mSingleReward.pickpal_services.count {
                case 4:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = false
                    cell.btnPoints.isHidden = false
                    cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                    cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                    cell.btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                    cell.btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                    break
                case 3:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = false
                    cell.btnPoints.isHidden = true
                    cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                    cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                    cell.btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                    break
                    
                case 2:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = false
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                    cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                    cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                    break
                    
                case 1:
                    cell.btnFoodAndDrinks.isHidden = false
                    cell.btnPromos.isHidden = true
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                    cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                    break
                    
                default:
                    cell.btnFoodAndDrinks.isHidden = true
                    cell.btnPromos.isHidden = true
                    cell.btnGuia.isHidden = true
                    cell.btnPoints.isHidden = true
                    break
                }
            }
            return cell
            
        case "optionsPoints":
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsPoints") as! OptionsPointsTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.isShowingHistory = isShowingHistory
            cell.isShowingTransfer = isShowingTransfer
            cell.isShowingInterchange = isShowingInterchange
            cell.isShowingInterchangeInProcess = isShowingInterchangeInProcess
            cell.isShowingInfo = isShowingInfo
            cell.isShowingMap = isShowingMap
            
            if !isShowingInterchangeInProcess {
                cell.lblInterchangeInProcess.alpha = 1
            }else{
                cell.lblInterchangeInProcess.alpha = 0
            }
            
            if !isShowingHistory {
                cell.lblHistory.textColor = .black
            }else{
                cell.lblHistory.textColor = UIColor(red: 255, green: 0, blue: 216, alpha: 1.0)
            }
            
            if !isShowingTransfer{
                cell.lblTransferPoints.alpha = 1
            }
            
            if !isShowingInterchange{
                cell.lblInterchangePoints.alpha = 1
            }
            
            return cell
            
        case "detail":
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailEstablishmentCell") as! DetailSingleGuideTableViewCell
            cell.selectionStyle = .none
            
            cell.singleInfo = mSingleReward.socialInfo
            if mSingleReward.descrip != nil {
                cell.lblDescrip.text = mSingleReward.descrip
            }
            
            if mSingleReward.establishmentCategory != nil {
                cell.lblCategory.text = mSingleReward.establishmentCategory
            }else{
                cell.lblCategory.text = ""
            }
            
            if mSingleReward.operation_schedule != nil {
                if mSingleReward.operation_schedule.count > 0 {
                    cell.lblHorario.text = CustomsFuncs.createHorario(horario: mSingleReward.operation_schedule)
                }
            }
            
            var direccion = ""
            if mSingleReward.address != nil {
                direccion = mSingleReward.address.street //+ " " + mSingleReward.address.colony
                
                if mSingleReward.address.noExt != nil && mSingleReward.address.noExt != ""{
                    direccion = direccion + " " + mSingleReward.address.noExt
                }
                
                if mSingleReward.address.noInt != nil && mSingleReward.address.noInt != ""{
                    direccion = direccion + " - " + mSingleReward.address.noInt
                }
                
                if mSingleReward.address.colony != nil && mSingleReward.address.colony != ""{
                    direccion = direccion + " " + mSingleReward.address.colony + ","
                }
                
                if mSingleReward.address.city != nil && mSingleReward.address.city != ""{
                    direccion = direccion + " " + mSingleReward.address.city
                }
                
                if mSingleReward.address.state != nil && mSingleReward.address.state != ""{
                    direccion = direccion + ", " + mSingleReward.address.state
                }
                
                if mSingleReward.address.zip_code != nil && mSingleReward.address.zip_code != ""{
                    direccion = direccion + ", C.P. " + mSingleReward.address.zip_code
                }
                
                if mSingleReward.reference_location != ""{
                    direccion = direccion + "\n" + mSingleReward.reference_location
                }
            }
            
            cell.lblDireccion.text = direccion
            
            var countSocialMedia = 0
            if mSingleReward.socialInfo != nil{
                cell.lblRedesSociales.isHidden = false
                cell.stackSocialMedia.isHidden = false
                
                if mSingleReward.socialInfo.delivery == nil {
                    cell.lblServicioADomicilio.text = "No"
                }else{
                    cell.lblServicioADomicilio.text = mSingleReward.socialInfo.delivery ? "Si" : "No"
                }
                
                if mSingleReward.socialInfo.delivery != nil {
                    cell.lblMetodoPago.text = mSingleReward.socialInfo.payment_methods
                }
                
                if mSingleReward.socialInfo.siteURL == nil {
                    cell.btnWeb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnWeb.isHidden = false
                }
                
                if mSingleReward.socialInfo.facebookURL == nil{
                    cell.btnFb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnFb.isHidden = false
                }
                
                if mSingleReward.socialInfo.instagramURL == nil{
                    cell.btnInsta.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnInsta.isHidden = false
                }
                
                if mSingleReward.socialInfo.twitterURL == nil{
                    cell.btnTwitter.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnTwitter.isHidden = false
                }
                
                if countSocialMedia == 4 {
                    cell.lblRedesSociales.isHidden = true
                    cell.stackSocialMedia.isHidden = true
                }
            }else{
                cell.lblRedesSociales.isHidden = true
                cell.stackSocialMedia.isHidden = true
            }
            return cell
            
        case "titles":
            let cell = tableView.dequeueReusableCell(withIdentifier: "titles") as! TittleOptionsTableViewCell
            cell.selectionStyle = .none
            cell.lblTitles.textAlignment = .center
            if isShowingHistory {
                cell.lblTitles.attributedText = CustomsFuncs.setSmallIconTitle(titleIcon: "ic_history_selected", titleTextPickpal: CustomsFuncs.getAttributedString(title1: " Historial de movimientos de \n", title2: "Puntos Móviles"))
                cell.viewDivisorTitle.isHidden = false
            }else if isShowingTransfer{
                //cell.ctlTopLabel.constant = 12
                cell.lblTitles.attributedText = CustomsFuncs.setSmallIconTitle(titleIcon: "ic_transfer_selected", titleTextPickpal: CustomsFuncs.getAttributedString(title1: " Transferir ", title2: "Puntos Móviles"))
                cell.viewDivisorTitle.isHidden = true
            }else if isShowingInterchange{
                let newString = NSMutableAttributedString()
                newString.append(CustomsFuncs.getAttributedString(title1: " Intercambia tus ", title2: "Puntos Móviles "))
                newString.append(CustomsFuncs.getAttributedString(title1: "por \n puntos de otros comercios de tu interés", title2: ""))
                cell.lblTitles.attributedText = CustomsFuncs.setSmallIconTitle(titleIcon: "ic_interchange_selected", titleTextPickpal: newString)
                cell.viewDivisorTitle.isHidden = false
            }else if isShowingInterchangeInProcess{
                cell.viewDivisorTitle.isHidden = false
                let newString = NSMutableAttributedString()
                newString.append(CustomsFuncs.getAttributedString(title1: " Intercambios en Proceso de\n", title2: "Puntos Móviles", colorText: 2))
                
                cell.lblTitles.attributedText = CustomsFuncs.setSmallIconTitle(titleIcon: "ic_interchange_process_selected", titleTextPickpal: newString)
                
            }
            
            return cell
            
        case "InterchangePoints":
            let cell = tableView.dequeueReusableCell(withIdentifier: "InterchangePoints") as! InterchangePointsTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.selectionStyle = .none
            cell.lblEst.text = textToEstSelect
            cell.mTransferCommission = mTransferComission
            cell.lblEstToTransfer.text = textToMultipleEstSelect
            cell.idSelected = mEstabId
            let price = mTransferComission.comision_intercambio * mTransferComission.tipo_cambio
            
            cell.lblPriceTransaction.text = "Comisión: \(valueTaxPoints) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTaxValue))"
            
            if valueInterchangePoints != 0{
                cell.tfEnterPoints.text = CustomsFuncs.numberFormat(value: Double(valueInterchangePoints))
                cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: pointsAfterGiftInterhange)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: pointsMoneyAfterGiftInterhange))"
            }
            
            cell.mRewardsMobile = mRewardsMobile
            cell.lblAmountAlready.attributedText =
                CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                 title2: "\(CustomsFuncs.numberFormat(value: mRewardsMobile.total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))", ziseFont: 11.0)
            
            if isCleanFields{
                multiplesIdsSelected.removeAll()
                cell.lblAmountAlready.attributedText =
                CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                 title2: "\(CustomsFuncs.numberFormat(value: 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))", ziseFont: 11.0)
                cell.tfEnterPoints.text = ""
                cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(0)))"
                cell.lblPriceTransaction.text = "Comisión: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))"
                isCleanFields = false
            }
            return cell
            
        case "transferPoints":
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendPoinsSingle") as! SendPoinsSingleTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.mTransferCommission = mTransferComission
            
            if textPhoneSelected != ""{
                if textUserSelected != ""{
                    cell.tfContact.text = textUserSelected + " / " + textPhoneSelected
                }else{
                    cell.tfContact.text = textPhoneSelected
                }
            }
            
            if textToEstSelect == "Establecimientos" {
                cell.tfEnterPoints.isUserInteractionEnabled = false
                //cell.btnTransferPoints.isUserInteractionEnabled = false
                cell.lblAmountAlready.isHidden = true
            }else{
                cell.mRewardsMobile = self.mRewardsMobile
                cell.tfEnterPoints.isUserInteractionEnabled = true
                //cell.btnTransferPoints.isUserInteractionEnabled = true
                cell.lblAmountAlready.isHidden = false
                
                cell.lblAmountAlready.attributedText =
                    CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                     title2: "\(CustomsFuncs.numberFormat(value: mRewardsMobile.total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))", ziseFont: 11.0)
            }
            
            if textPointsEnter != "0" && textPointsEnter != "" && newMoneyAfterSend != "" && newPointsAfterSend != ""{
            //if textPointsEnter != "0" && newMoneyAfterSend != "" && newPointsAfterSend != ""{
                let pd = Double(textPointsEnter) ?? 0
                let pi = Int(pd)
                cell.tfEnterPoints.text = String(pi)
                
                cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(newPointsAfterSend) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(newMoneyAfterSend)!))"
                
                cell.lblPriceTransaction.text = "Comisión: \(taxPoints) pts / \(CustomsFuncs.getFomatMoney(currentMoney: taxMoneyPoints))"
            }
            
            if isCleanFields{
                cell.tfEnterPoints.text = ""
                cell.tfContact.text = ""
                cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(0)))"
                cell.lblPriceTransaction.text = "Comisión: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))"
                isCleanFields = false
            }
            
            return cell
            
        default:
            if myHistoryPoints.count > 0{
                let data = myHistoryPoints[indexPath.row - 6]
                switch(myHistoryPoints[indexPath.row - 6].concept_int){
                case 1, 2: //MARK: - 1 =  Puntos abonados (compra) 2 = puntos utilizados compras
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AddedAndRedimPoints") as! AddedAndRedimPointsTableViewCell
//                    let data = myHistoryPoints[indexPath.row - 6]
                    
                    cell.selectionStyle = .none
                    
                    if data.concept_int == 2{
                        cell.imgTypeConcept.image = UIImage(named: "ic_history_redim")
                    }else{
                        cell.imgTypeConcept.image = UIImage(named: "ic_history_added")

                    }
                    cell.lblReferensNumbr.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:  ", title2: data.reference, ziseFont: 11.0, colorText: 0)
                    
                  //  cell.lblReferensNumbr.text! = data.reference
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 11.0, colorText: 0)
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 11.0, colorText: 0)
                    cell.lblConcept.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 14.0, colorText: 0)
                    cell.lblEstablishment.attributedText = formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 1)
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                        "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money))", ziseFont: 11.0, colorText: 0)
                    
                    cell.lblComision.visibility = .gone
//                    cell.lblComision.attributedText = CustomsFuncs.getAttributedString(title1: "Comision:\n", title2:
//                                     "\( data.str_tax!)", ziseFont: 12.0, colorText: 0)
                     cell.lablTotal.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2:
                                 "$\(CustomsFuncs.numberFormat(value: data.total_money!))", ziseFont: 12.0, colorText: 0)
                                 
                    
                    cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Saldo Actual:\n", title2:
                        "\(CustomsFuncs.numberFormat(value: data.next_balance)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_next_balance))", ziseFont: 11.0, colorText: 0)
                    
                    return cell
                    
                    
                case 3: //MARK: - Puntos abonados (transferencia)
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                    
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = true;
                    cell.lblMovements.isHidden = true
                    cell.collectinTransferPoints.isHidden = true
                    cell.imgArrowMovemen.isHidden = true
                    
                    
                    cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:"\(data.reference!)", ziseFont: 12.0, colorText: 0)
                    cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 14.0, colorText: 0)
                    
                    cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
                    
                    cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "De:\n", title2: data.transfer_from, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblCause.attributedText =  formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 3)
                    cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                    "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                        
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)

//                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    cell.lblPriceTransaction.visibility = .gone
                    cell.lblCurrentMoney.text = ""
                    
                    
                    
                    return cell
                    
                case 4: //MARK: - Transferencia enviada
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                    
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = true;
                    cell.lblMovements.isHidden = true
                    cell.collectinTransferPoints.isHidden = true
                    cell.imgArrowMovemen.isHidden = true
                    
                    
                    cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:"\(data.reference!)", ziseFont: 12.0, colorText: 0)
                    cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 14.0, colorText: 0)
                    
                    cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
                    
                    cell.lblCause.attributedText =  formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 4)
                    
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                    "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                        
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total real transferido:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    
                    cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Contacto:\n", title2: data.transfer_to, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblCurrentMoney.text = ""
                    
                    return cell
                    
                    
                case 5:
                    //MARK: - Solicitud de intercambio en proceso / Finalizado
                        
                        print("Case 5")
                        
                    print("Case 5")
                    print("case 5 inicial \( dinamicHeightInProcess)")
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                    dinamicHeightInProcess = data.left_amount > 0 ? 285:255
                    print("case 5 segunda \( dinamicHeightInProcess)")
                    cell.isUserInteractionEnabled = false;
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = true;
                    cell.lblMovements.visibility = .gone
                    cell.collectinTransferPoints.visibility = .gone
                    cell.imgArrowMovemen.visibility = .gone
                    
                    
                    cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 14.0, colorText: 0)
                    cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                        "\(data.reference!)", ziseFont: 12.0, colorText: 0)
                    
                    cell.imgIconTypeCard.image = UIImage(named: "ic_history_interchange_home")
                    cell.lblAmount.visibility = data.is_cancelled && data.exchange_details.count == 1 ? .gone:.visible
                    cell.lblToFromUser.text = ""
                    cell.lblCause.attributedText = formatStringAddressHistory(data: data, indexPath: indexPath)
                    
                    
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                        
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    
                    cell.lblPriceTransaction.visibility = data.str_tax == "0 pts" ? .gone:.visible
                    
                           
                    cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:
                        "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    
                    if data.left_amount_money > 0 {
                        
//                        dinamicHeightInProcess += 40
                        
                    }
                    
    //                cell.lblCurrentMoney.text = ""
                    cell.lblAmounTrasaction.text = ""
                    dinamicHeightInProcessMin = dinamicHeightInProcess + cell.lblCause.frame.height + 70
                    cell.heightCell.constant = dinamicHeightInProcessMin
                    cell.collectinTransferPoints.reloadData()
                    
                    
                    //Oscar Movimientos
                  if data.exchange_details.count > 0{
                                      
                    // CON ALGUN MOVIMIENTOS
                      cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    
                  
                   
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    cell.lblMovements.visibility = .visible
                    cell.imgArrowMovemen.visibility = .visible
                    cell.viewFrom = true
                    cell.listMovemenTransfer = data.exchange_details
                    
                    cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
                    cell.isUserInteractionEnabled = true;
                    
    //                if data.left_amount_money > 0 {
    //
    //                    dinamicHeightInProcess += 40
    //
    //                }
//                    cell.heightCell.constant = dinamicHeightInProcess
                    
                    if expandedIndexSet.contains(indexPath.row) {
                        if data.left_amount_money > 0 {
                            
                            dinamicHeightInProcess += 35
                            
                        }
                        
                        dinamicHeightInProcess += 30
                        
                        dinamicHeightInProcess = dinamicHeightInProcess + CGFloat(data.exchange_details.count * 60)
                        cell.collectinTransferPoints.visibility = .visible
                        cell.heightCell.constant = dinamicHeightInProcess
                        cell.collectinTransferPoints.reloadData()
    //                  cell.imgArrowMovemen.isHidden = false
                        cell.imgArrowMovemen.image =  UIImage(named: "ic_movement_up")
                      
                    } else {
                                    
//                        dinamicHeightInProcess = dinamicHeightInProcessMin
                       
                        
                        cell.collectinTransferPoints.visibility = .gone
                        cell.heightCell.constant = dinamicHeightInProcessMin
                        cell.imgArrowMovemen.visibility = .visible
                        cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
                        
                      }
                    }else{
                      // SIN MOVIMIENTOS
  
                        cell.heightCell.constant = dinamicHeightInProcessMin
                        cell.lblPriceTransaction.text = ""
                        cell.lblMovements.visibility = .gone
                       cell.imgArrowMovemen.visibility = .gone
                        cell.isUserInteractionEnabled = false;
                      
                   }
                    
                    
                    
                    if data.left_amount == 0{ // INTERCAMBIO FINALIZADO
                        cell.lblCurrentMoney.text = ""
                        cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    
                  
                        
                    }else{// INTERCAMBIO PENDIENTE
                        
                        cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                        dinamicHeightInProcess += 35
                    }
                    
                   
                    SingleTableView.beginUpdates()
                    SingleTableView.endUpdates()
                    return cell
                    
                    
                case 6: //MARK: -  Puntos abonados (intercambio)
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                            
                    cell.selectionStyle = .none
                    cell.isUserInteractionEnabled = true;
                    cell.lblMovements.isHidden = true
                    cell.collectinTransferPoints.isHidden = true
                    cell.imgArrowMovemen.isHidden = true
                    
                    
                    cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:"\(data.reference!)", ziseFont: 12.0, colorText: 0)
                    cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 14.0, colorText: 0)
                    
                    cell.imgIconTypeCard.image = UIImage(named: "ic_history_interchange_home")
                    
                    cell.lblCause.attributedText = formatText(establishmentName: data.establishment_name, streetName: data.address!, type: 6)
                    
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Por puntos de:\n", title2: data.exchange_from, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                        
                    cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                    
                    cell.lblCurrentMoney.text = ""
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    
                    cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
//                    cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "De:\n", title2: data.exchange_from, ziseFont: 12.0, colorText: 0)
                    
                    
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                    
                    cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                   
                    
                    cell.lblAmount.text = ""
                    cell.lblCurrentMoney.text = ""
                    
                    return cell
                    
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                    let data = myHistoryPoints[indexPath.row - 6]
                    cell.selectionStyle = .none
                    
                    cell.lblAmounTrasaction.text = ""
                    cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:"\(data.reference!)", ziseFont: 12.0, colorText: 0)
                    
                    if data.concept_int == 4{
                        //transferencia
                        cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
                       // cell.colorViewType.backgroundColor = UIColor.orange
                        cell.lblCause.attributedText = CustomsFuncs.getAttributedString(title1: "Comercio:\n", title2: data.establishment_name, ziseFont: 11.0, colorText: 0)
                        
                        cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Contacto:\n", title2: data.transfer_to, ziseFont: 11.0, colorText: 0)
                        
                        cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total real transferido:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                        
                    }else if data.concept_int == 3{
                        cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
                       //cell.colorViewType.backgroundColor = UIColor.orange
                        cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "De:\n", title2: data.transfer_from, ziseFont: 11.0, colorText: 0)
                        cell.lblCause.attributedText = CustomsFuncs.getAttributedString(title1: "Comercio:\n", title2: data.establishment_name, ziseFont: 11.0, colorText: 0)
                        cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total real transferido:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    }else{
                        //intercambio
                        cell.imgIconTypeCard.image = UIImage(named: "ic_history_interchange_home")
                      //  cell.colorViewType.backgroundColor = UIColor.purple;
                     //   cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Por puntos de:\n", title2: data.exchange_to, ziseFont: 11.0, colorText: 0)
                        
                        //cell.lblCause.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos de:\n", title2: data.exchange_from ?? "", ziseFont: 11.0, colorText: 0)
                        
                       
                        
                        cell.lblToFromUser.text = ""
                        cell.lblCause.attributedText = formatStringAddressProcessTwo(data: data)
                    }
                    
                    //cell.lblInterchangeCancel.isHidden = data.status_label.lowercased() == "cancelado" ? false : true
                    cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 11.0, colorText: 0)
                    cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 11.0, colorText: 0)
                    cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concept, ziseFont: 11.0, colorText: 0)
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                        "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money))", ziseFont: 11.0, colorText: 0)
//                    cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Saldo Actual:\n", title2:
//                        "\(CustomsFuncs.numberFormat(value: data.next_balance)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_next_balance))", ziseFont: 11.0, colorText: 0)
                    
                    if data.left_amount > 0{
                        
                        cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:
                            "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    }else{
                        
                        cell.lblCurrentMoney.text = ""
                        
                    }
                    
                    
                    if data.tax > 0{
                    cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisión:\n", title2: "\(CustomsFuncs.numberFormat(value: data.tax ?? 0)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.tax_money))", ziseFont: 11.0, colorText: 0)
                    }else{
                        
                        cell.lblPriceTransaction.text = ""
                        
                    }
                    
                    //Oscar Movimientos
                    if data.exchange_details.count > 0{
                        
                        cell.lblMovements.isHidden = false
                        cell.listMovemenTransfer = data.exchange_details
                        cell.imgArrowMovemen.isHidden = false
                                       
                   if expandedIndexSet.contains(indexPath.row) {
                                    
                           dinamicHeight = dinamicHeight + CGFloat(data.exchange_details.count * 60)
                           cell.collectinTransferPoints.isHidden = false
                            cell.collectinTransferPoints.reloadData()
                       //    cell.heightCell.constant = dinamicHeight
                   } else {
                                     
                        dinamicHeight = 280
                        cell.collectinTransferPoints.isHidden = true
                       // cell.heightCell.constant = dinamicHeight
                   }
                    }else{
                        
                        cell.lblMovements.isHidden = true
                        cell.imgArrowMovemen.isHidden = true
                    }
                    
                    return cell
                }
            }else{
                return UITableViewCell()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(structures[indexPath.row]){
        case "CardHeader":
            return 200
        case "titleOptions":
            return 0
        case "ServicePickpal":
            return 40
            case "optionsPoints":
            return 44
        case "MapInfo":
            return 30
        case "detail":
            return 510
        case "map":
            return 426
        case "titles":
            return isShowingTransfer ? 35 : 55
        case "transferPoints":
            return 490
        case  "InterchangePoints":
            return 420
        case "InterchangePointsInProcess":
            if self.indexPath == indexPath {
              return dinamicHeight
            }else{
                return 350
            }
        default:
            if myHistoryPoints.count > 0{
                switch(myHistoryPoints[indexPath.row - 6].concept_int){
                case 1, 2:
                    return dinamicHeightGeneral
                    
                case 3, 4:
                    
                    return 280
                    
                case 6:
                    print("case 6 tamaño\( dinamicHeightFinish)")
    //                if self.indexPath == indexPath {
    //
    //                  return dinamicHeightFinish
    //                }else{
    //                    return dinamicHeightFinish
    //                }
                
                return 280
                    
                case 5:
                    print("case 5 tamaño\( dinamicHeightInProcess)")
                    if self.indexPath == indexPath {
                        return dinamicHeightInProcess
                    }else{
                        return dinamicHeightInProcess
                    }
                    
                    
                default:
                    if self.indexPath == indexPath {
                      return dinamicHeightFinish
                    }else{
                        return dinamicHeightFinish
                    }
                }
            }else{
                return 0
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.indexPath = indexPath
        
          if(expandedIndexSet.contains(indexPath.row)){
                    expandedIndexSet.remove(indexPath.row)
            } else {
        
                    expandedIndexSet.insert(indexPath.row)
            }
                
            tableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
    
    
    func formatStringAddressProcess(data: PendingExchange) -> NSMutableAttributedString{
          
          var currentIndex = 0
          var attributedTitleString: NSAttributedString!
          var attributedString: NSAttributedString!
          var stringStreet: NSAttributedString!
          
          let attributesTitle: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
              .font: UIFont(name: "Aller-Bold", size: 12.0)!
          ]
          
          let attributesEstablishment: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
              .font: UIFont(name: "Aller", size: 12.0)!
          ]
          let attributesStreet: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
              .font: UIFont(name: "Aller-Italic", size: 10)!
          ]
    
          
          //"Intercambiaste: \(data.amount!) pts de \(data.exchange_from!) \n Por: \(establhistFrom)"
          let completeText = NSMutableAttributedString(string: "")
          
          attributedTitleString = NSAttributedString(string: "Solicitud de intercambio: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
          
        attributedTitleString = NSAttributedString(string: "\(data.amount!) pts de \(data.exchange_from!) \n ", attributes: attributesEstablishment)
          completeText.append(attributedTitleString)
        
        stringStreet = NSAttributedString(string: " (\(data.address!)) \n", attributes: attributesStreet)
        
        completeText.append(stringStreet)
          
          attributedTitleString = NSAttributedString(string: "\nPor puntos de: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
          
          
          for name in data.exchange_to!{
             
              if data.exchange_to_address_array.count > 0{
                  
                attributedString = NSAttributedString(string: "\(name)\n", attributes: attributesEstablishment)
                stringStreet = NSAttributedString(string: "  (\(data.exchange_to_address_array[currentIndex]))\n \n", attributes: attributesStreet)
                  
                  completeText.append(attributedString)
                  
                  completeText.append(stringStreet)
                  
                  //cadena += "\(attributedString!) \(stringStreet!), "
              }else{
                  attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
              }
               
               currentIndex += 1
               dinamicHeight += 15
          }
          
          
          return completeText
      }
    
    
    func formatStringAddressProcessTwo(data: HistoryRewards) -> NSMutableAttributedString{
          
          var currentIndex = 0
          var attributedTitleString: NSAttributedString!
          var attributedString: NSAttributedString!
          var stringStreet: NSAttributedString!
          
          let attributesTitle: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
              .font: UIFont(name: "Aller-Bold", size: 12.0)!
          ]
          
          let attributesEstablishment: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
              .font: UIFont(name: "Aller", size: 12.0)!
          ]
          let attributesStreet: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
              .font: UIFont(name: "Aller-Italic", size: 10)!
          ]
    
          
          //"Intercambiaste: \(data.amount!) pts de \(data.exchange_from!) \n Por: \(establhistFrom)"
          let completeText = NSMutableAttributedString(string: "")
          
          attributedTitleString = NSAttributedString(string: "Intercambiaste: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
          
          attributedTitleString = NSAttributedString(string: "\(data.amount!) pts de \(data.exchange_from!) \n ", attributes: attributesEstablishment)
          completeText.append(attributedTitleString)
        
         stringStreet = NSAttributedString(string: " (\(data.address!)) \n", attributes: attributesStreet)
        
         completeText.append(stringStreet)
          
          attributedTitleString = NSAttributedString(string: "\nPor: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
          
          
          for name in data.exchange_to!{
             
              if data.exchange_to_address_array.count > 0{
                  
                  attributedString = NSAttributedString(string: "\(name)\n", attributes: attributesEstablishment)
                  stringStreet = NSAttributedString(string: "  (\(data.exchange_to_address_array[currentIndex]))\n \n", attributes: attributesStreet)
                  
                  completeText.append(attributedString)
                  
                  completeText.append(stringStreet)
                  
                  //cadena += "\(attributedString!) \(stringStreet!), "
              }else{
                  attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
              }
               
               currentIndex += 1
               dinamicHeight += 10
          }
          
          
          return completeText
      }
    
    func formatText(establishmentName: String, streetName: String, type: Int)->NSMutableAttributedString{
        
        
        var attributedTitleString: NSAttributedString!
        var attributedString: NSAttributedString!
        var stringStreet: NSAttributedString!
        
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
            .font: UIFont(name: "Aller-Bold", size: 12.0)!
        ]
        
        let attributesEstablishment: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller", size: 12.0)!
        ]
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 10)!
        ]
  
        
        //"Intercambiaste: \(data.amount!) pts de \(data.exchange_from!) \n Por: \(establhistFrom)"
        let completeText = NSMutableAttributedString(string: "")
        
        let title = type == 6 ? " Puntos abonados del comercio:\n":"Comercio:\n"
        
        attributedTitleString = NSAttributedString(string: title, attributes: attributesTitle)
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "\(establishmentName)\n", attributes: attributesEstablishment)
        
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "(\(streetName))", attributes: attributesStreet)
        
        completeText.append(attributedTitleString)
        return completeText
        
    }
    
    
    func formatStringAddressHistory(data: HistoryRewards, indexPath: IndexPath) -> NSMutableAttributedString{
        
        
        var currentIndex = 0
        var attributedTitleString: NSAttributedString!
        var attributedString: NSAttributedString!
        var stringStreet: NSAttributedString!
        
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
            .font: UIFont(name: "Aller-Bold", size: 12.0)!
        ]
        
        let attributesEstablishment: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller", size: 12.0)!
        ]
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 10)!
        ]
  
        let completeText = NSMutableAttributedString(string: "")
        
        attributedTitleString = NSAttributedString(string: "Intercambiaste: ", attributes: attributesTitle)
        
        attributedTitleString = NSAttributedString(string: "Solicitud de intercambio: ", attributes: attributesTitle)

        
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "\(data.amount!) pts de \(data.exchange_from!) \n ", attributes: attributesEstablishment)
        completeText.append(attributedTitleString)
      
       stringStreet = NSAttributedString(string: " (\(data.address!)) \n", attributes: attributesStreet)
      
       completeText.append(stringStreet)
        
        if  data.exchange_to_address_array.count > 0{
        attributedTitleString = NSAttributedString(string: "\nPor puntos de: ", attributes: attributesTitle)
        completeText.append(attributedTitleString)
        
        
        for name in data.exchange_to!{
           
                if data.exchange_to_address_array.count > 0{
                    
                    attributedString = NSAttributedString(string: "\(name)\n", attributes: attributesEstablishment)
                    stringStreet = NSAttributedString(string: " (\(data.exchange_to_address_array[currentIndex])) \n  \n", attributes: attributesStreet)
                    completeText.append(attributedString)
                    
                    completeText.append(stringStreet)
          
                
                }else{
                    attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
                    completeText.append(attributedString)
                }
                 
                 
                if data.exchange_to_address_array.count > 1{
                    dinamicHeightInProcess += 50
                    dinamicHeightFinish += 70
                }else{
                    dinamicHeightInProcess += 130
                    dinamicHeightFinish += 70
                }
               
                currentIndex += 1
                
            }
//            dinamicHeightInProcess += CGFloat(data.exchange_to_address_array.count * 20)
        }
        print("Formato  tmaño celda: \(dinamicHeightInProcess)")
        self.indexPath = indexPath
        SingleTableView.beginUpdates()
        SingleTableView.endUpdates()
//        SingleTableView.reloadRows(at: [indexPath], with: .automatic)
        
        return completeText
    }
    
    
    
    func didSuccessViewMyReward(reward: MyRewardEstablishment){
        
        print("Monto Minimo \(reward.minExchange) esto quiere intercambiar")
        
        var axu :Double!
        
        if tipoIntercambio == "trasnferencia"{
            
             axu = Double (textPointsEnter)
            
        }else{
             axu = Double(valueInterchangePoints)
            
        }
       
        
        if   axu >= reward.minExchange{
            
            
            if tipoIntercambio == "trasnferencia"{
                
                removeSimbols()
                   let numberClean = textPhoneSelected.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "").suffix(10)
                   LoadingOverlay.shared.showOverlay(view: self.view)
                   pointWs.sendPointsToFriend(user_id: UserDefaults.standard.object(forKey: "client_id") as! Int, points: textPointsEnter, phoneToSend: String(numberClean), establishmentId: mRewardsMobile.establishment_id, friend: textUserSelected, tax: taxPoints)
                
            }else{
                
               var ids = [Int]()
               for data in multiplesIdsSelected{
                   ids.append(data.establishment_id)
               }
               self.pointWs.sendPointsToInterchange(user_id: UserDefaults.standard.object(forKey: "client_id") as! Int, points: valueInterchangePoints, establishmentId: mRewardsMobile.establishment_id, establishments_destination: ids, tax: valueTaxPoints)
                
            }
            
                
          
        }else{
            
             showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "El valor mínimo a intercambiar es de \(CustomsFuncs.numberFormat(value: reward.minExchange)) pts.")
        }
        
    }
    
    func didFailViewMyReward(error: String, subtitle: String){
        
        showMessage(title: error, message: subtitle)
    }
    
    func openMaps() {
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(mSingleReward.latitude),\(mSingleReward.latitude)&zoom=14&views=traffic&q=\(mSingleReward.latitude),\(mSingleReward.longitude)")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(mSingleReward.latitude, mSingleReward.longitude)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = mRewardsMobile.establishment_name
            mapItem.openInMaps(launchOptions: options)
        }
        
    }
    
    
}

extension Int {
    func formatnumbers() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}
