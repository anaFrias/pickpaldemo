//
//  EstabsRewardsCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class EstabsRewardsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vimgBackGround: UIImageView!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblTotalMoney: UILabel!
    @IBOutlet weak var imgSpaceWhite: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    //@IBOutlet weak var viewBackGround: UIView!
    @IBOutlet weak var lblCodeState: UILabel!
    
    @IBOutlet weak var viewAvailable: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
