//
//  TransferInterchangeTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/4/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol TransferInterchangeDelegate {
    func onCancelInterchange(id: Int, nameEstablishment: String,amountTransfer : Double )
}

class TransferInterchangeTableViewCell: UITableViewCell,  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 
    
    @IBOutlet weak var heightCell: NSLayoutConstraint!
    
    @IBOutlet weak var imgIconTypeCard: UIImageView!
    @IBOutlet weak var colorViewType: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblConcepto: UILabel!
    @IBOutlet weak var lblToFromUser: UILabel!
    @IBOutlet weak var lblCause: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPriceTransaction: UILabel!
    @IBOutlet weak var lblCurrentMoney: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblInterchangeCancel: UILabel!
    @IBOutlet weak var collectinTransferPoints: UICollectionView!
    @IBOutlet weak var lblNumberReferens: UILabel!
    @IBOutlet weak var imgArrowMovemen: UIImageView!
    
    @IBOutlet weak var lblAmounTrasaction: UILabel!
    @IBOutlet weak var lblMovementsProcess: UILabel!
    var delegate: TransferInterchangeDelegate!
    var mId: Int!
    var establishmentName = String ()
    var amountTransfer = Double()
    var listMovemenTransfer = [ExchangeDetails]()
    var viewFrom = Bool()
    
    @IBOutlet weak var lblMovements: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if collectinTransferPoints != nil {
           // collectinTransferPoints.register(TransferMovementViewCell.nib(), forCellWithReuseIdentifier: "transferMovementViewCell")
            collectinTransferPoints.delegate = self
            collectinTransferPoints.dataSource = self
            
           // print("Lista ",listMovemenTransfer )
        }
       
   
        
        print("Lista ",listMovemenTransfer )
        
        
        if lblInterchangeCancel != nil{
            lblInterchangeCancel.clipsToBounds = true
            lblInterchangeCancel.layer.cornerRadius = 8
        }
        
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.delegate.onCancelInterchange(id: mId, nameEstablishment: establishmentName, amountTransfer: amountTransfer)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "transferMovementViewCell", for: indexPath) as! TransferMovementViewCell
       
        print("Index \(indexPath.row) count \(listMovemenTransfer.count)")
        if  listMovemenTransfer[indexPath.row].isCancel != nil && listMovemenTransfer[indexPath.row].isCancel{

            cell.lblDescriptionMovement.text = "\(listMovemenTransfer[indexPath.row].establishmentidFrom!)  \(listMovemenTransfer[indexPath.row].establishmentidTo!)"
            cell.lblNumberMovement.text = "#\(indexPath.row + 1)"

        }else{
            
            cell.lblDescriptionMovement.text = "Se intercambiaron \(listMovemenTransfer[indexPath.row].establishmentidFrom!) por \(listMovemenTransfer[indexPath.row].establishmentidTo!)"
            cell.lblNumberMovement.text = "#\(indexPath.row + 1)"
            
        }
       
        //collectinTransferPoints.reloadData()
       return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

                return CGSize(width: collectionView.frame.size.width, height: 51)
       
        }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listMovemenTransfer.count
     }
     
     
    
   
    
}
