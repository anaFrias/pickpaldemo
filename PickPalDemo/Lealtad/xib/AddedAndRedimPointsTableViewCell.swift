//
//  AddedAndRedimPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/29/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AddedAndRedimPointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgTypeConcept: UIImageView!
    @IBOutlet weak var viewContentData: UIView!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblHour: UILabel!
    @IBOutlet weak var lblEstablishment: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblCurrentMoney: UILabel!
    @IBOutlet weak var viewColorConcept: UIView!
    @IBOutlet weak var lblConcept: UILabel!
    @IBOutlet weak var lblNumbreReferen: UILabel!
    @IBOutlet weak var lblReferensNumbr: UILabel!
    
    @IBOutlet weak var lablTotal: UILabel!
    @IBOutlet weak var lblComision: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
