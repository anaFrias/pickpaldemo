//
//  MyCardPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class MyCardPointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBottomCard: UIView!
    @IBOutlet weak var viewCard: UIView!
    @IBOutlet weak var imgImageCard: UIImageView!
    @IBOutlet weak var lblPlaceCard: UILabel!
    @IBOutlet weak var lblStateCard: UILabel!
    @IBOutlet weak var lblMoneyCard: UILabel!
    @IBOutlet weak var lblPointsCard: UILabel!
    @IBOutlet weak var lblVingencyInit: UILabel!
    @IBOutlet weak var lblVingencyFinish: UILabel!
    @IBOutlet weak var lblOnlyDatePoints: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBottomCard.clipsToBounds = true
        viewBottomCard.layer.cornerRadius = 5
        viewBottomCard.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
