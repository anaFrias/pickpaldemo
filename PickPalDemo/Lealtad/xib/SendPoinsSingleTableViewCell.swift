//
//  SendPoinsSingleTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/11/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol  SendPoinsSingleDelegate{
    func changeAmount(pointsToSend: Double, newMoneyAfter: Double, newPointsAfter:Double, moneyValueTosend: Double, taxValue: Int, moneyTax: Double)
    func invalidPointsTwo(message: String)
    func tranferToFriend()
    func onAddedPhone(phone: String)
}

class SendPoinsSingleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tfContact: UITextField!
    @IBOutlet weak var lblAmountAlready: UILabel!
    @IBOutlet weak var btnTransferPoints: UIButton!
    @IBOutlet weak var tfEnterPoints: UITextField!
    @IBOutlet weak var lblPriceTransaction: UILabel!
    @IBOutlet weak var newMoneyAfterTransaction: UILabel!
    

    var mTransferCommission: TransferCommission!
    var mRewardsMobile: RewardsMobile!
    var delegate: SendPoinsSingleDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onEditingText(_ sender: Any) {
//        print(tfEnterPoints.text!.count)
        if tfEnterPoints.text != ""{
            if tfEnterPoints.text!.hasPrefix("0")  {
                self.delegate.invalidPointsTwo(message: "El valor de los puntos a intercambiar no puede iniciar en cero.")
                tfEnterPoints.text = ""
                return
            }
            
            let points = Double(tfEnterPoints.text?.replacingOccurrences(of: ",", with: "") ?? "0") ?? 0
            
            let myTax = points * mTransferCommission.comision_transferencia
            let taxInt = floor(myTax)
            let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio
//            print("El impuesto para esta transaccion es: \(taxInt) puntos")
//            print("El valor del impuesto es \(moneyTax)")
//
            let newValuePoints = points + taxInt
            
            if newValuePoints.isLess(than: mRewardsMobile.total_points){
                let myTotalPoints = mRewardsMobile.total_points ?? 0
//                print("Tengo: \(myTotalPoints) puntos" )
                let valueOfMyPontsInMoney =  mRewardsMobile.total_money ?? 0
//                print("Todos mis puntos valen: \(valueOfMyPontsInMoney) pesos")
                
//                print("quiero reglar \(points) puntos")
                let valueOfMyPointstoGif = points * mTransferCommission.tipo_cambio
//                print("estos puntos valen \(valueOfMyPointstoGif) en pesos")
                
                let remindPointsAfterGif = myTotalPoints - (points + taxInt)
//                print("los puntos que me van a quedar son \(remindPointsAfterGif)")
                let remindMoneyAfterGif = valueOfMyPontsInMoney - (valueOfMyPointstoGif + moneyTax)
//                print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")
                
                //label.- costo de transaccion impuesto(puntos) / valor del impuesto en pesos
                
                
                //valor del impuesto = impuesto en puntos * tipo de cambio
                
                //costo de transaccion =  puntos ingresados * costo de comision de transferencia
                //vañor en pesos costo de transaccion * tipo cambio
                
                lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                
                newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: remindPointsAfterGif)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: remindMoneyAfterGif))"
                delegate.changeAmount(pointsToSend: points, newMoneyAfter: remindMoneyAfterGif, newPointsAfter: remindPointsAfterGif, moneyValueTosend: valueOfMyPointstoGif, taxValue: 0, moneyTax: 0)
                
                
                
            }else{
                //tfEnterPoints.text = "0"
                let myTax = mRewardsMobile.total_points * mTransferCommission.comision_transferencia // comision
                let taxInt = floor(myTax) //se pasa a entero los puntos
                let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio // sacamos el total de los puntos
//                print("El impuesto para esta transaccion es: \(taxInt) puntos")
//                print("El valor del impuesto es \(moneyTax)")
                
                let newValuePoints = mRewardsMobile.total_points - taxInt //puntos que puedo dar como maximo
                let valueOfMyPointstoGif = newValuePoints *  mTransferCommission.tipo_cambio
                
                tfEnterPoints.text = CustomsFuncs.numberFormat(value: mRewardsMobile.total_points)
                lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                
                newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: 0.0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0.0))"
                
                delegate.changeAmount(pointsToSend: mRewardsMobile.total_points, newMoneyAfter: 0, newPointsAfter: 0, moneyValueTosend: valueOfMyPointstoGif, taxValue: 0, moneyTax: 0)
                
            }
        }else{
            newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))"
            delegate.changeAmount(pointsToSend: 0, newMoneyAfter: mRewardsMobile.total_money, newPointsAfter: mRewardsMobile.total_points, moneyValueTosend: 0, taxValue: 0, moneyTax: 0)
            
        }
        
        
        /*print(tfEnterPoints.text!.count)
        if tfEnterPoints.text != ""{
            let points = Double(tfEnterPoints.text ?? "0") ?? 0
            if points.isLess(than: mRewardsMobile.total_points){
                let myTotalPoints = mRewardsMobile.total_points ?? 0
                print("Tengo: \(myTotalPoints) puntos" )
                let valueOfMyPontsInMoney = mRewardsMobile.total_points / mTransferCommission.tipo_cambio
                print("Todos mis puntos valen: \(valueOfMyPontsInMoney) pesos")
                
                print("quiero reglar \(points) puntos")
                let valueOfMyPointstoGif = points / mTransferCommission.tipo_cambio
                print("estos puntos valen \(valueOfMyPointstoGif) en pesos")
                
                let remindPointsAfterGif = myTotalPoints - points
                print("los puntos que me van a quedar son \(remindPointsAfterGif)")
                let remindMoneyAfterGif = valueOfMyPontsInMoney - valueOfMyPointstoGif
                print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")
                
                lblPriceTransaction.text = "Costo de transacción \(points) pts \(CustomsFuncs.getFomatMoney(currentMoney: valueOfMyPointstoGif))"
                newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(remindPointsAfterGif) pts / \(CustomsFuncs.getFomatMoney(currentMoney: remindMoneyAfterGif))"
                delegate.changeAmount(pointsToSend: points, newMoneyAfter: remindMoneyAfterGif, newPointsAfter: remindPointsAfterGif, moneyValueTosend: valueOfMyPointstoGif)
                
                
            }else{
                tfEnterPoints.text = "0"
                self.delegate.invalidPoints()
                newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(mRewardsMobile.total_points ?? 0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))"
                //mostrar mensaje de has exccedido los puntosque tienes
            }
        }else{
            newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(mRewardsMobile.total_points ?? 0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))"
            delegate.changeAmount(pointsToSend: 0, newMoneyAfter: mRewardsMobile.total_money, newPointsAfter: mRewardsMobile.total_points, moneyValueTosend: 0)
        }*/
    }
    
    @IBAction func OnEnterPhoneNumber(_ sender: Any) {
        print(tfContact.text ?? "")
        self.delegate.onAddedPhone(phone: tfContact.text ?? "")
    }
    
    
    @IBAction func onSendPointsToContact(_ sender: Any) {
        self.delegate.tranferToFriend()
    }

}
