//
//  BodyTransferPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol  BodyTransferPointsDelegate{
    func changeAmount(pointsToSend: Double, newMoneyAfter: Double, newPointsAfter:Double,
                      moneyValueTosend: Double, taxValue: Int, moneyTax: Double)
    func invalidPoints(message:String)
    func tranferToFriend()
    func onAddedPhone(phone: String)
}

class BodyTransferPointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tfContact: UITextField!
    @IBOutlet weak var viewEst: UIView!
    @IBOutlet weak var lblEst: UILabel!
    @IBOutlet weak var viewSelectEst: UIView!
    @IBOutlet weak var lblAmountAlready: UILabel!
    @IBOutlet weak var btnTransferPoints: UIButton!
    @IBOutlet weak var tfEnterPoints: UITextField!
    @IBOutlet weak var lblPriceTransaction: UILabel!
    @IBOutlet weak var newMoneyAfterTransaction: UILabel!
    @IBOutlet weak var viewContainerEst: UIView!

    
    var mTransferCommission: TransferCommission!
    var mRewardsMobile: RewardsMobile!
    var delegate: BodyTransferPointsDelegate!
    var idSelected = 0
    var pointEstablishmentSelect = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewEst.clipsToBounds = true
        viewEst.layer.cornerRadius = 20
        viewEst.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        viewSelectEst.clipsToBounds = true
        viewSelectEst.layer.cornerRadius = 20
        viewSelectEst.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func OnSelectEst(_ sender: Any) {
    }
    
    @IBAction func OntEXTwATCHER(_ sender: Any) {
        print(tfEnterPoints.text!.count)
        if tfEnterPoints.text != ""{
            if tfEnterPoints.text!.hasPrefix("0")  {
            self.delegate.invalidPoints(message: "El valor de los puntos a transferir no puede iniciar en cero.")
            tfEnterPoints.text = ""
            }else if idSelected == 0{
                self.delegate.invalidPoints(message: "Para ingresar los puntos que deseas transferir, selecciona primero el comercio de tu interés.")
                tfEnterPoints.text = ""
            }else{
                let points = Double(tfEnterPoints.text?.replacingOccurrences(of: ",", with: "") ?? "0") ?? 0
                
                let myTax = points * mTransferCommission.comision_transferencia
                let taxInt = floor(myTax)
                let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio
//                print("El impuesto para esta transaccion es: \(taxInt) puntos")
//                print("El valor del impuesto es \(moneyTax)")
                
                let newValuePoints = points + taxInt
                //newMoneyAfterTransaction.isHidden = false
                if newValuePoints.isLess(than: mRewardsMobile.total_points){
                    let myTotalPoints = mRewardsMobile.total_points ?? 0
                   // let myTotalPoints = pointEstablishmentSelect
                    print("Tengo: \(myTotalPoints) puntos" )
                    let valueOfMyPontsInMoney = mRewardsMobile.total_money ?? 0
//                    print("Todos mis puntos valen: \(valueOfMyPontsInMoney) pesos")
                    
//                    print("quiero reglar \(points) puntos")
                    let valueOfMyPointstoGif = points * mTransferCommission.tipo_cambio
//                    print("estos puntos valen \(valueOfMyPointstoGif) en pesos")
                    
                    let remindPointsAfterGif = myTotalPoints - (points + taxInt)
//                    print("los puntos que me van a quedar son \(remindPointsAfterGif)")
                    let remindMoneyAfterGif = valueOfMyPontsInMoney - (valueOfMyPointstoGif + moneyTax)
//                    print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")
                    
                    lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                    
                    newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: remindPointsAfterGif)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: remindMoneyAfterGif))"
                    delegate.changeAmount(pointsToSend: points, newMoneyAfter: remindMoneyAfterGif, newPointsAfter: remindPointsAfterGif, moneyValueTosend: valueOfMyPointstoGif, taxValue: 0, moneyTax: 0)
                    
                }else{
                    //tfEnterPoints.text = "0"
                    let myTax = mRewardsMobile.total_points * mTransferCommission.comision_transferencia // comision
                    let taxInt = floor(myTax) //se pasa a entero los puntos
                    let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio // sacamos el total de los puntos
//                    print("El impuesto para esta transaccion es: \(taxInt) puntos")
//                    print("El valor del impuesto es \(moneyTax)")
                    
                    /*let newValuePoints = mRewardsMobile.total_points - taxInt //puntos que puedo dar como maximo
                    let valueOfMyPointstoGif = newValuePoints * mTransferCommission.tipo_cambio*/
                    
                    tfEnterPoints.text = CustomsFuncs.numberFormat(value: mRewardsMobile.total_points)
                    lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                    
                    newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: 0.0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0.0))"
                    
                    delegate.changeAmount(pointsToSend: mRewardsMobile.total_points, newMoneyAfter: 0, newPointsAfter: 0, moneyValueTosend: mRewardsMobile.total_money, taxValue: 0, moneyTax: 0)
                }
            }
        }else{
            //newMoneyAfterTransaction.isHidden = true
            newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mRewardsMobile.total_money ?? 0))"
            delegate.changeAmount(pointsToSend: 0, newMoneyAfter: mRewardsMobile.total_money, newPointsAfter: mRewardsMobile.total_points, moneyValueTosend: 0, taxValue: 0, moneyTax: 0)
        }
    }
    
    @IBAction func onTransferPointsToContact(_ sender: Any) {
        self.delegate.tranferToFriend()
    }
    
    @IBAction func OnEnterNumberPhone(_ sender: Any) {
        self.delegate.onAddedPhone(phone: tfContact.text ?? "")
        
    }
    
}
