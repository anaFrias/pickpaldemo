//
//  LoadMoreEstTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/9/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol LoadMoreEstDelegate {
    func onLoadMore()
}

class LoadMoreEstTableViewCell: UITableViewCell {

    var delegate: LoadMoreEstDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func OnLoadMoreAction(_ sender: Any) {
        self.delegate.onLoadMore()
    }
    

}
