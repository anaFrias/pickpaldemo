//
//  EstTransferTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/8/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol EstTransferDelgate {
    func onSelectEst(id: Int, select: Bool)
}

class EstTransferTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var btnSelectItem: UIButton!
    
    var delegate: EstTransferDelgate!
    var mid: Int!
    var isSelect: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onSelectedEst(_ sender: Any) {
        let sel = self.isSelect ? false : true
        self.delegate.onSelectEst(id: mid, select: sel)
    }
    

}
