//
//  ServicesPickPalTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/13/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol ServicesDelegate {
    func goToNavigation(acttion: Int)
}

class ServicesPickPalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnFoodAndDrinks: UIButton!
    @IBOutlet weak var btnPromos: UIButton!
    @IBOutlet weak var btnGuia: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    @IBOutlet weak var ctlServiceRigth: NSLayoutConstraint!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    var delegate: ServicesDelegate!
    var singleInfo: SingleReward!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnFoodAndDrinks.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btnPromos.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btnGuia.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        btnPoints.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        if #available(iOS 13, *) {
            ctlServiceRigth.constant = 50
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func onOpenFAD(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[0])
        delegate.goToNavigation(acttion: image)
    }
    
    @IBAction func onOpenPromos(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[1])
        delegate.goToNavigation(acttion: image)
    }
    
    @IBAction func onOpenGuide(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[2])
        delegate.goToNavigation(acttion: image)
    }
    
    @IBAction func onOpenPointsMobile(_ sender: Any) {
        
    }

}
