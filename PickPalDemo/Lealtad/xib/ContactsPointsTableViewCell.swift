//
//  ContactsPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol ContactsPointsDelegate {
    func onSelectUser(number: String, name: String)
}

class ContactsPointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNumberPhone: UILabel!
    
    var name = String()
    var phone = String()
    var delegate: ContactsPointsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    @IBAction func onSelectContact(_ sender: Any) {
        self.delegate.onSelectUser(number: self.phone, name: self.name)
    }
    
}
