//
//  InterchangePointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/12/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol InterchangePointsDelegate {
    func onOpenSingleEstablishment()
    func onOpenMultiplesEstablishments()
    func onChangeInterchangeEnter(valeu: Double, moneyPoint: Double, taxValue: Int, moneyTax: Double, pointsAfterGift: Double, pointsAfterGiftMoney: Double)
    func invalidPoints(message:String)
    func onInitInterchange()
}

class InterchangePointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewEst: UIView!
    @IBOutlet weak var lblEst: UILabel!
    @IBOutlet weak var viewSelectEst: UIView!
    @IBOutlet weak var viewEstToTransfer: UIView!
    @IBOutlet weak var lblEstToTransfer: UILabel!
    @IBOutlet weak var viewSelectEstToTransfer: UIView!
    @IBOutlet weak var lblAmountAlready: UILabel!
    @IBOutlet weak var tfEnterPoints: UITextField!
    @IBOutlet weak var lblPriceTransaction: UILabel!
    @IBOutlet weak var btnTransferPoints: UIButton!
    @IBOutlet weak var newMoneyAfterTransaction: UILabel!
    
    var delegate: InterchangePointsDelegate!
    var mTransferCommission: TransferCommission!
    var mRewardsMobile: RewardsMobile!
    var idSelected = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewEst.clipsToBounds = true
        viewEst.layer.cornerRadius = 20
        viewEst.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        viewEstToTransfer.clipsToBounds = true
        viewEstToTransfer.layer.cornerRadius = 20
        viewEstToTransfer.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        viewSelectEst.clipsToBounds = true
        viewSelectEst.layer.cornerRadius = 20
        viewSelectEst.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        viewSelectEstToTransfer.clipsToBounds = true
        viewSelectEstToTransfer.layer.cornerRadius = 20
        viewSelectEstToTransfer.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func OnClickSelectEst(_ sender: Any) {
        self.delegate.onOpenSingleEstablishment()
    }
    
    @IBAction func OnClickSelectEstToTransfer(_ sender: Any) {
        self.delegate.onOpenMultiplesEstablishments()
    }
    
    
    @IBAction func OnChageEditing(_ sender: Any) {
//        print(tfEnterPoints.text!.count)
        if tfEnterPoints.text != ""{
            if tfEnterPoints.text!.hasPrefix("0")  {
                self.delegate.invalidPoints(message: "El valor de los puntos a intercambiar no puede iniciar en cero.")
                tfEnterPoints.text = ""
            }else if idSelected == 0{
                self.delegate.invalidPoints(message: "Para ingresar los puntos que deseas intercambiar, selcciona primero el comercio de tu interés.")
                tfEnterPoints.text = ""
            }else{
                let points = Double(tfEnterPoints.text?.replacingOccurrences(of: ",", with: "") ?? "0") ?? 0
                
                let myTax = points * mTransferCommission.comision_intercambio
                let taxInt = floor(myTax)
                let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio
//                print("El impuesto para esta transaccion es: \(taxInt) puntos")
//                print("El valor del impuesto es \(moneyTax)")
                
                let newValuePoints = points + taxInt
                newMoneyAfterTransaction.isHidden = true
                if newValuePoints.isLess(than: mRewardsMobile.total_points){
                    let myTotalPoints = mRewardsMobile.total_points ?? 0
//                    print("Tengo: \(myTotalPoints) puntos" )
                    let valueOfMyPontsInMoney = mRewardsMobile.total_money
//                    print("Todos mis puntos valen: \(valueOfMyPontsInMoney ?? 0.0) pesos")
                    
//                    print("quiero reglar \(points) puntos")
                    let valueOfMyPointstoGif = points * mTransferCommission.tipo_cambio
//                    print("estos puntos valen \(valueOfMyPointstoGif) en pesos")
                    
                    let remindPointsAfterGif = myTotalPoints - (points + taxInt)
//                    print("los puntos que me van a quedar son \(remindPointsAfterGif)")
                    let remindMoneyAfterGif = (valueOfMyPontsInMoney ?? 0.0) - (valueOfMyPointstoGif + moneyTax)
//                    print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")
                    
                    lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                    
                    newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: remindPointsAfterGif)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: remindMoneyAfterGif))"
                    
                    self.delegate.onChangeInterchangeEnter(valeu: points, moneyPoint: valueOfMyPointstoGif, taxValue: 0, moneyTax: 0, pointsAfterGift: remindPointsAfterGif, pointsAfterGiftMoney: remindMoneyAfterGif)
                    
                    
                    //delegate.changeAmount(pointsToSend: points, newMoneyAfter: remindMoneyAfterGif, newPointsAfter: remindPointsAfterGif, moneyValueTosend: valueOfMyPointstoGif, taxValue: Int(taxInt), moneyTax: moneyTax)
                    
                }else{
                    let myTax = mRewardsMobile.total_points * mTransferCommission.comision_intercambio // comision
                    let taxInt = floor(myTax) //se pasa a entero los puntos
                    let moneyTax = Double(taxInt) * mTransferCommission.tipo_cambio // sacamos el total de los puntos
//                    print("El impuesto para esta transaccion es: \(taxInt) puntos")
//                    print("El valor del impuesto es \(moneyTax)")
                    
                    let newValuePoints = mRewardsMobile.total_points - taxInt //puntos que puedo dar como maximo
                    let valueOfMyPointstoGif = newValuePoints * mTransferCommission.tipo_cambio
                    
                    tfEnterPoints.text = CustomsFuncs.numberFormat(value: mRewardsMobile.total_points)
                    lblPriceTransaction.text = "Costo de transacción: \(CustomsFuncs.numberFormat(value: taxInt)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTax))"
                    
                    let remindPointsAfterGif = mRewardsMobile.total_points - (newValuePoints + taxInt)
//                    print("los puntos que me van a quedar son \(remindPointsAfterGif)")
                    let remindMoneyAfterGif = mRewardsMobile.total_money - (valueOfMyPointstoGif + moneyTax)
//                    print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")
                    
                    newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: remindPointsAfterGif)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: remindMoneyAfterGif))"
                    
                    self.delegate.onChangeInterchangeEnter(valeu: mRewardsMobile.total_points, moneyPoint: mRewardsMobile.total_money, taxValue: 0, moneyTax: 0, pointsAfterGift: remindPointsAfterGif, pointsAfterGiftMoney: remindMoneyAfterGif)
                    //delegate.changeAmount(pointsToSend: newValuePoints, newMoneyAfter: 0, newPointsAfter: 0, moneyValueTosend: valueOfMyPointstoGif, taxValue: Int(taxInt), moneyTax: moneyTax)
                }
            }
        }else{
            self.delegate.onChangeInterchangeEnter(valeu: 0, moneyPoint: 0, taxValue: 0, moneyTax: 0, pointsAfterGift: 0, pointsAfterGiftMoney: 0)
            newMoneyAfterTransaction.isHidden = true
            //delegate.changeAmount(pointsToSend: 0, newMoneyAfter: mRewardsMobile.total_money, newPointsAfter: mRewardsMobile.total_points, moneyValueTosend: 0, taxValue: 0, moneyTax: 0)
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /*if tfEnterPoints.text != ""{
         let points = Double(tfEnterPoints.text ?? "0") ?? 0
         if points.isLess(than: mRewardsMobile.total_points){
         self.delegate.onChangeInterchangeEnter(valeu: points)
         /*let myTotalPoints = mRewardsMobile.total_points ?? 0
         print("Tengo: \(myTotalPoints) puntos" )
         let valueOfMyPontsInMoney = mRewardsMobile.total_points / mTransferCommission.tipo_cambio
         print("Todos mis puntos valen: \(valueOfMyPontsInMoney) pesos")
         
         print("quiero reglar \(points) puntos")
         let valueOfMyPointstoGif = points / mTransferCommission.tipo_cambio
         print("estos puntos valen \(valueOfMyPointstoGif) en pesos")
         
         let remindPointsAfterGif = myTotalPoints - points
         print("los puntos que me van a quedar son \(remindPointsAfterGif)")
         let remindMoneyAfterGif = valueOfMyPontsInMoney - valueOfMyPointstoGif
         print("el dinero que me quedara sera \(remindMoneyAfterGif) pesos")*/
         
         //delegate.changeAmount(pointsToSend: points, newMoneyAfter: remindMoneyAfterGif, newPointsAfter: remindPointsAfterGif, moneyValueTosend: valueOfMyPointstoGif)
         
         
         }else{
         tfEnterPoints.text = "0"
         self.delegate.onChangeInterchangeEnter(valeu: mRewardsMobile.total_points)
         //self.delegate.invalidPoints()
         }
         }*/
    }
    
    @IBAction func onContinue(_ sender: Any) {
        self.delegate.onInitInterchange()
    }
    
    
}
