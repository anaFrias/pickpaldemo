//
//  OptionsPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/20/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol OptionsPointsDelegate {
    
    func OnShowHistory(isShowing: Bool)
    
    func OnShowTransferPoints(isShowing: Bool)
    
    func OnShowInterchangePonts(isShowing: Bool)
    
    func OnShowInterchangeInProces(isShowing: Bool)
}

class OptionsPointsTableViewCell: UITableViewCell {
    
    //@IBOutlet weak var mapIcon: UIButton!
    //@IBOutlet weak var infoIcon: UIButton!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblTransferPoints: UILabel!
    @IBOutlet weak var lblInterchangePoints: UILabel!
    @IBOutlet weak var lblInterchangeInProcess: UILabel!
    
    var isShowingHistory = false
    var isShowingTransfer = false
    var isShowingInterchange = false
    var isShowingInterchangeInProcess = false
    var isShowingMap = false
    var isShowingInfo = false
    
    var delegate: OptionsPointsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func showMap(_ sender: Any) {
        //self.delegate.OnSelectedMap()
        //mapIcon.setImage(UIImage(named: "selectedMap"), for: .normal)
        lblTransferPoints.alpha = 1
        lblInterchangePoints.alpha = 1
        lblInterchangeInProcess.alpha = 1
        lblHistory.textColor = .black
    }
    
    @IBAction func showInfo(_ sender: Any) {
        //self.delegate.OnSelectedInfo()
        //infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        lblTransferPoints.alpha = 1
        lblInterchangePoints.alpha = 1
        lblInterchangeInProcess.alpha = 1
        lblHistory.textColor = .black
    }
    
    @IBAction func OnShowHistory(_ sender: Any) {
        if !isShowingHistory{
            self.delegate.OnShowHistory(isShowing: true)
            lblHistory.textColor = UIColor(red: 255, green: 0, blue: 216, alpha: 1.0)
            lblTransferPoints.alpha = 1
            lblInterchangePoints.alpha = 1
            lblInterchangeInProcess.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
            //mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
            
        }else{
            self.delegate.OnShowHistory(isShowing: false)
            lblHistory.textColor = .black
            //infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        }
    }
    
    @IBAction func OnShowTransferPoints(_ sender: Any) {
        if !isShowingTransfer{
            self.delegate.OnShowTransferPoints(isShowing: true)
            lblTransferPoints.alpha = 0
            lblHistory.textColor = .black
            lblInterchangePoints.alpha = 1
            lblInterchangeInProcess.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
            //mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
        }else{
            self.delegate.OnShowTransferPoints(isShowing: false)
            lblTransferPoints.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        }
    }
    
    @IBAction func onShowInterchangePoints(_ sender: Any) {
        if !isShowingInterchange{
            self.delegate.OnShowInterchangePonts(isShowing: true)
            lblInterchangePoints.alpha = 0
            lblTransferPoints.alpha = 1
            lblHistory.textColor = .black
            lblInterchangeInProcess.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
            //mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
        }else{
            self.delegate.OnShowInterchangePonts(isShowing: false)
            lblInterchangePoints.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        }
    }
    
    @IBAction func onShowInterchangeInProcess(_ sender: Any) {
        if !isShowingInterchangeInProcess{
            self.delegate.OnShowInterchangeInProces(isShowing: true)
            lblInterchangeInProcess.alpha = 0
            lblTransferPoints.alpha = 1
            lblInterchangePoints.alpha = 1
            lblHistory.textColor = .black
            //infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
            //mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
        }else{
            self.delegate.OnShowInterchangeInProces(isShowing: false)
            lblInterchangeInProcess.alpha = 1
            //infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        }
    }
    
}
