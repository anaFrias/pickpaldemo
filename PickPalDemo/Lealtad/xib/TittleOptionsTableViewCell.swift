//
//  TittleOptionsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/5/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TittleOptionsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitles: UILabel!
    @IBOutlet weak var viewDivisorTitle: UIView!
    @IBOutlet weak var ctlTopLabel: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
