//
//  MapInfoTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 7/21/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol MapInfoDelegate {
    func OnSelectedInfo()
    
    func OnSelectedMap()
}

class MapInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var mapIcon: UIButton!
    @IBOutlet weak var infoIcon: UIButton!
    @IBOutlet weak var lblNameStablishment: UILabel!
    
    var delegate: MapInfoDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func showMap(_ sender: Any) {
        self.delegate.OnSelectedMap()
        infoIcon.setImage(UIImage(named: "iconInfoDisabled"), for: .normal)
        mapIcon.setImage(UIImage(named: "selectedMap"), for: .normal)
        /*lblTransferPoints.alpha = 1
        lblInterchangePoints.alpha = 1
        lblInterchangeInProcess.alpha = 1
        lblHistory.textColor = .black*/
    }
    
    @IBAction func showInfo(_ sender: Any) {
        self.delegate.OnSelectedInfo()
        infoIcon.setImage(UIImage(named: "iconInfo"), for: .normal)
        mapIcon.setImage(UIImage(named: "iconLocation"), for: .normal)
        /*lblTransferPoints.alpha = 1
        lblInterchangePoints.alpha = 1
        lblInterchangeInProcess.alpha = 1
        lblHistory.textColor = .black*/
    }

}
