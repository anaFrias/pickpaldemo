//
//  MapPointsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/4/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

import GoogleMaps
import MapKit


protocol MapsCellDelegate {
    func openMaps()
}

class MapPointsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapa: MKMapView!
    var delegate: MapsCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btnOpenMap(_ sender: Any) {
        
        self.delegate.openMaps()
        
    }
    
}
