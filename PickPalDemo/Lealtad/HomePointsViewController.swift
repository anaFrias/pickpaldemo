//
//  HomePointsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/6/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
// SinglePointsViewController

import UIKit
import Nuke
import Contacts
import OneSignal
import SQLite
import Foundation
import CoreLocation


class HomePointsViewController: UIViewController, PointsWSDelegate, AlertAdsDelegate, userDelegate, EstPointDelegate, ContactsLealtadDelegate, BodyTransferPointsDelegate, InterchangePointsDelegate, MultipleEstsDelegate, TransferInterchangeDelegate, selectedSearchValue, googleWSDelegate, CLLocationManagerDelegate, AlertAcceptRequestExchangeDelegate, establishmentDelegate{

     let alertintercambio = AlertAcceptRequestExchange(nibName: "AlertAcceptRequestExchange", bundle: nil)
    
    @IBOutlet weak var lblHiUser: UILabel!
    @IBOutlet weak var lblMyMoney: UILabel!
    @IBOutlet weak var titleOptions: UILabel!
    @IBOutlet weak var contentViewTitleOptions: UIView!
    @IBOutlet weak var collectionHome: UICollectionView!
    
    //botonera superior historial
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var tableHistoryPoints: UITableView!
    
    //botonera superior Transferencia
    @IBOutlet weak var viewTransfer: UIView!
    @IBOutlet weak var lblTranfer: UILabel!
    @IBOutlet weak var tableTransferPoints: UITableView!
    
    //botonera superior Intercambio
    @IBOutlet weak var viewInterChange: UIView!
    @IBOutlet weak var lblInterchange: UILabel!
    @IBOutlet weak var tableInterchange: UITableView!
    
    //Intercambios en proceso
    @IBOutlet weak var tableInterchangeInProcess: UITableView!
    @IBOutlet weak var lblInterchangeInProcess: UILabel!
    @IBOutlet weak var cotainerNavigationBottom: UIView!
    
    //Constraints
    @IBOutlet weak var ctlTopHistoryTable: NSLayoutConstraint!
    @IBOutlet weak var ctlTopInterchangeInProcess: NSLayoutConstraint!
    @IBOutlet weak var ctlTopInterchange: NSLayoutConstraint!
    @IBOutlet weak var ctlTopDivisor: NSLayoutConstraint!
    @IBOutlet weak var ctlToDotNoUsePoints: NSLayoutConstraint!
    
    var pointWs = PointsWS()
    var usersWs = UserWS()
    var gws = GoogleServices()
    var establishment = EstablishmentWS()
    
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    
    //    Location variables
    var locationManager = CLLocationManager()
    var valueState = String()
    
    var myHistoryPoints = [ViewMyPoints]()
    var mListEstablishments = [RewardsMobile]()
    var idsSelected = [RewardsMobile]()
    var listPendingExchange = [PendingExchange]()
    var multiplesIdsSelected = [RewardsMobile]()
    var mTransferComission = TransferCommission()
    var listTransfer = [String]()
    var listInterchange = [String]()
    var mListBackGround = ["ic_tarjet_golden", "ic_tarjet_green", "ic_tarjet_purple", "ic_tarjet_red"]
    
    var isHistory: Bool = false
    var isTransfer: Bool = false
    var isEditPhone: Bool = false
    var isInterchange: Bool = false
    var isInterchangeInProcess: Bool = false
    var isCleanFields: Bool = false
    
    var mMoneyValueToSend = 0.0
    var valueTaxPoints = 0
    
    var textToEstSelect = "Establecimiento"
    var textToMultipleEstSelect = "Establecimiento(s)"
    var textUserSelected = ""
    var textPhoneSelected = ""
    var textPointsEnter = "0"
    var newMoneyAfterSend = ""
    var newPointsAfterSend = ""
    
    var valueInterchangePoints = 0
    var valueMoneyPointsInterchange = 0.0
    var pointsAfterGiftInterhange = 0.0
    var pointsMoneyAfterGiftInterhange = 0.0
    var moneyTaxValue = 0.0
    
    var contacts = [CNContact]()
    
    var listEstablishmentsSelect = [RewardsMobile]()
    var listllEstablishmentAround = [RewardsMobile]()
    var totalToExchange = Int()
    
    var pushExchange = PushExchange()
    var isPushExchange = false
    var dinamicHeightFinish: CGFloat = 240
    var dinamicHeightGeneral: CGFloat = 220
    var dinamicHeightInProcess: CGFloat = 270
    
    var indexPath = IndexPath()
    var expandCell = true
    var countList = 0
    
    var expandedIndexSet : IndexSet = []
    var infoToExchange = TransferDetails()
    var tipoIntercambio = String()
    var cancelAmount = Double()
    var cancelEstablishment = String()
    var openSingle = false
    var idSinglepontis = Int()
    
    @IBOutlet weak var lblMessagePonits: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegates()
        
        
      /*  ctlTopHistoryTable.constant = -41
        ctlTopInterchange.constant = -41
        ctlTopInterchangeInProcess.constant = -41*/
        
        //Prueba merge request
        
        gws.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            let clocation = locationManager.location
            if clocation?.coordinate.latitude == nil{
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                    locationManager.requestWhenInUseAuthorization()
                }else{
                    print("Si esta activado")
                    
                }
            }else{
                userLat = (clocation?.coordinate.latitude)!
                userLng = (clocation?.coordinate.longitude)!
                print(userLat)
                print(userLng)
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
            }
            
        }
        LoadingOverlay.shared.showOverlay(view: self.view)
        pointWs.getTotalPoinsMobile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        pointWs.getRewards(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        self.tableHistoryPoints.separatorStyle = UITableViewCellSeparatorStyle.none
        lblHiUser.attributedText = CustomsFuncs.getHiUser()
        
        pullContacts()
         pointWs.getUserEstablishmentsPoints()
         pointWs.getAllEstablishmentAround()
        
        
        if isPushExchange{
            
            print("despues del pa push", pushExchange.identifier)
            
            pointWs.getStatusRequestRxchange(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, establishment_from: pushExchange.establishmentid_to, request_id: pushExchange.identifier)
        }
        
//        ALERT INTERCAMBIO EJEMPLO
        
//        infoToExchange.AboutMe = "Oscar"
//        infoToExchange.aboutOther = "Joss"
//        infoToExchange.establishmentFromName = "EL MUNDO DE LATECNOLOÍA QUERETARO"
//        infoToExchange.establishmentTomName = "Tacos el pata"
//        infoToExchange.establishmentTomStreet = "direccion del local numero uno ANÁHUAC"
//        infoToExchange.establishmentFromStreet = "Dirección del local numero dos"
//        infoToExchange.totalToExchange = 100
//        alertAceptarIntercambio()
        
    
        self.view.isUserInteractionEnabled = true
    }
    
    func pullContacts() {
            let store = CNContactStore()
            store.requestAccess(for: CNEntityType.contacts) { hasPermission, error in
                if error != nil {
                    print(error!)
                }
            }
            
            if (CNContactStore.authorizationStatus(for: CNEntityType.contacts) == .authorized) {
                let request = CNContactFetchRequest(keysToFetch: [
                    CNContactGivenNameKey as CNKeyDescriptor,
                    CNContactFamilyNameKey as CNKeyDescriptor,
                    CNContactEmailAddressesKey as CNKeyDescriptor,
                    CNContactPhoneNumbersKey as CNKeyDescriptor
                ])
                
                do {
                    try store.enumerateContacts(with: request) {
                        (contact, stop) in
                        self.contacts.append(contact)
                        for data in contact.phoneNumbers{
                            print(data.value)
                        }
                    }
                } catch {
                    print("error: \(error)")
                }
            }else{
                showMessage(title: UserDefaults.standard.object(forKey: "first_name") as! String, message: "PickPal quiere acceder a tus contactos para realizar transferencias e intercambio de puntos móviles. Ve a ajustes para permitir acceder a tus contactos", toHome: true)
            }
        }
    
    override func viewWillAppear(_ animated: Bool) {
      //  super.viewWillAppear(false)
        
         //   pointWs.getTotalPoinsMobile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
          //  pointWs.getRewards(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
    }
    
    
    func didSuccessviewMyavailableEstablishmentsPoints(info: [RewardsMobile]) {
        
        // Oscar Lista de comercios
//        listEstablishmentsSelect = info
        
        if openSingle{
            
            var objet = RewardsMobile()
            for data in listllEstablishmentAround{
                if data.establishment_id == idSinglepontis{
                    objet = data
                    break
                }
            }
            
            let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "SinglePoints") as! SinglePointsViewController
            newVC.mEstabId = idSinglepontis
            newVC.mRewardsMobile = objet
            newVC.estabName = objet.establishment_name!
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
            
    
        }
        
        
    }
    
    func didSuccessAllEstablishmentAround(info: [RewardsMobile]) {
           
           listllEstablishmentAround = info
   }
    
    
    
    
    func didSuccessGetGeolocation(location: String, state: String) {
        let geoCoder = CLGeocoder()
        let location2 = CLLocation(latitude: userLat, longitude: userLng)
        geoCoder.reverseGeocodeLocation(location2, completionHandler: { (placemarks, error) -> Void in
            if let placeMark = placemarks?[0] {
                if let state = placeMark.administrativeArea {
                    self.valueState = state
                    UserDefaults.standard.set(state, forKey: "state")
                }else{
                  if let st = UserDefaults.standard.object(forKey: "state") as? String {
                        self.valueState = st
                    }
                }
            }
            
        })
    }
    
    @IBAction func search(_ sender: Any) {
        print("search")
        let newXIB = SearchViewController(nibName: "SearchViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.isFromPromos = false
        newXIB.state = "self.valueState"
        newXIB.municipality = "self.valueInfo"
        newXIB.mListEstablishments = self.mListEstablishments
        newXIB.from = Constants.PICKPAL_PUNTOS
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func back(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goOrdersLatest(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = false
        newVC.whereFrom = Constants.PICKPAL_PUNTOS
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goHome(_ sender: Any) {
        isHistory = false
        tableHistoryPoints.isHidden = true
        tableInterchange.isHidden = true
        tableInterchangeInProcess.isHidden = true
        collectionHome.isHidden = false
        collectionHome.isHidden = false
        lblHistory.textColor = .black
        lblTranfer.alpha = 1
        lblInterchange.alpha = 1
        lblInterchangeInProcess.alpha = 1
        
    }
    
    
    @IBAction func goSettings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = false
                newVC.isFromGuide = false
                newVC.whereFrom = Constants.PICKPAL_PUNTOS
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = false
            newVC.isFromGuide = false
            newVC.whereFrom = Constants.PICKPAL_PUNTOS
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func selectedSearchId(id: Int) {
        print(id)
        var objet = RewardsMobile()
        for data in listllEstablishmentAround{
            if data.establishment_id == id{
                objet = data
                break
            }
        }
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SinglePoints") as! SinglePointsViewController
        newVC.mEstabId = id
        newVC.mRewardsMobile = objet
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    @IBAction func onOpenEstablishmentsList(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "estList") as! EstablishmentsViewController
        newVC.mListEstablishments = listEstablishmentsSelect
        newVC.delegate = self
        newVC.multiplesIdsSelected = self.multiplesIdsSelected
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func onOpenContacts(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "contacts") as! ContactsViewController
        //newVC.contacts = contacts
        newVC.delegate = self
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func OnOpenHistory(_ sender: Any) {
        isCleanFields = true
        isEditPhone = false
        idsSelected.removeAll()
        textToEstSelect = "Establecimiento"
        self.tableHistoryPoints.contentOffset.y = 0
        if !isHistory{
            //titleOptions.attributedText = CustomsFuncs.getAttributedString(title1: "Historial de movimientos de ", title2: "Puntos Móviles")
            setSmallIconTitle(titleIcon: "ic_history_selected", titleTextPickpal: CustomsFuncs.getAttributedString(title1: " Historial de movimientos de \n", title2: "Puntos Móviles"))
            //imageOptions.image = UIImage(named: "ic_history_selected")
            ctlTopDivisor.constant = -1
            ctlToDotNoUsePoints.constant = 8
            isHistory = true
            isTransfer = false
            isInterchange = false
            isInterchangeInProcess = false
            lblHistory.textColor = UIColor(red: 255, green: 0, blue: 216, alpha: 1.0)
            lblTranfer.alpha = 1
            lblInterchange.alpha = 1
            lblInterchangeInProcess.alpha = 1
            viewHistory.clipsToBounds = false
            viewHistory.layer.masksToBounds = false
            viewHistory.layer.shadowColor = UIColor.black.cgColor
            viewHistory.layer.shadowOpacity = 1
            viewHistory.layer.shadowOffset = .zero
            viewHistory.layer.shadowRadius = 10
            viewHistory.layer.cornerRadius = 10
            LoadingOverlay.shared.showOverlay(view: self.view)
            usersWs.viewMyPoints(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            
       
            
        }else{
            isHistory = false
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = true
            tableInterchangeInProcess.isHidden = true
            collectionHome.isHidden = false
            lblHistory.textColor = .black
            /*lblHistory.textColor = .black
             lblInterchange.alpha = 1*/
        }
    }
    
    @IBAction func OnOpenTranferPoints(_ sender: Any) {
        idsSelected.removeAll()
        lblMessagePonits.isHidden = false
        isEditPhone = false
        multiplesIdsSelected.removeAll()
        textToEstSelect = "Establecimiento"
        if !isTransfer{
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPriceMoves(from: Constants.typeTranfer)
        }else{
            isCleanFields = true
            isTransfer = false
            lblTranfer.alpha = 1
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = true
            tableInterchangeInProcess.isHidden = true
            collectionHome.isHidden = false
            lblInterchange.alpha = 1
            /*lblHistory.textColor = .black
             lblInterchange.alpha = 1*/
        }
    }
    
    @IBAction func OnOpenInterchange(_ sender: Any) {
        isEditPhone = false
        isCleanFields = true
        lblMessagePonits.isHidden = true
        multiplesIdsSelected.removeAll()
        textToEstSelect = "Establecimiento"
        valueTaxPoints = 0
        textUserSelected = ""
        textPhoneSelected = ""
        textPointsEnter = ""
        if !isInterchange{
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPriceMoves(from: Constants.typeInterchange)
        }else{
            isInterchange = false
            lblInterchange.alpha = 1
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = true
            tableInterchangeInProcess.isHidden = true
            collectionHome.isHidden = false
        }
        valueInterchangePoints = 0
        idsSelected.removeAll()
        multiplesIdsSelected.removeAll()
        textToMultipleEstSelect = "Establecimiento(s)"
        tableInterchange.reloadData()
    }
    
    @IBAction func OnOpenInterchangeInProcess(_ sender: Any) {
        isCleanFields = true
        isEditPhone = false
        lblMessagePonits.isHidden = true
        actionInterchangeInProcess()
    }
    
    func actionInterchangeInProcess(){
        if !isInterchangeInProcess{
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointWs.getPendigExchange(user_id:String( UserDefaults.standard.object(forKey: "client_id") as! Int))
        }else{
            isInterchangeInProcess = false
            lblInterchangeInProcess.alpha = 1
            tableInterchangeInProcess.isHidden = true
            collectionHome.isHidden = false
        }
       
    }
    
    
    func openHistory(){
        
        isCleanFields = true
        isEditPhone = false
        idsSelected.removeAll()
        textToEstSelect = "Establecimiento"
        if !isHistory{
            //titleOptions.attributedText = CustomsFuncs.getAttributedString(title1: "Historial de movimientos de ", title2: "Puntos Móviles")
            setSmallIconTitle(titleIcon: "ic_history_selected", titleTextPickpal: CustomsFuncs.getAttributedString(title1: " Historial de movimientos de \n", title2: "Puntos Móviles"))
            //imageOptions.image = UIImage(named: "ic_history_selected")
            ctlTopDivisor.constant = -1
            ctlToDotNoUsePoints.constant = 8
            isHistory = true
            isTransfer = false
            isInterchange = false
            isInterchangeInProcess = false
            lblHistory.textColor = UIColor(red: 255, green: 0, blue: 216, alpha: 1.0)
            lblTranfer.alpha = 1
            lblInterchange.alpha = 1
            lblInterchangeInProcess.alpha = 1
            viewHistory.clipsToBounds = false
            viewHistory.layer.masksToBounds = false
            viewHistory.layer.shadowColor = UIColor.black.cgColor
            viewHistory.layer.shadowOpacity = 1
            viewHistory.layer.shadowOffset = .zero
            viewHistory.layer.shadowRadius = 10
            viewHistory.layer.cornerRadius = 10
            LoadingOverlay.shared.showOverlay(view: self.view)
            usersWs.viewMyPoints(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            
       
            
        }else{
            isHistory = false
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = true
            tableInterchangeInProcess.isHidden = true
            collectionHome.isHidden = false
            lblHistory.textColor = .black
            /*lblHistory.textColor = .black
             lblInterchange.alpha = 1*/
        }
        
    }
    
    
    func setDelegates(){
        //Web Services
        pointWs.delegate = self
        usersWs.delegate = self
        establishment.delegate = self
        //CardsMain
        collectionHome.dataSource = self
        collectionHome.delegate = self
        //History
        tableHistoryPoints.dataSource = self
        tableHistoryPoints.delegate = self
        tableHistoryPoints.isHidden = true
        //Transfer
        tableTransferPoints.dataSource = self
        tableTransferPoints.delegate = self
        tableTransferPoints.isHidden = true
        
        //Intercambio de puntos
        tableInterchange.isHidden = true
        
        //Intercambios pendientes
        tableInterchangeInProcess.isHidden = true
    }
    
    func showMessage(title: String, message: String, toHome: Bool = false, valueCall: Int = 1, whatsAppMesaage: Bool = false, backHome: Bool = false){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.mToHome = toHome
        newXIB.isFromHome = true
        newXIB.valueCall = valueCall
        newXIB.isFromAdd = true
        
        if whatsAppMesaage{
            newXIB.hiddenBtnWhats = false
            newXIB.msnWhatsApp = "Recibiste una Transferencia de Puntos Móviles. ¡Felicidades! \(UserDefaults.standard.object(forKey: "first_name") as? String ?? "") te acaba de transferir \(textPointsEnter) pts/ $\(mMoneyValueToSend) de \(idsSelected[0].establishment_name!). ¡Redímelos, Transfiérelos o Intercámbialos!, Descarga la App. de PickPal"
            
            openHistory()
        }
        
        newXIB.delegate = self
        
       // var menssageWhats = "Recibiste una Transferencia de Puntos Móviles. ¡Felicidades! \(UserDefaults.standard.object(forKey: "first_name") as! String) te acaba de transferir \(textPointsEnter) pts/ $\(mMoneyValueToSend) de \(idsSelected[0].establishment_name!). ¡Redímelos, Transfiérelos o Intercámbialos!"
        
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func setSmallIconTitle(titleIcon: String, titleTextPickpal: NSMutableAttributedString){
        // Create Attachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: titleIcon)
        // Set bound to reposition
        let imageOffsetY: CGFloat = -8.0
        imageAttachment.bounds = CGRect(x: -10, y: imageOffsetY, width: 25, height: 22)
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        // Add image to mutable string
        completeText.append(attachmentString)
        // Add your text to mutable string
        let textAfterIcon = titleTextPickpal
        completeText.append(textAfterIcon)
        self.titleOptions.textAlignment = .center
        self.titleOptions.attributedText = completeText
    }
}

//Solo funciones heredadas de los delegados
extension HomePointsViewController{
    
    func OnSetMultipleEst(ids: [RewardsMobile]) {
        textToMultipleEstSelect = ""
        multiplesIdsSelected = ids
        print(ids.count)
        var index = 0//tableInterchange.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        for data in ids{
            if index >= ids.count-1{
                textToMultipleEstSelect = textToMultipleEstSelect + data.establishment_name
            }else{
                textToMultipleEstSelect = textToMultipleEstSelect + data.establishment_name + " / "
            }
            index += 1
        }
        
        tableInterchange.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    }
    
    func onOpenSingleEstablishment() {
        for data in mListEstablishments{
            data.isSelect = false
        }
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "estList") as! EstablishmentsViewController
        newVC.mListEstablishments = listEstablishmentsSelect
        newVC.delegate = self
        newVC.multiplesIdsSelected = self.multiplesIdsSelected
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func onOpenMultiplesEstablishments() {
        if 0 < idsSelected.count{
            let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "multiples") as! MultipleEstsViewController
            newVC.mListEstablishments = listllEstablishmentAround
            newVC.delegate = self
            newVC.valueState = "QRO"//self.valueState
            newVC.isFirstTime = true
            newVC.idPrevious = idsSelected[0].establishment_id
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "Para elegir el o los comercios de los cuales quieres obtener puntos, primero selecciona el comercio del que deseas ceder o intercambiar puntos.", toHome: false)
        }
        
    }
    
    func didSuccessGetPoints(points: PointsMobile) {
        print(points)
        let formatted = CustomsFuncs.numberFormat(value: points.total_points ?? 0)
        let pointsAndMoney = "\(String(describing: formatted)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: points.total_money ?? 0.0))"
        
        lblMyMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Saldo de puntos móviles: ", title2: pointsAndMoney)
        
    }
    
    func didFailGetPoints(error: String, subtitle: String) {
        print(subtitle)
    }
    
    func didSuccessRewardsMobile(info: [RewardsMobile]) {
        LoadingOverlay.shared.hideOverlayView()
        
        listEstablishmentsSelect = info
        mListEstablishments = info
        if info.count > 0 {
            if isTransfer || isInterchange{
                if idsSelected.count > 0 {
                let idSel = idsSelected[0].establishment_id
                idsSelected.removeAll()
                
                for data in info{
                    if data.establishment_id == idSel{
                        idsSelected.append(data)
                        break
                    }
                }
                }
            }
            
            if isTransfer{
                self.textPointsEnter = ""
                self.textPhoneSelected = ""
                self.textUserSelected = ""
                self.isCleanFields = true
                tableTransferPoints.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }else if isInterchange{
                self.valueInterchangePoints = 0
                self.isCleanFields = true
                tableInterchange.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
            }else{
                var count = 0
                for data in mListEstablishments{
                    if count < 4{
                        data.backGroundColor = mListBackGround[count]
                    }else{
                        count = 0
                        data.backGroundColor = mListBackGround[count]
                    }
                    count = count + 1
                }
                
                collectionHome.reloadData()
            }
            //601466e77c08
        }else{
            showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "Aún no tienes Puntos Móviles. Haz compras en los comercios afiliados para ganar puntos.", toHome: true)
        }
    }
    
    func didFailRewardsMobile(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "Aún no tienes Puntos Móviles. Haz compras en los comercios afiliados para ganar puntos.", toHome: true)
    }
    
    func didSuccessViewMyPoints(viewMyPoints: [ViewMyPoints]) {
        LoadingOverlay.shared.hideOverlayView()
        self.myHistoryPoints = viewMyPoints
        tableHistoryPoints.reloadData()
        tableHistoryPoints.isHidden = false
        tableInterchange.isHidden = true
        tableTransferPoints.isHidden = true
        collectionHome.isHidden = true
        tableInterchangeInProcess.isHidden = true
        
    }
    
    func didFailViewMyPoints(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        print(subtitle)
    }
    
    func deletePromoById(id: Int) {
        //Por ahora solo redirige a intercambios pendientes
//        actionInterchangeInProcess()
        
        switch id {
        case 4,5,6:
            isHistory = false
            openHistory()
        default:
            actionInterchangeInProcess()
        }
            
    }
    
    func closeDialogAndExit(toHome: Bool) {
        if toHome {
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            
            
            
            
        }
        //self.navigationController?.popViewController(animated: true)
    }
    
    func OnResultEst(ids: [RewardsMobile]) {
        self.idsSelected = ids
        textToEstSelect = ids[0].establishment_name
        if isInterchange{
            tableInterchange.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }else{
            tableTransferPoints.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        }
    }
    
    func onContactDelegate(name: String, phone: String) {
        isEditPhone = false
        textUserSelected = name
        textPhoneSelected = phone
        tableTransferPoints.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    }
    
    func didSuccessComission(info: TransferCommission, whereFrom: String) {
        mTransferComission = info
        LoadingOverlay.shared.hideOverlayView()
        
        if whereFrom == Constants.typeTranfer{
            idsSelected.removeAll()
            //titleOptions.attributedText = CustomsFuncs.getAttributedString(title1: "Transferir ", title2: "Puntos Móviles")
            //imageOptions.image = UIImage(named: "ic_transfer_selected")
            ctlTopDivisor.constant = 40
            ctlToDotNoUsePoints.constant = -20
            isTransfer = true
            isHistory = false
            isInterchange = false
            isInterchangeInProcess = false
            listTransfer.removeAll()
            //listTransfer.append("subtitleTransfer")
            listTransfer.append("bodyTransfer")
            lblTranfer.alpha = 0
            //lblTranfer.textColor = UIColor(red: 252.0, green: 157.0, blue: 29.0, alpha: 1.0)
            lblHistory.textColor = .black
            lblInterchange.alpha = 1
            lblInterchangeInProcess.alpha = 1
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = true
            tableInterchangeInProcess.isHidden = true
            tableTransferPoints.isHidden = false
            collectionHome.isHidden = true
            tableTransferPoints.reloadData()
            setSmallIconTitle(titleIcon: "ic_transfer_selected", titleTextPickpal: CustomsFuncs.getAttributedString(title1: " Transferir ", title2: "Puntos Móviles"))
            
        }else if whereFrom == Constants.typeInterchange{
            idsSelected.removeAll()
            ctlTopDivisor.constant = -1
            ctlToDotNoUsePoints.constant = 8
            let newString = NSMutableAttributedString()
            newString.append(CustomsFuncs.getAttributedString(title1: " Intercambia tus ", title2: "Puntos Móviles "))
            newString.append(CustomsFuncs.getAttributedString(title1: "por \n puntos de otros comercios de tu interés", title2: ""))
            setSmallIconTitle(titleIcon: "ic_interchange_selected", titleTextPickpal: newString)
            //titleOptions.attributedText = newString
            //imageOptions.image = UIImage(named: "ic_interchange_selected")
            isTransfer = false
            isHistory = false
            isInterchange = true
            isInterchangeInProcess = false
            listInterchange.removeAll()
            listInterchange.append("body")
            lblTranfer.alpha = 1
            lblInterchangeInProcess.alpha = 1
            lblHistory.textColor = .black
            lblInterchange.alpha = 0
            tableHistoryPoints.isHidden = true
            tableInterchange.isHidden = false
            tableTransferPoints.isHidden = true
            collectionHome.isHidden = true
            tableInterchangeInProcess.isHidden = true
            tableInterchange.reloadData()
        }
    }
    
    func didFailComission(error: String, subtitle: String) {
        
    }
    func didSuccessGetPendingExchange(info: [PendingExchange]) {
        let newString = NSMutableAttributedString()
        newString.append(CustomsFuncs.getAttributedString(title1: " Intercambios en Proceso de\n", title2: "Puntos Móviles", colorText: 2))
        setSmallIconTitle(titleIcon: "ic_interchange_process_selected", titleTextPickpal: newString)
        
        print(info)
        listPendingExchange = info
        isInterchangeInProcess = true
        lblInterchangeInProcess.alpha = 0
        lblHistory.textColor = .black
        lblInterchange.alpha = 1
        lblTranfer.alpha = 1
        ctlTopDivisor.constant = -1
        ctlToDotNoUsePoints.constant = 8
        isTransfer = false
        isHistory = false
        isInterchange = false
        tableInterchangeInProcess.isHidden = false
        
        collectionHome.isHidden = true
        tableInterchange.isHidden = true
        tableHistoryPoints.isHidden = true
        tableTransferPoints.isHidden = true
        tableInterchangeInProcess.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailGetGetPendingExchange(error: String, subtitle: String) {
        collectionHome.isHidden = false
        tableInterchange.isHidden = true
        tableHistoryPoints.isHidden = true
        tableTransferPoints.isHidden = true
        tableInterchangeInProcess.isHidden = true
        
        isTransfer = false
        isHistory = false
        isInterchange = false
        isInterchangeInProcess = false
        
        lblInterchangeInProcess.alpha = 1
        lblHistory.textColor = .black
        lblInterchange.alpha = 1
        lblTranfer.alpha = 1
        
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "No tienes intercambios pendientes")
    }
    
    func changeAmount(pointsToSend: Double, newMoneyAfter: Double, newPointsAfter: Double,
                      moneyValueTosend: Double, taxValue: Int, moneyTax: Double) {
        textPointsEnter = /*isCleanFields ? "" :*/ String(pointsToSend)
        newMoneyAfterSend = String(newMoneyAfter)
        newPointsAfterSend = String(newPointsAfter)
        mMoneyValueToSend = moneyValueTosend
        valueTaxPoints = taxValue
        moneyTaxValue = moneyTax
    }
    
    func invalidPoints(message:String){
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: message)
    }
    
    func tranferToFriend(){
        if textPointsEnter != "0" && textPointsEnter != "0.0" && textPointsEnter != "" && textPhoneSelected != ""{
            if isEditPhone{
                if !textPhoneSelected.isPhone(){
                    showMessage(title: "Número Celular inválido", message: "El número celular debe de tener 10 dígitos.")
                    return
                }
            }
            
            tipoIntercambio = "trasnferencia"
            
            establishment.viewMyPointEstablishment(userId: "\(UserDefaults.standard.object(forKey: "client_id") as! Int)", establishmentId: "\(idsSelected[0].establishment_id!)")
            
           
        }else{
            showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "Ingresa todos los datos solicitados.")
        }
    }
    
    func didSuccessSendPoints(user_id: Int, points: Int, phoneToSend: String, establishmentId: Int, friend: String){
        pointWs.getTotalPoinsMobile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        pointWs.getRewards(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        for data in mListEstablishments{
            if establishmentId == data.establishment_id{
                //let user = UserDefaults.standard.object(forKey: "first_name") as! String
                
                //"63 pts / $28.25 fueron transferidos a "nombre de contacto" ("poner el telefono entre parentesis")".
                let message = "\(points) pts / \(CustomsFuncs.getFomatMoney(currentMoney: mMoneyValueToSend)) fueron transferidos a \(friend) (\(phoneToSend))."
                //let message = "Tus puntos de \(data.establishment_name ?? "") fueron transferidos \(points) pts /\(CustomsFuncs.getFomatMoney(currentMoney: mMoneyValueToSend)) exitosamente a: \(friend) \(phoneToSend)."
                let puntosTrasnf = Double(textPointsEnter) ?? 0.0
                let totalPuntos =  (idsSelected[0].total_points - puntosTrasnf  == 0.0)
                
                showMessage(title: "Transferencia exitosa", message: message, valueCall: 5, whatsAppMesaage: true, backHome: totalPuntos)
                textToEstSelect = "Establecimiento"
            }
        }
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailSendPoints(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: subtitle)
    }
    
    func onAddedPhone(phone: String) {
        isEditPhone = phone == "" ? false : true
        textPhoneSelected = phone
        
    }
    
    func removeSimbols(){
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: ".", with: "", options: NSString.CompareOptions.literal, range: nil)
        textPhoneSelected = textPhoneSelected.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range: nil)
    }
    
    func onChangeInterchangeEnter(valeu: Double, moneyPoint: Double, taxValue: Int, moneyTax: Double, pointsAfterGift: Double, pointsAfterGiftMoney: Double) {
        print(valeu)
        valueInterchangePoints = Int(valeu)
        valueTaxPoints = taxValue
        moneyTaxValue = moneyTax
        valueMoneyPointsInterchange = moneyPoint
        pointsAfterGiftInterhange = pointsAfterGift
        pointsMoneyAfterGiftInterhange = pointsAfterGiftMoney
        
    }
    
    func onInitInterchange() {
        print(valueInterchangePoints)
        print("Ids ", idsSelected)
        print("Multi ",multiplesIdsSelected)
        
        
        
        if valueInterchangePoints != 0 && multiplesIdsSelected.count > 0 && idsSelected.count > 0{
            tipoIntercambio = "intercambio"
            establishment.viewMyPointEstablishment(userId: "\(UserDefaults.standard.object(forKey: "client_id") as! Int)", establishmentId: "\(idsSelected[0].establishment_id!)")
            
           // multiplesIdsSelected.removeAll()
            
        }else{
           showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "Ingresa todos los datos solicitados.")
       }
                
    }
    
    
    func didSuccessViewMyReward(reward: MyRewardEstablishment){
        
        print("Monto Minimo \(reward.minExchange) esto quiere intercambiar")
        
        var axu :Double!
        
        if tipoIntercambio == "trasnferencia"{
            
             axu = Double (textPointsEnter)
            
        }else{
             axu = Double(valueInterchangePoints)
            
        }
       
        
        if   axu >= reward.minExchange{
            
            
            if tipoIntercambio == "trasnferencia"{
                
                removeSimbols()
               let numberClean = textPhoneSelected.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "").suffix(10)
               LoadingOverlay.shared.showOverlay(view: self.view)
               pointWs.sendPointsToFriend(user_id: UserDefaults.standard.object(forKey: "client_id") as! Int, points: textPointsEnter, phoneToSend: String(numberClean), establishmentId: idsSelected[0].establishment_id, friend: textUserSelected, tax: valueTaxPoints)
                
            }else{
                
                LoadingOverlay.shared.showOverlay(view: self.view)
                self.view.isUserInteractionEnabled = false
                var ids = [Int]()
                for data in multiplesIdsSelected{
                    ids.append(data.establishment_id)
                }
                self.pointWs.sendPointsToInterchange(user_id: UserDefaults.standard.object(forKey: "client_id") as! Int, points: valueInterchangePoints, establishmentId: idsSelected[0].establishment_id, establishments_destination: ids, tax: valueTaxPoints)
                print("ids: \(idsSelected)")
                
            }
            
                
          
        }else{
            
            if tipoIntercambio == "trasnferencia"{
                
                showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "El valor mínimo a transferir es de \(CustomsFuncs.numberFormat(value: reward.minExchange)) pts.")
                
            }else{
                showMessage(title: UserDefaults.standard.object(forKey: "first_name") as? String ?? "", message: "El valor mínimo a intercambiar es de \(CustomsFuncs.numberFormat(value: reward.minExchange)) pts.")
                
            }
            
        }
        
    }
    
    func didFailViewMyReward(error: String, subtitle: String){
        
        showMessage(title: error, message: subtitle)
    }
    
    
    func didSuccessInterchangePoints(info: [InterchangemyPoints]) {
        self.view.isUserInteractionEnabled = true
        pointWs.getTotalPoinsMobile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        pointWs.getRewards(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        LoadingOverlay.shared.hideOverlayView()
        textToMultipleEstSelect = "Establecimiento(s)"
        
        if info.count > 0{
            let message = "¡Enhorabuena! Intercambiaste \(valueInterchangePoints.formatnumber()) pts | \(CustomsFuncs.getFomatMoney(currentMoney: valueMoneyPointsInterchange)) de \(info[0].from_est ?? "") por \(info[0].to_est ?? "")."
            showMessage(title: "¡Tus Puntos Móviles ya se intercambiaron!", message: message, valueCall: 4)
        }else{
            let user = UserDefaults.standard.object(forKey: "first_name") as? String ?? ""
            showMessage(title: user, message: "PickPal buscará hacer el intercambio de forma automática y te notificaremos cuando quede realizado.", valueCall: 3)
        }
        
        isEditPhone = false
        isCleanFields = true
        lblMessagePonits.isHidden = true
//        multiplesIdsSelected.removeAll()
        textToEstSelect = "Establecimiento"
        valueTaxPoints = 0
        textUserSelected = ""
        textPhoneSelected = ""
        textPointsEnter = ""
        valueInterchangePoints = 0
//        idsSelected.removeAll()
//        multiplesIdsSelected.removeAll()
        textToMultipleEstSelect = "Establecimiento(s)"
//        tableInterchange.reloadData()
        
    }
    
    func didFailInterchangePoints(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    func didSuccessCancelInterchange(){
        pointWs.getPendigExchange(user_id:String( UserDefaults.standard.object(forKey: "client_id") as! Int))
        LoadingOverlay.shared.hideOverlayView()
        let user = UserDefaults.standard.object(forKey: "first_name") as? String ?? ""
        showMessage(title: user, message: "Cancelaste la solicitud. Por lo que se te reembolsaron \(CustomsFuncs.numberFormat(value:self.cancelAmount)) pts. de \(self.cancelEstablishment)", valueCall: 6)
    }
    
    func didFailCancelInterchange(error: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    func onCancelInterchange(id: Int, nameEstablishment: String,amountTransfer : Double ) {
        
        self.cancelAmount = amountTransfer
        self.cancelEstablishment = nameEstablishment
        LoadingOverlay.shared.showOverlay(view: self.view)
        pointWs.cancelInterchange(excharge_pk: id)
    }
    
    func didSuccessStatusRequestRxchange(dataExchan: TransferDetails){
        self.infoToExchange = dataExchan
        
            alertAceptarIntercambio()
        
     }
    
    
    func alertAceptarIntercambio(){
        
        
         self.view.isUserInteractionEnabled = true
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext

        
        alertintercambio.textTitle = "¡Un usuario quiere intercambiar puntos contigo!"
        alertintercambio.textMenssage = "\(infoToExchange.aboutOther!) quiere intercambiar"
        alertintercambio.infoToExchange = self.infoToExchange
        alertintercambio.delegate = self
            
               self.present(alertintercambio, animated: true, completion: nil)

        
    }
    
    func didFailStatusRequestRxchange(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showMessage(title: error, message: subtitle)
    }
    
    
    func didSuccessAcceptRequestExchange(total_transactions: Int){
        
        print("Se realizo el intercambio ", total_transactions)
        
        showMessage(title: "¡Tus Puntos Móviles ya se intercambiaron!", message: "¡Enhorabuena! Intercambiaste \(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentFromName!). Por \(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentTomName!).", valueCall: 4)
        
      //  showMessage(title: "¡Tus Puntos Móviles ya se intercambiaron!", message: "¡Enhorabuena! Intercambiaste \(pushExchange.amount) pts de \(pushExchange.name_establishmentid_from). Por \(pushExchange.amount) pts de \(pushExchange.name_establishmentid_to).")
        
    }
    
    func didFailAcceptRequestExchange(error: String, subtitle: String) {
           self.view.isUserInteractionEnabled = true
           LoadingOverlay.shared.hideOverlayView()
           showMessage(title: error, message: subtitle)
       }
    
    func AcceptRequestExchange(total_to_exchange: Int) {
         
        pointWs.acceptRequestExchange(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, establishment_from: pushExchange.establishmentid_from, request_id: pushExchange.identifier, establishmentto_id: pushExchange.establishmentid_to, amount: total_to_exchange )
        isHistory = false
        openHistory()
         alertintercambio.dismiss(animated: true, completion: nil)
    }
    

    
    
    
}

//Mark.- Tarjetas de establecimientos
extension HomePointsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mListEstablishments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EstabsRewards", for: indexPath) as! EstabsRewardsCollectionViewCell
        cell.clipsToBounds = false
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 5
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.35
        cell.layer.shadowOffset = .zero
        cell.layer.shadowRadius = 5
        
        cell.lblCodeState.text = mListEstablishments[indexPath.row].state
        cell.lblState.text = mListEstablishments[indexPath.row].place
        cell.lblPlace.text = mListEstablishments[indexPath.row].establishment_name
        cell.lblTotalPoints.text =  CustomsFuncs.numberFormat(value: mListEstablishments[indexPath.row].total_points) + " pts"
        
        cell.lblTotalMoney.text = CustomsFuncs.getFomatMoney(currentMoney: mListEstablishments[indexPath.row].total_money ?? 0.0)
        
        if let logo = mListEstablishments[indexPath.row].logo, logo != "" {
            Nuke.loadImage(with: URL(string: logo)!, into: cell.imgLogo)
        }
        //cell.vimgBackGround.image = UIImage(named: mListBackGround.randomElement() ?? "ic_tarjet_green")
        cell.vimgBackGround.image = UIImage(named: mListEstablishments[indexPath.row].backGroundColor)
        
        if mListEstablishments[indexPath.row].available{
             
            cell.viewAvailable.alpha = 0
             cell.isUserInteractionEnabled = true
            
        }else{
            
            cell.viewAvailable.alpha = 0.6
            cell.clipsToBounds = false
            cell.viewAvailable.layer.masksToBounds = false
            cell.viewAvailable.layer.cornerRadius = 5
            cell.isUserInteractionEnabled = false
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SinglePoints") as! SinglePointsViewController
        newVC.mEstabId = mListEstablishments[indexPath.row].establishment_id
        newVC.mRewardsMobile = mListEstablishments[indexPath.row]
        newVC.valueState = self.valueState
        newVC.estabName = mListEstablishments[indexPath.row].establishment_name!
        
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
 
    
    
}

//Mark.- Historial de pedidos
extension HomePointsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableInterchangeInProcess{
            return listPendingExchange.count
        }else if tableView == tableInterchange{
            return listInterchange.count
        }else if tableView == tableHistoryPoints {
            return myHistoryPoints.count
        }else{
            return listTransfer.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //MARK: - Intercambio en proceso
        if tableView == tableInterchangeInProcess{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
            cell.delegate = self
//            cell.isUserInteractionEnabled = false;
            let data = listPendingExchange[indexPath.row]
//            cell.collectinTransferPoints.isHidden = true
            cell.collectinTransferPoints.visibility = .gone
            cell.mId = data.id
            cell.establishmentName = data.exchange_from
            cell.amountTransfer = data.amount
            cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
            cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.date, ziseFont: 12.0, colorText: 0)
            cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concepInterchange, ziseFont: 14.0, colorText: 0)
           
            cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2: "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money))", ziseFont: 12.0, colorText: 0)
            cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                "\(data.reference!)", ziseFont: 12.0, colorText: 0)

            dinamicHeightInProcess = 290
            
            //Oscar Movimientos
          if data.exchange_details.count > 0{
                              
            // CON ALGUN MOVIMIENTOS
              cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
            
          
           
           
            cell.lblMovementsProcess.visibility = .visible
            cell.imgArrowMovemen.visibility = .visible
            cell.viewFrom = true
            cell.listMovemenTransfer = data.exchange_details
            cell.collectinTransferPoints.reloadData()
            cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
            cell.isUserInteractionEnabled = true;
            
            if expandedIndexSet.contains(indexPath.row) {
                if data.left_amount_money > 0 {
                    
                    dinamicHeightInProcess += 35
                    
                }
                dinamicHeightInProcess += 30
                dinamicHeightInProcess = dinamicHeightInProcess + CGFloat(data.exchange_details.count * 60)
                cell.collectinTransferPoints.visibility = .visible
//                cell.heightCell.constant = dinamicHeightInProcess
                cell.collectinTransferPoints.reloadData()
//                  cell.imgArrowMovemen.isHidden = false
                cell.imgArrowMovemen.image =  UIImage(named: "ic_movement_up")
              
            } else {
                            
//                    dinamicHeightInProcess = 260
                if data.left_amount_money > 0 {
                    
                    dinamicHeightInProcess += 40
                    
                }
                
                cell.collectinTransferPoints.visibility = .gone
//                cell.heightCell.constant = dinamicHeightInProcess
                cell.imgArrowMovemen.visibility = .visible
                cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
                
              }
            }else{
              // SIN MOVIMIENTOS
//                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
        
                cell.lblPriceTransaction.text = ""
                cell.lblMovementsProcess.visibility = .gone
                cell.imgArrowMovemen.visibility = .gone
//                cell.isUserInteractionEnabled = false;
              
           }
            
            cell.lblToFromUser.attributedText  =  formatStringAddressProcess(data: data, indexPath: indexPath)
            
            
            //Oscar
            
            var attributedString: NSAttributedString!
            let completeText = NSMutableAttributedString(string: "")
            let attributesEstablishment: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
                .font: UIFont(name: "Aller", size: 12.0)!
            ]
            let attributesTitle: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
                .font: UIFont(name: "Aller-Bold", size: 12.0)!
            ]
            
            if data.exchange_to_address_array.count > 0{
            attributedString = NSAttributedString(string: "De: ", attributes: attributesTitle)
            completeText.append(attributedString)
            }
            self.indexPath = indexPath
            dinamicHeightInProcess += 20
         
            
            for name in data.exchange_to!{
//                dinamicHeightInProcess = 300
                if data.exchange_to_address_array.count > 0{
                    
                    attributedString = NSAttributedString(string: "\(name) ", attributes: attributesEstablishment)
                  //  stringStreet = NSAttributedString(string: data.exchange_to_address_array[currentIndex], attributes: attributesStreet)
                    
                    completeText.append(attributedString)
                    
                 //   completeText.append(stringStreet)
                    
                    //cadena += "\(attributedString!) \(stringStreet!), "
                }else{
                    attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
                }
     
                dinamicHeightInProcess += 20
                print("tamaño \(dinamicHeightInProcess)")
                tableInterchangeInProcess.beginUpdates()
                tableInterchangeInProcess.endUpdates()
            }
            
            cell.lblCause.isHidden = true
            
//            cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:"\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money))", ziseFont: 12.0, colorText: 0)
            
            cell.lblAmount.visibility = .gone
            if data.tax_total > 0{
            
            cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisión:\n", title2: "\(CustomsFuncs.numberFormat(value: data.tax ?? 0)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.tax_total))", ziseFont: 12.0, colorText: 0)
            
            }else{
                
                
                cell.lblPriceTransaction.text = ""
                
                
            }
            
            cell.selectionStyle = .none
            tableInterchangeInProcess.beginUpdates()
            tableInterchangeInProcess.endUpdates()
            
            return cell
        }else if tableView == tableInterchange{
            //MARK: -  Intercambio de puntos
            let cell = tableView.dequeueReusableCell(withIdentifier: "InterchangePoints") as! InterchangePointsTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            cell.lblEst.text = textToEstSelect
            cell.mTransferCommission = mTransferComission
            cell.lblEstToTransfer.text = textToMultipleEstSelect
            cell.idSelected = idsSelected.count > 0 ? idsSelected[0].establishment_id : 0
            let price = mTransferComission.comision_intercambio * mTransferComission.tipo_cambio
            cell.tfEnterPoints.text = ""
            
            cell.lblPriceTransaction.text = "Comisión: \(valueTaxPoints) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTaxValue))"
            
            
            
            if !isCleanFields{
                if valueInterchangePoints != 0{
                    cell.tfEnterPoints.text = CustomsFuncs.numberFormat(value: Double(valueInterchangePoints))
                    cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(CustomsFuncs.numberFormat(value: pointsAfterGiftInterhange)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: pointsMoneyAfterGiftInterhange))"
                }
            }
            
            if idsSelected.count > 0 {
                cell.mRewardsMobile = idsSelected[0]
                cell.lblAmountAlready.attributedText =
                    CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                     title2: "\(CustomsFuncs.numberFormat(value: idsSelected[0].total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: idsSelected[0].total_money ?? 0))", ziseFont: 11.0)
            }else{
                cell.lblAmountAlready.attributedText =
                    CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                             title2: "\(CustomsFuncs.numberFormat(value: 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))", ziseFont: 11.0)
            }
            
            if isCleanFields{
                idsSelected.removeAll()
                cell.lblAmountAlready.attributedText =
                    CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                     title2: "\(CustomsFuncs.numberFormat(value: 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))", ziseFont: 11.0)
                cell.tfEnterPoints.text = ""
                cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(0)))"
                cell.lblPriceTransaction.text = "Comisión: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))"
                isCleanFields = false
            }
            
            tableInterchangeInProcess.beginUpdates()
            tableInterchangeInProcess.endUpdates()
            return cell
            //MARK: - Historial de puntos
        }else if tableView == tableHistoryPoints {
          
            
//            dinamicHeight = 260
            
            let data = myHistoryPoints[indexPath.row]
            
            switch(myHistoryPoints[indexPath.row].concep_int){
            case 1, 2://MARK: - 1 =  Puntos abonados (compra) 2 = puntos utilizados compras
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddedAndRedimPoints") as! AddedAndRedimPointsTableViewCell
//                let data = myHistoryPoints[indexPath.row]
                cell.isUserInteractionEnabled = false;
                cell.selectionStyle = .none
                
                if data.concep_int == 2{
                    cell.imgTypeConcept.image = UIImage(named: "ic_history_redim")
                    cell.viewColorConcept.backgroundColor = UIColor.red
                }else{
                    cell.imgTypeConcept.image = UIImage(named: "ic_history_added")
                    cell.viewColorConcept.backgroundColor = UIColor.green;
                }
                
                cell.lblNumbreReferen.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.reference, ziseFont: 12.0, colorText: 0)
                
                cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                cell.lblConcept.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concep, ziseFont: 14.0, colorText: 0)
                

                
                cell.lblEstablishment.attributedText = formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 1)
                cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                    "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount))", ziseFont: 12.0, colorText: 0)
                cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:
                    "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money))", ziseFont: 12.0, colorText: 0)
                cell.lblComision.visibility = .gone
                
//                cell.lblComision.attributedText = CustomsFuncs.getAttributedString(title1: "Comisión:\n", title2:
//                    "\( data.str_tax!)", ziseFont: 12.0, colorText: 0)
                cell.lablTotal.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2:
                                                                                    "$\(CustomsFuncs.numberFormat(value: data.total_money!))/\(data.total!)pts", ziseFont: 12.0, colorText: 0)
                
                return cell
                
                
                
            case 3: //MARK: - Puntos abonados (transferencia)
                
                print("Case 3")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = true;
                cell.lblMovements.isHidden = true
                cell.collectinTransferPoints.isHidden = true
                cell.imgArrowMovemen.isHidden = true
                
                cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concep, ziseFont: 14.0, colorText: 0)
                cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                    "\(data.reference!)", ziseFont: 12.0, colorText: 0)

                cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
              //  cell.colorViewType.backgroundColor = UIColor.orange
                cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "De:\n", title2: data.transfer_from, ziseFont: 12.0, colorText: 0)
                cell.lblCause.attributedText =  formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 3)
                cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
                cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
//  cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(CustomsFuncs.numberFormat(value: data.tax ?? 0)) pts. / \(CustomsFuncs.getFomatMoney(currentMoney: data.tax_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                
                cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                    
                cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)

                cell.lblPriceTransaction.visibility = .gone
//                cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                cell.lblCurrentMoney.text = ""
                
                return cell
                
            case 4: //MARK: - Transferencia enviada
                
                print("Case 4")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                
                cell.isUserInteractionEnabled = false;
                cell.selectionStyle = .none
                cell.lblMovements.isHidden = true
                cell.collectinTransferPoints.isHidden = true
                cell.imgArrowMovemen.isHidden = true
                
                cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concep, ziseFont: 14.0, colorText: 0)
                cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                    "\(data.reference!)", ziseFont: 12.0, colorText: 0)
                
                cell.imgIconTypeCard.image = UIImage(named: "ic_history_transfer")
                
                cell.lblCause.attributedText =  formatText(establishmentName: data.establishment_name!, streetName: data.address!, type: 4)
                
                cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
                "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                    
                cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                
                cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total real transferido:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
                
                cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Contacto:\n", title2: data.transfer_to, ziseFont: 12.0, colorText: 0)
                cell.lblCurrentMoney.text = ""
                
                return cell
                
            case 5: //MARK: - Solicitud de intercambio en proceso / Finalizado
                
                print("Case 5")
                print("case 5 inicial \( dinamicHeightInProcess)")
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                dinamicHeightInProcess = data.left_amount > 0 ? 235:255
                print("case 5 segunda \( dinamicHeightInProcess)")
                cell.isUserInteractionEnabled = false;
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = true;
                cell.lblMovements.visibility = .gone
                cell.collectinTransferPoints.visibility = .gone
                cell.imgArrowMovemen.visibility = .gone
                
                
                cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concep, ziseFont: 14.0, colorText: 0)
                cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                    "\(data.reference!)", ziseFont: 12.0, colorText: 0)
                
                cell.imgIconTypeCard.image = UIImage(named: "ic_history_interchange_home")
                cell.lblAmount.visibility = data.is_cancelled && data.exchange_details.count == 1 ? .gone:.visible
                cell.lblToFromUser.text = ""
                cell.lblCause.attributedText = formatStringAddressHistory(data: data, indexPath: indexPath)
                
                
                cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                    
                cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                
                cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                
                cell.lblPriceTransaction.visibility = data.str_tax == "0 pts" ? .gone:.visible
                
                       
                cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Pendiente de intercambio:\n", title2:
                    "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                
                if data.left_amount_money > 0 {
                    
                    dinamicHeightInProcess += 40
                    
                }
                
//                cell.lblCurrentMoney.text = ""
                cell.lblAmounTrasaction.text = ""
                
                
                //Oscar Movimientos
              if data.exchange_details.count > 0{
                                  
                // CON ALGUN MOVIMIENTOS
                  cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                
              
               
                cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                cell.lblMovements.visibility = .visible
                cell.imgArrowMovemen.visibility = .visible
              cell.viewFrom = true
              cell.listMovemenTransfer = data.exchange_details
                
                cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
                cell.isUserInteractionEnabled = true;
                
//                if data.left_amount_money > 0 {
//
//                    dinamicHeightInProcess += 40
//
//                }
                
                if expandedIndexSet.contains(indexPath.row) {
                    if data.left_amount_money > 0 {
                        
                        dinamicHeightInProcess += 35
                        
                    }
                    dinamicHeightInProcess += 30
                    dinamicHeightInProcess = dinamicHeightInProcess + CGFloat(data.exchange_details.count * 60)
                    cell.collectinTransferPoints.visibility = .visible
                    cell.heightCell.constant = dinamicHeightInProcess
                    cell.collectinTransferPoints.reloadData()
//                  cell.imgArrowMovemen.isHidden = false
                    cell.imgArrowMovemen.image =  UIImage(named: "ic_movement_up")
                  
                } else {
                                
//                    dinamicHeightInProcess = 260
                   
                    
                    cell.collectinTransferPoints.visibility = .gone
                    cell.heightCell.constant = dinamicHeightInProcess
                    cell.imgArrowMovemen.visibility = .visible
                    cell.imgArrowMovemen.image =  UIImage(named: "iconIonicIosArrowDown")
                    
                  }
                }else{
                  // SIN MOVIMIENTOS
//                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
            
                    cell.lblPriceTransaction.text = ""
                    cell.lblMovements.visibility = .gone
                   cell.imgArrowMovemen.visibility = .gone
                    cell.isUserInteractionEnabled = false;
                  
               }
                
                
                
                if data.left_amount == 0{ // INTERCAMBIO FINALIZADO
                    cell.lblCurrentMoney.text = ""
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                
//                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
//                    cell.lblPriceTransaction.text = ""
//                    cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total))", ziseFont: 12.0, colorText: 0)
                    
                    
                }else{// INTERCAMBIO PENDIENTE
                    
                    cell.lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Puntos intercambiados:\n", title2: "\(CustomsFuncs.numberFormat(value: data.total)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.total_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                    dinamicHeightInProcess += 35
                }
                 
                tableHistoryPoints.beginUpdates()
                tableHistoryPoints.endUpdates()
                return cell
                
            case 6:  //MARK: -  Puntos abonados (intercambio)
                
                print("Case 6")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell
                dinamicHeightFinish = 280
               
                cell.isUserInteractionEnabled = false;
                cell.selectionStyle = .none
                cell.isUserInteractionEnabled = false;
                cell.lblMovements.isHidden = true
                cell.collectinTransferPoints.isHidden = true
                cell.imgArrowMovemen.isHidden = true
                cell.lblToFromUser.text = ""
                cell.lblConcepto.attributedText = CustomsFuncs.getAttributedString(title1: "", title2: data.concep, ziseFont: 14.0, colorText: 0)
                cell.lblNumberReferens.attributedText = CustomsFuncs.getAttributedString(title1: "Número de referencia:   ", title2:
                    "\(data.reference!)", ziseFont: 12.0, colorText: 0)
                
                cell.imgIconTypeCard.image = UIImage(named: "ic_history_interchange_home")
                cell.lblCause.attributedText = formatText(establishmentName: data.establishment_name, streetName: data.address!, type: 6)
                 cell.lblToFromUser.attributedText = CustomsFuncs.getAttributedString(title1: "Hora:\n", title2: data.time_at, ziseFont: 12.0, colorText: 0)
                    
                cell.lblHour.attributedText = CustomsFuncs.getAttributedString(title1: "Fecha:\n", title2: data.created_at, ziseFont: 12.0, colorText: 0)
                

//                cell.lblCurrentMoney.attributedText = CustomsFuncs.getAttributedString(title1: "Monto de transacción:\n", title2:
//                    "\(CustomsFuncs.numberFormat(value: data.left_amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.left_amount_money ?? 0.0))", ziseFont: 12.0, colorText: 0)
                cell.lblCurrentMoney.text = ""
                cell.lblPriceTransaction.attributedText = CustomsFuncs.getAttributedString(title1: "Comisón:\n", title2: "\(data.str_tax!)", ziseFont: 12.0, colorText: 0)
                
                cell.lblAmounTrasaction.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\n", title2: "\(CustomsFuncs.numberFormat(value: data.amount)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: data.money_amount ?? 0.0))", ziseFont: 12.0, colorText: 0)
                cell.lblDate.attributedText = CustomsFuncs.getAttributedString(title1: "Por puntos de:\n", title2: data.exchange_from, ziseFont: 12.0, colorText: 0)
                
                cell.lblAmount.text = ""
                
                
                tableHistoryPoints.beginUpdates()
                tableHistoryPoints.endUpdates()
                
                return cell
                
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: "TransferInterchange") as! TransferInterchangeTableViewCell

//
                return cell
            }
             tableView.reloadRows(at: [indexPath], with: .automatic)
        }else{
            if listTransfer[indexPath.row] == "subtitleTransfer"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "subtitleTransfer")!
                cell.selectionStyle = .none
                return cell
                
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "transferPoints") as! BodyTransferPointsTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                cell.mTransferCommission = mTransferComission
                cell.lblEst.text = textToEstSelect
                cell.idSelected = idsSelected.count > 0 ? idsSelected[0].establishment_id : 0
//                cell.pointEstablishmentSelect = idsSelected[0].total_points ?? 0.0
                
                if textToEstSelect == "Establecimiento" {
                    //cell.btnTransferPoints.isUserInteractionEnabled = false
                    cell.lblAmountAlready.isHidden = true
                    cell.lblAmountAlready.isHidden = true
                }else{
                    cell.mRewardsMobile = idsSelected[0]
                    //cell.btnTransferPoints.isUserInteractionEnabled = true
                    cell.lblAmountAlready.isHidden = false
                    
                    cell.lblAmountAlready.attributedText =
                    CustomsFuncs.getAttributedString(title1: "Tienes: ",
                                                         title2: "\(CustomsFuncs.numberFormat(value: idsSelected[0].total_points ?? 0)) pts / \(CustomsFuncs.getFomatMoney(currentMoney: idsSelected[0].total_money ?? 0))", ziseFont: 11.0)
                }
                
                if textPointsEnter != "0" && textPointsEnter != "" && newMoneyAfterSend != "" && newPointsAfterSend != ""{
                    let pd = Double(textPointsEnter) ?? 0
                    let pi = Int(pd)
                    cell.tfEnterPoints.text = String(pi)
                    
                    //cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(newPointsAfterSend) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(newMoneyAfterSend)!))"
                    
                    cell.lblPriceTransaction.text = "Comisión: \(valueTaxPoints) pts / \(CustomsFuncs.getFomatMoney(currentMoney: moneyTaxValue))"
                }else{
                    cell.tfEnterPoints.text = ""
                }
                
                if isCleanFields{
                    cell.tfEnterPoints.text = ""
                    cell.tfContact.text = ""
                    textPointsEnter = ""
                    textUserSelected = ""
                    textPhoneSelected = ""
                    //cell.newMoneyAfterTransaction.text = "Nuevo saldo después de la transacción: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: Double(0)))"
                    cell.lblPriceTransaction.text = "Comisión: \(0) pts / \(CustomsFuncs.getFomatMoney(currentMoney: 0))"
                    isCleanFields = false
                }
                
            
                if textPhoneSelected != ""{
                    if textUserSelected != ""{
                        cell.tfContact.text = textUserSelected + " / " + textPhoneSelected
                    }else{
                        cell.tfContact.text = textPhoneSelected
                    }
                    
                }
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableInterchangeInProcess{
            return dinamicHeightInProcess
        }else if tableView == tableInterchange{
            return 550
        }else if tableView == tableHistoryPoints {
            switch(myHistoryPoints[indexPath.row].concep_int){
            case 1, 2:
                return dinamicHeightGeneral
                
            case 3, 4:
                
                return 260
                
            case 6:
                
              

//                  return dinamicHeightFinish
               
            
            return 270
                
            case 5:
                print("case 5 tamaño\( dinamicHeightInProcess)")
                if self.indexPath == indexPath {
                    return dinamicHeightInProcess
                }else{
                    return dinamicHeightInProcess
                }
                
                
            default:
                if self.indexPath == indexPath {
                  return dinamicHeightFinish
                }else{
                    return dinamicHeightFinish
                }
            }
        }else{
            if listTransfer[indexPath.row] == "subtitleTransfer"{
                return 44
            }else{
                return 470
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.indexPath = indexPath
        
          if(expandedIndexSet.contains(indexPath.row)){
                    expandedIndexSet.remove(indexPath.row)
            } else {
        
                    expandedIndexSet.insert(indexPath.row)
            }
                
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
    }
    
    
    
    @objc func expandMovements(){
        
        
      /*  print("El tamaño de la celda es de \(dinamicHeight)")
        tableHistoryPoints.updateConstraints()
         tableHistoryPoints.endUpdates()*/
      //  tableHistoryPoints.reloadRows(at: [self.indexPath], with: .automatic)
        
    }
    
    
    func formatStringAddressHistory(data: ViewMyPoints, indexPath: IndexPath) -> NSMutableAttributedString{
        
        var currentIndex = 0
        var attributedTitleString: NSAttributedString!
        var attributedString: NSAttributedString!
        var stringStreet: NSAttributedString!
        
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
            .font: UIFont(name: "Aller-Bold", size: 12.0)!
        ]
        
        let attributesEstablishment: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller", size: 12.0)!
        ]
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 10)!
        ]
  
        let completeText = NSMutableAttributedString(string: "")
        
        attributedTitleString = NSAttributedString(string: "Intercambiaste: ", attributes: attributesTitle)
        
        attributedTitleString = NSAttributedString(string: "Solicitud de intercambio: ", attributes: attributesTitle)

        
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "\(data.amount!) pts de \(data.exchange_from!) \n ", attributes: attributesEstablishment)
        completeText.append(attributedTitleString)
      
       stringStreet = NSAttributedString(string: " (\(data.address!)) \n", attributes: attributesStreet)
      
       completeText.append(stringStreet)
        
        if  data.exchange_to_address_array.count > 0{
        attributedTitleString = NSAttributedString(string: "\nPor puntos de: ", attributes: attributesTitle)
        completeText.append(attributedTitleString)
        
        
        for name in data.exchange_to!{
           
                if data.exchange_to_address_array.count > 0{
                    
                    attributedString = NSAttributedString(string: "\(name)\n", attributes: attributesEstablishment)
                    stringStreet = NSAttributedString(string: " (\(data.exchange_to_address_array[currentIndex])) \n  \n", attributes: attributesStreet)
                    completeText.append(attributedString)
                    
                    completeText.append(stringStreet)
          
                
                }else{
                    attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
                    completeText.append(attributedString)
                }
                 
                 
            if data.exchange_to_address_array.count > 1{
                    dinamicHeightInProcess += 70
                    dinamicHeightFinish += 70
                }else{
                    dinamicHeightInProcess += 100
                    dinamicHeightFinish += 60
                }
                
                currentIndex += 1
                
            }
        }
        self.indexPath = indexPath
            tableHistoryPoints.beginUpdates()
            tableHistoryPoints.endUpdates()

        
        return completeText
    }
    
    
    func formatText(establishmentName: String, streetName: String, type: Int)->NSMutableAttributedString{
        
        
        var attributedTitleString: NSAttributedString!
        var attributedString: NSAttributedString!
        var stringStreet: NSAttributedString!
        
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
            .font: UIFont(name: "Aller-Bold", size: 12.0)!
        ]
        
        let attributesEstablishment: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller", size: 12.0)!
        ]
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 10)!
        ]
  
        
        //"Intercambiaste: \(data.amount!) pts de \(data.exchange_from!) \n Por: \(establhistFrom)"
        let completeText = NSMutableAttributedString(string: "")
        
        let title = type == 6 ? " Puntos abonados del comercio:\n":"Comercio:\n"
        
        attributedTitleString = NSAttributedString(string: title, attributes: attributesTitle)
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "\(establishmentName)\n", attributes: attributesEstablishment)
        
        completeText.append(attributedTitleString)
        
        attributedTitleString = NSAttributedString(string: "(\(streetName))", attributes: attributesStreet)
        
        completeText.append(attributedTitleString)
        return completeText
        
    }
    
    
//    INTERCAMBIOS
    func formatStringAddressProcess(data: PendingExchange, inProcces: Bool = false,indexPath: IndexPath ) -> NSMutableAttributedString{
          
          var currentIndex = 0
          var attributedString: NSAttributedString!
          var attributedTitleString: NSAttributedString!
          var stringStreet: NSAttributedString!
          
          let attributesTitle: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00),
              .font: UIFont(name: "Aller-Bold", size: 12.0)!
          ]
        
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 10)!
        ]
          
          let attributesEstablishment: [NSAttributedString.Key: Any] = [
              .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
              .font: UIFont(name: "Aller", size: 12.0)!
          ]
          
    
          
          //"Intercambiaste: \(data.amount!) pts de \(data.exchange_from!) \n Por: \(establhistFrom)"
          let completeText = NSMutableAttributedString(string: "")

          attributedTitleString = NSAttributedString(string: "Solicitud de intercambio: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
        attributedTitleString = NSAttributedString(string: "\(data.amount!) pts de \(data.exchange_from!) \n ", attributes: attributesEstablishment)
        completeText.append(attributedTitleString)
      
       stringStreet = NSAttributedString(string: " (\(data.address!)) \n", attributes: attributesStreet)
      
       completeText.append(stringStreet)
          
        
          attributedTitleString = NSAttributedString(string: "\nPor puntos de: ", attributes: attributesTitle)
          completeText.append(attributedTitleString)
          
          
          for name in data.exchange_to!{
             
              if data.exchange_to_address_array.count > 0{
                  
                attributedString = NSAttributedString(string: "\(name)\n", attributes: attributesEstablishment)
                  stringStreet = NSAttributedString(string: " (\(data.exchange_to_address_array[currentIndex])) \n \n", attributes: attributesStreet)
                  
                  completeText.append(attributedString)
                  
                  completeText.append(stringStreet)
                
              
                  
                  //cadena += "\(attributedString!) \(stringStreet!), "
              }else{
                  attributedString = NSAttributedString(string: name, attributes: attributesEstablishment)
                completeText.append(attributedString)
              }
               
               currentIndex += 1
            dinamicHeightInProcess += 30
            
            
          }
        tableInterchangeInProcess.beginUpdates()
        tableInterchangeInProcess.endUpdates()
          return completeText
      }
    
    func  didFailGetGeolocation(title: String, subtitle: String) {
        
    }

    
    
}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[123456789][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}

extension Int {
    func formatnumber() -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        return formater.string(from: NSNumber(value: self))!
    }
}
