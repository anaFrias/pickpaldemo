//
//  LoadingOverlay.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 08/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class LoadingOverlay {
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(view: UIView) {
        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.center = view.center
        overlayView.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
        
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        view.addSubview(overlayView)
        
        activityIndicator.startAnimating()
        //        UploadingView.show(message: "HOLA")
    }
    public func showOverlayWithMessage(view: UIView, message: String) {
        UploadingView.show(message: message)
    }
    public func hideOverLayWithMessage() {
        UploadingView.hide()
    }
    public func hideOverlayView() {
        //         UploadingView.hide()
        activityIndicator.stopAnimating()
        overlayView.removeFromSuperview()
    }
    
}
