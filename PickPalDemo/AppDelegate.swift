//
//  AppDelegate.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import OneSignal
import CoreLocation
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
//    var socket = WebSocket(url: URL(string: Constants.wsURL + "\(1)/")!, protocols: ["chat"])
    var defaults = UserDefaults.standard
    var locationManager = CLLocationManager()
    var lastLocation:CLLocation?
    var ordersRef: DatabaseReference!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Listo"
        
        GMSPlacesClient.provideAPIKey("AIzaSyDkOCicqvQae2PZgaPv7rfrxS-lxTRhIC0")
        GMSServices.provideAPIKey("AIzaSyDHV1m4uIvINpUuiI1-1JpVsDQhOvuMRAU")
        FirebaseApp.configure()
        
//        let date = Date()
//        print(date.timeIntervalSince1970)
//        if let ttl = UserDefaults.standard.object(forKey: "ttl") as? Date {
//            print(ttl)
//            let timeInterval = date.timeIntervalSince((UserDefaults.standard.object(forKey: "ttl") as? Date)!)
//            print(timeInterval)
//            if (timeInterval > 600) {
//                print("han pasado 10 minutos")
//                if let statusItem = UserDefaults.standard.object(forKey: "statusItem") as? String {
//                    if statusItem != "in_process" || statusItem != "ready"  {
//                        print("ya esta entregada o ya expiró, se borró todo amigos")
//                        defaults.removeObject(forKey: "order_elements")
//                        defaults.removeObject(forKey: "order_done")
//                        defaults.removeObject(forKey: "last_id")
//                        defaults.removeObject(forKey: "stb_name")
//                    }
//
//                }
//
//
//            }
//        }
        
        if launchOptions?[UIApplicationLaunchOptionsKey.location] != nil {
            //You have a location when app is in killed/ not running state
            print(UIApplicationLaunchOptionsKey.location)
        }
        
//        if let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as? Int {
//            socket = WebSocket(url: URL(string: Constants.wsURL + "\(establishment_id)/")!, protocols: ["chat"])
//            socket.delegate = self
//            socket.connect()
//        }else{
//            socket = WebSocket(url: URL(string: Constants.wsURL + "1")!, protocols: ["chat"])
//            socket.delegate = self
//            socket.connect()
//        }
        getDataFromFirebase()
//        let formatter = DateFormatter()
//        // initially set the format based on your datepicker date / server String
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let myString = formatter.string(from: date) // string purpose I add here
//        // convert your string to date
//        let yourDate = formatter.date(from: myString)
//        //then again set the date format whhich type of output you need
//        formatter.dateFormat = "dd_MM_yyyy"
//        // again convert your date to string
//        let myStringafd = formatter.string(from: yourDate!)
//        ordersRef = Database.database().reference().child("orders")
//        ordersRef.observe(.value, with: { (snapshot) -> Void in
//            if !(snapshot.value is NSNull) {
////                Hay id de establecimiento
//                if let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as? Int {
////                    Hay id de la orden
//                    if let order_id = UserDefaults.standard.object(forKey: "order_id") as? Int {
////                        Hay orden con el id del establecimiento
//                        if let establishment = (snapshot.value as! NSDictionary).value(forKey: "\(establishment_id)") as? NSDictionary {
////                            Hay orden con la fecha de hoy
//                            if let actualDate = establishment.value(forKey: myStringafd) as? NSDictionary {
////                                Hay orden con el id
//                                if let actualOrder = actualDate.value(forKey: "\(order_id)") as? NSDictionary {
////                                    Hay llave del status
//                                    print(actualOrder, "actual order")
//                                    if let status = actualOrder.value(forKey: "status") as? Int {
////                                        Hay info del estatus
//                                        if let stats = (Constants.status as? NSDictionary)?.value(forKey: "\(status)") as? String {
//                                            self.defaults.set(stats, forKey: "statusItem")
//                                            switch stats {
//                                            //                                                Estatus: Entregada
//                                            case "delivered":
//                                                let currentDate = Date()
//                                                UserDefaults.standard.set(currentDate, forKey: "hora_entrega")
//                                                let formatter = DateFormatter()
//                                                formatter.locale = Locale(identifier: "en_US_POSIX")
//                                                formatter.dateFormat = "h:mm a"
//                                                formatter.amSymbol = "AM"
//                                                formatter.pmSymbol = "PM"
//                                                let dateString = formatter.string(from: currentDate)
//                                                print(dateString, "delivered")
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "deletePedido"), object: self)
////                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
//                                                break
//                                            //                                                Estatus: Lista
//                                            case "ready":
//                                                let currentDate = Date()
////                                                UserDefaults.standard.set(currentDate, forKey: "hora_lista")
//                                                let formatter = DateFormatter()
//                                                formatter.locale = Locale(identifier: "en_US_POSIX")
//                                                formatter.dateFormat = "h:mm a"
//                                                formatter.amSymbol = "AM"
//                                                formatter.pmSymbol = "PM"
//                                                let dateString = formatter.string(from: currentDate)
//                                                print(dateString, "ready")
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "goReady"), object: self)
////                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
//                                                break
//                                            //                                                Estatus: En proceso
//                                            case "in_process":
//                                               let currentDate = Date()
////                                               UserDefaults.standard.set(currentDate, forKey: "hora_entregada")
//                                                let formatter = DateFormatter()
//                                                formatter.locale = Locale(identifier: "en_US_POSIX")
//                                                formatter.dateFormat = "h:mm a"
//                                                formatter.amSymbol = "AM"
//                                                formatter.pmSymbol = "PM"
//                                               let dateString = formatter.string(from: currentDate)
//                                                print(dateString, "in process")
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "goHome"), object: self)
//                                               NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
////                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "showView"), object: self)
//                                                break
//                                            //                                                Estatus: por pagar
//                                            case "to_pay":
//                                                let currentDate = Date()
//                                                let formatter = DateFormatter()
//                                                formatter.locale = Locale(identifier: "en_US_POSIX")
//                                                formatter.dateFormat = "h:mm a"
//                                                formatter.amSymbol = "AM"
//                                                formatter.pmSymbol = "PM"
//                                                let dateString = formatter.string(from: currentDate)
//                                                print(dateString, "to pay")
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "showView"), object: self)
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
//                                                break
//                                            case "delayed":
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
//                                                break
//                                            default:
//                                                break
//                                            }
//
//
//                                        }
//
//                                    }
//                                }
//                            }
//                        }
//
//                    }
//                }
//            }
//
//
//        })
        
        
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
//        OneSignal.setRequiresUserPrivacyConsent(true);
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "0056ffc6-fe1b-41a1-84ed-93aa4353e706",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if (error == nil) {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }else{
                    
                }
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
       
        if let _ = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary{
            // presenting
            let storyboard = UIStoryboard(name: "Splash", bundle: nil)
            let vc = storyboard.instantiateInitialViewController() as! Loader
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
//        let date = Date()
//        let formatter = DateFormatter()
//        // initially set the format based on your datepicker date / server String
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let myString = formatter.string(from: date) // string purpose I add here
//        // convert your string to date
//        let yourDate = formatter.date(from: myString)
//        //then again set the date format whhich type of output you need
//        formatter.dateFormat = "dd_MM_yyyy"
//        // again convert your date to string
//        let myStringafd = formatter.string(from: yourDate!)
//        if let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as? Int {
//            if let order_id = UserDefaults.standard.object(forKey: "order_id") as? Int {
//                ordersRef = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
//                //        ordersRef.observe(.value, with: { (snapshot) -> Void in
//                //            let status = (snapshot.value as! NSDictionary).value(forKey: "status") as! Int
//                //            if let stats = (Constants.status as? NSDictionary)?.value(forKey: "\(status)") as? String {
//                //                NotificationCenter.default.post(name: Notification.Name(rawValue: "showView"), object: self)
//                //                //                    statusGlobal = stats
//                //                self.defaults.set(stats, forKey: "statusItem")
//                //                if stats == "delivered" {
//                //
//                //                    //Borrar el defaults del pedido
//                //                    NotificationCenter.default.post(name: Notification.Name(rawValue: "deletePedido"), object: self)
//                //                }
//                //                if stats == "ready" {
//                //                    NotificationCenter.default.post(name: Notification.Name(rawValue: "goReady"), object: self)
//                //                }
//                //
//                //                if stats == "in_process" {
//                //                    NotificationCenter.default.post(name: Notification.Name(rawValue: "goHome"), object: self)
//                //                }
//                //
//                //            }
//
//                //        })
//            }
//
//        }
        
        getDataFromFirebase()
        if locationManager.location != nil {
            createRegion(location: locationManager.location)
        }
        self.locationManager.stopUpdatingLocation()
        self.locationManager.startMonitoringSignificantLocationChanges()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEvents.activateApp()

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let payloadData = response.notification.request.content.userInfo
        let NSDict = payloadData as NSDictionary
        if let custom = NSDict.value(forKey: "custom") as? NSDictionary{
            let info = custom.value(forKey: "a") as! NSDictionary
            print("info push ", info)
             print("info custom ", custom)
            if let newsfeed = info.value(forKey: "is_newsfedd") as? Bool{
                if newsfeed{
                    print("news")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "refresh"), object: self, userInfo: payloadData)
                }
            }else{
                if let linkTo = info.value(forKey: "link_to") as? Bool {
                    if linkTo {
                        print("link to")
                        //                    Abrir landing
                        guard let url = URL(string: "https://pickpal.com/site/retrybill/") else {
                            return //be safe
                        }
                        
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url)
                        }
                    }else{
                        if let isFactura = info.value(forKey: "is_factura") as? Bool {
                            if isFactura {
                                print("factura")
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "factura"), object: self, userInfo: payloadData)
                            }
                            
                        }else{
                            
                        }
                    }
                }else{
               //     if info.value(forKey: "is_invite_exchange") as! Bool{
                        
                   
                    
                    if let num_table = (info.value(forKey: "amount") as? Int) {
                    
                    print("Es de intercambio")
                        
                    let dataPusExchange = PushExchange()
                   
                    dataPusExchange.amount = (info.value(forKey: "amount") as! Int)
                    dataPusExchange.establishmentid_from  = (info.value(forKey: "establishmentid_from") as! Int)
                    dataPusExchange.establishmentid_to = (info.value(forKey: "establishmentid_to") as! Int)
                    dataPusExchange.identifier = (info.value(forKey: "identifier") as? Int ?? 0)

                        let parameters = [
                            "dataPusExchange": dataPusExchange
                        ] as [String: Any]
                            
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "openHomePoints"), object: nil,userInfo: parameters)
                        
                    }else{
                        
                        if let isMovil = info.value(forKey: "movil") as? Bool {
                            if isMovil {
                                print("Movil")
                                if let id = info.value(forKey: "establishment_id") as? Int{
                                    openSinglePonits(id:  id )
                                    }
                                
                            }
                            
                        }
                        
                    }
                    
                //    }
                    
                   
                    
                    
                    /*
                     amount = 10;
                           "establishmentid_from" = 31;
                           "establishmentid_to" = 71;
                           identifier = 2126;
                           "is_invite_exchange" = 1;
                       };
                     */
                    
                    
                }
            }
            
            if let newsfeed = info.value(forKey: "is_newsfedd") as? Bool{
                if !newsfeed{
                    if let linkTo = info.value(forKey: "link_to") as? Bool {
                        if !linkTo {
                            if let isFactura = info.value(forKey: "is_factura") as? Bool {
                                if !isFactura {
                                    if let order_id = info.value(forKey: "order_id") as? Int{
                                        if let status_order = info.value(forKey: "status_order ") as? Int {
                                            if status_order == 5 || status_order == 8 || status_order == 9 {
                                                
                                            }else{
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                            }
                                        }else{
                                             NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                        }
                                       
                                    }
                                }
                                
                            }else{
                                if let notif = info.value(forKey: "notification_on") as? Bool {
                                    if notif {
                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reorderOrder"), object: self, userInfo: payloadData)
                                    }else{
                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
//                                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                                        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
//                                        newVC.readyDesplegar = true
//                                        newVC.currentOrderId = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
//                                        self.navigationController?.pushViewController(newVC, animated: true)
                                    }
                                }
                                if let order_id = info.value(forKey: "order_id") as? Int{
                                    if let status_order = info.value(forKey: "status_order ") as? Int {
                                        if status_order == 5 || status_order == 9 {
                                            
                                        }else{
                                            NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                        }
                                    }else{
                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                    }
                                }
                            }
                        }
                    }else{
                        if let order_id = info.value(forKey: "order_id") as? Int{
                            if let status_order = info.value(forKey: "status_order") as? Int {
                                if status_order == 5 || status_order == 8  || status_order == 9  {
//                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                }else{
                                    
                                    if info.value(forKey: "number_table") as? Int  != 0 {
                                        NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                    }
                                }
                            }else{
                                if info.value(forKey: "type_order") as? String  != "SD" {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                                }
                            }
                        }
                    }
                }else{
                    if let order_id = info.value(forKey: "order_id") as? Int{
                        if let status_order = info.value(forKey: "status_order ") as? Int {
                            if status_order == 5 || status_order == 8 || status_order == 9{
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                            }else{
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                            }
                        }else{
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                        }
                    }
                }
                
            }
            
            
        }else{
            
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
            let valueNotification = payloadData as! [String:Any]
//            if let notif = info.value(forKey: "notification_on") as? Bool {
            if let notif = valueNotification["notification_on"] as? Bool {
                if notif {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "reorderOrder"), object: self, userInfo: payloadData)
                }else{
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "openPedido"), object: self, userInfo: payloadData)
                }
            }
        }
    
        print(NSDict)
    }
    
    
    //    MARK: para mostrar la alerta
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
        getDataFromFirebase()
    }
//    Location Services
    func createRegion(location:CLLocation?) {
        
        if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            let coordinate = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
            let regionRadius = 5000.0
            
            let region = CLCircularRegion(center: CLLocationCoordinate2D(
                latitude: coordinate.latitude,
                longitude: coordinate.longitude),
                                          radius: regionRadius,
                                          identifier: "aabb")
            print(region, "region")
            region.notifyOnExit = true
            region.notifyOnEntry = true
            
            //Send your fetched location to server
            
            //Stop your location manager for updating location and start regionMonitoring
            self.locationManager.stopUpdatingLocation()
            self.locationManager.startMonitoring(for: region)
        }
        else {
            print("System can't track regions")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Entered Region")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Exited Region")
        
        locationManager.stopMonitoring(for: region)
        
        //Start location manager and fetch current location
        locationManager.startUpdatingLocation()
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if UIApplication.shared.applicationState == .active {
        } else {
            print("send location to server")
            //App is in BG/ Killed or suspended state
            //send location to server
            // create a New Region with current fetched location
            let location = locations.last
            lastLocation = location
            print(location)
            //Make region and again the same cycle continues.
            self.createRegion(location: lastLocation)
        }
    }
    
    func getDataFromFirebase() {
        let date = Date()
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd_MM_yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
//        let allOrdersRef = Database.database().reference().child("orders")
//        allOrdersRef.observe(.value, with: { (snapshot) -> Void in
//        if let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as? Int {
//            if let order_id = UserDefaults.standard.object(forKey: "order_id") as? Int {
//                self.ordersRef = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
//            }
//        }
        ordersRef = Database.database().reference().child("orders")
        self.ordersRef.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                //                Hay id de establecimiento
                if let establishment_id = UserDefaults.standard.object(forKey: "establishment_id") as? Int {
                    //                    Hay id de la orden
                    if let order_id = UserDefaults.standard.object(forKey: "order_id") as? Int {
                        //                        Hay orden con el id del establecimiento
                        if let establishment = (snapshot.value as! NSDictionary).value(forKey: "-\(establishment_id)") as? NSDictionary {
                            //                            Hay orden con la fecha de hoy
                            if let actualDate = establishment.value(forKey: myStringafd) as? NSDictionary {
                                //                                Hay orden con el id
                                if let actualOrder = actualDate.value(forKey: "-\(order_id)") as? NSDictionary {
                                    //                                    Hay llave del status
//                                    print(actualOrder, "actual order")
                                    if let status = actualOrder.value(forKey: "status_client") as? Int {
//                                    if let status = (snapshot.value as! NSDictionary).value(forKey: "status") as? Int {
                                        //                                        Hay info del estatus
                                        if let stats = (Constants.status as? NSDictionary)?.value(forKey: "\(status)") as? String {
                                            self.defaults.set(stats, forKey: "statusItem")
                                            switch stats {
                                            //                                                Estatus: Entregada
                                            case "delivered":
                                                let currentDate = Date()
                                                UserDefaults.standard.set(currentDate, forKey: "hora_entrega")
                                                let formatter = DateFormatter()
                                                formatter.locale = Locale(identifier: "en_US_POSIX")
                                                formatter.dateFormat = "h:mm a"
                                                formatter.amSymbol = "a.m."
                                                formatter.pmSymbol = "p.m."
                                                let dateString = formatter.string(from: currentDate)
                                                print(dateString, "delivered")
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "deletePedido"), object: self)
                                                //                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                                                break
                                            //                                                Estatus: Lista
                                            case "ready":
//                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "goReady"), object: self)
                                               
                                                let parameters = [
                                                    "order_ready": true
                                                ] as [String: Any]
                                                
                                                
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: nil,userInfo: parameters)
                                            
                                        
                                                
                                             
                                                let currentDate = Date()
                                                //                                                UserDefaults.standard.set(currentDate, forKey: "hora_lista")
                                                let formatter = DateFormatter()
                                                formatter.locale = Locale(identifier: "en_US_POSIX")
                                                formatter.dateFormat = "h:mm a"
                                                formatter.amSymbol = "a.m."
                                                formatter.pmSymbol = "p.m."
                                                let dateString = formatter.string(from: currentDate)
                                                print(dateString, "ready")
                                                
                                                //                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                                                break
                                            //                                                Estatus: En proceso
                                            case "in_process":
                                                let currentDate = Date()
                                                //                                               UserDefaults.standard.set(currentDate, forKey: "hora_entregada")
                                                let formatter = DateFormatter()
                                                formatter.locale = Locale(identifier: "en_US_POSIX")
                                                formatter.dateFormat = "h:mm a"
                                                formatter.amSymbol = "a.m."
                                                formatter.pmSymbol = "p.m."
                                                let dateString = formatter.string(from: currentDate)
                                                print(dateString, "in process")
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "goHome"), object: self)
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                                                //                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "showView"), object: self)
                                                break
                                            //                                                Estatus: por pagar
                                            case "to_pay":
                                                let currentDate = Date()
                                                let formatter = DateFormatter()
                                                formatter.locale = Locale(identifier: "en_US_POSIX")
                                                formatter.dateFormat = "h:mm a"
                                                formatter.amSymbol = "a.m."
                                                formatter.pmSymbol = "p.m."
                                                let dateString = formatter.string(from: currentDate)
                                                print(dateString, "to pay")
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "showView"), object: self)
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                                                break
                                            case "delayed":
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                                                break
                                            case "expired":
                                                NotificationCenter.default.post(name: Notification.Name(rawValue: "deletePedido"), object: self)
                                                break
                                            default:
                                                break
                                            }
                                            
                                            
                                        }
                                        
                                    }
                                }
                            }
                        }
                            
                    }
                }
            }
        })
//            }}})
    }
    
    
    func openHomePonits(data: PushExchange){
        

        guard let window = UIApplication.shared.keyWindow else { return }

            let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints")  as! HomePointsViewController
            
            newVC.pushExchange = data
            newVC.isPushExchange = true
          

            window.rootViewController = newVC
            window.makeKeyAndVisible()
           
    }
    
    
    func openSinglePonits(id: Int){
        
//        guard let window = UIApplication.shared.keyWindow else { return }
//        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
//        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints")  as! HomePointsViewController
//
//
//        newVC.openSingle = true
//        newVC.idSinglepontis = id
//        window.rootViewController = newVC
//        window.makeKeyAndVisible()
        
        
        
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)


        if  let conversationVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as? HomePointsViewController,
            let tabBarController = self.window?.rootViewController as? UITabBarController,
            let navController = tabBarController.selectedViewController as? UINavigationController {

            conversationVC.openSingle = true
            conversationVC.idSinglepontis = id
                
                navController.pushViewController(conversationVC, animated: true)
        }
        
        
        
    }

}
