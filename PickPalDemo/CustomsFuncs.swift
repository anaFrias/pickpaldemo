//
//  CustomsFuncs.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class CustomsFuncs: NSObject {
    
    static func getHiUser() -> NSMutableAttributedString{
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        
        print("Hora actual: \(hour):\(minute):\(second)")
        
        var welcome = ""
        
        if (hour < 12) {// Check if hour is between 8 am and 11pm
            welcome = "¡Buenos días! "
        } else if (hour <= 18) {
            welcome = "¡Buenas tardes! "
        } else {
            welcome = "¡Buenas noches! "
        }
        
        let greenColor = UIColor(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
        let attributedStringColor = [NSAttributedStringKey.foregroundColor : greenColor];
        
        let welcomeAttr = NSAttributedString(string: (UserDefaults.standard.object(forKey: "first_name") as? String ?? ""), attributes: attributedStringColor)
        let hi = NSAttributedString(string: welcome, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0)])
        
        let newString = NSMutableAttributedString()
        newString.append(hi)
        newString.append(welcomeAttr)
        
        return newString
    }
    
    static func getAttributedString(title1 : String, title2: String, ziseFont: Float = 13.0, colorText: Int = 1) -> NSMutableAttributedString{
        
        var color = UIColor()
        switch colorText {
        case 1: //color verde
            color = UIColor(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
            break
        case 2://Magenta
            color = UIColor(red: 0/255, green: 149/255, blue: 255/255, alpha: 1)
            break
        default: //color gris
            color = UIColor(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
        }
        
        
        let attributes:[NSAttributedString.Key : Any] = [
            .foregroundColor : color,
            .font : UIFont(name: "Aller-Bold", size: CGFloat(ziseFont))
        ]
        
        let titleOne = NSAttributedString(string: title1, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller-Bold", size: CGFloat(ziseFont))])
        let titleTwo = NSAttributedString(string: title2, attributes: attributes)
        
        let newString = NSMutableAttributedString()
        newString.append(titleOne)
        newString.append(titleTwo)
        
        return newString
    }
    
    //Funcion para formatear a pesos
    static func getFomatMoney(currentMoney: Double) -> String{
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: currentMoney as NSNumber) {
            return formattedTipAmount
        }else{
            return String(currentMoney)
        }
    }
    
    static func numberFormat(value: Double) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let costString = formatter.string(from: NSNumber(value: value))
        print("Fromart ",costString)
        return (costString ?? "0")
        
    }
    
    //funcion para obtenerlos iconos de los servicios de pickpal
    static func getIconsServicePickPal (data: [String]) -> [String]{
        var icons = [String]()
        for service1 in data{
            if service1 == "POD" {//Pickpal Order guide_icon_food_and_drinks
                icons.append("guide_icon_food_and_drinks")
            }
        }
        
        for service2 in data{
            if service2 == "PPR" {//Pickpal Promos guide_icon
                icons.append("guide_icon_promos")
            }
        }
        
        for service3 in data{
            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
                icons.append("guide_icon")
            }
        }
        
        for service4 in data{
            if service4 == "PPM" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("guide_icon_points")
            }
        }
        
        return icons
    }
    
    static func getIconsOrderTypesPickPal (data: [String]) -> [String]{
        var icons = [String]()
        for service1 in data{
            if service1 == "RS" {//Recoger en sucursal
                icons.append("ic_Shop")
            }
        }
        
        for service2 in data{
            if service2 == "RB" {//Pickpal Promos guide_icon
                icons.append("ic_getInBar")
            }
        }
        
        for service3 in data{
            if service3 == "SM" {//Guia PickPal guide_icon_promos
                icons.append("ic_serviceTable")
            }
        }
        
        for service4 in data{
            if service4 == "SD" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("ic_serviceHome")
            }
        }
        
        return icons
    }
    
    static func getImage(image: String) -> Int{
        switch image {
        case "guide_icon_food_and_drinks":
            return 1
    
        case "guide_icon_promos":
            return 2
            
        case "guide_icon":
            return 3
        default://guide_icon_points
            return 4
        }
    }
    
    //Funcion para obtener los horarios
    static func createHorario(horario: [ScheduleItem]) -> String{
        var i = 0;
        var j = 0;
        var horarios = [itemHorario]()
        for item in horario{
            if i == 0{
                let x = itemHorario()
                x.open = item.open_hour
                x.close = item.closing_hour
                x.addname(name: item.day)
                horarios.append(x)
                j += 1
            }else{
                if (horarios[j-1].open == item.open_hour && horarios[j-1].close == item.closing_hour){
                    horarios[j-1].addname(name: item.day)
                }else{
                    let x = itemHorario()
                    x.open = item.open_hour
                    x.close = item.closing_hour
                    x.addname(name: item.day)
                    horarios.append(x)
                    j += 1
                }
            }
            i += 1
        }
        
        var txtHorario = ""
        
        for z in 0..<horarios.count{
            if(z == horarios.count - 1){
                if(horarios[z].names.count > 1){
                    txtHorario += horarios[z].names[0] + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    
                }else{
                    if(horarios[z].names[0] == "Todos"){
                        txtHorario += "Lunes - Domingo: " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }else{
                        txtHorario += horarios[z].names[0] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }
                    
                }
            }else{
                if(horarios[z].names.count > 1){
                    txtHorario +=  horarios[z].names[0]  + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }else{
                    txtHorario += horarios[z].names[0]  + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }
            }
        }
        return txtHorario
    }
    
    static func setSmallIconTitle(titleIcon: String, titleTextPickpal: NSMutableAttributedString) -> NSMutableAttributedString{
        let titleOptions = UILabel()
        // Create Attachment
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = UIImage(named: titleIcon)
        // Set bound to reposition
        let imageOffsetY: CGFloat = -8.0
        imageAttachment.bounds = CGRect(x: -10, y: imageOffsetY, width: 25, height: 22)
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        // Add image to mutable string
        completeText.append(attachmentString)
        // Add your text to mutable string
        let textAfterIcon = titleTextPickpal
        completeText.append(textAfterIcon)
        
        return completeText
    }
    
    

}


extension String {

    // formatting text for currency textField
    func currencyInputFormatting() -> String {

        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2

        var amountWithPrefix = self

        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")

        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))

        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }

        return formatter.string(from: number)!
    }
}

extension UIView {

    enum Visibility: String {
        case visible = "visible"
        case invisible = "invisible"
        case gone = "gone"
    }

    var visibility: Visibility {
        get {
            let constraint = (self.constraints.filter{$0.firstAttribute == .height && $0.constant == 0}.first)
            if let constraint = constraint, constraint.isActive {
                return .gone
            } else {
                return self.isHidden ? .invisible : .visible
            }
        }
        set {
            if self.visibility != newValue {
                self.setVisibility(newValue)
            }
        }
    }

    @IBInspectable
    var visibilityState: String {
        get {
            return self.visibility.rawValue
        }
        set {
            let _visibility = Visibility(rawValue: newValue)!
            self.visibility = _visibility
        }
    }

    func setVisibility(_ visibility: Visibility) {
        let constraints = self.constraints.filter({$0.firstAttribute == .height && $0.constant == 0 && $0.secondItem == nil && ($0.firstItem as? UIView) == self})
        let constraint = (constraints.first)

        switch visibility {
        case .visible:
            constraint?.isActive = false
            self.isHidden = false
            break
        case .invisible:
            constraint?.isActive = false
            self.isHidden = true
            break
        case .gone:
            self.isHidden = true
            if let constraint = constraint {
                constraint.isActive = true
            } else {
                let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 0)
                // constraint.priority = UILayoutPriority(rawValue: 999)
                self.addConstraint(constraint)
                constraint.isActive = true
            }
            self.setNeedsLayout()
            self.setNeedsUpdateConstraints()
        }
    }
}
