//
//  SelectCardViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import OpenpayKit

class SelectCardViewController: UIViewController,paymentDelegate {

    
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var cards = [Card]()
    var paymentWS = PaymentWS()
    var idEstablishment = Int()
    var purchasedAmount  = Double()
    var infoWallet = ItemSingelWallet()
    var backGroundColor = UIColor()
    var paymentSourceId = String()
    var cardSelect = Int()
    var total = Double()
    
    var from = ""
    
    //OpenPay
    var openpay : Openpay!
    var sessionID = String()
    
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var tableViewListCars: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        paymentWS.delegate = self
        paymentWS.getUserCards(client_id: client_id)
    
        lblTotal.text = "\(CustomsFuncs.getFomatMoney(currentMoney:total))"
        tableViewListCars.delegate = self
        tableViewListCars.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        paymentWS.getUserCards(client_id: client_id)
    }
    
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func btnPay(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "PaymentProcess") as! PaymentProcessViewController
        
        newVC.payment_source_id = paymentSourceId
        newVC.amount = purchasedAmount
        newVC.idEstablishment = idEstablishment
        newVC.infoWallet = self.infoWallet
        newVC.backGroundColor = self.backGroundColor
        
        newVC.from = from
        
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    


}


extension SelectCardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func didSuccessGetCards(cards: [Card]) {
        
        self.cards = cards
        tableViewListCars.reloadData()
        
        if cards.count > 0{
        cardSelect = 0
        paymentSourceId = cards[0].payment_source_id
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == cards.count{

//            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCardViewController
//
//            newVC.cards = self.cards
//            self.navigationController?.pushViewController(newVC, animated: true)
            addCardOpenPay()
//            OPENPAY
//            self.openpay.loadCardForm(in: self, successFunction: self.successCard, failureFunction: self.failCard, formTitle: "Openpay")
        }else{
            
            paymentSourceId = cards[indexPath.row].payment_source_id
            cardSelect = indexPath.row
            tableViewListCars.reloadData()
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row < cards.count){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilecardItem", for: indexPath) as! ProfileCardItemTableViewCell
            cell.txt.text = cards[indexPath.row].card
            
            if cardSelect == indexPath.row{
                
               
                cell.imgBackground.image = UIImage(named: "backgroundBtn")
            }else{
                
                cell.imgBackground.image = UIImage(named: "rectangle_border_gray")
                
                
                
            }
            
//            OPENPAY
//            if cards[indexPath.row].brand.uppercased() == "MASTERCARD"{
//                cell.IMG.image = UIImage(named: "iconMC")
//            }else if cards[indexPath.row].brand.uppercased() == "AMERICAN_EXPRESS"{
//                cell.IMG.image = UIImage(named: "iconAE")
//            }else{
//                cell.IMG.image = UIImage(named: "visaCard")
//            }
//            CONEKTA
            if cards[indexPath.row].brand == "MC"{
                cell.IMG.image = UIImage(named: "iconMC")
            }else if cards[indexPath.row].brand == "AMERICAN_EXPRESS"{
                cell.IMG.image = UIImage(named: "iconAE")
            }else{
                cell.IMG.image = UIImage(named: "visaCard")
            }

            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddCard", for: indexPath)
//            if cards.count >= 3 {
//                cell.isUserInteractionEnabled = false
//            }else{
//                cell.isUserInteractionEnabled = true
//            }
            return cell
        }
    }
    
    func didFailGetCards(error: String, subtitle: String) {
        
    }
    
    
}

extension SelectCardViewController: ordersDelegate {
    
    func addCardOpenPay() {
        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey:  Constants.API_KEY, isProductionMode: false, isDebug: false)
        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Openpay")
    }

    func successSessionID(sessionID: String) {
            print("SessionID: \(sessionID)")
            self.sessionID = sessionID
            //        LoadingOverlay.shared.showOverlay(view: self.view)
            openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
        }
        
        func failSessionID(error: NSError) {
            LoadingOverlay.shared.hideOverlayView()
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
                
            }
        }
        func successCard() {
            openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
            
        }
        
        func failCard(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }
        
        func successToken(token: OPToken) {
            print("TokenID: \(token.id)")
            DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: self.view)
                self.paymentWS.addNewCardOppa(client_id:  self.client_id, token_card: token.id, session_id: self.sessionID)
            }
        }
    

        
        func failToken(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }
    
    
    func didSuccessAddCard() {
        LoadingOverlay.shared.hideOverlayView()
        paymentWS.getUserCards(client_id: client_id)
    }
    
    func didFailAddCard(error: String, subtitle: String){
        
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = "Error"
        newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(subtitle)"
        self.present(newXIB, animated: true, completion: nil)
    }

}
