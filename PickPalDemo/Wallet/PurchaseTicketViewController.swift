//
//  PurchaseTicketViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 29/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
class PurchaseTicketViewController: UIViewController {
    
    
    @IBOutlet weak var lblAmountPurchase: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblPickpalAmount: UILabel!
    @IBOutlet weak var lblTotalPurchase: UILabel!
    @IBOutlet weak var lblNameWallet: UILabel!
    @IBOutlet weak var ExpeditionDate : UILabel!
    @IBOutlet weak var imgLogoEstablishment: UIImageView!
    
    @IBOutlet weak var containerHaeder: UIView!
    @IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblHeaderNameEstablishment: UILabel!
    
    var reward:Double!
    var total:Double!
    var amount:Double!
    var expirationDate:String!
    var infoWallet = ItemSingelWallet()
    var backGroundColor = UIColor()
    
    var from = ""
    @IBOutlet weak var containerMessage: UIView!
    
    //(reward: Double, total:Double, amount: Double, expirationDate: String)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        containerHaeder.clipsToBounds = false
        containerHaeder.layer.masksToBounds = false
        containerHaeder.layer.cornerRadius = 5

        containerCard.clipsToBounds = false
        containerCard.layer.masksToBounds = false
        containerCard.layer.cornerRadius = 5
        
        
        
        containerMessage.clipsToBounds = false
        containerMessage.layer.masksToBounds = false
        containerMessage.layer.cornerRadius = 15
        containerMessage.layer.shadowColor = UIColor.black.cgColor
        containerMessage.layer.shadowOpacity = 0.35
        containerMessage.layer.shadowOffset = .zero
        containerMessage.layer.shadowRadius = 2
        
        Nuke.loadImage(with: URL(string: infoWallet.logo)!, into: imgLogoEstablishment)
        lbllocation.text = "\(infoWallet.place ?? ""),\(infoWallet.state ?? "")"
        containerHaeder.backgroundColor = backGroundColor
        lblAmountPurchase.text = "\(CustomsFuncs.getFomatMoney(currentMoney:amount!))"
        lblPickpalAmount.text = "+\(CustomsFuncs.getFomatMoney(currentMoney:reward!)) que te regala Pickpal"
        lblTotalPurchase.text = "un total de: \(CustomsFuncs.getFomatMoney(currentMoney:total!))"
        lblNameWallet.text = "A tu wallet de \(infoWallet.name!)"
        lblHeaderNameEstablishment.text = "\(infoWallet.name!)"
        
        
        ExpeditionDate.text = "VIGENCIA HASTA  \(expirationDate ?? "")"
//
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "yyyy/MM/dd"
//
//        let dateFormatterPrint = DateFormatter()
//        dateFormatterPrint.dateFormat = "dd/mm/yyyy"
//
//        if let date = dateFormatterGet.date(from: expirationDate) {
//            ExpeditionDate.text = "VIGENCIA HASTA  \(date)"
//
//        } else {
//           print("There was an error decoding the string")
//        }
//
    }
    
    
    @IBAction func btnAceptar(_ sender: Any) {
        
        
        if from == "Carrito"{
            
            guard let navigationController = self.navigationController else { return }
            var navigationArray = navigationController.viewControllers // Para obtener toda la pila de UIViewController como Array
            navigationArray.remove(at: navigationArray.count - 4) // To remove previous UIViewController
            navigationArray.remove(at: navigationArray.count - 3) // To remove previous UIViewController
            navigationArray.remove(at: navigationArray.count - 2) // To remove previous UIViewController
            navigationArray.remove(at: navigationArray.count - 1) // To remove previous UIViewController
            
            
            self.navigationController?.viewControllers = navigationArray
            
            self.navigationController?.popViewController(animated: true)
        }else{
            
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
        
        
        
    }
    
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    

}
