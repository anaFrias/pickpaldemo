//
//  PayWhitQRViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 09/02/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
import Firebase

class PayWhitQRViewController: UIViewController, AlertAdsDelegate, walletDelegate {


    @IBOutlet weak var textFileAmountPay: UITextField!
    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var lblTextAmount: UILabel!
    @IBOutlet weak var imgQR: UIImageView!
    
    @IBOutlet weak var btnGenerateQR: UIButton!
    @IBOutlet weak var containerQR: UIView!
    
    @IBOutlet weak var containerHeader: UIView!
    @IBOutlet weak var lbLocationEstablishment: UILabel!
    @IBOutlet weak var logoEstablishment: UIImageView!
    @IBOutlet weak var cardHeader: UIView!
    @IBOutlet weak var lblBalanceUser: UILabel!
    @IBOutlet weak var containerCompleteProcess: UIView!
    @IBOutlet weak var lblNewBalanceUser: UILabel!
    @IBOutlet weak var lblHeaderNameEstablishment: UILabel!
    
    var infoWallet = infoHeaderWallet()
    var infoWalletSingle = ItemSingelWallet()
    var backGroundColor = UIColor()
    var userCurrentAmount = Double()
    var walletWS = WalletWS()
    
    var amountPay = Double()
    var modifiersObserve: DatabaseReference!
    var itemReference: DatabaseReference!
    var handle: DatabaseHandle!
    var handleOperation: DatabaseHandle!
    var extrasObserve: DatabaseReference!

    
    @IBOutlet weak var lblAmuntPAy: UILabel!
    @IBOutlet weak var lblMesaggeEstablishment: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        walletWS.delegate = self
        cardHeader.clipsToBounds = false
        cardHeader.layer.masksToBounds = false
        cardHeader.layer.cornerRadius = 5
        
        lblNewBalanceUser.visibility = .gone
        containerHeader.clipsToBounds = false
        containerHeader.layer.masksToBounds = false
        containerHeader.layer.cornerRadius = 5
        
        Nuke.loadImage(with: URL(string: infoWallet.img)!, into: logoEstablishment)
        lbLocationEstablishment.text = "\(infoWallet.place ?? ""),\(infoWallet.state ?? "")"
        
        lblHeaderNameEstablishment.text = "\(infoWallet.name!)"
        lblBalanceUser.text = "\(CustomsFuncs.getFomatMoney(currentMoney: userCurrentAmount))"
        containerHeader.backgroundColor = backGroundColor
        
        lblGreeting.text = "Hola, \( UserDefaults.standard.object(forKey: "first_name")!)"
        textFileAmountPay.addTarget(self, action: #selector(self.newBalance(_:)), for: .editingChanged)

        containerQR.isHidden = true
        containerCompleteProcess.isHidden = true
    }
    
    
    
    @objc func newBalance(_ textField: UITextField){
        
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textFileAmountPay.text = amountString
        }
        var textToMoney = textFileAmountPay.text!.replacingOccurrences(of: "$", with: "")
        textToMoney = textToMoney.replacingOccurrences(of: ",", with: "")
        
        if textToMoney != ""{
            lblNewBalanceUser.visibility = .visible
            amountPay = Double (textToMoney) ?? 0
            let total = userCurrentAmount - amountPay
            
            lblNewBalanceUser.text = "El saldo de tu Wallet después de este pago será de \(CustomsFuncs.getFomatMoney(currentMoney:total    ))"
        }else{
            
            lblNewBalanceUser.visibility = .gone
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        containerQR.isHidden = true
        containerCompleteProcess.isHidden = true
        
        
        
    }

    @IBAction func btnGenerate(_ sender: Any) {
        
        
        var textToMoney = textFileAmountPay.text!.replacingOccurrences(of: "$", with: "")
        textToMoney = textToMoney.replacingOccurrences(of: ",", with: "")
        
        
        if  textToMoney != ""{
            
            amountPay = Double (textToMoney)!
            
            if amountPay > 2{
                
                
                if amountPay <= userCurrentAmount{
                    
                    walletWS.getQRCodePay(wallet_pk: infoWallet.id!, amount: amountPay)
                    
                }else{
                    
                    showAlert(title: "Saldo insuficiente", subtitle: "El monto ingresado no puede ser mayor a tu saldo disponible" )
                    
                }
                
            }else{
                
                showAlert(title: "\(UserDefaults.standard.object(forKey: "first_name")!)", subtitle: "El monto minimo es de $3.00" )
            }
            
        }else{
              
            showAlert(title: "\(UserDefaults.standard.object(forKey: "first_name")!)", subtitle: "Por favor ingresa una cantidad para continuar" )
            
        }
        
    }
    
    
    
    @IBAction func btnContinue(_ sender: Any) {
        
        // Enviar al single
        

        
        walletWS.getMyWallet(idEstablishment: infoWallet.id)
        
        
    }
    
    
    func didMyWallet(walletInfo: Wallet,haeaderWallet: infoHeaderWallet) {
        
        containerQR.isHidden = true
        containerCompleteProcess.isHidden = true
        textFileAmountPay.text = ""
        textFileAmountPay.isEnabled = true
        self.btnGenerateQR.isHidden = false
        self.infoWallet.moneyWallet = walletInfo.money_wallet
        self.infoWallet.available_money =  walletInfo.available_money_to_buy
           
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleEstablishmentWallet") as! HistoryWalletViewController
        newVC.infoWallet = self.infoWallet
        newVC.headerWallet = walletInfo
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func didFailMyWallet(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = subtitle
            newXIB.mTitle = error
            newXIB.mTitleButton = "Aceptar"
            newXIB.mToHome = false
            newXIB.mId = 0
            newXIB.valueCall = 2
            newXIB.isFromAdd = false
            newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    func showAlert(title: String,subtitle:String ){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = title
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
     func didGetQRPay(urlQR:String, folio: String, client_id: Int){
    
        containerCompleteProcess.isHidden = true
        Nuke.loadImage(with: URL(string: urlQR)!, into: imgQR)
        containerQR.isHidden = false
        textFileAmountPay.isEnabled = false
        
        itemReference = Database.database().reference().child("qr_payments").child("-\(client_id)").child(folio)
        handle = itemReference.observe(.value) { (snapshot) in
            
            if let dictionary = snapshot.value as? NSDictionary {
                
                if dictionary.value(forKey: "is_used") as! Bool{
                    
                    self.containerCompleteProcess.isHidden = false
                    self.containerQR.isHidden = true
                    self.lblAmuntPAy.text = "\(CustomsFuncs.getFomatMoney(currentMoney:  self.amountPay))"
                    
                }else{
                    self.btnGenerateQR.isHidden = true
                    
                    self.lblMesaggeEstablishment.text = "Pago Exitoso en \(self.infoWallet.name!)"
                    self.lblTextAmount.text = "Código Válido por \(CustomsFuncs.getFomatMoney(currentMoney:  self.amountPay))"
                    
                }
                
            }
        
        }
        
    }
    
    
    func  didFailetQRPay(error: String, subtitle: String){
        
        showAlert(title: error,subtitle:subtitle )
        
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if itemReference != nil {
            itemReference.removeObserver(withHandle: handle)
        }
        
    }
    
    
}
