    //
//  HomeWalletViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 18/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
    class HomeWalletViewController: UIViewController, AlertAdsDelegate,submenuProtocolDelegate {
        
 
        
        func gotToMyWallets() {
            
        }
    
    @IBOutlet weak var lblGreeting: UILabel!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var listEstablismentTableView: UITableView!
    @IBOutlet weak var containerFilter: UIView!
    @IBOutlet weak var containerNotWallets: UIView!
    @IBOutlet weak var containerNotFound: UIView!
    @IBOutlet weak var textFileSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
        
    @IBOutlet weak var lblPagarMensaje: UILabel!
    var statusProfile = StatusProfile()
    var walletSW = WalletWS()
    var listWallet = [infoHeaderWallet]()
    var listWalletAux = [infoHeaderWallet]()
    var uiColorArray = [UIColor(red: 0.62, green: 0.73, blue: 0.23, alpha: 1.00), UIColor(red: 0.08, green: 0.25, blue: 0.53, alpha: 1.00), UIColor(red: 0.95, green: 0.78, blue: 0.27, alpha: 1.00), UIColor(red: 0.77, green: 0.16, blue: 0.12, alpha: 1.00)]
        
    var filterListWallet = [infoHeaderWallet]()
    var wordSearch = "SUSHI ROLL"
    var textTermsAndConditions = ""
    var avatar = UserDefaults.standard.object(forKey: "avatar") as! String
    var infoWallet = infoHeaderWallet()
    var infoWalletSingle = ItemSingelWallet()
    var openMenu = false
    var goToPay = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listEstablismentTableView.delegate = self
        listEstablismentTableView.dataSource = self
        walletSW.delegate = self
        
        textFileSearch.clipsToBounds = true
        textFileSearch.layer.cornerRadius = 18
        textFileSearch.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        imgUser.layer.masksToBounds = true
        imgUser.layer.shadowColor = UIColor.black.cgColor
        imgUser.layer.shadowOpacity = 0.35
        imgUser.layer.shadowOffset = .zero
        imgUser.layer.shadowRadius = 2

        
        
        btnSearch.clipsToBounds = true
        btnSearch.layer.cornerRadius = 18
        btnSearch.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        if avatar != ""{
            
            Nuke.loadImage(with: URL(string: avatar)!, into: imgUser)
        }
        
        if goToPay{
            
            lblPagarMensaje.visibility = .visible
        }else{
            lblPagarMensaje.visibility = .gone
        }
        
        containerNotWallets.isHidden = true
        containerNotFound.isHidden = true
  
        containerNotWallets.isHidden = true
        containerFilter.clipsToBounds = false
        containerFilter.layer.masksToBounds = false
        containerFilter.layer.cornerRadius = 18
        containerFilter.layer.shadowColor = UIColor.black.cgColor
        containerFilter.layer.shadowOpacity = 0.35
        containerFilter.layer.shadowOffset = .zero
        containerFilter.layer.shadowRadius = 2
        listEstablismentTableView.register(UINib(nibName: "EstablishmentWalletCell", bundle: nil), forCellReuseIdentifier: "walletCell")

        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
       
        
        
        
        var welcome = ""
        
        if (hour < 12) {
            welcome = "¡Buenos días "
        } else if (hour <= 18) {
            welcome = "¡Buenas tardes "
        } else {
            welcome = "¡Buenas noches "
        }
        
        let greenColor = UIColor(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
        let attributedStringColor = [NSAttributedStringKey.foregroundColor : greenColor];
        let welcomeAttr = NSAttributedString(string: "\((UserDefaults.standard.object(forKey: "first_name") as? String ?? "" ))!", attributes: attributedStringColor)
        let hi = NSAttributedString(string: welcome, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0)])
        
        let newString = NSMutableAttributedString()
        newString.append(hi)
        newString.append(welcomeAttr)
        
        lblGreeting.attributedText = newString
        
        
        walletSW.getMyWalletList(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
        
        textFileSearch.addTarget(self, action: #selector(self.refreshSearch(_:)), for: .editingChanged)
       
        if openMenu{
            
            presentSubMenu ()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func goToBuyWallet(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController

        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    
    @IBAction func btnFilterList(_ sender: Any) {
        
        filterList()
        
    }
    
    @IBAction func btnSeeTermsConditions(_ sender: Any) {
        
        walletSW.getTermnsConditions()
        
        
    }
        
        
        @IBAction func btnSubMenu(_ sender: Any) {
            
            
            var misWallets = false
            var sinWallet = false
            
            sinWallet = listWallet.count > 0
            
            var index = 0
            
            for wallet in listWallet{
                
                if wallet.moneyWallet == 0{
                    
                    index += 1
                    
                }
                
            }
            
            if index == listWallet.count {
                sinWallet = false
                misWallets = true
                
            }
            
            
            let newXIB = SubMenuWalletViewController(nibName: "SubMenuWalletViewController", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.sinWallet = sinWallet
            newXIB.misWallets = misWallets
            newXIB.delegate = self
            present(newXIB, animated: true, completion: nil)
            
    }
        
        
        
        
        
   
        func didGetTermnsConditions(textTermnsConditions: String) {
            let newXIB = TermsAndConditionsView(nibName: "TermsAndConditionsView", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textTermsAndConditions = textTermnsConditions
            newXIB.hiddenTerms  = false
            newXIB.hiddenList  = true
            newXIB.texTitel = "TÉRMINOS Y CONDICIONES"
            present(newXIB, animated: true, completion: nil)
        }
        
        func didFailGetTermnsConditions(error: String, subtitle: String) {
            
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = subtitle
            newXIB.mTitle = error
            newXIB.mTitleButton = "Aceptar"
            newXIB.mToHome = false
            newXIB.mId = 0
            newXIB.valueCall = 2
            newXIB.isFromAdd = false
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
        }
        
        
        func didGetListFaqs(lisFaqs: [ItemFaqs]) {
            
            
            let newXIB = TermsAndConditionsView(nibName: "TermsAndConditionsView", bundle: nil)
            newXIB.modalTransitionStyle = .coverVertical
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textTermsAndConditions = ""
            newXIB.hiddenTerms  = true
            newXIB.hiddenList  = false
            newXIB.texTitel = "PREGUNTAS FRECUENTES"
            newXIB.listfaqsArray = lisFaqs
            present(newXIB, animated: true, completion: nil)
            
            
            
        }
    

        @IBAction func btnReloadList(_ sender: Any) {
            
            
            self.listWallet = listWalletAux
            self.listEstablismentTableView.reloadData()
            containerNotFound.isHidden = true
            textFileSearch.text = ""
        }
        
        
        @objc func refreshSearch(_ textField: UITextField){
            
            filterList()
            
        }
        
        
        @IBAction func btnCleanTexfilSearch(_ sender: Any) {
            
            textFileSearch.text = ""
            self.listWallet = listWalletAux
            self.listEstablismentTableView.reloadData()
            containerNotFound.isHidden = true
            listEstablismentTableView.isHidden = false
            
        }
        
        
        
        func filterList(){
            
            let searchText  = textFileSearch.text!
             //add matching text to arrya
            self.listWallet = self.listWallet.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})

            
             self.listEstablismentTableView.reloadData();
            
            
            if listWallet.count == 0{
                
                containerNotFound.isHidden = false
                listEstablismentTableView.isHidden = true
                
            }
            
            if searchText == ""{
                
                self.listWallet = listWalletAux
                self.listEstablismentTableView.reloadData()
                containerNotFound.isHidden = true
                listEstablismentTableView.isHidden = false
            }
            
        }
        
        
        func presentSubMenu (){
            
            
//            let newXIB = SubMenuWalletViewController(nibName: "SubMenuWalletViewController", bundle: nil)
//            newXIB.modalTransitionStyle = .coverVertical
//            newXIB.modalPresentationStyle = .overCurrentContext
//            newXIB.delegate = self
//            present(newXIB, animated: true, completion: nil)
//
            
            
        }
        
        func gotToPay(){
            
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
            newVC.goToPay = true
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
        func goToQuestions() {
            walletSW.getListFaqs()
        }
        
        func goToBuy() {
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController

            newVC.from = "SubMenu"
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
        func openAlertSinWallet() {
            print("Sin Wallets")
        }
        
}
    
    
    extension HomeWalletViewController: UITableViewDelegate, UITableViewDataSource, EstablishmentWalletDelagate, walletDelegate{
  
    
    func getEstablishmentWallet(listWallet: [infoHeaderWallet]){
        
        var count = 0
        var countColor = 0
        for _ in listWallet{
            if count < 4{
                listWallet[countColor].backGroundColor = uiColorArray[count]
            }else{
                count = 0
                listWallet[countColor].backGroundColor = uiColorArray[count]
            }
            count = count + 1
            countColor = countColor + 1
        }
        
        self.listWallet = listWallet
        
        
        listWalletAux = listWallet
       
        self.listEstablismentTableView.reloadData()
        
        containerNotFound.isHidden = true
        
        if listWallet.count == 0{
            containerNotWallets.isHidden = false
        }
        
        
    }
    
    
    func didFailGetEstablishmentWallet(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return listWallet.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "walletCell", for: indexPath) as! EstablishmentWalletTableViewCell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "walletCell")  as! EstablishmentWalletTableViewCell
        let data = listWallet[indexPath.row]
        cell.selectionStyle = .none
       
        cell.loadInfoCard(info: data, from: "MyWallets")
        
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }
    
        
        
    func goToSingle(itemSelect: infoHeaderWallet) {
        
        if goToPay{
            
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "PayWhitQR") as! PayWhitQRViewController
            newVC.userCurrentAmount = itemSelect.moneyWallet!
            newVC.infoWallet = itemSelect
            newVC.backGroundColor = itemSelect.backGroundColor
            newVC.infoWalletSingle = infoWalletSingle
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }else{
            
            infoWallet = itemSelect
            
            walletSW.getMyWallet(idEstablishment: itemSelect.id)
            
        }
        
       
        
    }
        
        
        func didMyWallet(walletInfo: Wallet,haeaderWallet: infoHeaderWallet) {
               
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "SingleEstablishmentWallet") as! HistoryWalletViewController
            newVC.infoWallet = self.infoWallet
            newVC.headerWallet = walletInfo
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
        func didFailMyWallet(error: String, subtitle: String) {
            
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.mMessage = subtitle
                newXIB.mTitle = error
                newXIB.mTitleButton = "Aceptar"
                newXIB.mToHome = false
                newXIB.mId = 0
                newXIB.valueCall = 2
                newXIB.isFromAdd = false
                newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
        }
    
}
