//
//  TermsAndConditionsView.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 25/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TermsAndConditionsView: UIViewController {
    
    @IBOutlet weak var lblTermsAndConditions: UITextView!
    @IBOutlet weak var listFaqs: UITableView!
    @IBOutlet weak var lblTitleWindow: UILabel!
    
    var textTermsAndConditions = "Hola usuario bonito"
    var listfaqsArray = [ItemFaqs]()
    var hiddenList = false
    var hiddenTerms = false
    var texTitel = ""
    var dinamicHeight: CGFloat = 40
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listFaqs.delegate = self
        listFaqs.dataSource = self
        lblTermsAndConditions.text = textTermsAndConditions
        listFaqs.isHidden = hiddenList
        lblTermsAndConditions .isHidden = hiddenTerms
        lblTitleWindow.text = texTitel
        listFaqs.register(UINib(nibName: "ItemFaqsTableViewCell", bundle: nil), forCellReuseIdentifier: "itemFaqsCell")
       
       
    }


    @IBAction func closeView(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }

}


extension TermsAndConditionsView: UITableViewDelegate, UITableViewDataSource, walletDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return listfaqsArray.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemFaqsCell", for: indexPath) as! ItemFaqsTableViewCell
        
        
        let data = listfaqsArray[indexPath.row]
        cell.selectionStyle = .none
        cell.loadInfo(info: data)
        dinamicHeight += cell.llblAnswer.frame.height
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
        return cell
    }
    
    
    func goToSingle() {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleEstablishmentWallet") as! HistoryWalletViewController

        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return dinamicHeight
        
    }
    
}
