//
//  SubMenuWalletViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 29/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol submenuProtocolDelegate {
    func goToBuy()
    func gotToMyWallets()
    func gotToPay()
    func goToQuestions()
    func openAlertSinWallet()
}


class SubMenuWalletViewController: UIViewController {
    @IBOutlet weak var btnFrequentQuestions: UIButton!
    
    @IBOutlet weak var btnPagarView: UIButton!
    @IBOutlet weak var btnMisWalletsView: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var btnWallets: UIButton!
    var sinWallet = false
    var misWallets = Bool()
    var delegate:submenuProtocolDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()

        viewContainer.clipsToBounds = false
        viewContainer.layer.masksToBounds = false
        viewContainer.layer.cornerRadius = 25
        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOpacity = 0.35
        viewContainer.layer.shadowOffset = .zero
        viewContainer.layer.shadowRadius = 2
        
        btnPay.clipsToBounds = false
        btnPay.layer.masksToBounds = false
        btnPay.layer.cornerRadius = 10
        
        
        btnBuy.clipsToBounds = false
        btnBuy.layer.masksToBounds = false
        btnBuy.layer.cornerRadius = 10
        
        btnWallets.clipsToBounds = false
        btnWallets.layer.masksToBounds = false
        btnWallets.layer.cornerRadius = 10
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.styleThick.rawValue]
        let underlineAttributedString = NSAttributedString(string: "Preguntas Frecuentes", attributes: underlineAttribute)
        btnFrequentQuestions.setAttributedTitle(underlineAttributedString, for: .normal)
        
        if !sinWallet{
            
            btnPagarView.backgroundColor = UIColor(red: 0.89, green: 0.89, blue: 0.89, alpha: 1.00)
            btnWallets.backgroundColor = UIColor(red: 0.89, green: 0.89, blue: 0.89, alpha: 1.00)
            

        }
        
        
        if misWallets{
            
            btnWallets.backgroundColor = UIColor(red: 0.96, green: 0.09, blue: 0.17, alpha: 1.00)
            
        }
        
        
        
    }

    @IBAction func bntClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnBuyBalance(_ sender: Any) {
        
       
        self.dismiss(animated: true, completion: nil)
        self.delegate.goToBuy()
            
        
        
        
    }
    @IBAction func btnGoToPay(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        if sinWallet{
            
           
            self.delegate.gotToPay()
            
        }else{
            
            self.delegate.openAlertSinWallet()
        }
        
    }
    
    
    @IBAction func btnGoToMyWallets(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        if sinWallet{

           
            self.delegate.gotToMyWallets()
            
        }else{
            
            if misWallets{
                
                self.delegate.gotToMyWallets()
                
            }else{
                self.delegate.openAlertSinWallet()
            }
            
           
        }
        
    }
    
    @IBAction func btnFrequentQuestions(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.goToQuestions()
        
        
    }
    
    

}
