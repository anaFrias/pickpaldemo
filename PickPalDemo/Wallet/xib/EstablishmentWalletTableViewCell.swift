//
//  EstablishmentTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 19/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

protocol EstablishmentWalletDelagate {
    func goToSingle(itemSelect: infoHeaderWallet )
}

class EstablishmentWalletTableViewCell: UITableViewCell {

    @IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var imgEstablishment: UIImageView!
    @IBOutlet weak var lblCashEstablishment: UILabel!
    @IBOutlet weak var containerBackgrondColor: UIView!
    @IBOutlet weak var lblNameEstalishment: UILabel!
    
    @IBOutlet weak var viewBalanceOut: UIView!
    @IBOutlet weak var lblLocation: UILabel!
    
    var delegate: EstablishmentWalletDelagate!
    var textLocation = String()
    var logo = String()
    var textmoneyWallet = Double()
    var itemInfo = infoHeaderWallet()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerCard.clipsToBounds = false
        containerCard.layer.masksToBounds = false
        containerCard.layer.cornerRadius = 5
        containerCard.layer.shadowColor = UIColor.black.cgColor
        containerCard.layer.shadowOpacity = 0.30
        containerCard.layer.shadowOffset = .zero
        containerCard.layer.shadowRadius = 2
        
        
        imgEstablishment.clipsToBounds = false
        imgEstablishment.layer.masksToBounds = true
        imgEstablishment.layer.cornerRadius = 15
        imgEstablishment.layer.shadowColor = UIColor.black.cgColor
        imgEstablishment.layer.shadowOpacity = 0.30
        imgEstablishment.layer.shadowOffset = .zero
        imgEstablishment.layer.shadowRadius = 2
        
        
        containerBackgrondColor.clipsToBounds = false
        containerBackgrondColor.layer.masksToBounds = false
        containerBackgrondColor.layer.cornerRadius = 5
        containerBackgrondColor.layer.shadowColor = UIColor.black.cgColor
        containerBackgrondColor.layer.shadowOpacity = 0.30
        containerBackgrondColor.layer.shadowOffset = .zero
        containerBackgrondColor.layer.shadowRadius = 2
        
        viewBalanceOut.clipsToBounds = false
        viewBalanceOut.layer.masksToBounds = false
        viewBalanceOut.layer.cornerRadius = 15
        
        
        
        
//        containerBackgrondColor.clipsToBounds = true
//        containerBackgrondColor.layer.cornerRadius = 5
//        containerBackgrondColor.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func goToSingle(_ sender: Any) {
        
        self.delegate.goToSingle(itemSelect: itemInfo)
        
        
    }
    
    
    func loadInfoCard(info: infoHeaderWallet, from: String)  {
        
        
        print(info)
        lblCashEstablishment.text = "\(CustomsFuncs.getFomatMoney(currentMoney:info.moneyWallet!))"
        lblLocation.text = "\(info.place!), \(info.state!)"
        Nuke.loadImage(with: URL(string: info.img)!, into: imgEstablishment)
        lblNameEstalishment.text = info.name
        containerBackgrondColor.backgroundColor = info.backGroundColor
        
        viewBalanceOut.isHidden = true
        
        if from == "Comprar"{
            viewBalanceOut.isHidden =  !(info.moneyWallet == 0)
        }
        
        self.itemInfo = info
        
    }
    
}
