//
//  BuyWalletViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 19/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class BuyWalletViewController: UIViewController, AlertAdsDelegate,submenuProtocolDelegate {

    
    @IBOutlet weak var lblSinComercios: UILabel!
    @IBOutlet weak var lblGreeting: UILabel!
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var listEstablismentTableView: UITableView!
    @IBOutlet weak var containerFilter: UIView!
    @IBOutlet weak var containerNotFound: UIView!
    @IBOutlet weak var textFileSearch: UITextField!
    @IBOutlet weak var btnFrequentQuestions: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var lblTextSelectEstabishmet: UILabel!
    @IBOutlet weak var bntImgHome: UIButton!
    
    @IBOutlet weak var btnBackView: UIButton!
    
    var statusProfile = StatusProfile()
    var walletSW = WalletWS()
    var listWallet = [infoHeaderWallet]()
    var listWalletAux = [infoHeaderWallet]()
    var uiColorArray = [UIColor(red: 0.62, green: 0.73, blue: 0.23, alpha: 1.00), UIColor(red: 0.08, green: 0.25, blue: 0.53, alpha: 1.00), UIColor(red: 0.95, green: 0.78, blue: 0.27, alpha: 1.00), UIColor(red: 0.77, green: 0.16, blue: 0.12, alpha: 1.00)]
    
    var uiColorBackground = UIColor()
        
    var filterListWallet = [infoHeaderWallet]()
    var avatar = UserDefaults.standard.object(forKey: "avatar") as! String
    var infoWallet = infoHeaderWallet()
    var from = ""
    var sinWallet = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        listEstablismentTableView.delegate = self
        listEstablismentTableView.dataSource = self
        walletSW.delegate = self
        
        
        textFileSearch.clipsToBounds = true
        textFileSearch.layer.cornerRadius = 18
        textFileSearch.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        
        imgUser.layer.cornerRadius = imgUser.frame.height/2
        imgUser.clipsToBounds = true
        imgUser.layer.masksToBounds = true
        imgUser.layer.shadowColor = UIColor.black.cgColor
        imgUser.layer.shadowOpacity = 0.35
        imgUser.layer.shadowOffset = .zero
        imgUser.layer.shadowRadius = 2
        
        btnSearch.clipsToBounds = true
        btnSearch.layer.cornerRadius = 18
        btnSearch.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
       
        if avatar != ""{
            
            Nuke.loadImage(with: URL(string: avatar)!, into: imgUser)
        }
       
        containerNotFound.isHidden = true
  

        containerFilter.clipsToBounds = false
        containerFilter.layer.masksToBounds = false
        containerFilter.layer.cornerRadius = 18
        containerFilter.layer.shadowColor = UIColor.black.cgColor
        containerFilter.layer.shadowOpacity = 0.35
        containerFilter.layer.shadowOffset = .zero
        containerFilter.layer.shadowRadius = 2
        listEstablismentTableView.register(UINib(nibName: "EstablishmentWalletCell", bundle: nil), forCellReuseIdentifier: "walletCell")
        
        
        if from == "SubMenu" {
            
            btnBackView.setImage(UIImage(named: "cambiarhome.png"), for: .normal)
            bntImgHome.setImage(UIImage(named: "HomeButton.png"), for: .normal)
            
        }
        
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
       
        
        
        
        var welcome = ""
        
        if (hour < 12) {
            welcome = "¡Buenos días "
        } else if (hour <= 18) {
            welcome = "¡Buenas tardes "
        } else {
            welcome = "¡Buenas noches "
        }
        
        let greenColor = UIColor(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
        let attributedStringColor = [NSAttributedStringKey.foregroundColor : greenColor];
        let welcomeAttr = NSAttributedString(string: "\((UserDefaults.standard.object(forKey: "first_name") as? String ?? "" ))!", attributes: attributedStringColor)
        let hi = NSAttributedString(string: welcome, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0)])
        
        let newString = NSMutableAttributedString()
        newString.append(hi)
        newString.append(welcomeAttr)

        lblGreeting.attributedText = newString
        
        walletSW.getAvailableEstablishmentsWalelt()
        
        textFileSearch.addTarget(self, action: #selector(self.refreshSearch(_:)), for: .editingChanged)
        
        
        
    }
    
    
    
    

    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom =  Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func btnFilterList(_ sender: Any) {
        
        filterList()
        
    }
    
    
    @IBAction func btnSeeFaqs(_ sender: Any) {
        
        
        walletSW.getListFaqs()
        
        
        
    }
    
    
    @IBAction func btnSubMenu(_ sender: Any) {
        
        
        walletSW.getMyWalletList(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
    }
    
    func getEstablishmentWallet(listWallet: [infoHeaderWallet]){
                
        var misWallets = false
        sinWallet = listWallet.count > 0
        
        // SI TIENE WALLETS VERIFICAMOS QUE NO TENGA SALDO EN NINGUNO
        if listWallet.count != 0{
            var index = 0
            
            for wallet in listWallet{
                
                if wallet.moneyWallet == 0{
                    
                    index += 1
                    
                }
                
            }
            
            if index == listWallet.count {
                sinWallet = false
                misWallets = true
                
            }
        }
        
        
        let newXIB = SubMenuWalletViewController(nibName: "SubMenuWalletViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.sinWallet = sinWallet
        newXIB.misWallets = misWallets
        newXIB.delegate = self
        present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    func didFailGetEstablishmentWallet(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func openAlertSinWallet() {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "¡Compra saldo y obtén 10% más! "
        newXIB.mTitle = "No tienes ningún Wallet con saldo"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    
    
    
    
    func gotToPay(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        newVC.goToPay = true
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func goToQuestions() {
        walletSW.getListFaqs()
    }
    
    func goToBuy() {
//        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
//        let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController
//
//        //newVC.singleInfo = info
//
//        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func gotToMyWallets(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }

    
    @IBAction func btnCleanSearch(_ sender: Any) {
        
        textFileSearch.text = ""
        self.listWallet = listWalletAux
        self.listEstablismentTableView.reloadData()
        containerNotFound.isHidden = true
        lblTextSelectEstabishmet.isHidden = false
        listEstablismentTableView.isHidden = false

        
    }
    
    func didGetListFaqs(lisFaqs: [ItemFaqs]) {
        
        
        let newXIB = TermsAndConditionsView(nibName: "TermsAndConditionsView", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.textTermsAndConditions = ""
        newXIB.hiddenTerms  = true
        newXIB.hiddenList  = false
        newXIB.texTitel = "PREGUNTAS FRECUENTES"
        newXIB.listfaqsArray = lisFaqs
        present(newXIB, animated: true, completion: nil)
        
        
        
    }
    
    
    @objc func refreshSearch(_ textField: UITextField){
        
        filterList()
       
        
    }
    
    
    func filterList(){
        
        let searchText  = textFileSearch.text!
         //add matching text to arrya
        self.listWallet = self.listWallet.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})

        
         self.listEstablismentTableView.reloadData();
        
        
        if listWallet.count == 0{
            
            containerNotFound.isHidden = false
            listEstablismentTableView.isHidden = true
        }
        
        if searchText == ""{
            
            self.listWallet = listWalletAux
            self.listEstablismentTableView.reloadData()
            containerNotFound.isHidden = true
            lblTextSelectEstabishmet.isHidden = false
            listEstablismentTableView.isHidden = false
        }else{
            
            lblTextSelectEstabishmet.isHidden = true
        }
        
    }


}

extension BuyWalletViewController: UITableViewDelegate, UITableViewDataSource, EstablishmentWalletDelagate, walletDelegate{
    
    
    func didGetListWalletAvailable(listWallet: [infoHeaderWallet]) {
        var count = 0
        var countColor = 0
        for _ in listWallet{
            if count < 4{
                listWallet[countColor].backGroundColor = uiColorArray[count]
            }else{
                count = 0
                listWallet[countColor].backGroundColor = uiColorArray[count]
            }
            count = count + 1
            countColor = countColor + 1
        }
        
        self.listWallet = listWallet
        
        
        listWalletAux = listWallet
       
        self.listEstablismentTableView.reloadData()
        
        if listWallet.count > 0{
            
            containerNotFound.isHidden = true
            lblSinComercios.isHidden = true
            lblTextSelectEstabishmet.isHidden = false
        }else{
            lblSinComercios.isHidden = false
            containerNotFound.isHidden = true
            lblTextSelectEstabishmet.isHidden = true
        }
       
        
    }
        
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return listWallet.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "walletCell", for: indexPath) as! EstablishmentWalletTableViewCell
        let data = listWallet[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "walletCell")  as! EstablishmentWalletTableViewCell
            
      
        cell.isUserInteractionEnabled = data.moneyWallet != 0
       
        cell.selectionStyle = .none
        cell.delegate = self
        cell.loadInfoCard(info: data, from: "Comprar")
        
        
        return cell
    }
    
    
    func goToSingle(itemSelect: infoHeaderWallet) {
        
        self.uiColorBackground = itemSelect.backGroundColor
        walletSW.getInfoSingleEstablishmentsWalelt(idEstablishment: itemSelect.id)
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 110
    }
    
    
    
    func didGetInfoWallet(infoWallet: ItemSingelWallet){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleWallet") as! SingleWalletViewController
        newVC.infoWallet = infoWallet
        newVC.backGroundColor =  self.uiColorBackground
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func didFailGetInfoWallet(error: String, subtitle: String) {
        
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    
}

