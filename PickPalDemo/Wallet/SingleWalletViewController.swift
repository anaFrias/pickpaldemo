//
//  SingleWalletViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 21/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import GoogleMaps
import MapKit
import UIKit
import Nuke

class SingleWalletViewController: UIViewController , AlertAdsDelegate,submenuProtocolDelegate,walletDelegate {

    
    @IBOutlet weak var containerBuy: UIView!
    @IBOutlet weak var cantainerMap: UIView!
    @IBOutlet weak var containerInfo: UIView!
    @IBOutlet weak var mapEstablishment: GMSMapView!
 
    
    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var containerHeader: UIView!
    @IBOutlet weak var lbLocationEstablishment: UILabel!
    @IBOutlet weak var logoEstablishment: UIImageView!
    @IBOutlet weak var cardHeader: UIView!
    @IBOutlet weak var lblHeaderEstablismentName: UILabel!
    
    //Variables Compra saldo
    
    @IBOutlet weak var lblAvailableBalance: UILabel!
    @IBOutlet weak var textFileAmountBuy: UITextField!
    @IBOutlet weak var lblAmountPickpal: UILabel!
    @IBOutlet weak var lblTotalPickpalCash: UILabel!
    @IBOutlet weak var lblMessageWallet: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var lblMessagePickpal: UILabel!
    var newBalance = Double()
    var userBalance = Double()
    var amountPurchased = Double()
    var walletSW = WalletWS()
    var sinWallet = false
    
    //Variables Mapa
    
    var locationManager = CLLocationManager()
    var location: CLLocationCoordinate2D!
    
    //Variables Info
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSchedule: UILabel!
    @IBOutlet weak var lblPaymentProcess: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    
    @IBOutlet weak var lblTitleSocialMedia: UILabel!
    
    //Botones
    
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var btnWeb: UIButton!
    @IBOutlet weak var btnInstagram: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnTwiter: UIButton!
    
    
    var infoWallet = ItemSingelWallet()
    var backGroundColor = UIColor()
    var amountBuy = Double()
    
    var from = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapEstablishment.delegate = self
        locationManager.delegate = self
        walletSW.delegate = self
        
        cardHeader.clipsToBounds = false
        cardHeader.layer.masksToBounds = false
        cardHeader.layer.cornerRadius = 5
        
       
        containerHeader.clipsToBounds = false
        containerHeader.layer.masksToBounds = false
        containerHeader.layer.cornerRadius = 5
        
        Nuke.loadImage(with: URL(string: infoWallet.logo)!, into: logoEstablishment)
        lbLocationEstablishment.text = "\(infoWallet.place ?? ""),\(infoWallet.state ?? "")"
        lblHeaderEstablismentName.text = "\(infoWallet.name!)"
        lblAvailableBalance.text = "\(CustomsFuncs.getFomatMoney(currentMoney:infoWallet.amountAvailable!))"
        containerHeader.backgroundColor = backGroundColor
        lblMessagePickpal.text = "¡Recuerda que con PickPal Cash, \n por cada peso que compres, obtienes un \(infoWallet.rewardPercent!)% \n más de regalo!"
        
        
        userBalance = infoWallet.userCurrentAmount
        lblGreeting.text = "Hola, \( UserDefaults.standard.object(forKey: "first_name")!)"
        textFileAmountBuy.addTarget(self, action: #selector(self.getPicpalMoney(_:)), for: .editingChanged)
        
        
        
        btnWeb.visibility = infoWallet.socialnformation.siteURL == nil ? .gone:.visible
        btnInstagram.visibility = infoWallet.socialnformation.instagramURL == nil ? .gone:.visible
        btnFacebook.visibility = infoWallet.socialnformation.facebookURL == nil ? .gone:.visible
        btnTwiter.visibility = infoWallet.socialnformation.twitterURL == nil ? .gone:.visible
        
        lblTitleSocialMedia.isHidden = (infoWallet.socialnformation.siteURL  == nil && infoWallet.socialnformation.twitterURL == nil && infoWallet.socialnformation.instagramURL == nil && infoWallet.socialnformation.facebookURL == nil)  ? true:false
        
      
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom =  Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goToBuy(_ sender: Any) {
        containerBuy.isHidden = false
        cantainerMap.isHidden = true
        containerInfo.isHidden = true
        
        btnBuy.setImage(UIImage(named: "ic_buy_green.png"), for: .normal)
        btnMap.setImage(UIImage(named: "iconLocation.png"), for: .normal)
        btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: .normal)
    }
    
    @IBAction func goToMaps(_ sender: Any) {
        cantainerMap.isHidden = false
        containerBuy.isHidden = true
        containerInfo.isHidden = true
        
        btnBuy.setImage(UIImage(named: "ic_buy_grey.png"), for: .normal)
        btnMap.setImage(UIImage(named: "ic_gps_single.png"), for: .normal)
        btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: .normal)
        
        LoadMap()
       
        
    }
    @IBAction func goToInfo(_ sender: Any) {
        containerBuy.isHidden = true
        cantainerMap.isHidden = true
        containerInfo.isHidden = false
        btnBuy.setImage(UIImage(named: "ic_buy_grey.png"), for: .normal)
        btnMap.setImage(UIImage(named: "iconLocation.png"), for: .normal)
        btnInfo.setImage(UIImage(named: "ic_info_green.png"), for: .normal)
        
        var direccion = ""
        if infoWallet.address != nil {
            direccion = infoWallet.address.street //+ " " + mSingleReward.address.colony
            
            if infoWallet.address.noExt != nil && infoWallet.address.noExt != ""{
                direccion = direccion + " " + infoWallet.address.noExt
            }
            
            if infoWallet.address.noInt != nil && infoWallet.address.noInt != ""{
                direccion = direccion + " - " + infoWallet.address.noInt
            }
            
            if infoWallet.address.colony != nil && infoWallet.address.colony != ""{
                direccion = direccion + " " + infoWallet.address.colony + ","
            }
            
            if infoWallet.address.city != nil && infoWallet.address.city != ""{
                direccion = direccion + " " + infoWallet.address.city
            }
            
            if infoWallet.address.state != nil && infoWallet.address.state != ""{
                direccion = direccion + ", " + infoWallet.address.state
            }
            
            if infoWallet.address.zip_code != nil && infoWallet.address.zip_code != ""{
                direccion = direccion + ", C.P. " + infoWallet.address.zip_code
            }
            
            if infoWallet.address.referencePlace != ""{
                direccion = direccion + "\n" + infoWallet.address.referencePlace
            }
        }
        
        lblDescription.text = infoWallet.descripcion
        lblLocation.text = direccion
        lblCategory.text = infoWallet.business_area
        
        lblPaymentProcess .text = infoWallet.socialnformation.paymentAllow
        lblDelivery.text = infoWallet.socialnformation.delivery ? "Si":"No"
        
        if infoWallet.operation_schedule != nil {
            if infoWallet.operation_schedule.count > 0 {
                lblSchedule.text  = CustomsFuncs.createHorario(horario: infoWallet.operation_schedule)
            }
        }
       
    }
    
    
    @IBAction func goWeb(_ sender: Any) {
        self.openUrl(url: infoWallet.socialnformation.siteURL  ?? "")
    }
    
    @IBAction func goInstagran(_ sender: Any) {
        self.openUrl(url: infoWallet.socialnformation.instagramURL ?? "")
    }
    
    @IBAction func goFacebook(_ sender: Any) {
        self.openUrl(url: infoWallet.socialnformation.facebookURL ?? "")
    }
    
    @IBAction func goTwitter(_ sender: Any) {
        self.openUrl(url: infoWallet.socialnformation.twitterURL ?? "")
    }
    
    
    @IBAction func btnSubMenu(_ sender: Any) {
        
        
        walletSW.getMyWalletList(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
        
    }
    
    func getEstablishmentWallet(listWallet: [infoHeaderWallet]){
                
        var misWallets = false
        sinWallet = listWallet.count > 0
        
        // SI TIENE WALLETS VERIFICAMOS QUE NO TENGA SALDO EN NINGUNO
        if listWallet.count != 0{
            var index = 0
            
            for wallet in listWallet{
                
                if wallet.moneyWallet == 0{
                    
                    index += 1
                    
                }
                
            }
            
            if index == listWallet.count {
                sinWallet = false
                misWallets = true
                
            }
        }
        
        
        let newXIB = SubMenuWalletViewController(nibName: "SubMenuWalletViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.sinWallet = sinWallet
        newXIB.misWallets = misWallets
        newXIB.delegate = self
        present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    func didFailGetEstablishmentWallet(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func openAlertSinWallet() {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "¡Compra saldo y obtén 10% más! "
        newXIB.mTitle = "No tienes ningún Wallet con saldo"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    func gotToPay(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        newVC.goToPay = true
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func goToQuestions() {
        walletSW.getListFaqs()
    }
    
    func goToBuy() {
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController

        newVC.from = "SubMenu"

        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func gotToMyWallets(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    func didGetListFaqs(lisFaqs: [ItemFaqs]) {
        
        
        let newXIB = TermsAndConditionsView(nibName: "TermsAndConditionsView", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.textTermsAndConditions = ""
        newXIB.hiddenTerms  = true
        newXIB.hiddenList  = false
        newXIB.texTitel = "PREGUNTAS FRECUENTES"
        newXIB.listfaqsArray = lisFaqs
        present(newXIB, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    
    @IBAction func btnContinuePaymentMethod(_ sender: Any) {
        
        
        if amountPurchased > 49  {
            
            if  amountPurchased <= infoWallet.amountAvailable!{
                
                let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "SelectCard") as! SelectCardViewController
                
                newVC.purchasedAmount = self.amountPurchased
                newVC.idEstablishment = infoWallet.id
                newVC.infoWallet = self.infoWallet
                newVC.backGroundColor = self.backGroundColor
                newVC.total = amountPurchased
                newVC.from = from
                
                self.navigationController?.pushViewController(newVC, animated: true)
                
            }else{
                
                showAlert(title: "Saldo disponible a la venta insuficiente",subtitle:"El monto de compra no puede exceder el saldo disponible a la venta por el comercio" )
                
                
            }
            
            
            
            
        }else{
            
            showAlert(title: "Error",subtitle:"El monto mínimo de compra es de $50.00" )
            
        }
        
        
 
    }
    
    
    @IBAction func btnOpenMap(_ sender: Any) {
        
    
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)){
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(Float(infoWallet.latitude!)),\(Float(infoWallet.longitude!))&directionsmode=driving")! as URL)
            } else {
                openUrl(url:"https://www.google.com/maps/search/?api=1&query=\(infoWallet.latitude!),\(infoWallet.longitude!)")
            }
            
            
        } else {
            
            print("Can't use com.google.maps://");
            
            let coordinate = CLLocationCoordinate2DMake(infoWallet.latitude!, infoWallet.longitude!)
                let region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.01, 0.02))
                let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                let options = [
                    MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
                    MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)]
                mapItem.name = infoWallet.name
                mapItem.openInMaps(launchOptions: options)
            
        }
        
    }
    
    func openUrl(url: String){
        if let reviewURL = URL(string: url), UIApplication.shared.canOpenURL(reviewURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(reviewURL)
            }
        }
    }

}



//MARK: - Extension Buy

extension SingleWalletViewController{
    
    @objc func getPicpalMoney(_ textField: UITextField){
        
        
       
        if textFileAmountBuy.text != ""{
            
//            var textAmount = textFileAmountBuy.text!.replacingOccurrences(of: "$", with: "")
//            textAmount = textFileAmountBuy.text!.replacingOccurrences(of: ".00", with: "")
//            textFileAmountBuy.text = CustomsFuncs.getFomatMoney(currentMoney: Double(textAmount)!)
            
            if let amountString = textField.text?.currencyInputFormatting() {
                textFileAmountBuy.text = amountString
                }
        getMoneyBonus()
            
        }else{
            
            lblMessageWallet.isHidden = true
            lblTotalPickpalCash.isHidden = true
            lblAmountPickpal.text = ""
            newBalance = 0
            
            continueButton.isEnabled = false
            
        }
        
        
        
    }
    
    
    func getMoneyBonus(){
        
        lblTotalPickpalCash.isHidden = false
        lblMessageWallet.isHidden = false
        continueButton.isEnabled = true
        var textToMoney = textFileAmountBuy.text!.replacingOccurrences(of: "$", with: "")
        textToMoney = textToMoney.replacingOccurrences(of: ",", with: "")
        if textToMoney != ""{
        amountPurchased = Double (textToMoney)!
        let porcent = infoWallet.rewardPercent / 100
        let moneyPickPal = amountPurchased * porcent
        let total =  amountPurchased + moneyPickPal
        lblGreeting.text = "Hola, \( UserDefaults.standard.object(forKey: "first_name")!)"
        lblAmountPickpal.text = "\(CustomsFuncs.getFomatMoney(currentMoney:moneyPickPal))"
        newBalance = total + userBalance
        lblTotalPickpalCash.text = "Total: \(CustomsFuncs.getFomatMoney(currentMoney:total))"
        lblMessageWallet.text = "El saldo total de tu Wallet después de esta compra será de \(CustomsFuncs.getFomatMoney(currentMoney:newBalance))"
        }
        
    }
    
    
    
}




//MARK: - Extension Mapa

extension SingleWalletViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
  
    
    
    
    func LoadMap(){
        
        
        mapEstablishment.isMyLocationEnabled = true
        mapEstablishment.settings.myLocationButton = true
        location = CLLocationCoordinate2D(latitude: infoWallet.latitude, longitude:infoWallet.longitude)
        
        mapEstablishment.camera = GMSCameraPosition(target: location,zoom: 16,bearing: 0, viewingAngle: 0)
        
    }
    
    
  func locationManager(_ manager: CLLocationManager,didChangeAuthorization status: CLAuthorizationStatus) {
   
    guard status == .authorizedWhenInUse else {
      return
    }
   
        locationManager.requestLocation()
        mapEstablishment.isMyLocationEnabled = true
        mapEstablishment.settings.myLocationButton = true


  }

  
  func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]) {
    


  }

  // 8
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        print(error)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
       
        reverseGeocode(coordinate: position.target)
        
       
    }
    
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
   
      let geocoder = GMSGeocoder()


      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
//            self.isEditing = true
        }
      }
        
    }
    
    
    
    func showAlert(title: String,subtitle:String ){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = title
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
 
}
