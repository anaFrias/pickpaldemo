//
//  TerminosYCondicionesViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 20/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController, walletDelegate {

    
    var acepTermsAndConditions = false
    @IBOutlet weak var btnacepTermsAndConditions: UIButton!
    @IBOutlet weak var btnGoToHome: UIButton!
    @IBOutlet weak var textTTermsAndConditions: UITextView!
    
    var statusProfile = StatusProfile()
    var walletWS = WalletWS()
    var avatar = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        walletWS.delegate = self
        
        textTTermsAndConditions.text = statusProfile.walletTermnsConditions
      
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnClose(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func goToFrequentQuestions(_ sender: Any) {
     
        
       
        
    }
    @IBAction func goToHomeWallet(_ sender: Any) {
        
        
        walletWS.acceptedTermnsConditions()
        
        
    }
    
    
    
    func didAcceptedTermnsConditions() {
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        newVC.avatar = self.avatar
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailacceptedTermnsConditions(error: String, subtitle: String) {
        
        
        
        
    }
    
    @IBAction func ntbAcepTermsAndConditions(_ sender: Any) {
        
        
        if acepTermsAndConditions{
            
            btnacepTermsAndConditions.setImage(UIImage(named: "ic_acep_terms.png"), for: .normal)
            btnGoToHome.isEnabled = true
            
            acepTermsAndConditions = false
            
        }else{
            btnacepTermsAndConditions.setImage(UIImage(named: "ic_decline_terms.png"), for: .normal)
            btnGoToHome.isEnabled = false
            
            acepTermsAndConditions = true
        }
        
    }
    
}


