//
//  SingleEstablishmentWalletViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 19/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
class HistoryWalletViewController: UIViewController , AlertAdsDelegate {
    
    @IBOutlet weak var logoEstablishment: UIImageView!
    @IBOutlet weak var tableListHistory: UITableView!
    @IBOutlet weak var lblGreeting: UILabel!
    @IBOutlet weak var lblAmountBalance: UILabel!
    @IBOutlet weak var lblREF: UILabel!
    @IBOutlet weak var containerHeader: UIView!
    @IBOutlet weak var lbLocationEstablishment: UILabel!
    @IBOutlet weak var cardHeader: UIView!
    @IBOutlet weak var lblQuantityAvailable: UILabel!
    @IBOutlet weak var lblNameEstablishment: UILabel!
    @IBOutlet weak var lblHaederNameEstablishment: UILabel!
    
    @IBOutlet weak var heightContainerGeneral: NSLayoutConstraint!
    
    @IBOutlet weak var btnBuy: UIButton!
    
    @IBOutlet weak var btnPay: UIButton!
    var listMovents = [[ItemHistoryWallet]]()
    var moments = [ItemHistoryWallet]()
    var dataHstory = [[ItemHistoryWallet]]()
    var listHeaders = [String]()
    var infoWalletSingle = ItemSingelWallet()
    
    var uiColorBackground = UIColor()
    var logoStablishment = String()
    var walletSW = WalletWS()
    var item = ItemHistoryWallet()
    var infoWallet = infoHeaderWallet()
    var headerWallet = Wallet()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        walletSW.delegate = self
        cardHeader.clipsToBounds = false
        cardHeader.layer.masksToBounds = false
        cardHeader.layer.cornerRadius = 5
        
       
        containerHeader.clipsToBounds = false
        containerHeader.layer.masksToBounds = false
        containerHeader.layer.cornerRadius = 5
        
        lblQuantityAvailable.text = "\(CustomsFuncs.getFomatMoney(currentMoney: headerWallet.available_money_to_buy!))"
        lblREF.text = "No. de Wallet Pickpal: \(headerWallet.reference!)"
        lblNameEstablishment.text = "SALDO DISPONIBLE A LA VENTA"
        tableListHistory.delegate = self
        tableListHistory.dataSource = self
        
        walletSW.getMyTransactions(idWallet: infoWallet.id, logo: infoWallet.img)
        
        
        Nuke.loadImage(with: URL(string: infoWallet.img)!, into: logoEstablishment)
        lbLocationEstablishment.text = "\(infoWallet.place!),\(infoWallet.state!)"
        lblHaederNameEstablishment.text = "\(infoWallet.name!)"
        lblAmountBalance.text = "\(CustomsFuncs.getFomatMoney(currentMoney:infoWallet.moneyWallet!))"
        containerHeader.backgroundColor = infoWallet.backGroundColor
        lblGreeting.text = "Hola, \( UserDefaults.standard.object(forKey: "first_name")!)"
        
        tableListHistory.register(UINib(nibName: "ItemHistorySingleTableViewCell", bundle: nil), forCellReuseIdentifier: "itemHistoryCell")
        
        
        if infoWallet.moneyWallet == 0 {
            
            btnPay.setBackgroundImage(UIImage(named: "background_enable_btn.png"), for: .normal)
           
            
        }
        
        if headerWallet.available_money_to_buy == 0{
            
            btnBuy.setBackgroundImage(UIImage(named: "background_enable_btn.png"), for: .normal)
            btnBuy.isEnabled = false
            
        }
        
        let footerView = UIView()
        footerView.frame.size.height = 2
        tableListHistory.tableFooterView = footerView
        
        //INICIALIZAMOS EL TAMAÑO DEL CONTENEDOR GENERA Y DESACTIVAMOS EL SCROLL DEL TABLET VIEW
        heightContainerGeneral.constant = 400
        tableListHistory.isScrollEnabled = false;
        
    }
    
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom =  Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func btnToPay(_ sender: Any) {
        
        if infoWallet.moneyWallet == 0 {
            
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "¡Compra saldo y obtén 10% más! "
            newXIB.mTitle = "No tienes ningún Wallet con saldo"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mToHome = false
            newXIB.mId = 0
            newXIB.valueCall = 2
            newXIB.isFromAdd = false
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
           
            
        }else{
            
            
            let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "PayWhitQR") as! PayWhitQRViewController
            newVC.userCurrentAmount = infoWallet.moneyWallet!
            newVC.infoWallet = infoWallet
            newVC.infoWalletSingle =  infoWalletSingle
            newVC.backGroundColor = self.infoWallet.backGroundColor
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
        
    }
    
    @IBAction func btnGoToBuy(_ sender: Any) {
        
        
        walletSW.getInfoSingleEstablishmentsWalelt(idEstablishment: infoWallet.idEstablisment)
        
        
    }
    

    @IBAction func btnSeePayments(_ sender: Any) {
        
        
        
       
        tableListHistory.reloadData()
    }
    
    
    @IBAction func btnSeePurchases(_ sender: Any) {
        
        
        tableListHistory.reloadData()
        
        
        
    }
    
    
}


extension HistoryWalletViewController: UITableViewDelegate, UITableViewDataSource, EstablishmentWalletDelagate, walletDelegate{
    
    func didMyTransactionsWallet(listTransactions: [ItemHistoryWallet]) {
        
        
        //ADAPTAMOS EL TAMAÑO DEL SCROLL DEPENDIENDO DE LA CANTIDAD DE MOVIMIENTOS GENERALES
        
        heightContainerGeneral.constant += CGFloat(listTransactions.count * 97)
        
        //SE OBTIENE LA LISTA
        moments = listTransactions
        
        //OBTNEMOS EL NUMEOR Y NUMERO DE LAS SECCIONES
        var index = 0
        for _ in listTransactions{

            if listHeaders.contains(listTransactions[index].createdAt){
                
               
            }else{
                listHeaders.append(listTransactions[index].createdAt)
                
                
            }
            index += 1
        }
        
        //OBETENMOS LOS MOVIMIENTOS CORRESPONDIENTES A CADA SECCION
        var indexHeader = 0
        var indexTransactions = 0
        var section = [ItemHistoryWallet]()
        
        for _ in listHeaders{
            print("Seccion: ", indexHeader )
            for _ in listTransactions{
                
                if listHeaders[indexHeader] == listTransactions[indexTransactions].createdAt{
                    print("Valor: ", listTransactions[indexTransactions])
                    section.append(listTransactions[indexTransactions])
                    
                }
                
                indexTransactions += 1
            }
            
            self.dataHstory.append(section)
            section.removeAll()
            indexTransactions = 0
            indexHeader += 1
            
        }
        
        
        tableListHistory.reloadData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return moments.count
    }
    

    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemHistoryCell", for: indexPath) as! ItemHistorySingleTableViewCell
        

        let data = self.dataHstory[indexPath.section][indexPath.row]
//        let data = self.moments[indexPath.row]
        cell.selectionStyle = .none
        cell.loadInfo(item: data)
        return cell
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataHstory.count
        
    }
    
    
    override func viewDidLayoutSubviews() {
            
         super.viewDidLayoutSubviews()
        tableListHistory.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -(tableListHistory.bounds.height/2), right: 0)
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if section < dataHstory.count {
            
            print("Saize Section: ",dataHstory[section].count)
            
            return  dataHstory[section].count
            
        }else{
            
            return 0
        }
           
        
        
    }
    

    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var attributedTitleString: NSAttributedString!
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.00),
            .font: UIFont(name: "Aller-Bold", size: 12.0)!
        ]
        let completeText = NSMutableAttributedString(string: "")
        
        if section < dataHstory.count {
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd/mm/yyyy"

            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd/mm/yyyy"
        
            if let date = dateFormatterGet.date(from: self.listHeaders[section]) {

                var dateStrint = dateFormatterPrint.string(from: date)
//                dateStrint = dateStrint.replacingOccurrences(of: " ", with: " de ")
                attributedTitleString = NSAttributedString(string: dateStrint , attributes: attributesTitle)
                completeText.append(attributedTitleString)

            } else {
                attributedTitleString = NSAttributedString(string: self.listHeaders[section] , attributes: attributesTitle)
                completeText.append(attributedTitleString)
               print("There was an error decoding the string")
            }

                
        }else{
            attributedTitleString = NSAttributedString(string: "", attributes: attributesTitle)
            completeText.append(attributedTitleString)
        }
       
       
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 18))
        let label = UILabel(frame: CGRect(x: 10, y: 5, width: tableView.frame.size.width, height: 18))
        label.font = UIFont.systemFont(ofSize: 14)
        label.attributedText = completeText
        view.addSubview(label)
        view.backgroundColor = UIColor.white

        return view
    }
    
    
    func goToSingle(itemSelect: infoHeaderWallet) {
        
        
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    
    
    func didGetInfoWallet(infoWallet: ItemSingelWallet){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleWallet") as! SingleWalletViewController

        newVC.infoWallet = infoWallet
        newVC.backGroundColor = self.infoWallet.backGroundColor
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    func didFailGetInfoWallet(error: String, subtitle: String) {
        
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    func didFailGetTermnsConditions(error: String, subtitle: String) {
        
        
    }
}


