//
//  ItemHistorySingleTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 26/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class ItemHistorySingleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var lblNamEstablishment: UILabel!
    @IBOutlet weak var lblReferens: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblBonusMoney: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
    
    func loadInfo(item:ItemHistoryWallet){
        
        Nuke.loadImage(with: URL(string: item.logo)!, into: logo)
        lblNamEstablishment.text = item.concept
        lblReferens.text = "Referencia: \(item.reference!)"
        lblAmount.text = "\(CustomsFuncs.getFomatMoney(currentMoney:Double(item.amount!)!))"
        lblDate.text = DatabaseFunctions.shared.getHourPay(pagado: "\(item.timeAt!) :00")
        
        
        switch item.conceptInt {
        case 1:
            
     
            lblBonusMoney .text = "PickPal te regalo: +$\(item.reward!)"
            lblAmount.attributedText = CustomsFuncs.getAttributedString(title1: "Total:\(CustomsFuncs.getFomatMoney(currentMoney:Double(item.total) ?? 0))", title2: "", ziseFont: 12.0, colorText: 0)
            
        default:
            lblBonusMoney .text = ""
        }
          
    }
    
    
}
