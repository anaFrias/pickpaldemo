//
//  ItemFaqsTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 25/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ItemFaqsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var llblAnswer: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadInfo(info: ItemFaqs){
        
        lblTitle.text = info.title
        llblAnswer.text = info.answer
        
    }
    
}
