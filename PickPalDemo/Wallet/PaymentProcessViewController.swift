//
//  PaymentProcessViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PaymentProcessViewController: UIViewController, walletDelegate, AlertAdsDelegate {

    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var payment_source_id = String()
    var amount = Double()
    var idEstablishment = Int()
    var walletWS = WalletWS()
    var alert = UIAlertController()
    var seconds = 5
    var timer = Timer()
    var infoWallet = ItemSingelWallet()
    var backGroundColor = UIColor()
    var from = ""
    
    @IBOutlet weak var lblTimeCancel: UILabel!
    @IBOutlet weak var imgProgressBar: UIImageView!
    @IBOutlet weak var viewBarCancel: UIView!
    @IBOutlet weak var lblTotalPay: UILabel!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        walletWS.delegate = self
        
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateProgressBar), userInfo: nil, repeats: true)
        
        imgProgressBar.loadGif(name: "GifProcess")
        
        
        lblTimeCancel.text  = timeString(time: TimeInterval(seconds))
        lblTotalPay.text = "\(CustomsFuncs.getFomatMoney(currentMoney:amount))"
        
//        walletWS.processOrderWallet(establishment_id: idEstablishment,client_id: client_id, amount: amount, payment_method: payment_source_id )
        
    }

    func didProcessOrder(reward: Double, total:Double, amount: Double, expirationDate: String){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "TicketViewController") as! PurchaseTicketViewController
        
            newVC.reward = reward
            newVC.total = total
            newVC.amount = amount
            newVC.expirationDate = expirationDate
            newVC.infoWallet = self.infoWallet
            newVC.backGroundColor = self.backGroundColor
            newVC.from = from
        
        
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    func didFailProcessOrder(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.whereFrom  = Constants.PICKPAL_WALLET
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func btnCancelOrder(_ sender: Any) {
        self.btnCancel.isEnabled = false
        self.timer.invalidate()
        alert = UIAlertController(title: "PickPal", message: "¿Seguro que deseas cancelar tu pedido?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { action in
            switch action.style{
            case .default:
                if self.seconds != 0 {
                    
                    let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
                    
                    
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            switch action.style{
            case .default:
                
                print("default")
            case .cancel:
                print("cancel")
                if self.seconds == 0 {
                    let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController

                    //newVC.singleInfo = info
                    
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                 
                }
            case .destructive:
                print("destructive")
                
                
            }}))
       
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func timeString(time: TimeInterval) -> String {
        let seconds = Int(time)
        let miliseconds = Int(time)  / 3600
        
        return String(format: "%02i:%02is",  miliseconds, seconds)
    }
    
    
    
    @objc func updateProgressBar() {
        print(seconds, "Seconds")
        seconds -= 1
        
        if seconds == 0 {
            
            timer.invalidate()
            
            lblTimeCancel.text = "00:00s"
            
            walletWS.processOrderWallet(establishment_id: idEstablishment,client_id: client_id, amount: amount, payment_method: payment_source_id )
            
            
        }else{
            
            lblTimeCancel.text = timeString(time: TimeInterval(seconds))
            
        }
 
    }
    
    
}
