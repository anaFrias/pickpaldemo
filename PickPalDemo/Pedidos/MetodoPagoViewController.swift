//
//  MetodoPagoViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase


class MetodoPagoViewController: UIViewController, paymentDelegate, UNUserNotificationCenterDelegate, ordersDelegate, reorderDelegate, sentToDelegate, deleteCarritoDelegate, walletDelegate, AlertAdsDelegate {
    
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var establishment_id = Int()
    var total = Double()
    var subtotal = Double()
    var notes = String()
    var promotionalCode = String()
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var address_id = 0
    var payment_source_id = ""
    var paymentWS = PaymentWS()
    var ordersWS = OrdersWS()
    var defaults = UserDefaults.standard
    let center = UNUserNotificationCenter.current()
    var pickpalService = Double()
    var discount = Double()
    var mPoints = Double()
    var mMoneyPoints = Double()
    
    var lugar = String()
    var municipio = String()
    var categoria = String()
    var handle:DatabaseHandle?
    var handle2:DatabaseHandle?
    var referenceItems: DatabaseReference!
    var referenceExtras: DatabaseReference!
    var order_elements = [Dictionary<String,Any>]()
    var hourScheduled = String()
    var isScheduled = Bool()
    var paymentType = Int()
    var inBar = Bool()
    var percent = Double()
    var table = Int()
    var addressDelivery = Address()
    var shipping_cost = Double()
    var type_order = String()
    var timeDelivery = Int()
    var allow_process_nopayment = Bool()
    var pickpalCashMoney = Double()
    
    var hourFancy = String()
    var dateScheduled = String()
   
    var auxTotal = Double()
    var minimumBilling = Double()
    
    @IBOutlet weak var efectivoLbl: UILabel!
    @IBOutlet weak var imageCash: UIImageView!
    @IBOutlet weak var rectangleCash: UIImageView!
    
    @IBOutlet var clicCashGesture: UITapGestureRecognizer!
    
    @IBOutlet weak var btncontinuarView: UIButton!
    @IBOutlet weak var rectanguloEfectivo: UIImageView!
    @IBOutlet weak var iconoEfectivo: UIImageView!
    
    @IBOutlet weak var constraintCash: NSLayoutConstraint!
    @IBOutlet weak var etiquetaEfectivo: UILabel!
    @IBOutlet weak var viewTarjeta: UIView!
    @IBOutlet weak var viewEfectivo: UIView!
    @IBOutlet weak var viewPickpalCash: UIView!
    
    @IBOutlet weak var btnViewContinuar: UIButton!
    
    @IBOutlet weak var pagaTuPedido: UILabel!
    
    @IBOutlet weak var borderTarjeta: UIImageView!
    @IBOutlet weak var iconTarjeta: UIImageView!
    @IBOutlet weak var tituloTarjeta: UILabel!
    @IBOutlet weak var disclaimerText: UILabel!
    
    @IBOutlet weak var viewBordeTarjetaPickpalCash: UIImageView!
    @IBOutlet weak var iconPickpalCash: UIImageView!
    @IBOutlet weak var lblPickaplCash: UILabel!
    
    @IBOutlet weak var lblSinSaldoDisponible: UILabel!
    
    
    // Variables Wallet
    
    @IBOutlet weak var heigthContainerPickpalCash: NSLayoutConstraint!
    @IBOutlet weak var containerGeneralPickpalCash: UIView!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var containerPickpalCash: UIView!
    @IBOutlet weak var containerBaanceDepleted: UIView!
    
    @IBOutlet weak var viewPayPickpalCash: UIView!
    @IBOutlet weak var viewBuyBalance: UIView!
    @IBOutlet weak var lblMessageBalanceInsufficient: UILabel!
    @IBOutlet weak var lblBalanceUser: UILabel!
    @IBOutlet weak var textBalanceuser: UILabel!
    
    @IBOutlet weak var lblMensajePagosComplemtarios: UILabel!
    @IBOutlet weak var heigthConteinerPayCash: NSLayoutConstraint!
    
    @IBOutlet weak var lblTextBalanceInsufficient: UILabel!
    @IBOutlet weak var lblMessageBalance: UILabel!
    var balanceUser = Double()
    var myCash = MyCash()
    var walletSW = WalletWS()
    var amountPayPickpalCash = Double()
    var pickpal_cash_money = 0.0
    var selectCash = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Mesa metodoPago ", table)
        paymentWS.delegate = self
        ordersWS.delegate = self
        walletSW.delegate = self
        defaults.removeObject(forKey: "screen")
        defaults.removeObject(forKey: "storyboard")
        inactiveProductsBeforeProcessing(items: detail)
        inactiveExtrasBeforeProcessing(items: order_elements)
        inactiveModifiersBeforeProcessing(items: order_elements)
        clicCashGesture.isEnabled = true
       
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
       
        total = auxTotal
       
        
        lblTotal.text = "\(CustomsFuncs.getFomatMoney(currentMoney:auxTotal))"
       
        /*
         all = ChoiceItem(0, 'Todos')
         cash = ChoiceItem(1, 'Efectivo')
         card = ChoiceItem(2, 'TDC/TDD')
         */
        
        containerGeneralPickpalCash.isHidden = true
        heigthContainerPickpalCash.constant = 0
        btnViewContinuar.visibility = .gone
        
        switch paymentType {
        case 1: // Solo efectivo
            
            // Deshabilitar pago tarjeta
            clicCashGesture.isEnabled = false
            viewTarjeta.isUserInteractionEnabled = false
            viewEfectivo.isUserInteractionEnabled = true
            disclaimerText.alpha = 0
            borderTarjeta.image = borderTarjeta.image?.withRenderingMode(.alwaysTemplate)
            borderTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            iconTarjeta.image = iconTarjeta.image?.withRenderingMode(.alwaysTemplate)
            iconTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            tituloTarjeta.textColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            
            
            // Deshabilitar pago pickpal cash
            viewPickpalCash.isUserInteractionEnabled = false
            viewPickpalCash.isUserInteractionEnabled = true
            viewBordeTarjetaPickpalCash.image = borderTarjeta.image?.withRenderingMode(.alwaysTemplate)
            viewPickpalCash.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            iconPickpalCash.image = iconTarjeta.image?.withRenderingMode(.alwaysTemplate)
            viewPickpalCash.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            lblPickaplCash.textColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
        
            
            
        case 2: // Solo tarjeta
            
            // Deshabilitar pago pickpal cash
            viewEfectivo.isUserInteractionEnabled = false
            viewEfectivo.isUserInteractionEnabled = true
            rectanguloEfectivo.image = borderTarjeta.image?.withRenderingMode(.alwaysTemplate)
            viewEfectivo.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            iconoEfectivo.image = iconoEfectivo.image?.withRenderingMode(.alwaysTemplate)
            viewEfectivo.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            etiquetaEfectivo.textColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
            
            
            if subtotal < 3 { // Restriccion de compra el montominivo solicitado por conekta es de 3 pesos

                borderTarjeta.image = borderTarjeta.image?.withRenderingMode(.alwaysTemplate)
                borderTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                iconTarjeta.image = iconTarjeta.image?.withRenderingMode(.alwaysTemplate)
                iconTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                tituloTarjeta.textColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                disclaimerText.alpha = 1
                viewEfectivo.alpha = 0
                viewTarjeta.isUserInteractionEnabled = false
                pagaTuPedido.alpha = 0

            }else{
            
                disclaimerText.alpha = 0
                viewTarjeta.isUserInteractionEnabled = true
                viewEfectivo.alpha = 0
                pagaTuPedido.alpha = 0
                
            }

            
            
        default: // Todos los metodos de pago
            
            if subtotal < 3 {

                borderTarjeta.image = borderTarjeta.image?.withRenderingMode(.alwaysTemplate)
                borderTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                iconTarjeta.image = iconTarjeta.image?.withRenderingMode(.alwaysTemplate)
                iconTarjeta.tintColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                tituloTarjeta.textColor = UIColor.init(red: 214/255, green: 214/255, blue: 214/255, alpha: 1)
                disclaimerText.alpha = 1
                viewTarjeta.isUserInteractionEnabled = false

            }else{

                disclaimerText.alpha = 0
                pagaTuPedido.alpha = 0
                viewEfectivo.alpha = 1

            }
        
        }
    }
    
    
    
    func loadPickpalCash(){
        
        lblBalanceUser.text = "\(CustomsFuncs.getFomatMoney(currentMoney: myCash.userCurrentAmount))"
        
        lblSinSaldoDisponible.visibility = .gone
        
        //Escondemos el boton de continuar si el saldo no essuficiente
        if insufficientBalance(){
            
            btncontinuarView.visibility = .gone
            
        }else{
            btncontinuarView.visibility = .visible
        }
        
        if myCash.availablePickpalCash{


            

     //Verificamos que el usuario tenga saldo y colocamos cash como la opcion por default
            if balanceUser > 0{
                selectCash = true
                containerPickpalCash.isHidden = false
                containerGeneralPickpalCash.isHidden = false
                heigthContainerPickpalCash.constant = 190
                heigthConteinerPayCash.constant = 113
                lblBalanceUser.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                lblPickaplCash.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                iconPickpalCash.image = iconPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                iconPickpalCash.tintColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                viewBordeTarjetaPickpalCash.image = viewBordeTarjetaPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                viewBordeTarjetaPickpalCash.tintColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                textBalanceuser.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
//                lblBalanceUser.text = "\(CustomsFuncs.getFomatMoney(currentMoney:balanceUser))"
            
                
            }else{
                
                //En caso de no tener saldo escondemos el boton par apagar con pickpal
                selectCash = false
                containerPickpalCash.isHidden = true
                heigthConteinerPayCash.constant = 0
                heigthContainerPickpalCash.constant = 110
                
                lblSinSaldoDisponible.visibility = .visible
                lblSinSaldoDisponible.text = "No tienes saldo disponible en PickPal Cash"
                containerGeneralPickpalCash.isHidden = false
               if !myCash.availableToBuy{
               
                heigthContainerPickpalCash.constant = 10
               }
            }
            
            
            // Si el comercio tiene saldo a la venta se muestra el boton de compra
            if !myCash.availableToBuy{
                
                lblMensajePagosComplemtarios.visibility = .gone
                containerBaanceDepleted.isHidden = true
                heigthContainerPickpalCash.constant = 110
                lblSinSaldoDisponible.visibility = .visible
                lblSinSaldoDisponible.text = "Actualmente el comercio no tiene saldo disponible a la venta"
                
                if balanceUser == 0{
                
                    heigthContainerPickpalCash.constant = 10
                    
                }
                
            }else{
                
                containerBaanceDepleted.isHidden = false
               
                
            }
            
            
        }else{
            
            containerGeneralPickpalCash.isHidden = true
            heigthContainerPickpalCash.constant = 0
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        walletSW.getMyCash(idEstablishment: establishment_id)
        
        
        total = auxTotal
          
        if total > 3{
            disclaimerText.visibility = .gone
        }else{
            disclaimerText.visibility = .visible
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print (  addressDelivery.pk, shipping_cost ,type_order)   
    }
    
    @IBAction func popVew(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        var arrayViews = [Bool]()
        var homeViewController = UIViewController()
        for viewController in (self.navigationController?.viewControllers)! {
            if viewController.isKind(of: CarritoViewController.self){
                arrayViews.append(true)
                homeViewController = viewController
            }else{
                arrayViews.append(false)
            }
        }
        if arrayViews.contains(true) {
            self.navigationController?.popToViewController(homeViewController, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
            newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.esablishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    func didSuccessRegisterOrder(code: String, folio: String, order_id: Int, prep_time: Int, hour_scheduled: String, is_scheduled: Bool) {
        LoadingOverlay.shared.hideOverlayView()
//        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            //            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                print("13 minutes has passed")
                print(order_id)
                if DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" && DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB" && DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay" {
                    self.ordersWS.changeOrderStatus(order_id:order_id, status: 7)
                }
            })
            
        }
        
        if  DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id) && (DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB") {
        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content1 = UNMutableNotificationContent()
        let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content1.title = "¡Ve a pagar tu Pedido a \(establecimiento)!"
        content1.body = "Paga tu pedido mostrando tu códido QR, tienes 5 minutos ¡Corre!. Haz click aquí para ver los detalles de tu pedido..."
        content1.sound = UNNotificationSound.default()
        let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
        center.delegate = self
        center.add(request1, withCompletionHandler: nil)
        
        let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
//        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let content2 = UNMutableNotificationContent()
        content2.title = "PickPal"
        content2.body = "Lo sentimos tu pedido de \(establecimiento) ha caducado."
        content2.sound = UNNotificationSound.default()
        content2.userInfo = ["establishment_name": establishment_name1, "order_id": order_id, "notification_on": true]
        let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
        content2.categoryIdentifier = "caducado"
        
        UNUserNotificationCenter.current().add(request1) { (error) in
            if let error = error {
                print("Se ha producido un error: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request2) { (error) in
            if let error = error {
                print("Se ha producido un error: \(error)")
            }
        }
        
        }
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd_MM_yyyy"
        let myStringafd = formatter.string(from: yourDate!)

        let establishment_name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let reference = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
        reference.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                let valueOrder = snapshot.value as! NSDictionary
                let elements = DatabaseFunctions.shared.createOrderElements(products: valueOrder.value(forKey: "products") as! NSArray)
                let addresDescription = "\(self.addressDelivery.street!) # \(self.addressDelivery.numeroExterior!) , \(self.addressDelivery.colony!) , \(self.addressDelivery.state!), C.P. \(self.addressDelivery.zip_code!)"
                DatabaseFunctions.shared.insertFromFirebase(establishment_id: self.establishment_id, establishment_name: establishment_name, in_site: self.paraAqui, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: self.promotionalCode, total: self.total, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: valueOrder.value(forKey: "total") as! Double, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order_id, order_elements: elements, statusItem: "to_pay", qr: valueOrder.value(forKey: "qr") as! String, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: valueOrder.value(forKey: "last4") as! String, statusController: "to_pay", is_scheduled: self.isScheduled, in_bar: self.inBar, percent: self.percent, table_number: self.table, readyFlag: true, points: self.mPoints, money_puntos: self.mMoneyPoints, order_type: self.type_order, address: addresDescription, shipping_cost: self.shipping_cost as! Double, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String(self.timeDelivery),to_refund: false, days_elapsed: 0, allow_process_nopayment: false,est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? "", est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: valueOrder.value(forKey: "is_generic")as? Bool ?? false,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "", sendNotifications: false, notified: valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: self.pickpal_cash_money, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false,total_card: 0,total_cash:0 ,total_to_pay: 0, total_done_payment: 0 ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                
                
                
                
                DatabaseFunctions.shared.deleteItem()
            }
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.showPedido = true
            self.navigationController?.pushViewController(newVC, animated: true)
        })
        
        let statusElement = DatabaseFunctions.shared.checkStatusOrder(orderId: order_id)
        
    }
    func reorder(order_id: Int, is_canceled: Bool) {
        self.ordersWS.retryOrder(order_id: order_id)
    }
    func didSuccessRetryOrder(order_id: Int) {
        LoadingOverlay.shared.hideOverlayView()
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd_MM_yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        let establishment_name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let reference = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
        reference.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                let valueOrder = snapshot.value as! NSDictionary
                let elements = DatabaseFunctions.shared.createOrderElements(products: valueOrder.value(forKey: "products") as! NSArray)
                let addresDescription = "\(self.addressDelivery.street!) # \(self.addressDelivery.numeroExterior!) , \(self.addressDelivery.colony!) , \(self.addressDelivery.state!), C.P. \(self.addressDelivery.zip_code!)"
                DatabaseFunctions.shared.insertFromFirebase(establishment_id: self.establishment_id, establishment_name: establishment_name, in_site: self.paraAqui, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: self.promotionalCode, total: self.total, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: valueOrder.value(forKey: "total") as! Double, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order_id, order_elements: elements, statusItem: "to_pay", qr: valueOrder.value(forKey: "qr") as! String, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: valueOrder.value(forKey: "last4") as! String, statusController: "to_pay", is_scheduled: self.isScheduled, in_bar: self.inBar, percent: self.percent, table_number: self.table, readyFlag: true, points: self.mPoints, money_puntos: self.mMoneyPoints, order_type: self.type_order, address: addresDescription, shipping_cost: self.shipping_cost as! Double, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String(self.timeDelivery),to_refund: false, days_elapsed: 0, allow_process_nopayment: false,est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? "", est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: valueOrder.value(forKey: "is_generic")as? Bool ?? false,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "", sendNotifications: false, notified: valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: self.pickpal_cash_money, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false,total_card: 0,total_cash:0 ,total_to_pay: 0, total_done_payment: 0 ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                DatabaseFunctions.shared.deleteItem()
            }
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.showPedido = true
            self.navigationController?.pushViewController(newVC, animated: true)
        })
        
        print("Tipo Orden BD Metodo ", DatabaseFunctions.shared.getOrderIdType(order_id: order_id))
         if  DatabaseFunctions.shared.getAllowProcessNopaymentOrder(order_id: order_id) && (DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB") {
        //        let statusElement = DatabaseFunctions.shared.checkStatusOrder(orderId: order_id)
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
                print("13 minutes has passed")
                print(order_id)
                if DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" && DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB" && DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay"  {
                    
                    self.ordersWS.changeOrderStatus(order_id:order_id, status: 7)
                }
            })
            
        }
        
        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        //        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content1 = UNMutableNotificationContent()
        let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content1.title = "¡Ve a pagar tu Pedido a \(establecimiento)!"
        content1.body = "Paga tu pedido mostrando tu códido QR, tienes 5 minutos ¡Corre!. Haz click aquí para ver los detalles de tu pedido..."
        content1.sound = UNNotificationSound.default()
        let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
        center.delegate = self
        center.add(request1, withCompletionHandler: nil)
        
        let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
        //        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let content2 = UNMutableNotificationContent()
        content2.title = "PickPal"
        content2.body = "Lo sentimos tu pedido de \(establecimiento) ha caducado."
        content2.sound = UNNotificationSound.default()
        content2.userInfo = ["establishment_name": establishment_name1, "order_id": order_id, "notification_on": true]
        let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
        content2.categoryIdentifier = "caducado"
        center.delegate = self
        center.add(request2, withCompletionHandler: nil)
        
        UNUserNotificationCenter.current().add(request1) { (error) in
            if let error = error {
                print("Se ha producido un error: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request2) { (error) in
            if let error = error {
                print("Se ha producido un error: \(error)")
            }
        }
        }
    }
    
    func didFailRetryOrder(error: String, subtitle: String) {
        print(error)
    }
    func didSuccessChangeOrderStatus() {
        print("didSuccessChangeOrderStatus")
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
    }
    func didFailChangeOrderStatus(error: String, subtitle: String) {
        print(error)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        if let notifictionon = response.notification.request.content.userInfo as? NSDictionary {
            if let notif = notifictionon.value(forKey: "notification_on") as? Bool {
                let reorder = ReOrderViewController()
                reorder.delegate = self
                reorder.modalTransitionStyle = .crossDissolve
                reorder.modalPresentationStyle = .overCurrentContext
                reorder.order_id = (response.notification.request.content.userInfo as? NSDictionary)?.value(forKey: "order_id") as! Int
                self.present(reorder, animated: true, completion: nil)
            }
        }
        
        
    }
    func didFailRegisterOrder(error title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func clickCard(_ sender: Any) {
        
        
        if selectCash{
            
            if  insufficientBalance(){
                
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                newVC.detail = detail
                newVC.paraAqui = paraAqui
                newVC.establishment_id = establishment_id
                newVC.total = total
                newVC.first_time  = true
                newVC.notes = notes
                newVC.discount = discount
                newVC.pickpalService =  pickpalService
                newVC.promotionalCode = promotionalCode
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = hourScheduled
                newVC.isScheduled = isScheduled
                newVC.inBar = self.inBar
                newVC.percent = percent
                newVC.table = table
                newVC.hourScheduledFormatted = hourFancy
                newVC.dateScheduled = dateScheduled
                newVC.mPoints = mPoints
                newVC.mMoneyPoints = mMoneyPoints
                newVC.subtotal = Double(subtotal)
                newVC.paymentType =  paymentType
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  self.shipping_cost
                newVC.type_order = self.type_order
                newVC.timeDelivery = self.timeDelivery
                newVC.pickpal_cash_money =  pickpal_cash_money
                newVC.minimumBilling = self.minimumBilling
                
                self.navigationController?.pushViewController(newVC, animated: true)
                
            }else{
                
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                newVC.detail = detail
                newVC.paraAqui = paraAqui
                newVC.establishment_id = establishment_id
                newVC.total = auxTotal
                newVC.first_time  = true
                newVC.notes = notes
                newVC.discount = discount
                newVC.pickpalService =  pickpalService
                newVC.promotionalCode = promotionalCode
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = hourScheduled
                newVC.isScheduled = isScheduled
                newVC.inBar = self.inBar
                newVC.percent = percent
                newVC.table = table
                newVC.hourScheduledFormatted = hourFancy
                newVC.dateScheduled = dateScheduled
                newVC.mPoints = mPoints
                newVC.mMoneyPoints = mMoneyPoints
                newVC.subtotal = Double(subtotal)
                newVC.paymentType =  paymentType
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  self.shipping_cost
                newVC.type_order = self.type_order
                newVC.timeDelivery = self.timeDelivery
                newVC.pickpal_cash_money = 0
                newVC.minimumBilling = self.minimumBilling
                
                self.navigationController?.pushViewController(newVC, animated: true)
                
                
            }
            
            
        }else{
            
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.establishment_id = establishment_id
            newVC.total = auxTotal
            newVC.first_time  = true
            newVC.notes = notes
            newVC.discount = discount
            newVC.pickpalService =  pickpalService
            newVC.promotionalCode = promotionalCode
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.order_elements = order_elements
            newVC.hourScheduled = hourScheduled
            newVC.isScheduled = isScheduled
            newVC.inBar = self.inBar
            newVC.percent = percent
            newVC.table = table
            newVC.hourScheduledFormatted = hourFancy
            newVC.dateScheduled = dateScheduled
            newVC.mPoints = mPoints
            newVC.mMoneyPoints = mMoneyPoints
            newVC.subtotal = Double(subtotal)
            newVC.paymentType =  paymentType
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  self.shipping_cost
            newVC.type_order = self.type_order
            newVC.timeDelivery = self.timeDelivery
            newVC.pickpal_cash_money = 0
            newVC.minimumBilling = self.minimumBilling
            
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
    }
    
    func inactiveProductsBeforeProcessing(items: [Dictionary<String,Any>]) {
        
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id
        )
        var itemsId = [Int]()
        var productsName = [String]()
        for item in items {
            itemsId.append(item["id"]! as! Int)
            productsName.append(item["item_name"] as! String)
        }
        referenceItems = Database.database().reference().child("inactive_items").child("-\(establishmentId)").child("items")
        handle = referenceItems.observe(.value) { (snapshot) in
            print(snapshot.value)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var productsInactivated = [String]()
                for (index,id) in itemsId.enumerated() {
                    print(index)
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                        idsInactivated.append(id)
                        productsInactivated.append(productsName[index])
                    }
                }
                if idsInactivated.count > 0 {
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = productsInactivated
                    print(idsInactivated, productsInactivated)
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    self.present(newXIB, animated: true, completion: nil)
                }
                
            }
        }
        print("hold on")
    }
    
    func inactiveExtrasBeforeProcessing(items: [Dictionary<String,Any>]) {
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        referenceExtras = Database.database().reference().child("inactive_extras").child("-\(establishmentId)")
        print(referenceExtras)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle2 = referenceExtras.observe(.value) { (snapshot) in
            var extrasId = [Int]()
            var modifExtrasName = [String]()
            for item in items {
                if let complements = item["complements"] as? NSDictionary {
                    if let complement_id = complements.value(forKey: "complement_id") as? NSArray {
                        for compl in complement_id {
                            extrasId.append(compl as! Int)
                        }
                    }
                    if let complement_name = complements.value(forKey: "complement_name") as? NSArray {
                        for name in complement_name {
                            modifExtrasName.append(name as! String)
                        }
                    }
                }
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                        if let modifiers_name = (modi as! NSDictionary).value(forKey: "modifier_name") as? NSArray {
                            for name in modifiers_name {
                                modifExtrasName.append(name as! String)
                            }
                        }
                    }
                }
            }
            
//            extrasId = [-63]
//            modifExtrasName = ["Salsa de anguila"]
            print(extrasId, modifExtrasName)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var namesInactivated = [String]()
                for (index, id) in extrasId.enumerated() {
//                    print(snapshot.value as! NSDictionary)
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                       print( ((snapshot.value as! NSDictionary).value(forKey: "\(id)")) as! NSDictionary)
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        idsInactivated.append(id2)
                        namesInactivated.append(modifExtrasName[index])
                    }
                }
                print(idsInactivated, namesInactivated)
                if idsInactivated.count > 0 {
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = namesInactivated
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    func inactiveModifiersBeforeProcessing(items: [Dictionary<String,Any>]) {
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        referenceExtras = Database.database().reference().child("inactive_modifiers").child("-\(establishmentId)")
        print(referenceExtras)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle2 = referenceExtras.observe(.value) { (snapshot) in
            var extrasId = [Int]()
            var modifExtrasName = [String]()
            for item in items {
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                        if let modifiers_name = (modi as! NSDictionary).value(forKey: "modifier_name") as? NSArray {
                            for name in modifiers_name {
                                modifExtrasName.append(name as! String)
                            }
                        }
                    }
                }
            }
//            extrasId = [63]
//            modifExtrasName = ["Salsa de anguila"]
            print(extrasId, modifExtrasName)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var namesInactivated = [String]()
                var idsItems = [Int]()
                for (index, id) in extrasId.enumerated() {
                    print(snapshot.value as! NSDictionary)
                    if (snapshot.value as! NSDictionary).value(forKey: "-\(id)") != nil {
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "-\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        let id3 = (((snapshot.value as! NSDictionary).value(forKey: "-\(id)")) as! NSDictionary).value(forKey: "item") as! Int
                        idsItems.append(id3)
                        idsInactivated.append(id2)
                        namesInactivated.append(modifExtrasName[index])
                    }
                }
                print(idsInactivated, namesInactivated)
                if idsInactivated.count > 0 {
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = namesInactivated
                    newXIB.idItems = idsItems
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    func deleteCarrito(showView: Bool, stb_id: Int, municipio: String, categoria: String, lugar: String, estbName: String, stayInCart: Bool) {
        if !stayInCart {
            sentSingleEstablishment(idEstablishment: stb_id, estbName: estbName, category: categoria, place: lugar, munic: municipio)
        }else{
            sentCarrito(idEstablishment: stb_id, estbName: estbName, category: categoria, place: lugar, munic: municipio)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if referenceItems != nil {
            referenceItems.removeObserver(withHandle: handle!)
        }
        
        if referenceExtras != nil{
            referenceExtras.removeObserver(withHandle: handle2!)
        }
    }
    @IBAction func clickCash(_ sender: Any) {
       
        
        goProcessOrderViewController(cash: true)
    }
    
  
    
    @IBAction func btnPayPickpalCash(_ sender: Any) {
        
        
        if insufficientBalance(){

            btnViewContinuar.visibility = .gone

        }else{

            btnViewContinuar.visibility = .visible

        }
    
                
            if selectCash{

                // Cambiamos el color a Gris
                lblBalanceUser.textColor = UIColor(red: 0.43, green: 0.48, blue: 0.52, alpha: 1.00)
                lblPickaplCash.textColor = UIColor(red: 0.43, green: 0.48, blue: 0.52, alpha: 1.00)
                iconPickpalCash.image = iconPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                iconPickpalCash.tintColor = UIColor(red: 0.43, green: 0.48, blue: 0.52, alpha: 1.00)
                viewBordeTarjetaPickpalCash.image = viewBordeTarjetaPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                viewBordeTarjetaPickpalCash.tintColor = UIColor(red: 0.43, green: 0.48, blue: 0.52, alpha: 1.00)
                textBalanceuser.textColor = UIColor(red: 0.43, green: 0.48, blue: 0.52, alpha: 1.00)


                // Cambaimos la bandera
                selectCash = false
                btnViewContinuar.visibility = .gone


            }else{

                // Cambaimos el color a verde
                lblBalanceUser.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                lblPickaplCash.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                iconPickpalCash.image = iconPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                iconPickpalCash.tintColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                viewBordeTarjetaPickpalCash.image = viewBordeTarjetaPickpalCash.image?.withRenderingMode(.alwaysTemplate)
                viewBordeTarjetaPickpalCash.tintColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)
                textBalanceuser.textColor = UIColor(red: 0.12, green: 0.85, blue: 0.53, alpha: 1.00)

                selectCash = true

            }

        
           
            
            

        
    }
    
    @IBAction func btnBuyBalance(_ sender: Any) {
        
//        self.walletSW.getMyWallet(idEstablishment: establishment_id)
        
        
        walletSW.getInfoSingleEstablishmentsWalelt(idEstablishment: establishment_id)
        
    }
        
    //COMPRAR SALDO WALLET
        
    func didGetInfoWallet(infoWallet: ItemSingelWallet){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newXIB = storyboard.instantiateViewController(withIdentifier: "SingleWallet") as! SingleWalletViewController
        
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.infoWallet = infoWallet
        newXIB.from = "Carrito"
        newXIB.backGroundColor = UIColor(red: 0.62, green: 0.73, blue: 0.23, alpha: 1.00)
        self.navigationController?.pushViewController(newXIB, animated: true)
        
        
    }
    
    
    
    func didFailGetInfoWallet(error: String, subtitle: String) {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    func sentCarrito(idEstablishment: Int, estbName: String, category: String, place: String, munic: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        newVC.establishment_id = idEstablishment
        newVC.municipio = municipio
        newVC.categoria = category
        newVC.lugar = place
        newVC.esablishmentName = estbName
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func sentSingleEstablishment(idEstablishment: Int, estbName: String, category: String, place: String, munic: String) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
        newVC.id = idEstablishment
        newVC.municipioStr = municipio
        newVC.categoriaStr = categoria
        newVC.lugarString = lugar
        newVC.establString = estbName
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    
    func goProcessOrderViewController(cash: Bool){
    
    
        //SI CASH ESTA SELECCIONADA
        if selectCash{
            
            //COMPROBAMOS SI EL SALDO NO ES SUFICIENTE
            
            if insufficientBalance(){
                //SE HACE EL COBRO DEL TOTAL RESTANTE
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "InvoiceOrder") as! InvoiceOrderViewController
                newVC.detail = detail
                newVC.paraAqui = paraAqui
                newVC.payment_source_id = payment_source_id
                newVC.establishment_id = establishment_id
                newVC.total = total
                newVC.notes = notes
                newVC.promotionalCode = promotionalCode
                newVC.discount = discount
                newVC.pickpalService = pickpalService
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = hourScheduled
                newVC.isScheduled = isScheduled
                newVC.inBar = inBar
                newVC.percent = percent
                newVC.table = table
                newVC.hourFancy = hourFancy
                newVC.dateScheduled = dateScheduled
                newVC.mPoints = self.mPoints
                newVC.mMoneyPoints = self.mMoneyPoints
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  Float(self.shipping_cost)
                newVC.type_order = self.type_order
                newVC.timeDelivery = self.timeDelivery
                newVC.isInsufficientBalance = insufficientBalance()
                newVC.pickpal_cash_money = pickpal_cash_money
     
                self.navigationController?.pushViewController(newVC, animated: true)
                    
                
            }else{

                // EL SADO ES SUFICIENTE, SE HACE EL COBRO POR EL TOTAL COMPLETO (SIN CASH)
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
                newVC.address_id = address_id
                newVC.detail = detail
                newVC.paraAqui = paraAqui
                newVC.establishment_id = establishment_id
                newVC.payment_source_id = payment_source_id
                newVC.total = auxTotal
                newVC.notes = notes
                newVC.promotionalCode = promotionalCode
                newVC.isCash = cash
                newVC.discount = discount
                newVC.pickpalService = pickpalService
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = hourScheduled
                newVC.isScheduled = isScheduled
                newVC.percent = percent
                newVC.hourFancy = hourFancy
                newVC.dateScheduled = dateScheduled
                newVC.mPoints = self.mPoints
                newVC.mMoneyPoints = self.mMoneyPoints
                print("Mesa boton", self.table)
                newVC.table = self.table
                newVC.subtotal = Double(subtotal)
                newVC.paymentType =  paymentType
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  Float(self.shipping_cost)
                newVC.type_order = self.type_order
                newVC.timeDelivery = self.timeDelivery
                newVC.pickpal_cash_money = 0
                self.navigationController?.pushViewController(newVC, animated: true)
    
                    
            }
            
            
        }else{
            
            //CASH NO ESTA SELECCIONADO LO ENVIAMOS A PROCESAR LA ORDEN
            
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
            newVC.address_id = address_id
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.establishment_id = establishment_id
            newVC.payment_source_id = payment_source_id
            newVC.total = auxTotal
            newVC.notes = notes
            newVC.promotionalCode = promotionalCode
            newVC.isCash = cash
            newVC.discount = discount
            newVC.pickpalService = pickpalService
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.order_elements = order_elements
            newVC.hourScheduled = hourScheduled
            newVC.isScheduled = isScheduled
            newVC.percent = percent
            newVC.hourFancy = hourFancy
            newVC.dateScheduled = dateScheduled
            newVC.mPoints = self.mPoints
            newVC.mMoneyPoints = self.mMoneyPoints
            print("Mesa boton", self.table)
            newVC.table = self.table
            newVC.subtotal = Double(subtotal)
            newVC.paymentType =  paymentType
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  Float(self.shipping_cost)
            newVC.type_order = self.type_order
            newVC.timeDelivery = self.timeDelivery
            newVC.pickpal_cash_money = 0
            self.navigationController?.pushViewController(newVC, animated: true)
            
            
        }
    
    }
    
    
    
    func insufficientBalance() -> Bool{
        var isInsufficientBalance = Bool()
        total = auxTotal
        // comprobamos que el saldo sea suficiente
        if balanceUser >= total{
            
            //Indicamso que el saldo usado sera ocual al total y limpiamos la variable total
            pickpal_cash_money = total
            total = 0
            isInsufficientBalance = false
        }else{
        
            //Si no es suficiente calculamos el total e indicamos que se usara todo el saldo
            pickpal_cash_money = balanceUser
            total = total - balanceUser
            isInsufficientBalance = true
        
        }
        return isInsufficientBalance
    }
    
    
    @IBAction func btnContinuar(_ sender: Any) {
        
        insufficientBalance()
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "InvoiceOrder") as! InvoiceOrderViewController
        newVC.detail = detail
        newVC.paraAqui = paraAqui
        newVC.payment_source_id = "99999"
        newVC.establishment_id = establishment_id
        newVC.total = total
        newVC.notes = notes
        newVC.promotionalCode = promotionalCode
        newVC.discount = discount
        newVC.pickpalService = pickpalService
        newVC.municipio = municipio
        newVC.categoria = categoria
        newVC.lugar = lugar
        newVC.order_elements = order_elements
        newVC.hourScheduled = hourScheduled
        newVC.isScheduled = isScheduled
        newVC.inBar = inBar
        newVC.percent = percent
        newVC.table = table
        newVC.hourFancy = hourFancy
        newVC.dateScheduled = dateScheduled
        newVC.mPoints = self.mPoints
        newVC.mMoneyPoints = self.mMoneyPoints
        newVC.addressDelivery = self.addressDelivery
        newVC.shipping_cost =  Float(self.shipping_cost)
        newVC.type_order = self.type_order
        newVC.timeDelivery = self.timeDelivery
        newVC.myCash = self.myCash
        newVC.pickpal_cash_money = pickpal_cash_money
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    
    func didCheckMyCash(walletInfo: MyCash) {
//        self.viewDidAppear(true)
        myCash = walletInfo
        balanceUser = myCash.userCurrentAmount ?? 0
        loadPickpalCash()
        
    }
    
    
    func didFailCheckMyCash(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
}
