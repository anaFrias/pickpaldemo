//
//  ParaLlevarCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ParaLlevarCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var greenBackground: UIImageView!
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var paraLlevarLbl: UILabel!
    @IBOutlet weak var iconoParaLlevar: UIImageView!
    
}
