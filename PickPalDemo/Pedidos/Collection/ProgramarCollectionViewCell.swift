//
//  ProgramarCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ProgramarCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageClock: UIImageView!
    @IBOutlet weak var programarLbl: UILabel!
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var background: UIImageView!
}
