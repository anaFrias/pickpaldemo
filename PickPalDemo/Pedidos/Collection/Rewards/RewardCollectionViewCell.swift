//
//  RewardCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 8/27/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
protocol passPercent {
    func percentTyped(reward: Double)
}

class RewardCollectionViewCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var valueText: UILabel!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var textInput: UITextField!
    @IBOutlet weak var textInputPropina: UILabel!
    var delegate: passPercent!
    @IBOutlet weak var otroPorcentaje: UITextField!
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Porcentaje")
        if textField.text != "" {
            let percentaje = (Float(textField.text!.replacingOccurrences(of: "%", with: "")) as! Float)/100
            textInput.text = textField.text! + "%"
            self.delegate.percentTyped(reward: Double(percentaje))
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 2 // Bool
    }
}

