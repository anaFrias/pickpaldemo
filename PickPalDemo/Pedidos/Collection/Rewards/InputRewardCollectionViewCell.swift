//
//  InputRewardCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 8/27/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class InputRewardCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var inputText: UITextField!
    
}
