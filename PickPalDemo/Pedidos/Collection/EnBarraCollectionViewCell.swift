//
//  EnBarraCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 7/25/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class EnBarraCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var iconBarra: UIImageView!
    @IBOutlet weak var programarLbl: UILabel!
}
