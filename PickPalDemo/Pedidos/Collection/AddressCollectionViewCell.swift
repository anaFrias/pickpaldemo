//
//  AddressCollectionViewCell.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 09/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AddressCollectionViewCell: UICollectionViewCell {
   
    
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var lbAddres: UILabel!
    @IBOutlet weak var lblDataAddress: UILabel!
}
