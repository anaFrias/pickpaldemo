//
//  ParaAquiCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ParaAquiCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var paraAquiLbl: UILabel!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var checked: UIImageView!
    @IBOutlet weak var iconoParaAqui: UIImageView!
    
}
