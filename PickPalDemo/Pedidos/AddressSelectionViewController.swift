//
//  AddressSelectionViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import UserNotifications

class AddressSelectionViewController: UIViewController, paymentDelegate, AddAddressDelegate, EditAddressDelegate, EditAddress2Delegate {
    
    @IBOutlet weak var txtTotal: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var paymentWS = PaymentWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var address = [BillingAddress]()
    var billing_address_id = 0
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var establishment_id = Int()
    var total = Double()
    var payment_source_id = String()
    var flags = [Bool]()
    var selected = -1
    var notes = String()
    var promotionalCode = String()
    var pickpalService = Double()
    var discount = Double()
    var order_elements = [Dictionary<String,Any>]()
    var hourScheduled = String()
    var isScheduled = Bool()
    var inBar = Bool()
    var percent = Double()
    var hourFancy = String()
    var dateScheduled = String()
    var subtotal = Double()
    var paymentType = Int()
    var addressDelivery = Address()
    var shipping_cost = Float()
    var type_order = String()
    var timeDelivery = Int()
    var mPoints = Double()
    var mMoneyPoints = Double()
    var lugar = String()
    var municipio = String()
    var categoria = String()
    var table = Int()
    var myCash = MyCash()
    var pickpal_cash_money = Double()
    var isInsufficientBalance = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        paymentWS.delegate = self
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double(total) as NSNumber) {
            txtTotal.text = "\(formattedTipAmount)"
        }
        
        //txtTotal.text = "$\(total) MXN"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        paymentWS.getBillingAddress(client_id: client_id)
        LoadingOverlay.shared.showOverlay(view: self.view)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func Order(_ sender: Any) {
        if billing_address_id != 0{
//            Insertar la notificación hardcoded
            let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
//            let trigger1 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
//            let content1 = UNMutableNotificationContent()
//            content1.title = "PickPal"
////            content1.body = "Tu factura de \(establishment_name1) no pudo ser procesada"
////            content1.body = "Tu factura de \(establishment_name1) ya se encuentra disponible"
//            content1.body = "Tu factura de  \(establishment_name1) no pudo ser procesada, envía tus datos de facturación y una captura de pantalla de tu pedido al correo de operaciones@pickpal.com​ para darle seguimiento."
//            content1.sound = UNNotificationSound.default()
//            let request1 = UNNotificationRequest(identifier: "notification", content: content1, trigger: trigger1)
//            content1.categoryIdentifier = "FEL"
//            
//            UNUserNotificationCenter.current().add(request1) { (error) in
//                if let error = error {
//                    print("Se ha producido un error: \(error)")
//                }
//            }
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
            newVC.address_id = billing_address_id
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.establishment_id = establishment_id
            newVC.payment_source_id = payment_source_id
            newVC.total = total
            newVC.notes = notes
            newVC.promotionalCode = promotionalCode
            newVC.discount = discount
            newVC.pickpalService = pickpalService
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.order_elements = order_elements
            newVC.isScheduled = isScheduled
            newVC.hourScheduled = hourScheduled
            newVC.inBar = inBar
            newVC.percent = percent
            newVC.table = table
            newVC.hourFancy = hourFancy
            newVC.dateScheduled = dateScheduled
            newVC.subtotal = subtotal
            newVC.mPoints = self.mPoints
            newVC.mMoneyPoints = self.mMoneyPoints
            newVC.paymentType = paymentType
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  self.shipping_cost
            newVC.type_order = self.type_order
            newVC.timeDelivery = self.timeDelivery
            newVC.pickpal_cash_money = pickpal_cash_money
            newVC.pickpal_cash_money = pickpal_cash_money
            
            print("Addres Select Direccion: " ,  self.addressDelivery.pk)
            print("Addres Select Pickpla: " ,  self.shipping_cost)
            print("Addres Select Tipo orden: " ,  self.type_order)
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            let alert = UIAlertController(title: "PickPal", message: "Por favor selecciona una direccion de facturación", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func didSuccessGetBillingAddress(info: [BillingAddress]) {
        LoadingOverlay.shared.hideOverlayView()
        address = info
        flags.removeAll()
        flags.append(true)
        for _ in 0..<info.count - 1{
            flags.append(false)
        }
        billing_address_id = address[0].id
        tableView.reloadData()
    }
    func didFailGetBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if error != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
        
    }
    func clickAddAddress(indexPath: IndexPath){
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BilingAddressViewController") as! BillingAddressViewController
        newVC.detail = detail
        newVC.paraAqui = paraAqui
        newVC.establishment_id = establishment_id
        newVC.payment_source_id = payment_source_id
        newVC.total = total
        newVC.notes = notes
        newVC.promotionalCode = promotionalCode
        newVC.billingAddresses = address
        newVC.info = address[indexPath.row]
        newVC.isScheduled = isScheduled
        newVC.hourScheduled = hourScheduled
        newVC.inBar = inBar
        newVC.percent = percent
        newVC.table = table
        newVC.hourFancy = hourFancy
        newVC.dateScheduled = dateScheduled
        newVC.addressDelivery = self.addressDelivery
        newVC.shipping_cost =  self.shipping_cost
        newVC.type_order = self.type_order
        newVC.timeDelivery = self.timeDelivery
        newVC.pickpal_cash_money = pickpal_cash_money
        newVC.isInsufficientBalance = self.isInsufficientBalance
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func clickEditAddress(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BilingAddressViewController") as! BillingAddressViewController
        newVC.isUpdate = true
        newVC.billing_addres_id = address[indexPath.row].id
        newVC.addressId = address[indexPath.row].id
        newVC.billingAddresses = address
        newVC.info = address[indexPath.row]
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func clickEdit2Address(indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BilingAddressViewController") as! BillingAddressViewController
        newVC.isUpdate = true
        newVC.addressId = address[indexPath.row].id
        newVC.info = address[indexPath.row]
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
}
extension AddressSelectionViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return address.count + 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == address.count ){
           
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "BilingAddressViewController") as! BillingAddressViewController
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.establishment_id = establishment_id
            newVC.payment_source_id = payment_source_id
            newVC.total = total
            newVC.notes = notes
            newVC.promotionalCode = promotionalCode
            newVC.isScheduled = isScheduled
            newVC.hourScheduled = hourScheduled
            newVC.inBar = inBar
            newVC.percent = percent
            newVC.table = table
            newVC.hourFancy = hourFancy
            newVC.dateScheduled = dateScheduled
            newVC.mPoints = self.mPoints
            newVC.mMoneyPoints = self.mMoneyPoints
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  self.shipping_cost
            newVC.type_order = self.type_order
             newVC.timeDelivery = self.timeDelivery
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            billing_address_id = address[indexPath.row].id
            for i in 0..<flags.count {
                if i != indexPath.row {
                    flags[i] = false
                }else{
                    flags[i] = true
                }
            }
            selected = indexPath.row
            tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row == address.count ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddressCell", for: indexPath) as! AddAddressCellTableViewCell
            cell.delegate = self
            cell.indexPath = indexPath
            return cell
        
        }else{
            if flags[indexPath.row]{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCellTableViewCell
                cell.rfc.text = address[indexPath.row].RFC
                cell.indexPath = indexPath
                cell.background.image = UIImage(named: "RectangleGreen")
                cell.editButton.setImage(UIImage(named: "iconEdit"), for: .normal)
                cell.rfc.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                cell.delegate = self
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCellTableViewCell
                cell.rfc.text = address[indexPath.row].RFC
                cell.indexPath = indexPath
                cell.editButton.setImage(UIImage(named: "EditWhite"), for: .normal)
                billing_address_id = address[indexPath.row].id
                cell.background.image = UIImage(named: "rectangle_border_gray")
                cell.rfc.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                cell.delegate = self
                return cell
            }
        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
