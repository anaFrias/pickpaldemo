//
//  AddressFormViewController.swift
//  PickPalTesting
//
//  Created by Oscar on 03/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved. GMSMapViewDelegate
//

import UIKit
import GoogleMaps
import MapKit

protocol AddressFormAddressDelegate {
    func setDataDelivery( addressSelect:Address, shoppingCost: Double, timeDelivery: Int)
    func backListAddress()
}

class AddressFormViewController: UIViewController ,UITextFieldDelegate, deliveryDelegate {
    // Agregar una nueva dirección
    var direccion = Address()
    var delivery = DeliveryWS()
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int

//    NUEVO MAPA
    @IBOutlet weak var newMap: GMSMapView!
    
    
    @IBOutlet weak var scrollViewForm: UIScrollView!
    @IBOutlet weak var viewForm: UIStackView!
    @IBOutlet weak var textF_Calle: UITextField!
    @IBOutlet weak var textF_Numero_ext: UITextField!
    @IBOutlet weak var textF_Numero_Interior: UITextField!
    @IBOutlet weak var textF_Colonia: UITextField!
   @IBOutlet weak var textF_Estado: UITextField!
   @IBOutlet weak var textF_CP: UITextField!
   @IBOutlet weak var textF_Calle1: UITextField!
   @IBOutlet weak var textF_Calle2: UITextField!
   @IBOutlet weak var textF_Referencia: UITextField!
   @IBOutlet weak var textF_Municipio: UITextField!
   // MKMapView GMSMapView!
    @IBOutlet weak var mapDeliveryView: MKMapView!
    @IBOutlet weak var btnRefresh: UIButton!
    
    @IBOutlet weak var lblAddressComplete: UILabel!
    var from = "MyAddress"
    
    var locationAux: CLLocationCoordinate2D!
    
    
    
    @IBOutlet weak var titleSection: UILabel!
    
    var centerMapCoordinate:CLLocationCoordinate2D!
    var marker:GMSMarker!
    var locationManager = CLLocationManager()
    var isEditAddrress = false
    var editAddress = Address()
    var cirlce: GMSCircle!
    var mapView: GMSMapView!
    var titleSectionText = String()
    var isNewAddress = false
    var delegate: AddressFormAddressDelegate?
    var autocomplete = false
    var typeAddress = String()
    var actionAddress = String()
    @IBOutlet weak var viewTitle: UIView!
    
   private var centerCoordinate = CLLocationCoordinate2D()
    
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnSaveAddress(_ sender: Any) {
        
        self.direccion.calleReferencia1 = textF_Calle1.text
        self.direccion.calleReferencia2 = textF_Calle2.text
        self.direccion.referencia = textF_Referencia.text
        
        self.editAddress.calleReferencia1 = textF_Calle1.text
        self.editAddress.calleReferencia2 = textF_Calle2.text
        self.editAddress.referencia = textF_Referencia.text
        
//        if isEditAddrress{
//
//            print("Esta es la direccion a guardar", direccion.calle!)
//            print("Coordenadas Nuevas: \(editAddress.latitud) \(editAddress.longitud) ")
//            direccion.pk = editAddress.pk
//            delivery.updateAddress(address: editAddress, client_id: client_id)
//
//        }else{
//
//            print("Esta es la direccion a guardar", editAddress.calle)
//            delivery.setAddres(address: editAddress, client_id: client_id)
//
//        }
        
        switch actionAddress {
        
        case "newAddress":
            print("Esta es la direccion a guardar", editAddress.street)
            delivery.setAddres(address: editAddress, client_id: client_id)
 
        case "editAddres":
            editAddress.pk = direccion.pk
            delivery.updateAddress(address: editAddress, client_id: client_id)
            
            
        case "saveAddress":
            delivery.getDeliveryCost(addressDelivery: String(direccion.pk!), establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
            
        default:
            print("Caso defult")
        }
          
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delivery.delegate = self
        
        titleSection.text = titleSectionText
        viewTitle.clipsToBounds = false
        viewTitle.layer.masksToBounds = false
        viewTitle.layer.cornerRadius = 5
        viewTitle.layer.shadowColor = UIColor.black.cgColor
        viewTitle.layer.shadowOpacity = 0.35
        viewTitle.layer.shadowOffset = .zero
        viewTitle.layer.shadowRadius = 5
        
        
        viewForm.clipsToBounds = false
        viewForm.layer.masksToBounds = false
        viewForm.layer.cornerRadius = 5
        viewForm.layer.shadowColor = UIColor.black.cgColor
        viewForm.layer.shadowOpacity = 0.35
        viewForm.layer.shadowOffset = .zero
        viewForm.layer.shadowRadius = 5
        
        
//        NUEVO MAPA
    
        
        locationManager.delegate = self
        newMap.delegate = self

        
        if CLLocationManager.locationServicesEnabled() {
          // 3
          locationManager.requestLocation()

          // 4
          newMap.isMyLocationEnabled = true
          newMap.settings.myLocationButton = true
            
            
            switch typeAddress {
            case "edit":
                loadInfoAddress()
                
            case "autocomplete":
                btnRefresh.isHidden = true
                loadInfoAddress()
                
                
            default:
                btnRefresh.isHidden = true
                print("Caso default")
                var location = CLLocationCoordinate2D(latitude: editAddress.latitude, longitude: editAddress.longitude )
             
                newMap.camera = GMSCameraPosition( target: location, zoom: 16,bearing: 0,viewingAngle: 0)
                reverseGeocode(coordinate: location)
                
            }
            
            
            
        } else {
          // 5
          locationManager.requestWhenInUseAuthorization()
        }

    }
    
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
                      
     //   searchAddress()

  }

    func loadMap(latitude: Double,longitude: Double ){
        
        if locationManager.location != nil {
            
            // MARK: - MKMapView
        
            centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: longitude )
        
            let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0), fromEyeCoordinate: CLLocationCoordinate2D(latitude: longitude ?? 0.0, longitude: longitude ?? 0.0), eyeAltitude: 800.0)
            mapDeliveryView.setCamera(camera, animated: false)
            let eyeCoordinate = CLLocationCoordinate2D(latitude: latitude ?? 0.0, longitude: longitude ?? 0.0)
            let annotation = MKPointAnnotation()
            annotation.coordinate = eyeCoordinate
            mapDeliveryView.addAnnotation(annotation)
            
        
            
        }
        
    }
    
    
    @IBAction func btnRefreshlocation(_ sender: Any) {
        
        newMap.camera = GMSCameraPosition( target: locationAux, zoom: 16,bearing: 0,viewingAngle: 0)
        reverseGeocode(coordinate: locationAux)
    }
    
    
    func searchAddress(){
        
        print("Buscando calle")
        
        if (textF_Calle.text! != "" && textF_Numero_ext.text!  != "" && textF_Colonia.text!  != "" && textF_Estado.text!  != "" && textF_CP.text!  != "" && textF_Municipio.text!  != "" && textF_CP.text!.count == 5){
        
            mapDeliveryView.removeAnnotations(mapDeliveryView.annotations)
        
            
        let location = "\(textF_Calle.text!) \(textF_Numero_ext.text!) \(textF_Estado.text!) \(textF_CP.text!)"
            
            print("Calle ", location)
            
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { [weak self] placemarks, error in
            if let placemark = placemarks?.first, let location = placemark.location {
                let mark = MKPlacemark(placemark: placemark)

                if var region = self?.mapDeliveryView.region {
                    
                    region.center = location.coordinate
                    region.span.longitudeDelta /= 8.0
                    region.span.latitudeDelta /= 8.0
                    
                    let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), fromEyeCoordinate: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), eyeAltitude: 100.0)
                    
                    self?.mapDeliveryView.setCamera(camera, animated: false)
                    self?.mapDeliveryView.addAnnotation(mark)
                   
                }
            }
        }
            
         }else{
            
            print("Falta informacion")
            
        }
    }
    
    
    
    func loadInfoAddress(){
                           
        if typeAddress == "edit"{
        lblAddressComplete.text = "\(direccion.street!) \(direccion.colony!)   \(direccion.zip_code!) \(direccion.city!) \(direccion.state!)  "
        
        textF_Referencia.text = "\(direccion.referencia ?? "")"
        textF_Calle1.text = "\(direccion.calleReferencia1 ?? "")"
        textF_Calle2.text =  "\(direccion.calleReferencia2 ?? "")"
        centerMapCoordinate = CLLocationCoordinate2D(latitude: direccion.latitude ?? 0.0, longitude: direccion.longitude ?? 0.0)
            
           actionAddress = "saveAddress"
            print("Coordenadas Actuales: \(direccion.city!)  \(direccion.latitude!) \(direccion.longitude!) ")
            
        }else{
            
            lblAddressComplete.text = "\(editAddress.referencia!)"
            print("Coordenadas Actuales: \(direccion.city ?? "")  \(direccion.latitude ?? 0.0) \(direccion.longitude ?? 0.0) ")
            centerMapCoordinate = CLLocationCoordinate2D(latitude: editAddress.latitude ?? 0.0, longitude: editAddress.longitude ?? 0.0)
//            isEditAddrress = true
            
        }
        
        
//        loadMap(latitude: editAddress.latitud ?? 0.0, longitude: editAddress.longitud ?? 0.0)
        
        locationAux = centerMapCoordinate
        newMap.camera = GMSCameraPosition(target: centerMapCoordinate,zoom: 16,bearing: 0, viewingAngle: 0)
        
       
        
    }
    
    
    func saveAddressUser(addressId: Int) {
        print("Respuesta direccion ", addressId)

        if from == "carrito"{
            direccion.pk = addressId
            editAddress.pk = addressId
            delivery.getDeliveryCost(addressDelivery: String(addressId), establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        }else{
            self.navigationController?.popViewController(animated: false)?.restorationClass
            delegate?.backListAddress()
            
            
        }
        
         
    }
    
    func failSaveAddress(error: String, subtitle: String) {
         print("Error al guardar ", error)
        
    
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    
    func  updateAddressUser(){
           
        print("Se Actulizo la direccion")
         
        if from == "carrito"{
            
            delivery.getDeliveryCost(addressDelivery: String(direccion.pk!), establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        }else{
            
            self.navigationController?.popViewController(animated: false)?.restorationClass
            self.navigationController?.popViewController(animated: false)?.restorationClass
 
        }
       
            
    }
    
    
    func failUpdateddress(error: String, subtitle: String) {
         print("Error al Actualizar ", error)
        
    
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    
    @IBAction func btnInicio(_ sender: Any) {
        
        print("Boton home de formulario")
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func btnPedidos(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
               self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func btnAjustes(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                      let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                      self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    
    func getShoppingCost(shoppingCost: Double, timeDelivery: Int) {
        
        if actionAddress == "saveAddress"{
            
            self.delegate!.setDataDelivery(addressSelect: direccion, shoppingCost: shoppingCost, timeDelivery: timeDelivery)
        }else{
            
            self.delegate!.setDataDelivery(addressSelect: editAddress, shoppingCost: shoppingCost, timeDelivery: timeDelivery)
        }
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    func failShoppingCost(error: String, subtitle: String) {
        

         print("ERROR al obtener precio de envio ", error)
         let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
         newXIB.modalTransitionStyle = .crossDissolve
         newXIB.modalPresentationStyle = .overCurrentContext
         newXIB.errorMessageString = error
         newXIB.subTitleMessageString = subtitle
         present(newXIB, animated: true, completion: nil)
         
    }
    

}


// MARK: - CLLocationManagerDelegate
//1
extension AddressFormViewController: CLLocationManagerDelegate, GMSMapViewDelegate {
  // 2
  func locationManager(_ manager: CLLocationManager,didChangeAuthorization status: CLAuthorizationStatus) {
    // 3
    guard status == .authorizedWhenInUse else {
      return
    }
    // 4
    locationManager.requestLocation()

    //5
    newMap.isMyLocationEnabled = true
    newMap.settings.myLocationButton = true
  }

  // 6
  func locationManager(_ manager: CLLocationManager,didUpdateLocations locations: [CLLocation]) {
    
    guard let location = locations.first else {
      return
    }

  }

  // 8
    func locationManager(_ manager: CLLocationManager,didFailWithError error: Error) {
        print(error)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
       
        reverseGeocode(coordinate: position.target)
        
       
    }
    
    
    func reverseGeocode(coordinate: CLLocationCoordinate2D) {
   
      let geocoder = GMSGeocoder()


      geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
        
        print("Coordenadas mapa: lat: \(coordinate.latitude)  long: \(coordinate.longitude)")
        
        // MARK: - CLLocationManagerDelegate
        guard
          let addressInfo = response?.firstResult(),
          let addressComplete = addressInfo.lines
        
          else {
            return
        }

        if  addressInfo.thoroughfare != nil{
        var numberComplet = ""
        var street = ""
        var string = ""
        var replaced = ""
        var stringReferencia = ""
            
        if self.isEditAddrress {
            
            
            string = addressInfo.thoroughfare!
            self.lblAddressComplete.text =  addressComplete.joined(separator: "\n")
    
        }else{
            string = self.editAddress.referencia!
            stringReferencia = self.editAddress.referencia!
            self.editAddress.referencia = ""
            
            stringReferencia = stringReferencia.components(separatedBy: CharacterSet.decimalDigits).joined()
            let stringArray =  stringReferencia.map { String($0) }
            
            for item in stringArray {
                if item != ","{
                    
                    street += item
                    
                }else{
                    if item == "," && street != ""{
                        break
                    }
                    
                }
            }
            
            print("Calle completa: ", street)
            self.editAddress.street = street
        }
            
            let stringArray =  string.components(separatedBy: CharacterSet.decimalDigits.inverted)
            
            for item in stringArray {
                if let number = Int(item) {
                    print("number: \(number)")
                    numberComplet += "\(number)"
                }else{
                    if item == "" && numberComplet != ""{
                        break
                    }
                    
                }
            }
            
            
        
            if self.isEditAddrress {
                replaced = string.replacingOccurrences(of: numberComplet, with: "")
                self.editAddress.street = replaced// addressInfo.thoroughfare
            }
            
            self.editAddress.numeroExterior = numberComplet
            self.editAddress.numeroInterior = ""
            self.editAddress.colony = addressInfo.subLocality
            self.editAddress.state =  addressInfo.administrativeArea
            self.editAddress.zip_code = addressInfo.postalCode
            self.editAddress.city = addressInfo.locality
            self.editAddress.latitude = coordinate.latitude
            self.editAddress.longitude = coordinate.longitude
               
            
            self.isEditAddrress = true
//            a < b ? a : b
            
            if self.direccion.pk != nil{
                
                self.actionAddress = "editAddres"
            }else{
                self.actionAddress = "newAddress"
            }
            
      }
        
        
        UIView.animate(withDuration: 0.25) {
          self.view.layoutIfNeeded()
//            self.isEditing = true
        }
      }
        
    }
 
}
