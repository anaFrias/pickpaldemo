//
//  CarritoViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Firebase

protocol carritoDelegate {
    func seleccionFiltro(seleccion: Int)
    
}
class CarritoViewController: UIViewController,establishmentDelegate, actionCarrito, inputEntry, paymentDelegate, reloadDataDelegate, userDelegate, inputNotesEntry, deleteCarritoDelegate,  chooseTypeOrderDelegate, hourScheduledDelegate, ordersDelegate, programmatedChooserDelegate, pedidosFiltroDelegate, selectAddressProtocol,mailingAddressDelegate, walletDelegate,CustomerServiceDelegate {

    
    
    
    @IBOutlet weak var conteinerNameeStablishment: UIView!
    
    var delegate: carritoDelegate!
    var order_elements = [Dictionary<String,Any>]()
    var notes = ""
    var establishment_id = Int()//UserDefaults.standard.object(forKey: "last_id") as! Int
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var modificadores_precio = [[Double]]()
    var modificadores = [[String]]()
    var complementos = [[String]]()
    var complementos_precio = [[Double]]()
    var complementos_cantidad = [[Int]]()
    var pricesFinal =  [Double]()
    var subtotal:Double = 0.00
    var subtotalAux:Double = 0.00
    var total: Double = 0.0
    var llevar = Bool()
    var applied_Code = false
    var codeString = String()
    var total_code = Double()
    var defaults = UserDefaults.standard
    var mailingAddress = MailingAddressViewController()
    
    var paymentWS = PaymentWS()
    var userWS = UserWS()
    var establishmentWS = EstablishmentWS()
    var ordersws = OrdersWS()
   
    var modifiers = [Modifiers]()
    var complements = [Complements]()
    var comission = Double()
    var arrayNotes = String()
    var isPercent = Bool()
    var discount = Double()
    var handle: DatabaseHandle!
    var handle2: DatabaseHandle!
    var handle3: DatabaseHandle!
    
    var itemsObserve: DatabaseReference!
    var extrasObserve: DatabaseReference!
    var modifiersObserve: DatabaseReference!
    
    var municipio = String()
    var categoria = String()
    var lugar = String()
    var esablishmentName = String()
    var limit_extra = Int()
    var carry_mode = Int()
    var carryModeArray = [Int]()
    var carryModeArrayBool = [Bool]()
    var finishTime = String()
    var isActive = Bool()
    var hourScheduled = String()
    var hourFancy = String()
    var isScheduled = Bool()
    var paymentType = Int()
    var inBar = Bool()
    var dateSchedule = String()
    var dateCreatedSchedule = String()
    var selectedOptionsProgrammed = false
    var percent:Double = 0.0
    var choosenPercent = Double()
    var percentValid = false
    var validPercent = Double()
    var rewardSaved = Int()
    var mPoints = Double()
    var mMoneyPoints = Double()
    var discountPointApply = false
    var isUsedPoint = false
    
    var mMyReward = MyRewardEstablishment()
    var mInvoincing = Invoicing()
    var totalSecond:Double = 0.0
    var typeService = String()
    var selectItem = false

    var shoppingCost = Double()
    var filterDefaut = String()
    var auxPointMoney = Double()
    var descPuntos = Bool()
    var minimumBilling = Double()
    var prices_complementsArray = [Double]()
    
    
    //Wallet
    
    var walletWS = WalletWS()
    var myCash = MyCash()
    var scannerDelegate = ScannerViewController()
   
    @IBOutlet weak var palomitaParaComer: UIImageView!
    @IBOutlet weak var palomitaParaLlevar: UIImageView!
    @IBOutlet weak var est_name: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backroundLlevar: UIImageView!
    @IBOutlet weak var backgroundAqui: UIImageView!
    @IBOutlet weak var txtAqui: UILabel!
    @IBOutlet weak var txtLlevar: UILabel!
    
    @IBOutlet weak var paraComerView: UIView!
    @IBOutlet weak var paraLlevarView: UIView!
    @IBOutlet weak var bolsita: UIImageView!
    @IBOutlet weak var charola: UIImageView!
    var valueOfPointsInmoey = 0.0
    var optionsSize = 0
    var filtroSize = 200
    var direccionSize = 0
    
    var timeToLast = Int()
    var inSiteString = String()
    var items = [String]()
    var defaultReward = Int()
    var tableSelected = Int()
    var numArrayTables = [String]()
    var programarPedido = false
    var statusServiceProgramer = String()
    var addressSelect = Address()
    var isSelectAddress = false
   // var shipping_cost = 0.0
    var type_order = String()
    var picpalService = Double()
    var isSelectTypeService = false
    var timeDelivery = Int()
    var defualtItem = Bool()
    var order_payments = NSDictionary ()
    var dinamicHeightProducts: CGFloat = 25
    var customerServiceWS = CustomerServiceWS()
    var visibilityButtomSM = false
    var total_amount_money = Double()
    var total_amount = Double()
    
    var fromFolio = String()
    var idDeskUser = Int()
    var listTable = [TableJointUser]()
    
    /////////////////////////////////////////////////////////////////////////////
    
    var notesTemp = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        defaultSelection()
        
        conteinerNameeStablishment.clipsToBounds = false
               conteinerNameeStablishment.layer.masksToBounds = false
               conteinerNameeStablishment.layer.cornerRadius = 5
               conteinerNameeStablishment.layer.shadowColor = UIColor.black.cgColor
               conteinerNameeStablishment.layer.shadowOpacity = 0.35
               conteinerNameeStablishment.layer.shadowOffset = .zero
               conteinerNameeStablishment.layer.shadowRadius = 2
        
        est_name.text = DatabaseFunctions.shared.getEstablishmentName(query:DatabaseProvider.sharedInstance.establishment_name)
        paymentWS.delegate = self
        userWS.delegate = self
        establishmentWS.delegate = self
        ordersws.delegate = self
        walletWS.delegate = self
        scannerDelegate.delegate = self
        customerServiceWS.delegate = self
    
     //    self.view.isUserInteractionEnabled = true
        
        tableView.flashScrollIndicators()
        print("Puntos por usuario: \(mPoints)")
       
        walletWS.getMyCash(idEstablishment: establishment_id)
        ordersws.getPointToUse(establishment_id: establishment_id, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, subtotal: self.subtotal)
        establishmentWS.getInvoicingByPickpal(establishmentId: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        
        customerServiceWS.mySignedTables(client_id: client_id, establishment_id: establishment_id)
        
        defaults.set("PedidosStoryboard", forKey: "storyboard")
        defaults.set("Carrito", forKey: "screen")
       
        let str = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        let encoded = str.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
        if let order_elements = dictionary as? [Dictionary<String,Any>] {//UserDefaults.standard.object(forKey: "order_elements") as? [Dictionary<String, Any>] {
            self.order_elements = order_elements
            //            print(order_elements)
            for i in 0..<order_elements.count{
                var prices_complements = 0.0
                var tempPriceArray = [Double]()
                if let arrayM = ((order_elements[i] )["complements"]) as? NSDictionary {
                    //                    print(((order_elements[i] )["complements"]) as? NSDictionary)
                    if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                        for price in arrayM.value(forKey: "complement_price") as! [Double] {
                            tempPriceArray.append(price)
                        }
                    }
                }
                if let arrayM = ((order_elements[i] )["modifiers"]) as? NSArray {
                    //                    print(arrayM)
                    for modif in arrayM {
                        //                        print(modif)
                        if let arr = (modif as! NSDictionary).value(forKey: "modifier_price") as? NSArray {
                            for tempM in arr {
                                //                                print(tempM)
                                tempPriceArray.append(tempM as! Double)
                            }
                            
                        }
                        
                    }
                }
                
                for p in tempPriceArray{
                    prices_complements += p
                }
                
                if prices_complements > 0{
                    prices_complementsArray.append(prices_complements)
                }
                
                //                print(((order_elements[i] as NSDictionary).value(forKey: "price") as! Double),prices_complements,((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double) )
//                pricesFinal.append((((order_elements[i] as NSDictionary).value(forKey: "price") as! Double) + (prices_complements * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double))) / ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double))
                
                pricesFinal.append(((order_elements[i] as NSDictionary).value(forKey: "price_product") as! Double) + prices_complements )
                
                
            }
            calculateprice(prices: pricesFinal, discount: 0)
            inactivateItemsObserve(items: order_elements)
            inactivateExtrasObserve(items: order_elements)
            inactivateModifiersObserve(items: order_elements)
        }
        UserDefaults.standard.removeObject(forKey: "date_saved")
        notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if descPuntos{
            
            for index in 0...items.count {
                if items[index] == "total"{
                    tableView.reloadData()
                    break
                }
            }
            
        }
//        calculateprice(prices: pricesFinal, discount: discount)
     
    }
    
    func didSuccessGetCheckTimeOrder(total_time: Int) {
        let currentDay = Date()
        let finishTimeDate = self.finishTime
        //        para pasarlo a formato de Date
        let form = DateFormatter()
        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateRegister = form.date(from: finishTimeDate)
        self.timeToLast = total_time
        let calendar = Calendar.current
        let date = calendar.date(byAdding:.minute, value: total_time, to: dateRegister!)!
        let timeInterval = date.timeIntervalSince1970 ?? 0 * 1000
        let timeIntervalCurrent = currentDay.timeIntervalSince1970 ?? 0 * 1000
        print(timeInterval)
        if self.paymentType == 2 || self.paymentType == 0 {
            if (timeIntervalCurrent > timeInterval) {
                self.isActive = false
            }else{
                if !carryModeArray.contains(2) {
                    self.isActive = false
                }else{
                    if !carryModeArray.contains(5) {
                        self.isActive = false
                    }else{
                        self.isActive = true
                    }
                }
            }
        }else{
            self.isActive = false
        }
        
    }
    
    
    
    func didFailGetCheckTimeOrder(error: String, subtitle: String) {
        self.timeToLast = 0
    }
    
    func didSuccessGetInvoicing(invoicingInfo: Invoicing) {
        
       mInvoincing = invoicingInfo
        minimumBilling = invoicingInfo.minimum_billing
        
      //  picpalService = mInvoincing.schedule_orders.pickpal_service_cost
        
        print(carry_mode)
        //        self.num_tables = invoicingInfo.number_tables
        self.paymentType = invoicingInfo.payment_methods
        self.finishTime = invoicingInfo.schedule_orders.finish_date
        for i in 0..<4 {
            if i == 0 {
                carryModeArrayBool.append(true)
            }else{
                carryModeArrayBool.append(false)
            }
        }
        for i in 0..<invoicingInfo.number_tables {
            numArrayTables.append("Mesa \(i+1)")
        }
        
        if invoicingInfo.payment_methods == 1 {
            carryModeArray = [2,-1,-1,5]
        }else{
            if !invoicingInfo.order_mode.contains(2){
                if !invoicingInfo.order_mode.contains(3){
                    if !invoicingInfo.order_mode.contains(4){
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [-1,-1,-1,-1]
                        }else{
                            carryModeArray = [-1,-1,-1,5]
                        }
                    }else{
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [-1,-1,4,-1]
                        }else{
                            carryModeArray = [-1,-1,4,5]
                        }
                    }
                }else{
                    if !invoicingInfo.order_mode.contains(4){
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [-1,3,-1,-1]
                        }else{
                            carryModeArray = [-1,-3,-1,5]
                        }
                    }else{
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [-1,3,4,-1]
                        }else{
                            carryModeArray = [-1,3,4,5]
                        }
                    }
                }
            }else{
                if !invoicingInfo.order_mode.contains(3){
                    if !invoicingInfo.order_mode.contains(4){
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [2,-1,-1,-1]
                        }else{
                            carryModeArray = [2,-1,-1,5]
                        }
                    }else{
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [2,-1,4,-1]
                        }else{
                            carryModeArray = [2,-1,4,5]
                        }
                    }
                }else{
                    if !invoicingInfo.order_mode.contains(4){
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [2,3,-1,-1]
                        }else{
                            carryModeArray = [2,3,-1,5]
                        }
                    }else{
                        if !invoicingInfo.order_mode.contains(5){
                            carryModeArray = [2,3,4,-1]
                        }else{
                            carryModeArray = [2,3,4,5]
                        }
                    }
                }
            }
            
        }
        defaultReward = invoicingInfo.default_reward
        rewardSaved = invoicingInfo.default_reward
        let order = getSavedOrder()
        ordersws.checktimeOrder(establishmentId: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id), client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, order: order)
    }
    func didFailGetInvoicing(error: String, subtitle: String) {
        print(error)
    }
    func getPercentOf(percent: Double) {
        validPercent = percent
        if percent > 0.3 {
            percentValid = false
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = "Atencion"
            newXIB.subTitleMessageString = "Puede dejar un máximo de hasta 30% en propina"
            present(newXIB, animated: true, completion: nil)
        }else{
            percentValid = true
            self.percent = percent
            calculateprice(prices: pricesFinal, discount: discount)
           
        }
        
        tableView.reloadData()
    }
    func getTableChoosen(table: Int) {
        print("Mesa Selecionada ", table )
        tableSelected = table
    }
    
    func expandSettings(llevar: Bool, expand: Bool, selectedItem: Int, isScheduled: Bool, inBar: Bool, percent: Float, expandItem: String, order_payments: NSDictionary) {
        
        
        self.order_payments = order_payments
        
     /*   for index in 0...items.count {
            print("\(items[index])")
            if items[index] == "total"{
                tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                break
            }
        }*/
        
        
        selectItem = true
        if selectedItem != 3 || selectedItem != 1{
            
            statusServiceProgramer = "Inmediato"
            programarPedido = false
           
            hourScheduled = ""
            hourFancy = ""
            dateSchedule = ""
            dateCreatedSchedule = ""
        }
        
        if inBar {
           
            defaultReward = rewardSaved
            percentValid = false
        }else if !llevar {
            defaultReward = rewardSaved
            percentValid = false
        }
        
        if expand {
                    
            switch expandItem {
                case "llevar":
                        optionsSize = 0
                        filtroSize = 320
                        direccionSize = 0
                        shoppingCost = 0
                        isSelectAddress = false
                        self.llevar = true
                        getPercentOf(percent:0.0)
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
               /* self.mPoints = 0
                self.mMoneyPoints = 0
                self.descPuntos = false*/
                
                case "domicilio":
                         optionsSize = 0
                         filtroSize = 320
                         direccionSize = 190
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
                    if defaultReward > 0 {
                                                  
                     let defualPropina =  Double(defaultReward)/100
                        getPercentOf(percent: defualPropina )
                    }
                         
                         
                case "pedidoInmediato":
                         filtroSize = 320
                         optionsSize = 0
                         programarPedido = false
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
                         
                case "pedidoProgramado":
                          filtroSize = 410
                          optionsSize = 0
                          programarPedido = true
                    hourScheduled = ""
                    hourFancy = "Selecciona la hora:"
                    dateSchedule = ""
                    dateCreatedSchedule = ""
                    
                case "barra":
                        optionsSize = 210
                        filtroSize = 200
                        direccionSize = 0
                        shoppingCost = 0
                        isSelectAddress = false
                        programarPedido = false
                        self.llevar = false
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
                        tableView.reloadData()
                        
                case "mesa":
                        
                        optionsSize = 250
                        filtroSize = 200
                        direccionSize = 0
                        shoppingCost = 0
                        isSelectAddress = false
                        programarPedido = false
                        self.llevar = false
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
                    //LLamar Mesero Pedir cuenta
                    
//                    if listTable.count > 0{
//                        openDialogDisJointTable()
//                    }
                    
                    if tableSelected != 0{
                        alertDisJointTable(nameEstablisment: "Nombre establecimiento", numberTable: tableSelected, idDeskUser: idDeskUser)
                    }else{
                        
                        
                    }
                    
                        tableView.reloadData()
                        
                default:
                        optionsSize = 210//392
                        filtroSize = 200
                        direccionSize = 0
                        isSelectAddress = false
                    hourScheduled = ""
                    hourFancy = ""
                    dateSchedule = ""
                    dateCreatedSchedule = ""
            }
                if inBar {
                    UserDefaults.standard.removeObject(forKey: "date_saved")
                }
        }else{
               // optionsSize = 210
                UserDefaults.standard.removeObject(forKey: "date_saved")
        }

             self.llevar = llevar
     
        
                 for i in 0..<4{
                     if i == selectedItem {
                         carryModeArrayBool[i] = true
                     }else{
                         carryModeArrayBool[i] = false
                     }
                 }
         tableView.reloadData()
         tableView.beginUpdates()
         self.isScheduled = programarPedido
         self.inBar = inBar
         tableView.endUpdates()
         UIView.animate(withDuration: 0.2) {
            
            
       //  print("direccionSize ", self.direccionSize)
     //    self.calcularServicioPicpal()
            
          //   calcularServicioPicpal()
                     
                     //            self.tableView.setContentOffset(CGPoint(x: self.tableView.frame.origin.x, y: self.getEmbeededTableViewHeight()), animated: true)
         //            self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
         }

    }
    
    
    func menssageDialog(titulo: String, mensaje: String) {
           let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
           newXIB.modalTransitionStyle = .crossDissolve
           newXIB.modalPresentationStyle = .overCurrentContext
           newXIB.mMessage = mensaje
           newXIB.mTitle = titulo
           newXIB.mToHome = false
           newXIB.mTitleButton = "Aceptar"
           newXIB.mId = 0
           newXIB.valueCall = 0
           newXIB.isFromAdd = false
           newXIB.delegate = self
           self.present(newXIB, animated: true, completion: nil)
       }
    
    func getTypeService(typeService: String) {
        switch typeService {
            case "RS":
                percentValid = true
            case "SM":
                percentValid = false
            case "RB":
                percentValid = false
            case "SD":
                percentValid = false
            default:
                percentValid = true
        }

        isSelectTypeService = true
        self.typeService = typeService
        
        calcularServicioPicpal()
        tableView.reloadData()
        
    }
    
    func getStatusServiceProgramer(status: String) {
       
        statusServiceProgramer = status
    }
    
    func showDialogError(title: String, message: String) {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    
    func chooserOne(llevar: Bool, expand: Bool, selectedItem: Int, isScheduled: Bool, inBar: Bool, percent: Double,expandItem:String) {
        
       if selectedItem != 3 || selectedItem != 1{
            
            statusServiceProgramer = "Inmediato"
            
        }
        
        if inBar {
           
            defaultReward = rewardSaved
            percentValid = false
        }else if !llevar {
            defaultReward = rewardSaved
            percentValid = false
        }
        tableView.beginUpdates()
        
        if expand {
            
            
            if inBar {
                UserDefaults.standard.removeObject(forKey: "date_saved")
            }
        }else{
            optionsSize = 210
            UserDefaults.standard.removeObject(forKey: "date_saved")
        }
        
//        self.llevar = llevar
        for i in 0..<4{
            if i == selectedItem {
                carryModeArrayBool[i] = true
            }else{
                carryModeArrayBool[i] = false
            }
        }
        self.isScheduled = isScheduled
        self.inBar = inBar
        tableView.endUpdates()
        UIView.animate(withDuration: 0.2) {
           
            
            //            self.tableView.setContentOffset(CGPoint(x: self.tableView.frame.origin.x, y: self.getEmbeededTableViewHeight()), animated: true)
//            self.tableView.scrollToRow(at: IndexPath(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.percent = percent
            self.calculateprice(prices: self.pricesFinal, discount: self.discount)
        })
        
    }
    func showDatePicker() {
        let tempArray = getSavedOrder()
        //        print(order)
        let datePicker = HourPickerViewController()
        datePicker.modalTransitionStyle = .crossDissolve
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.delegate = self
        let estimated = self.finishTime
        let form = DateFormatter()
        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if form.date(from: estimated) != nil {
            let calendar = Calendar.current
            let date = calendar.date(byAdding:.minute, value: -self.timeToLast, to: form.date(from: estimated)!)!
            datePicker.dateLimit = date
        }
        datePicker.order = tempArray
        self.present(datePicker, animated: true, completion: nil)
    }
    func showValuePicker() {
        let valuePicker = ChooseProgrammatedViewController()
        valuePicker.modalTransitionStyle = .crossDissolve
        valuePicker.modalPresentationStyle = .overCurrentContext
        valuePicker.delegate = self
        self.present(valuePicker, animated: true, completion: nil)
    }
    func getValueProgrammed(in_site: Bool) {
        //        if in_site == false {
        //            inSiteString = "Para comer aquí"
        //        }else{
        //            inSiteString = "Para llevar"
        //        }
       // llevar = true//in_site
        selectedOptionsProgrammed = programarPedido
        tableView.reloadData()
        tableView.setContentOffset(tableView.contentOffset, animated: false)
        
        //        tableView.reloadRows(at: [IndexPath(row: order_elements.count + 7, section: 0)], with: .bottom)
    }
    func hourSchedule(hrFancy: String, hourWS: String, date: String, dateCheck: String) {
        hourScheduled = hourWS
        hourFancy = hrFancy
        dateSchedule = date
        dateCreatedSchedule = dateCheck
        if let finishTimeDate = UserDefaults.standard.object(forKey: "date_saved") as? Date { //hora guardada
            let currentDay = Date() //Hora actual
            let calendar = Calendar.current
            let date = calendar.date(byAdding:.minute, value: -2, to: currentDay)!
            if date <= finishTimeDate {
                self.tableView.reloadData()
            }else{
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Horario inválido"
                newXIB.subTitleMessageString = "Selecciona otro horario"
                present(newXIB, animated: true, completion: nil)
            }
        }
        
    }
    func finishInputNotes(code: String) {
        notes = code
        notesTemp = code
    }
    
    func calculateprice(prices: [Double], discount: Double){
        subtotal = 0.00
        total = 0.00
        items.removeAll()
        items.append("filtroPedidos")
        items.append("header") //0
        //        for order in order_elements {
        //
        //        }
        
        
        for i in 0..<prices.count{
            items.append("productos")//N
            subtotal += Double(prices[i]) * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double)
            subtotalAux +=  Double(prices[i]) * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double)
        }
        print("valor order_elements " ,order_elements.count)
        items.append("subtotal")//N +1
        
        if percent != 0.0 {
            choosenPercent = subtotal * percent
            //            subtotal = subtotal + (subtotal*percent)
            items.append("propina")
        }else{
            choosenPercent = 0.0
        }
         
        items.append("servicio")
        items.append("selecionarDireccion")
        items.append("settings")
        items.append("cuponTxt")
        items.append("cuponItm")
        items.append("total")
        items.append("notas")
        print("Valor items ", items.count)
       // self.view.isUserInteractionEnabled = false
        //LoadingOverlay.shared.showOverlay(view: self.view)
        //paymentWS.getPickPalComission(total: Double(subtotal))
    
         total = subtotal  + choosenPercent + picpalService + shoppingCost
        if isSelectTypeService{
        //    self.view.isUserInteractionEnabled = false
            LoadingOverlay.shared.showOverlay(view: self.view)
             calcularServicioPicpal()
        }
        
       
        
    }
    
    func didSuccessGetPickpalService(service: Double) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        comission = service
        
        total = subtotal + service + choosenPercent + picpalService + shoppingCost
        // calcularServicioPicpal()
        
        if isPercent {
            total_code = discount * total
        }else{
            total_code = discount
        }
        total = total - total_code
        if total < 0 {
            total = 0
        }
        totalSecond = total
        
        /*if mMyReward.totalMoney.isLess(than: Double(self.subtotal))  {
            self.mMoneyPoints = mMyReward.totalMoney
        }else{
            self.mMoneyPoints = Double(subtotal)
        }*/
        
        if isUsedPoint{
            
            if descPuntos{
                total = self.total - self.mMoneyPoints
            }
            
            
        }
        tableView.reloadData()
        
        
    }
    func didFailGetPickpalService(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        comission = 0
        total = subtotal + 0 + choosenPercent
        
        if isPercent {
            total_code = discount * total
        }else{
            total_code = discount
        }
        total = total - total_code
        if total < 0 {
            total = 0
        }
        totalSecond = total
        tableView.reloadData()
    }
    func didFailGetPickpalService(service: Double) {
        comission = 0
        total = subtotal + service + choosenPercent
        
        if isPercent {
            total_code = discount * total
        }else{
            total_code = discount
        }
        total = total - total_code
        if total < 0 {
            total = 0
        }
        totalSecond = total
        tableView.reloadData()
    }
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    defaults.set(prices, forKey: "totalPrice")
    //    defaults.set(cantis, forKey: "cantidad")
    //    defaults.set(names, forKey: "item_name")
    func reloadOrders() {
        pricesFinal.removeAll()
        viewDidLoad()
        
    }
    func getSavedOrder() -> [Dictionary<String,Any>] {
        var tempArray = [Dictionary<String,Any>]()
        for i in 0..<pricesFinal.count{
            //            print(order_elements as NSArray)
            let total = pricesFinal[i] * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double)
            totalSecond = total
            var complements = [Dictionary<String,Any>]()
            var modifiers = [Dictionary<String,Any>]()
            if let arrayM = ((order_elements[i] )["complements"]) as? NSDictionary {
                if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                    print(((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_id"] as! [Int]))
                    let comp_names_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_name"] as! [String])
                    let comp_cuantity_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_cant"] as! [Int])
                    let comp_prices_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_price"] as! [Double])
                    let category = ((order_elements[i])["complements"] as! Dictionary<String,Any>)["category"] as! String
                    let comp_id_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_id"] as! [Int])
                    for i in 0 ..< comp_names_array.count{
                        for j in 0 ..< comp_cuantity_array[i]{
                            let comp_dic = ["name": comp_names_array[i], "price": Double(comp_prices_array[i])/Double(comp_cuantity_array[i]), "category" : category, "id": comp_id_array[i]] as [String : Any]
                            complements.append(comp_dic)
                        }
                    }
                }
            }else{
                complements = []
            }
            if let arrayM = ((order_elements[i] )["modifiers"]) as? NSArray {
                
                for i in 0 ..< arrayM.count{
                    
                    let mod_item_dict = arrayM[i] as! NSDictionary
                    let category = mod_item_dict.value(forKey: "category") as! String
                    if (mod_item_dict.value(forKey: "modifier_name") as! NSArray).count > 0 {
                        if let name = (mod_item_dict.value(forKey: "modifier_name") as! NSArray)[0] as? String, let price = (mod_item_dict.value(forKey: "modifier_price") as! NSArray)[0] as? Double, let id = (mod_item_dict.value(forKey: "modifier_id") as! NSArray)[0] as? Int {
                            let mod_dic = ["name": name, "price": price, "id": id, "category": category] as [String : Any]
                            modifiers.append(mod_dic)
                        }
                    }
                }
            }else{
                modifiers = []
            }
            if let isPack = ((order_elements[i] as NSDictionary).value(forKey: "is_package") as? Bool) {
                let dictionary = ["item_name": ((order_elements[i] as NSDictionary).value(forKey: "name") as! String), "all_complements": complements, "all_modifiers": modifiers, "total": total, "quantity": ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Int), "is_package": ((order_elements[i] as NSDictionary).value(forKey: "is_package") as! Bool), "id": ((order_elements[i] as NSDictionary).value(forKey: "id") as! Int)] as [String : Any]
                //                print(dictionary)
                tempArray.append(dictionary)
            }else{
                let dictionary = ["item_name": ((order_elements[i] as NSDictionary).value(forKey: "name") as! String), "all_complements": complements, "all_modifiers": modifiers, "total": total, "quantity": ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Int), "is_package": false, "id": ((order_elements[i] as NSDictionary).value(forKey: "id") as! Int)] as [String : Any]
                //                print(dictionary)
                tempArray.append(dictionary)
            }
            
        }
        return tempArray
        
    }
    @IBAction func `continue`(_ sender: Any) {
        //        print(notes)
        
         
        
     //   if  isSelectTypeService {
        
        if  continueToPay(service: typeService) {
        var tempArray = [Dictionary<String,Any>]()
        for i in 0..<pricesFinal.count{
            //            print(order_elements as NSArray)
            let total = pricesFinal[i] * ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double)
            totalSecond = total
            var complements = [Dictionary<String,Any>]()
            var modifiers = [Dictionary<String,Any>]()
            if let arrayM = ((order_elements[i] )["complements"]) as? NSDictionary {
                if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                    print(((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_id"] as! [Int]))
                    let comp_names_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_name"] as! [String])
                    let comp_cuantity_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_cant"] as! [Int])
                    let comp_prices_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_price"] as! [Double])
                    let category = ((order_elements[i])["complements"] as! Dictionary<String,Any>)["category"] as! String
                    let comp_id_array = ((((order_elements[i] )["complements"]) as! Dictionary<String,Any>)["complement_id"] as! [Int])
                    for i in 0 ..< comp_names_array.count{
                        for j in 0 ..< comp_cuantity_array[i]{
                            let comp_dic = ["name": comp_names_array[i], "price": Double(comp_prices_array[i])/Double(comp_cuantity_array[i]), "category" : category, "id": comp_id_array[i]] as [String : Any]
                            complements.append(comp_dic)
                        }
                    }
                }
            }else{
                complements = []
            }
            print(order_elements)
            if let arrayM = ((order_elements[i] )["modifiers"]) as? NSArray {
                
                for i in 0 ..< arrayM.count{
                    
                    let mod_item_dict = arrayM[i] as! NSDictionary
                    let category = mod_item_dict.value(forKey: "category") as! String
                    //                    print((mod_item_dict.value(forKey: "modifier_price") as! NSArray)[0] as! Double
                    var modifiersString = [String]()
                    var priceModifiers = [Double]()
                    var idModifiers = [Int]()
                    for j in 0..<(mod_item_dict.value(forKey: "modifier_name") as! NSArray).count{
                        modifiersString.append((mod_item_dict.value(forKey: "modifier_name") as! NSArray)[j] as! String)
                        priceModifiers.append((mod_item_dict.value(forKey: "modifier_price") as! NSArray)[j] as! Double)
                        idModifiers.append((mod_item_dict.value(forKey: "modifier_id") as! NSArray)[j] as! Int)
                        
                        let mod_dic = ["name": (mod_item_dict.value(forKey: "modifier_name") as! NSArray)[j] as! String, "price": (mod_item_dict.value(forKey: "modifier_price") as! NSArray)[j] as! Double, "id": (mod_item_dict.value(forKey: "modifier_id") as! NSArray)[j] as! Int, "category": category] as [String : Any]
                        modifiers.append(mod_dic)
                    }
                    
                }
            }else{
                modifiers = []
            }
            if let isPack = ((order_elements[i] as NSDictionary).value(forKey: "is_package") as? Bool) {
                let dictionary = ["item_name": ((order_elements[i] as NSDictionary).value(forKey: "name") as! String), "all_complements": complements, "all_modifiers": modifiers, "total": total, "quantity": ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Int), "note":"", "is_package": ((order_elements[i] as NSDictionary).value(forKey: "is_package") as! Bool), "id": ((order_elements[i] as NSDictionary).value(forKey: "id") as! Int)] as [String : Any]
                //                print(dictionary)
                tempArray.append(dictionary)
            }else{
                let dictionary = ["item_name": ((order_elements[i] as NSDictionary).value(forKey: "name") as! String), "all_complements": complements, "all_modifiers": modifiers, "total": total, "quantity": ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Int), "note":"", "is_package": false, "id": ((order_elements[i] as NSDictionary).value(forKey: "id") as! Int)] as [String : Any]
                //                print(dictionary)
                tempArray.append(dictionary)
            }
            
        }
        print(tempArray)
        //        para obtener el día actual
        let currentDay = Date()
        let formatte = DateFormatter()
        formatte.dateFormat = "yyyy-MM-dd"
        formatte.locale = Locale(identifier: "es_MX")
        let dateFinishHour = formatte.string(from: currentDay)
        let finishTimeDate = dateCreatedSchedule + " " + hourScheduled + ":00"
        //        para pasarlo a formato de Date
        let form = DateFormatter()
        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
        form.locale = Locale(identifier: "es_MX")
        let dateRegister = form.date(from: finishTimeDate)
        let timeInterval = dateRegister?.timeIntervalSince1970 ?? 0 * 1000
        let timeIntervalCurrent = currentDay.timeIntervalSince1970
        print(timeInterval)
        
        
        if let finishTimeDate1 = UserDefaults.standard.object(forKey: "date_saved") as? Date {
            let calendar = Calendar.current
            let date = calendar.date(byAdding:.minute, value: -2, to: currentDay)!
            if date <= finishTimeDate1 {
                if programarPedido {
//                if programarPedido {
                    if hourScheduled != ""  {
                        if timeIntervalCurrent < timeInterval {
                            if subtotal > 3 { // Restriccion de compra el montominivo solicitado por conekta es de 3 pesos
                                
                                if typeService  == "SD"{
                                    
                                    
                                    if isSelectAddress{
                                        
                                        
                                        continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                                        
                                       //  continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                                        
                                    }else{
                                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                                        newXIB.modalTransitionStyle = .crossDissolve
                                        newXIB.modalPresentationStyle = .overCurrentContext
                                        newXIB.errorMessageString = "Aún no haz seleccionado una dirección de envío"
                                        newXIB.subTitleMessageString = "¿A dónde lo llevamos?"
                                        present(newXIB, animated: true, completion: nil)
                                    }
                                    
                                }else{
                                   //  continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                                    
                                    continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                                }
                              
                                
                        
                            }else{
                                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                                newXIB.modalTransitionStyle = .crossDissolve
                                newXIB.modalPresentationStyle = .overCurrentContext
                                newXIB.errorMessageString = "Servicio no disponible"
                                newXIB.subTitleMessageString = "”Programar mi pedido” está disponible únicamente con pagos con TDC/TDD en consumos mayores a $3."
                                present(newXIB, animated: true, completion: nil)
                            }
                        }else{
                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                            newXIB.modalTransitionStyle = .crossDissolve
                            newXIB.modalPresentationStyle = .overCurrentContext
                            newXIB.errorMessageString = "Horario inválido"
                            newXIB.subTitleMessageString = "Selecciona otro horario"
                            present(newXIB, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                        newXIB.modalTransitionStyle = .crossDissolve
                        newXIB.modalPresentationStyle = .overCurrentContext
                        newXIB.errorMessageString = "Escoge un horario"
                        newXIB.subTitleMessageString = "Por favor, escoge un horario para recoger tu pedido"
                        present(newXIB, animated: true, completion: nil)
                    }
                    //                    }else{
                    //                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                    //                        newXIB.modalTransitionStyle = .crossDissolve
                    //                        newXIB.modalPresentationStyle = .overCurrentContext
                    //                        newXIB.errorMessageString = "Sin selección"
                    //                        newXIB.subTitleMessageString = "Selecciona como deseas tu pedido programado."
                    //                        present(newXIB, animated: true, completion: nil)
                    //                    }
                }else{
                   
                    
                   // continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                    
                    continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                        
                 /*   let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                    newVC.paraAqui = !llevar
                    newVC.detail = tempArray
                    newVC.order_elements = order_elements
                    newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    newVC.total = Double(total)
                    newVC.notes = notes
                    newVC.promotionalCode = codeString
                    newVC.pickpalService = comission
                    newVC.discount = Double(total_code)
                    newVC.municipio = municipio
                    newVC.categoria = categoria
                    newVC.lugar = lugar
                    newVC.hourScheduled = ""
                    newVC.isScheduled = isScheduled
                    newVC.paymentType = self.paymentType
                    newVC.inBar = inBar
                    newVC.percent = Double(choosenPercent)
                    newVC.table = tableSelected
                    newVC.hourFancy = hourFancy
                    newVC.dateScheduled = dateSchedule
                    newVC.subtotal = Double(subtotal)
                    newVC.mPoints = Double(mPoints)
                    newVC.mMoneyPoints = Double(mMoneyPoints)
                    self.navigationController?.pushViewController(newVC, animated: true)
                    */
                    print("linea 1019")
                    
                }
            }else{
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Horario inválido"
                newXIB.subTitleMessageString = "Selecciona otro horario"
                present(newXIB, animated: true, completion: nil)
            }
        }else{
            if programarPedido {
                print("El pedido es programado")
                if hourScheduled != "" {
                    if timeIntervalCurrent < timeInterval {
                        if subtotal > 3 {
                         /*   let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                            let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                            newVC.detail = tempArray
                            newVC.paraAqui = !llevar
                            newVC.establishment_id = establishment_id
                            newVC.total = Double(total)
                            newVC.first_time  = true
                            newVC.notes = notes
                            newVC.discount = discount
                            newVC.pickpalService = comission
                            newVC.promotionalCode = codeString
                            newVC.municipio = municipio
                            newVC.categoria = categoria
                            newVC.lugar = lugar
                            newVC.order_elements = order_elements
                            newVC.hourScheduled = hourScheduled
                            newVC.isScheduled = isScheduled
                            newVC.hourScheduledFormatted = hourFancy
                            newVC.inBar = inBar
                            newVC.dateScheduled = dateSchedule
                            newVC.percent = Double(choosenPercent)
                            newVC.table = tableSelected
                            newVC.mPoints = Double(mPoints)
                            newVC.mMoneyPoints = Double(mMoneyPoints)
                            newVC.addressDelivery = self.addressSelect
                            newVC.shipping_cost = self.shoppingCost
                            newVC.type_order = self.typeService
                            newVC.timeDelivery = self.timeDelivery
                            
                            print("Direccion: " ,  self.addressSelect.pk)
                            print("Pickpla: " ,  self.shoppingCost)
                            print("Tipo orden: " ,  self.typeService)
                            
                            self.navigationController?.pushViewController(newVC, animated: true)
                        */
                            
                           // continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                            
                            continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
 
                        }else{
                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                            newXIB.modalTransitionStyle = .crossDissolve
                            newXIB.modalPresentationStyle = .overCurrentContext
                            newXIB.errorMessageString = "Horario inválido"
                            newXIB.subTitleMessageString = "”Programar mi pedido” está disponible únicamente con pagos con TDC/TDD en consumos mayores a $3."
                            present(newXIB, animated: true, completion: nil)
                        }
                        
                    }else{
                        //                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                        //                        newXIB.modalTransitionStyle = .crossDissolve
                        //                        newXIB.modalPresentationStyle = .overCurrentContext
                        //                        newXIB.errorMessageString = "Servicio no disponible"
                        //                        newXIB.subTitleMessageString = "”Programar mi pedido” está disponible únicamente con pagos con TDC/TDD en consumos mayores a $35."
                        //                        present(newXIB, animated: true, completion: nil)
                        
                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                        newXIB.modalTransitionStyle = .crossDissolve
                        newXIB.modalPresentationStyle = .overCurrentContext
                        newXIB.errorMessageString = "Horario inválido"
                        newXIB.subTitleMessageString = "Selecciona otro horario"
                        present(newXIB, animated: true, completion: nil)
                    }
                }else{
                    //                    if selectedOptionsProgrammed {
                    //                        if hourScheduled == "" {
                    let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.errorMessageString = "Escoge un horario"
                    newXIB.subTitleMessageString = "Por favor, escoge un horario para recoger tu pedido"
                    present(newXIB, animated: true, completion: nil)
                    //                        }
                    //                    }else{
                    //                        if hourScheduled == "" {
                    //                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                    //                            newXIB.modalTransitionStyle = .crossDissolve
                    //                            newXIB.modalPresentationStyle = .overCurrentContext
                    //                            newXIB.errorMessageString = "Sin selección"
                    //                            newXIB.subTitleMessageString = "Selecciona como deseas tu pedido y el horario de entrega."
                    //                            present(newXIB, animated: true, completion: nil)
                    //
                    //                        }else{
                    //                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                    //                            newXIB.modalTransitionStyle = .crossDissolve
                    //                            newXIB.modalPresentationStyle = .overCurrentContext
                    //                            newXIB.errorMessageString = "Sin selección"
                    //                            newXIB.subTitleMessageString = "Selecciona como deseas tu pedido programado."
                    //                            present(newXIB, animated: true, completion: nil)
                    //                        }
                    //
                    //                    }
                }
            }else{
                if inBar {
                    if validPercent <= 0.3 {
 
                            continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                        
                    }else{
                        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                        newXIB.modalTransitionStyle = .crossDissolve
                        newXIB.modalPresentationStyle = .overCurrentContext
                        newXIB.errorMessageString = "Valor inválido"
                        newXIB.subTitleMessageString = "Puede dejar un máximo de hasta 30% en propina."
                        present(newXIB, animated: true, completion: nil)
                    }
                }else{
                    if typeService  == "SM" {
                        if tableSelected > 0 {
//                            if subtotal > 34 {
                               /* let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                                let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                                newVC.detail = tempArray
                                newVC.paraAqui = !llevar
                                newVC.establishment_id = establishment_id
                                newVC.total = Double(total)
                                newVC.first_time  = true
                                newVC.notes = notes
                                newVC.discount = discount
                                newVC.pickpalService = comission
                                newVC.promotionalCode = codeString
                                newVC.municipio = municipio
                                newVC.categoria = categoria
                                newVC.lugar = lugar
                                newVC.order_elements = order_elements
                                newVC.hourScheduled = hourScheduled
                                newVC.isScheduled = isScheduled
                                newVC.inBar = inBar
                                newVC.hourScheduledFormatted = hourFancy
                                newVC.dateScheduled = dateSchedule
                                newVC.percent = Double(choosenPercent)
                                newVC.table = tableSelected
                                newVC.mPoints = Double(mPoints)
                                newVC.mMoneyPoints = Double(mMoneyPoints)
                                newVC.addressDelivery = self.addressSelect
                                newVC.shipping_cost = self.shoppingCost
                                newVC.type_order = self.typeService
                                newVC.timeDelivery = self.timeDelivery
                                
                                
                                self.navigationController?.pushViewController(newVC, animated: true)*/
                                
                              //  continueToPaymentMethod(typePay: 2, tempArray: tempArray)
                                
                                continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                                
//                            }else{
//                                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//                                newXIB.modalTransitionStyle = .crossDissolve
//                                newXIB.modalPresentationStyle = .overCurrentContext
//                                newXIB.errorMessageString = "Servicio no disponible"
//                                newXIB.subTitleMessageString = "Pagos con TDC/TDD únicamente en consumos mayores a $35."
//                                present(newXIB, animated: true, completion: nil)
//                            }
                            
                        }else{
                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                            newXIB.modalTransitionStyle = .crossDissolve
                            newXIB.modalPresentationStyle = .overCurrentContext
                            newXIB.errorMessageString = "Sin selección"
                            newXIB.subTitleMessageString = "Selecciona una mesa para continuar."
                            present(newXIB, animated: true, completion: nil)
                            
                        }
                    }else{
                        
                        
                      /*  let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                        newVC.paraAqui = !llevar
                        newVC.detail = tempArray
                        newVC.order_elements = order_elements
                        newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                        newVC.total = Double(total)
                        newVC.notes = notes
                        newVC.promotionalCode = codeString
                        newVC.pickpalService = comission
                        newVC.discount = Double(total_code)
                        newVC.municipio = municipio
                        newVC.categoria = categoria
                        newVC.lugar = lugar
                        newVC.hourScheduled = ""
                        newVC.isScheduled = isScheduled
                        newVC.paymentType = self.paymentType
                        newVC.inBar = inBar
                        newVC.percent = Double(choosenPercent)
                        newVC.table = tableSelected
                        newVC.hourFancy = hourFancy
                        newVC.dateScheduled = dateSchedule
                        newVC.subtotal = Double(subtotal)
                        newVC.mPoints = Double(mPoints)
                        newVC.mMoneyPoints = Double(mMoneyPoints)
                        newVC.addressDelivery = self.addressSelect
                        newVC.shipping_cost = self.shoppingCost
                        newVC.type_order = self.typeService
                        newVC.timeDelivery = self.timeDelivery*/
                        
                        if typeService  == "SD"{
                            
                            if isSelectAddress{
                            
                                
//                                if subtotal > 34 {
                                    
                                    
                                  /*  let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                                                           let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                                                           newVC.paraAqui = !llevar
                                                           newVC.detail = tempArray
                                                           newVC.order_elements = order_elements
                                                           newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                                                           newVC.total = Double(total)
                                                           newVC.notes = notes
                                                           newVC.promotionalCode = codeString
                                                           newVC.pickpalService = comission
                                                           newVC.discount = Double(total_code)
                                                           newVC.municipio = municipio
                                                           newVC.categoria = categoria
                                                           newVC.lugar = lugar
                                                           newVC.hourScheduled = ""
                                                           newVC.isScheduled = isScheduled
                                                           newVC.paymentType = self.paymentType
                                                           newVC.inBar = inBar
                                                           newVC.percent = Double(choosenPercent)
                                                           newVC.table = tableSelected
                                                           newVC.hourFancy = hourFancy
                                                           newVC.dateScheduled = dateSchedule
                                                           newVC.subtotal = Double(subtotal)
                                                           newVC.mPoints = Double(mPoints)
                                                           newVC.mMoneyPoints = Double(mMoneyPoints)
                                                           newVC.addressDelivery = self.addressSelect
                                                           newVC.shipping_cost = self.shoppingCost
                                                           newVC.type_order = self.typeService
                                                           newVC.timeDelivery = self.timeDelivery
                                    
                                    
                                    self.navigationController?.pushViewController(newVC, animated: true)*/
                                  //  continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                                    
                                    continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                                    
//                                }else{
//                                    let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//                                    newXIB.modalTransitionStyle = .crossDissolve
//                                    newXIB.modalPresentationStyle = .overCurrentContext
//                                    newXIB.errorMessageString = "Servicio no disponible"
//                                    newXIB.subTitleMessageString = "Pagos con TDC/TDD únicamente en consumos mayores a $35."
//                                    present(newXIB, animated: true, completion: nil)
//                                }
                            
                                //self.navigationController?.pushViewController(newVC, animated: true)
                                
                            }else{
                                
                                  let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                                               newXIB.modalTransitionStyle = .crossDissolve
                                               newXIB.modalPresentationStyle = .overCurrentContext
                                               newXIB.errorMessageString = "Aún no haz seleccionado una dirección de envío"
                                               newXIB.subTitleMessageString = "¿A dónde lo llevamos?"
                                               present(newXIB, animated: true, completion: nil)
                            }
                            
                            
                        }else{
                            
                           // continueToPaymentMethod(typePay: mInvoincing.payment_methods, tempArray: tempArray)
                            
                            continueToPaymentMethod(typePay: order_payments.value(forKey: typeService) as! Int, tempArray: tempArray)
                            
                              //  self.navigationController?.pushViewController(newVC, animated: true)
                            
                        }
                        
                    }
                    
                }
                
            }
        }
        
        }else{
            
            
      let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
          newXIB.modalTransitionStyle = .crossDissolve
          newXIB.modalPresentationStyle = .overCurrentContext
          newXIB.errorMessageString = "Sin selección"
          newXIB.subTitleMessageString = "Aún no haz seleccionado como deseas tu pedido"
          present(newXIB, animated: true, completion: nil)
            
        }
    }
  /*  override func viewWillAppear(_ animated: Bool) {
        //        tableSelected = 0
        //        inBar = false
        //        llevar = false
        //        hourScheduled = ""
        //        isScheduled = false
        //        percent = 0.0
        //        defaultReward = rewardSaved
        //        percentValid = false
        //        tableView.reloadData()
    }*/
    override func viewWillDisappear(_ animated: Bool) {
        defaults.removeObject(forKey: "screen")
        defaults.removeObject(forKey: "storyboard")
        if itemsObserve != nil {
            itemsObserve.removeObserver(withHandle: handle)
        }
        if extrasObserve != nil {
            extrasObserve.removeObserver(withHandle: handle2)
        }
        if modifiersObserve != nil {
            modifiersObserve.removeObserver(withHandle: handle3)
        }
    }
    func inactivateItemsObserve(items: [Dictionary<String,Any>]) {
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        itemsObserve = Database.database().reference().child("inactive_items").child("-\(establishmentId)").child("items")
       
        handle = itemsObserve.observe(.value) { (snapshot) in
            var itemsId = [Int]()
            for item in items {
                itemsId.append(item["id"]! as! Int)
            }
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var isPackage = [Bool]()
                for id in itemsId {
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                        idsInactivated.append(id)
                        let val = (snapshot.value as! NSDictionary).value(forKey: "\(id)") as! NSDictionary
                        let isCombo = val.value(forKey: "is_combo") as! Bool
                        isPackage.append(isCombo)
                    }
                }
                if idsInactivated.count > 0 {
                    let newXIB = DeleteCartViewController(nibName: "DeleteCartViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.delegate = self
                    newXIB.textString = "\(DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)) ha agotado algunos platillos y/o bebidas"
                    newXIB.titleString = "Tu carrito será eliminado"
                    newXIB.idProduct = idsInactivated
                    newXIB.isCombo = isPackage
                    newXIB.establishment_id = establishmentId
                    newXIB.municipio = self.municipio
                    newXIB.categoria = self.categoria
                    newXIB.lugar = self.lugar
                    newXIB.estName = self.esablishmentName
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    func inactivateExtrasObserve(items: [Dictionary<String,Any>]) {
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        extrasObserve = Database.database().reference().child("inactive_extras").child("-\(establishmentId)")
      
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle2 = extrasObserve.observe(.value) { (snapshot) in
            var extrasId = [Int]()
            for item in items {
                if let complements = item["complements"] as? NSDictionary {
                    if let complement_id = complements.value(forKey: "complement_id") as? NSArray {
                        for compl in complement_id {
                            extrasId.append(compl as! Int)
                        }
                    }
                }
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                    }
                }
            }
           
            if !(snapshot.value is NSNull) {
                print(snapshot.value as! NSDictionary)
                var idsInactivated = [Int]()
                for id in extrasId {
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        idsInactivated.append(id2)
                    }
                }
                if idsInactivated.count > 0 {
                    let newXIB = DeleteCartViewController(nibName: "DeleteCartViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.delegate = self
                    newXIB.textString = "\(DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)) ha agotado algunos platillos y/o bebidas"
                    newXIB.titleString = "Tu carrito será eliminado"
                    newXIB.idProduct = idsInactivated
                    newXIB.isCombo = []
                    newXIB.establishment_id = establishmentId
                    newXIB.municipio = self.municipio
                    newXIB.categoria = self.categoria
                    newXIB.lugar = self.lugar
                    newXIB.estName = self.esablishmentName
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    func inactivateModifiersObserve(items: [Dictionary<String,Any>]) {
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        modifiersObserve = Database.database().reference().child("inactive_modifiers").child("-\(establishmentId)")
        print(modifiersObserve)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle3 = modifiersObserve.observe(.value) { (snapshot) in
            //            print(snapshot.value as! NSDictionary)
            var extrasId = [Int]()
            for item in items {
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                    }
                }
            }
            print(extrasId)
            if !(snapshot.value is NSNull) {
                print(snapshot.value as! NSDictionary)
                var idsInactivated = [Int]()
                for id in extrasId {
                    if (snapshot.value as! NSDictionary).value(forKey: "-\(id)") != nil {
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "-\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        idsInactivated.append(id2)
                    }
                }
                if idsInactivated.count > 0 {
                    let newXIB = DeleteCartViewController(nibName: "DeleteCartViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.delegate = self
                    newXIB.textString = "\(DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)) ha agotado algunos platillos y/o bebidas"
                    newXIB.titleString = "Tu carrito será eliminado"
                    newXIB.idProduct = idsInactivated
                    newXIB.isCombo = []
                    newXIB.establishment_id = establishmentId
                    newXIB.municipio = self.municipio
                    newXIB.categoria = self.categoria
                    newXIB.lugar = self.lugar
                    newXIB.estName = self.esablishmentName
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    func deleteCarrito(showView: Bool, stb_id: Int, municipio: String, categoria: String, lugar: String, estbName: String, stayInCart: Bool) {
        //        print("delete carrito")
        
        let str = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        let encoded = str.data(using: .utf8)
        let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
        if let order_elements = dictionary as? [Dictionary<String,Any>] {//UserDefaults.standard.object(forKey: "order_elements") as? [Dictionary<String, Any>] {
            pricesFinal.removeAll()
            self.order_elements = order_elements
            //            print(order_elements)
            for i in 0..<order_elements.count{
                var prices_complements = 0.0
                var tempPriceArray = [Double]()
                if let arrayM = ((order_elements[i] )["complements"]) as? NSDictionary {
                    //                    print(((order_elements[i] )["complements"]) as? NSDictionary)
                    if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                        for price in arrayM.value(forKey: "complement_price") as! [Double] {
                            tempPriceArray.append(price)
                        }
                    }
                }
                if let arrayM = ((order_elements[i] )["modifiers"]) as? NSArray {
                    //                    print(arrayM)
                    for modif in arrayM {
                        //                        print(modif)
                        if let arr = (modif as! NSDictionary).value(forKey: "modifier_price") as? NSArray {
                            for tempM in arr {
                                //                                print(tempM)
                                tempPriceArray.append(tempM as! Double)
                            }
                            
                        }
                        
                    }
                }
                
                for p in tempPriceArray{
                    prices_complements += p
                }
                
                //                print(((order_elements[i] as NSDictionary).value(forKey: "price") as! Double),prices_complements,((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double) , "price")
                pricesFinal.append((((order_elements[i] as NSDictionary).value(forKey: "price") as! Double) + prices_complements) / ((order_elements[i] as NSDictionary).value(forKey: "quantity") as! Double))
                
                //                print(pricesFinal, "pricesFinal")
                
            }
            calculateprice(prices: pricesFinal, discount: 0)
        }else{
            //            print("no hay nada")
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = stb_id
            newVC.municipioStr = municipio
            newVC.categoriaStr = categoria
            newVC.lugarString = lugar
            newVC.establString = estbName
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        //        self.navigationController?.popViewController(animated: true)
        
        
    }
    func didSuccessRegisterCode(codigo_pk: Int, discount: Double, type_discount: Int, type_discount_string: String) {
        LoadingOverlay.shared.hideOverlayView()
        applied_Code = true
        if type_discount_string == "Efectivo" {
            isPercent = false
        }else{
            isPercent = true
        }
        
        self.discount = discount
        
        calculateprice(prices: pricesFinal, discount: discount)
    }
    func didSuccessRegisterCode(codigo_pk: Int, discount: Double) {
        LoadingOverlay.shared.hideOverlayView()
        applied_Code = true
        total_code = discount
        calculateprice(prices: pricesFinal, discount: discount)
    }
    
    func didFailRegisterCode(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        codeString = ""
        let alert = UIAlertController(title: "PickPal", message: "\(subtitle)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func didSuccessGetPickPalComision(comision: Double) {
        comission = comision
        calculateprice(prices: pricesFinal, discount: Double(total_code))
    }
    func didFailGetPickPalComision(title: String, subtitle: String) {
        comission = 0
    }
    func finishInput(code: String) {
        codeString = code
        if code != ""{
            LoadingOverlay.shared.showOverlay(view: self.view)
            paymentWS.registerCode(client_id: client_id, code: code)
        }
    }
    
    @IBAction func paraAqui(_ sender: Any) {
        if !llevar{
            backroundLlevar.image = UIImage(named: "borderGrayLeft")
            backgroundAqui.image = UIImage(named: "greenBackgroundRight")
            txtLlevar.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
            txtAqui.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
          //  llevar = true
            palomitaParaLlevar.alpha = 0
            palomitaParaComer.alpha = 1
        }
    }
    
    @IBAction func paraLlevar(_ sender: Any) {
        if llevar{
            backroundLlevar.image = UIImage(named: "greenBackground")
            backgroundAqui.image = UIImage(named: "borderGray")
            txtAqui.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
            txtLlevar.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            llevar = false
            palomitaParaLlevar.alpha = 1
            palomitaParaComer.alpha = 0
        }
    }
    
    func addItem(name: String, cantidad: Int, position: Int) {
        order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) + 1, forKey: "quantity")
        order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Double) * pricesFinal[position], forKey: "price")
        let id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        let name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
        
        DatabaseFunctions.shared.insertUpdateNewItemCart(establishment_id: id, establishment_name: name, in_site: false, notes: notes, discount_code: "", total: Double(total), pickpal_comission: comission, total_after_checkout: Double(total), order_has_been_sent: false, order_elements: order_elements)
        calculateprice(prices: pricesFinal, discount: Double(total_code))
        tableView.reloadData()
    }
    
    func substractItem(name: String, cantidad: Int, position: Int) {
        
        // SI LA CANTIDAD DE PRODUCTOS == 0 SE ELIMINA LA TARJETA
        if(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) - 1 == 0){
            //            if arrayNotes.count > 0 {
            //                arrayNotes.remove(at: position)
            //            }
            order_elements.remove(at: position)
            pricesFinal.remove(at: position)
            defaults.set(arrayNotes, forKey: "notas")
            //            defaults.set(order_elements, forKey: "order_elements")
            let id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            let name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
            DatabaseFunctions.shared.insertUpdateNewItemCart(establishment_id: id, establishment_name: name, in_site: false, notes: notes, discount_code: "", total: Double(total), pickpal_comission: comission, total_after_checkout: Double(total), order_has_been_sent: false, order_elements: order_elements)
            
            //SI ERA EL UNICO PRODUCTO VUELVE A LA PANTALLA ANTERIROR SINO CALCULA EL PRECIO
            if order_elements.count == 0{
                DatabaseFunctions.shared.deleteItem()
                self.navigationController?.popViewController(animated: true)
            }else{
                calculateprice(prices: pricesFinal, discount: 0)
            }
        }else{
            
            // SI LA CANTIDAD ES MAYOR A UNO EL PROCESO ES EL EL MIMSO
//            if (((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) - 1) > 1{
//
//
//                order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) - 1, forKey: "quantity")
//
//                order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Double) * pricesFinal[position], forKey: "price")
//
//
//            }else{
                
                if prices_complementsArray.count > 0 {
//
//                    let numberComplements = (((order_elements[position] as NSDictionary).value(forKey: "complements") as! NSDictionary).value(forKey: "complement_cant") as! [Int])
                   
                    let newPrece =  pricesFinal[position] - prices_complementsArray[0]
                    
                    order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) - 1, forKey: "quantity")
                    order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Double) * newPrece, forKey: "price")
                    
                }else{
                    
                    
                    order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int) - 1, forKey: "quantity")
                    
                    order_elements[position].updateValue(((order_elements[position] as NSDictionary).value(forKey: "quantity") as! Double) * pricesFinal[position] , forKey: "price")
                    
                }
                
                
//            }
            
            
            //            defaults.set(order_elements, forKey: "order_elements")
            let id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            let name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
            //            DatabaseFunctions.shared.insertUpdateNewItemCart(establishment_id: id, establishment_name: name, in_site: false, notes: notes, billing_address_id: 0, payment_method: 0, discount_code: "", total: total, estimated_time: 0, pickpal_comission: comission, total_after_checkout: total + comission, order_has_been_sent: false, code: "", order_number: "", folio: "", register_date: "", process_time: "", ready_time: "", pay_time: "", order_id: 0, order_elements: order_elements)
            DatabaseFunctions.shared.insertUpdateNewItemCart(establishment_id: id, establishment_name: name, in_site: false, notes: notes, discount_code: "", total: Double(total), pickpal_comission: comission, total_after_checkout: Double(total), order_has_been_sent: false, order_elements: order_elements)
            calculateprice(prices: pricesFinal, discount: 0)
           
        }
        
        // SI AUN HAY ELEMENTOS SE RECARGA LA LISTA
        if order_elements.count != 0{
         tableView.reloadData()
        }
        
        
        
    }
    func editModifiers(position: Int) {
        let newXIB = editComplexItemsViewController(nibName: "editComplexItemsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        //        print(order_elements[position])
        newXIB.order_elements = order_elements[position]
        newXIB.itemEdited = position
        newXIB.dictionaryInfo = order_elements
        newXIB.cantidadItems = (order_elements[position] as NSDictionary).value(forKey: "quantity") as! Int
        newXIB.delegate = self
        present(newXIB, animated: true, completion: {
        })
    }
    func getEmbeededTableViewHeight() -> CGFloat{
        var value = CGFloat()
        if items.contains("header") {
            value += 44
        }
        for order in order_elements {
            if ((order as NSDictionary).value(forKey: "complex") as! Bool){
                var extra = 0
                if let arrayM = ((order )["complements"]) as? NSDictionary {
                    if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                        for _ in 0..<((((order )["complements"]) as! Dictionary<String,Any>)["complement_name"] as! [String]).count{
                            extra += 20
                        }
                    }
                }
                if let arrayM = ((order )["modifiers"]) as? NSArray {
                    for modif in arrayM {
                        let array = (modif as! NSDictionary).value(forKey: "modifier_name") as! NSArray
                        for _ in 0..<array.count {
                            extra += 20
                        }
                    }
                }
                //                return CGFloat(70 + (extra + 20))
                print(CGFloat(70 + (extra + 20)))
                value += CGFloat(70 + (extra + 20))
            }else{
                value += 55
            }
        }
        if items.contains("subtotal"){
            value +=  40
        }
        if items.contains("servicio") {
            value +=  40
        }
        if items.contains("cuponTxt") {
            value +=  45
//             value +=  CGFloat(optionsSize)
        }
        
        if items.contains("notas" ){
            value +=  92
        }
        if items.contains("selecionarDireccion" ){
            value +=   CGFloat(direccionSize)
        }
        if items.contains("settings") {
            value +=  CGFloat(optionsSize)
        }
        if items.contains("propina"){
            value +=  40
        }
        
        if items.contains("total"){
             if mMyReward.totalPoints < 0{
                value +=  55
             }else{
                value +=  200
            }
        }
        print(value)
        return value
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension CarritoViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if percent == 0.0 {
        //            return order_elements.count + 8
        //        }else{
        //            return order_elements.count + 9
        //        }
        return items.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if(indexPath.row == 0){
        
        print("valor dinamicHeightProducts \(dinamicHeightProducts)")
        
        if items[indexPath.row] == "filtroPedidos" {
            return     CGFloat(filtroSize)
        }else if items[indexPath.row] == "header" {
            return 44
            //        }else if(indexPath.row < (order_elements.count + 1) && indexPath.row != 0){
        }else if items[indexPath.row] == "productos" {
            print("valor indexPath \(indexPath.row) restando \(indexPath.row  - 2)  tamaño \(order_elements.count) ")
            
        //   var orderElement =  order_elements[indexPath.row - 2]
            
            if order_elements.count > 0 && ((order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "complex") as! Bool){
                var extra = 0
                if let arrayM = ((order_elements[indexPath.row - 2] )["complements"]) as? NSDictionary {
                    if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                        for _ in 0..<((((order_elements[indexPath.row - 2] )["complements"]) as! Dictionary<String,Any>)["complement_name"] as! [String]).count{
                            extra += 20
                        }
                    }
                }
                if let arrayM = ((order_elements[indexPath.row - 2] )["modifiers"]) as? NSArray {
                    for modif in arrayM {
                        let array = (modif as! NSDictionary).value(forKey: "modifier_name") as! NSArray
                        for _ in 0..<array.count {
                            extra += 20
                        }
                    }
                }
                return CGFloat(70 + (extra + 20))
            }else{
                return 70
            }
            //        }else if(indexPath.row == order_elements.count){
        }else if items[indexPath.row] == "subtotal"{
            return 40
            //        }else if(indexPath.row == (order_elements.count + 1)){
        }else if items[indexPath.row] == "servicio" {
            return 40
            //        }else if(indexPath.row == (order_elements.count + 2)){
        }else if items[indexPath.row] == "cuponTxt" {
            return 45
            //        }else if(indexPath.row == (order_elements.count + 6)){
        }else if items[indexPath.row] == "notas" {
            return 92
            //        }else if indexPath.row == (order_elements.count + 7) {
        }else if items[indexPath.row] == "selecionarDireccion" {
            return     CGFloat(direccionSize)
        }else if items[indexPath.row] == "settings" {
            return     CGFloat(optionsSize)
        }else if items[indexPath.row] == "total"{
            if mMyReward.totalPoints < 0{
                return 55
             }else{
                return 200
            }
        }else{
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if(indexPath.row == 0){
        
        if items[indexPath.row] == "filtroPedidos" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "filtroPedidos", for: indexPath)  as! FiltroPedidos
            cell.delegate = self
            if dateSchedule.isEmpty {
                    let date = Date()
                    let form = DateFormatter()
                    form.dateFormat = "dd/MM/yyyy"
                    let dateString = form.string(from: date)
                
                print("Fecha ", dateString)
                        cell.currentDate.text = dateString
                }else{
                    cell.currentDate.text = dateSchedule
                }
                cell.timeLabel.text = hourFancy
//                cell.valueLabel.isHidden = true
            if defualtItem{
                ordersws.getServicesPickPal(total: subtotal, type_serivice: filterDefaut)
                cell.itemSelect = filterDefaut
                defualtItem = false
            }
            
            return cell
            //        }else if(indexPath.row < order_elements.count + 1 && indexPath.row != 0){
        }else
        
        if items[indexPath.row] == "header" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerPedidos", for: indexPath)
            return cell
            //        }else if(indexPath.row < order_elements.count + 1 && indexPath.row != 0){
        }else if items[indexPath.row] == "productos"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemPedidos", for: indexPath) as! ItemPedidosTableViewCell
            cell.delegate = self
            
            dinamicHeightProducts = cell.nombre.frame.height + 12
            
            
            cell.nombre.text = ((order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "name") as! String)
            let tempPrcie = pricesFinal[indexPath.row - 2] * ((order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "quantity") as! Double)
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(tempPrcie.description)! as NSNumber) {
                cell.precio.text = "\(formattedTipAmount)"
            }
            
            //cell.precio.text = "$" + tempPrcie.description + "0"
            cell.cantidad.text = ((order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "quantity") as! Int).description
            cell.name = (order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "name") as! String
            cell.cuantiti = (order_elements[indexPath.row - 2] as NSDictionary).value(forKey: "quantity") as! Int
            cell.position = indexPath.row - 2
            var tempNameArray = [String]()
            var tempPriceArray = [Double]()
            var tempCantArray = [Int]()
            var tempCatArray = [String]()
            if let _ = ((order_elements[indexPath.row - 2] )["complements"]) as? NSDictionary, let _ = ((order_elements[indexPath.row - 2] )["modifiers"]) as? NSArray {
                tempCantArray.append(1)
                tempNameArray.append(order_elements[indexPath.row - 2]["name"] as! String)
//                tempPriceArray.append(order_elements[indexPath.row - 2]["price"] as! Double / Double(cell.cuantiti))
                tempPriceArray.append(order_elements[indexPath.row - 2]["price_product"] as! Double )
                tempCatArray.append("Producto")
            }else{
                if let _ = ((order_elements[indexPath.row - 2] )["complements"]) as? NSDictionary {
                    tempCantArray.append(1)
                    tempNameArray.append(order_elements[indexPath.row - 2]["name"] as! String)
                    tempPriceArray.append(order_elements[indexPath.row - 2]["price"] as! Double / Double(cell.cuantiti))
                    tempCatArray.append("Producto")
                }else{
                    if let _ = ((order_elements[indexPath.row - 2] )["modifiers"]) as? NSArray {
                        tempCantArray.append(1)
                        tempNameArray.append(order_elements[indexPath.row - 2]["name"] as! String)
                        tempPriceArray.append(order_elements[indexPath.row - 2]["price"] as! Double / Double(cell.cuantiti))
                        tempCatArray.append("Producto")
                    }
                }
            }
            
            if let arrayM = ((order_elements[indexPath.row - 2] )["complements"]) as? NSDictionary {
                print(arrayM)
                if (arrayM.value(forKey: "complement_name") as? NSArray) != nil {
                    for name in ((((order_elements[indexPath.row - 2] )["complements"]) as! Dictionary<String,Any>)["complement_name"] as! [String]){
                        tempNameArray.append(name)
                        if let category = ((((order_elements[indexPath.row - 2] )["complements"]) as! Dictionary<String,Any>)["category"] as? String) {
                            tempCatArray.append(category)
                        }
                    }
                    for cant in ((((order_elements[indexPath.row - 2] )["complements"]) as! Dictionary<String,Any>)["complement_cant"] as! [Int]){
                        tempCantArray.append(cant)
                    }
                    for price in ((((order_elements[indexPath.row - 2] )["complements"]) as! Dictionary<String,Any>)["complement_price"] as! [Double]){
                        tempPriceArray.append(price)
                    }
                    
                }
            }
            if let arrayM = ((order_elements[indexPath.row - 2] )["modifiers"]) as? NSArray {
                print(arrayM)
                //                if let modifier_category = (arrayM as! NSDictionary).value(forKey: "category") as? String{
                //                    tempCatArray.append(modifier_category)
                //
                //                }
                for modifiers in arrayM {
                    print(modifiers)
                    if let modifier_name = (modifiers as! NSDictionary).value(forKey: "modifier_name") as? [String] {
                        for name in modifier_name {
                            tempNameArray.append(name)
                            if let modifier_category = (modifiers as! NSDictionary).value(forKey: "category") as? String{
                                tempCatArray.append(modifier_category)
                                
                            }
                        }
                    }
                    if let modifier_price = (modifiers as! NSDictionary).value(forKey: "modifier_price") as? [Double] {
                        for cant in modifier_price{
                            tempPriceArray.append(cant)
                            tempCantArray.append(1)
                            
                        }
                    }
                    
                }
            }
            cell.modifier_name = tempNameArray
            cell.modifier_cant =  tempCantArray
            cell.modifier_price = tempPriceArray
            print(tempCatArray)
            cell.modifier_category = tempCatArray
            cell.tableView.reloadData()
            return cell
            //        }else if(indexPath.row == order_elements.count + 1){
        }else if items[indexPath.row] == "subtotal" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subTotalPedidos", for: indexPath) as! SubTotalPedidosTableViewCell
            //cell.subtotal.text = "$" +  subtotal.description + "0"
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(subtotal.description)! as NSNumber) {
                cell.subtotal.text = "\(formattedTipAmount)"
            }
            cell.subtotalLabel.text = "Subtotal"
            return cell
            //        }else if(indexPath.row == (order_elements.count + 2)){
        }else if items[indexPath.row] == "propina" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServicioPedidos", for: indexPath) as! ServicioPedidosTableViewCell
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            let porcentaje = Int(percent * 100)
            if let formattedTipAmount = formatter.string(from: (Double(subtotal.description)! * percent) as NSNumber) {
                cell.txt.text = "\(porcentaje)%   \(formattedTipAmount)"
            }
            cell.textInfo.text = "Propina"
            return cell
            //        }else if(indexPath.row == (order_elements.count + 4)){
        }else if items[indexPath.row] == "servicio" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServicioPedidos", for: indexPath) as! ServicioPedidosTableViewCell
            let formatter = NumberFormatter()
             cell.txt.text = "0.0"
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            //if let formattedTipAmount = formatter.string(from: Double(comission) as NSNumber) {
            if let formattedTipAmount = formatter.string(from: Double(picpalService) as NSNumber) {
                cell.txt.text = "\(formattedTipAmount)"
            }
            //cell.txt.text = "$\(comission)0"
            cell.textInfo.text = "Servicio de PickPal"
            return cell
            //        }else if(indexPath.row == (order_elements.count + 3)){
        }else if items[indexPath.row] == "cuponTxt"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtCuponPedidos", for: indexPath)
            return cell
            //        }else if(indexPath.row == (order_elements.count + 5)){
        }else if items[indexPath.row] == "cuponItm"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "inputCuponPedidos", for: indexPath) as! InputCuponPedidosTableViewCell
            cell.delegate = self
            if applied_Code{
                cell.isUserInteractionEnabled = false
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: Double(total_code.description)! as NSNumber) {
                    cell.txt.text = "\(formattedTipAmount)"
                }
                cell.txt.alpha = 1
            }else{
                cell.txt.alpha = 0
            }
            return cell
        }else if items[indexPath.row] == "total"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalPedidos", for: indexPath) as! TotalPedidosTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            
            let totalCash = CustomsFuncs.getFomatMoney(currentMoney: auxPointMoney )
           // let totalCash = CustomsFuncs.getFomatMoney(currentMoney: self.mMoneyPoints )
            let totalAux = totalCash != "" ? totalCash : "$" + String(auxPointMoney )
            let pointsAndMoney = "\(CustomsFuncs.numberFormat(value: total_amount)) pts / \(totalAux)"
            
            cell.lblTotalPoints.attributedText = CustomsFuncs.getAttributedString(title1: "Tienes: ", title2: pointsAndMoney)
            
            cell.btnUserPoints.isUserInteractionEnabled = auxPointMoney > 0
            
            if isUsedPoint{
                cell.imgCheckNo.image = UIImage(named: "emptyRoundedCheckBox")
                cell.imgCheckYes.image = UIImage(named: "fillRoundedCheckBox")
                cell.total.text = CustomsFuncs.getFomatMoney(currentMoney: Double(self.total))
                descPuntos = true
            }else{
                cell.imgCheckNo.image = UIImage(named: "fillRoundedCheckBox")
                cell.imgCheckYes.image = UIImage(named: "emptyRoundedCheckBox")
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US")
                formatter.numberStyle = .currency
                
                if let formattedTipAmount = formatter.string(from: (Double(total.description)!) as NSNumber) {
                    totalSecond = Double(total.description)!
                    print("totalSecond \(totalSecond)")
                    cell.total.text = "\(formattedTipAmount)"
                }
            }
            
            if mMyReward.totalPoints < 0{
                cell.viewContainerRadioButtons.isHidden = true
                cell.lblQuestionPayPoints.isHidden = true
                cell.lblTotalPoints.isHidden = true
                cell.clTopViewDivider.constant = (-cell.layer.frame.height) + 50
                cell.viewDivisorHeaderAux.isHidden = true
            }
            
            return cell
            
        }else if items[indexPath.row] == "notas"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "notasPedido", for: indexPath) as! NotasPedidoTableViewCell
            var tempNotes = ""
            cell.delegate = self
            if notesTemp == "" {
                cell.notes.text = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
            }else{
                cell.notes.text = notesTemp
            }

            return cell
        }else
        
            if items[indexPath.row] == "selecionarDireccion"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "selecionarDireccion", for: indexPath) as! SelectAddressTableViewCell
                cell.delegate = self
               
                
                if(isSelectAddress){
                    cell.containerInfoAddress.visibility = .visible
                    cell.lbCostoEnvio.text =   "\(CustomsFuncs.getFomatMoney(currentMoney:self.shoppingCost))"
                    cell.labelAdressSelecte.text = "\(addressSelect.street!) # \(addressSelect.numeroExterior!),  \(addressSelect.colony!), \(addressSelect.state!), C.P. \(addressSelect.zip_code ?? "") "
                   
                     direccionSize = 370
                    
                   
                    
                }else{
                    
                    if cell.itemSelect == 15{
                        cell.porcent = defaultReward
                    }
                        
                }
                
                cell.cllectionPropinaRepartidor.reloadData()
                return cell
            }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "pedidoSettings", for: indexPath) as! OrderTypeTableViewCell
            cell.selectionStyle = .none
            cell.numberOfOptions = self.carryModeArray
            cell.paymentType = self.paymentType
            cell.tipoPedido = self.typeService
            cell.delegate = self
            cell.selectedOptions = carryModeArrayBool
            cell.isActive = self.isActive
            cell.bandera = true
                
            // Servicio de cliente
            cell.visibilitySM = visibilityButtomSM
            cell.lblNumberTabletClient.visibility = tableSelected > 0 ? .visible: .gone
//            cell.lblNumberTabletClient.text = "Fuiste asignado a la mesa \(tableSelected)"
                
                // Create Attachment
                let imageAttachment = NSTextAttachment()
                imageAttachment.image = UIImage(named:"ic_serviceTable")
                // Set bound to reposition
                let imageOffsetY: CGFloat = -5.0
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: 25, height: 25)
                // Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                // Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                // Add image to mutable string
                completeText.append(attachmentString)
                // Add your text to mutable string
                let textAfterIcon = NSAttributedString(string: "  Fuiste asignado a la mesa \(tableSelected)")
                completeText.append(textAfterIcon)
                cell.lblNumberTabletClient.textAlignment = .center
                cell.lblNumberTabletClient.attributedText = completeText
                
            cell.btnServicioMasa.isHidden = tableSelected > 0
            cell.lblAfiliateMesa.isHidden = tableSelected > 0
                
                
        
            //inSiteString
//                defaultReward = 30
            cell.selectedPercent = defaultReward
            cell.arrayMesas = numArrayTables
            if !percentValid {
                cell.selectedItems.removeAll()
                if defaultReward == 10 || defaultReward == 0 {
                    cell.selectedItems.append(true)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                }else if defaultReward == 15 {
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(true)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                }else if defaultReward == 20 {
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(true)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                }else if defaultReward == 25 {
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(true)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                }else if defaultReward == 30 {
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(true)
                    cell.selectedItems.append(false)
                    cell.selectedItems.append(false)
                }else{
                    print("No es porcentaje correcto")
                }
                
                getPercentOf(percent: converPorcen(valorDefault: defaultReward))
                 tableView.reloadData()
            }
//            cell.collectionView.reloadData()
            cell.collectionPropina.reloadData()
            cell.collectionPropina2.reloadData()
                
                
               
            return cell
        }
        
        
    }
}

extension CarritoViewController: TotalPedidosDelegate, AlertAdsDelegate{
    
    func onChangeEnterPoints(points: Double, subTotal: Double) {
        let subtotalWithPoints = points * mMyReward.tipoCambio
        valueOfPointsInmoey = subtotalWithPoints
        print("total: \(self.total)")
        print("variable Daniel: \(valueOfPointsInmoey)")
        print("subtotal: \(self.subtotal)")
        print("total auxiliar: \(totalSecond)")
         
    }
    
    func onChangeCheck(enable: Bool) {
        if enable{ //esta ocupando puntos moviles
            isUsedPoint = true
            if !descPuntos {
                ordersws.getPointToUse(establishment_id: establishment_id, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, subtotal: self.subtotal)
                descPuntos = true
            }
            
        }else{
            isUsedPoint = false
            descPuntos = false
            self.total = subtotal + comission + choosenPercent + picpalService + shoppingCost
            self.mPoints = 0
            self.mMoneyPoints = 0
            
            for index in 0...items.count {
                print("\(items[index])")
                if items[index] == "total"{
                    tableView.reloadData()
                    break
                }
            }
        }
        
    //calcularServicioPicpal()
        
    }
    
    func deletePromoById(id: Int) {
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        
    }
    
//    @IBAction func OnUsePoint(_ sender: Any) {
//        self.delegate.onChangeCheck(enable: true)
//        imgCheckNo.image = UIImage(named: "emptyRoundedCheckBox")
//        imgCheckYes.image = UIImage(named: "fillRoundedCheckBox")
//    }
//
//    @IBAction func OnNotUsePoints(_ sender: Any) {
//        self.delegate.onChangeCheck(enable: false)
//        imgCheckNo.image = UIImage(named: "fillRoundedCheckBox")
//        imgCheckYes.image = UIImage(named: "emptyRoundedCheckBox")
//    }
    
    
    func addAddress(){
                             
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "DireccionesRegistradas") as! MailingAddressViewController
        newVC.delegate = self
        newVC.from = "carrito"
        
        self.navigationController?.pushViewController(newVC, animated: true)
                        
        
    }
    
    
    
    func showDialogFail(title: String, description: String) {
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = description
        present(newXIB, animated: true, completion: nil)
        
        
    }
    
    func setAddress(address: Address) {
        isSelectAddress = true
        addressSelect = address
        
        if defaultReward > 0 {
                                      
         let defualPropina = Double(defaultReward)/100
            getPercentOf(percent: defualPropina )
        }
        
        
        
        tableView.reloadData()
    }
    func setShoppingCost(shoppingCost: Double, timeDelivery: Int) {
         
        self.shoppingCost = shoppingCost
        self.timeDelivery =     timeDelivery
         total = subtotal  + choosenPercent + picpalService + shoppingCost
        tableView.reloadData()
       
    }
    
    func didSuccessGetServicePickPal(service_amount: Double, service_percent: Double) {
         
        picpalService = 0
        
        if  service_percent > 0{
             picpalService = (subtotal + service_amount) * service_percent
            
        }else{
            
             picpalService = service_amount
            
        }
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        
        
        ordersws.getPointToUse(establishment_id: establishment_id, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, subtotal: self.subtotal)
        
//        if isUsedPoint{
//
//             total = subtotal  + choosenPercent + picpalService + shoppingCost
//
//            if subtotal <=  Double(self.mMoneyPoints){
//
//                total =  choosenPercent + picpalService + shoppingCost
//
//            }else{
//
//                self.total = (self.subtotal - Double(self.mMoneyPoints)) + choosenPercent + picpalService + shoppingCost
//            }
//
//
//
//        }else{
//
//             total = subtotal  + choosenPercent + picpalService + shoppingCost
//        }
         
        tableView.reloadData()
       // tableView.reloadData()
        
    }
    
    func didFailGetServicePickPal(error: String, subtitle: String) {
        
        showDialogFail(title: "Error de comunicación", description: "No se pudo obtener el costo del servicio PickPal")
        
    }
    
    
    func calcularServicioPicpal(){
        
       // print("Servico pickpal ", total - shoppingCost)
        ordersws.getServicesPickPal(total: subtotal, type_serivice: typeService)
    
        
    }
    
    
    func continueToPay(service: String) -> Bool{
        
        
        var isContinue = false
        
        switch service {
        case "RS":
            shoppingCost = 0
            isContinue = true
            self.addressSelect = Address()
            
        case "SM":
            shoppingCost = 0
            isContinue = true
             self.addressSelect = Address()
            
        case "RB":
            shoppingCost = 0
            isContinue = true
            self.addressSelect = Address()
            
        case "SD":
            
//            if(isSelectAddress){
                 isContinue = true
//            }
           
            
        default:
             isContinue = false
        }
        
        return isContinue
        
    }
    
}

extension CarritoViewController{
    
    func didSuccessGetPointsToUse(points: CountEstablishmentPoints) {
        
        self.total_amount_money = points.total_amount_money
        self.total_amount = points.total_amount
        self.auxPointMoney = points.total_amount_money
        
        //ANTERIORI
//        self.auxPointMoney = points.amount_money
     
        
        if isUsedPoint{
            self.mMoneyPoints = points.amount_money
             total = subtotal  + choosenPercent + picpalService + shoppingCost
            
            if subtotal <=  Double(self.mMoneyPoints){
                
                total =  choosenPercent + picpalService + shoppingCost
                
            }else{
                
                self.total = (self.subtotal - Double(self.mMoneyPoints)) + choosenPercent + picpalService + shoppingCost
            }
            
            mPoints = points.amount
                
            
        }else{
            
             total = subtotal  + choosenPercent + picpalService + shoppingCost
            mPoints = 0
        }
        
        
        
//
//        if isUsedPoint{
//
//             self.mMoneyPoints = points.amount_money
//             self.mPoints = points.amount
//             self.total =  self.total - points.amount_money
//
//            print ("Total Con puntos: ",self.total)
//        }
       
        
        for index in 0...items.count {
            print("\(items[index])")
            if items[index] == "total"{
                tableView.reloadData()
                break
            }
        }
        

    }
    
    func didFailGetPointsToUse(error: String, subtitle: String) {
        
    }
    
    func continueToPaymentMethod(typePay:Int, tempArray: [Dictionary<String,Any>]) {
        
        print("Formar de pago aceptada: ", tempArray)
        
         print("Mesa Selecionada ", tableSelected )
         print("total: \(total)")
        
        choosenPercent = typeService == "RS" ? 0:choosenPercent
        
        if myCash.availablePickpalCash{
            
            
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
            newVC.paraAqui = !llevar
            newVC.detail = tempArray
            newVC.order_elements = order_elements
            newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            newVC.auxTotal = total
            newVC.notes = notes
            newVC.promotionalCode = codeString
            newVC.pickpalService = self.picpalService
            newVC.discount = total_code
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.hourScheduled = hourScheduled
            newVC.isScheduled = isScheduled
            newVC.paymentType = typePay
            newVC.inBar = inBar
            newVC.percent = choosenPercent
            newVC.table = self.tableSelected
            newVC.hourFancy = hourFancy
            newVC.dateScheduled = dateSchedule
            newVC.subtotal = subtotal
            newVC.mPoints = mPoints
            newVC.mMoneyPoints = mMoneyPoints
            newVC.addressDelivery = self.addressSelect
            newVC.shipping_cost = self.shoppingCost
            newVC.type_order = self.typeService
            newVC.timeDelivery = self.timeDelivery
            newVC.myCash = self.myCash
            newVC.minimumBilling = self.minimumBilling
            
            self.navigationController?.pushViewController(newVC, animated: true)
             
            
        }else{
        
                switch typePay {
                         
                    /*
                     all = ChoiceItem(0, 'Todos')
                     cash = ChoiceItem(1, 'Efectivo')
                     card = ChoiceItem(2, 'TDC/TDD')
                     */
                    
                case 0: // Si admite Efectivo y TDC
                    
                    let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                    newVC.paraAqui = !llevar
                    newVC.detail = tempArray
                    newVC.order_elements = order_elements
                    newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    newVC.auxTotal = total
                    newVC.notes = notes
                    newVC.promotionalCode = codeString
                    newVC.pickpalService = self.picpalService
                    newVC.discount = total_code
                    newVC.municipio = municipio
                    newVC.categoria = categoria
                    newVC.lugar = lugar
                    newVC.hourScheduled = hourScheduled
                    newVC.isScheduled = isScheduled
                    newVC.paymentType = typePay
                    newVC.inBar = inBar
                    newVC.percent = choosenPercent
                    newVC.table = self.tableSelected
                    newVC.hourFancy = hourFancy
                    newVC.dateScheduled = dateSchedule
                    newVC.subtotal = subtotal
                    newVC.mPoints = mPoints
                    newVC.mMoneyPoints = mMoneyPoints
                    newVC.addressDelivery = self.addressSelect
                    newVC.shipping_cost = self.shoppingCost
                    newVC.type_order = self.typeService
                    newVC.timeDelivery = self.timeDelivery
                    newVC.myCash = self.myCash
                    newVC.minimumBilling = self.minimumBilling
                    
                    
                    self.navigationController?.pushViewController(newVC, animated: true)
                     break
                    
                 case 1:// solo admite efectivo
                    
                   let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                    newVC.paraAqui = !llevar
                    newVC.detail = tempArray
                    newVC.order_elements = order_elements
                    newVC.establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                    newVC.auxTotal = total
                    newVC.notes = notes
                    newVC.promotionalCode = codeString
                    newVC.pickpalService = self.picpalService
                    newVC.discount = total_code
                    newVC.municipio = municipio
                    newVC.categoria = categoria
                    newVC.lugar = lugar
                    newVC.hourScheduled = hourScheduled
                    newVC.isScheduled = isScheduled
                    newVC.paymentType = typePay
                    newVC.inBar = inBar
                    newVC.percent = choosenPercent
                    newVC.table = self.tableSelected
                    newVC.hourFancy = hourFancy
                    newVC.dateScheduled = dateSchedule
                    newVC.subtotal =  subtotal
                    newVC.mPoints = mPoints
                    newVC.mMoneyPoints = mMoneyPoints
                    newVC.addressDelivery = self.addressSelect
                    newVC.shipping_cost = self.shoppingCost
                    newVC.type_order = self.typeService
                    newVC.timeDelivery = self.timeDelivery
                    newVC.myCash = self.myCash
                    newVC.minimumBilling = self.minimumBilling
                    
                    self.navigationController?.pushViewController(newVC, animated: true)
                    break
                    
                 case 2://solo admite tarjeta
                    if subtotal > 3{
                        
                    let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                        newVC.detail = tempArray
                        newVC.paraAqui = !llevar
                        newVC.establishment_id = establishment_id
                        newVC.total = total
                        newVC.first_time  = true
                        newVC.notes = notes
                        newVC.discount = discount
                        newVC.pickpalService = comission
                        newVC.promotionalCode = codeString
                        newVC.municipio = municipio
                        newVC.categoria = categoria
                        newVC.lugar = lugar
                        newVC.order_elements = order_elements
                        newVC.hourScheduled = hourScheduled
                        newVC.isScheduled = isScheduled
                        newVC.inBar = inBar
                        newVC.hourScheduledFormatted = hourFancy
                        newVC.dateScheduled = dateSchedule
                        newVC.percent = choosenPercent
                        newVC.table = tableSelected
                        newVC.mPoints = mPoints
                        newVC.mMoneyPoints = mMoneyPoints
                        newVC.addressDelivery = self.addressSelect
                        newVC.shipping_cost = self.shoppingCost
                        newVC.type_order = self.typeService
                        newVC.timeDelivery = self.timeDelivery
                        newVC.pickpalService = Double(self.picpalService)
                        newVC.minimumBilling = self.minimumBilling
                        self.navigationController?.pushViewController(newVC, animated: true)
                        
                        }else{
                            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                                newXIB.modalTransitionStyle = .crossDissolve
                                newXIB.modalPresentationStyle = .overCurrentContext
                                newXIB.errorMessageString = "Servicio no disponible"
                                newXIB.subTitleMessageString = "Pagos con TDC/TDD únicamente en consumos mayores a $3."
                                present(newXIB, animated: true, completion: nil)
                        }
                    
                     break
                    
                default:
                    showDialogFail(title: "Error", description: "En metodo de pago")
                }
        }
        
    }
    
    func defaultSelection(){
        
        print("Extrayendo seleccion")
        let preferences = UserDefaults.standard

        let currentLevelKey = "itemSelecteKey"

        if preferences.object(forKey: currentLevelKey) == nil {
            
            print("no se pudo obtener el dato xD ")
        
        } else {
            
            let itemSelecte = preferences.string(forKey: currentLevelKey)// preferences.integer(forKey: currentLevelKey)
            
            filterDefaut = preferences.string(forKey: currentLevelKey)!
            defualtItem = true
           //  ordersws.getServicesPickPal(total: subtotal, type_serivice: filterDefaut)
            print("Seleccion default ", itemSelecte!)
        }
        
        tableView.reloadData()
        
    }
    
    
    func converPorcen(valorDefault:Int)->Double{
        
        var conver = Double()
        
        switch valorDefault {
        case 10:
            conver = 0.1
            
          case 15:
            conver = 0.15
            case 20:
            conver = 0.2
            case 25:
            conver = 0.25
            case 30:
            conver = 0.3
            
        default:
            conver = 0.0
        }
        
        
        return conver
    }
    
}

extension CarritoViewController: SelectTabletDelgate, scanerDelgate,AlertJoinToTableProtocol{

    
    func openScaner() {
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "ScannerQR") as! ScannerViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.from = "Carrito"
        newVC.delegate = self
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    func didCheckMyCash(walletInfo: MyCash) {
        
        myCash = walletInfo
        
    }
    
    
    func didFailCheckMyCash(error: String, subtitle: String) {
        menssageDialog(titulo: error, mensaje: subtitle)
    }
    
    
    func openAlertTAblet() {
        
        
        if listTable.count == 0{
            
            let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.visibleFolio = false
                newXIB.visibleQR = false
                newXIB.delegate = self
                self.present(newXIB, animated: true, completion: nil)
            
        }else{

//            alertDisJointTable(nameEstablisment: "Nombre establecimiento", numberTable: listTable[0].numberTable, idDeskUser: listTable[0].deskUserId)

            openDialogDisJointTable()


        }


       
        
    }
    
    func folioTable(folio: String) {
        
//        self.dismiss(animated: true, completion: nil)
        fromFolio = "Scanner"
        customerServiceWS.joinToTableOrder(client_id: client_id, folio: folio, establishment_id: establishment_id)
        
    }
    
    
    func getMySignedTables(idEstablishment: Int, deskUserId: Int, listTable: [TableJointUser]){
        
        visibilityButtomSM = idEstablishment != 0
        tableSelected = idEstablishment
        self.idDeskUser = deskUserId
        self.listTable = listTable
        tableView.reloadData()
    }
    
    
    func didFailMySignedTables(error: String, subtitle: String){
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = error
        newXIB.mTitle = subtitle
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func sendFolio(folio: String){
        
        customerServiceWS.joinToTableOrder(client_id: client_id, folio: folio, establishment_id: establishment_id)
        
        
        fromFolio = "Manual"
        
    }
    
    
    
    func getJoinToTableOrder(establishmentName: String, numberTable: Int){
        
        tableSelected = numberTable
        
        let newXIB = AlertJoinToTable(nibName: "AlertJoinToTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.name = "\(establishmentName)"
        newXIB.numberTable = numberTable
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func reloadListEstablishment(){
        
        tableView.reloadData()
        
    }
    
    
    func goToHome() {
        
    }
    
    
    func didFailJoinToTableOrder(error: String, subtitle: String){
        
        if fromFolio == "Scanner"{
            
            let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.visibleQR = true
                newXIB.delegate = self
                self.present(newXIB, animated: true, completion: nil)
            
        }else{
            
            let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.visibleFolio = true
                newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
        }
        

        
    }
    
    func didFailJoinToTableUser(error: String, subtitle: String){
        
//        let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
//        newXIB.modalTransitionStyle = .crossDissolve
//        newXIB.modalPresentationStyle = .overCurrentContext
//        newXIB.textTitle = error
//        newXIB.textMessage = subtitle
//        newXIB.textFail = ""
//        self.present(newXIB, animated: true, completion: nil)
//
        
        if fromFolio == "Scanner"{
            
            let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.visibleQR = true
                newXIB.delegate = self
                self.present(newXIB, animated: true, completion: nil)
            
        }else{
            
            let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.visibleFolio = true
                newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    func didFailJoinToTable(error: String, subtitle: String,code:Int){
        
        
        if fromFolio == "Scanner"{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Código QR inválido"
            newXIB.code = code
            self.present(newXIB, animated: true, completion: nil)
            
        }else{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Folio inválido"
            newXIB.code = code
            self.present(newXIB, animated: true, completion: nil)
            
        }
        
    }
    
    


    func calcularTotal(){
        self.total = subtotal + comission + choosenPercent + picpalService + shoppingCost
       
        descPuntos = false
        onChangeCheck(enable: isUsedPoint)
        
//
        
    }
    
}


extension CarritoViewController: AlrtDisJointTableDelegate{
    
    
    func alertDisJointTable(nameEstablisment: String, numberTable: Int, idDeskUser: Int){
        
        
        
        let text1 = NSAttributedString(string: "\(UserDefaults.standard.object(forKey: "first_name")!), actualmente estas afiliado a la ", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0) as Any])
        
        let text2 = NSAttributedString(string: "mesa \(String(format: "%02d", numberTable)) ", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller-Bold", size: 13.0) as Any])
        
        let text3 = NSAttributedString(string: "del establecimiento: \n \n \(esablishmentName)", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0) as Any])
        
        let newString = NSMutableAttributedString()
        newString.append(text1)
        newString.append(text2)
        newString.append(text3)
        
        
        let newXIB = AlrtDisJointTable(nibName: "AlrtDisJointTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.idDeskUser = idDeskUser
        newXIB.textBtnCancel = "Si, continuar afiliado"
        newXIB.textBtnAcept = "No, Afiliarme a otra mesa"
        newXIB.titleText = "¿Deseas continuar afiliado a la mesa del establecimiento?"
        newXIB.newString = newString
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    func disJointTable(idDeskUser: Int) {
        
        customerServiceWS.disJointTable(help_desk_id: idDeskUser)
        
    }
    
    
    func didDisjointTable() {
        
        customerServiceWS.mySignedTables(client_id: client_id, establishment_id: establishment_id)
            
        let newXIB = AlertSelectTablet(nibName: "AlertSelectTablet", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.visibleFolio = false
            newXIB.visibleQR = false
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)

        
    }
    
    
    func didFailDisjointTable(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error//"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        newXIB.expander = true
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    func openDialogDisJointTable(){
        

        let text1 = NSAttributedString(string: "\(UserDefaults.standard.object(forKey: "first_name")!), actualmente estas afiliado a la ", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0) as Any])
        
        let text2 = NSAttributedString(string: "mesa \(String(format: "%02d", listTable[0].numberTable)) ", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller-Bold", size: 13.0) as Any])
        
        let text3 = NSAttributedString(string: "del establecimiento: \n \n  \(listTable[0].establishmentName!)", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0) as Any])
        
        let newString = NSMutableAttributedString()
        newString.append(text1)
        newString.append(text2)
        newString.append(text3)
        
        
        let newXIB = AlrtDisJointTable(nibName: "AlrtDisJointTable", bundle: nil)
           newXIB.modalTransitionStyle = .crossDissolve
           newXIB.modalPresentationStyle = .overCurrentContext
           newXIB.delegate = self
           newXIB.idDeskUser = listTable[0].deskUserId
           newXIB.textBtnCancel = "No"
           newXIB.textBtnAcept = "Si, afiliarme al nuevo comercio"
           newXIB.titleText = "¿Deseas desafiliarte y cambiar de establecimiento?"
           newXIB.newString = newString
           self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
}
