//
//  AddCardViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import OpenpayKit
class AddCardViewController: UIViewController, getDateCard, paymentDelegate, UITextFieldDelegate, ordersDelegate {
    

    @IBOutlet weak var inputNumeros: UITextField!
    @IBOutlet weak var inputNombre: UITextField!
    @IBOutlet weak var vencimiento: UILabel!
    @IBOutlet weak var inputCVV: UITextField!
    @IBOutlet weak var backgroundCards: UIImageView!
    @IBOutlet weak var backgroundPaypal: UIImageView!
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var digitsButton: UIButton!
    @IBOutlet weak var ownerButton: UIButton!
    @IBOutlet weak var cvvButton: UIButton!
    
    @IBOutlet weak var wrongTarjeta: UIImageView!
    @IBOutlet weak var wrongNombre: UIImageView!
    @IBOutlet weak var wrongVencimiento: UIImageView!
    @IBOutlet weak var wrongCVV: UIImageView!
    
    @IBOutlet weak var vencimientoIncorrecto: UILabel!
    
    @IBOutlet weak var numPedido: UIView!
    
    @IBOutlet weak var anadirMetodo: UIButton!
    var paymentWS = PaymentWS()
    var orderWS = OrdersWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    
    
    var tarjeta = true
    let conekta = Conekta()
    var Month = String()
    var Year = String()
    var userToken = String()
    var cards = [Card]()
    var numTarjeta = String()
    var isFromPromos:Bool = false
    
    
    //OpenPay
    var openpay : Openpay!
    var sessionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        paymentWS.delegate = self
        orderWS.delegate = self
        conekta.delegate = self
//        Producción
//        conekta.publicKey = "key_Zyrw2SJQmKemoaMWqZVkibg"
//        pruebas
        conekta.publicKey = "key_BW4Azjs4xe7RQe9bRkqAafQ"
        // Nueva key conekta.publicKey = "key_FPnCsUTaQB8kfzersCzbVNA"
        conekta.collectDevice()
        inputNumeros.delegate = self
        inputCVV.delegate = self
        inputNombre.delegate = self
        anadirMetodo.isEnabled = true
        if let order_done = UserDefaults.standard.object(forKey: "order_done") as? Bool {
            if order_done{
                numPedido.alpha = 1
            }else{
                numPedido.alpha = 0
            }
        }else{
            numPedido.alpha = 0
        }
        addCardOpenPay()
//        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey:  Constants.API_KEY, isProductionMode: false, isDebug: false)
        
    }
    func didSuccessCheckCard() {
        wrongTarjeta.alpha = 0
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        
//        addCardOpenPay()
        
//        let card = TokenizeCardRequest(cardNumber: numTarjeta, holderName: inputNombre.text!, expirationYear: Year.substring(from:Year.index(Year.endIndex, offsetBy: -2)), expirationMonth: Month, cvv2: inputCVV.text!)
//
//       // request token by card model
//       openpay.tokenizeCard(card: card) { (OPToken) in
//        if (OPToken.id != nil) {
//            self.userToken = OPToken.id
//
//            self.paymentWS.addNewCard(client_id: self.client_id, token_card: self.userToken)
//        }else{
////                let alert = UIAlertController(title: "PickPal", message: "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias", preferredStyle: .alert)
////                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
////                self.present(alert, animated: true, completion: nil)
//            LoadingOverlay.shared.hideOverlayView()
//            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//            newXIB.modalTransitionStyle = .crossDissolve
//            newXIB.modalPresentationStyle = .overCurrentContext
//            newXIB.errorMessageString = "PickPal"
//            newXIB.subTitleMessageString = "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias"
//            self.present(newXIB, animated: true, completion: nil)
//            self.anadirMetodo.isEnabled = true
//        }
//       } failureFunction: { (NSError) in
//            DispatchQueue.main.async {
//                LoadingOverlay.shared.hideOverlayView()
//                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//                newXIB.modalTransitionStyle = .crossDissolve
//                newXIB.modalPresentationStyle = .overCurrentContext
//                newXIB.errorMessageString = "PickPal"
//                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias"
//                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
//            }
//       }
        
    }
    func didFailCheckCard(error: String, subtitle: String) {
        wrongTarjeta.alpha = 1
        if error == "Existe" {
//            let alert = UIAlertController(title: "PickPal", message: "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
//            self.present(alert, animated: true, completion: nil)
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = "PickPal"
            newXIB.subTitleMessageString = subtitle
            self.present(newXIB, animated: true, completion: nil)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == inputNumeros {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else if textField == inputCVV{
            if numTarjeta.count == 16 {
                let maxLength = 3
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }else{
                let maxLength = 4
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
            
        }else{
            return true
        }
        
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == inputCVV {
            textField.text = ""
            wrongCVV.alpha = 0
        }else if textField == inputNumeros {
            textField.text = ""
            wrongTarjeta.alpha = 0
        }else {
            wrongNombre.alpha = 0
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == inputCVV {
            if textField.text! == "" {
                wrongCVV.alpha = 1
            }
        }else if textField == inputNumeros {
            let str = customStringFormatting(of: textField.text!)
            if textField.text! == "" {
                wrongTarjeta.alpha = 1
            }
            if (textField.text?.count)! < 15  {
                wrongTarjeta.alpha = 1
            }
            numTarjeta = textField.text!
            textField.text = str
            
        }else {
            if textField.text! == "" {
                wrongNombre.alpha = 1
            }
        }
    }
    
    func customStringFormatting(of str: String) -> String {
        return str.characters.chunk(n: 4)
            .map{ String($0) }.joined(separator: " ")
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    func didSuccessAddCard() {
        LoadingOverlay.shared.hideOverlayView()
        self.navigationController?.popViewController(animated: true)
    }
    func didFailAddCard(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        self.anadirMetodo.isEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func añadir(_ sender: Any) {
        
        print(numTarjeta)
        if(inputNombre.text != "" && numTarjeta != "" && inputCVV.text != "" && vencimiento.text != "Fecha de vcto. *"){
            if(numTarjeta.characters.count == 16 || numTarjeta.characters.count == 15){
                if(inputCVV.text?.characters.count == 3 || inputCVV.text?.characters.count == 4){
                    let base64 = checkCards(currentCard: numTarjeta)
//                    Year.substring(start: 2, offsetBy: 2)
                    orderWS.checkCard(client_id: client_id, cc: base64, month: Month, year: Year.substring(start: 2, offsetBy: 2)!)
                    anadirMetodo.isEnabled = false
                }else{
                    let alert = UIAlertController(title: "PickPal", message: "CVV incorrecto, ingrese los dígitos correctos", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Número de tarjeta incorrecto, ingrese la numeración correcta", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            if let first_name = UserDefaults.standard.object(forKey: "first_name") as? String {
                let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }else{
                let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            if inputNumeros.text == "" {
                wrongTarjeta.alpha = 1
            }
            if inputNombre.text == "" {
                wrongNombre.alpha = 1
            }
            if inputCVV.text == "" {
                wrongCVV.alpha = 1
            }
            if vencimiento.text == "Vencimiento *" {
                wrongVencimiento.alpha = 1
            }
            
        }
    }
    
    func checkCards(currentCard: String) -> String {
        let split = currentCard.count
        var substring = String()
        if split == 16 {
            substring = currentCard.substring(start: 12, offsetBy: 4)!
        }else{
            substring = currentCard.substring(start: 11, offsetBy: 4)!
        }

        print(substring)
        let utf8str = substring.data(using: .utf8)
//        ENCODED
        if let base64Encoded = utf8str?.base64EncodedString(options: .init(rawValue: 0)){
//            DECODED
            if let base64Decored = Data(base64Encoded: base64Encoded, options: .init(rawValue: 0)).map({String(data: $0, encoding: String.Encoding.utf8)}){
                print(base64Encoded)
                return base64Encoded
                print("Decoded:  \(base64Decored)")
            }
        }
        return " "
    }

    func sendDate(month: String, year: String) {
        let date = Date()
        let currentCalendar = Calendar.current
        let currentMonth = currentCalendar.component(.month, from: date)
        let currentYear = currentCalendar.component(.year, from: date)
        
        if Int(year)! == currentYear {
            if Int(month)! <= currentMonth {
                wrongVencimiento.alpha = 1
                vencimientoIncorrecto.alpha = 1
            }else{
                wrongVencimiento.alpha = 0
                vencimientoIncorrecto.alpha = 0
            }
        }else{
            wrongVencimiento.alpha = 0
            vencimientoIncorrecto.alpha = 0
        }
        
        Month = month
        Year = year
        vencimiento.text = month + "/" + year
        vencimiento.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
    }
    
    @IBAction func tapPaypal(_ sender: Any) {
        if tarjeta{
            backgroundPaypal.image = UIImage(named: "greenBackgroundRight")
            backgroundCards.image = UIImage(named: "borderGray")
            tarjeta = false
        }
    }
    
    @IBAction func vencimientoSelector(_ sender: Any) {
        let datePicker = CardDatePickerViewController(nibName: "CardDatePickerViewController", bundle: nil)
        datePicker.delegate = self
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.modalTransitionStyle = .coverVertical
        self.present(datePicker, animated: true, completion: nil)
    }
    
    @IBAction func tapCards(_ sender: Any) {
        if !tarjeta{
            backgroundCards.image = UIImage(named: "greenBackground")
            backgroundPaypal.image = UIImage(named: "borderGray")
            tarjeta = true
        }
    }
    @IBAction func TapFields(_ sender: UIButton) {
        if sender == digitsButton {
            inputNumeros.becomeFirstResponder()
        }else if sender == ownerButton {
            inputNombre.becomeFirstResponder()
        }else{
            inputCVV.becomeFirstResponder()
        }
    }
    
}
extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}


extension AddCardViewController {
    
    func addCardOpenPay() {
        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey:  Constants.API_KEY, isProductionMode: false, isDebug: false)
        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Openpay")
    }

    func successSessionID(sessionID: String) {
            print("SessionID: \(sessionID)")
            self.sessionID = sessionID
            //        LoadingOverlay.shared.showOverlay(view: self.view)
            openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
        }
        
        func failSessionID(error: NSError) {
            LoadingOverlay.shared.hideOverlayView()
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
                self.anadirMetodo.isEnabled = true
                
            }
        }
        func successCard() {
            openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
            
        }
        
        func failCard(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
                self.anadirMetodo.isEnabled = true
            }
        }
        
        func successToken(token: OPToken) {
            print("TokenID: \(token.id)")
            DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: self.view)
                self.paymentWS.addNewCardOppa(client_id:  self.client_id, token_card: token.id, session_id: self.sessionID)
            }
        }
//        func didSuccessAddCard() {
//            paymentWS.getUserCardsOppa(client_id: client_id)
//        }
//        func didFailAddCard(error: String, subtitle: String) {
//            let alert = UIAlertController(title: "PickPal", message: "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
        
        func failToken(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
                self.anadirMetodo.isEnabled = true
            }
        }
}
