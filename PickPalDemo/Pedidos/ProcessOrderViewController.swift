//
//  ProcessOrderViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class ProcessOrderViewController: UIViewController, paymentDelegate, errorMessageDelegate, UNUserNotificationCenterDelegate, ordersDelegate, reorderDelegate, sentToDelegate, deleteCarritoDelegate{

    @IBOutlet weak var imgGIF: UIImageView!
    @IBOutlet weak var txtSeconds: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblTitleTotal: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    var seconds = 5
    var timer = Timer()
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var establishment_id = Int()
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var total = Double()
    var subtotal = Double()
    var paymentType = Int()
    var notes = String()
    var promotionalCode = String()
    var address_id = Int()
    var payment_source_id = String()
    var paymentWS = PaymentWS()
    var ordersWS = OrdersWS()
    var defaults = UserDefaults.standard
    var orderSent = Bool()
    var isCash = Bool()
    let center = UNUserNotificationCenter.current()
    var ordersRef: DatabaseReference!
    var itemsRef: DatabaseReference!
    var extrasRef: DatabaseReference!
    var handle: DatabaseHandle!
    var handle2: DatabaseHandle!
    var handle3: DatabaseHandle!
    var hourScheduled = String()
    var isScheduled = Bool()
    var inBar = Bool()
    var percent = Double()
    var table = Int()
    var mPoints = Double()
    var mMoneyPoints = Double()
    
    var pickpalService = Double()
    var discount = Double()
    
    var lugar = String()
    var municipio = String()
    var categoria = String()
    var alert = UIAlertController()
    var cancelOrder = false
    var orderIdAfter = Int()
    var order_elements = [Dictionary<String,Any>]()
    var reorder_establishment_id = Int()
    var reorder_register_date = String()
    var reorder_establishment_name = String()
    var hourFancy = String()
    var dateScheduled = String()
//    Openpay
    var session_id = String()
    var addressDelivery = Address()
    var shipping_cost = Float()
    var type_order = String()
    var timeDelivery = Int()
     var allow_process_nopayment = Bool()
    var myCash = MyCash()
    var pickpal_cash_money = Double()
    var isInsufficientBalance = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateProgressBar), userInfo: nil, repeats: true)
        
        self.navigationController?.isNavigationBarHidden = true
        imgGIF.loadGif(name: "GifProcess")
        paymentWS.delegate = self
        ordersWS.delegate = self
        inactiveProductsBeforeProcessing(items: detail)
        inactiveExtrasBeforeProcessing(items: order_elements)
        inactiveModifiersBeforeProcessing(items: order_elements)
        if let sessid = UserDefaults.standard.object(forKey: "session_id") as? String {
             self.session_id = sessid
        }
        
        
        if self.total > 0{
            lblTotal.text = CustomsFuncs.getFomatMoney(currentMoney: Double(self.total))
            
        }else{
            
            lblTotal.text = CustomsFuncs.getFomatMoney(currentMoney: Double(self.pickpal_cash_money))
        }
        
        if pickpal_cash_money > 0 || mMoneyPoints > 0{
            
            lblTitleTotal.text = "RESTANTE A PAGAR:"
            
        }
        if total == 0{
            
            lblTitleTotal.text = "TOTAL A PAGAR:"
        }
        
        print("Ultimo Direccion: " ,  self.addressDelivery.pk)
        print("Ultimo Pickpla: " ,  self.shipping_cost)
        print("Ultimo  Tipo orden: " ,  self.type_order)
        
    }

    func inactiveProductsBeforeProcessing(items: [Dictionary<String,Any>]) {
        
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id
        )
        var itemsId = [Int]()
        var productsName = [String]()
        for item in items {
            itemsId.append(item["id"]! as! Int)
            productsName.append(item["item_name"] as! String)
        }
        itemsRef = Database.database().reference().child("inactive_items").child("-\(establishmentId)").child("items")
        handle = itemsRef.observe(.value) { (snapshot) in
//            print(snapshot.value)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var productsInactivated = [String]()
                for (index,id) in itemsId.enumerated() {
//                    print(index)
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                        idsInactivated.append(id)
                        productsInactivated.append(productsName[index])
                    }
                }
                if idsInactivated.count > 0 {
                    self.timer.invalidate()
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = productsInactivated
//                    print(idsInactivated, productsInactivated)
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    //                        newXIB.idProduct = id//63
                    self.present(newXIB, animated: true, completion: nil)
                }
                
            }
        }
        print("hold on")
    }
    
    func inactiveExtrasBeforeProcessing(items: [Dictionary<String,Any>]) {
        
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        extrasRef = Database.database().reference().child("inactive_extras").child("-\(establishmentId)")
//        print(referenceExtras)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle3 = extrasRef.observe(.value) { (snapshot) in
            var extrasId = [Int]()
            var modifExtrasName = [String]()
            for item in items {
                if let complements = item["complements"] as? NSDictionary {
                    if let complement_id = complements.value(forKey: "complement_id") as? NSArray {
                        for compl in complement_id {
                            extrasId.append(compl as! Int)
                        }
                    }
                    if let complement_name = complements.value(forKey: "complement_name") as? NSArray {
                        for name in complement_name {
                            modifExtrasName.append(name as! String)
                        }
                    }
                }
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                        if let modifiers_name = (modi as! NSDictionary).value(forKey: "modifier_name") as? NSArray {
                            for name in modifiers_name {
                                modifExtrasName.append(name as! String)
                            }
                        }
                    }
                }
            }
            print(extrasId, modifExtrasName)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var namesInactivated = [String]()
                for (index, id) in extrasId.enumerated() {
                    //                    print(snapshot.value as! NSDictionary)
                    if (snapshot.value as! NSDictionary).value(forKey: "\(id)") != nil {
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        idsInactivated.append(id2)
                        namesInactivated.append(modifExtrasName[index])
                    }
                }
                print(idsInactivated, namesInactivated)
                if idsInactivated.count > 0 {
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = namesInactivated
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    
    func inactiveModifiersBeforeProcessing(items: [Dictionary<String,Any>]) {
        
        let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        extrasRef = Database.database().reference().child("inactive_modifiers").child("-\(establishmentId)")
        print(extrasRef)
        //        extrasObserve = Database.database().reference().child("inactive_extras").child("\(establishmentId)").child("items")
        handle3 = extrasRef.observe(.value) { (snapshot) in
            var extrasId = [Int]()
            var modifExtrasName = [String]()
            for item in items {
                if let modifiers = item["modifiers"] as? NSArray {
                    for modi in modifiers {
                        if let modifiers_id = (modi as! NSDictionary).value(forKey: "modifier_id") as? NSArray {
                            for mod_id in modifiers_id {
                                extrasId.append(mod_id as! Int)
                            }
                        }
                        if let modifiers_name = (modi as! NSDictionary).value(forKey: "modifier_name") as? NSArray {
                            for name in modifiers_name {
                                modifExtrasName.append(name as! String)
                            }
                        }
                    }
                }
            }
            //            extrasId = [63]
            //            modifExtrasName = ["Salsa de anguila"]
            print(extrasId, modifExtrasName)
            if !(snapshot.value is NSNull) {
                var idsInactivated = [Int]()
                var namesInactivated = [String]()
                var idsItems = [Int]()
                for (index, id) in extrasId.enumerated() {
                    print(snapshot.value as! NSDictionary)
                    if (snapshot.value as! NSDictionary).value(forKey: "-\(id)") != nil {
                        let id2 = (((snapshot.value as! NSDictionary).value(forKey: "-\(id)")) as! NSDictionary).value(forKey: "pk") as! Int
                        let id3 = (((snapshot.value as! NSDictionary).value(forKey: "-\(id)")) as! NSDictionary).value(forKey: "item") as! Int
                        idsItems.append(id3)
                        idsInactivated.append(id2)
                        namesInactivated.append(modifExtrasName[index])
                    }
                }
                print(idsInactivated, namesInactivated)
                if idsInactivated.count > 0 {
                    let newXIB = SpentProductViewController(nibName: "SpentProductViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                    newXIB.ids = idsInactivated
                    newXIB.nameProducts = namesInactivated
                    newXIB.idItems = idsItems
                    newXIB.delegate = self
                    newXIB.mun = self.municipio
                    newXIB.pla = self.lugar
                    newXIB.cat = self.categoria
                    newXIB.isComplement = true
                    self.present(newXIB, animated: true, completion: nil)
                }
            }
        }
    }
    
    func deleteCarrito(showView: Bool, stb_id: Int, municipio: String, categoria: String, lugar: String, estbName: String, stayInCart: Bool) {
        if !stayInCart {
            sentSingleEstablishment(idEstablishment: stb_id, estbName: estbName, category: categoria, place: lugar, munic: municipio)
        }else{
            sentCarrito(idEstablishment: stb_id, estbName: estbName, category: categoria, place: lugar, munic: municipio)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if itemsRef != nil {
            itemsRef.removeObserver(withHandle: handle)
            
        }
        if ordersRef != nil {
            ordersRef.removeObserver(withHandle: handle2)
        }
        
        if extrasRef != nil {
            extrasRef.removeObserver(withHandle: handle3)
        }
        
    }
    
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func Cancel(_ sender: Any) {
        self.cancelButton.isEnabled = false
        self.timer.invalidate()
        alert = UIAlertController(title: "PickPal", message: "¿Seguro que deseas cancelar tu pedido?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: { action in
            switch action.style{
            case .default:
                if self.seconds == 0 {
                    DatabaseFunctions.shared.deleteItem()
                    self.ordersWS.changeOrderStatus(order_id: self.orderIdAfter, status: 8)
                    self.cancelOrder = true
                    
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    self.present(navController, animated: true, completion: nil)*/
                    
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    DatabaseFunctions.shared.deleteItem()
                    
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(newVC, animated: true)
                    
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    self.present(navController, animated: true, completion: nil)*/
                }
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
            switch action.style{
            case .default:
                
                print("default")
            case .cancel:
                print("cancel")
                if self.seconds == 0 {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                    newVC.showPedido = true
                    newVC.currentOrderId = self.orderIdAfter
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    if(self.promotionalCode != ""){
//                        self.paymentWS.registerOrderOppa(establishment_id: self.establishment_id, client_id: self.client_id, in_site: self.paraAqui, total: self.total, detail: self.detail, payment_method: self.payment_source_id, billing_address: self.address_id, discount_code: self.promotionalCode, notes: self.notes, discount_total: self.discount, pickpal_service: self.pickpalService, device_session_id: self.session_id)
                        self.paymentWS.registerOrder(establishment_id: self.establishment_id, client_id: self.client_id, in_site: self.paraAqui, total: self.total, detail: self.detail, payment_method: self.payment_source_id, billing_address: self.address_id, discount_code: self.promotionalCode, notes: self.notes, discount_total: self.discount, pickpal_service: self.pickpalService, hour_schedule: self.hourScheduled, is_schedule: self.isScheduled, in_bar: self.inBar, percent: self.percent, number_table: self.table, points: Int(self.mPoints), type_order: self.type_order, delivery_address: self.addressDelivery.pk ?? 0, shipping_cost: self.shipping_cost, time_delivery: self.timeDelivery, moneyPoints: self.mMoneyPoints,pickpal_cash_money: self.pickpal_cash_money)
                        
                    }else{
//                        self.paymentWS.registerOrderOppa(establishment_id: self.establishment_id, client_id: self.client_id, in_site: self.paraAqui, total: self.total, detail: self.detail, payment_method: self.payment_source_id, billing_address: self.address_id, discount_code: 0, notes: self.notes, discount_total: self.discount , pickpal_service: self.pickpalService, device_session_id: self.session_id)
                        self.paymentWS.registerOrder(establishment_id: self.establishment_id, client_id: self.client_id, in_site: self.paraAqui, total: self.total, detail: self.detail, payment_method: self.payment_source_id, billing_address: self.address_id, discount_code: 0, notes: self.notes, discount_total: self.discount , pickpal_service: self.pickpalService, hour_schedule: self.hourScheduled, is_schedule: self.isScheduled,in_bar: self.inBar, percent: self.percent, number_table: self.table, points: Int(self.mPoints), type_order: self.type_order, delivery_address: self.addressDelivery.pk ?? 0, shipping_cost: self.shipping_cost, time_delivery: self.timeDelivery, moneyPoints: self.mMoneyPoints,pickpal_cash_money: self.pickpal_cash_money)
                    }
                }
            case .destructive:
                print("destructive")
                
                
            }}))
       
        self.present(alert, animated: true, completion: nil)
        
    }
    func didSuccessRegisterOrder(code: String, folio: String, order_id: Int, prep_time: Int, allow_process_nopayment: Bool) {
        //        MARK: Mandaba a la pantalla de pedidos para el seguimiento
        orderIdAfter = order_id
        if !cancelOrder {
             
            if isCash && (self.type_order == "SM" || self.type_order == "RB") && !allow_process_nopayment {
                LoadingOverlay.shared.hideOverlayView()
                DispatchQueue.global(qos: .background).async {
                    print("This is run on the background queue")
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                        print("13 minutes has passed")
                        print(order_id)
                         if (DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB") && DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay"  {
                            self.ordersWS.changeOrderStatus(order_id:order_id, status: 7)
                        }
                    })
                    
                }
            /*    if DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id) && ( DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "SM" || DatabaseFunctions.shared.getOrderIdType(order_id: order_id) == "RB"){*/
                //let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 20, repeats: false)
                let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 300, repeats: false)
//                let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                let content1 = UNMutableNotificationContent()
                let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                content1.title = "¡Ve a pagar tu Pedido a \(establecimiento)!"
                content1.body = "Paga tu pedido mostrando tu código QR, tienes 10 minutos. ¡Corre! haz clic aquí para ver los detalles..."
                content1.sound = UNNotificationSound.default()
                content1.userInfo = ["establishment_name": establecimiento, "order_id": order_id, "notification_on": false]
                content1.sound = UNNotificationSound(named: "pedidolisto.wav")
                let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
                center.delegate = self
                center.add(request1, withCompletionHandler: nil)
               //  let trigge3 = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: false)
                let trigge3 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
                //                let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
                let content3 = UNMutableNotificationContent()
                let establecimiento1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                content3.title = "¡Ve a pagar tu Pedido a \(establecimiento1)!"
                content3.body = "Paga tu pedido mostrando tu código QR, tienes 5 minutos. ¡Corre! haz clic aquí para ver los detalles..."
                content3.sound = UNNotificationSound.default()
                content3.userInfo = ["establishment_name": establecimiento1, "order_id": order_id, "notification_on": false]
                content3.sound = UNNotificationSound(named: "pedidolisto.wav")
                let request3 = UNNotificationRequest(identifier: "\(order_id)-", content: content3, trigger: trigge3)
                center.delegate = self
                center.add(request3, withCompletionHandler: nil)
                
                let establishment_name1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                let establishment_id1 = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                let cDate = Date()
                let format = DateFormatter()
                format.dateFormat = "dd_MM_yyyy"
                let currentDate = format.string(from: cDate)
                
               // let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 100, repeats: false)
                
                let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
//                let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
                let content2 = UNMutableNotificationContent()
                content2.title = "Pedido cancelado"
//                content2.body = "Lo sentimos, el tiempo para pagar tu pedido en \(establecimiento) ha expirado."
                content2.body = "Haz click aquí si deseas ordenar nuevamente TU PEDIDO de \(establecimiento)"
                content2.sound = UNNotificationSound.default()
                content2.userInfo = ["establishment_name": establishment_name1, "order_id": order_id, "notification_on": true, "establishment_id": "-\(establishment_id1)", "register_date": currentDate]
                content2.sound = UNNotificationSound(named: "pedidolisto.wav")
                let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
                content2.categoryIdentifier = "caducado"
                center.delegate = self
                center.add(request2, withCompletionHandler: nil)
                
                UNUserNotificationCenter.current().add(request1) { (error) in
                    if let error = error {
                        print("Se ha producido un error request1: \(error)")
                    }
                }
                
                UNUserNotificationCenter.current().add(request2) { (error) in
                    if let error = error {
                        print("Se ha producido un error request2: \(error)")
                    }
                }
                UNUserNotificationCenter.current().add(request3) { (error) in
                    if let error = error {
                        print("Se ha producido un error request3: \(error)")
                    }
                }
               // }
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let myString = formatter.string(from: date)
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd_MM_yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                let establishment_name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                //            let reference = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
                ordersRef = Database.database().reference().child("orders").child("-\(establishment_id)").child(myStringafd).child("-\(order_id)")
                handle2 = ordersRef.observe(.value, with: { (snapshot) -> Void in
                    if !(snapshot.value is NSNull) {
                        let valueOrder = snapshot.value as! NSDictionary
                        let elements = DatabaseFunctions.shared.createOrderElements(products: valueOrder.value(forKey: "products") as! NSArray)
                        print(self.isCash)
                        self.reorder_register_date = myStringafd
                        self.reorder_establishment_id = self.establishment_id
                        self.reorder_establishment_name = establishment_name
                        
                        let addresDescription = ""/*"\(self.addressDelivery.calle!) # \(self.addressDelivery.numeroExterior!) , \(self.addressDelivery.colonia!) , \(self.addressDelivery.estado!), C.P. \(self.addressDelivery.CP!)"*/
                        DatabaseFunctions.shared.insertFromFirebase(establishment_id: self.establishment_id, establishment_name: establishment_name, in_site: self.paraAqui, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: self.promotionalCode, total: self.total, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: valueOrder.value(forKey: "total") as! Double, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "process_hour") as? String ?? "", ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order_id, order_elements: elements, statusItem: "to_pay", qr: valueOrder.value(forKey: "qr") as! String, is_paid: false, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: self.isCash, last4: "", statusController: "to_pay", is_scheduled: self.isScheduled, in_bar: self.inBar, percent: self.percent, table_number: self.table, readyFlag: true, points: self.mPoints, money_puntos: self.mMoneyPoints, order_type: self.type_order, address: addresDescription, shipping_cost: Double(self.shipping_cost), folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: (valueOrder.value(forKey: "time_delivery") as! NSNumber).stringValue ?? "0", to_refund: false, days_elapsed: 0, allow_process_nopayment: allow_process_nopayment, est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? "", est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: valueOrder.value(forKey: "is_generic")as? Bool ?? false,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "", sendNotifications: true, notified: valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: self.pickpal_cash_money, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false,total_card: 0,total_cash:0 ,total_to_pay: 0, total_done_payment: 0 ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                        DatabaseFunctions.shared.deleteItem()
                    }
                    
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                    newVC.showPedido = true
                    newVC.currentOrderId = order_id
                    self.navigationController?.pushViewController(newVC, animated: true)
                })
            }else{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let myString = formatter.string(from: date)
                let yourDate = formatter.date(from: myString)
                formatter.dateFormat = "dd_MM_yyyy"
                let myStringafd = formatter.string(from: yourDate!)
                
                let establishment_name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                //            let reference = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
                ordersRef = Database.database().reference().child("orders").child("-\(establishment_id)").child(myStringafd).child("-\(order_id)")
                print("id ordersRef ", ordersRef)
                handle2 = ordersRef.observe(.value, with: { (snapshot) -> Void in
                    if !(snapshot.value is NSNull) {
                        let valueOrder = snapshot.value as! NSDictionary
                        
                        
                        let elements = DatabaseFunctions.shared.createOrderElements(products: valueOrder.value(forKey: "products") as! NSArray)
                        print(self.isCash)
                        var addresDescription = String()
                        if self.type_order == "SD"{
                            addresDescription = "\(self.addressDelivery.street!) # \(self.addressDelivery.numeroExterior!) , \(self.addressDelivery.colony!) , \(self.addressDelivery.state!), C.P. \(self.addressDelivery.zip_code ?? "")"
                            }
                        
                        DatabaseFunctions.shared.insertFromFirebase(establishment_id: self.establishment_id, establishment_name: establishment_name, in_site: self.paraAqui, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: self.promotionalCode, total: self.total, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: valueOrder.value(forKey: "total") as! Double, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "process_hour") as? String ?? "", ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order_id, order_elements: elements, statusItem: "to_pay", qr: valueOrder.value(forKey: "qr") as! String, is_paid: false, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: self.isCash, last4: "", statusController: "to_pay", is_scheduled: self.isScheduled, in_bar: self.inBar, percent: self.percent, table_number: self.table, readyFlag: true, points: self.mPoints, money_puntos: self.mMoneyPoints, order_type: self.type_order, address: addresDescription, shipping_cost: Double(self.shipping_cost), folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: (valueOrder.value(forKey: "time_delivery") as! NSNumber).stringValue ?? "0", to_refund: false, days_elapsed: 0, allow_process_nopayment: allow_process_nopayment, est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? ""
                                                                    , est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: valueOrder.value(forKey: "is_generic")as? Bool ?? false,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "", sendNotifications: true, notified: valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: self.pickpal_cash_money, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false,total_card: 0,total_cash:0 ,total_to_pay: 0, total_done_payment: 0 ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                        DatabaseFunctions.shared.deleteItem()
                        
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                           let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                                           newVC.showPedido = true
                                           newVC.currentOrderId = order_id
                                           self.navigationController?.pushViewController(newVC, animated: true)
                        
                        
                    }else{
                        
                        print("Esta vacio")
                        print("Pedido Cancelado")
                        DatabaseFunctions.shared.deleteItem()
                                           self.ordersWS.changeOrderStatus(order_id: self.orderIdAfter, status: 8)
                                           self.cancelOrder = true
                                           
                                           /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                           let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                           newVC.modalPresentationStyle = .fullScreen
                                           let navController = UINavigationController(rootViewController: newVC)
                                           self.present(navController, animated: true, completion: nil)*/
                                           
                                           let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                           let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                                           newVC.modalPresentationStyle = .fullScreen
                                           self.navigationController?.pushViewController(newVC, animated: true)
                        
                    }
                    
                   
                })
            }
        }else{
            self.ordersWS.changeOrderStatus(order_id: order_id, status: 8)
        }
        
    }
    func reorder(order_id: Int, is_canceled: Bool) {
        if is_canceled {
            self.ordersWS.changeOrderStatus(order_id:order_id, status: 8)
            //        DatabaseFunctions.shared.deleteDueOrder(order_id: order_id)
            UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: { (nofitications) in
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)"])
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(order_id)+"])
            })
        }else{
           self.ordersWS.retryOrder(order_id: order_id)
        }
        
    }
    func didSuccessRetryOrder(order_id: Int) {
        
        //        MARK: Regitro de notificaciones
        
        print("Tipo Orden BD Process ", DatabaseFunctions.shared.getOrderIdType(order_id: order_id))
        if  DatabaseFunctions.shared.getAllowProcessNopaymentOrder (order_id: order_id) && DatabaseFunctions.shared.getOrderIdType(order_id: order_id) != "SD"  && DatabaseFunctions.shared.getIsConfirmedOrder(order_id: order_id) && DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay" {
        
        LoadingOverlay.shared.hideOverlayView()
        //        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(900), execute: {
//            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
                print("15 minutes has passed")
                print(order_id)
                 if  DatabaseFunctions.shared.getOrderIdStatus(order_id: order_id) == "to_pay"  {
                    self.ordersWS.changeOrderStatus(order_id:order_id, status: 7)
                }
            })
            
        }
        
            
        
        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 300, repeats: false)
//        let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content1 = UNMutableNotificationContent()
        let establecimiento = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content1.title = "¡Ve a pagar tu Pedido a \(establecimiento)!"
        content1.body = "Paga tu pedido mostrando tu código QR, tienes 10 minutos. ¡Corre! haz clic aquí para ver los detalles..."
       content1.sound = UNNotificationSound.default()
        content1.sound = UNNotificationSound(named: "pedidolisto.wav")
        content1.userInfo = ["establishment_name": establecimiento, "order_id": order_id, "notification_on": false]
        let request1 = UNNotificationRequest(identifier: "\(order_id)", content: content1, trigger: trigge1)
        center.delegate = self
        center.add(request1, withCompletionHandler: nil)
        
        let trigge3 = UNTimeIntervalNotificationTrigger(timeInterval: 600, repeats: false)
        //                let trigge1 = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        let content3 = UNMutableNotificationContent()
        let establecimiento1 = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        content3.title = "¡Ve a pagar tu Pedido a \(establecimiento1)!"
        content3.body = "Paga tu pedido mostrando tu código QR, tienes 5 minutos. ¡Corre! haz clic aquí para ver los detalles..."
        content3.sound = UNNotificationSound.default()
        content3.userInfo = ["establishment_name": establecimiento1, "order_id": order_id, "notification_on": false]
        content3.sound = UNNotificationSound(named: "pedidolisto.wav")
        let request3 = UNNotificationRequest(identifier: "\(order_id)-", content: content3, trigger: trigge3)
        center.delegate = self
       center.add(request3, withCompletionHandler: nil)
        
        let establishment_name1 = reorder_establishment_name//DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
        let establishment_id1 = reorder_establishment_id//DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        let cDate = Date()
        let format = DateFormatter()
        format.dateFormat = "dd_MM_yyyy"
        let currentDate = format.string(from: cDate)
        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
//        let trigge2 = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let content2 = UNMutableNotificationContent()
        content2.title = "Pedido cancelado"
//        content2.body = "Lo sentimos, el tiempo para pagar tu pedido en \(establecimiento) ha expirado."
        content2.body = "Haz click aquí si deseas ordenar nuevamente TU PEDIDO de \(establecimiento)"
        content2.sound = UNNotificationSound(named: "pedidolisto.wav")
        content2.userInfo = ["establishment_name": establishment_name1, "order_id": order_id, "notification_on": true, "establishment_id": "-\(establishment_id1)", "register_date": currentDate]
        let request2 = UNNotificationRequest(identifier: "\(order_id)+", content: content2, trigger: trigge2)
        content2.categoryIdentifier = "caducado"
        center.delegate = self
        center.add(request2, withCompletionHandler: nil)
        
        UNUserNotificationCenter.current().add(request1) { (error) in
            if let error = error {
                print("Se ha producido un error request1: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request2) { (error) in
            if let error = error {
                print("Se ha producido un error request2: \(error)")
            }
        }
        
        UNUserNotificationCenter.current().add(request3) { (error) in
            if let error = error {
                print("Se ha producido un error request3: \(error)")
            }
        }
 }
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd_MM_yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        let establishment_name = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
//        let reference = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
        ordersRef = Database.database().reference().child("orders").child("-\(establishment_id)").child(myStringafd).child("\(order_id)")
        handle2 = ordersRef.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                self.reorder_register_date = myStringafd
                self.reorder_establishment_id = self.establishment_id
                let valueOrder = snapshot.value as! NSDictionary
                let elements = DatabaseFunctions.shared.createOrderElements(products: valueOrder.value(forKey: "products") as! NSArray)
                let addresDescription = "\(self.addressDelivery.street!) # \(self.addressDelivery.numeroExterior!) , \(self.addressDelivery.colony!) , \(self.addressDelivery.state!), C.P. \(self.addressDelivery.zip_code!)"
                DatabaseFunctions.shared.insertFromFirebase(establishment_id: self.establishment_id, establishment_name: establishment_name, in_site: self.paraAqui, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: self.promotionalCode, total: self.total, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: valueOrder.value(forKey: "total") as! Double, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date: valueOrder.value(forKey: "register_date") as! String, process_time: valueOrder.value(forKey: "process_hour") as? String ?? "", ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order_id, order_elements: elements, statusItem: "to_pay", qr: valueOrder.value(forKey: "qr") as! String, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: self.isCash, last4: "", statusController: "to_pay", is_scheduled: self.isScheduled, in_bar: self.inBar, percent: self.percent, table_number: self.table, readyFlag: false, points: self.mPoints, money_puntos: self.mMoneyPoints, order_type: self.type_order, address: addresDescription, shipping_cost: Double(self.shipping_cost), folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: (valueOrder.value(forKey: "time_delivery") as! NSNumber).stringValue , to_refund: valueOrder.value(forKey: "to_refund") as! Bool , days_elapsed: 0, allow_process_nopayment: false, est_delivery_hour: valueOrder.value(forKey: "est_delivery_hour") as? String ?? "", est_ready_hour: valueOrder.value(forKey: "est_ready_hour")as? String ?? "", is_generic: valueOrder.value(forKey: "is_generic")as? Bool ?? false,confirmed:valueOrder.value(forKey: "confirmed")as? Bool ?? false, confirm_hour: valueOrder.value(forKey: "confirm_hour") as? String ?? "",  sendNotifications: true, notified:  valueOrder.value(forKey: "notified") as? Bool ?? false,pickpal_cash_money: self.pickpal_cash_money, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: valueOrder.value(forKey: "active_customer_service") as? Bool ?? false,total_card: 0,total_cash:0 ,total_to_pay: 0, total_done_payment: 0 ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                DatabaseFunctions.shared.deleteItem()
            }
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.showPedido = true
            newVC.currentOrderId = order_id
            self.navigationController?.pushViewController(newVC, animated: true)
        })
    }
    
    func didFailRetryOrder(error: String, subtitle: String) {
        print(error)
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print(response.notification.request.content.userInfo)
        if let notifictionon = response.notification.request.content.userInfo as? NSDictionary {
            if let notif = notifictionon.value(forKey: "notification_on") as? Bool {
                if notif {
                    let reorder = ReOrderViewController()
                    reorder.delegate = self
                    reorder.modalTransitionStyle = .crossDissolve
                    reorder.modalPresentationStyle = .overCurrentContext
                    reorder.order_id = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
                    reorder.establishment_id = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "establishment_id") as! String
                    reorder.register_date = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "register_date") as! String
                    reorder.nombreComercio = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "establishment_name") as! String
                    self.present(reorder, animated: true, completion: nil)
                }else{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                    newVC.readyDesplegar = true
                    newVC.currentOrderId = (response.notification.request.content.userInfo as! NSDictionary).value(forKey: "order_id") as! Int
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
            }else{
                if let isFactura = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "is_factura") as? Bool {
                    if isFactura {
                        let order_id = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "verificarFacturacion") as! VerificarFacturacionViewController
                        newVC.orderId = order_id!
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                    
                }else{
                    if let order_id = (((response.notification.request.content.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int {
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
                        newVC.currentOrderId = order_id
                        newVC.showPedido = true
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                }
                
                
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func didSuccessChangeOrderStatus() {
        print("didSuccessChangeOrderStatus")
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
    }
    func didFailChangeOrderStatus(error: String, subtitle: String) {
        print(error)
    }
    func dataFromFirebase(code: String, folio: String, order_id: Int, prep_time: Int)  {
        let date = Date()
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd_MM_yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        let establishment_id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        ordersRef = Database.database().reference().child("orders").child("\(establishment_id)").child(myStringafd).child("\(order_id)")
        
        handle2 = ordersRef.observe(.value, with: { (snapshot) -> Void in
            if !(snapshot.value is NSNull) {
                let order_number = (snapshot.value as! NSDictionary).value(forKey: "order_number") as! String
                UserDefaults.standard.set(order_number, forKey: "order_number")
                let status = (snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                if let stats = (Constants.status as? NSDictionary)?.value(forKey: "\(status)") as? String {
                    if stats == "in_process" {
                        let currentDate = UserDefaults.standard.object(forKey: "hora_pedido") as! Date
                        let calendar = Calendar.current
                        let date = calendar.date(byAdding:.minute, value: prep_time, to: currentDate)
                        let formatter = DateFormatter()
                        formatter.locale = Locale(identifier: "en_US_POSIX")
                        formatter.timeZone = TimeZone(identifier: "UTC")
                        formatter.dateFormat = "h:mm a"
                        formatter.amSymbol = "a.m."
                        formatter.pmSymbol = "p.m."
                        let dateString = formatter.string(from: date!)
                        print(dateString)
                        //                        MARK: hacer append al status item que ya tengo guardado
                        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "PedidosAnteriores") as! PedidosAnterioresViewController
                        newVC.showPedido = true
                        self.navigationController?.pushViewController(newVC, animated: true)
                        
                    }
                }
            }
            
        })
    }
    func didFailRegisterOrder(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        newXIB.popFlag = true
        newXIB.delegate = self
        present(newXIB, animated: true, completion: {})
    }
    
    func popViewController(error: String) {
        if error == "Establecimiento cerrado" {
            for viewController in (self.navigationController?.viewControllers)! {
                if viewController.isKind(of: HomeViewController.self) {
                    self.navigationController?.popToViewController(viewController, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.showSplash = false
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
                DatabaseFunctions.shared.deleteItem()
            }
        }else{
            if isScheduled || inBar || table > 0{
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "CardSelector") as! CardSelectorViewController
                newVC.detail = detail
                newVC.paraAqui = paraAqui
                newVC.establishment_id = establishment_id
                newVC.total = total
                newVC.first_time  = true
                newVC.notes = notes
                newVC.discount = discount
                newVC.pickpalService = pickpalService
                newVC.promotionalCode = promotionalCode
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = hourScheduled
                newVC.isScheduled = isScheduled
                newVC.inBar = inBar
                newVC.hourScheduledFormatted = hourFancy
                newVC.dateScheduled = dateScheduled
                newVC.percent = percent
                newVC.table = table
                
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  Double(self.shipping_cost)
                newVC.type_order = self.type_order
                 newVC.timeDelivery = self.timeDelivery
                newVC.myCash = self.myCash
                
                print("Metodo Pago Direccion: " , addressDelivery.pk)
                print("Metodo Pago Pickpla: " ,  self.shipping_cost)
                print("Metodo Pago Tipo orden: " ,  self.type_order)
                
                
                self.navigationController?.pushViewController(newVC, animated: true)
                self.navigationController?.popViewController(animated: true)
            }else{
                let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "MetodoPago") as! MetodoPagoViewController
                newVC.total = total
                newVC.detail = detail
                newVC.pickpalService = pickpalService
                newVC.discount = discount
                newVC.establishment_id = establishment_id
                newVC.paraAqui = paraAqui
                newVC.notes = notes
                newVC.municipio = municipio
                newVC.categoria = categoria
                newVC.lugar = lugar
                newVC.order_elements = order_elements
                newVC.hourScheduled = ""
                newVC.isScheduled = isScheduled
                newVC.inBar = inBar
                newVC.percent = percent
                newVC.table = table
                
                newVC.addressDelivery = self.addressDelivery
                newVC.shipping_cost =  Double(self.shipping_cost)
                newVC.type_order = self.type_order
                 newVC.timeDelivery = self.timeDelivery
                newVC.myCash = self.myCash
                print("Metodo Pago Direccion: " , addressDelivery.pk)
                print("Metodo Pago Pickpla: " ,  self.shipping_cost)
                print("Metodo Pago Tipo orden: " ,  self.type_order)
                
                
                self.navigationController?.pushViewController(newVC, animated: true)
            }
            
        }
    }
    @objc func updateProgressBar() {
        print(seconds, "Seconds")
        seconds -= 1
        if seconds == 0 {
            timer.invalidate()
            
            orderSent = true
//            if isCash {
//                if(promotionalCode != ""){
//                    paymentWS.registerOrder(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: promotionalCode, notes: notes)
//                }else{
//                    paymentWS.registerOrder(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: 0, notes: notes)
//                }
//            }else{ç
            //            MARK: dESCOMENTAR
                if(promotionalCode != ""){
                    paymentWS.registerOrder(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: promotionalCode, notes: notes, discount_total: discount, pickpal_service: pickpalService, hour_schedule: hourScheduled, is_schedule: isScheduled, in_bar: self.inBar, percent: self.percent, number_table: self.table, points: Int(self.mPoints), type_order: self.type_order, delivery_address: self.addressDelivery.pk as? Int ?? 0, shipping_cost: self.shipping_cost, time_delivery: timeDelivery, moneyPoints: self.mMoneyPoints,pickpal_cash_money: self.pickpal_cash_money)
//                    paymentWS.registerOrderOppa(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: promotionalCode, notes: notes, discount_total: discount, pickpal_service: pickpalService, device_session_id: session_id)
                }else{
//                    paymentWS.registerOrderOppa(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: 0, notes: notes, discount_total:discount , pickpal_service: pickpalService, device_session_id: session_id)
                    paymentWS.registerOrder(establishment_id: establishment_id, client_id: client_id, in_site: paraAqui, total: total, detail: detail, payment_method: payment_source_id, billing_address: address_id, discount_code: 0, notes: notes, discount_total:discount , pickpal_service: pickpalService, hour_schedule: hourScheduled, is_schedule: isScheduled,in_bar: self.inBar, percent: self.percent, number_table: self.table, points: Int(self.mPoints), type_order: self.type_order, delivery_address: self.addressDelivery.pk as? Int ?? 0, shipping_cost: self.shipping_cost, time_delivery: timeDelivery, moneyPoints: self.mMoneyPoints,pickpal_cash_money: self.pickpal_cash_money)
                }
//            }
            
//            defaults.set(detail, forKey: "order_elements")
        }else{
            txtSeconds.text = timeString(time: TimeInterval(seconds))
        }
        
    }
    
    func sentCarrito(idEstablishment: Int, estbName: String, category: String, place: String, munic: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        newVC.establishment_id = idEstablishment
        newVC.municipio = munic
        newVC.categoria = category
        newVC.lugar = place
        newVC.esablishmentName = estbName
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func sentSingleEstablishment(idEstablishment: Int, estbName: String, category: String, place: String, munic: String) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
        newVC.id = idEstablishment
        newVC.municipioStr = municipio
        newVC.categoriaStr = categoria
        newVC.lugarString = lugar
        newVC.establString = estbName
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func timeString(time: TimeInterval) -> String {
        let seconds = Int(time)
        let miliseconds = Int(time)  / 3600
        
        return String(format: "%02i:%02is",  miliseconds, seconds)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
