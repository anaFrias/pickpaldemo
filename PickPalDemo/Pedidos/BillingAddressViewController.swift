//
//  BillingAddressViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import iOSDropDown

class BillingAddressViewController: UIViewController, paymentDelegate, UITextFieldDelegate, userDelegate {

    @IBOutlet weak var Rfc: UITextField!
    
    @IBOutlet weak var nombre_completo: UITextField!
    
    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var NoExterior: UITextField!
    @IBOutlet weak var NoInterior: UITextField!
    @IBOutlet weak var Colonia: UITextField!
    @IBOutlet weak var EntidadFederativa: UITextField!
    @IBOutlet weak var Zip: UITextField!
    @IBOutlet weak var Calle: UITextField!
    @IBOutlet weak var totalAPagar: UILabel!
    
    @IBOutlet weak var RFCButton: UIButton!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    
    @IBOutlet weak var indicatorRFC: UIImageView!
    @IBOutlet weak var indicatorRazon: UIImageView!
    @IBOutlet weak var indicatorMail: UIImageView!
    
    @IBOutlet weak var rfcWrongText: UILabel!
    @IBOutlet weak var mailWrongText: UILabel!
    
    //factura 4.0
    //Código Postal
    @IBOutlet weak var zipCodeField: UITextField!
    @IBOutlet weak var indicatorZipCode: UIImageView!
    @IBOutlet weak var zipCodeButton: UIButton!
    
    //CFDI
    @IBOutlet weak var cfdiIndicator: UIImageView!
    @IBOutlet weak var cfdiFieldDropDown: DropDown!
    @IBOutlet weak var cfdiButton: UIButton!
    
    
    var addressDelivery = Address()
    var shipping_cost = Float()
    var type_order = String()
    var timeDelivery = Int()
    
    var notes = String()
    var promotionalCode = String()
    
    var paymentWS = PaymentWS()
    var usersWs = UserWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var isUpdate = false
    var addressId = 0
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var establishment_id = Int()
    var total = Double()
    var payment_source_id = String()
    var billing_addres_id = Int()
    var billingAddresses = [BillingAddress]()
    var info = BillingAddress()
    var hourScheduled = String()
    var isScheduled = Bool()
    var inBar = Bool()
    var percent = Double()
    var table = Int()
    var hourFancy = String()
    var dateScheduled = String()
    var mPoints = Double()
    var mMoneyPoints = Double()
    var pickpal_cash_money = Double()
    var isInsufficientBalance = Bool()
    var taxRegimenId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentWS.delegate = self
        usersWs.delegate = self
//        paymentWS.getBillingAddress(client_id: client_id)
        self.navigationController?.isNavigationBarHidden = true
        if(isUpdate)
        {
            if info.id! == 0 {
                billing_addres_id = info.id
            }else{
                billing_addres_id = info.id
                Rfc.text = info.RFC
                nombre_completo.text = info.full_name
                Email.text = info.email_send
            }

//            paymentWS.getSingleAddress(address_id: billing_addres_id, client_id: client_id)
//            LoadingOverlay.shared.showOverlay(view: self.view)
        }        // Do any additional setup after loading the view.
        Rfc.delegate = self
        nombre_completo.delegate = self
        Email.delegate = self
        zipCodeField.delegate = self
        cfdiFieldDropDown.delegate = self
        
        Rfc.addTarget(self, action: #selector(uppercaseRFC(_:)), for: .editingChanged)
//        totalAPagar.text = "$" + total.description + "0 MXN"
        self.zipCodeField.text = info.zip_code
        
        requestTaskRegimen()
    }
    
    func requestTaskRegimen(){
        LoadingOverlay.shared.showOverlay(view: self.view)
        usersWs.getTaxRegimenList()
    }
    
    func didSuccessGetTaskRegimen(regimenList: [TaxRegimenDTO]) {
        LoadingOverlay.shared.hideOverlayView()
        
        if isUpdate{
            self.taxRegimenId = info.tax_regime ?? 0
            self.cfdiFieldDropDown.text = findTaxRegime(taxRegime: info.tax_regime ?? 0, regimenList: regimenList)
        }
        var taxNames = [String]()
        for data in regimenList{
            taxNames.append(data.value)
        }
        cfdiFieldDropDown.optionArray = taxNames
        
        cfdiFieldDropDown.didSelect{(selectedText , index ,id) in
            self.taxRegimenId = regimenList[index].key
        }
    }
    
    func didFailGetTaskRegimen(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func findTaxRegime(taxRegime: Int, regimenList: [TaxRegimenDTO]) -> String{
        var tr = ""
        for data in regimenList{
            if taxRegime == data.key{
                tr = data.value
                break
            }
        }
        
        return tr
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == Rfc {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 13
        }else{
            return true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == Rfc {
            indicatorRFC.alpha = 0
            rfcWrongText.alpha = 0
        }else if textField == nombre_completo {
            indicatorRazon.alpha = 0
        }else if textField == Email{
            indicatorMail.alpha = 0
            mailWrongText.alpha = 0
        }else if textField == zipCodeField{
            indicatorZipCode.alpha = 0
        }else{
            cfdiIndicator.alpha = 0
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == Rfc {
            if textField.text != "", textField != nil {
                indicatorRFC.alpha = 1
                if isValidRFC(testStr: textField.text!) {
                    indicatorRFC.image = UIImage(named: "correctIcon")
                }else{
                    indicatorRFC.image = UIImage(named: "wrongIcon")
                    rfcWrongText.alpha = 1
                }
            }else{
                indicatorRFC.alpha = 1
                indicatorRFC.image = UIImage(named: "wrongIcon")
            }
            
        }else if textField == nombre_completo {
            if textField.text == "" {
                indicatorRazon.alpha = 1
                indicatorRazon.image = UIImage(named: "wrongIcon")
            }
            
        }else if textField == zipCodeField{
            if textField.text == "" {
                indicatorZipCode.alpha = 1
                indicatorZipCode.image = UIImage(named: "redClose")
            }
        }else if textField == cfdiFieldDropDown{
            if textField.text != "" {
                cfdiIndicator.alpha = 0
            }else{
                indicatorRFC.alpha = 1
                indicatorRFC.image = UIImage(named: "redClose")
            }
        }else{
            if textField.text == "" {
                indicatorMail.alpha = 1
                indicatorMail.image = UIImage(named: "wrongIcon")
                
            }else{
                if !isValidEmail(testStr: textField.text) {
                    indicatorMail.alpha = 1
                    mailWrongText.alpha = 1
                    indicatorMail.image = UIImage(named: "wrongIcon")
                }else{
                    indicatorMail.alpha = 1
                    indicatorMail.image = UIImage(named: "correctIcon")
                }
            }
        }
    }
    @objc func uppercaseRFC (_ textField: UITextField) {
        Rfc.text = textField.text?.uppercased()
    }
    func isValidRFC(testStr:String) -> Bool {
        guard testStr != nil else { return false }
        
        let result = matches(for: "^([A-ZÑ\\x26]{3,4}([0-9]{2})(0[0-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\\d]{3})?$", in: testStr)
        if result.count > 0 {
            if result[0] == testStr {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
        
    }
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func SetBillingAddress(_ sender: Any) {
        if zipCodeField.text == ""{
            indicatorZipCode.alpha = 1
            indicatorZipCode.image = UIImage(named: "redClose")
        }
        
        if cfdiFieldDropDown.text == ""{
            cfdiIndicator.alpha = 1
            cfdiIndicator.image = UIImage(named: "redClose")
        }
        
        if(Rfc.text != "" && Email.text != "" && nombre_completo.text != "" && zipCodeField.text != "" && self.taxRegimenId != 0){
            if !checkAddress(addresses: billingAddresses, currentRFC: Rfc.text!) {
                if isValidRFC(testStr: Rfc.text!){
                    if isValidEmail(testStr: Email.text) {
                        if(!isUpdate){
                            paymentWS.setBillingAddress(
                                client_id: client_id, billing_address_id: 0, RFC: Rfc.text!, nombre_completo: nombre_completo.text!,
                                email: Email.text!, tax_regime: self.taxRegimenId, zip_Code: zipCodeField.text!)
                        }else{
                            paymentWS.setBillingAddress(
                                client_id: client_id, billing_address_id: billing_addres_id, RFC: Rfc.text!, nombre_completo: nombre_completo.text!,
                                email: Email.text!, tax_regime: self.taxRegimenId, zip_Code: zipCodeField.text!)
                        }
                    }else{
                        mailWrongText.alpha = 1
                    }
                }else{
                    rfcWrongText.alpha = 1
                }
            }else{
                rfcWrongText.alpha = 1
                rfcWrongText.text  = "El RFC ya está registrado"
            }
            
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func checkAddress(addresses: [BillingAddress], currentRFC: String) -> Bool {
        var flags = [Bool]()
        if addresses.count == 0 {
            flags.append(false)
        }
        for address in addresses {
            if address.RFC == currentRFC.uppercased() {
                if info.email_send != Email.text || info.full_name != nombre_completo.text || info.zip_code != zipCodeField.text || info.tax_regime != self.taxRegimenId{
                    flags.append(false)
                }else{
                    flags.append(true)
                }
            }else{
                flags.append(false)
            }
        }
        if flags.contains(true) {
            return true
        }else{
            return false
        }
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didSuccessSetBillingAddress(billing_address_id: Int) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didFailSetBillingAddress(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessGetSingleBillingAddress(info: BillingAddress) {
        LoadingOverlay.shared.hideOverlayView()
//        if info.id! == 0 {
//            billing_addres_id = info.id
//        }else{
//            billing_addres_id = info.id
//            Rfc.text = info.RFC
//            nombre_completo.text = info.full_name
//            Email.text = info.email_send
//        }
    }
    
    func didFailGetSingleBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
        
    func didSuccessGetBillingAddress(info: [BillingAddress]) {
        print(info)
    }
    func didFailGetBillingAddress(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
    }
    
    @IBAction func touchInput(_ sender: UIButton) {
        if sender == RFCButton {
            Rfc.becomeFirstResponder()
        }else if sender == nameButton {
            nombre_completo.becomeFirstResponder()
        }else if sender == Email{
            Email.becomeFirstResponder()
        }else if sender == cfdiButton{
            cfdiFieldDropDown.becomeFirstResponder()
        }else{
            zipCodeField.becomeFirstResponder()
        }
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

