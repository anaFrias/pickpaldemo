//
//  editComplexItemsViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke

protocol reloadDataDelegate {
    func reloadOrders()
}
class editComplexItemsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var subtitle: UILabel!
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblProductPrice: UILabel!
    
    @IBOutlet weak var containerProdcut: UIView!
    
    var order_elements = Dictionary<String,Any>()
    var itemsInTable = [[String]]()
//    var item = Items()
    var ws = OrdersWS()
    var modifiers = [[modifier_prop]]()
    var complements = [complement_prop]()
    var savedModifiers = [Dictionary<String,Any>]()
    var savedComplements = [Dictionary<String,Any>]()
    var selectedCells = [[Bool]]()
    var numComplementSelected = [Int]()
    
    var nameStoreComplements = [String]()
    var priceStoreComplements = [Double]()
    var cantStoreComplements = [Int]()
    var idStoreComplements = [Int]()
    
    var nameStoreModifiers = [[String]]()
    var priceStoreModifiers = [[Double]]()
    var idStoreModifiers = [[Int]]()
    var categoryModifiers = [String]()
    
    var totalModfiers = Double()
    var totalComplements = Double()
    
    var itemEdited = Int()
    var dictionaryInfo = [Dictionary<String,Any>]()
    
    var dictionaryModifiers = [Dictionary<String,Any>]()
    var dictionaryComplements = Dictionary<String,Any>()
    
    var complementsArrayString = [String]()
    var complementsArrayPrice = [Double]()
    var complementsArrayId = [Int]()
    var complementsArrayCant = [Int]()
    
    var modifiersArrayString = [[String]]()
    var modifiersArrayPrice = [[Double]]()
    var modifiersArrayId = [[Int]]()
    
    
    var modifiers_name = [String]()
    
    var cantidadItems = Int()
    var limit_extra = Int()
    var delegate : reloadDataDelegate!
    var extrasCounter = [Int]()
    
    var max_required = [Int]()
    var required = [Bool]()
    
    var requiredCells = [Bool]()
    
    var nameComplements = [String]()
    var precioAuxiliar = Double()

    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = UserDefaults.standard.object(forKey: "first_name") as? String {
            subtitle.text = "\(user) escoge entre las siguientes opciones para hacer de tu pedido el que más te guste."
        }
        
        precioAuxiliar = (order_elements["price_product"] as! Double)
        
        if (order_elements["image"] as! String) != "" {
            Nuke.loadImage(with: URL(string: (order_elements["image"] as! String))!, into: imgProduct)
        }
        
        lblProductName.text = (order_elements["name"] as! String)
        lblProductPrice.text = "\(CustomsFuncs.getFomatMoney(currentMoney:(order_elements["price_product"] as! Double)))"
       
        
        tableView.register(UINib(nibName: "EditModifiersTableViewCell", bundle: nil), forCellReuseIdentifier: "editModifiers")
        tableView.register(UINib(nibName: "EditComplementsTableViewCell", bundle: nil), forCellReuseIdentifier: "editComplements")
        tableView.register(UINib(nibName: "NotasEditTableViewCell", bundle: nil), forCellReuseIdentifier: "notesCellXib")
        tableView.register(UINib(nibName: "AddToMyOrderEditTableViewCell", bundle: nil), forCellReuseIdentifier: "addToMyEditedOrder")
        ws.delegate = self
        if let item_id = UserDefaults.standard.object(forKey: "item_id") as? Int {
            ws.viewSingleProduct(product_id: item_id, establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        }
        print(order_elements["complements"] as! NSDictionary)
        if let name = (order_elements["complements"] as! NSDictionary).value(forKey: "complement_name") as? [String]  {
            nameStoreComplements = name
            priceStoreComplements = (order_elements["complements"] as! NSDictionary).value(forKey: "complement_price") as! [Double]
            cantStoreComplements = (order_elements["complements"] as! NSDictionary).value(forKey: "complement_cant") as! [Int]
            idStoreComplements = (order_elements["complements"] as! NSDictionary).value(forKey: "complement_id") as! [Int]
            totalComplements = priceStoreComplements.reduce(0,+)
            numComplementSelected = cantStoreComplements
        }else{
            numComplementSelected = [0]
        }
        let modifiers = order_elements["modifiers"] as! NSArray
        for modifier in modifiers {
            nameStoreModifiers.append((modifier as! NSDictionary).value(forKey: "modifier_name") as! [String])
            priceStoreModifiers.append((modifier as! NSDictionary).value(forKey: "modifier_price") as! [Double])
            idStoreModifiers.append((modifier as! NSDictionary).value(forKey: "modifier_id") as! [Int])
            categoryModifiers.append((modifier as! NSDictionary).value(forKey: "category") as! String)
        }
        
        let reduce = priceStoreModifiers.reduce([], +)
        totalModfiers = reduce.reduce(0, +)
        dictionaryComplements = ["complement_name": nameStoreComplements, "complement_cant": cantStoreComplements, "complement_price": priceStoreComplements, "complement_id": idStoreComplements, "category": "EXTRAS" ] as [String:Any]
        for i in 0..<modifiers.count {
            let dictionary = ["modifier_name": nameStoreModifiers[i], "modifier_price": priceStoreModifiers[i], "modifier_id": idStoreModifiers[i], "category": categoryModifiers[i]] as [String:Any]
            dictionaryModifiers.append(dictionary)
        }
        modifiersArrayId = idStoreModifiers
        modifiersArrayPrice = priceStoreModifiers
        modifiersArrayString = nameStoreModifiers
        complementsArrayString = nameStoreComplements
        complementsArrayId = idStoreComplements
        complementsArrayPrice = priceStoreComplements
        complementsArrayCant = cantStoreComplements
        
        //        MARK:
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
extension editComplexItemsViewController: ordersDelegate, addRemoveComplementsEdit, editMyOrderDelegate {
    
    func addComplementsEdit(indexPath: IndexPath, cant: Int) {
        extrasCounter[indexPath.row] = 1
        if extrasCounter.reduce(0,+) <= limit_extra {
            let cantidad = cant + 1
            if numComplementSelected.count > 0 {
                numComplementSelected[indexPath.row] = cantidad
            }else{
                numComplementSelected.append(cantidad)
            }
            
            //MARK:WORK ON IT
            //anterior
//            totalComplements = Double(cantidad) * Double(complements[indexPath.row].value)!
            
            totalComplements =  Double(cantidad)  * Double(complements[indexPath.row].value)!
            
            if !complementsArrayId.contains(complements[indexPath.row].id as! Int) {
                
                complementsArrayString.append(complements[indexPath.row].complement_name as! String)
                complementsArrayCant.append(cantidad)
                complementsArrayPrice.append(totalComplements)
                complementsArrayId.append(complements[indexPath.row].id as! Int)
                
            }else{
                
                complementsArrayString[complementsArrayId.index(of: complements[indexPath.row].id as! Int)!] = complements[indexPath.row].complement_name
                complementsArrayPrice[complementsArrayId.index(of: complements[indexPath.row].id as! Int)!] = totalComplements
                complementsArrayCant[complementsArrayId.index(of: complements[indexPath.row].id as! Int)!] = cantidad
                complementsArrayId[complementsArrayId.index(of: complements[indexPath.row].id as! Int)!] = complements[indexPath.row].id as! Int
            
            }
            
//            if indexPath.row > 0{
//
//                order_elements["price"] =  (order_elements["price"] as! Double) + (complementsArrayPrice[indexPath.row - 1])
//
//            }else{
//
//                order_elements["price"] =  (order_elements["price"] as! Double) + (complementsArrayPrice[indexPath.row] )
//            }
            
            dictionaryComplements = ["complement_name": complementsArrayString, "complement_price": complementsArrayPrice, "complement_cant": complementsArrayCant, "complement_id": complementsArrayId, "category": "EXTRAS"] as [String : Any]
            tableView.reloadData()
        }
    }
    
    func removeComplementsEdit(indexPath: IndexPath, cant: Int) {
        for z in 0..<numComplementSelected.count {
            if numComplementSelected[z] == 0 {
                extrasCounter[z] = 0
            }
        }
        let cantidad = (cant - 1) //< 0 ? 0:cant - 1
        if cantidad >= 0 {
            numComplementSelected[indexPath.row] = cantidad
            //MARK:WORK ON IT
            //Anterior
//        totalComplements = Double(cantidad) * Double(complements[indexPath.row].value)!
            totalComplements =  Double(cantidad)  * Double(complements[indexPath.row].value)!

        if cantidad == 0 {
//            // COMPROBAMOS SI YA HABIA COMPLEMENTO (SI cantStoreComplements.count ES 0 VIENE DEL CARRITO SIN COMPLEMENTO)
//            // Y si la cantidad de productos es mayor a 1, si solo es un producto no se ace operacion
//            if cantStoreComplements.count > 0 {
//
//               if (order_elements["quantity"] as! Int) > 1{
//                    //VALIDACION PARA RESTAR O NO AL ROW
//                    if indexPath.row > 0{
//                        //ACTULIZAMOS EL PECIO DEL PRODUCTO
//                        order_elements["price"] =  precioAuxiliar - (complementsArrayPrice[indexPath.row - 1] * Double((order_elements["quantity"] as! Int)))
//
//                    }else{
//
//                        order_elements["price"] = precioAuxiliar - (complementsArrayPrice[indexPath.row] * Double((order_elements["quantity"] as! Int)))
//                    }
//
//
//                }else{
//
//                    print("No se hizo ninguna peracion 1")
//                }
//            }else{
//                print("No se hizo ninguna peracion 2")
//
//            }
            
                complementsArrayString.remove(at: complementsArrayId.index(of: complements[indexPath.row].id)!)
                complementsArrayCant.remove(at: complementsArrayId.index(of: complements[indexPath.row].id)!)
                complementsArrayPrice.remove(at: complementsArrayId.index(of: complements[indexPath.row].id)!)
                complementsArrayId.remove(at: complementsArrayId.index(of: complements[indexPath.row].id)!)
            
            
            
            }else{
                complementsArrayString[complementsArrayId.index(of: complements[indexPath.row].id)!] = complements[indexPath.row].complement_name as! String
                complementsArrayCant[complementsArrayId.index(of: complements[indexPath.row].id)!] = cantidad
                complementsArrayPrice[complementsArrayId.index(of: complements[indexPath.row].id)!] = totalComplements
                complementsArrayId[complementsArrayId.index(of: complements[indexPath.row].id)!] = complements[indexPath.row].id as! Int
            }
            dictionaryComplements = ["complement_name": complementsArrayString, "complement_price": complementsArrayPrice, "complement_cant": complementsArrayCant, "complement_id": complementsArrayId, "category": "EXTRAS"] as [String : Any]
//
            tableView.reloadData()
        }else{
            
//            totalComplements = 0
            
        }
        if numComplementSelected[indexPath.row] == 0 {
            extrasCounter[indexPath.row] = 0
        }
        
    }
    func editMyOrderSend(index: Int, idItem: Int) {
        var requiredInfo = 0
        var requiredInfo2 = 0
        for x in 0..<modifiers.count {
            if requiredCells[x], required[x] {
                requiredInfo += 1
            }
            if required[x] {
                requiredInfo2 += 1
            }
        }
        if requiredInfo == requiredInfo2 {
//                let dictionary = ["name": order_elements["name"] as! String, "quantity": cantidadItems, "price": totalModfiers * Double(cantidadItems) , "complex": true, "complements": dictionaryComplements, "modifiers": dictionaryModifiers, "id": order_elements["id"] as! Int] as [String : Any]
            let dictionary = ["name": order_elements["name"] as! String, "quantity": cantidadItems, "price": order_elements["price"] as! Double , "complex": true, "complements": dictionaryComplements, "modifiers": dictionaryModifiers, "id": order_elements["id"] as! Int, "price_product":order_elements["price_product"] as! Double, "image": order_elements["image"] as! String ] as [String : Any]
                dictionaryInfo[itemEdited] = dictionary as! Dictionary<String,Any>
                print(dictionaryInfo)

                let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
                let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
                let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName, order_elements: dictionaryInfo, establishment_id: establishmentId, notes: notes)
                self.delegate.reloadOrders()
                self.dismiss(animated: true, completion: nil)
        }else{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.subTitleMessageString = "Por favor, escoge un producto obligatorio para poder continuar"
            newXIB.errorMessageString = "Producto obligatorio"
            present(newXIB, animated: true, completion: nil)
        }
    }
    func didSuccessViewSingleProduct(item: Items) {
        if let limExt = item.limit_extra {
            limit_extra = limExt
        }
        
        if (item.modifiers?.count)! > 0 {
            for i in 0..<(item.modifiers?.count)! {
                var modifiers = [String]()
                for _ in 0..<(item.modifiers![i].modifiers.count) {
                    modifiers.append("modifiers")
                }
                self.modifiers.append(item.modifiers![i].modifiers)
                modifiers_name.append(item.modifiers![i].modifiersName)
                itemsInTable.append(modifiers)
                max_required.append(item.modifiers![i].max_required)
                required.append(item.modifiers![i].requiredInfo)
            }
        }
        
        if (item.complements?.count)! > 0 {
            
            for i in 0..<(item.complements?.count)! {
                var complements = [String]()
                for _ in 0..<(item.complements![i].complements.count) {
                    complements.append("complements")
                }
                itemsInTable.append(complements)
                self.complements = item.complements![0].complements
                nameComplements.append(item.complements![i].complement_name)
            }
            
        }
        for y in 0..<complements.count {
            self.extrasCounter.append(0)
        }
        let notesAndAdd = ["notas","addOrder"]
        itemsInTable.append(notesAndAdd)
        //        MARK: WORK ON IT
        for i in 0..<modifiers.count {
            var selectedCell = [Bool]()
            for j in 0..<modifiers[i].count {
                if  idStoreModifiers[i].contains(modifiers[i][j].id) {
                    selectedCell.append(true)
                }else{
                    selectedCell.append(false)
                }
                print(selectedCell)
            }
            selectedCells.append(selectedCell)
            requiredCells = required
        }
        
//        numComplementSelected.removeAll()
//
//        for i in 0..<complements.count {
//
//            numComplementSelected.append(0)
//
//        }
        numComplementSelected.removeAll()
        for i in 0..<complements.count {
            //            MARK: WORK ON IT
            if idStoreComplements.count > 0 {
                for j in 0..<idStoreComplements.count{
                    if complements[i].id == idStoreComplements[j] {
//                        print(complements[i].id, idStoreComplements[j])
//                        numComplementSelected = cantStoreComplements
                        
                        numComplementSelected.append(cantStoreComplements[j])
                        extrasCounter[i] = 1
                    }else{
                        numComplementSelected.append(0)
                    }
                }
//                print(idStoreComplements, complements[i].id)
                print(numComplementSelected)
            }else{
               numComplementSelected.append(0)
            }
        }
        tableView.reloadData()
        
    }

    func didFailViewSingleProduct(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
}
extension editComplexItemsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return itemsInTable.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsInTable[section].count 
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if itemsInTable[indexPath.section][indexPath.row] == "modifiers" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editModifiers", for: indexPath) as! EditModifiersTableViewCell
            cell.nameModifier.text = modifiers[indexPath.section][indexPath.row].modifier_name
            if modifiers[indexPath.section][indexPath.row].value == 0.00 {
                cell.priceLabel.text = ""
            }else{
                cell.priceLabel.text = "+ $"+modifiers[indexPath.section][indexPath.row].value.description + "0"
            }
            if modifiers[indexPath.section][indexPath.row].is_active {
                cell.agotado.alpha = 0
                cell.nameModifier.textColor = UIColor.init(red: 90/255, green: 105/255, blue: 118/255, alpha: 1)
                cell.priceLabel.textColor = UIColor.init(red: 90/255, green: 105/255, blue: 118/255, alpha: 1)
                cell.setButtonModifiers.isEnabled = true
            }else{
                cell.agotado.alpha = 1
                cell.nameModifier.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.priceLabel.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.setButtonModifiers.isEnabled = false
            }
            
            cell.setButtonModifiers.isSelected = selectedCells[indexPath.section][indexPath.row]
            cell.selectionStyle = .none
            return cell
        }else if itemsInTable[indexPath.section][indexPath.row] == "complements" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editComplements", for: indexPath) as! EditComplementsTableViewCell
            //            MARK: WORK ON IT
            cell.complementName.text = complements[indexPath.row].complement_name
            if complements[indexPath.row].value == "0.00" {
                cell.priceComplements.text = ""
            }else{
                cell.priceComplements.text = "$\(complements[indexPath.row].value!)"
            }
            if complements[indexPath.row].in_stock {
                cell.agotadoImg.alpha = 0
                cell.complementName.textColor = UIColor.init(red: 90/255, green: 105/255, blue: 118/255, alpha: 1)
                cell.itemsCant.textColor = UIColor.init(red: 90/255, green: 105/255, blue: 118/255, alpha: 1)
                cell.priceComplements.textColor = UIColor.init(red: 90/255, green: 105/255, blue: 118/255, alpha: 1)
                cell.removeButton.isEnabled = true
                cell.addButton.isEnabled = true
            }else{
                cell.agotadoImg.alpha = 1
                cell.complementName.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.itemsCant.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.priceComplements.textColor = UIColor.init(red: 192/255, green: 195/255, blue: 204/255, alpha: 1)
                cell.removeButton.isEnabled = false
                cell.addButton.isEnabled = false
            }
            
            //OSCAR PARA MOSTRAR LOS COMPLEMENTOS ACTUALES
            //            if numComplementSelected.count >  complements.count {
          
                if numComplementSelected[indexPath.row] > 0{
                    cell.itemsCant.text = "\(numComplementSelected[indexPath.row])"
                }else{
                    cell.itemsCant.text = "0"
                }
  
                        
            if numComplementSelected.count >= complements.count {
                cell.cant = numComplementSelected[indexPath.row]
            }else{
                cell.cant = 0
            }
            
            cell.indexPath = indexPath
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else if itemsInTable[indexPath.section][indexPath.row] == "notas"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "notesCellXib", for: indexPath) as! NotasEditTableViewCell
//           cell.notesEdit.text = UserDefaults.standard.object(forKey: "notas") as! String
            cell.notesEdit.delegate = self
            cell.selectionStyle = .none
            cell.visibility = .gone
            return UITableViewCell()
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "addToMyEditedOrder", for: indexPath) as! AddToMyOrderEditTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.totalPrice.text = "$\(totalComplements + totalModfiers)"
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if itemsInTable[indexPath.section][indexPath.row] == "modifiers" {
            print(modifiersArrayString[indexPath.section].count)
            totalModfiers = Double(modifiers[indexPath.section][indexPath.row].value!)
            if selectedCells[indexPath.section][indexPath.row] {
                let cell = tableView.cellForRow(at: indexPath) as! EditModifiersTableViewCell
                cell.setButtonModifiers.isSelected = false
                selectedCells[indexPath.section][indexPath.row] = false
                if modifiersArrayPrice[indexPath.section].contains(totalModfiers), modifiersArrayString[indexPath.section].contains(modifiers[indexPath.section][indexPath.row].modifier_name as! String) {
                    let index = modifiersArrayString[indexPath.section].index(of: modifiers[indexPath.section][indexPath.row].modifier_name as! String)
                    modifiersArrayString[indexPath.section].remove(at: index!)
                    modifiersArrayPrice[indexPath.section].remove(at: index!)
                    modifiersArrayId[indexPath.section].remove(at: index!)
                }
                if required[indexPath.section] {
                    requiredCells[indexPath.section] = false
                }
            }else{
                if modifiersArrayString[indexPath.section].count < max_required[indexPath.section] {
                    let cell = tableView.cellForRow(at: indexPath) as! EditModifiersTableViewCell
                    cell.setButtonModifiers.isSelected = true
                    selectedCells[indexPath.section][indexPath.row] = true
                    if !modifiersArrayPrice[indexPath.section].contains(totalModfiers), !modifiersArrayString[indexPath.section].contains(modifiers[indexPath.section][indexPath.row].modifier_name as! String) {
                        modifiersArrayString[indexPath.section].append(modifiers[indexPath.section][indexPath.row].modifier_name as! String)
                        modifiersArrayPrice[indexPath.section].append(totalModfiers)
                        modifiersArrayId[indexPath.section].append(modifiers[indexPath.section][indexPath.row].id as! Int)
                    }
                }
                if required[indexPath.section] {
                    requiredCells[indexPath.section] = true
                }
            }
            
            //            MARK: WORK ON IT
            
//                        insertar en el dictionario los modificadores
            dictionaryModifiers[indexPath.section] = ["modifier_name": modifiersArrayString[indexPath.section], "modifier_price": modifiersArrayPrice[indexPath.section], "modifier_id": modifiersArrayId[indexPath.section], "category": categoryModifiers[indexPath.section]] as [String:Any]
            let dictionary = ["name": order_elements["name"] as! String, "quantity": cantidadItems, "price": totalModfiers * Double(cantidadItems) , "complex": true, "complements": dictionaryComplements, "modifiers": dictionaryModifiers, "id": order_elements["id"] as! Int] as [String : Any]
            dictionaryInfo[itemEdited] = dictionary as! Dictionary<String,Any>

            print(dictionaryInfo)
            let establishmentId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            let establishmentName = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
            if required[indexPath.section] {
                if requiredCells[indexPath.section] {
                    DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName,  order_elements: dictionaryInfo, establishment_id: establishmentId, notes: notes)
                }
            }else{
                DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establishmentName,  order_elements: dictionaryInfo, establishment_id: establishmentId, notes: notes)
            }
            
//            UserDefaults.standard.set(dictionaryInfo, forKey: "order_elements")
//            var ordr_elemnts = UserDefaults.standard.object(forKey: "order_elements") as! [Dictionary<String, Any>]
//            print(ordr_elemnts)

            tableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if itemsInTable[section].contains("modifiers") {
            let header = HeaderCellViewController()
            header.sectionString = modifiers_name[section]
            if required[section] {
                 header.alphaObligatory = 1
            }else{
                 header.alphaObligatory = 0
            }
            header.maxAlpha = 1
            header.maxChooseString = ("Máximo a elegir: \(max_required[section])")
            return header.view
        }else if itemsInTable[section].contains("complements") {
            let header = HeaderCellViewController()
            header.sectionString = "\(nameComplements[section - modifiers_name.count])"
            header.maxChooseString = ("Máximo a elegir: \(limit_extra)")
            header.alphaObligatory = 0
            header.maxAlpha = 1
            return header.view
        }else{
            let header = HeaderCellViewController()
//            header.sectionString = "Notas"
            header.alphaObligatory = 0
            header.maxAlpha = 0
            return header.view
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if itemsInTable[indexPath.section][indexPath.row] == "modifiers" {
            return 46
        }else if itemsInTable[indexPath.section][indexPath.row] == "complements" {
            return 46
        }else if itemsInTable[indexPath.section][indexPath.row] == "notas"{
            return 0
        }else{
            return 107
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
extension editComplexItemsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        UserDefaults.standard.set(textField.text, forKey: "notas")
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 40 // Bool
    }
}
