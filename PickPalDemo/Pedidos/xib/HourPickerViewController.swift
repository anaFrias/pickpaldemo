//
//  HourPickerViewController.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/10/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
protocol hourScheduledDelegate {
    func hourSchedule(hrFancy: String, hourWS: String, date: String, dateCheck: String)
}

class HourPickerViewController: UIViewController, ordersDelegate {

    @IBOutlet weak var datePicker: UIDatePicker!
    var dateLimit = Date()
    var dateScheduled = String()
    var dateScheduled24 = String()
    var dateScheduledDate = String()
    var dateScheduleFecha = String()
    var delegate: hourScheduledDelegate!
    var order = [Dictionary<String,Any>]()
    var orderws = OrdersWS()
    
    var standInMidnight = true
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateMimim = Date()
        UserDefaults.standard.set(dateMimim, forKey: "date_saved")
        datePicker.maximumDate = dateLimit
    
//        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 100)
        datePicker.addTarget(self, action: #selector(HourPickerViewController.handler(_:)), for: .valueChanged)
        orderws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        orderws.checktimeOrder(establishmentId: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id), client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, order: order)
        // Do any additional setup after loading the view.
    }
    func didSuccessGetCheckTimeOrder(total_time:Int) {
        LoadingOverlay.shared.hideOverlayView()
        let dateMimim = Date()
        let calendar = Calendar.current
        let date = calendar.date(byAdding:.minute, value: total_time, to: dateMimim)!
        datePicker.setDate(date, animated: true)
        datePicker.minimumDate = date
//        UserDefaults.standard.set(date, forKey: "date_saved")
        let timeFormatter = DateFormatter()
        //        timeFormatter.timeStyle = .short
        timeFormatter.locale = Locale(identifier: "en_US_POSIX")
        timeFormatter.dateFormat = "h:mm a"
        timeFormatter.amSymbol = "AM"
        timeFormatter.pmSymbol = "PM"
        let strDate = timeFormatter.string(from: datePicker.date)
        print(strDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let hourString = formatter.string(from: datePicker.date)
        let formatterDate = DateFormatter()
        formatterDate.dateFormat = "dd/MM/yyyy"
        let stringDate = formatterDate.string(from: datePicker.date)
        let formatterDate2 = DateFormatter()
        formatterDate2.dateFormat = "yyyy-MM-dd"
        let stringDate2 = formatterDate2.string(from: datePicker.date)
        print(stringDate2)
       
        dateScheduled = strDate
        dateScheduled24 = hourString
        dateScheduledDate = stringDate
        dateScheduleFecha = stringDate2
    }
    func didFailGetCheckTimeOrder(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    @objc func handler(_ sender: UIDatePicker) {
        let date = Date()
        let formatterDate2 = DateFormatter()
        formatterDate2.dateFormat = "yyyy-MM-dd"
        let stringDate2 = formatterDate2.string(from: datePicker.date)
        let stringDate3 = formatterDate2.string(from: date)
        print(stringDate2, stringDate3)
        let customDate = stringDate2 + " 00:00:00"
        let formatoDate = DateFormatter()
        formatoDate.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        print(formatoDate.date(from: customDate))
        
        if stringDate3 != stringDate2 {
            if formatoDate.date(from: customDate) != nil {
                if standInMidnight {
                    datePicker.setDate(formatoDate.date(from: customDate)!, animated: true)
                    datePicker.maximumDate = dateLimit
                    
                    standInMidnight = false
                }
            }
        }else{
            standInMidnight = true
        }
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        let strDate = timeFormatter.string(from: datePicker.date)
        print(strDate)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let formatterDate = DateFormatter()
        formatterDate.dateFormat = "dd/MM/yyyy"
        let dateString = formatterDate.string(from: datePicker.date)
        let hourString = formatter.string(from: datePicker.date)
        print(hourString)
        dateScheduledDate = dateString
        dateScheduled = strDate
        dateScheduled24 = hourString
        dateScheduleFecha = stringDate2
    }
    @IBAction func dismiss(_ sender: Any) {
        self.delegate.hourSchedule(hrFancy: dateScheduled, hourWS: dateScheduled24, date: dateScheduledDate, dateCheck: dateScheduleFecha)
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
