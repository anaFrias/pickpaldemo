//
//  AlertSelectTablet.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol SelectTabletDelgate {
    func openScaner()
    func sendFolio(folio: String)
}

class AlertSelectTablet: UIViewController {

    @IBOutlet weak var textFileFolio: UITextField!
    @IBOutlet weak var lblFolioFail: UILabel!
    @IBOutlet weak var textFolio: UITextField!
    @IBOutlet weak var lblTextQRFail: UILabel!
    
    var visibleFolio = false
    var visibleQR = false
    var delegate:SelectTabletDelgate!
    var textoFail = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblFolioFail.visibility = visibleFolio ? .visible: .gone
        
        
        lblTextQRFail.visibility = visibleQR ? .visible: .gone

    }
    
    
    
    @IBAction func btnScanner(_ sender: Any) {
        
        delegate.openScaner()
        self.dismiss(animated: true, completion: nil)
        
        print("Scanner")
        
    }
    
    
    @IBAction func btnSelectMesa(_ sender: Any) {
       
        if textFileFolio.text! != ""{
            delegate.sendFolio(folio: textFileFolio.text!)
        }
        self.dismiss(animated: true, completion: nil)
        
        
        
        
    }
    
    @IBAction func btnCloseAlert(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
