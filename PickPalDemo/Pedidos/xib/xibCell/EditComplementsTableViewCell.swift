//
//  EditComplementsTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol addRemoveComplementsEdit {
    func addComplementsEdit(indexPath: IndexPath, cant: Int)
    func removeComplementsEdit(indexPath: IndexPath, cant: Int)
}
class EditComplementsTableViewCell: UITableViewCell {

    @IBOutlet weak var itemsCant: UILabel!
    @IBOutlet weak var complementName: UILabel!
    @IBOutlet weak var priceComplements: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var agotadoImg: UIImageView!
    
    
    var cant = Int()
    var delegate: addRemoveComplementsEdit!
    var indexPath = IndexPath()
    override func awakeFromNib() {
        super.awakeFromNib()
        removeButton.layer.cornerRadius = 3
        addButton.layer.cornerRadius = 3
        // Initialization code
    }

    @IBAction func addComplement(_ sender: Any) {
        self.delegate.addComplementsEdit(indexPath: indexPath, cant: cant)
    }
    @IBAction func removeComplement(_ sender: Any) {
        self.delegate.removeComplementsEdit(indexPath: indexPath, cant: cant)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
