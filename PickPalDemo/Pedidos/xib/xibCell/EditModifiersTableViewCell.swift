//
//  EditModifiersTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class EditModifiersTableViewCell: UITableViewCell {

    @IBOutlet weak var setButtonModifiers: UIButton!
    @IBOutlet weak var nameModifier: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var agotado: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
