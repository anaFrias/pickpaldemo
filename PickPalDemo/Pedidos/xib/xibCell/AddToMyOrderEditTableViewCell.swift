//
//  AddToMyOrderEditTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol editMyOrderDelegate {
    func editMyOrderSend(index: Int, idItem: Int)
}
class AddToMyOrderEditTableViewCell: UITableViewCell {
    var delegate: editMyOrderDelegate!
    var index = Int()
    var id_item = Int()
    
    @IBOutlet weak var totalPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func editMyOrder(_ sender: Any) {
        self.delegate.editMyOrderSend(index: index, idItem: id_item)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
