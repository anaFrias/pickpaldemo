//
//  HeaderCellViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class HeaderCellViewController: UIViewController {

    @IBOutlet weak var obligatory: UIImageView!
    @IBOutlet weak var maxChoose: UILabel!
    @IBOutlet weak var sectionTitle: UILabel!
    var sectionString = String()
    var alphaObligatory = CGFloat()
    var maxChooseString = String()
    var maxAlpha = CGFloat()
    override func viewDidLoad() {
        super.viewDidLoad()
        sectionTitle.text = sectionString
        obligatory.alpha = alphaObligatory
        maxChoose.alpha = maxAlpha
        maxChoose.text = maxChooseString
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
