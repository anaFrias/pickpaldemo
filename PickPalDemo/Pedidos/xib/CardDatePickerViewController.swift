//
//  CardDatePickerViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 18/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol getDateCard {
    func sendDate ( month: String, year: String)
}
class CardDatePickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    @IBOutlet weak var viewListo: UIView!
    var delegate: getDateCard?
    var pickerData = [[String]]()
    @IBOutlet weak var picker: UIPickerView!
    let date = Date()
    let calendar = Calendar.current
    var year = Int()
    var month = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewListo.layer.borderColor = UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1).cgColor
        viewListo.layer.borderWidth = 0.5
        
        year = calendar.component(.year, from: date)
        month = calendar.component(.month, from: date)
        for i in 0..<2{
            var yearMonth = [String]()
            for j in 0..<12 {
                if i == 0 {
                    switch j {
                    case 0...8:
                        yearMonth.append("0\(j + 1)")
                        break
                    default:
                        yearMonth.append("\(j + 1)")
                        break
                    }
                }else{
                    yearMonth.append("\(year + j)")
                }
            }
            pickerData.append(yearMonth)
        }
        print(pickerData)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData[component].count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[component][row]
    }
    
    @IBAction func setDay(_ sender: Any) {
        let month = picker.selectedRow(inComponent: 0)
        let year = picker.selectedRow(inComponent: 1)
        self.delegate?.sendDate(month: pickerData[0][month], year: pickerData[1][year])
        self.dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
