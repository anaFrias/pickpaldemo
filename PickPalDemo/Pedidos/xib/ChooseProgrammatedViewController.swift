//
//  ChooseProgrammatedViewController.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 7/29/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol programmatedChooserDelegate {
    func getValueProgrammed(in_site: Bool)
}

class ChooseProgrammatedViewController: UIViewController {

    var delegate: programmatedChooserDelegate!
    var values = ["Para Llevar", "Para Aquí"]
    var in_site = Bool()
    @IBOutlet weak var pickerView: UIPickerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if values[0] == "Para Llevar" {
            in_site = false
        }else{
            in_site = true
        }
        
        // Do any additional setup after loading the view.
        pickerView.reloadAllComponents()
    }

    @IBAction func doneBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.getValueProgrammed(in_site: self.in_site)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChooseProgrammatedViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return values.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return values[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if values[row] == "Para Llevar" {
            self.in_site = false
        }else{
            self.in_site = true
        }
        
    }
}
