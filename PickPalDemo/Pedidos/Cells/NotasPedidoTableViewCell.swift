//
//  NotasPedidoTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 22/08/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol inputNotesEntry {
    func finishInputNotes(code: String)
}
class NotasPedidoTableViewCell: UITableViewCell, UITextViewDelegate {

    
    @IBOutlet weak var notes: UITextView!
    var delegate: inputNotesEntry!
    override func awakeFromNib() {
        super.awakeFromNib()
        notes.delegate = self

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        notes.layer.borderColor = UIColor.lightGray.cgColor
        notes.layer.borderWidth = 1
        // Configure the view for the selected state
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        delegate.finishInputNotes(code: notes.text!)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
                         
            delegate.finishInputNotes(code: notes.text!)

     }
    
}
