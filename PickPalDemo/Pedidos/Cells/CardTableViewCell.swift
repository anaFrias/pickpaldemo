//
//  CardTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var cardimage: UIImageView!
    @IBOutlet weak var cardNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
