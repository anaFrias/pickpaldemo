//
//  OptionFacturaTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class OptionFacturaTableViewCell: UITableViewCell {

    @IBOutlet weak var tuFactura: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var txtOption: UILabel!
    
     var menssageFactura = String()
    override func awakeFromNib() {
        super.awakeFromNib()
        tuFactura.text = menssageFactura
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
        
        // Configure the view for the selected state
    }

}
