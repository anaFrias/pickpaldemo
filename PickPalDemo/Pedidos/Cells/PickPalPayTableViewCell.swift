//
//  PickPalPayTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/21/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol PickPalPayDelegate {
    func onChangeCheck(enable: Bool)
    func onEnterMaxPoints()
    func onChangeEnterPoints(points: Double)
}

class PickPalPayTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var imgCheckYes: UIImageView!
    @IBOutlet weak var imgCheckNo: UIImageView!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var lblPriceTransaction: UILabel!
    @IBOutlet weak var tfPointsEnter: UITextField!
    
    var delegate: PickPalPayDelegate!
    var mMyReward: MyRewardEstablishment!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tfPointsEnter.delegate = self
        lblPriceTransaction.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func OnUsePoint(_ sender: Any) {
        self.delegate.onChangeCheck(enable: true)
        imgCheckNo.image = UIImage(named: "emptyRoundedCheckBox")
        imgCheckYes.image = UIImage(named: "fillRoundedCheckBox")
        tfPointsEnter.isEnabled = true
        
        
    }
    
    @IBAction func OnNotUsePoints(_ sender: Any) {
        self.delegate.onChangeCheck(enable: false)
        imgCheckNo.image = UIImage(named: "fillRoundedCheckBox")
        imgCheckYes.image = UIImage(named: "emptyRoundedCheckBox")
        tfPointsEnter.isEnabled = false
    }
    
    @IBAction func onChangePoint(_ sender: Any) {
        print(tfPointsEnter.text!)
        if tfPointsEnter.text!.count > 0{
            let characters = Double(tfPointsEnter.text ?? "0")
            if  (mMyReward.totalPoints ?? 0).isLess(than: characters ?? 0){
                delegate.onChangeEnterPoints(points: 0)
                delegate.onEnterMaxPoints()
                tfPointsEnter.text = ""
            }else{
                delegate.onChangeEnterPoints(points: characters ?? 0)
                lblPriceTransaction.isHidden = false
                let doubleStr = String(format: "%.2f", mMyReward.tipoCambio )
                let points = tfPointsEnter.text ?? "0"
                lblPriceTransaction.text = "Costo de transacción: \(points) pts / $\(doubleStr)"
            }
        }else{
            lblPriceTransaction.isHidden = true
        }
    }
}
