//
//  SelectAddressTableViewCell.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 06/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit


protocol selectAddressProtocol {
    func addAddress()
    func getPercentOf(percent: Double)
}

class SelectAddressTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, passPercent {
    
    
    @IBOutlet weak var containerInfoAddress: UIView!
    @IBOutlet weak var labelAdressSelecte: UILabel!
    

    @IBOutlet weak var cllectionPropinaRepartidor: UICollectionView!
    var selectedItems = [Bool]()
    var delegate: selectAddressProtocol!
    var propina = String()
    var dynamicWidth =  85
    var itemSelect = Int()
    var autoScroll = true
    //var propinaDefault = Int()
    @IBOutlet weak var textoPropina: UILabel!
    var porcent = Int()
    @IBOutlet weak var lbCostoEnvio: UILabel!
    @IBAction func btnAddAddress(_ sender: Any) {
        
        delegate.addAddress()
       
    }
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cllectionPropinaRepartidor.delegate = self
        cllectionPropinaRepartidor.dataSource = self
        // Se inicializa para que no se seleione ninguno por defaut hasta que se obtenga el valor del WS
        itemSelect = 15
        
        containerInfoAddress.visibility = .gone
       
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
           if textField.text != "" {
            let percentaje = (Double(textField.text!.replacingOccurrences(of: "%", with: ""))!)/100
               propina = textField.text!.replacingOccurrences(of: "%", with: "") + "%"
               dynamicWidth = 170
               itemSelect = 5
               cllectionPropinaRepartidor.reloadItems(at: [IndexPath(item: 5, section: 0)])
            self.delegate.getPercentOf(percent: Double(percentaje))
           }
       }
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           guard let text = textField.text else { return true }
           let newLength = text.characters.count + string.characters.count - range.length
           return newLength <= 3 // Bool
       }
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        
        print("indexPath  \(indexPath.row) porcent \(porcent) ")
        
       if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tenPercent", for: indexPath) as! RewardCollectionViewCell
           
                if itemSelect == 0 || porcent == 10{
                    cell.background.image = UIImage(named: "greenBackground")
                    cell.valueText.textColor = UIColor.white
                    
                    
                }else{
                    cell.background.image = UIImage(named: "borderGrayLeft")
                   
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    
                    
                }
            
            return cell
        }else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fifteenPercent", for: indexPath) as! RewardCollectionViewCell
           
                if itemSelect == 1 || porcent == 15{
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    cell.valueText.textColor = UIColor.white
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                     
                        autoScroll = false
                    }
                   
                }else{
                    cell.background.image = UIImage(named: "middleGrayLine")
                    
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            return cell
        }else if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twentyPercent", for: indexPath) as! RewardCollectionViewCell
            
                if itemSelect == 2 || porcent == 20{
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    cell.valueText.textColor = UIColor.white
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                     
                        autoScroll = false
                    }
                    
                }else{
                    cell.background.image = UIImage(named: "middleGrayLine")
                   
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            return cell
        }else if indexPath.row == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twentyfivePercent", for: indexPath) as! RewardCollectionViewCell
            
                if itemSelect == 3 || porcent == 25{
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    cell.valueText.textColor = UIColor.white
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                     
                        autoScroll = false
                    }
                    
                }else{
                    cell.background.image = UIImage(named: "middleGrayLine")
                    
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            return cell
        }else if indexPath.row == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thirtyPercent", for: indexPath) as! RewardCollectionViewCell
            
                if itemSelect == 4 || porcent == 30{
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    cell.valueText.textColor = UIColor.white
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                        
                        autoScroll = false
                    }
                }else{
                    cell.background.image = UIImage(named: "middleGrayLine")
                   
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            return cell
        }else if indexPath.row == 5 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherPercent", for: indexPath) as! RewardCollectionViewCell
          
                if itemSelect == 5 {
                    
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                     
                        autoScroll = false
                    }
                    cell.valueText.textColor = UIColor.white
                    cell.textInput.backgroundColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
                    cell.textInput.textColor = UIColor.white
                    
                }else{
                    
                    cell.background.image = UIImage(named: "middleGrayLine")
                    
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                     cell.textInput.backgroundColor = UIColor.white
                    cell.textInput.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            cell.textInput.text = propina
            cell.textInput.delegate = self
            return cell
        }else {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "notNowPercent", for: indexPath) as! RewardCollectionViewCell
           
                if itemSelect == 6{
                    cell.background.image = UIImage(named: "greenBackgroundMiddle")
                    cell.valueText.textColor = UIColor.white
                    if autoScroll{
                        
                        self.cllectionPropinaRepartidor?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                     
                        autoScroll = false
                    }
                    
                }else{
                    cell.background.image = UIImage(named: "middleGrayLine")
                    cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                }
            
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        porcent = 0
        itemSelect = indexPath.row
        
          let currentCell = collectionView.cellForItem(at: indexPath) as! RewardCollectionViewCell

        
        switch indexPath.row {
        
        case 0:
            
            currentCell.background.image = UIImage(named: "borderGrayLeft")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
          self.delegate.getPercentOf(percent: 0.1)
            textoPropina.isHidden = false
            
            case 1:
            
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
            self.delegate.getPercentOf(percent: 0.15)
                textoPropina.isHidden = false
            case 2:
            
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
            self.delegate.getPercentOf(percent: 0.2)
                textoPropina.isHidden = false
            case 3:
            
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                textoPropina.isHidden = false
            self.delegate.getPercentOf(percent: 0.25)
            case 4:
            
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                textoPropina.isHidden = false
            self.delegate.getPercentOf(percent: 0.3)
            case 5:
            
            currentCell.background.image = UIImage(named: "borderGray")
//            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
          //   self.textFieldDidEndEditing(currentCell.textInput)
                textoPropina.isHidden = false
            dynamicWidth = 185
            case 6:
                textoPropina.isHidden = true
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
            self.delegate.getPercentOf(percent: 0.0)
        case 7:
            textoPropina.isHidden = true
            currentCell.background.image = UIImage(named: "borderGray")
            currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
            self.delegate.getPercentOf(percent: 0.0)
            
        default:
            textoPropina.isHidden = true
             currentCell.background.image = UIImage(named: "middleGrayLine")
             currentCell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
            
        }
                  
      /*  if indexPath.row == 0 {
            
              currentCell.background.image = UIImage(named: "greenBackground")
              currentCell.valueText.textColor = UIColor.white
                      dynamicWidth = 85
                     // selectedItems[indexPath.item] = true
                      cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                      self.delegate.getPercentOf(percent: 0.1)
            
      }else if indexPath.row == 1 {
            
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                //cllectionPropinaRepartidor.valueText.textColor = UIColor.white
                dynamicWidth = 85
                //selectedItems[indexPath.item] = true
                cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.15)
            
      }else if indexPath.row == 2 {
            
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                // selectedItems[indexPath.item] = true
                cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.2)
            
      }else if indexPath.row == 3 {
            
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                //  selectedItems[indexPath.item] = true
                cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.25)
            
      }else if indexPath.row == 4 {
            
                      currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                      currentCell.valueText.textColor = UIColor.white
                      dynamicWidth = 85
                     // selectedItems[indexPath.item] = true
                      cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                      self.delegate.getPercentOf(percent: 0.3)
            
      }else if indexPath.row == 5 {
            
                      currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                      currentCell.valueText.textColor = UIColor.white
                      currentCell.textInput.becomeFirstResponder()
                      dynamicWidth = 170
                     // selectedItems[indexPath.item] = true
                      cllectionPropinaRepartidor.reloadItems(at: [indexPath])
            
      }else{
            
                      currentCell.background.image = UIImage(named: "greenBackgroundRight")
                      currentCell.valueText.textColor = UIColor.white
                      dynamicWidth = 85
                    //  selectedItems[indexPath.item] = true
                      cllectionPropinaRepartidor.reloadItems(at: [indexPath])
                      self.delegate.getPercentOf(percent: 0.0)
            
      }*/
        
         cllectionPropinaRepartidor.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            if indexPath.item == 5 {
                print("Intem: \(indexPath.item) tamaño: \(dynamicWidth)")
                return CGSize(width: dynamicWidth, height: 50)
            }else{
                print("Intem: \(indexPath.item) tamaño: \(85)")
                return CGSize(width: 85, height: 50)
            }
        
    }
    
    
    func percentTyped(reward: Double) {
        self.delegate.getPercentOf(percent: reward)
    }
    
}
