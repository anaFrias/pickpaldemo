//
//  AddressCell2TableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 22/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol EditAddress2Delegate {
    func clickEdit2Address(indexPath: IndexPath)
}
class AddressCell2TableViewCell: UITableViewCell {

    @IBOutlet weak var rfc: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var edit: UIImageView!
    var delegate: EditAddress2Delegate!
    var indexPath: IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func onClick(_ sender: Any) {
        self.delegate.clickEdit2Address(indexPath: indexPath)
    }

}
