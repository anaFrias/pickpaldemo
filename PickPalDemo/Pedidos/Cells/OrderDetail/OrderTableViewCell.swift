//
//  OrderTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 30/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    var id = 0
    var name: String!
    var cuantiti: Int!
    var position: Int!
    var delegate: actionCarrito!
    var modifier_price: [Double]!
    var modifier_cant: [Int]!
    var modifier_name: [String]!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var precio: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        // Initialization code
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if modifier_name != nil{
            return modifier_name.count //+ 1
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subItemItem", for: indexPath) as! SubItemPedidosTableViewCell
        if(indexPath.row == modifier_name.count){}
        if modifier_cant[indexPath.row] == 1{
            cell.name.text = "- " + modifier_name[indexPath.row]
        }else{
            cell.name.text = "- " + modifier_cant[indexPath.row].description + " " + modifier_name[indexPath.row]
        }
        if modifier_price[indexPath.row] == 0.00 {
            cell.price.text = ""
        }else{
            if modifier_name[indexPath.row] == name {
                cell.price.text = "$" + modifier_price[indexPath.row].description + "0"
            }else{
                cell.price.text = "+ $" + modifier_price[indexPath.row].description + "0"
            }
            
        }
        
        return cell
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
