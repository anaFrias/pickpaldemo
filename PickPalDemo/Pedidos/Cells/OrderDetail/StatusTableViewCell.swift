//
//  StatusTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 30/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class StatusTableViewCell: UITableViewCell {

    @IBOutlet weak var time_pay: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    @IBOutlet weak var checkProcess: UIImageView!
    @IBOutlet weak var time_process: UILabel!
    
    @IBOutlet weak var checkReady: UIImageView!
    @IBOutlet weak var time_ready: UILabel!
    
    @IBOutlet weak var time_delivered: UILabel!
    @IBOutlet weak var checkDelivered: UIImageView!
    
    @IBOutlet weak var checkEnjoy: UIImageView!
    @IBOutlet weak var enjoyFoodView: UIView!
    @IBOutlet weak var deliveredView: UIView!
    @IBOutlet weak var readyView: UIView!
    @IBOutlet weak var processView: UIView!
    @IBOutlet weak var toPayView: UIView!
    @IBOutlet weak var pedidoDemoradoText: UILabel!
    @IBOutlet weak var demoradaImage: UIImageView!
    
    //etiquetas que pueden cambiar
    @IBOutlet weak var porPagarText: UILabel!
    @IBOutlet weak var processo: UILabel!
    @IBOutlet weak var listaText: UILabel!
    @IBOutlet weak var entregadaText: UILabel!
    
//    Etiquetas de referencia y pedido
    @IBOutlet weak var verifRefLabel: UILabel!
    @IBOutlet weak var digitoVerPersonal: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
