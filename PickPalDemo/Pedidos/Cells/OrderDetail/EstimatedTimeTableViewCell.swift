//
//  EstimatedTimeTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 06/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class EstimatedTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var estimatedLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
