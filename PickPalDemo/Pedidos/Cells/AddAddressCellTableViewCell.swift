//
//  AddAddressCellTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol AddAddressDelegate {
    func clickAddAddress(indexPath: IndexPath)
}
class AddAddressCellTableViewCell: UITableViewCell {
    
    var delegate: AddAddressDelegate!
    var indexPath = IndexPath()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onClick(_ sender: Any) {
        self.delegate.clickAddAddress(indexPath: indexPath)
    }
}
