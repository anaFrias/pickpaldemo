//
//  OrderTypeTableViewCell.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
import iOSDropDown
protocol chooseTypeOrderDelegate {
    func chooserOne(llevar: Bool, expand: Bool, selectedItem: Int, isScheduled: Bool, inBar: Bool, percent: Double ,expandItem: String )
    func showValuePicker()
    func getValueProgrammed(in_site: Bool)
    func getPercentOf(percent: Double)
    func getTableChoosen(table: Int)
    func getStatusServiceProgramer(status: String)
    func openAlertTAblet()
    
}

class OrderTypeTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,
UITextFieldDelegate, passPercent, AlertAdsDelegate, carritoDelegate {
    
    
    
    @IBOutlet weak var lblNumberTabletClient: UILabel!
    @IBOutlet weak var btnServicioMasa: UIButton!
    @IBOutlet weak var viewProgramarPedido: UIView!
    @IBOutlet weak var downArrow: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var llevarBtn: UIButton!
    @IBOutlet weak var aquiBtn: UIButton!
    @IBOutlet weak var textoPropina: UILabel!
    

    
    //    MARK: para pedidos en barra
    @IBOutlet weak var viewEnBarra: UIImageView!
    @IBOutlet weak var view5: UIImageView!
    @IBOutlet weak var view8: UIImageView!
    @IBOutlet weak var view10: UIImageView!
    @IBOutlet weak var view15: UIImageView!
    @IBOutlet weak var view20: UIImageView!
    @IBOutlet weak var otroview: UIImageView!
    @IBOutlet weak var ahoraNo: UIImageView!
    
    @IBOutlet weak var cincoPercent: UILabel!
    @IBOutlet weak var ochoPercent: UILabel!
    @IBOutlet weak var diezPercent: UILabel!
    @IBOutlet weak var quincePercent: UILabel!
    @IBOutlet weak var veintePercent: UILabel!
    
    @IBOutlet weak var otroPercent: UILabel!
    @IBOutlet weak var ahoraNoLabel: UILabel!
    @IBOutlet weak var percentValue: UITextField!
    
    //    cambio de propinas
    @IBOutlet weak var collectionPropina: UICollectionView!
    
    @IBOutlet weak var collectionPropina2: UICollectionView!
    @IBOutlet weak var dropDownInput: DropDown!
    
    var bandera  = true
    
    @IBOutlet weak var viewMesa: UIView!
    var numberOfOptions = [Int]()
    var selectedOptions = [Bool]()
    var llevar = true
    var delegate: chooseTypeOrderDelegate!
    var isActive = Bool()
    var llevarString = String()
    var showOptions = false
    var selectedPercent = Int()
    var dynamicWidth: CGFloat = 85
    var selectedItems = [Bool]()
    var propina = String()
    var arrayMesas = [String]()
    var paymentType = Int()
    var serviceType = String()
    var service_available = [String]()
    var itemSelect = Int()
    var carritoView = CarritoViewController()
    @IBOutlet weak var lblAfiliateMesa: UILabel!
    
    var tipoPedido = String()
     var percent = Float()
    var autoScroll = true
    
    var visibilitySM = false
    var numberTablet = Int()
    
    //Elementos En Sucursa
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        collectionPropina.delegate = self
        collectionPropina.dataSource = self
        collectionPropina2.delegate = self
        collectionPropina2.dataSource = self
       
        
        self.heightConstraint.constant = 0
        aquiBtn.alpha = 0
        llevarBtn.alpha = 0
        // Initialization code
        percentValue.delegate = self
        dropDownInput.listHeight = 100
        
       
        
    
    }
    

    
   func adaptarCell(tipo: String) {
 
        
        switch tipo {
        case "RS":
            print("Sucursal")
//            viewProgramarPedido.isHidden = true
        case "SM":
            
             print("Servicio Mesa")
             print("Numero de mesas ", arrayMesas)
            viewMesa.alpha = 1
            viewEnBarra.alpha = 0
//            viewProgramarPedido.isHidden = true
            dropDownInput.optionArray = arrayMesas
             
            dropDownInput.didSelect{(selectedText , index ,id) in
                   self.delegate.getTableChoosen(table: index+1)
     
            }
            
        case "RB":
            print("Servicio Barra")
//            viewProgramarPedido.isHidden = true
            viewMesa.alpha = 0
            viewEnBarra.alpha = 1
            
        case "SD":
            print("Servicio Domicilio")
//            viewProgramarPedido.isHidden = false
            viewMesa.alpha = 0
            viewEnBarra.alpha = 0
            
            
        default:
            print("ninguno rifa")
        }
        
    }

  
    
    func getAvailableService(available_service: [String]) {
        self.service_available = available_service
       
   }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            let percentaje = (Double(textField.text!.replacingOccurrences(of: "%", with: ""))!)/100
            propina = textField.text!.replacingOccurrences(of: "%", with: "") + "%"
            dynamicWidth = 170
            selectedItems[5] = true
            collectionPropina.reloadItems(at: [IndexPath(item: 5, section: 0)])
            self.delegate.getPercentOf(percent: Double(percentaje))
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 3 // Bool
    }
    @IBAction func paraLlevr(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
            self.heightConstraint.constant = 0
            self.aquiBtn.alpha = 0
            self.llevarBtn.alpha = 0
            self.downArrow.transform = CGAffineTransform.identity
        }) { (finished) in
            self.llevar = true
            self.delegate.getValueProgrammed(in_site: true)
        
        }
    }
    func percentTyped(reward: Double) {
        self.delegate.getPercentOf(percent: reward)
    }
    @IBAction func paraAqui(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
            self.heightConstraint.constant = 0
            self.aquiBtn.alpha = 0
            self.llevarBtn.alpha = 0
            self.downArrow.transform = CGAffineTransform.identity
        }) { (finished) in
            self.llevar = false
            self.delegate.getValueProgrammed(in_site: false)
        }
    }
    @IBAction func desplegarValores(_ sender: Any) {
        if showOptions {
            UIView.animate(withDuration: 0.5, animations: {
                self.layoutIfNeeded()
                self.heightConstraint.constant = 0
                self.aquiBtn.alpha = 0
                self.llevarBtn.alpha = 0
                self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            }) { (finished) in
                self.showOptions = false
            }
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.layoutIfNeeded()
                self.heightConstraint.constant = 95
                self.aquiBtn.alpha = 1
                self.llevarBtn.alpha = 1
                self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            }) { (finished) in
                self.showOptions = true
            }
        }
        
    }
 
    @IBAction func cincoPorciento(_ sender: Any) {
        view5.image = UIImage(named: "greenBackground")
        cincoPercent.textColor = UIColor.white
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.05)
        textoPropina.isHidden = false
    }
    @IBAction func ochoPorciento(_ sender: Any) {
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "greenBackgroundMiddle")
        ochoPercent.textColor = UIColor.white
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.08)
        textoPropina.isHidden = false
    }
    @IBAction func diezPorciento(_ sender: Any) {
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "greenBackgroundMiddle")
        diezPercent.textColor = UIColor.white
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.1)
        textoPropina.isHidden = false
    }
    @IBAction func quincePorciento(_ sender: Any) {
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "greenBackgroundRight")
        quincePercent.textColor = UIColor.white
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.15)
        textoPropina.isHidden = false
    }
    @IBAction func veintePorciento(_ sender: Any) {
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "greenBackground")
        veintePercent.textColor = UIColor.white
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.2)
        textoPropina.isHidden = false
    }
    
    @IBAction func otro(_ sender: Any) {
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "greenBackgroundMiddle")
        otroPercent.textColor = UIColor.white
        percentValue.textColor = UIColor.white
        
        ahoraNo.image = UIImage(named: "borderGray")
        ahoraNoLabel.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.text = ""
        percentValue.becomeFirstResponder()
        textoPropina.isHidden = false
        
    }
    @IBAction func ahorano(_ sender: Any) {
        textoPropina.isHidden = true
        view5.image = UIImage(named: "borderGrayLeft")
        cincoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view8.image = UIImage(named: "middleGrayLine")
        ochoPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view10.image = UIImage(named: "middleGrayLine")
        diezPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view15.image = UIImage(named: "borderGray")
        quincePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        view20.image = UIImage(named: "borderGrayLeft")
        veintePercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        otroview.image = UIImage(named: "middleGrayLine")
        otroPercent.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        percentValue.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
        
        ahoraNo.image = UIImage(named: "greenBackgroundRight")
        ahoraNoLabel.textColor = UIColor.white
        percentValue.text = ""
        self.delegate.getPercentOf(percent: 0.0)
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionView {
            return 4
        }else{
            return 7
        }
        
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        adaptarCell(tipo: tipoPedido)
        
    
//           MARK: - Propinas CollectionView
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tenPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackground")
                        cell.valueText.textColor = UIColor.white
                        
                        percent = 0.1
                   
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                    }else{
                        cell.background.image = UIImage(named: "borderGrayLeft")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                return cell
            }else if indexPath.row == 1 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "fifteenPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        percent = 0.15
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                        
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                return cell
            }else if indexPath.row == 2 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twentyPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row]{
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        percent = 0.2
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                return cell
            }else if indexPath.row == 3 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twentyfivePercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        percent = 0.25
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                return cell
            }else if indexPath.row == 4 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thirtyPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        percent = 0.3
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                return cell
            }else if indexPath.row == 5 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        cell.textInput.backgroundColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
                        cell.textInput.textColor = UIColor.white
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                        cell.textInput.backgroundColor = UIColor.white
                        cell.textInput.textColor = UIColor.init(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                cell.textInput.text = propina
                cell.textInput.delegate = self
           
                return cell
            }else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "notNowPercent", for: indexPath) as! RewardCollectionViewCell
                if selectedItems.count > 0{
                    if selectedItems[indexPath.row] {
                        cell.background.image = UIImage(named: "greenBackgroundMiddle")
                        cell.valueText.textColor = UIColor.white
                        if autoScroll{
                            
                            self.collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                         
                            autoScroll = false
                        }
                    }else{
                        cell.background.image = UIImage(named: "middleGrayLine")
                        cell.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                    }
                }
                
                return cell
            }
        
             
        
        
//        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

          //MARK: - Filtro Propinas
            
            let currentCell = collectionView.cellForItem(at: indexPath) as! RewardCollectionViewCell

            for i in 0..<7 {
                selectedItems[i] = false
                if i != indexPath.row {
                    if let item = collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? RewardCollectionViewCell {
                        if i == 0 {
                            item.background.image = UIImage(named: "borderGrayLeft")
                            item.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                        }else if i == 7 {
                            item.background.image = UIImage(named: "borderGray")
                            item.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                        }else{
                            item.background.image = UIImage(named: "middleGrayLine")
                            item.valueText.textColor = UIColor(red: 79/255, green: 93/255, blue: 117/255, alpha: 1)
                        }
                    }
                }
            }
            if indexPath.row == 0 {
                currentCell.background.image = UIImage(named: "greenBackground")
                currentCell.valueText.textColor = UIColor.white
                textoPropina.isHidden = false
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.1)
            }else if indexPath.row == 1 {
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                textoPropina.isHidden = false
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.15)
            }else if indexPath.row == 2 {
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                textoPropina.isHidden = false
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.2)
            }else if indexPath.row == 3 {
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                textoPropina.isHidden = false
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.25)
            }else if indexPath.row == 4 {
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.3)
                textoPropina.isHidden = false
            }else if indexPath.row == 5 {
                currentCell.background.image = UIImage(named: "greenBackgroundMiddle")
                currentCell.valueText.textColor = UIColor.white
                currentCell.textInput.becomeFirstResponder()
                dynamicWidth = 170
                selectedItems[indexPath.item] = true
                textoPropina.isHidden = false
                collectionPropina.reloadItems(at: [indexPath])
            }else{
                currentCell.background.image = UIImage(named: "greenBackgroundRight")
                currentCell.valueText.textColor = UIColor.white
                textoPropina.isHidden = true
                dynamicWidth = 85
                selectedItems[indexPath.item] = true
                collectionPropina.reloadItems(at: [indexPath])
                self.delegate.getPercentOf(percent: 0.0)
            }
           
//        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView {
            //            if numberOfOptions.count == 3 {
            //                return CGSize(width: collectionView.frame.size.width / 3, height: 141.5)
            //            }else if numberOfOptions.count == 2 {
            //                return CGSize(width: collectionView.frame.size.width / 2, height: 141.5)
            //            }else{
            return CGSize(width: 80 * 4, height: 117)
            //            }
        }else{
            
            
            if indexPath.item == 5 {
                print("Intem: \(indexPath.item) tamaño: \(dynamicWidth)")
                return CGSize(width: dynamicWidth, height: 50)
            }else{
                print("Intem: \(indexPath.item) tamaño: \(85)")
                return CGSize(width: 85, height: 50)
            }
        }
    }
    
    
    
    func delegado() {
        
               dropDownInput.optionArray = arrayMesas
                 
               dropDownInput.didSelect{(selectedText , index ,id) in
                      self.delegate.getTableChoosen(table: index+1)
        
               }
       
        
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        vc.delegate = self
       
    }
    
    func seleccionFiltro(seleccion: Int) {
     
        switch seleccion {

            case 0:
                viewProgramarPedido.isHidden = true
                serviceType = "servicioSucursar"
            
            case 1:
                
                viewProgramarPedido.isHidden = true
                serviceType = "servicioEnMesa"
            
            case 2:
               viewProgramarPedido.isHidden = true
                serviceType = "servicioBarra"
            
            case 3:
                viewProgramarPedido.isHidden = false
                serviceType = "servicioDomicilio"
            
            default:
                
                serviceType = ""
            
        }
       
       

    }
    
    
    @IBAction func btnAlertSleectTablet(_ sender: Any) {
        
        delegate.openAlertTAblet()
        
        
    }
    
}
