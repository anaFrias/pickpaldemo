//
//  InvoiceOrderViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 04/02/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class InvoiceOrderViewController: UIViewController {

    
    
    @IBOutlet weak var lblTotal: UILabel!
    
    
    // programado
    
    @IBOutlet weak var containerProgrammed: UIView!
    @IBOutlet weak var lblTotalprogrammed: UILabel!
    @IBOutlet weak var lblHourProgrammed: UILabel!
    @IBOutlet weak var lblDateProgrammed: UILabel!
    
    
    // Variables Globales
    var paymentWS = PaymentWS()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var address = [BillingAddress]()
    var billing_address_id = 0
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var establishment_id = Int()
    var total = Double()
    var payment_source_id = String()
    var flags = [Bool]()
    var selected = -1
    var notes = String()
    var promotionalCode = String()
    var pickpalService = Double()
    var discount = Double()
    var order_elements = [Dictionary<String,Any>]()
    var hourScheduled = String()
    var isScheduled = Bool()
    var inBar = Bool()
    var percent = Double()
    var hourFancy = String()
    var dateScheduled = String()
    var subtotal = Double()
    var paymentType = Int()
    var addressDelivery = Address()
    var shipping_cost = Float()
    var type_order = String()
    var timeDelivery = Int()
    var mPoints = Double()
    var mMoneyPoints = Double()
    var lugar = String()
    var municipio = String()
    var categoria = String()
    var table = Int()
    var facturar = Bool()
    var hourScheduledFormatted = String()
    var myCash = MyCash()
    var pickpal_cash_money = Double()
    var isInsufficientBalance = Bool()
    @IBOutlet weak var lblTotalPagarGeneral: UILabel!
    
    @IBOutlet weak var llblTituloTotalPagar: UILabel!
    @IBOutlet weak var lblTitlService: UILabel!
    
    @IBOutlet weak var containerHour: UIView!
    @IBOutlet weak var btnSiInvoicer: UIButton!
    @IBOutlet weak var btnNoInvoicer: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lblHourProgrammed.text = hourScheduledFormatted
        lblDateProgrammed.text = dateScheduled
        
        
        
        if isInsufficientBalance || mMoneyPoints > 0{
            
        
        lblTotal.text = "\(CustomsFuncs.getFomatMoney(currentMoney:total))"
        lblTotalprogrammed.text = "\(CustomsFuncs.getFomatMoney(currentMoney:total))"
           llblTituloTotalPagar.text = "RESTANTE A PAGAR:"
            lblTotalPagarGeneral.text = "RESTANTE A PAGAR:"
        }else{
            

        lblTotal.text = "\(CustomsFuncs.getFomatMoney(currentMoney:pickpal_cash_money))"
        lblTotalprogrammed.text = "\(CustomsFuncs.getFomatMoney(currentMoney:pickpal_cash_money))"

        }
        
        if isScheduled {
            containerProgrammed.isHidden = false
        }else{
            if type_order == "SM"{
                
                if table > 0 {
                    containerProgrammed.isHidden = false
                    lblTitlService.text = "SERVICIO A MESA:"
                    lblHourProgrammed.text = "Mesa \(table)"
                }else{
                    containerProgrammed.isHidden = true
                }
            }else{
                
                containerProgrammed.isHidden = true
                
            }
        }

    
    }
    
    @IBAction func btnNoInvoicerOrder(_ sender: Any) {
        btnSiInvoicer.setImage(UIImage(named: "emptyRoundedCheckBox.png"), for: .normal)
        btnNoInvoicer.setImage(UIImage(named: "fillRoundedCheckBox.png"), for: .normal)
        facturar = false
        
    }
    

    @IBAction func btnYesInvoicerOrder(_ sender: Any) {
        
        btnNoInvoicer.setImage(UIImage(named: "emptyRoundedCheckBox.png"), for: .normal)
        btnSiInvoicer.setImage(UIImage(named: "fillRoundedCheckBox.png"), for: .normal)
        
        facturar = true
        
    }
    @IBAction func btnGoToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    
    
    @IBAction func btnGoToRegisterOrder(_ sender: Any) {
        
        if facturar{
            //                openpay = nil
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "AddressSelection") as! AddressSelectionViewController
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.payment_source_id = payment_source_id
            newVC.establishment_id = establishment_id
            newVC.total = total
            newVC.notes = notes
            newVC.promotionalCode = promotionalCode
            newVC.discount = discount
            newVC.pickpalService = pickpalService
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.order_elements = order_elements
            newVC.hourScheduled = hourScheduled
            newVC.isScheduled = isScheduled
            newVC.inBar = inBar
            newVC.percent = percent
            newVC.table = table
            newVC.hourFancy = hourScheduledFormatted
            newVC.dateScheduled = dateScheduled
            newVC.mPoints = self.mPoints
            newVC.mMoneyPoints = self.mMoneyPoints
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  self.shipping_cost
            newVC.type_order = self.type_order
            newVC.timeDelivery = self.timeDelivery
            newVC.myCash = self.myCash
            newVC.pickpal_cash_money = pickpal_cash_money
            newVC.isInsufficientBalance = self.isInsufficientBalance
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            //                openpay = nil
            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
            newVC.address_id = 0
            newVC.detail = detail
            newVC.paraAqui = paraAqui
            newVC.establishment_id = establishment_id
            newVC.payment_source_id = payment_source_id
            newVC.total = total
            newVC.notes = notes
            newVC.promotionalCode = promotionalCode
            newVC.discount = discount
            newVC.pickpalService = pickpalService
            newVC.municipio = municipio
            newVC.categoria = categoria
            newVC.lugar = lugar
            newVC.hourScheduled = hourScheduled
            newVC.isScheduled = isScheduled
            newVC.inBar = inBar
            newVC.percent = percent
            newVC.table = table
            newVC.hourFancy = hourScheduledFormatted
            newVC.dateScheduled = dateScheduled
            newVC.mPoints = mPoints
            newVC.mMoneyPoints = mMoneyPoints
            newVC.subtotal = subtotal
            newVC.paymentType = paymentType
            newVC.addressDelivery = self.addressDelivery
            newVC.shipping_cost =  self.shipping_cost
            newVC.type_order = self.type_order
            newVC.timeDelivery = self.timeDelivery
            newVC.myCash = self.myCash
            newVC.pickpal_cash_money = pickpal_cash_money
           
            print("2 Card Select Direccion: " ,  self.addressDelivery.pk)
            print("2 Card Select Pickpla: " ,  self.shipping_cost)
            print("2 Card Select Tipo orden: " ,  self.type_order)
            
            self.navigationController?.pushViewController(newVC, animated: true)
        }
 
    }
    
}
