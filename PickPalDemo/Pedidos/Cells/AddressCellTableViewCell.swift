//
//  AddressCellTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol EditAddressDelegate {
    func clickEditAddress(indexPath: IndexPath)
}
class AddressCellTableViewCell: UITableViewCell {

    @IBOutlet weak var rfc: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var edit: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    var delegate: EditAddressDelegate!
    var indexPath: IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onClick(_ sender: Any) {
        self.delegate.clickEditAddress(indexPath: indexPath)
    }
}
