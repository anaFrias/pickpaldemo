//
//  TotalPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

protocol TotalPedidosDelegate {
    func onChangeCheck(enable: Bool)
    
    func onChangeEnterPoints(points: Double, subTotal: Double)
}

class TotalPedidosTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var total: UILabel!
    
    @IBOutlet weak var imgCheckYes: UIImageView!
    @IBOutlet weak var imgCheckNo: UIImageView!
    
    @IBOutlet weak var viewdivisor: UIView!
    @IBOutlet weak var viewContainerRadioButtons: UIView!
    @IBOutlet weak var lblQuestionPayPoints: UILabel!
    @IBOutlet weak var lblTotalPoints: UILabel!
    @IBOutlet weak var clTopDivisor: NSLayoutConstraint!
    
    @IBOutlet weak var btnUserPoints: UIButton!
    
    @IBOutlet weak var clTopViewDivider: NSLayoutConstraint!
    @IBOutlet weak var viewDivisorHeaderAux: UIView!
    
    var delegate: TotalPedidosDelegate!
    let formatter = NumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
        viewdivisor.frame.size.height = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func OnUsePoint(_ sender: Any) {
        self.delegate.onChangeCheck(enable: true)
        imgCheckNo.image = UIImage(named: "emptyRoundedCheckBox")
        imgCheckYes.image = UIImage(named: "fillRoundedCheckBox")
    }
    
    @IBAction func OnNotUsePoints(_ sender: Any) {
        self.delegate.onChangeCheck(enable: false)
        imgCheckNo.image = UIImage(named: "fillRoundedCheckBox")
        imgCheckYes.image = UIImage(named: "emptyRoundedCheckBox")
    }
}



//subtotal + comission + choosenPercent + picpalService + shoppingCost
