//
//  MessageFacturaCell.swift
//  PickPalDemo
//
//  Created by Oscar Martinez on 06/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class MessageFacturaCell: UITableViewCell {

    @IBOutlet weak var lblMessageFactura: UILabel!
    var amount = Double()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblMessageFactura.text = "Únicamente pedidos mayores a \(CustomsFuncs.getFomatMoney(currentMoney:amount)) podrán ser facturados"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
