//
//  ServicioPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 21/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ServicioPedidosTableViewCell: UITableViewCell {

    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var textInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
