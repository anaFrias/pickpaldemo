//
//  SubItemPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 28/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SubItemPedidosTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
