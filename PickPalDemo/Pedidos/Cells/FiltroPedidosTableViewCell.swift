//
//  FiltroPedidos.swift
//  PickPalTesting
//
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

 @objc protocol pedidosFiltroDelegate {
   @objc optional func menssageDialog(titulo: String, mensaje: String)
    @objc optional func expandSettings(llevar: Bool, expand: Bool, selectedItem: Int, isScheduled: Bool, inBar: Bool, percent: Float, expandItem: String,order_payments: NSDictionary)
   @objc optional func getTypeService(typeService: String)
   func showDatePicker()
   @objc optional func getStatusServiceProgramer(status: String)
}

class FiltroPedidos:  UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, establishmentDelegate {
   
    
    @IBOutlet weak var filtroCollectionView: UICollectionView!
    
    
    //hided stuff
    @IBOutlet weak var horaPedidoLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var currentDate: UILabel! //Aqui se muestra la fecha
    @IBOutlet weak var viewHours: UIView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var imgPedidoProgramado: UIImageView!
    @IBOutlet weak var imgPedidoInmediato: UIImageView!
    @IBOutlet weak var cellConteinerFilter: UIView!
    
   
    var totalWidth = 220.0
    var shadowRadius = CGFloat()
    @IBAction func btnPedidoProgramado(_ sender: Any) {
        
        imgPedidoProgramado.image = UIImage(named: "fillRoundedCheckBox")
        imgPedidoInmediato.image = UIImage(named: "emptyRoundedCheckBox")
        self.delegate!.getStatusServiceProgramer!(status: "pedidoProgramado")
        self.delegate!.expandSettings!(llevar: false, expand: true, selectedItem:3, isScheduled: true, inBar: false, percent: 0.0, expandItem: "pedidoProgramado", order_payments: self.order_payments)
    }
    
    @IBAction func btnPedidoInmediato(_ sender: Any) {
    
        imgPedidoInmediato.image = UIImage(named: "fillRoundedCheckBox")
        imgPedidoProgramado.image = UIImage(named: "emptyRoundedCheckBox")
        self.delegate!.getStatusServiceProgramer!(status: "pedidoInmediato")
        self.delegate?.expandSettings!(llevar: false, expand: true, selectedItem:3, isScheduled: false, inBar: false, percent: 0.0, expandItem: "pedidoInmediato", order_payments: self.order_payments)
//
       
    
    }
    
    
    var service_available = [String]()
    var order_payments = NSDictionary ()
    var itemSelect = String()
    var establishWS = EstablishmentWS ()
    var carritoView = CarritoViewController ()
    var delegate : pedidosFiltroDelegate?
    var numberOfOptions = [Int]()
    var selectedOptions = [Bool]()
    var llevar = true
    var serviceType = String()
    
    @IBOutlet weak var itemFilterConteiner: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filtroCollectionView.delegate = self
        filtroCollectionView.dataSource = self
        establishWS.delegate = self
    
        //conteinerFilter
        shadowRadius = 2
        
       filtroCollectionView.clipsToBounds = false
        filtroCollectionView.layer.masksToBounds = false
        filtroCollectionView.layer.cornerRadius = 5
        filtroCollectionView.layer.shadowColor = UIColor.black.cgColor
        filtroCollectionView.layer.shadowOpacity = 0.35
        filtroCollectionView.layer.shadowOffset = .zero
        filtroCollectionView.layer.shadowRadius = 2
        
        
        filtroCollectionView.clipsToBounds = false
         filtroCollectionView.layer.masksToBounds = false
         filtroCollectionView.layer.cornerRadius = 5
         filtroCollectionView.layer.shadowColor = UIColor.black.cgColor
         filtroCollectionView.layer.shadowOpacity = 0.35
         filtroCollectionView.layer.shadowOffset = .zero
         filtroCollectionView.layer.shadowRadius = 2
        
        
        
        establishWS.getInvoicingByPickpal(establishmentId: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))

   
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
      func didSuccessGetInvoicing(invoicingInfo: Invoicing) {

        print("Caso Servicios ", invoicingInfo.invoicing_by_pickpal)
           
            self.service_available = invoicingInfo.available_service!
        self.order_payments = invoicingInfo.order_payments!
        
//        self.conteinerFilter.frame.size.width = CGFloat(self.totalWidth)
            
         ordenarFiltro(array: service_available)
        
          
           
        }

        func didFailGetInvoicing(error: String, subtitle: String) {
               
            print("Error ", error)
          
            self.delegate!.menssageDialog!(titulo: error, mensaje: subtitle)
            
        }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Filtro tamaño ", self.service_available.count)
        return self.service_available.count
       }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
    //MARK: - Filtro Collectionview
                 switch service_available[indexPath.row] {
                   case "RS":
                       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paraLlevar", for: indexPath) as! ParaLlevarCollectionViewCell

                       if service_available.contains("RS"){
                            cell.isUserInteractionEnabled = true
                            if itemSelect == "RS" {
//                                print("Caso selecionado 0")
                               cell.checked.alpha = 1
                               cell.greenBackground.image = UIImage(named: "greenBackground")
                               cell.iconoParaLlevar.image = UIImage(named: "ic_en_sucursal_blanco")
                               cell.paraLlevarLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                            }else{
                            
//                              print("Caso SIN SELECIONAR 0")
                               cell.backgroundColor = UIColor.white
                               cell.checked.alpha = 0
                               cell.greenBackground.image = UIImage(named: "")
                               cell.iconoParaLlevar.image = UIImage(named: "ic_Shop")
                               cell.paraLlevarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                               
                           }
                        
                       }/*else{
                        
                            cell.isUserInteractionEnabled = false
                            cell.checked.alpha = 0
                            cell.iconoParaLlevar.image = UIImage(named: "ic_en_sucursal")
                            cell.greenBackground.image = UIImage(named: "borderGray")
                            cell.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
                            cell.paraLlevarLbl.textColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)


                        }*/
                       
                       /*cell.clipsToBounds = false
                       cell.layer.masksToBounds = false
                       cell.layer.cornerRadius = 5
                       cell.layer.shadowColor = UIColor.black.cgColor
                       cell.layer.shadowOpacity = 0.35
                       cell.layer.shadowOffset = .zero
                       cell.layer.shadowRadius = shadowRadius*/
                       
                   return cell

                    case "SM":

                           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paraAqui", for: indexPath) as! ParaAquiCollectionViewCell
                           if service_available.contains("SM"){
                            cell.isUserInteractionEnabled = true
                            if  itemSelect == "SM" {
//                                print("Caso selecionado 1")
                                cell.checked.alpha = 1
                                cell.background.image = UIImage(named: "greenBackgroundMiddle")
                                cell.iconoParaAqui.image = UIImage(named: "ic_servicio_mesa_blanco")
                                cell.paraAquiLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)


                            }else{
//                                print("Caso SIN SELECIONAR 1")
                                cell.backgroundColor = UIColor.white
                                cell.background.image = UIImage(named: "")
                                cell.iconoParaAqui.image = UIImage(named: "ic_serviceTable")
                                cell.checked.alpha = 0
                                cell.paraAquiLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)

                            }

                            
                           }/*else{

                            
                                cell.isUserInteractionEnabled = false
                                cell.checked.alpha = 0
                                cell.background.image = UIImage(named: "borderGray")
                                cell.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
                                cell.paraAquiLbl.textColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                                cell.iconoParaAqui.image = UIImage(named: "ic_servicio_mesa")
                            
                            

                            }*/
                           
                           
                           /*cell.clipsToBounds = false
                           cell.layer.masksToBounds = false
                           cell.layer.cornerRadius = 5
                           cell.layer.shadowColor = UIColor.black.cgColor
                           cell.layer.shadowOpacity = 0.35
                           cell.layer.shadowOffset = .zero
                           cell.layer.shadowRadius = shadowRadius*/
                           
                           return cell

                     case "RB":
                            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "barra", for: indexPath) as! EnBarraCollectionViewCell

                            if service_available.contains("RB"){
                                cell.isUserInteractionEnabled = true
                                if itemSelect == "RB" {
                                    cell.checked.alpha = 1
                                    cell.background.image = UIImage(named: "greenBackgroundRight")
                                    cell.iconBarra.image = UIImage(named: "ic_en_barra_blanco")
                                    cell.programarLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)

                                }else{
                                    cell.checked.alpha = 0
                                    cell.backgroundColor = UIColor.white
                                    cell.iconBarra.image = UIImage(named: "ic_getInBar")
                                    cell.background.image = UIImage(named: "")
                                    cell.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)

                                }

                                
                                
                            }/*else{
                                
                                cell.isUserInteractionEnabled = false
                                cell.checked.alpha = 0
                                cell.background.image = UIImage(named: "borderGray")
                                cell.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
                                cell.programarLbl.textColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                                cell.iconBarra.image = UIImage(named: "ic_en_barra")
                                
                            }*/
                            
                            
                            /*cell.clipsToBounds = false
                            cell.layer.masksToBounds = false
                            cell.layer.cornerRadius = 5
                            cell.layer.shadowColor = UIColor.black.cgColor
                            cell.layer.shadowOpacity = 0.35
                            cell.layer.shadowOffset = .zero
                            cell.layer.shadowRadius = shadowRadius*/
                            
                        return cell
                    case "SD":

                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "programar", for: indexPath) as! ProgramarCollectionViewCell
                        if service_available.contains("SD"){
                            cell.isUserInteractionEnabled = true
                            if itemSelect == "SD" {
                            self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 3, isScheduled: true, inBar: false, percent: 0.0, expandItem: "domicilio", order_payments: self.order_payments)
                                cell.checked.alpha = 1
                                cell.background.image = UIImage(named: "greenBackgroundRight")
                                cell.imageClock.image = UIImage(named: "ic_a_domicilio_blanco")
                                cell.programarLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)

                            }else{
                                cell.checked.alpha = 0
                                cell.backgroundColor = UIColor.white
                                cell.imageClock.image = UIImage(named: "ic_serviceHome")
                                cell.background.image = UIImage(named: "")
                                cell.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                            }

                            
                             
                        }/*else{

                            cell.isUserInteractionEnabled = false
                            cell.checked.alpha = 0
                            cell.background.image = UIImage(named: "borderGray")
                            cell.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
                            cell.programarLbl.textColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                            cell.imageClock.image = UIImage(named: "ic_a_domicilio")


                        }*/

                        
                        /*cell.clipsToBounds = false
                        cell.layer.masksToBounds = false
                        cell.layer.cornerRadius = 5
                        cell.layer.shadowColor = UIColor.black.cgColor
                        cell.layer.shadowOpacity = 0.35
                        cell.layer.shadowOffset = .zero
                        cell.layer.shadowRadius = shadowRadius*/
                    return cell
                   default:
                       print("hola por defualt")
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "programar", for: indexPath) as! ProgramarCollectionViewCell

                    return cell
               }
                
        
    }
        
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        //MARK: - Filtro Collectionview
         cleanServiceSelecction()
            itemSelect = ""
        if  service_available[indexPath.row] != "SD" {
            
            imgPedidoInmediato.image = UIImage(named: "fillRoundedCheckBox")
                   imgPedidoProgramado.image = UIImage(named: "emptyRoundedCheckBox")
                   self.delegate!.getStatusServiceProgramer!(status: "pedidoInmediato")

        }
        
            switch service_available[indexPath.row] {
                
            case "RS":
            itemSelect = "RS"
             llevar = true
            if let paraLLevarCell = collectionView.cellForItem(at: indexPath) as? ParaLlevarCollectionViewCell, let programadoCell = collectionView.cellForItem(at: IndexPath(item: 3, section: 0)) as? ProgramarCollectionViewCell, let comerAquiCell = collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? ParaAquiCollectionViewCell, let cellBarra = collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? EnBarraCollectionViewCell {
                    
                //MARK: -  CellView  en Sucursal
               
                    paraLLevarCell.checked.alpha = 1
                    paraLLevarCell.greenBackground.image = UIImage(named: "greenBackground")
                    paraLLevarCell.paraLlevarLbl.textColor =  UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                
                    comerAquiCell.checked.alpha = 0
                    comerAquiCell.background.image = UIImage(named: "middleGrayLine")
                    comerAquiCell.paraAquiLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                    
                    cellBarra.checked.alpha = 0
                    cellBarra.background.image = UIImage(named: "middleGrayLine")
                    cellBarra.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                    
                    programadoCell.checked.alpha = 0
                    programadoCell.background.image = UIImage(named: "middleGrayLine")
                    programadoCell.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                }
             
         //   self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 0, isScheduled: false, inBar: false, percent: 0.0, expandItem: "llevar")
                
            self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 1, isScheduled: true, inBar: false, percent: 0.0, expandItem: "llevar",order_payments: self.order_payments)
                
            case "SM":
                
                 //MARK: -  CellView  Servicio Mesa
                 itemSelect = "SM"
                 llevar = false
                if let paraLLevarCell = collectionView.cellForItem(at: indexPath) as? ParaLlevarCollectionViewCell, let programadoCell = collectionView.cellForItem(at: IndexPath(item: 3, section: 0)) as? ProgramarCollectionViewCell, let comerAquiCell = collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? ParaAquiCollectionViewCell, let cellBarra = collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? EnBarraCollectionViewCell {
                        
                        comerAquiCell.checked.alpha = 1
                        comerAquiCell.background.image = UIImage(named: "greenBackground")
                        comerAquiCell.paraAquiLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                        
                        paraLLevarCell.checked.alpha = 0
                        paraLLevarCell.greenBackground.image = UIImage(named: "middleGrayLine")
                        paraLLevarCell.paraLlevarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                        
                        cellBarra.checked.alpha = 0
                        cellBarra.background.image = UIImage(named: "middleGrayLine")
                        cellBarra.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                        
                        programadoCell.checked.alpha = 0
                        programadoCell.background.image = UIImage(named: "middleGrayLine")
                        programadoCell.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                    }
                

                 
                 self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 1, isScheduled: false, inBar: false, percent: 0.0, expandItem: "mesa",order_payments: self.order_payments)

                
                
            case "RB":
                 //MARK: -  CellView  Recoger en Barra
                  itemSelect = "RB"
                  llevar = true
                               if let currentCell = collectionView.cellForItem(at: indexPath) as? ParaLlevarCollectionViewCell, let programadoCell = collectionView.cellForItem(at: IndexPath(item: 3, section: 0)) as? ProgramarCollectionViewCell, let comerAquiCell = collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as? ParaAquiCollectionViewCell, let cellBarra = collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? EnBarraCollectionViewCell {
                                   
                                  cellBarra.checked.alpha = 1
                                  cellBarra.background.image = UIImage(named: "greenBackground")
                                  cellBarra.programarLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                                                                  
                                
                                   currentCell.checked.alpha = 0
                                   currentCell.greenBackground.image = UIImage(named: "middleGrayLine")
                                   currentCell.paraLlevarLbl.textColor =  UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                                   
                                   comerAquiCell.checked.alpha = 0
                                   comerAquiCell.background.image = UIImage(named: "middleGrayLine")
                                   comerAquiCell.paraAquiLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                                   
                                  
                                   programadoCell.checked.alpha = 0
                                   programadoCell.background.image = UIImage(named: "middleGrayLine")
                                   programadoCell.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                               }
                 
                

                           
                 self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 2, isScheduled: false, inBar: true, percent: 0.0, expandItem: "barra",order_payments: self.order_payments)
                
                
            case "SD":
                 //MARK: -  CellView  Servicio Domicilio
                 itemSelect = "SD"
                llevar = true
                 
                                if let currentCell = collectionView.cellForItem(at: indexPath) as? ParaAquiCollectionViewCell, let programadoCell = collectionView.cellForItem(at: IndexPath(item: 3, section: 0)) as? ProgramarCollectionViewCell, let llevarCell = collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as? ParaLlevarCollectionViewCell, let cellBarra = collectionView.cellForItem(at: IndexPath(item: 2, section: 0)) as? EnBarraCollectionViewCell {
                                    
                                    programadoCell.checked.alpha = 1
                                    programadoCell.background.image = UIImage(named: "greenBackgroundMiddle")
                                    programadoCell.programarLbl.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                                    
                                    currentCell.checked.alpha = 0
                                    currentCell.background.image = UIImage(named: "middleGrayLine")
                                    currentCell.iconoParaAqui.image = UIImage(named: "ic_servicio_mesa")
                                    currentCell.paraAquiLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                                    
                                    llevarCell.checked.alpha = 0
                                    llevarCell.greenBackground.image = UIImage(named: "middleGrayLine")
                                    llevarCell.paraLlevarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                                    
                                    cellBarra.checked.alpha = 0
                                    cellBarra.background.image = UIImage(named: "middleGrayLine")
                                    cellBarra.programarLbl.textColor = UIColor(red: 53/255, green: 53/255, blue: 53/255, alpha: 1)
                                    
                                }
                 
                 self.delegate!.expandSettings!(llevar: llevar, expand: true, selectedItem: 3, isScheduled: true, inBar: false, percent: 0.0, expandItem: "domicilio",order_payments: self.order_payments)
                
                
            default:
                print("Caso Defualt")
            }
        
             
       // self.delegate!.getTypeService?(typeService: serviceType)
        self.delegate!.getTypeService?(typeService: service_available[indexPath.row])
        filtroCollectionView.reloadData()
            
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width: 80, height: 117)
        
    }
    
    
    func typeService(cellSelection: Int){
        
        switch cellSelection {

            case 0:
                //viewProgramarPedido.isHidden = true
                serviceType = "RS"
            
            case 1:
                
                //viewProgramarPedido.isHidden = true
                serviceType = "SM"
            
            case 2:
               //viewProgramarPedido.isHidden = true
                serviceType = "RB"
            
            case 3:
               // viewProgramarPedido.isHidden = false
                serviceType = "SD"
            
            default:
                
                serviceType = ""
            
        }

        self.delegate!.getTypeService?(typeService: serviceType)
         
    }
    
    @IBAction func btn_DataPiker(_ sender: Any) {
        
        self.delegate!.showDatePicker()
        
    }
    
    
  

 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

     guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout,
         let dataSourceCount = collectionView.dataSource?.collectionView(collectionView, numberOfItemsInSection: section),
         dataSourceCount > 0 else {
             return .zero
     }


     let cellCount = CGFloat(dataSourceCount)
     let itemSpacing = flowLayout.minimumInteritemSpacing
     let cellWidth = flowLayout.itemSize.width + itemSpacing
     var insets = flowLayout.sectionInset
    
    

     let totalCellWidth = (cellWidth * cellCount) - itemSpacing
     let contentWidth = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right

     guard totalCellWidth < contentWidth else {
         return insets
     }
    
   // itemFilterConteiner.sizeThatFits( CGSize(width: totalCellWidth, height: 30))

     
    chageSizeConteiner(totalCellWidth: totalCellWidth)

     let padding = (contentWidth - totalCellWidth) / 2.0
     insets.left = padding
     insets.right = padding
     self.totalWidth = Double(padding)
     return insets
 }
    
    
    func chageSizeConteiner(totalCellWidth: CGFloat){
        
        itemFilterConteiner.frame.size.height = totalCellWidth
       
    }
    
    
    func defaultSelection(selecDefaul: String){

        print("Seleccion En el filtro ", itemSelect)
        
        switch itemSelect {

            case "RS":
               
            self.delegate!.expandSettings!(llevar: true, expand: true, selectedItem: 0, isScheduled: false, inBar: false, percent: 0.0, expandItem: "llevar",order_payments: self.order_payments)
            self.delegate!.getTypeService?(typeService: itemSelect)
            
            case "SM":
                
                 self.delegate!.expandSettings!(llevar: false, expand: true, selectedItem: 1, isScheduled: false, inBar: false, percent: 0.0, expandItem: "mesa",order_payments: self.order_payments)
            self.delegate!.getTypeService?(typeService: itemSelect)
            case "RB":
              
            self.delegate!.expandSettings!(llevar: true, expand: true, selectedItem: 2, isScheduled: false, inBar: true, percent: 0.0, expandItem: "barra",order_payments: self.order_payments)
            self.delegate!.getTypeService?(typeService: itemSelect)
            case "SD":
              
            self.delegate!.expandSettings!(llevar: true, expand: true, selectedItem: 3, isScheduled: true, inBar: false, percent: 0.0, expandItem: "domicilio",order_payments: self.order_payments)
            self.delegate!.getTypeService?(typeService: itemSelect)
            default:
                
                print("Sin opcion")
            
        }

        
    }
    
    
    
    func cleanServiceSelecction(){
        
        let preferences = UserDefaults.standard

        let currentLevelKey = "itemSelecteKey"

        
        
        preferences.set("", forKey: currentLevelKey)

        //  Save to disk
        let didSave = preferences.synchronize()

        if !didSave {
            //  Couldn't save (I've never seen this happen in real world testing)
            
            print("No se pudo guardar")
            
        }
        
       
        
    }
    
    
    // orden filtro
    // RS  RB SM SD
    
    
    func ordenarFiltro(array: [String]){
        
        var tiposPedidos = [String]()
       // var tiposPedidos2 = ["RS","SD", "SD"]
       
       if array.contains("RS"){
           // tiposPedidos.remove(at: 0)
        tiposPedidos.append("RS")
            
        }
        
        if array.contains("RB"){
           // tiposPedidos.remove(at: 2)
            
             tiposPedidos.append("RB")
           
        }
        
        if array.contains("SM"){
          //  tiposPedidos.remove(at: 1)
           tiposPedidos.append("SM")
            
        }
        
        if array.contains("SD"){
             // tiposPedidos [contador] = "RB"
            tiposPedidos.append("SD")
           
        }
        
       // service_available = tiposPedidos
        service_available = tiposPedidos
        filtroCollectionView.frame = CGRect(x: 0, y: 0, width: tiposPedidos.count * 80, height: 117)
      //  filtroCollectionView.frame = CGRect(x: 0, y: 0 , width: CGFloat(tiposPedidos.count * 60), height: 117)
        
//        if tiposPedidos.count < 4{
            filtroCollectionView.center.x = (itemFilterConteiner.center.x - 20)
//        }
        
        defaultSelection(selecDefaul: itemSelect)
        filtroCollectionView.reloadData()
        
    }
    
    
}
