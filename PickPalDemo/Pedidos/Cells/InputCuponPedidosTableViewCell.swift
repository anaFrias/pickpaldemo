//
//  InputCuponPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 17/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol inputEntry {
    func finishInput(code: String)
}
class InputCuponPedidosTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var txt: UILabel!
    var delegate: inputEntry!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        input.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate.finishInput(code: input.text!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
