//
//  ItemPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol actionCarrito{
    func addItem(name: String, cantidad: Int, position: Int)
    func substractItem(name: String, cantidad: Int, position: Int)
    func editModifiers(position: Int)
}
class ItemPedidosTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var conteinerCell: UIView!
    var id = 0
    var name: String!
    var cuantiti: Int!
    var position: Int!
    var delegate: actionCarrito!
    var modifier_price: [Double]!
    var modifier_cant: [Int]!
    var modifier_name: [String]!
    var modifier_category: [String]!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var tableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        // Initialization code
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if modifier_name != nil{
            return modifier_name.count
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subItemItem", for: indexPath) as! SubItemPedidosTableViewCell
        if modifier_cant[indexPath.row] == 1{
            cell.name.text = modifier_category[indexPath.row] + ": - " + modifier_name[indexPath.row]
        }else{
            cell.name.text = modifier_category[indexPath.row] + ": - " + modifier_cant[indexPath.row].description + " " + modifier_name[indexPath.row]
        }
        if modifier_price[indexPath.row] > 0.00 {
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            
            if modifier_name[indexPath.row] == name  {
                if let formattedTipAmount = formatter.string(from: Double(modifier_price[indexPath.row].description )! as NSNumber) {
                    cell.price.text = "\(formattedTipAmount)"
                }
                //cell.price.text = "$" + modifier_price[indexPath.row].description + "0"
            }else{
                if let formattedTipAmount = formatter.string(from: Double(modifier_price[indexPath.row].description )! as NSNumber) {
                    cell.price.text = "+ \(formattedTipAmount)"
                }
                //cell.price.text = "+ $" + modifier_price[indexPath.row].description + "0"
            }
            
        }else{
            cell.price.text = ""
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.editModifiers(position: position!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func substract(_ sender: Any) {
        delegate.substractItem(name: name! , cantidad: cuantiti!, position: position!)
    }
    
    @IBAction func add(_ sender: Any) {
        delegate.addItem(name: name! , cantidad: cuantiti!, position: position!)
    }
}
