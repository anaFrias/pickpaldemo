//
//  SubTotalPedidosTableViewCell.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SubTotalPedidosTableViewCell: UITableViewCell {

    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
