//
//  MailingAddressViewController.swift
//  PickPalTesting
//
//  Created by Oscar on 03/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit
import Alamofire

protocol mailingAddressDelegate {
    func setAddress(address: Address)
    func setShoppingCost(shoppingCost: Double, timeDelivery: Int)
    func showDialogFail(title: String, description: String)
}

class MailingAddressViewController: UIViewController,deliveryDelegate, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MKMapViewDelegate, AddressFormAddressDelegate, googleWSDelegate, CLLocationManagerDelegate  {
    func backListAddress() {
        self.navigationController?.popViewController(animated: false)?.restorationClass
    }
    
    
    
    func setDataDelivery(addressSelect: Address, shoppingCost: Double, timeDelivery: Int) {
         
        self.delegate.setShoppingCost(shoppingCost: shoppingCost, timeDelivery: timeDelivery)
         self.delegate.setAddress(address: addressSelect)
         self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        
    }
    

    @IBOutlet weak var conteinerGPS: UIView!
    
    @IBOutlet weak var textfSearchAddress: UITextField!
    
    @IBOutlet weak var contenedorBuscador: UIView!
    
    @IBOutlet weak var streeName: UITextField!
    @IBOutlet weak var interiorNumber: UITextField!
    @IBOutlet weak var outdoorNumber: UITextField!
    @IBOutlet weak var suburb: UITextField!
    @IBOutlet weak var reference: UITextField!
    @IBOutlet weak var streeOne: UITextField!
    @IBOutlet weak var streenTwo: UITextField!
    @IBOutlet weak var mapDeliveryView: MKMapView!
    @IBOutlet weak var state: UITextField!
    @IBOutlet weak var municipality: UITextField!
    
    @IBOutlet weak var contendorDirecciones: UIView!
    @IBOutlet weak var viewMapa: UIView!
    @IBOutlet weak var viewFormulario: UIView!
    @IBOutlet weak var ctlPruebaY: NSLayoutConstraint!
    @IBOutlet weak var viewWhereSendOrder: UIView!
    @IBOutlet weak var collectioViewAddress: UICollectionView!
    
    @IBOutlet weak var contendorView: UIView!
    
    @IBOutlet weak var zipCode: UITextField!
    var delegate: mailingAddressDelegate!
    var client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var deliveryDelegate = DeliveryWS()
    var arrayAddress = [Address]()
    var addressSelect = Address()
    var addressDefault = Address()
    var itemSelect = Int()
    var isContinue = Bool()
    var dataDelivery = AddressFormViewController()
    var locationManager = CLLocationManager()
    var googleWS = GoogleServices()
    var from = ""
    var addressCatalog = [Address]()
    
    var arrPlaces = NSMutableArray(capacity: 100)
    var centerMapCoordinate:CLLocationCoordinate2D!
    var marker:GMSMarker!
    var historial = true
    private var centerCoordinate = CLLocationCoordinate2D()

    
    @IBOutlet weak var lblTitleWindow: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectioViewAddress.delegate = self
        collectioViewAddress.dataSource = self
        deliveryDelegate.delegate = self
        mapDeliveryView.delegate = self
        dataDelivery.delegate = self
        googleWS.delegate = self
        viewFormulario.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
     //   contendorDirecciones.frame = CGRect(x: 0, y: 0, width:  300, height: 117)
        
        contendorView.clipsToBounds = false
        contendorView.layer.masksToBounds = false
        contendorView.layer.cornerRadius = 5
        contendorView.layer.shadowColor = UIColor.black.cgColor
        contendorView.layer.shadowOpacity = 0.35
        contendorView.layer.shadowOffset = .zero
        contendorView.layer.shadowRadius = 5
        
        contenedorBuscador.clipsToBounds = false
        contenedorBuscador.layer.masksToBounds = false
        contenedorBuscador.layer.cornerRadius = 5
        contenedorBuscador.layer.shadowColor = UIColor.black.cgColor
        contenedorBuscador.layer.shadowOpacity = 0.35
        contenedorBuscador.layer.shadowOffset = .zero
        contenedorBuscador.layer.shadowRadius = 5
        
        // Do any additional setup after loading the view.
        arrayAddress.removeAll()
         self.loadData()
        
        self.viewFormulario.isHidden = true
        self.viewMapa.isHidden = true
        
        textfSearchAddress.addTarget(self, action: #selector(AddressFormViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        
        if from != "carrito"{
            
            collectioViewAddress.isHidden = true
            lblTitleWindow.text = "Agrega una dirección"
            
        }else{
            
            lblTitleWindow.text = "Agrega o escoge una dirección"
            
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.loadData()
        
        //Autocompetado
        
       // getPlaces(searchString: "Sal")
    }
    
    
    func loadData() {
        self.conteinerGPS.visibility = .visible
        arrayAddress.removeAll()
         self.collectioViewAddress.reloadData()
        deliveryDelegate.getAddresList(client_id: client_id)
        
    }
    
    @IBAction func btn_AgregarDireccion(_ sender: Any) {
        
        print("Vamos agregar una direccion xD")
        
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
        newV.titleSectionText = "Agregar una nueva dirección"
        newV.from = self.from
        newV.editAddress = addressDefault
        newV.actionAddress = "newAddress"
        newV.isEditAddrress = true
        newV.delegate = self
        self.navigationController?.pushViewController(newV, animated: true)
                                   
                                
    }
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addressCell", for: indexPath) as! AddressCollectionViewCell //AddressCollectionViewCell;
        
        let cellda = collectionView.dequeueReusableCell(withReuseIdentifier: "addressCell", for: indexPath) as! AddressCollectionViewCell

        if historial{
//       cell.lbAddres.text = "\(arrayAddress[indexPath.row].calle!) #\(arrayAddress[indexPath.row].numeroExterior!),  \(arrayAddress[indexPath.row].colonia!)"
            
            
//            cellda.lbAddres.text = "#\(arrayAddress[indexPath.row].calle!)"
//            cellda.lblDataAddress.text = "\(arrayAddress[indexPath.row].colonia!), \(arrayAddress[indexPath.row].CP!),  \(arrayAddress[indexPath.row].estado!)"
            
            
            if arrayAddress[indexPath.row].numeroExterior! != ""{
                
                if arrayAddress[indexPath.row].numeroInterior! != ""{
                    
                    cellda.lbAddres.text = "# \(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), no. Int. \(arrayAddress[indexPath.row].numeroInterior!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
                    
                }else{
                    
                    cellda.lbAddres.text = "# \(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
                }
                
            }else{
                
                if arrayAddress[indexPath.row].numeroInterior! != ""{
                    
                    cellda.lbAddres.text = "\(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), no. Int. \(arrayAddress[indexPath.row].numeroInterior!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
                }else{
                    
                    cellda.lbAddres.text = "\(arrayAddress[indexPath.row].numeroExterior!) \(arrayAddress[indexPath.row].street!) \(arrayAddress[indexPath.row].colony!), \(arrayAddress[indexPath.row].zip_code!),  \(arrayAddress[indexPath.row].state!)"
                }
            }
            
            
        }else{
            
            cellda.lbAddres.text = "\(arrayAddress[indexPath.row].referencia!)"
            cellda.lblDataAddress.text = arrayAddress[indexPath.row].state ?? ""
            
        }
        
        return cellda;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            return CGSize(width: collectionView.frame.size.width, height: 51)
   
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayAddress.count
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Precionates la direccion", indexPath.row )

        addressSelect = arrayAddress[indexPath.row]
        itemSelect = indexPath.row
        
        //loadInfoAddress(addressSelect: addressSelect)
       // self.viewFormulario.frame = CGRect(x: 0, y: 0, width: 365, height: 376)
       
        
        
       // deliveryDelegate.getDeliveryCost(addressDelivery: addressSelect, establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        
        if historial{
            
 
        print("Editar la direccion" , addressSelect.street!)
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
         let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
                newV.isEditAddrress = true
                newV.typeAddress = "edit"
                newV.direccion = addressSelect
                newV.from =  self.from
                newV.titleSectionText = "Dirección Registrada"
        newV.delegate = self
        self.navigationController?.pushViewController(newV, animated: true)
        
        }else{
            
            
           
            if indexPath.row != (arrayAddress.count - 1){
                
                self.googleWS.getInfoStreet(location: self.centerCoordinate, address: addressSelect.numeroInterior)
                
                
            }else{
                
                let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
                newV.titleSectionText = "Agregar una nueva dirección"
                newV.editAddress = addressDefault
                newV.isEditing = true
                newV.actionAddress = "newAddress"
                newV.from = self.from
                newV.delegate = self
                self.navigationController?.pushViewController(newV, animated: true)
                
            }

            
            
        }
       
    }
    
    
    
    
    func loadInfoAddress(addressSelect: Address){
                              
          streeName.text = addressSelect.street
          interiorNumber.text = addressSelect.numeroInterior
          outdoorNumber.text = addressSelect.numeroExterior
          suburb.text = addressSelect.colony
          state.text = addressSelect.state
          municipality.text = addressSelect.city
          zipCode.text = addressSelect.zip_code
          streeOne.text = addressSelect.calleReferencia1
          streenTwo.text = addressSelect.calleReferencia2
          reference.text = addressSelect.referencia
            centerMapCoordinate = CLLocationCoordinate2D(latitude: addressSelect.latitude ?? 0.0, longitude: addressSelect.longitude ?? 0.0)
        
//
//        if locationManager.location != nil {
//
//            // MARK: - MKMapView
//            centerMapCoordinate = CLLocationCoordinate2D(latitude: addressSelect.latitud ?? 0.0, longitude: addressSelect.longitud )
//
//            let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: addressSelect.latitud ?? 0.0, longitude: addressSelect.longitud ?? 0.0), fromEyeCoordinate: CLLocationCoordinate2D(latitude: addressSelect.latitud ?? 0.0, longitude: addressSelect.longitud ?? 0.0), eyeAltitude: 800.0)
//            mapDeliveryView.setCamera(camera, animated: false)
//            let eyeCoordinate = CLLocationCoordinate2D(latitude: addressSelect.latitud ?? 0.0, longitude: addressSelect.longitud ?? 0.0)
//            let annotation = MKPointAnnotation()
//            annotation.coordinate = eyeCoordinate
//            mapDeliveryView.addAnnotation(annotation)
//            /*self.viewFormulario.isHidden = false
//            self.viewMapa.isHidden = false*/
//
//        }
//
//           print("Coordenadas Actuales: \(addressSelect.municipio ?? "") \(addressSelect.longitud) ")
//
       }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        addressDefault.latitude = locValue.latitude
        addressDefault.longitude = locValue.longitude
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    
     func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
    
//         self.centerMapCoordinate = CLLocationCoordinate2D(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude )
//
//        addressDefault.latitud = mapView.centerCoordinate.latitude
//        addressDefault.longitud = mapView.centerCoordinate.longitude
//
//         print( "Coordenadas\(mapView.centerCoordinate.latitude) \(mapView.centerCoordinate.longitude)")
//
//         mapDeliveryView.removeAnnotations(mapDeliveryView.annotations)
//
//         let annotation = MKPointAnnotation()
//         annotation.coordinate = mapView.centerCoordinate
//         mapDeliveryView.addAnnotation(annotation)
         
         
     }
    
    
    func getAddressUser(addressCatalog: [Address]){
        
        self.addressCatalog = addressCatalog
        historial = true
        self.arrayAddress = addressCatalog
        itemSelect = addressCatalog.count + 1
        collectioViewAddress.reloadData()
    }

    
    func getShoppingCost(shoppingCost: Double, timeDelivery: Int) {
        
        
        
        if isContinue {
            self.delegate.setShoppingCost(shoppingCost: shoppingCost, timeDelivery: timeDelivery)
            self.delegate.setAddress(address: addressSelect)
            self.navigationController?.popViewController(animated: true)
        }else{
            
            isContinue = false
            self.viewFormulario.isHidden = false
            self.viewMapa.isHidden = false
        }
        
        
        
    }
    
    
    func failShoppingCost(error: String, subtitle: String) {
        
        self.viewFormulario.isHidden = true
        self.viewMapa.isHidden = true
         print("ERROR al obtener precio de envio ", error)
         let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
         newXIB.modalTransitionStyle = .crossDissolve
         newXIB.modalPresentationStyle = .overCurrentContext
         newXIB.errorMessageString = error
         newXIB.subTitleMessageString = subtitle
         present(newXIB, animated: true, completion: nil)
         
    }
    
    func failGetAddress(error: String, subtitle: String){
        
         print("ERROR Lista direcciones ", error)
        
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
    }
    

    

    @IBAction func btnInicio(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func btnPedidos(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func btnAjustes(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
               self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    func clickAddAddress(indexPath: IndexPath) {
       
       // deliveryDelegate.getDeliveryCost(addressDelivery: addressSelect, establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))

        
    }
    @IBAction func selectAndContinue(_ sender: Any) {
        
        print("Vamos a guardar la direccion")
        isContinue = true
        if addressSelect.street != streeName.text! || addressSelect.numeroExterior != interiorNumber.text! ||
            addressSelect.numeroInterior != outdoorNumber.text! || addressSelect.colony != suburb.text! ||
            addressSelect.city != municipality.text! || addressSelect.state != state.text || addressSelect.referencia != reference.text! || addressSelect.zip_code != zipCode.text! || addressSelect.calleReferencia1 != streeOne.text || addressSelect.calleReferencia2 != streenTwo.text!{
            
            
            print("Se tiene que actualiar la direccion")
            updateAddress()
            
            
        }else{
            
             print("Continua sin actualizar")
           //  deliveryDelegate.getDeliveryCost(addressDelivery: addressSelect, establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
            
        }
              
        
    }
    
    func  updateAddressUser(){
           
       /* let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = "Proceso Excitoso"
        newXIB.subTitleMessageString = "Dirección actulizada correctamente"
        present(newXIB, animated: true, completion: nil)*/
        
        
         print("Actualizacion Correcta")
   // deliveryDelegate.getDeliveryCost(addressDelivery: addressSelect, establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
            
    }
    
    
    func failUpdateddress(error: String, subtitle: String) {
         print("Error al Actualizar ", error)
        
    
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
    func updateAddress(){
        
           //address.pk = editAddress.pk
           addressSelect.street = streeName.text!
           addressSelect.numeroExterior = outdoorNumber.text!
           addressSelect.numeroInterior = interiorNumber.text
           addressSelect.colony = suburb.text!
           addressSelect.state =  state.text!
           addressSelect.zip_code = zipCode.text!
           addressSelect.calleReferencia1 = streeOne.text
           addressSelect.calleReferencia2 = streenTwo.text
           addressSelect.referencia = reference.text
           addressSelect.city = municipality.text
           addressSelect.latitude = centerMapCoordinate.latitude
           addressSelect.longitude = centerMapCoordinate.longitude
        //Actualizar direccion
        deliveryDelegate.updateAddress(address: addressSelect, client_id: client_id)
        
    }
    
    @IBAction func textFSearchAddress(_ sender: Any) {
        
        
        
    }
    
    @IBAction func btnOpenAutoComplete(_ sender: Any) {
        
       /* textfSearchAddress.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)*/
        
//        gpaViewController.placeDelegate = self
//
//        present(gpaViewController, animated: true, completion: nil)
//
    }
    
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print("Buscando ", textfSearchAddress.text ?? "" )
   
        
        
        if textfSearchAddress.text == "" {
            self.arrayAddress.removeAll()
            if from == "carrito"{
                
               
                historial = true
                self.arrayAddress = addressCatalog
                itemSelect = addressCatalog.count + 1
               
                self.conteinerGPS.visibility = .visible
                
            }else{
                
                
                self.conteinerGPS.visibility = .visible
                
            }
            self.collectioViewAddress.reloadData()
            
            
        }else {
            forwardGeoCoding(searchText: textfSearchAddress.text ?? "")
        }
     //   googlePlacesResult(input: textfSearchAddress.text ?? "")

    }
    
    func didSuccessGetInfoStreet(location: CLLocationCoordinate2D) {
        
        textfSearchAddress.text = ""
        
//        addressSelect.referencia = ""
        addressSelect.longitude = location.longitude
        addressSelect.latitude  = location.latitude
        let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
          let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
                newV.isEditAddrress = false
                newV.editAddress = addressSelect
                newV.typeAddress = "autocomplete"
                newV.actionAddress = "newAddress"
                newV.autocomplete = true
                newV.titleSectionText = "Agregar nueva dirección"
                newV.from =  self.from
                // newV.titleSectionText = "Agregar una nueva dirección"
                newV.delegate = self
         self.navigationController?.pushViewController(newV, animated: true)
        
        print("location: \(location.latitude) \(location.longitude)")
    }
    
    func didFailGetInfoStreet(title: String, subtitle: String) {
        print("Error")
    }
    
        
    //MARK: - Autocomplete Google -
           func forwardGeoCoding(searchText:String) {
               googlePlacesResult(input: searchText) { (result) -> Void in
                   let searchResult:NSDictionary = ["keyword":searchText,"results":result]
                  if result.count > 0{
                    self.collectioViewAddress.isHidden = false
                    self.conteinerGPS.visibility = .gone
                       let features = searchResult.value(forKey: "results") as! NSArray
                       self.arrPlaces = NSMutableArray(capacity: 100)
                       print(features.count)
                     let address = Address()
                    self.arrayAddress.removeAll()
                       for jk in 0...features.count-1{
                        self.historial = false
                           let dict = features.object(at: jk) as! NSDictionary
                            let address = Address()
                        address.referencia = dict.value(forKey: "description") as! String
                                         
                        let placeId = dict.value(forKey: "place_id") as! String
                        address.numeroInterior =  dict.value(forKey: "place_id") as! String
                        var  placesClient = GMSPlacesClient.shared()
                        
                        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
                                                                    UInt(GMSPlaceField.placeID.rawValue))

                        placesClient.fetchPlace(fromPlaceID: placeId, placeFields: fields, sessionToken: nil, callback: {
                          (place: GMSPlace?, error: Error?) in
                          if let error = error {
                            print("An error occurred: \(error.localizedDescription)")
                            return
                          }
                          if let place = place {
                           
                            print("El Lugar es: \(place.name) coordenadas lat \(address.latitude) long \(address.longitude)")
                          }
                        })
                        
                        let addresJson = dict.value(forKey: "terms") as! NSArray
                            
                        
                        if addresJson.count == 4{
                            print("Tamaño \(addresJson.count )")
                            address.street = (addresJson[0] as! NSDictionary).value(forKey: "value") as? String ?? ""
                            address.colony = (addresJson[1] as! NSDictionary).value(forKey: "value") as? String ?? ""
                            address.city = (addresJson[2] as! NSDictionary).value(forKey: "value") as? String ?? ""
                            address.state = (addresJson[3] as! NSDictionary).value(forKey: "value") as? String ?? ""
                                
                        }else{
                           
                            
                            if addresJson.count == 3{
                            address.street = (addresJson[0] as! NSDictionary).value(forKey: "value") as? String ?? ""
                            address.colony = (addresJson[1] as! NSDictionary).value(forKey: "value") as? String ?? ""
                            }
                        }
                         
                        print("Guardar \(address)")
                        self.arrayAddress.append(address)
                       }
                        address.referencia = "¿No encuentras tu dirección?"
                        address.state = "Utilizar ubicación actual"
                        self.arrayAddress.append(address)
                        self.collectioViewAddress.reloadData()
                       DispatchQueue.main.async(execute: {
                           if self.arrPlaces.count != 0
                           {
                            /*+   self.tblLoction.isHidden = false
                               self.lblNodata.isHidden = true
                               self.tblLoction.reloadData()*/
                           }
                           else
                           {
                               /*self.tblLoction.isHidden = true
                               self.lblNodata.isHidden = false
                               self.tblLoction.reloadData()*/
                           }
                       });
                  }
                
               }
           }

           //MARK: - Google place API request -
           func googlePlacesResult(input: String, completion: @escaping (_ result: NSArray) -> Void) {
            let searchWordProtection = input.replacingOccurrences(of: " ", with: "");
            
            if searchWordProtection.characters.count != 0 {
                    
                let urlString = NSString(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=address&location=%@,%@&radius=1000&language=en&key= AIzaSyCJeFpFP1IUdM-ve4o5YBzoUtf2M_FZNn8",input,"\(addressDefault.latitude )","\(addressDefault.longitude)")
                
                
                   print("Url place: ", urlString)
                   let url = NSURL(string: urlString.addingPercentEscapes(using: String.Encoding.utf8.rawValue)!)
                   print(url!)
                   let defaultConfigObject = URLSessionConfiguration.default
                   let delegateFreeSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: OperationQueue.main)
                   let request = NSURLRequest(url: url! as URL)
                   let task =  delegateFreeSession.dataTask(with: request as URLRequest, completionHandler:
                   {
                       (data, response, error) -> Void in
                       if let data = data
                       {
                           do {
                               let jSONresult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                               let results:NSArray = jSONresult["predictions"] as! NSArray
                               let status = jSONresult["status"] as! String
                               if status == "NOT_FOUND" || status == "REQUEST_DENIED"
                               {
                                   let userInfo:NSDictionary = ["error": jSONresult["status"]!]
                                let newError = NSError(domain: "API Error", code: 666, userInfo: userInfo as [NSObject : AnyObject] as! [String : Any])
                                   let arr:NSArray = [newError]
                                   completion(arr)
                                   return
                               }
                               else
                               {
                                   completion(results)
                               }
                           }
                           catch
                           {
                               print("json error: \(error)")
                           }
                       }
                       else if let error = error
                       {
                           print(error)
                       }
                   })
                   task.resume()
               }
           }
    
    
    
    
}

extension MailingAddressViewController: GMSAutocompleteViewControllerDelegate {
  
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    textfSearchAddress.text = place.name
    
            
    let storyboar = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
    let newV = storyboar.instantiateViewController(withIdentifier: "FormularioDireccion") as! AddressFormViewController
    newV.titleSectionText = "Agregar una nueva dirección"
    newV.centerMapCoordinate = place.coordinate
    newV.isNewAddress = true
    newV.from =  self.from
    newV.delegate = self
    self.navigationController?.pushViewController(newV, animated: true)
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
    }
    
}
