//
//  CardSelectorViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import OpenpayKit

class CardSelectorViewController: UIViewController, paymentDelegate, establishmentDelegate {
    var cards = [Card]()
    var facturar = false
    var paymentWS = PaymentWS()
    var establishmentWS = EstablishmentWS()
    var payment_source_id = String()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var paraAqui = Bool()
    var detail = [Dictionary<String,Any>]()
    var notes = String()
    var promotionalCode = String()
    var establishment_id = Int()
    var total = Double()
    var first_time = false
    var itemsTable = [String]()
    var hourScheduled = String()
    var isScheduled = Bool()
    var hourScheduledFormatted = String()
    var dateScheduled = String()
    var inBar = Bool()
    var addressDelivery = Address()
    var shipping_cost = Double()
    var type_order = String()
    var  timeDelivery = Int()
    var picpalService = Float()
    var pickpal_cash_money = Double()
    
    @IBOutlet weak var lblTitleTotal: UILabel!
    var pickpalService = Double()
    var discount = Double()
//    var openpay : Openpay!
    
    var lugar = String()
    var municipio = String()
    var categoria = String()
    var percent = Double()
    var mPoints = Double()
    var mMoneyPoints = Double()
    var subtotal = Double()
    var paymentType = Int()
    var myCash = MyCash()
    var minimumBilling = Double()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtTotal: UILabel!
    
    var stuffInTable = [String]()
    var order_elements = [Dictionary<String,Any>]()
    
    @IBOutlet weak var viewScheduled: UIView!
    @IBOutlet weak var totalAPagar: UILabel!
    @IBOutlet weak var horarioProgramado: UILabel!
    
    @IBOutlet weak var tituloHorarioProg: UILabel!
    @IBOutlet weak var fechaProgramada: UILabel!
    
    var session_id = String()
    var table = Int()
    
    //OpenPay
    var openpay : Openpay!
    var sessionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        paymentWS.delegate = self
        establishmentWS.delegate = self
        paymentWS.getUserCards(client_id: client_id)
        LoadingOverlay.shared.showOverlay(view: self.view)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double(total) as NSNumber) {
            txtTotal.text = "\(formattedTipAmount)"
            totalAPagar.text = "\(formattedTipAmount)"
        }
        horarioProgramado.text = hourScheduledFormatted
        fechaProgramada.text = dateScheduled
        if isScheduled {
            viewScheduled.alpha = 1
        }else{
            if type_order == "SM" {
            if table > 0 {
                viewScheduled.alpha = 1
                tituloHorarioProg.text = "SERVICIO A MESA:"
                horarioProgramado.text = "Mesa \(table)"
            }else{
                viewScheduled.alpha = 0
            }
            }else{
                viewScheduled.alpha = 0
                
            }
        }
        
        if pickpal_cash_money > 0 || mMoneyPoints > 0{
            lblTitleTotal.text = "RESTANTE A PAGAR:"
            
        }
        
//        minimumBilling = 10000
        
        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey: Constants.API_KEY, isProductionMode: false, isDebug: false)
//        openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
        //txtTotal.text = "$\(total)0 MXN"
    }
//    func successSessionID(sessionID: String) {
//        print("SessionID: \(sessionID)")
//        //        openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
//        self.session_id = sessionID
//        UserDefaults.standard.set(session_id, forKey: "session_id")
//    }
//
//    func failSessionID(error: NSError) {
//        print("\(error.code) - \(error.localizedDescription)")
//    }
//    func successCard() {
//        LoadingOverlay.shared.showOverlay(view: self.view)
////        openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
//        //        openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
//
//    }
//
//    func failCard(error: NSError) {
//        print("\(error.code) - \(error.localizedDescription)")
//    }
    
//    func successToken(token: OPToken) {
//        print("TokenID: \(token.id)")
//        LoadingOverlay.shared.hideOverlayView()
//        LoadingOverlay.shared.showOverlay(view: self.view)
//        self.paymentWS.addNewCardOppa(client_id: self.client_id, token_card: token.id, session_id: self.session_id)
//    }
//    func failToken(error: NSError) {
//        print("\(error.code) - \(error.localizedDescription)")
//    }
    func didSuccessAddCard() {
//        LoadingOverlay.shared.hideOverlayView()
//        LoadingOverlay.shared.showOverlay(view: self.view)
        paymentWS.getUserCards(client_id: client_id)
    }
    func didFailAddCard(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let alert = UIAlertController(title: "PickPal", message: "Imposible agregar tu tarjeta, verifica que tus datos sean correctos o ingresa otra tarjeta. Gracias", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default,handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        if !first_time{
            paymentWS.getUserCards(client_id: client_id)
            LoadingOverlay.shared.showOverlay(view: self.view)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func settings(_ sender: Any) {
//        openpay = nil
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func pedidos(_ sender: Any) {
//        openpay = nil
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    func didSuccessGetInvoicing(invoicingInfo: Invoicing) {
        if invoicingInfo.invoicing_by_pickpal {
            stuffInTable.append("textFactura")
            stuffInTable.append("yes")
            stuffInTable.append("no")
            stuffInTable.append("pedidosMayores")

        }
        tableView.reloadData()
    }
    func didFailGetInvoicing(error: String, subtitle: String) {
        print(error)
        tableView.reloadData()
    }
    @IBAction func home(_ sender: Any) {
//        openpay = nil
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    func menssageDialog(titulo: String, mensaje: String){
        
        print("Titulo ", titulo, " Mensaje ", mensaje)
    }
    
    @IBAction func Ordenar(_ sender: Any) {
        let currentDay = Date() //Hora actual
        if let finishTimeDate = UserDefaults.standard.object(forKey: "date_saved") as? Date { //hora guardada
            let calendar = Calendar.current
            let date = calendar.date(byAdding:.minute, value: -2, to: currentDay)!
            if date <= finishTimeDate {
                if payment_source_id != ""{
                    if facturar{
                        //                openpay = nil
                        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "AddressSelection") as! AddressSelectionViewController
                        newVC.detail = detail
                        newVC.paraAqui = paraAqui
                        newVC.payment_source_id = payment_source_id
                        newVC.establishment_id = establishment_id
                        newVC.total = total
                        newVC.notes = notes
                        newVC.promotionalCode = promotionalCode
                        newVC.discount = discount
                        newVC.pickpalService = pickpalService
                        newVC.municipio = municipio
                        newVC.categoria = categoria
                        newVC.lugar = lugar
                        newVC.order_elements = order_elements
                        newVC.hourScheduled = hourScheduled
                        newVC.isScheduled = isScheduled
                        newVC.inBar = inBar
                        newVC.percent = percent
                        newVC.table = table
                        newVC.mPoints = self.mPoints
                        newVC.mMoneyPoints = self.mMoneyPoints
                        newVC.hourFancy = hourScheduledFormatted
                        newVC.dateScheduled = dateScheduled
                        newVC.addressDelivery = self.addressDelivery
                        newVC.shipping_cost =  Float(self.shipping_cost)
                        newVC.type_order = self.type_order
                        newVC.timeDelivery = self.timeDelivery
                        newVC.pickpal_cash_money = pickpal_cash_money
                        
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }else{
                        //                openpay = nil
                        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                        let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
                        newVC.address_id = 0
                        newVC.detail = detail
                        newVC.paraAqui = paraAqui
                        newVC.establishment_id = establishment_id
                        newVC.payment_source_id = payment_source_id
                        newVC.total = total
                        newVC.notes = notes
                        newVC.promotionalCode = promotionalCode
                        newVC.discount = discount
                        newVC.pickpalService = pickpalService
                        newVC.municipio = municipio
                        newVC.categoria = categoria
                        newVC.lugar = lugar
                        newVC.hourScheduled = hourScheduled
                        newVC.isScheduled = isScheduled
                        newVC.inBar = inBar
                        newVC.percent = percent
                        newVC.table = table
                        newVC.hourFancy = hourScheduledFormatted
                        newVC.dateScheduled = dateScheduled
                        newVC.mPoints = mPoints
                        newVC.mMoneyPoints = mMoneyPoints
                        newVC.subtotal = subtotal
                        newVC.paymentType = paymentType
                        newVC.addressDelivery = self.addressDelivery
                        newVC.shipping_cost =  Float(self.shipping_cost)
                        newVC.type_order = self.type_order
                        newVC.timeDelivery = self.timeDelivery
                        newVC.pickpal_cash_money =  pickpal_cash_money
                        print("Card Select Direccion: " ,  self.addressDelivery.pk)
                        print("Card Select Pickpla: " ,  self.shipping_cost)
                        print("Card Select Tipo orden: " ,  self.type_order)
                        
                        
                        self.navigationController?.pushViewController(newVC, animated: true)
                    }
                }else{
                    let alert = UIAlertController(title: "PickPal", message: "Por favor selecciona un método de pago", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Horario inválido"
                newXIB.subTitleMessageString = "Selecciona otro horario"
                present(newXIB, animated: true, completion: nil)
            }
        }else{
            if payment_source_id != ""{
                if facturar{
                    //                openpay = nil
                    let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "AddressSelection") as! AddressSelectionViewController
                    newVC.detail = detail
                    newVC.paraAqui = paraAqui
                    newVC.payment_source_id = payment_source_id
                    newVC.establishment_id = establishment_id
                    newVC.total = total
                    newVC.notes = notes
                    newVC.promotionalCode = promotionalCode
                    newVC.discount = discount
                    newVC.pickpalService = pickpalService
                    newVC.municipio = municipio
                    newVC.categoria = categoria
                    newVC.lugar = lugar
                    newVC.order_elements = order_elements
                    newVC.hourScheduled = hourScheduled
                    newVC.isScheduled = isScheduled
                    newVC.inBar = inBar
                    newVC.percent = percent
                    newVC.table = table
                    newVC.hourFancy = hourScheduledFormatted
                    newVC.dateScheduled = dateScheduled
                    newVC.mPoints = self.mPoints
                    newVC.mMoneyPoints = self.mMoneyPoints
                    newVC.addressDelivery = self.addressDelivery
                    newVC.shipping_cost =  Float(self.shipping_cost)
                    newVC.type_order = self.type_order
                    newVC.timeDelivery = self.timeDelivery
                    newVC.pickpal_cash_money = pickpal_cash_money
                    self.navigationController?.pushViewController(newVC, animated: true)
                }else{
                    //                openpay = nil
                    let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "processOrder") as! ProcessOrderViewController
                    newVC.address_id = 0
                    newVC.detail = detail
                    newVC.paraAqui = paraAqui
                    newVC.establishment_id = establishment_id
                    newVC.payment_source_id = payment_source_id
                    newVC.total = total
                    newVC.notes = notes
                    newVC.promotionalCode = promotionalCode
                    newVC.discount = discount
                    newVC.pickpalService = pickpalService
                    newVC.municipio = municipio
                    newVC.categoria = categoria
                    newVC.lugar = lugar
                    newVC.hourScheduled = hourScheduled
                    newVC.isScheduled = isScheduled
                    newVC.inBar = inBar
                    newVC.percent = percent
                    newVC.table = table
                    newVC.hourFancy = hourScheduledFormatted
                    newVC.dateScheduled = dateScheduled
                    newVC.mPoints = mPoints 
                    newVC.mMoneyPoints = mMoneyPoints
                    newVC.subtotal = subtotal
                    newVC.paymentType = paymentType
                    newVC.addressDelivery = self.addressDelivery
                    newVC.shipping_cost =  Float(self.shipping_cost)
                    newVC.type_order = self.type_order
                    newVC.timeDelivery = self.timeDelivery
                    newVC.pickpal_cash_money =  pickpal_cash_money
                    print("2 Card Select Direccion: " ,  self.addressDelivery.pk)
                    print("2 Card Select Pickpla: " ,  self.shipping_cost)
                    print("2 Card Select Tipo orden: " ,  self.type_order)
                    
                    self.navigationController?.pushViewController(newVC, animated: true)
                }
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Por favor selecciona un método de pago", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default,handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    @IBAction func popView(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func didSuccessGetCards(cards: [Card]) {
        LoadingOverlay.shared.hideOverlayView()
        stuffInTable.removeAll()
        self.cards = cards
        for card in cards {
            stuffInTable.append("cards")
        }
        stuffInTable.append("addCard")
        let establishmenId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
        establishmentWS.getInvoicingByPickpal(establishmentId: establishmenId)
//        tableView.reloadData()
    }
    
    func didFailGetCards(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if error != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = error
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
            stuffInTable.append("addCard")
            let establishmenId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            establishmentWS.getInvoicingByPickpal(establishmentId: establishmenId)
        }else{
            stuffInTable.append("addCard")
            let establishmenId = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)
            establishmentWS.getInvoicingByPickpal(establishmentId: establishmenId)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension CardSelectorViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stuffInTable.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if stuffInTable[indexPath.row] == "yes" {
            if total > minimumBilling {
                if let cell = tableView.cellForRow(at: indexPath) as? OptionFacturaTableViewCell {
                    if let cell2 = tableView.cellForRow(at: IndexPath(row: indexPath.row + 1, section: indexPath.section)) as? OptionFacturaTableViewCell {
                        if(!facturar){
                            cell.checkBox.image = UIImage(named: "fillRoundedCheckBox")
                            cell2.checkBox.image = UIImage(named: "emptyRoundedCheckBox")
                        }
                    }
                    cell.tuFactura.alpha = 1
                    
                    if pickpal_cash_money > 0{
                        cell.tuFactura.text = "El monto facturado será unicamente el pagado con TDC/TDD"
                    }else{
                        
                        cell.tuFactura.text = "Tu factura será enviada al correo electrónico registrado."
                        
                    }
                    
                    facturar = true
                }
            }
            
            
        }else if stuffInTable[indexPath.row] == "no"{
            if total > minimumBilling {
                if let cell = tableView.cellForRow(at: indexPath) as? OptionFacturaTableViewCell {
                    if let cell2 = tableView.cellForRow(at: IndexPath(row: indexPath.row - 1, section: indexPath.section)) as? OptionFacturaTableViewCell {
                        if(facturar){
                            cell.checkBox.image = UIImage(named: "fillRoundedCheckBox")
                            cell2.checkBox.image = UIImage(named: "emptyRoundedCheckBox")
                        }
                        cell.tuFactura.alpha = 0
                        cell2.tuFactura.alpha = 0
                        cell.tuFactura.alpha = 0
                    }
                    facturar = false
                }
            }
            
        }else if stuffInTable[indexPath.row] == "addCard"{
//            if cards.count < 3 {
//            let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCardViewController
//            first_time = false
//            newVC.cards = self.cards
//            self.navigationController?.pushViewController(newVC, animated: true)
            
            addCardOpenPay()
            
        }else if stuffInTable[indexPath.row] == "cards"{
            for i in 0..<cards.count{
                let indexPath = IndexPath(row: i, section: 0)
                if let cell = tableView.cellForRow(at: indexPath) as? CardTableViewCell {
                    cell.background.image = UIImage(named: "rectangle_border_gray")
                    cell.cardNumber.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                }
            }
            let cell = tableView.cellForRow(at: indexPath) as! CardTableViewCell
            cell.background.image = UIImage(named: "RectangleGreen")
            cell.cardNumber.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            payment_source_id = cards[indexPath.row].payment_source_id
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if stuffInTable[indexPath.row] == "cards"{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cardItem", for: indexPath) as? CardTableViewCell {
                cell.cardNumber.text = cards[indexPath.row].card
//                OPENPAY
//                if cards[indexPath.row].brand.uppercased() == "MASTERCARD"{
//                    cell.cardimage.image = UIImage(named: "iconMC")
//                }else if cards[indexPath.row].brand.uppercased() == "AMERICAN_EXPRESS"{
//                    cell.cardimage.image = UIImage(named: "iconAE")
//                }else {
//                    cell.cardimage.image = UIImage(named: "visaCard")
//                }
//                CONEKTA
                if cards[indexPath.row].brand == "MC"{
                    cell.cardimage.image = UIImage(named: "iconMC")
                }else if cards[indexPath.row].brand == "AMERICAN_EXPRESS"{
                    cell.cardimage.image = UIImage(named: "iconAE")
                }else {
                    cell.cardimage.image = UIImage(named: "visaCard")
                }
                if indexPath.row == 0 {
                    cell.background.image = UIImage(named: "RectangleGreen")
                    cell.cardNumber.textColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                    payment_source_id = cards[indexPath.row].payment_source_id
                }else{
                    cell.background.image = UIImage(named: "rectangle_border_gray")
                    cell.cardNumber.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
                }
                return cell
            }else{
                return UITableViewCell()
            }
        }else if stuffInTable[indexPath.row] == "addCard"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCard", for: indexPath)
            return cell
        }else if stuffInTable[indexPath.row] == "textFactura"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "txtFactura", for: indexPath)
            return cell
        }else if stuffInTable[indexPath.row] == "yes"{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "optionFactura", for: indexPath) as? OptionFacturaTableViewCell {
                if total > minimumBilling {
                    cell.txtOption.text = "Si"
                    cell.checkBox.image = UIImage(named: "emptyRoundedCheckBox")//UIImage(named: "fillRoundedCheckBox")

                   
                    
                    cell.tuFactura.alpha = 0
                }else{
                    cell.txtOption.text = "Si"
                    cell.txtOption.textColor = UIColor.lightGray
                    cell.checkBox.image = UIImage(named: "emptyRoundedCheckBox")//UIImage(named: "fillRoundedCheckBox")
                    cell.checkBox.image = cell.checkBox.image?.withRenderingMode(.alwaysTemplate)
                    cell.checkBox.tintColor = UIColor.lightGray
                    cell.tuFactura.alpha = 0
                    
                    
                    
                }
                
                return cell
            }else{
                return UITableViewCell()
            }
        }else if stuffInTable[indexPath.row] == "no"{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "optionFactura", for: indexPath) as? OptionFacturaTableViewCell {
//                if total > 30 {
//                    cell.txtOption.text = "No"
//                    cell.checkBox.image = UIImage(named: "emptyRoundedCheckBox")
//                    cell.tuFactura.alpha = 0
//                }else{
                    cell.txtOption.text = "No"
                    cell.checkBox.image = UIImage(named: "fillRoundedCheckBox")
                    cell.tuFactura.alpha = 0
                
//                }
                
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "pedidosMayores", for: indexPath) as! MessageFacturaCell
            cell.lblMessageFactura.visibility =  minimumBilling < total ? .gone: .visible
            cell.lblMessageFactura.text =  total < minimumBilling ? "Únicamente pedidos mayores a \(CustomsFuncs.getFomatMoney(currentMoney:  self.minimumBilling)) podrán ser facturados":""
            
           
                
          
    //        tableView.endUpdates()
            return cell
          
        }
    }
    
    
}

extension CardSelectorViewController: ordersDelegate {
    
    func addCardOpenPay() {
        openpay = Openpay(withMerchantId: Constants.MERCHANT_ID, andApiKey:  Constants.API_KEY, isProductionMode: false, isDebug: false)
        openpay.loadCardForm(in: self, successFunction: successCard, failureFunction: failCard, formTitle: "Openpay")
    }

    func successSessionID(sessionID: String) {
            print("SessionID: \(sessionID)")
            self.sessionID = sessionID
            //        LoadingOverlay.shared.showOverlay(view: self.view)
            openpay.createTokenWithCard(address: nil, successFunction: successToken, failureFunction: failToken)
        }
        
        func failSessionID(error: NSError) {
            LoadingOverlay.shared.hideOverlayView()
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
                
                UserDefaults.standard.set(self.session_id, forKey: "session_id")
                
            }
        }
        func successCard() {
            openpay.createDeviceSessionId(successFunction: successSessionID, failureFunction: failSessionID)
            
        }
        
        func failCard(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }
        
        func successToken(token: OPToken) {
            print("TokenID: \(token.id)")
            DispatchQueue.main.async {
            LoadingOverlay.shared.showOverlay(view: self.view)
                self.paymentWS.addNewCardOppa(client_id:  self.client_id, token_card: token.id, session_id: self.sessionID)
            }
        }
    

        
        func failToken(error: NSError) {
            print("\(error.code) - \(error.localizedDescription)")
            DispatchQueue.main.async {
                LoadingOverlay.shared.hideOverlayView()
                let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.errorMessageString = "Error \(error.code)"
                newXIB.subTitleMessageString = "Imposible agregar tu tarjeta:\n \(error.localizedDescription)"
                self.present(newXIB, animated: true, completion: nil)
//                self.anadirMetodo.isEnabled = true
            }
        }

}




