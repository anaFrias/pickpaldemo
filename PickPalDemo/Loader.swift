//
//  Loader.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import SQLite
//import Spring
import RevealingSplashView

let path = NSSearchPathForDirectoriesInDomains(
    .documentDirectory, .userDomainMask, true
).first!

class Loader: UIViewController, ordersDelegate, userDelegate, UIViewControllerTransitioningDelegate {
    
    var orderws = OrdersWS()
    var userws = UserWS()
    var neews = false
    var factura = false
    var orderIdFactura = 0
    let transition = CircularTransition()
    var openPedidos = Bool()
    var orderIdExpand = Int()
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var ImageGiF: UIImageView!
    //    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    //        transition.transitionMode = .present
    //        transition.startingPoint = CGPoint.zero
    //        transition.circleColor = UIColor.white
    //        return transition
    //    }
    //
    //    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    //        transition.transitionMode = .dismiss
    //        transition.startingPoint = CGPoint.zero
    //        transition.circleColor = UIColor.white
    //        return transition
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.object(forKey: "client_id") as? Int) != nil {
            self.ImageGiF.loadGif(name: "gifLenguita")
            //            if let name = UserDefaults.standard.object(forKey: "first_name") as? String {
            //                welcomeLabel.text = "Hola, \(name)"
            //                welcomeLabel.alpha = 1
            //            }else{
            welcomeLabel.alpha = 0
            //            }
            redirect()
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let mvc = storyBoard.instantiateInitialViewController()
                mvc?.modalTransitionStyle = .crossDissolve
                mvc?.modalPresentationStyle = .fullScreen
                self.present(mvc!, animated: true, completion: {
                })
            })
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.refresh(_:)), name: NSNotification.Name(rawValue: "refresh"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.factura(_:)), name: NSNotification.Name(rawValue: "factura"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.openPedido(_:)), name: NSNotification.Name(rawValue: "openPedido"), object: nil)
        // Do any additional setup after loading the view.
    }
    @objc func refresh (_ notification: NSNotification) {
        neews = true
    }
    @objc func factura (_ notification: NSNotification) {
        factura = true
        print((((notification.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as! Int)
        let orderId = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as! Int
        orderIdFactura = orderId
    }
    @objc func openPedido (_ notification: NSNotification) {
        //        print((((notification.userInfo as! NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as! Int)
        openPedidos = true
        if let orderId = (((notification.userInfo! as NSDictionary).value(forKey: "custom") as? NSDictionary)?.value(forKey: "a") as? NSDictionary)?.value(forKey: "order_id") as? Int {
            orderIdExpand = orderId
        }
        
        
    }
    
    func redirect() {
        //        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "singlelogo")!,iconInitialSize: CGSize(width: 139, height: 135), backgroundColor: UIColor(red:220/255, green:25/255, blue:53/255, alpha:1.0))
        //
        //        //Adds the revealing splash view as a sub view
        //        self.view.addSubview(revealingSplashView)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
                if invited {
                    UserDefaults.standard.removeObject(forKey: "first_name")
                    UserDefaults.standard.removeObject(forKey: "client_id")
                    UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
                    UserDefaults.standard.removeObject(forKey: "avatar")
                    UserDefaults.standard.removeObject(forKey: "invited")
                    UserDefaults.standard.removeObject(forKey: "age")
                }
            }
            if UserDefaults.standard.object(forKey: "client_id") != nil {
                if let complete_r = UserDefaults.standard.object(forKey: "complete_register") as? Bool, complete_r{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as!  CompleteProfileViewController
                    newVC.client_id = UserDefaults.standard.object(forKey: "client_id") as? Int
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    self.present(navController, animated: true, completion: nil)
                }else{
                    //let place = UserDefaults.standard.string(forKey: "place") ?? ""
                    let newVC: UIViewController
                    /*switch place{
                    case "home":
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                        (newVC as! HomeViewController).homePush = false
                        (newVC as! HomeViewController).neews = self.neews
                        (newVC as! HomeViewController).factura = self.factura
                        (newVC as! HomeViewController).orderIdFactura = self.orderIdFactura
                        (newVC as! HomeViewController).showSplash = true
                    case "promos":
                        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                        newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
                    default:
                        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                        newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
                    }*/
                    let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                    newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
                    
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    navController.modalTransitionStyle = .crossDissolve
                    navController.modalPresentationStyle = .fullScreen
                    //                    navController.modalPresentationStyle = .custom
                    self.present(navController, animated: true, completion: nil)
                    //                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    //                        let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
                    //                        newVC.client_id = UserDefaults.standard.object(forKey: "client_id") as? Int
                    //                        let navController = UINavigationController(rootViewController: newVC)
                    //                        self.present(navController, animated: true, completion: nil)
                }
            }else{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let mvc = storyBoard.instantiateInitialViewController()
                mvc?.modalTransitionStyle = .crossDissolve
                mvc?.modalPresentationStyle = .fullScreen
                self.present(mvc!, animated: true, completion: {
                })
            }
        })
        
    }
    func didSuccessViewProfile(profile: UserProfile) {
        LoadingOverlay.shared.hideOverlayView()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            if UserDefaults.standard.object(forKey: "client_id") != nil {
                if let complete_r = UserDefaults.standard.object(forKey: "complete_register") as? Bool, complete_r{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
                    newVC.client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    self.present(navController, animated: true, completion: nil)
                }else{
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    newVC.homePush = false
                    newVC.neews = self.neews
                    newVC.modalPresentationStyle = .fullScreen
                    let navController = UINavigationController(rootViewController: newVC)
                    self.present(navController, animated: true, completion: nil)
                }
            }else{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let mvc = storyBoard.instantiateInitialViewController()
                mvc?.modalTransitionStyle = .crossDissolve
                mvc?.modalPresentationStyle = .fullScreen
                self.present(mvc!, animated: true, completion: {
                })
            }
        })
    }
    
    func didSuccessGetLastOrder(order_id: Int, establishment_id: Int) {
        //        UserDefaults.standard.set(order_id, forKey: "order_id")
        //        UserDefaults.standard.set(establishment_id, forKey: "establishment_id")
        //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
        //            if UserDefaults.standard.object(forKey: "client_id") != nil {
        //                if let complete_r = UserDefaults.standard.object(forKey: "complete_register") as? Bool, complete_r{
        //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                    let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
        //                    newVC.client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
        //                    let navController = UINavigationController(rootViewController: newVC)
        //                    self.present(navController, animated: true, completion: nil)
        //                }else{
        //                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
        //                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        //                    newVC.homePush = false
        //                    newVC.neews = self.neews
        //                    let navController = UINavigationController(rootViewController: newVC)
        //                    self.present(navController, animated: true, completion: nil)
        //                }
        //            }else{
        //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //                let mvc = storyBoard.instantiateInitialViewController()
        //                mvc?.modalTransitionStyle = .crossDissolve
        //                self.present(mvc!, animated: true, completion: {
        //                })
        //            }
        //        })
    }
    func didSuccessGetLastOrder(order_id: Int, establishment_id: Int, establishment_name: String, register_date_key: String, register_date: String, register_hour: String) {
        //        UserDefaults.standard.set(order_id, forKey: "order_id")
        //        UserDefaults.standard.set(establishment_id, forKey: "establishment_id")
        //        UserDefaults.standard.set(establishment_name, forKey: "stb_name")
        //        UserDefaults.standard.set(register_date, forKey: "r_date")
        //        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
        //            if UserDefaults.standard.object(forKey: "client_id") != nil {
        //                if let complete_r = UserDefaults.standard.object(forKey: "complete_register") as? Bool, complete_r{
        //                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                    let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
        //                    newVC.client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
        //                    let navController = UINavigationController(rootViewController: newVC)
        //                    self.present(navController, animated: true, completion: nil)
        //                }else{
        //                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
        //                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        //                    newVC.homePush = false
        //                    newVC.neews = self.neews
        //                    let navController = UINavigationController(rootViewController: newVC)
        //                    self.present(navController, animated: true, completion: nil)
        //                }
        //            }else{
        //                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //                let mvc = storyBoard.instantiateInitialViewController()
        //                mvc?.modalTransitionStyle = .crossDissolve
        //                self.present(mvc!, animated: true, completion: {
        //                })
        //            }
        //        })
    }
    func didFailViewProfile(title: String, subtitle: String) {
        if title != "Error de comunicación" {
            LoadingOverlay.shared.hideOverlayView()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let mvc = storyBoard.instantiateInitialViewController()
            mvc?.modalTransitionStyle = .crossDissolve
            self.present(mvc!, animated: true, completion: {
            })
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            newVC.homePush = false
            newVC.neews = neews
            newVC.modalPresentationStyle = .fullScreen
            let navController = UINavigationController(rootViewController: newVC)
            self.present(navController, animated: true, completion: nil)
        }
        
    }
    func didFailGetLastOrder(error: String, subtitle: String) {
        //        if title == "Error" {
        //            let storyboard = UIStoryboard(name: "Home", bundle: nil)
        //            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        //            newVC.homePush = false
        //            newVC.neews = neews
        //            let navController = UINavigationController(rootViewController: newVC)
        //            self.present(navController, animated: true, completion: nil)
        //            LoadingOverlay.shared.hideOverlayView()
        //        }else{
        //            LoadingOverlay.shared.hideOverlayView()
        //            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //            let mvc = storyBoard.instantiateInitialViewController()
        //            mvc?.modalTransitionStyle = .crossDissolve
        //            self.present(mvc!, animated: true, completion: {
        //            })
        //        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func createDB (){
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
}
