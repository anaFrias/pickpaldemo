//
//  ListPromoTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/27/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol ListPromoDelegate {
    func onNoinstallWhatsapp()
    
    func sendCaptureimageSingle(image: UIImage)
}

class ListPromoTableViewCell: UITableViewCell {

    @IBOutlet weak var imglOGO: UIImageView!
    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var lblDescrip: UILabel!
    @IBOutlet weak var lblDeadLine: UILabel!
    @IBOutlet weak var contentSinglePromo: UIView!
    @IBOutlet weak var contentHeader: UIView!
    @IBOutlet weak var contentImageOfPromo: UIView!
    
    @IBOutlet weak var imgSharedPromo: UIButton!
    @IBOutlet weak var lblNameHeader: UILabel!
    var titlePromo:String!
    var delegate: ListPromoDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onSharedImage(_ sender: Any) {
       
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(contentSinglePromo.bounds.size, true, scale)
        contentSinglePromo.layer.render(in: UIGraphicsGetCurrentContext()!)
        let images = UIGraphicsGetImageFromCurrentImageContext()!
    
        UIGraphicsEndImageContext()
        delegate.sendCaptureimageSingle(image: images)
        
        
        
      /*  let msg = "*¡Descarga pickpal en tu celular y entérate de las promociones de este comercio y de muchos más!*" + "\n¡Aprovecha y ahorra dinero en " + titlePromo + "\nDescarga aqui: itunes.apple.com/us/app/apple-store/1378364730?mt=8"
        
        let urlWhats = "whatsapp://send?text=\(msg)"
        
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL as URL)
                    }
                } else {
                    self.delegate.onNoinstallWhatsapp()
                    
                }
            }
        }*/
    }
}
