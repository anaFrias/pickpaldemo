//
//  PromosListAdvsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/24/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromosListAdvsTableViewCell: UITableViewCell {

    @IBOutlet weak var imImagePromo: UIImageView!
    //@IBOutlet weak var lblNameEstab: UILabel!
    @IBOutlet weak var lblTitlePromo: UILabel!
    @IBOutlet weak var lblDescripPromo: UILabel!
    @IBOutlet weak var lblDeadLine: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onSharedImage(_ sender: Any) {
        
    }
    
}
