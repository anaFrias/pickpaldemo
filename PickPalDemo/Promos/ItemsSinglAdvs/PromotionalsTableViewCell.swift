//
//  PromotionalsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/22/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class PromotionalsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionsPromotionals: UICollectionView!
    
    var promotionals: [Promotional]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionsPromotionals.delegate = self
        collectionsPromotionals.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension PromotionalsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotionals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromoCollection", for: indexPath) as! PromoCollectionViewCell
        if let image = promotionals[indexPath.row].image, !image.isEmpty {
            Nuke.loadImage(with: URL(string: image)!, into: cell.imgIcon)
        }
        cell.lblTitle.text = promotionals[indexPath.row].name
        cell.lblDescrip.text = promotionals[indexPath.row].descr
        cell.lblDeadLine.text = promotionals[indexPath.row].deadline
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 375, height: 400)
    }
    
    
}

