//
//  PromoCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/27/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescrip: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblDeadLine: UILabel!
    
    
}
