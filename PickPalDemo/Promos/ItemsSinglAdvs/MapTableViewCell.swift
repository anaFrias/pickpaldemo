//
//  MapTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps

protocol MapsDelegate {
    func openMaps()
}

class MapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var viewMap: GMSMapView!
    
    
    var delegate: MapsDelegate!
    var latitud = CLLocationDegrees()
    var longitud = CLLocationDegrees()
    var establString = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
        cell.mapview.setCamera(camera, animated: true)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
        let annotation = MKPointAnnotation()
        annotation.coordinate = eyeCoordinate
        cell.mapview.addAnnotation(annotation)*/
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)

            let anotation = MKPointAnnotation()
            anotation.coordinate = location
            anotation.title = establString
            
            mapview.addAnnotation(anotation)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func openMap(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitud),\(longitud)&zoom=14&views=traffic&q=\(latitud),\(longitud)")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitud, longitud)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = establString
            mapItem.openInMaps(launchOptions: options)
        }
    }
}
