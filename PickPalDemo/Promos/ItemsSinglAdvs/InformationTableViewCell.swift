//
//  InformationTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/22/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol informationDelegate {
    func pressMap(isShow: Bool)
    func pressInfo(isShow: Bool)
}

class InformationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNameEstab: UILabel!
    @IBOutlet var btnMap: UIButton!
    @IBOutlet var btnInfo: UIButton!
    
    var delegate: informationDelegate!
    
    var isMapSelect: Bool = false
    var isInfoSelect: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func showMap(_ sender: Any) {
        if isMapSelect && !isInfoSelect {
            self.delegate.pressMap(isShow: false)
            isMapSelect = false
            btnMap.setImage(UIImage(named: "iconLocation.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: UIControlState.normal)
            
        }else if !isMapSelect && isInfoSelect{
            self.delegate.pressMap(isShow: true)
            isInfoSelect = false
            isMapSelect = true
            btnMap.setImage(UIImage(named: "selectedMap.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: UIControlState.normal)
        }else if !isMapSelect && !isInfoSelect{
            self.delegate.pressMap(isShow: true)
            isMapSelect = true
            btnMap.setImage(UIImage(named: "selectedMap.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: UIControlState.normal)
        }
    }
    
    @IBAction func showInformation(_ sender: Any) {
        if !isMapSelect && isInfoSelect {
            self.delegate.pressInfo(isShow: false)
            isInfoSelect = false
            btnMap.setImage(UIImage(named: "iconLocation.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfoDisabled.png"), for: UIControlState.normal)
        }else if isMapSelect && !isInfoSelect{
            self.delegate.pressInfo(isShow: true)
            isMapSelect = false
            isInfoSelect = true
            btnMap.setImage(UIImage(named: "iconLocation.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfo.png"), for: UIControlState.normal)
        }else if !isMapSelect && !isInfoSelect{
            self.delegate.pressInfo(isShow: true)
            isInfoSelect = true
            btnMap.setImage(UIImage(named: "iconLocation.png"), for: UIControlState.normal)
            btnInfo.setImage(UIImage(named: "iconInfo.png"), for: UIControlState.normal)
        }
    }
    
}
