//
//  SlidersTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/22/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class SlidersTableViewCell: UITableViewCell, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollSliders: UIScrollView!
    @IBOutlet weak var pageControlSliders: UIPageControl!
    
    var images = [""]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewDidLayoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControlSliders.numberOfPages = images.count
        pageControlSliders.currentPage = Int(pageNumber)
        pageControlSliders.currentPageIndicatorTintColor = UIColor.white
        pageControlSliders.size(forNumberOfPages: images.count)
    }
    
    func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = scrollSliders.frame.size.width * CGFloat(index)
                frame.size = scrollSliders.frame.size
                
                let view = UIView(frame: frame)
                view.bounds.size.height = scrollSliders.frame.size.height
                //            view.backgroundColor = UIColor.red
                let image = UIImageView()
                if images[index] != "" {
                    Nuke.loadImage(with: URL(string: images[index])!, into: image)
                }else{
                    image.image = UIImage(named: "placeholderImageRed")
                }
                
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: scrollSliders.frame.size)
                view.clipsToBounds = true
                
                
                scrollSliders.addSubview(image)
                
                width += Int(scrollSliders.frame.width)
                
                self.scrollSliders.addSubview(view)
            }
            scrollSliders.contentSize = CGSize(width: (scrollSliders.frame.size.width * CGFloat(images.count)), height: scrollSliders.frame.size.height)
            scrollSliders.delegate = self
        }
        
    }

}
