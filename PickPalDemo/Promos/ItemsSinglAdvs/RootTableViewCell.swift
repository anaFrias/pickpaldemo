//
//  RootTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/22/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class RootTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblComida: UILabel!
    @IBOutlet weak var lblMunicipio: UILabel!
    @IBOutlet weak var lblCategoria: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
