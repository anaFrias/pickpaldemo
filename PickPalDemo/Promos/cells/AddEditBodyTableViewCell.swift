//
//  AddEditBodyTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 1/9/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

protocol AddEditBodyDelegate {
    func OnTitleTextChange(title: String)
    
    func OnDescriptionTextChange(Description: String)
    
    func OnDateTextChange(Date: String, isFirstDate: Bool)
    
    func OnSelectImage(idImage: Int)
    
    func OnEditing()
    
    func onClearDates(isFirstDate: Bool)
}

class AddEditBodyTableViewCell: UITableViewCell, PhotosDelegate, SelectImageAdvsDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var clPhotosAdvs: UICollectionView!
    @IBOutlet weak var tfNameAadvs: UITextField!{
        didSet{
            tfNameAadvs.setLeftView(image: UIImage.init(named: "ic_title_descrip")!)
            tfNameAadvs.tintColor = .darkGray
        }
    }
    
    @IBOutlet weak var tfDescripAdvs: UITextField!{
        didSet{
            tfDescripAdvs.setLeftView(image: UIImage.init(named: "ic_title_descrip")!)
            tfDescripAdvs.tintColor = .darkGray
        }
    }
    @IBOutlet weak var tfDateAdvs: UITextField!{
        didSet{
            tfDateAdvs.setLeftView(image: UIImage.init(named: "calendar_advs")!)
            tfDateAdvs.tintColor = .darkGray
        }
    }
    
    @IBOutlet weak var tfDateFinal: UITextField!{
        didSet{
            tfDateFinal.setLeftView(image: UIImage.init(named: "calendar_advs")!)
            tfDateFinal.tintColor = .darkGray
        }
    }
    
    var delegate: AddEditBodyDelegate!
    var mAdminPromotionals = Promotionals()
    var mListImages = [Images]()
    let datePicker = UIDatePicker()
    var mDateStringInit = ""
    var mDateStringFinal = ""
    var isFromEdit = Bool()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        clPhotosAdvs.delegate = self
        clPhotosAdvs.dataSource = self
        tfNameAadvs.delegate = self
        showDatePicker()
    }
    
    func showDatePicker(){
        displayCalendarToUser()
    }
    
    func displayCalendarToUser(){
        let date = Date()
        
        let calendar = Calendar.current
        let cal = Calendar(identifier: .gregorian)
        
        var components = DateComponents()
        
        let anio = calendar.component(.year, from: date)
        let mes = calendar.component(.month, from: date)
        let dia = getDayOfMoth(currentYear: calendar.component(.year, from: date), currentMonth: calendar.component(.month, from: date), currentDay: calendar.component(.day, from: date))
        
        components.calendar = cal
        components.day = dia
        components.month = mes
        components.year = anio
        
        let maxDate = calendar.date(from: components)
        print(maxDate! as Date)
        
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.maximumDate = maxDate! as Date
        
        tfDateAdvs.inputAccessoryView = createToolbarToDate(isDateStart: true)
        tfDateFinal.inputAccessoryView = createToolbarToDate(isDateStart: false)
        tfDateAdvs.inputView = datePicker
        tfDateFinal.inputView = datePicker
    }
    
    func createToolbarToDate(isDateStart: Bool)->UIToolbar{
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton:UIBarButtonItem
        let cancelButton:UIBarButtonItem
        
        if isDateStart {
            doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePickerDateStart));
            cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelDatePickerStart));
        }else{
            doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePickerDateFinal));
            cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelDatePickerFinal));
        }
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        return toolbar
    }
    
    @objc func donedatePickerDateStart(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        mDateStringInit = formatter.string(from: datePicker.date)
        tfDateAdvs.text = mDateStringInit
        self.delegate.OnDateTextChange(Date: formatter.string(from: datePicker.date), isFirstDate: true)
        self.delegate.OnEditing()
    }
    
    @objc func donedatePickerDateFinal(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        mDateStringFinal = formatter.string(from: datePicker.date)
        tfDateFinal.text = mDateStringFinal
        
        self.delegate.OnDateTextChange(Date: formatter.string(from: datePicker.date), isFirstDate: false)
        self.delegate.OnEditing()
    }
    
    @objc func cancelDatePickerStart(){
        self.delegate.OnEditing()
        if isFromEdit {
            tfDateAdvs.text = mDateStringInit
            self.delegate.OnDateTextChange(Date: mDateStringInit, isFirstDate: true)
        }else{
            tfDateAdvs.text = ""
            self.delegate.onClearDates(isFirstDate: true)
        }
    }
    
    @objc func cancelDatePickerFinal(){
        self.delegate.OnEditing()
        if isFromEdit {
            tfDateFinal.text = mDateStringFinal
            self.delegate.OnDateTextChange(Date: mDateStringFinal, isFirstDate: false)
        }else{
            tfDateFinal.text = ""
            self.delegate.onClearDates(isFirstDate: false)
        }
    }
    
    func getDayOfMoth(currentYear: Int, currentMonth: Int, currentDay: Int) -> Int {
        let dateComponents = DateComponents(year: currentYear, month: currentMonth)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        //let daysInMonth = range.count
        print("Dias del mes: " + String(range.count))
        return range.count
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func OnSelectImage(id: Int) {
        for image in mListImages {
            if image.id == id {
                image.isSelected = true
                self.delegate.OnSelectImage(idImage: id)
            }else{
                image.isSelected = false
            }
        }
        clPhotosAdvs.reloadData()
    }
    
    func SelectImage(id: Int) {
        self.delegate.OnSelectImage(idImage: id)
    }
    
    func UnSelectImage() {
        self.delegate.OnSelectImage(idImage: 0)
    }
    
    @IBAction func OnChangeTitle(_ sender: Any) {
        print(tfNameAadvs.text!.count)
        self.delegate.OnTitleTextChange(title: tfNameAadvs.text!)
    }
    
    @IBAction func OnChangeDescription(_ sender: Any) {
        checkMaxlength(texField: tfDescripAdvs, maxLenth: 100)
        self.delegate.OnDescriptionTextChange(Description: tfDescripAdvs.text!)
        print(tfDescripAdvs.text?.count ?? 0)
    }
    
    @IBAction func OnChangeDate(_ sender: Any) {
        print("tfDateAdvs: \(tfDateAdvs.text!)")
    }
    
    func checkMaxlength(texField: UITextField, maxLenth: Int){
        if texField.text!.count > maxLenth{
            texField.deleteBackward()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength: Int = 100
        
        if textField == tfNameAadvs{
            maxLength = 45
        }else if textField == tfDescripAdvs{
            maxLength = 100
        }
        
        let currentString: NSString = textField.text! as NSString
        
        let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}

extension UITextField {
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
    }
}

extension AddEditBodyTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView (_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mListImages.count
    }
    
    func collectionView (_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "proCell", for: indexPath) as! PhotosCollectionViewCell
        if let image = mListImages[indexPath.row].image, !image.isEmpty {
            Nuke.loadImage(with: URL(string: image)!, into: cell.imgImagePromo)
        }
        
        cell.delegate = self
        cell.idPromo = mListImages[indexPath.row].id
        cell.isSelect = mListImages[indexPath.row].isSelected
        
        if mListImages[indexPath.row].isSelected {
            cell.imgCheckSelectImage.isHidden = false
            cell.viewShadowImage.isHidden = false
        }else{
            cell.imgCheckSelectImage.isHidden = true
            cell.viewShadowImage.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 130.0, height: 100.0)
    }
}
