//
//  AdminPromosBottomCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol ButtonAdminPromosDelgate {
    func OnAddNewPromo()
    func OnErrorAddNew(message:String)
}

class AdminPromosBottomCell: UITableViewCell {
    
    @IBOutlet weak var lblNoMorePromos: UILabel!
    @IBOutlet weak var contentButtonAddPromo: UIView!
    @IBOutlet weak var viewButtonCorner: UIButton!
    
    var delegate: ButtonAdminPromosDelgate!
    var currentPromos: Int!
    var totalPackaagePromo: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func onGoToAddNewPromo(_ sender: Any) {
        if currentPromos >= totalPackaagePromo{
            self.delegate.OnErrorAddNew(message: "Lo sentimos, has agotado tus promociones")
        }else{
            self.delegate.OnAddNewPromo()
        }
    }
    
}
