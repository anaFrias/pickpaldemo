//
//  SelectImageAdvsTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/27/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol SelectImageAdvsDelegate {
    func SelectImage(id: Int)
    func UnSelectImage()
}

class SelectImageAdvsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgImageAdvs: UIImageView!
    @IBOutlet weak var imgCheckAdvs: UIImageView!
    @IBOutlet weak var viewShadow: UIView!
    
    var mIdImage = Int()
    var isSelect = Bool()
    
    var delegate: SelectImageAdvsDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func OnTapSelectedImageAdvs(_ sender: Any) {
       if isSelect {
            self.delegate.UnSelectImage()
            self.viewShadow.isHidden = true
            self.imgCheckAdvs.isHidden = true
            self.imgCheckAdvs.layoutIfNeeded()
            self.viewShadow.layoutIfNeeded()
        }else{
            self.delegate.SelectImage(id: self.mIdImage)
            self.viewShadow.isHidden = false
            self.imgCheckAdvs.isHidden = false
            self.imgCheckAdvs.layoutIfNeeded()
            self.viewShadow.layoutIfNeeded()
        }
    }
    
}
