//
//  HorizontalPromoTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

protocol HPromosDelegate {
    func sharePromotional(position: Int)
    
    func onGoToSingle(position: Int)
    
    func sendCaptureimage(image: UIImage)
}

protocol HPpromosHorizontalDelegate {
    func onPressedItemPromo(promo: Promotional)
    func noInstallWhatsapp()
    func shared(imagesToShare: [AnyObject])
}

class HorizontalPromoTableView: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, HPromosDelegate {
    
    var mIndexPath = IndexPath()
    func sharePromotional(position: Int) {
        self.shareNow(position: position)
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var promos = [Promotional]()
    var delegate: HPpromosHorizontalDelegate!
    
    var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
    override func prepareForReuse() {
        collectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "PromosItemAuxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PromosItemAux")
        collectionView.register(UINib(nibName: "PromosImagenItemCell", bundle: nil), forCellWithReuseIdentifier: "PromosItemImagen")
        collectionView.register(UINib(nibName: "EmptyPromosCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cellEmptyPromos")
    }
    
    func onGoToSingle(position: Int) {
        self.delegate.onPressedItemPromo(promo: promos[position])
    }
    
    func sendCaptureimage(image: UIImage) {
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image as AnyObject)
        self.delegate.shared(imagesToShare: imagesToShare)
    }
    
    func shareNow(position: Int){
        UIGraphicsBeginImageContext(collectionView.frame.size)
        collectionView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image)
        imagesToShare.append("Hola mundo" as AnyObject)
        self.delegate.shared(imagesToShare: imagesToShare)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        mIndexPath = indexPath
        if promos[indexPath.row].isEmpty {
            let itemOne = collectionView.dequeueReusableCell(withReuseIdentifier: "cellEmptyPromos", for: indexPath) as! EmptyPromosCollectionViewCell
            itemOne.layer.masksToBounds = false
            itemOne.contenEmptyPromos.layer.cornerRadius = 15
            itemOne.layer.shadowColor = UIColor.black.cgColor
            itemOne.layer.shadowOffset = CGSize(width: 5, height: 5)
            itemOne.layer.shadowRadius = CGFloat(5)
            itemOne.layer.shadowOpacity = 0.24
            itemOne.center.x = self.center.x
            let not = NSAttributedString(string: "No hay \n", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller-Bold", size: 25.0)])
            let promo_today = NSAttributedString(string: "promociones del día", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 25.0)])
            var newString = NSMutableAttributedString()
            newString.append(not)
            newString.append(promo_today)
            itemOne.lblPromosDIa.attributedText = newString
            return itemOne
        }else{
            
            if promos[indexPath.row].isCustomImage{
            
         
                let itemImagen = collectionView.dequeueReusableCell(withReuseIdentifier: "PromosItemImagen", for: indexPath) as! PromosImagenItemCell
                if promos.count == 1{
                    itemImagen.center.x = self.center.x
                }
                
                
                itemImagen.cellContent.clipsToBounds = true
                itemImagen.cellContent.layer.masksToBounds = true
                itemImagen.cellContent.layer.cornerRadius = 15
                itemImagen.cellContent.layer.shadowColor = UIColor.black.cgColor
                itemImagen.cellContent.layer.shadowOpacity = 0.35
                itemImagen.cellContent.layer.shadowOffset = .zero
                itemImagen.cellContent.layer.shadowRadius = 5
//
//                 itemImagen.cellContent.clipsToBounds = true
//                 itemImagen.cellContent.layer.cornerRadius = 18
//                    itemImagen.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
//
//                 itemImagen.cellContent.clipsToBounds = true
//                 itemImagen.cellContent.layer.cornerRadius = 18
//                 itemImagen.cellContent.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
//
                itemImagen.clipsToBounds = true
                                itemImagen.layer.masksToBounds = true
                                itemImagen.layer.cornerRadius = 15
                                itemImagen.layer.shadowColor = UIColor.black.cgColor
                                itemImagen.layer.shadowOpacity = 0.35
                                itemImagen.layer.shadowOffset = .zero
                                itemImagen.layer.shadowRadius = 5
                
                itemImagen.delegate = self
                itemImagen.position = indexPath.row
                itemImagen.lblTitlePromo.text = promos[indexPath.row].establishment
                itemImagen.lblDescriptionPromo.text = promos[indexPath.row].category_info
                itemImagen.lblDatePromotion.text = promos[indexPath.row].deadline
                
                
                
//                itemImagen.layer.masksToBounds = false
//                itemImagen.layer.shadowColor = UIColor.black.cgColor
//                itemImagen.layer.shadowOffset = CGSize(width: 5, height: 5)
//                itemImagen.layer.shadowRadius = CGFloat(5)
//                itemImagen.layer.shadowOpacity = 0.24
                Nuke.loadImage(with: URL(string: promos[indexPath.row].image)!, into: itemImagen.imgPromo)
                
                UIGraphicsBeginImageContext(itemImagen.frame.size)
                itemImagen.layer.render(in: UIGraphicsGetCurrentContext()!)
                let images = UIGraphicsGetImageFromCurrentImageContext()!
                UIGraphicsEndImageContext()
                itemImagen.image = images
                
                
                return itemImagen
                
                
            }else{
                
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "PromosItemAux", for: indexPath) as! PromosItemAuxCollectionViewCell
                if promos.count == 1{
                    item.center.x = self.center.x
                }
                item.cellContentHorizontalPromo.clipsToBounds = true
                item.cellContentHorizontalPromo.layer.masksToBounds = true
                item.cellContentHorizontalPromo.layer.cornerRadius = 15
                item.cellContentHorizontalPromo.layer.shadowColor = UIColor.black.cgColor
                item.cellContentHorizontalPromo.layer.shadowOpacity = 0.35
                item.cellContentHorizontalPromo.layer.shadowOffset = .zero
                item.cellContentHorizontalPromo.layer.shadowRadius = 5
                item.delegate = self
                item.position = indexPath.row
                item.headerTitle.text = promos[indexPath.row].establishment
                item.headerDescription.text = promos[indexPath.row].category_info
                item.bodyTitle.text = promos[indexPath.row].name
                item.bodyDescription.text = promos[indexPath.row].descr
                item.disclaimer.text = promos[indexPath.row].deadline
//                item.headerView.clipsToBounds = true
//                item.headerView.layer.cornerRadius = 15
//                item.headerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//                item.layer.masksToBounds = false
//                item.layer.shadowColor = UIColor.black.cgColor
//                item.layer.shadowOffset = CGSize(width: 5, height: 5)
//                item.layer.shadowRadius = CGFloat(5)
//                item.layer.shadowOpacity = 0.24
                Nuke.loadImage(with: URL(string: promos[indexPath.row].image)!, into: item.imageView)
                
                UIGraphicsBeginImageContext(item.frame.size)
                item.layer.render(in: UIGraphicsGetCurrentContext()!)
                let images = UIGraphicsGetImageFromCurrentImageContext()!
                UIGraphicsEndImageContext()
                item.image = images
                
                return item
                        
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if promos[indexPath.row].isEmpty {
            return CGSize(width: 300.0, height: 200.0)
        }else{
            return CGSize(width: 317.0, height: 240.0)
        }
    }
    
}
