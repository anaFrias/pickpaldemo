//
//  EmptyPromosCollectionViewCell.swift
//  PickPalDemo
//
//  Created by Dev on 2/28/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class EmptyPromosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPromosDIa: UILabel!
    @IBOutlet weak var imgBigPromo: UIImageView!
    @IBOutlet weak var contenEmptyPromos: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
