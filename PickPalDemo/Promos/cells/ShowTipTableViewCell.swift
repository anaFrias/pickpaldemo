//
//  ShowTipTableViewCell.swift
//  PickPalDemo
//
//  Created by Dev on 3/3/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ShowTipTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPromoOne: UILabel!
    @IBOutlet weak var lblPromoTwo: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
