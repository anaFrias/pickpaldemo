//
//  PromosImagenItemCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 24/11/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromosImagenItemCell: UICollectionViewCell {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblDatePromotion: UILabel!
    @IBOutlet weak var imgPromo: UIImageView!
    @IBOutlet weak var lblTitlePromo: UILabel!
    @IBOutlet weak var lblDescriptionPromo: UILabel!
    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var cellContent: UIView!
    
    var delegate: HPromosDelegate!
    var position = Int()
    var isPrreview = false
    var image = UIImage()
    override func awakeFromNib() {
        super.awakeFromNib()
        

    }
    
    @IBAction func btnSharePromo(_ sender: Any) {
        
        if !isPrreview{
                let scale = UIScreen.main.scale
                UIGraphicsBeginImageContextWithOptions(cellContent.bounds.size, true, scale)
                cellContent.layer.render(in: UIGraphicsGetCurrentContext()!)
                let images = UIGraphicsGetImageFromCurrentImageContext()!
            
                UIGraphicsEndImageContext()
                delegate.sendCaptureimage(image: images)
            
            
        }
        
        
    }
    
    @IBAction func btnGoSingle(_ sender: Any) {
        if !isPrreview{
        delegate.onGoToSingle(position: position)
        }
    }
    

}
