//
//  AdminPromosBodyCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

protocol MyPromosDelegate {
    func goToEdit(id: Int, nameEstab: String, indice: Int, isNews: Bool)
    func showDialogToDelete(PromotionalId: Int, nameEstab: String, valueCall: Int, message: String, titleButton: String, toHome: Bool, isNews: Bool)
}

class AdminPromosBodyCell: UITableViewCell {

    @IBOutlet weak var lblNamePromo: UILabel!
    @IBOutlet weak var imgEditPromo: UIButton!
    @IBOutlet weak var imgDeletePromo: UIButton!
    @IBOutlet weak var pickpalLogoContent: UIView!
    @IBOutlet weak var viewPromotionals: UIView!
    @IBOutlet weak var imgImageSelected: UIImageView!
    
    var mIndice = Int()
    var id = Int()
    var nameEstab = String()
    var isNews = Bool()
    var delegate: MyPromosDelegate!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func goToDelete(_ sender: Any) {
        let message = isNews ? "¿Deseas eliminar esta noticia?" : "¿Deseas eliminar esta promoción?"
        self.delegate.showDialogToDelete(PromotionalId: self.id, nameEstab: self.nameEstab, valueCall: 0, message: message, titleButton: "Eliminar", toHome: false, isNews: isNews)
    }
    
    @IBAction func goToEdit(_ sender: Any) {
        self.delegate.goToEdit(id: self.id, nameEstab: self.nameEstab, indice: self.mIndice, isNews: isNews)
    }

}
