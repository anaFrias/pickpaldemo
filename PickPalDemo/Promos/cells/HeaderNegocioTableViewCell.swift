//
//  HeaderNegocioTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/24/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class HeaderNegocioTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNegocios: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
