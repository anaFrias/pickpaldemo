//
//  PhotosCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 1/2/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol PhotosDelegate {
    func OnSelectImage(id: Int)
}

class PhotosCollectionViewCell: UICollectionViewCell {
    
    var delegate: PhotosDelegate!
    var isSelect = Bool()
    var idPromo = Int()
    
    @IBOutlet weak var imgImagePromo: UIImageView!
    @IBOutlet weak var imgCheckSelectImage: UIImageView!
    @IBOutlet weak var viewShadowImage: UIView!
    
    @IBAction func didPressOnSelectImage(_ sender: Any) {
        self.delegate.OnSelectImage(id: self.idPromo)
    }
}
