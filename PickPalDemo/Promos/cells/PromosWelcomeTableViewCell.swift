//
//  PromosWelcomeTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/9/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromosWelcomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var grettinsLabel: UILabel!
    @IBOutlet weak var adviceLabel: UILabel!
    @IBOutlet weak var headerIcon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
