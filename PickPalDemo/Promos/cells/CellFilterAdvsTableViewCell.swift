//
//  CellFilterAdvsTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/28/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class CellFilterAdvsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
