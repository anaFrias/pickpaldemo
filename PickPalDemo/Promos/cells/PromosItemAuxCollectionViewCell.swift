//
//  PromosItemAuxCollectionViewCell.swift
//  PickPalDemo
//
//  Created by Dev on 1/29/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromosItemAuxCollectionViewCell: UICollectionViewCell {

    var delegate: HPromosDelegate!
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerDescription: UILabel!
    @IBOutlet weak var bodyTitle: UILabel!
    @IBOutlet weak var bodyDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var disclaimer: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cellContentHorizontalPromo: UIView!
    
    var position = Int()
    var isPreview = false
    var image = UIImage()
    
    @IBAction func sharedPressed(_ sender: UIButton) {
        /*print(position.description)1920 x 1080
        delegate.sharePromotional(position: position)*/
        if !isPreview {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(cellContentHorizontalPromo.bounds.size, true, scale)
        cellContentHorizontalPromo.layer.render(in: UIGraphicsGetCurrentContext()!)
        let images = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        delegate.sendCaptureimage(image: images)
        }
        
    }
    @IBAction func contentCellPressed(_ sender: Any) {
        if !isPreview {
        delegate.onGoToSingle(position: position)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
