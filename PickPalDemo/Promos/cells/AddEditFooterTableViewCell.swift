//
//  AddEditFooterTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 1/9/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol AddEditFooterDelegate {
    func OnClickSaveCallBack()
}

class AddEditFooterTableViewCell: UITableViewCell {

    var delegate: AddEditFooterDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func OnClickSave(_ sender: Any) {
        self.delegate.OnClickSaveCallBack()
    }
    
}
