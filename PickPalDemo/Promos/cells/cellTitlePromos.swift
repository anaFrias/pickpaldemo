//
//  cellTitlePromos.swift
//  PickPalTesting
//
//  Created by Hector Vela on 2/14/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class cellTitlePromos: UITableViewCell {

    @IBOutlet weak var lblTilePromo: UILabel!
    
    var nameTitle = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTilePromo.text = nameTitle
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
