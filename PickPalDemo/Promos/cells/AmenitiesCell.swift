
import UIKit

class AmenitiesCell: UITableViewCell {
    @IBOutlet weak var amenityIcon: UIImageView!
    @IBOutlet weak var amenityTitle: UILabel!
    @IBOutlet weak var amenityDescription: UILabel!
    
}
