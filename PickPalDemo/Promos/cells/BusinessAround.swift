//
//  BusinessAround.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/12/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol businessSingleDelegate {
    func goToSingle(position: Int)
    func suscribeNotifications(position: Int)
}
class BusinessAround: UITableViewCell {
    
    @IBOutlet weak var establishmentCover: UIImageView!
    @IBOutlet weak var promoCount: UILabel!
    @IBOutlet weak var partnerImage: UIImageView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessKind: UILabel!
    @IBOutlet weak var iconBell: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    
    var position = Int()
    var delegate: businessSingleDelegate!
    
    @IBAction func goSingle(_ sender: UIButton) {
        print("single establishment")
        self.delegate.goToSingle(position: position)
    }
    
    @IBAction func suscribeNotifications(_ sender: UIButton) {
        print("notificaciones")
        self.delegate.suscribeNotifications(position: position)
    }
}
