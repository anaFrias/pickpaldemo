//
//  NegociosTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/24/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol AdvsDelegate {
    func goToMyPomos(id: Int, nameEstab: String, is_month_payment: Bool, descriptionEstablishment: String)
}

class NegociosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var lblNameEstab: UIButton!
    @IBOutlet weak var viewCell: UIView!
    
    var id = Int()
    var nameEstab = String()
    var is_month_payment = Bool()
    var descriptionEstablishment = String()
    var delegate: AdvsDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func goToPromos(_ sender: Any) {
        self.delegate.goToMyPomos(id: self.id, nameEstab: self.nameEstab, is_month_payment: self.is_month_payment, descriptionEstablishment: self.descriptionEstablishment)
    }
}
