//
//  AdminPromosHeaderCell.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AdminPromosHeaderCell: UITableViewCell {

    @IBOutlet weak var lblTotalPromos: UILabel!
    @IBOutlet weak var lblNameEstablishment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
