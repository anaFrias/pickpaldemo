import UIKit

class RedesSocialesItemCell: UITableViewCell {
    
    @IBOutlet weak var buttonWeb: UIButton!
    var webUrl = String()
    @IBOutlet weak var buttonInsta: UIButton!
    var instaUrl = String()
    @IBOutlet weak var buttonFace: UIButton!
    var faceUrl = String()
    @IBOutlet weak var buttonTwitter: UIButton!
    var twitterUrl = String()
    @IBOutlet weak var lblSocialNetwork: UILabel!
    
    @IBAction func buttonWeb(_ sender: UIButton) {
        self.launchUrl(url: webUrl)
    }
    
    
    @IBAction func buttonInsta(_ sender: UIButton) {
        self.launchUrl(url: instaUrl)
    }
    
    
    @IBAction func buttonFace(_ sender: UIButton) {
        self.launchUrl(url: faceUrl)
    }
    
    
    @IBAction func buttonTwitter(_ sender: UIButton) {
        self.launchUrl(url: twitterUrl)
    }
    
    func launchUrl(url: String){
        if let reviewURL = URL(string: url), UIApplication.shared.canOpenURL(reviewURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(reviewURL)
            }
        }
    }
}
