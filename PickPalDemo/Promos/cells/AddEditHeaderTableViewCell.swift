//
//  AddEditHeaderTableViewCell.swift
//  PickPalTesting
//
//  Created by Dev on 1/9/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AddEditHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNameEstablishment: UILabel!
    @IBOutlet weak var lblNameTitleScreen: UILabel!
    @IBOutlet weak var imgPromo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
