
import UIKit

class promosAmenitiesCell: UITableViewCell {

    @IBOutlet weak var descr_label: UILabel!
    @IBOutlet weak var descr_text: UILabel!
    @IBOutlet weak var category_label: UILabel!
    @IBOutlet weak var category_text: UILabel!
    @IBOutlet weak var schedules_label: UILabel!
    @IBOutlet weak var schedules_text: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
