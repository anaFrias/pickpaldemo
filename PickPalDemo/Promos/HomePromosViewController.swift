//
//  HomePromosViewController.swift
//  PickPalTesting
//
//  Created by Dev on 11/12/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import Foundation
import CoreLocation
import Nuke
import Firebase
import SQLite
import OneSignal

class HomePromosViewController: UIViewController, googleWSDelegate, PromosDelegate, selectedSearchValue, cellActionsDelegate, userDelegate, setValuesDelegate, businessSingleDelegate, CLLocationManagerDelegate, HPpromosHorizontalDelegate, AlertAdsDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var newsFeedTotal: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var viewNewsFeed: UIView!
    
    var structures = [String]()
    
    var valueInfo = String()
    var valuePlace = String()
    var valueCat = String()
    var valueState = String()
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    
    var defaults = UserDefaults.standard
    var logoString = String()
    var ids = [Int]()
    
    var gws = GoogleServices()
    var pmws = PromosWS()
    var userws = UserWS()
    //    Location variables
    var locationManager = CLLocationManager()
    var myCurrentLocation:CLLocationCoordinate2D!
    var latitudes = [Double]()
    var longitudes = [Double]()
    
    var promos = [Promotional]()
    var establishments = [PromoEstablishment]()
    
    var ref: DatabaseReference!
    var isUpdatedVersion = Bool()
    var isAdminPickPal = false
    var idBussinessLine = String()
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        //self.tableView.delegate = self
        //self.tableView.dataSource = self
        if locationManager.location == nil {
            
            initlocationManager()
            
        }
        DatabaseFunctions.shared.createDB()
        registerDeviceNotifications()
        self.setLocationData()
        
        ref = Database.database().reference().child("establishmentsadvs")
        
        self.tableView.reloadData()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
        userws.getUnreadNewsFeedLog(client_id: client_id)
        
       
        
    }
    
    func registerDeviceNotifications(){
        if let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId {
            print(userId)
            userws.registerDeviceNotifications(client_pk: UserDefaults.standard.object(forKey: "client_id") as! Int, token: userId)
        }
        
    }
    
    func didSuccessRegisterDeviceNotifications() {
        OneSignal.setSubscription(true)
        print("success on register notification")
    }
    
    func didFailRegisterDeviceNotifications(title: String, subtitle: String) {
        print(title)
    }
    
    func didSuccessGetNewsFeedLog(newsFeed: Int) {
        if newsFeed > 0 {
            newsFeedTotal.text = "\(1)"
            viewNewsFeed.alpha = 1
        }else{
            viewNewsFeed.alpha = 0
        }
        
    }
    
    func shared(imagesToShare: [AnyObject]) {
        let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func closeModal() {
        searchButton.alpha = 1
    }
    
    func didFailGetNewsFeedLog(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    //16/12/2019 Josue Valdez.- Se agrga funcion para el boton de settings
    @IBAction func ShowSettings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = true
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        gws.delegate = self
        pmws.delegate = self
        userws.delegate = self
        userws.isUpdateAvailable()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            let clocation = locationManager.location
            if clocation?.coordinate.latitude == nil{
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                    //                    locationManager.requestAlwaysAuthorization()
                    locationManager.requestWhenInUseAuthorization()
                }
            }else{
                userLat = (clocation?.coordinate.latitude)!//20.5880600 //QRO//19.4326//CDMX
                userLng = (clocation?.coordinate.longitude)!//-100.3880600 //QRO//-99.13330000000002//CDMX
                //                userLat = 20.5880600 //QRO//19.4326//CDMX
                //                userLng = -100.3880600 //QRO//-99.13330000000002//CDMX
                print(userLat)
                print(userLng)
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
            }
        }
        
        structures.removeAll()
        structures = ["grettings", "filters", "banner", "horizontal", "showtip"]
    }
    
    func didSuccessStatusProfile(avalidableAdvs: StatusProfile){
        self.isAdminPickPal = avalidableAdvs.availableAdvs
        pmws.viewEstablishmentAround(latitude: userLat, longitude: userLng, codeState: self.valueState.stripingDiacritics)
    }
    
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
        }
    }
    
    @IBAction func toNewsFeed(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
            newVC.isFromPromos = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToSingle(position: Int) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        pmws.viewSingleAdvsEstablishment(establishmentID: establishments[position].id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
        if let logoimg = establishments[position].logo{
            self.logoString = logoimg
        }
    }
    
    func noInstallWhatsapp() {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "Lo sentimos, no podemos compartir esta promoción porque Whatsapp no esta instalado en este dispositivo."
        newXIB.mTitle = "PickPal Notipromos"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.isFromAdd = true
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs) {
        LoadingOverlay.shared.hideOverlayView()
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        newVC.singleInfo = info
        newVC.id = info.id
        newVC.municipioStr = info.address.city
        newVC.categoriaStr = info.address.colony
        newVC.lugarString = info.advsCategorry
        newVC.establString = info.name
        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailSingleAdvs(error: String, subtitle: String) {
        print(subtitle)
    }
    
    func onPressedItemPromo(promo: Promotional) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        var idEstab = 0
        for promoAdvs in establishments{
            //let str = String(promoAdvs.name.utf8.prefix(14))
            let normalizedEstabName = promoAdvs.name!.folding(options: .diacriticInsensitive, locale: nil)
            if normalizedEstabName.elementsEqual(promo.establishment.folding(options: .diacriticInsensitive, locale: nil))  {
                idEstab = promoAdvs.id
                break
            }
        }
        
        if idEstab != 0 {
            pmws.viewSingleAdvsEstablishment(establishmentID: idEstab, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }
    }
    
    func suscribeNotifications(position: Int) {
        print("notificationes", establishments[position].activeNotifications)
        
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                pmws.businessSuscription(establishmentID: establishments[position].id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int, suscribe: !establishments[position].activeNotifications, position: position)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            pmws.businessSuscription(establishmentID: establishments[position].id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int, suscribe: !establishments[position].activeNotifications, position: position)
        }
    }
    
    func didSuccessSuscribre(position: Int, isSuscribe: Bool) {
        establishments[position].activeNotifications = !establishments[position].activeNotifications
        tableView.reloadRows(at: [IndexPath(row: position + 5, section: 0)], with: .automatic)
        if isSuscribe{
            showToast(message: "Alertas activadas para " + establishments[position].name)
        }else{
            showToast(message: "Alertas desactivadas para " + establishments[position].name)
        }
    }
    
    func openModalAndSingle(modal: Bool, place: Bool, category: Bool, city: Bool) {
        if isUpdatedVersion {
            if modal {
                searchButton.alpha = 0
                if place {
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Promos"
                    newXIB.municipio =  valueInfo
                    newXIB.categoria = valueCat
                    newXIB.places = false
                    newXIB.delegate = self
                    newXIB.state = valueState
                    newXIB.idBussinessLine = idBussinessLine
                    newXIB.from = "adv"
                    present(newXIB, animated: true, completion: nil)
                }else if category {
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Categoría"
                    newXIB.municipio = valueInfo
                    newXIB.categoria = "none"
                    newXIB.places = false
                    newXIB.categories = true
                    newXIB.delegate = self
                    newXIB.state = valueState
                    newXIB.from = "adv"
                    present(newXIB, animated: true, completion: nil)
                }else{
                    let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .coverVertical
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.titleString = "Estado"
                    newXIB.municipio = "none"
                    newXIB.categoria = "none"
                    newXIB.places = true
                    newXIB.delegate = self
                    newXIB.state = valueState
                    newXIB.from = "adv"
                    present(newXIB, animated: true, completion: nil)
                }
            }else{
                print("cat", valueCat)
                print("place", valuePlace)
                if valueCat != "Categoría" && valuePlace != "Ubicación" {
                    print(ref)
                    ref.observe(.value, with: { (snapshot) -> Void in
                        self.ids.removeAll()
                        print(self.valueState)
                        if snapshot.exists(){
                            if let state = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary {
                                print(self.valueInfo)
                                if let singleState = state.value(forKey: self.valueInfo) as? NSDictionary {
                                    print("entrasingle state",singleState)
                                    for item in singleState.allValues as NSArray {
                                        let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                        let categories = (item as! NSDictionary).value(forKey: "category") as! String
                                        
                                        if categories == self.valueCat {
                                            self.ids.append(ids)
                                        }
                                    }
                                }
                            }
                        }
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                        nwVC.ids = self.ids
                        nwVC.isFromPromos = true;
                        nwVC.userLat = self.userLat
                        nwVC.userLng = self.userLng
                        nwVC.valueCity = self.valueInfo
                        nwVC.valueCategory = self.valueCat
                        nwVC.valuePlace = self.valuePlace
                        nwVC.valueState = self.valueState
                        nwVC.typeKitchen = false
                        nwVC.from = "adv"
                        
                        self.navigationController?.pushViewController(nwVC, animated: true)
                    })
                }else{
                    ref.observe( .value, with: { (snapshot) -> Void in
                        print("?", snapshot)
                        self.ids.removeAll()
                        if !(snapshot.value is NSNull) {
                            if let stateQuery = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                                print(stateQuery)
                                print(self.valueInfo)
                                let state = self.valueInfo.elementsEqual("Santiago de Querétaro") ? "Querétaro" : self.valueInfo
                                if let singleState = (stateQuery.value(forKey: state) as? NSDictionary)  {
                                    for item in singleState.allValues as NSArray {
                                        let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                        self.ids.append(ids)
                                        
                                    }
                                }
                            }
                        }
                        
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                        nwVC.ids = self.ids
                        nwVC.isFromPromos = true
                        nwVC.valueCity = self.valueInfo
                        nwVC.valueCategory = self.valueCat
                        nwVC.valuePlace = self.valuePlace
                        nwVC.valueState = self.valueState
                        nwVC.userLat = self.userLat
                        nwVC.userLng = self.userLng
                        nwVC.typeKitchen = false
                        nwVC.from = "adv"
                        
                        self.navigationController?.pushViewController(nwVC, animated: true)
                        
                    })
                }
            }
        }else{
            self.updateAlert()
        }
    }
    
    func deletePromoById(id: Int) {
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        //debido a que valueCall = 1, indica que se va a salir de la pantalla o solo cerrar el dialogo
        if !isAdminPickPal {
            UserDefaults.standard.set("menu", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    
    func setValue(value: String, place: Bool, category: Bool, state: String, isType: Bool, idBussinessLine: String) {
        searchButton.alpha = 1
        if place {
            valueInfo = value
            valueState = state
            //            UserDefaults.standard.set(valueState, forKey: "state")
            //            UserDefaults.standard.set(valueInfo, forKey: "municipalty")
        }else if category {
            valueCat = value
            //            UserDefaults.standard.set(valueCat, forKey: "category")
        }else{
            valuePlace = value
            //            UserDefaults.standard.set(valuePlace, forKey: "place")
        }
        //        MARK: Descomentar si es necesario
        self.idBussinessLine = idBussinessLine
        tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
        //        MARK: get kitchen / valueState
        //ws.getAllTypeKitchen(codeState: valueState)
    }
    
    func didSuccessGetGeolocation(location: String, state: String) {
        let clocation = locationManager.location
        let geoCoder = CLGeocoder()
        let location2 = CLLocation(latitude: userLat, longitude: userLng)
        //        MAARK: Por sis i o por si no
        self.setLocationData()
        geoCoder.reverseGeocodeLocation(location2, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            if let placeMark = placemarks?[0] {
                // Location name
                if let locationName = placeMark.location {
                    //                    print(locationName)
                }
                // Street address
                if let street = placeMark.thoroughfare {
                    //                    print(street)
                }
                // City
                //                if UserDefaults.standard.object(forKey: "state") == nil{
                if let state = placeMark.administrativeArea {
                    if let st = UserDefaults.standard.object(forKey: "state") as? String {
                        self.valueState = st
                    }else{
                        self.valueState = state
                        UserDefaults.standard.set(state, forKey: "state")
                    }
                    //                        self.ws.viewEstablishmentAround(latitude: self.userLat, longitude: self.userLng, codeState: self.valueState)
                }
                // City
                if let city = placeMark.subAdministrativeArea {
                    if let loc = UserDefaults.standard.object(forKey: "municipalty") as? String {
                        self.valueInfo = loc
                    }else{
                        self.valueInfo = location
                        UserDefaults.standard.set(location, forKey: "municipalty")
                    }
                }
                
                // Zip code
                if let zip = placeMark.isoCountryCode {
                    //                    print(zip)
                }
                // Country
                if let country = placeMark.country {
                    //                    print(country)
                }
            }
        })
        
        userws.getStatusProfile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
    }
    
    func didSuccessGetViewEstablishmentAround(info: [Int], total: Int) {
        print(info)
        if total > 0 {
            pmws.getEstablishmentsById(ids: info, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
            pmws.getPromotionals(ids: info, inicio:0, fin: 10)
        }else{
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "No se encontraron promociones, vuelve a intentarlo más tarde"
            newXIB.mTitle = "PickPal Notipromos"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromHome = true
            newXIB.valueCall = 1
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
            let pro = Promotional()
            pro.isEmpty = true
            promos.append(pro)
            tableView.reloadData()
        }
        
    }
    
    func didFailGetViewEstablishmentAround(error: String, subtitle: String) {
        print(subtitle)
        // pmws.viewSingleEstablishment(establishmentID: 63, clientID: 4)
    }
    
    func didSuccessGetPromoEstablshmentsById(info: [PromoEstablishment]) {
        print("---------")
        print(info)
        establishments = info
        for _ in (1...info.count) {
            structures.append("establishment")
        }
        
        tableView.reloadData()
        print("---------")
    }
    func didFailGetEstablshmentsById(error: String, subtitle: String) {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "No se encontraron promociones, vuelve a intentarlo más tarde"
        newXIB.mTitle = "PickPal Notipromos"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.isFromHome = true
        newXIB.valueCall = 1
        newXIB.isFromAdd = true
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        let pro = Promotional()
        pro.isEmpty = true
        promos.append(pro)
        tableView.reloadData()
    }
    func didSuccessGetLastPromotionals(info: [Promotional]) {
        print(info)
        var infoAux = [Promotional]()
        if info.count > 0{
            for promo in info {
                if promo.Header != "Mis Promociones" && promo.Header != "Mis Promociones" {
                    infoAux.append(promo)
                }
            }
            
            promos = infoAux
            tableView.reloadData()
        }else{
            didFailGetEstablshmentsById(error: "", subtitle: "")
        }
        
        
    }
    
    func didFailGetLastPromotionals(error: String, subtitle: String) {
        let pro = Promotional()
        pro.isEmpty = true
        promos.append(pro)
        tableView.reloadData()
    }
    
    func didFailGetLastState(title: String, subtitle: String) {
        showToast(message : title)
    }
    
    func sendMessage(title: String, subtitle: String) {
        print(title)
        print(subtitle)
    }
    func setLocationData(){
        if let mun = UserDefaults.standard.object(forKey: "municipalty") as? String {
            valueInfo = mun  == "Santiago de Querétaro" ? "Querétaro": mun
        }else{
            valueInfo = "Estado"
        }
        
        if let mun = UserDefaults.standard.object(forKey: "place") as? String {
            valuePlace = mun
        }else{
            valuePlace = "Ubicación"
        }
        
        if let mun = UserDefaults.standard.object(forKey: "category") as? String {
            valueCat = mun
        }else{
            valueCat = "Categoría"
        }
        
        if let st = UserDefaults.standard.object(forKey: "state") as? String {
            valueState = st
        }else{
            valueState = ""
        }
    }
    
    @IBAction func backToMenu(_ sender: UIButton) {
        UserDefaults.standard.set("menu", forKey: "place")
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func search(_ sender: UIButton) {
        let newXIB = SearchViewController(nibName: "SearchViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.isFromPromos = true
        newXIB.state = self.valueState
        newXIB.municipality = self.valueInfo
        newXIB.from = "adv"
        present(newXIB, animated: true, completion: nil)
    }
    
    func selectedSearchId(id: Int) {
        var tempids = [Int]()
        tempids.append(id)
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
        nwVC.ids = tempids
        nwVC.isFromPromos = true
        nwVC.userLat = self.userLat
        nwVC.userLng = self.userLng
        nwVC.valueCity = self.valueInfo
        nwVC.valueCategory = self.valueCat
        nwVC.valuePlace = self.valuePlace
        nwVC.valueState = self.valueState
        nwVC.from = "adv"
        self.defaults.set(self.ids, forKey: "ids_establishment")
        self.navigationController?.pushViewController(nwVC, animated: true)
        
    }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-100, width: 300, height: 45))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 2
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    @IBAction func pedidos(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.isFromPromos = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
}

extension HomePromosViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(structures[indexPath.row]){
        case "grettings":
            let cell = tableView.dequeueReusableCell(withIdentifier: "promoHeaderCell", for: indexPath) as! PromosWelcomeTableViewCell
            cell.selectionStyle = .none
            
            let date = Date()
            let calendar = Calendar.current
            
            let hour = calendar.component(.hour, from: date)
            let minute = calendar.component(.minute, from: date)
            let second = calendar.component(.second, from: date)
            
            print("Hora actual: \(hour):\(minute):\(second)")
            
            var welcome = ""
            
            if (hour < 12) {// Check if hour is between 8 am and 11pm
                welcome = "¡Buenos días "
            } else if (hour <= 18) {
                welcome = "¡Buenas tardes "
            } else {
                welcome = "¡Buenas noches "
            }
            
            cell.nameLabel.text = welcome + (UserDefaults.standard.object(forKey: "first_name") as? String ?? "") + "!"
            
            return cell
            
        case "filters":
            let cell = tableView.dequeueReusableCell(withIdentifier: "filtersCell") as! FiltersTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.valueInfo = valueInfo
            cell.valueCat = valueCat
            cell.valuePlace = valuePlace
            cell.collectionViewMenu.reloadData()
            return cell
        case "banner":
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell")!
            cell.selectionStyle = .none
            return cell
        case "horizontal":
            let cell = tableView.dequeueReusableCell(withIdentifier: "horizontalPromos") as! HorizontalPromoTableView
            cell.delegate = self
            cell.selectionStyle = .none
            cell.promos = self.promos
            cell.collectionView.reloadData()
            return cell
        case "showtip":
            let cell = tableView.dequeueReusableCell(withIdentifier: "showtip") as! ShowTipTableViewCell
            if establishments.count > 0 && self.promos.count > 0{
                cell.lblPromoTwo.isHidden = false
                cell.lblPromoOne.text = "Promociones de los comercios cercanos a ti."
            }else{
                cell.lblPromoOne.text = "¡Oops! Cerca de ti no hay comercios con promociones."
                cell.lblPromoTwo.isHidden = true
            }
            
            cell.selectionStyle = .none
            return cell
        default:
            let i = indexPath.row - 5
            if establishments[i].currentPromotionals > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentCell") as! BusinessAround
                if let image = establishments[i].image,image != "" {
                    Nuke.loadImage(with: URL(string: image)!, into: cell.establishmentCover)
                }
                cell.position = i
                cell.delegate = self
                cell.selectionStyle = .none
                cell.partnerImage.isHidden = !establishments[i].noProfit
                cell.promoCount.text = establishments[i].currentPromotionals.description
                cell.businessName.text = establishments[i].name.description
                cell.businessKind.text = establishments[i].advsCategory.description + " - " + establishments[i].subadvsCategory.description
                cell.establishmentCover.layer.masksToBounds = false
                cell.establishmentCover.layer.shadowColor = UIColor.black.cgColor
                cell.establishmentCover.layer.shadowOffset = CGSize(width: 1, height: 0)
                cell.establishmentCover.layer.shadowRadius = CGFloat(5)
                cell.establishmentCover.layer.shadowOpacity = 0.24
                cell.bottomView.layer.masksToBounds = false
                cell.bottomView.layer.shadowColor = UIColor.black.cgColor
                cell.bottomView.layer.shadowOffset = CGSize(width: 1, height: 1)
                cell.bottomView.layer.shadowRadius = CGFloat(5)
                cell.bottomView.layer.shadowOpacity = 0.24
                if establishments[i].activeNotifications {
                    cell.iconBell.image = UIImage(named: "iconBell")
                }
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    
    private func numberOfSections(in tableView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.row == 0 {
        switch(structures[indexPath.row]){
        case "grettings":
            return 110
        case "filters":
            return 50
        case "banner":
            return 60
        case "horizontal":
            return 260
        case "showtip":
            return 50
        default:
            if establishments[indexPath.row - 5].currentPromotionals > 0 {
                return 235
            } else {
                return 0
            }
        }
    }
}

extension HomePromosViewController{
    
   
    
    func initlocationManager(){
        
        let authorizationStatus = CLLocationManager.authorizationStatus()

        if (authorizationStatus == CLAuthorizationStatus.denied) {
            getLocation()
        }else{
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.navigationController?.isNavigationBarHidden = true
           
        }
        
    }

    func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                let alertController = UIAlertController (title: "¡Oh no!", message: "¡Necesitamos tu ubicación para encontrar los comercios cercanos a ti!", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Configuración", style: .cancel) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                                
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (_) -> Void in
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                  
                }
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
            }
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                if CLLocationManager.authorizationStatus() == .denied  {
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                    
                }
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("autorizado")
            }
            
        } else {
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("autorizado")
            manager.requestAlwaysAuthorization()
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
            break
        case .denied, .notDetermined, .restricted:
            print("denegado")
           
        default:
            break
        }
    }
    
    
}
