//
//  PromosCollectionItem.swift
//  PickPalTesting
//
//  Created by Fidel ESpino on 12/9/19.
//  Copyright © 2019 IW. All rights reserved.
//

import UIKit

class PromosCollectionItem: UICollectionViewCell {
    
    var delegate: HPromosDelegate!
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerDescription: UILabel!
    @IBOutlet weak var bodyTitle: UILabel!
    @IBOutlet weak var bodyDescription: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var disclaimer: UILabel!
    @IBOutlet weak var buttonShare: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    var position = Int()
    @IBAction func sharedPressed(_ sender: UIButton) {
        print(position.description)
        delegate.sharePromotional(position: position)
    }
    @IBAction func contentCellPressed(_ sender: Any) {
        delegate.onGoToSingle(position: position)
    }
}
