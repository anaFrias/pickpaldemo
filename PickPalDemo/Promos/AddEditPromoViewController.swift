//
//  AddEditPromoViewController.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class AddEditPromoViewController: UIViewController, PromosDelegate, AlertAdsDelegate, AddEditBodyDelegate, userDelegate, PhotosDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var tfNamePromo: UITextField!{
        didSet{
            tfNamePromo.setLeftView(image: UIImage.init(named: "ic_title_descrip")!)
            tfNamePromo.background = UIImage(named: "RectangleDarkGray")
            tfNamePromo.leftViewMode = UITextFieldViewMode.always
        }
    }
    
    @IBOutlet weak var tfDescripPromo: UITextField!{
        didSet{
            tfDescripPromo.setLeftView(image: UIImage.init(named: "ic_title_descrip")!)
            tfDescripPromo.background = UIImage(named: "RectangleDarkGray")
        }
    }
    @IBOutlet weak var tfDateInit: UITextField!{
        didSet{
            tfDateInit.setLeftView(image: UIImage.init(named: "calendar_advs")!)
            tfDateInit.background = UIImage(named: "RectangleDarkGray")
        }
    }
    
    @IBOutlet weak var tfDateFinal: UITextField!{
        didSet{
            tfDateFinal.setLeftView(image: UIImage.init(named: "calendar_advs")!)
            tfDateFinal.background = UIImage(named: "RectangleDarkGray")
        }
    }
    

    @IBAction func btnOpenGalery(_ sender: Any) {
        
        self.openGallery()
        
    }
    
    @IBAction func btnCancelSelectImange(_ sender: Any) {
        
         conteinerImageSelect.isHidden = true
        
    }
    
    
    
    @IBOutlet weak var conteinerImageSelect: UIView!
    @IBOutlet weak var imageSelect: UIImageView!
    
    @IBOutlet weak var lblRegisterEstabTitle: UILabel!
    @IBOutlet weak var lblSubtitleScreen: UILabel!
    @IBOutlet weak var colectionImage: UICollectionView!
    
    @IBOutlet weak var viewConteinerListImagen: UIView!
    @IBOutlet weak var viewConteinerButtons: UIView!
    
    /*@IBOutlet weak var AddEdirTableView: UITableView!
     @IBOutlet weak var viewBody: UIView!*/
    
    @IBOutlet weak var heightGeneral: NSLayoutConstraint!
    var structures = [String]()
    
    var isFromEdit = Bool()
    var mListImages = [Images]()
    var mAdminPromotionals = Promotionals()
    
    var pmws = PromosWS()
    
    var mIdEstab:Int = 0
    var mNameStab:String = ""
    var mMessageToUser = ""
    var mDateStringInit = ""
    var mDateStringFinal = ""
    var mPromotionalId = 0
    var isFromPromos:Bool = false
    var isNews:Bool = false
    var is_month_payment: Bool = true
    var isUpdatedVersion = Bool()
    var userws = UserWS()
    var menuImagen = false
    var descriptionEstablish = String()
    var titleDatapiker = String()
    
    //variables con valores de los campos de texto
    var mTitleAdvs = ""
    var mDescriptionAdvs = ""
    var mDateAdvs = ""
    var mIdImage: Int = 0
    let datePicker = UIDatePicker()
    var base64String = String()
    var savedPromotionalCode = String()
    var isFirst = true
    
    @IBOutlet weak var viewSaveButtom: UIView!
    var isDateStart = true
    var isGalery = false
    var imagenGalery = UIImage()
    var idImagegalery = Int()
    var savedPromotionalCodeAux = String()
    var urlImgSelect = "https://www.imor.es/wp-content/uploads/2017/09/imagen-de-prueba-320x240.jpg"
    @IBOutlet weak var collectionPreview: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        structures.removeAll()
        //AddEdirTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        structures = ["header", "body"]
        pmws.delegate = self
        userws.delegate = self
        colectionImage.delegate = self
        colectionImage.dataSource = self
        tfNamePromo.delegate = self
        userws.isUpdateAvailable()
        showDatePicker()
        collectionPreview.delegate = self
        collectionPreview.dataSource = self
        collectionPreview.register(UINib(nibName: "PromosItemAuxCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PromosItemAux")
        collectionPreview.register(UINib(nibName: "PromosImagenItemCell", bundle: nil), forCellWithReuseIdentifier: "PromosItemImagen")
        
        lblSubtitleScreen.text = mNameStab
//        titleDatapiker = "Fecha Inicial"
        tfNamePromo.addTarget(self, action: #selector(self.refreshPreview(_:)), for: .editingChanged)
        tfDescripPromo.addTarget(self, action: #selector(self.refreshPreview(_:)), for: .editingChanged)
        tfDateInit.addTarget(self, action: #selector(self.refreshPreview(_:)), for: .editingChanged)
        tfDateFinal.addTarget(self, action: #selector(self.refreshPreview(_:)), for: .editingChanged)
       
        if !isNews{
            if isFromEdit{
                tfDateInit.text = "  \(mAdminPromotionals.start_date!) a \(mAdminPromotionals.end_date!)"
                tfDateFinal.text = mAdminPromotionals.end_date
                tfNamePromo.text = mAdminPromotionals.name
                tfDescripPromo.text = mAdminPromotionals.descriptionAdvs
                /*cell.mDateStringInit = mAdminPromotionals.start_date
                 cell.mDateStringFinal = mAdminPromotionals.end_date*/
                
                mPromotionalId = mAdminPromotionals.id
                mMessageToUser = "Se realizó exitosamente la edición de tu promoción."
                mDateStringInit = mAdminPromotionals.start_date
                mDateStringFinal = mAdminPromotionals.end_date
                mTitleAdvs = mAdminPromotionals.name
                mDescriptionAdvs = mAdminPromotionals.descriptionAdvs
                lblRegisterEstabTitle.text = "Editar Promoción Registrada"
                mIdImage = mAdminPromotionals.image_key
                for image in mListImages {
                    if image.id == mAdminPromotionals.image_key {
                        image.isSelected = true
                        mIdImage = mAdminPromotionals.image_key
                    }else{
                        image.isSelected = false
                    }
                }
                
                isGalery = mAdminPromotionals.isCustomImage
                conteinerImageSelect.isHidden = false
                urlImgSelect  = mAdminPromotionals.image
                
                
                //UICollectionViewController.reloadData()
                
            }else{
                mMessageToUser = "Se realizó exitosamente la creación de tu promoción."
                lblRegisterEstabTitle.text = "Agregar Promoción"
            }
            
            heightGeneral.constant = self.view.frame.height > 700 ?   700:500
            
            //AddEdirTableView.reloadData()
            
        }else{
            
            tfDateInit.isHidden = true
            viewConteinerButtons.isHidden = true
            tfDateFinal.isHidden = true
            colectionImage.isHidden = true
            tfNamePromo.placeholder = " Nombre de Noticia"
            tfDescripPromo.placeholder = " Descripción de Noticia"
            
           
            
            if isFromEdit{
                tfNamePromo.text = mAdminPromotionals.name
                tfDescripPromo.text = mAdminPromotionals.descriptionAdvs
                
                mPromotionalId = mAdminPromotionals.id
                mMessageToUser = "Se realizó exitosamente la edición de tu noticia."
                mTitleAdvs = mAdminPromotionals.name
                mDescriptionAdvs = mAdminPromotionals.descriptionAdvs
                lblRegisterEstabTitle.text = "Editar Noticia Registrada"
            }else{
                lblRegisterEstabTitle.text = "Agregar Noticia"
                mMessageToUser = "Se realizó exitosamente la creación de tu noticia."
                heightGeneral.constant = 450
            }
        }
    }
    
    
    @objc func refreshPreview(_ textField: UITextField) {
//        showDatePicker()
//        tfNamePromo.text = "  \(tfNamePromo.text ?? "")"
        collectionPreview.reloadData()

    }
    
    @objc func refreshPreviewDate(_ textField: UITextField) {
        showDatePicker()
//        tfNamePromo.text = "  \(tfNamePromo.text ?? "")"
        collectionPreview.reloadData()

    }
    
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
        }
    }
    
    @IBAction func goToPedidosActuales(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    
    func validateFields(title: String, message: String, dateStart: String, dateEnd: String) -> String {
        if isNews {
            if title.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty {
                return "Por favor agregue un título a la noticia"
            }else if message.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty{
                return "Por favor agregue una descripción a la noticia"
            }
        }else{
            let date1 = stringToDate(registerDay: dateStart)
            let date2 = stringToDate(registerDay: dateEnd)
            
            if title.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty {
                return "Por favor agregue un título a la promoción"
            }else if message.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty{
                return "Por favor agregue una descripción a la promoción"
            }else if dateStart.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty{
                return "Por favor agregue una fecha inicial a la promoción"
            }else if dateEnd.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty{
                return "Por favor agregue una fecha final a la promoción"
            }else if mIdImage == 0 && path == ""{
                return "Por favor seleccione una imagen"
            }else if date1 > date2{
                return "La fecha final no puede ser menor a la fecha inicial"
            }
        }
        
        return ""
    }
    
    func stringToDate (registerDay: String) -> Date{
        let estimated = registerDay
        let form = DateFormatter()
        form.dateFormat = "dd/MM/yyyy"
        var dateRegister = Date()
        
        if form.date(from: estimated) != nil {
            dateRegister = form.date(from: estimated)!
            return dateRegister
        }
        return dateRegister
    }
    
    func didSuccessSavePromo() {
        LoadingOverlay.shared.hideOverlayView()
        showAlertDialogSuccess()
    }
    
    
    
    func didFailSavePromo(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        if menuImagen{
            
            viewConteinerListImagen.isHidden = true
            menuImagen = false
            
        }else{
            goBack()
            
        }
       
    }
    
    func deletePromoById(id: Int) {
        //No se hace nada aqui
    }
    
    func closeDialogAndExit(toHome: Bool) {
        goBack()
    }
    
    @IBAction func goToHome(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func goBack() {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MyPromotionals") as! MyPromotionalsViewController
        newVC.mIdEstab = mIdEstab
        newVC.mNameStab = mNameStab
        newVC.isFromPromos = isFromPromos
        newVC.is_month_payment = is_month_payment
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func showAlertDialogSuccess() {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = mMessageToUser
        newXIB.mTitle = mNameStab
        newXIB.mTitleButton = "Listo"
        newXIB.mId = 0
        newXIB.isFromAdd = true
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func onSavePromosNews(_ sender: Any) {
        if validateFields(title: self.mTitleAdvs, message: self.mDescriptionAdvs, dateStart: self.mDateStringInit, dateEnd: self.mDateStringFinal).isEmpty{
            LoadingOverlay.shared.showOverlay(view: self.view)
            
            pmws.savePromoNews(name: self.mTitleAdvs,
                               description: mDescriptionAdvs,
                               start_date: mDateStringInit,
                               end_date: mDateStringFinal,
                               image_id: mIdImage,
                               establishment_id: mIdEstab,
                               isUpDate: isFromEdit,
                               idPromotional: String(mPromotionalId),
                               isNews: isNews, imageFilePath: savedPromotionalCode)
            
        }else{
            let alert = UIAlertController(title: "PickPal Notipromos", message: validateFields(title: self.mTitleAdvs, message: self.mDescriptionAdvs,dateStart: self.mDateStringInit, dateEnd: self.mDateStringFinal), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {action in
                print("Hola mundo")
            }))
            self.present(alert, animated: true)
        }
    }
    
    //Funcion de AddEditBodyDelegate
    func OnTitleTextChange(title: String) {
        mTitleAdvs = title
        print("tfNameAadvs: \(mTitleAdvs)")
    }
    
    func OnDescriptionTextChange(Description: String) {
        mDescriptionAdvs = Description
        print("tfDescripAdvs: \(mDescriptionAdvs)")
    }
    
    func OnDateTextChange(Date: String, isFirstDate: Bool) {
        if isFirstDate {
            mDateStringInit = Date
            
        }else{
            mDateStringFinal =  Date
        }
    }
    
    func OnSelectImage(id: Int) {
        for image in mListImages {
            if image.id == id {
                image.isSelected = true
                mIdImage = id
                urlImgSelect = image.image
                Nuke.loadImage(with: URL(string: image.image)!, into: imageSelect)
                print("Id de imagen " + String(mIdImage))
            }else{
                image.isSelected = false
            }
        }
        colectionImage.reloadData()
    }
    
    func OnSelectImage(idImage: Int) {
        
    }
    
    func OnEditing() {
        self.view.endEditing(true)
    }
    
    func onClearDates(isFirstDate: Bool){
        if isFirstDate {
            mDateStringInit = ""
        }else{
            mDateStringFinal = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength: Int = 100
        
        if textField == tfNamePromo{
            maxLength = 45
        }else if textField == tfDescripPromo{
            maxLength = 100
        }
        
        let currentString: NSString = textField.text! as NSString
        
        let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func checkMaxlength(texField: UITextField, maxLenth: Int){
        if texField.text!.count > maxLenth{
            texField.deleteBackward()
        }
    }
    
    @IBAction func OnChangeTitle(_ sender: Any) {
        print(tfNamePromo.text!.count)
        OnTitleTextChange(title: tfNamePromo.text!)
    }
    @IBAction func OnChangeDescription(_ sender: Any) {
        checkMaxlength(texField: tfDescripPromo, maxLenth: 100)
        OnDescriptionTextChange(Description: tfDescripPromo.text!)
        print(tfDescripPromo.text?.count ?? 0)
    }
    /* @IBAction func OnChangeTitle(_ sender: Any) {
        print(tfNamePromo.text!.count)
        OnTitleTextChange(title: tfNamePromo.text!)
    }
    
    @IBAction func OnChangeDescription(_ sender: Any) {
        checkMaxlength(texField: tfDescripPromo, maxLenth: 100)
        OnDescriptionTextChange(Description: tfDescripPromo.text!)
        print(tfDescripPromo.text?.count ?? 0)
    }*/
    
    func showDatePicker(){
         displayCalendarToUser()
        
     }
     
     func displayCalendarToUser(){
         let date = Date()
         
         let calendar = Calendar.current
         let cal = Calendar(identifier: .gregorian)
         
         var components = DateComponents()
         
         let anio = calendar.component(.year, from: date)
         let mes = calendar.component(.month, from: date)
         let dia = getDayOfMoth(currentYear: calendar.component(.year, from: date), currentMonth: calendar.component(.month, from: date), currentDay: calendar.component(.day, from: date))
         
         components.calendar = cal
         components.day = dia
         components.month = mes
         components.year = anio
         
         let maxDate = calendar.date(from: components)
         print(maxDate! as Date)
         
         datePicker.datePickerMode = .date
         datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 100)
         datePicker.minimumDate = Date()
         datePicker.maximumDate = maxDate! as Date
         
         tfDateInit.inputAccessoryView = createToolbarToDate(isDateStart: true)
         tfDateFinal.inputAccessoryView = createToolbarToDate(isDateStart: false)
         tfDateInit.inputView = datePicker
         tfDateFinal.inputView = datePicker
     }
     

    
    func createToolbarToDate(isDateStart: Bool)->UIToolbar{
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton:UIBarButtonItem
        let cancelButton:UIBarButtonItem
        
        let leftTitle = UIBarButtonItem(title: isFirst ?  "Ingresar Fecha":"Fecha Final", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        leftTitle.tintColor = UIColor.black
        
        if isDateStart {
            self.isDateStart = false
            doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePickerDateStart));
            cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelDatePickerStart));
        }else{
            self.isDateStart = true
            doneButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(donedatePickerDateFinal));
            cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelDatePickerFinal));
        }
        
        toolbar.setItems([cancelButton,spaceButton, leftTitle,spaceButton, doneButton], animated: true)
        isFirst = !isFirst
        return toolbar
    }
    
    @objc func donedatePickerDateStart(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        if  self.isDateStart {
            self.isDateStart = false
            mDateStringInit = formatter.string(from: datePicker.date)
            tfDateInit.text = "  \(mDateStringInit)"
            OnDateTextChange(Date: formatter.string(from: datePicker.date), isFirstDate: true)
            OnEditing()
        }else{
            
            self.isDateStart = true
            mDateStringFinal = formatter.string(from: datePicker.date)
            tfDateFinal.text = "\(mDateStringFinal)"
            
            OnDateTextChange(Date: formatter.string(from: datePicker.date), isFirstDate: false)
            OnEditing()
        }
        collectionPreview.reloadData()
        
    }
    
    @objc func donedatePickerDateFinal(){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        mDateStringFinal = formatter.string(from: datePicker.date)
        tfDateFinal.text = "  \(mDateStringFinal)"
        
        OnDateTextChange(Date: formatter.string(from: datePicker.date), isFirstDate: false)
        OnEditing()
        collectionPreview.reloadData()
    }
    
    @objc func cancelDatePickerStart(){
        OnEditing()
        if isFromEdit {
            tfDateInit.text = " \(mDateStringInit)"
            OnDateTextChange(Date: mDateStringInit, isFirstDate: true)
        }else{
            tfDateInit.text = ""
            onClearDates(isFirstDate: true)
        }
    }
    
    @objc func cancelDatePickerFinal(){
        OnEditing()
        if isFromEdit {
            tfDateFinal.text = mDateStringFinal
            OnDateTextChange(Date: mDateStringFinal, isFirstDate: false)
        }else{
            tfDateFinal.text = ""
            onClearDates(isFirstDate: false)
        }
    }
    
    func getDayOfMoth(currentYear: Int, currentMonth: Int, currentDay: Int) -> Int {
        let dateComponents = DateComponents(year: currentYear, month: currentMonth)
        let calendar = Calendar.current
        let date = calendar.date(from: dateComponents)!
        
        let range = calendar.range(of: .day, in: .month, for: date)!
        //let daysInMonth = range.count
        print("Dias del mes: " + String(range.count))
        return range.count
    }
    
    @IBAction func btnOpenImagenLits(_ sender: Any) {
        
        viewConteinerListImagen.isHidden = false
        menuImagen = true
        viewSaveButtom.isHidden = true
        conteinerImageSelect.isHidden = true
        heightGeneral.constant = self.view.frame.height > 700 ?   self.view.frame.height - 180:500
       
    }
    
    @IBAction func btnSelectImgPickPal(_ sender: Any) {
        collectionPreview.reloadData()
        isGalery = false
        menuImagen = false
        viewConteinerListImagen.isHidden = true
        conteinerImageSelect.isHidden = false
        viewSaveButtom.isHidden = false
        savedPromotionalCode = ""
        
       
        
    }
    
    
}

/*extension AddEditPromoViewController: UITableViewDelegate, UITableViewDataSource{
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 return structures.count
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 switch(structures[indexPath.row]){
 case "header":
 let cell = tableView.dequeueReusableCell(withIdentifier: "AddEditHeader", for: indexPath) as! AddEditHeaderTableViewCell
 cell.selectionStyle = .none
 cell.lblNameEstablishment.text = mNameStab
 if isNews {
 if isFromEdit {
 cell.lblNameTitleScreen.text = "Editar Noticia Registrada"
 }else{
 cell.lblNameTitleScreen.text = "Agregar Noticia"
 }
 }else{
 if isFromEdit {
 cell.lblNameTitleScreen.text = "Editar Promoción Registrada"
 }else{
 cell.lblNameTitleScreen.text = "Agregar Promoción"
 }
 }
 
 return cell
 
 default:
 let cell = tableView.dequeueReusableCell(withIdentifier: "AddEditBody", for: indexPath) as! AddEditBodyTableViewCell
 cell.selectionStyle = .none
 cell.mListImages = self.mListImages
 cell.delegate = self
 cell.isFromEdit = self.isFromEdit
 cell.mAdminPromotionals = self.mAdminPromotionals
 if isNews {
 cell.tfDateAdvs.isHidden = true
 cell.tfDateFinal.isHidden = true
 cell.clPhotosAdvs.isHidden = true
 cell.tfNameAadvs.placeholder = "Nombre de Noticia"
 cell.tfDescripAdvs.placeholder = "Descripción de Noticia"
 
 if isFromEdit {
 cell.tfNameAadvs.text = mAdminPromotionals.name
 cell.tfDescripAdvs.text = mAdminPromotionals.descriptionAdvs
 }
 
 }else{
 if isFromEdit {
 cell.tfDateAdvs.text = mAdminPromotionals.start_date
 cell.tfDateFinal.text = mAdminPromotionals.end_date
 cell.tfNameAadvs.text = mAdminPromotionals.name
 cell.tfDescripAdvs.text = mAdminPromotionals.descriptionAdvs
 cell.mDateStringInit = mAdminPromotionals.start_date
 cell.mDateStringFinal = mAdminPromotionals.end_date
 }
 
 cell.clPhotosAdvs.reloadData()
 }
 return cell
 
 }
 }
 
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 switch(structures[indexPath.row]){
 case "header":
 return 80
 default:
 return 333
 }
 
 }
 
 }*/

extension AddEditPromoViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView (_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionPreview{
            
            return  1
            
        }else{
            return mListImages.count
        }
        
    }
    
    func collectionView (_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == collectionPreview{
            
            heightGeneral.constant = 770//self.view.frame.height > 700 ?   self.view.frame.height - 180:800
            
            var textDatepromotion = "Promoción valida del --/--- al --/---"
            let formatDate = DateFormatter()
            formatDate.dateFormat = "dd/mm/yyyy"
            let stringToDateInit = formatDate.date(from: mDateStringInit)
            let formatStringInit = DateFormatter()
            formatStringInit.dateFormat = "dd "
            var dateToStringInit = ""
            if  stringToDateInit != nil{
                dateToStringInit = formatStringInit.string(from: stringToDateInit!)
                textDatepromotion = "Promoción valida del \(dateToStringInit) al --/---"
            }
            let formatDate2 = DateFormatter()
            formatDate2.dateFormat = "dd/mm/yyyy"
            let stringToDateFinal = formatDate2.date(from: mDateStringFinal)
            let formatStringFinal = DateFormatter()
            formatStringFinal.dateFormat = "dd MMMM"
            var dateToStringFinal = ""
            if  stringToDateFinal != nil{
                dateToStringFinal = formatStringFinal.string(from: stringToDateFinal!)
                
                textDatepromotion = "Promoción valida del \(dateToStringInit) al \(dateToStringFinal)"
            }
            
            if isGalery{
            
         
                let itemImagen = collectionView.dequeueReusableCell(withReuseIdentifier: "PromosItemImagen", for: indexPath) as! PromosImagenItemCell
              
                itemImagen.center.x = self.collectionPreview.center.x
                itemImagen.cellContent.clipsToBounds = true
                itemImagen.cellContent.layer.masksToBounds = true
                itemImagen.cellContent.layer.cornerRadius = 15
                itemImagen.cellContent.layer.shadowColor = UIColor.black.cgColor
                itemImagen.cellContent.layer.shadowOpacity = 0.35
                itemImagen.cellContent.layer.shadowOffset = .zero
                itemImagen.cellContent.layer.shadowRadius = 5
                itemImagen.position = indexPath.row
                itemImagen.lblTitlePromo.text = mNameStab
                itemImagen.lblDescriptionPromo.text = descriptionEstablish
                itemImagen.lblDatePromotion.text = textDatepromotion
                itemImagen.isPrreview = true
                
                if isFromEdit{
                    
                    Nuke.loadImage(with: URL(string: urlImgSelect)!, into: itemImagen.imgPromo)
                    
                }else{
                    
                    itemImagen.imgPromo.image =  imagenGalery
                }
                
//                Nuke.loadImage(with: URL(string: urlImgSelect)!, into: itemImagen.imgPromo)

                return itemImagen
                
                
            }else{
                
                
                
                
                let item = collectionView.dequeueReusableCell(withReuseIdentifier: "PromosItemAux", for: indexPath) as! PromosItemAuxCollectionViewCell
    
                item.center.x = self.collectionPreview.center.x
                item.cellContentHorizontalPromo.clipsToBounds = true
                item.cellContentHorizontalPromo.layer.masksToBounds = true
                item.cellContentHorizontalPromo.layer.cornerRadius = 15
                item.cellContentHorizontalPromo.layer.shadowColor = UIColor.black.cgColor
                item.cellContentHorizontalPromo.layer.shadowOpacity = 0.35
                item.cellContentHorizontalPromo.layer.shadowOffset = .zero
                item.cellContentHorizontalPromo.layer.shadowRadius = 5
//                item.delegate = self
                item.isPreview = true
                item.position = indexPath.row
                item.headerTitle.text = mNameStab
                item.headerDescription.text = descriptionEstablish
                item.bodyTitle.text = mTitleAdvs
                item.bodyDescription.text = mDescriptionAdvs
                item.disclaimer.text = textDatepromotion
                item.headerView.clipsToBounds = true
                item.headerView.layer.cornerRadius = 15
                item.headerView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
//                item.imageView.image = imagenGalery
                Nuke.loadImage(with: URL(string: urlImgSelect)!, into: item.imageView)
                
                return item
                
            }
            
        }else{
        
        
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "proCell", for: indexPath) as! PhotosCollectionViewCell
                
                if isGalery{
                    
                    if mListImages[indexPath.row].id == idImagegalery {
                        
                        cell.imgImagePromo.image = imagenGalery
                    }else{
                        
                        if let image = mListImages[indexPath.row].image, !image.isEmpty {
                            Nuke.loadImage(with: URL(string: image)!, into: cell.imgImagePromo)
                        }
                    }
                    
                }else{
                    
                    if let image = mListImages[indexPath.row].image, !image.isEmpty {
                        Nuke.loadImage(with: URL(string: image)!, into: cell.imgImagePromo)
                    }
                    
                }
                
            
                cell.delegate = self
                cell.idPromo = mListImages[indexPath.row].id
                cell.isSelect = mListImages[indexPath.row].isSelected
                
                if mListImages[indexPath.row].isSelected {
                    cell.imgCheckSelectImage.isHidden = false
                    cell.viewShadowImage.isHidden = false
                    
                    if mListImages[indexPath.row].id == idImagegalery{
                        
                        self.savedPromotionalCode = self.savedPromotionalCodeAux
                        
                    }else{
                        
                        
                        self.savedPromotionalCode = ""
                        
                    }
                    
                }else{
                    cell.imgCheckSelectImage.isHidden = true
                    cell.viewShadowImage.isHidden = true
                }
                
                return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        
        if collectionView == collectionPreview{
              
            return CGSize(width: 20.0, height: 200.0)
            
        }else{
            
            return CGSize(width: 130, height: 80.0)
            
        }
        
    }
}

/*extension UITextField {
    func setLeftView(image: UIImage) {
        let iconView = UIImageView(frame: CGRect(x: 10, y: 10, width: 25, height: 25))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 45))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
    }
}*/

//MARK: Get image from camera roll & Photo library
extension AddEditPromoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cámara", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openCamera()
        }))
        actionSheet.addAction(UIAlertAction(title: "Carrete", style: .default, handler: {(alert:UIAlertAction!) -> Void in
            self.openGallery()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let picker = UIImagePickerController()
        picker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }else {
            self.openGallery()
        }
    }
    
    func openGallery() {
        if isGalery{
            mListImages.remove(at: idImagegalery)
            colectionImage.reloadData()
        }
        var pickerGallery = UIImagePickerController()
        pickerGallery.delegate = self
        pickerGallery.sourceType = .photoLibrary
        pickerGallery.allowsEditing = true
        self.present(pickerGallery, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
         
        let uuid = UUID().uuidString
        let myImageName = "imagen" + uuid + ".png"
        let imagePath = fileInDocumentsDirectory(myImageName)
        _ = saveImage((info[UIImagePickerControllerOriginalImage] as? UIImage)!, path: imagePath)
        imagenGalery = (info[UIImagePickerControllerOriginalImage] as? UIImage)!
            conteinerImageSelect.isHidden = false
            savedPromotionalCode = "\(path)/" + myImageName
        savedPromotionalCodeAux = "\(path)/" + myImageName
//        var imagGalery = Images()
//        idImagegalery = 0
//        imagGalery.id = idImagegalery
//        imagGalery.image = "\(path)/" + myImageName
//        imagGalery.isSelected = true
//        mListImages.append(imagGalery)
//        colectionImage.reloadData()
//        isGalery = true
        
        imageSelect.image = imagenGalery
        collectionPreview.reloadData()
        isGalery = true
        print("\(path)/" + myImageName)
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImagePNGRepresentation(image)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        var newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
        
    }
     
}
