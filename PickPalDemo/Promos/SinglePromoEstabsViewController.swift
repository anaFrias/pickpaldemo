//
//  SinglePromoEstabsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/22/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class SinglePromoEstabsViewController: UIViewController, PromosDelegate, informationDelegate, MapsDelegate {
    
    @IBOutlet weak var SingleTableView: UITableView!
    
    var id: Int!
    var ws = PromosWS()
    var municipioStr = String()
    var categoriaStr = String()
    var lugarString = String()
    var establString = String()
    var structures = [String]()
    var singleInfo = SingleEstabAdvs()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        SingleTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        LoadingOverlay.shared.showOverlay(view: self.view)
        ws.viewSingleAdvsEstablishment(establishmentID: self.id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
    }
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs)  {
        structures.removeAll()
        structures = ["root", "sliders", "information", "banner", "promotionals"]
        LoadingOverlay.shared.hideOverlayView()
        singleInfo = info
        SingleTableView.reloadData()
    }
    
    func didFailGetSingleEstablishment(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goHome(_ sender: Any) {
        
    }
    
    @IBAction func goPedidos(_ sender: Any) {
        
    }
    
    @IBAction func goSettings(_ sender: Any) {
        
    }
    
    @IBAction func goToBack(_ sender: Any) {
    }
    
    @IBAction func goToMenu(_ sender: Any) {
    }
    
    
    @IBAction func goToPedidos(_ sender: Any) {
    }
    
    @IBAction func goToSettings(_ sender: Any) {
    }
    
    func pressMap(isShow: Bool) {
        if isShow{
            print("mostrar mapa")
            print("ocultar info")
            structures = ["root", "sliders", "information", "maps"]
            SingleTableView.reloadData()
        }else{
            print("ocultar todo")
            structures = ["root", "sliders", "information", "banner", "promotionals"]
            SingleTableView.reloadData()
        }
    }
    
    func pressInfo(isShow: Bool) {
        if isShow{
            print("mostrar info")
            print("ocultar mapa")
            structures = ["root", "sliders", "information", "infoSocial"]
            SingleTableView.reloadData()
        }else{
            print("ocultar todo")
            structures = ["root", "sliders", "information", "banner", "promotionals"]
            SingleTableView.reloadData()
        }
    }
    
    func createHorario(horario: [ScheduleItem]) -> String{
        var i = 0;
        var j = 0;
        var horarios = [itemHorario]()
        for item in horario{
            if i == 0{
                let x = itemHorario()
                x.open = item.open_hour
                x.close = item.closing_hour
                x.addname(name: item.day)
                horarios.append(x)
                j += 1
            }else{
                if (horarios[j-1].open == item.open_hour && horarios[j-1].close == item.closing_hour){
                    horarios[j-1].addname(name: item.day)
                }else{
                    let x = itemHorario()
                    x.open = item.open_hour
                    x.close = item.closing_hour
                    x.addname(name: item.day)
                    horarios.append(x)
                    j += 1
                }
            }
            i += 1
        }
        
        var txtHorario = ""
        
        for z in 0..<horarios.count{
            if(z == horarios.count - 1){
                if(horarios[z].names.count > 1){
                    txtHorario += horarios[z].names[0] + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    
                }else{
                    if(horarios[z].names[0] == "Todos"){
                        txtHorario += "Lunes - Domingo: " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }else{
                        txtHorario += horarios[z].names[0] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }
                    
                }
            }else{
                if(horarios[z].names.count > 1){
                    txtHorario +=  horarios[z].names[0]  + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }else{
                    txtHorario += horarios[z].names[0]  + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }
            }
        }
        return txtHorario
    }
    
    func openMaps() {
        
    }
}

extension SinglePromoEstabsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if structures[indexPath.row] == "root" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "root", for: indexPath) as! RootTableViewCell
            cell.selectionStyle = .none
            if singleInfo != nil {
                cell.lblMunicipio.text = singleInfo.address.city
                cell.lblCategoria.text = singleInfo.advsCategorry
                cell.lblComida.text = singleInfo.subadvsCategory
            }
            return cell
        } else if(structures[indexPath.row] == "sliders"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlidersTableViewCell", for: indexPath) as! SlidersTableViewCell
            cell.selectionStyle = .none
            if singleInfo != nil {
                cell.images = singleInfo.gallery
            }
            return cell
        } else if(structures[indexPath.row] == "information"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "InformationTableViewCell", for: indexPath) as! InformationTableViewCell
            cell.selectionStyle = .none
            if singleInfo != nil {
                cell.lblNameEstab.text = singleInfo.name
                cell.delegate = self
            }
            return cell
        }else if(structures[indexPath.row] == "banner"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BanersTableViewCell", for: indexPath) as! BanersTableViewCell
            cell.selectionStyle = .none
            return cell
        }else if(structures[indexPath.row] == "promotionals") {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionalsTableViewCell", for: indexPath) as! PromotionalsTableViewCell
            cell.selectionStyle = .none
            cell.promotionals = singleInfo.promotionals
            cell.collectionsPromotionals.reloadData()
            return cell
        }else if(structures[indexPath.row] == "infoSocial"){
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialInformationSingleAdvs", for: indexPath) as! SocialInformationSingleAdvsTableViewCell
            cell.selectionStyle = .none
            if singleInfo != nil {
                cell.singleInfo = singleInfo.socialnformation
                cell.lblDescrip.text = singleInfo.descripcion
                cell.lblCat.text = singleInfo.advsCategorry
                cell.lblHorario.text = createHorario(horario: singleInfo.operation_schedule)
                
                
                if singleInfo.socialnformation.siteURL == nil {
                    cell.btnWeb.isHidden = true
                }else{
                    cell.btnWeb.isHidden = false
                }
                
                if singleInfo.socialnformation.facebookURL == nil{
                    cell.btnFb.isHidden = true
                }else{
                    cell.btnFb.isHidden = false
                }
                
                if singleInfo.socialnformation.instagramURL == nil{
                    cell.btnInsta.isHidden = true
                }else{
                    cell.btnInsta.isHidden = false
                }
                
                if singleInfo.socialnformation.twitterURL == nil{
                    cell.btnTwitter.isHidden = true
                }else{
                    cell.btnTwitter.isHidden = false
                }
                
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Map", for: indexPath) as! MapTableViewCell
            cell.selectionStyle = .none
            
            if singleInfo != nil {
                let latit = CLLocationDegrees(singleInfo.latitude)
                let longi = CLLocationDegrees(singleInfo.longitude)
                cell.latitud = latit
                cell.longitud = longi
                
                cell.establString = singleInfo.name
                
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if structures[indexPath.row] == "root" {
            return 33
        } else if(structures[indexPath.row] == "sliders"){
            return 200
        } else if(structures[indexPath.row] == "information"){
            return 43.5
        }else if(structures[indexPath.row] == "banner"){
            return 44
        }else if(structures[indexPath.row] == "promotionals"){
            return 400
        }else if(structures[indexPath.row] == "infoSocial"){
            return 280
        }else if(structures[indexPath.row] == "maps"){
            return 390
        }else{
            return 0
        }
    }
    
    
    func didFailGetTermnsConditions(error: String, subtitle: String){
        
        
    }
    
    
    

}
