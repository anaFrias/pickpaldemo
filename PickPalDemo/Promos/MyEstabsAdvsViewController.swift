//
//  MyEstabsAdvsViewController.swift
//  PickPalTesting
//
//  Created by Dev on 12/24/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class MyEstabsAdvsViewController: UIViewController, PromosDelegate, AdvsDelegate, userDelegate, AlertAdsDelegate {
    
    @IBOutlet weak var tableAdvs: UITableView!
    
    var pmws = PromosWS()
    var estabsAdvs = [EstablishmentsAdvs]()
    let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
    var isFromPromos:Bool = false
    var isUpdatedVersion = Bool()
    var userws = UserWS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pmws.delegate = self
        userws.delegate = self
        
        userws.isUpdateAvailable()
        
        LoadingOverlay.shared.showOverlay(view: self.view)
        pmws.getEstablishmentAdvs(client_id: client_id)
        tableAdvs.separatorStyle = UITableViewCellSeparatorStyle.none
        
    }
    
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
        }
    }
    
    func didSuccessGetEstablishmentsAdvs(info: [EstablishmentsAdvs]) {
        LoadingOverlay.shared.hideOverlayView()
        estabsAdvs = info
        tableAdvs.reloadData()
    }
    
    func didFailGetEstablishmentsAdvs(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = error + "\n" + subtitle
        newXIB.mTitle = "PickPal Notipromos"
        newXIB.mToHome = true
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func GoToHome(_ sender: Any) {
        if !isFromPromos {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            UserDefaults.standard.set("promos", forKey: "place")
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func goToPedidosActuales(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func GoToBack(_ sender: Any) {
        exitFromAdminPromos()
    }
    
    func exitFromAdminPromos(){
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = isFromPromos
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func goToMyPomos(id: Int, nameEstab: String, is_month_payment: Bool, descriptionEstablishment: String) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MyPromotionals") as! MyPromotionalsViewController
        newVC.mIdEstab = id
        newVC.mNameStab = nameEstab
        newVC.isFromPromos = isFromPromos
        newVC.is_month_payment = is_month_payment
        newVC.descriptionEstablish = descriptionEstablishment
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func deletePromoById(id: Int) {
        exitFromAdminPromos()
    }
    
    func closeDialogAndExit(toHome: Bool) {
        exitFromAdminPromos()
    }
}

extension MyEstabsAdvsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return estabsAdvs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cellHeader = tableView.dequeueReusableCell(withIdentifier: "HeaderNegocio", for: indexPath) as! HeaderNegocioTableViewCell
            return cellHeader
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NegociosAdvs", for: indexPath) as! NegociosTableViewCell
            cell.lblNameEstab.setTitle(estabsAdvs[indexPath.row].establishmentName, for: UIControlState.normal)
            cell.viewBackground.layer.cornerRadius = 10 //le damos un corner a la vista verde
            
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            
            cell.viewCell.layer.cornerRadius = 10
            cell.viewCell.layer.borderColor = UIColor.black.cgColor  // set cell border color here
            cell.viewCell.layer.borderWidth = 1 // set border width here
            cell.id = estabsAdvs[indexPath.row].id
            cell.nameEstab = estabsAdvs[indexPath.row].establishmentName
            cell.is_month_payment = estabsAdvs[indexPath.row].is_month_payment
            cell.descriptionEstablishment = estabsAdvs[indexPath.row].descriptionEstablishment
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height:CGFloat = CGFloat()
        if indexPath.row == 0 {
            height = 80
        }else {
            height = 70
        }
        
        return height
    }
}
