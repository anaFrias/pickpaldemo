//
//  MenuViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 11/15/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Alamofire
import Firebase
import OneSignal

class MenuViewController: UIViewController, submenuProtocolDelegate, UNUserNotificationCenterDelegate {
    
    
    @IBOutlet weak var pickPalButton: UIButton!
    @IBOutlet weak var promoButton: UIButton!
    @IBOutlet weak var lblCopyRight: UILabel!
    
    @IBOutlet weak var imgPointsPickPal: UIImageView!
    @IBOutlet weak var imgPromos: UIImageView!
    
    @IBOutlet weak var imgGuidePickpal: UIImageView!
    
    @IBOutlet weak var imgCustomerService: UIImageView!
    @IBOutlet weak var imgWallet: UIImageView!
    @IBOutlet weak var imgFoodAndDrinks: UIImageView!
    
    var pointsSw = PointsWS()
    var guideWS = GuidePickPalWS()
    var ws = PromosWS()
    var dialogInfo = AlertInfoPopUpviewController()
    var userWS = UserWS()
    var statusProfile = StatusProfile()
    var orderws = OrdersWS()
    var walletSW = WalletWS()
    var sinWallet = false
    var customerService = CustomerServiceWS()
    var amountOrders = 0
    var openPuntosPickPal = true
    
    @IBOutlet weak var btnInfoCash: UIImageView!
    @IBOutlet weak var btnInfoOrder: UIImageView!
    @IBOutlet weak var btnInfoPromos: UIImageView!
    @IBOutlet weak var btnInfoGuia: UIImageView!
    @IBOutlet weak var btnInfoPuntosMoviles: UIImageView!
    @IBOutlet weak var btnInfoCustomerService: UIImageView!
    
    @IBOutlet weak var btnInfoWallet: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        ws.delegate = self
        guideWS.delegate = self
        pointsSw.delegate = self
        userWS.delegate = self
        orderws.delegate = self
        walletSW.delegate = self
        customerService.delegate = self
        
        OneSignal.setSubscription(true)
        
        let foodAndDrinks = UITapGestureRecognizer(target: self, action: #selector(goToFoodAndDrinks))
        let pickpalPromos = UITapGestureRecognizer(target: self, action: #selector(goToPromos))
        let pickpalGuide = UITapGestureRecognizer(target: self, action: #selector(goToGuidePickPal))
        let pointsPickPal = UITapGestureRecognizer(target: self, action: #selector(goToPointsPickPal))
        
        let walletPickPal  = UITapGestureRecognizer(target: self, action: #selector(goToWalletPickPal))
        
        let CustomerService  = UITapGestureRecognizer(target: self, action: #selector(goToCustomerService))
        
        let infoOrder = UITapGestureRecognizer(target: self, action: #selector(messageInfoOrder))
        let infoPromos = UITapGestureRecognizer(target: self, action: #selector(messageInfoPromos))
        
        let infoGuia = UITapGestureRecognizer(target: self, action: #selector(messageInfoGuia))
        
        let infoPuntosMoviles = UITapGestureRecognizer(target: self, action: #selector(messageInfoPuntosMoviles))
        let infoWallet = UITapGestureRecognizer(target: self, action: #selector(messageInfoWallet))
        
        let infoCustomerService = UITapGestureRecognizer(target: self, action: #selector(messageInfoCustomerService))
        
        
        btnInfoOrder.isUserInteractionEnabled = true
        btnInfoPromos.isUserInteractionEnabled = true
        btnInfoGuia.isUserInteractionEnabled = true
        btnInfoPuntosMoviles.isUserInteractionEnabled = true
        btnInfoWallet.isUserInteractionEnabled = true
        btnInfoCustomerService.isUserInteractionEnabled = true
        imgFoodAndDrinks.addGestureRecognizer(foodAndDrinks)
        imgPromos.addGestureRecognizer(pickpalPromos)
        imgGuidePickpal.addGestureRecognizer(pickpalGuide)
        imgPointsPickPal.addGestureRecognizer(pointsPickPal)
        imgWallet.addGestureRecognizer(walletPickPal)
        imgCustomerService.addGestureRecognizer(CustomerService)
        
        btnInfoOrder.addGestureRecognizer(infoOrder)
        btnInfoPromos.addGestureRecognizer(infoPromos)
        btnInfoGuia.addGestureRecognizer(infoGuia)
        btnInfoPuntosMoviles.addGestureRecognizer(infoPuntosMoviles)
        btnInfoWallet.addGestureRecognizer(infoWallet)
        btnInfoCustomerService.addGestureRecognizer(infoCustomerService)
        //CopyRight.
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        let formattedDate = format.string(from: date)
        lblCopyRight.text = "© Copyright: PickPal " + formattedDate
        userWS.getStatusProfile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        userWS.viewProfile(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
        DatabaseFunctions.shared.createDB()
        orderws.get_last_order(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MenuViewController.openHomePoints(_:)), name: NSNotification.Name(rawValue: "openHomePoints"), object: nil)
        
    }
    
    @objc func goToFoodAndDrinks() {
        
        UserDefaults.standard.set("home", forKey: "place")
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @objc func goToPromos() {
        self.view.isUserInteractionEnabled = false
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.ws.getTotalPickPal()
    }
    
    @objc func goToGuidePickPal() {
        self.view.isUserInteractionEnabled = false
        LoadingOverlay.shared.showOverlay(view: self.view)
        guideWS.CountEstablishmentGuide()
    }
    
    @objc func goToPointsPickPal(){
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                self.view.isUserInteractionEnabled = false
                LoadingOverlay.shared.showOverlay(view: self.view)
                pointsSw.CountEstablishmentPoints()
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            self.view.isUserInteractionEnabled = false
            LoadingOverlay.shared.showOverlay(view: self.view)
            pointsSw.CountEstablishmentPoints()
        }
       
    }
    
    
    @objc func goToWalletPickPal() {

        
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                if let idUser = UserDefaults.standard.object(forKey: "client_id") as? Int{
                walletSW.getMyWalletList(client_id: idUser)
                }
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            if let idUser = UserDefaults.standard.object(forKey: "client_id") as? Int{
            walletSW.getMyWalletList(client_id: idUser)
            }
        }
        
        
        
       
       
    }
    
    @objc func goToCustomerService() {
        
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                customerService.getMyListCustomerService()
                LoadingOverlay.shared.showOverlay(view: self.view)
                self.view.isUserInteractionEnabled = false
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            customerService.getMyListCustomerService()
            LoadingOverlay.shared.showOverlay(view: self.view)
            self.view.isUserInteractionEnabled = false
        }
        
        
    }
    
    
    func goToBuy() {
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "BuyWallet") as! BuyWalletViewController

        newVC.from = "SubMenu"
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func gotToMyWallets(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func gotToPay(){
        
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeWallet") as! HomeWalletViewController
        newVC.goToPay = true
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func goToQuestions() {
        walletSW.getListFaqs()
    }
    
    
    func didGetListFaqs(lisFaqs: [ItemFaqs]) {
        
        
        let newXIB = TermsAndConditionsView(nibName: "TermsAndConditionsView", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.textTermsAndConditions = ""
        newXIB.hiddenTerms  = true
        newXIB.hiddenList  = false
        newXIB.texTitel = "PREGUNTAS FRECUENTES"
        newXIB.listfaqsArray = lisFaqs
        present(newXIB, animated: true, completion: nil)
        
        
        
    }
    
    
    @objc func messageInfoOrder() {
        
        showDialogInfo(title: "Pickpal Order", message: "Pide y paga cualquier producto más rápido y sin esperar. Solo elije el tipo de pedido de tu preferencia:\n \n- Recoger en sucursal \n- Servicio a mesa \n- Recoger en barra \n- Servicio a Domicilio\n \n¿Tienes prisa? Programa tu pedido")
        
    }
    
    
    
    @objc func messageInfoPromos() {
        
        showDialogInfo(title: "PickPal Notipromos", message: "¡Todas las promociones en 1 solo lugar!\n \nSubscríbete a los comercios de tu interés para recibirlas. Incluso puedes compartirlas con tus contactos.")
        
    }
    
    @objc func messageInfoGuia() {
        
        showDialogInfo(title: "Pickpal Carta Digital", message: "Conoce el catálogo de productos de los distintos giros de comercios, asi como su información básica.")
        
    }
    
    @objc func messageInfoPuntosMoviles() {
        
        showDialogInfo(title: "Puntos Móviles", message: "Gana puntos de lealtad por cada compra, redímelos en el comercio que quieras y ahorra dinero.")
        
    }
    
    
    @objc func messageInfoWallet() {
        
        showDialogInfo(title: "PickPal Cash", message: "¡Compra saldo de tus restaurantes favoritos y obtén una bonificación adicional!\n \nUsa tu saldo para pagar en los establecimientos afiliados y ahorra dinero.", isWallet : true, textGreen: "¡Por cada peso que compres PickPal te regala un 10% más!")
        
    }
    
    @objc func messageInfoCustomerService() {
        
        showDialogInfo(title: "Llamar Mesero / Pedir Cuenta", message: "\"Llama a tu mesero\" o \"Pide la Cuenta\" desde un solo click cuantas veces quieras. ¡Atiéndete como un rey con PickPal!",isWallet: true,textGreen: "Vincula tu mesa para usar este servicio")
        
    }
    
    func showDialog(title: String, message: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 0
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func didFailGetLastState(title: String, subtitle: String) {
        print("Fail Last state")
    }
    
    func showDialogInfo(title: String, message: String, isWallet : Bool = false, textGreen: String = "") {
        let newXIB = AlertInfoPopUpviewController(nibName: "AlertInfoPopUpviewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mTitle = title
        newXIB.mMessage = message
        newXIB.isWallet = isWallet
        if isWallet{
            newXIB.textGreen = textGreen
        }
       
        self.present(newXIB, animated: true, completion: nil)
    }
}

//Extension para puntos moviles
extension MenuViewController: PointsWSDelegate{
    
    func didSuccessCountEstablishmentPoints(number: Int) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailCountEstablishmentPoints(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showDialog(title: "PickPal Puntos Móviles", message: "Aún no tienes Puntos Móviles. Haz compras en los comercios afiliados para ganar puntos.")
    }
}

//Extension para Guia PickPal
extension MenuViewController: GuideDelegate{
    
    func didSuccessCountEstablishmentGuide(number: Int) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailCountEstablishmentGuide(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showDialog(title: "PickPal Guía", message: "¡Oops! ¡Por el momento no hay un comercio con guía, vuelve más tarde para aprovechar de los mejores comercios!")
    }
    
}

//Extension para promos
extension MenuViewController: PromosDelegate{
    
    func didSuccessGetPickPalCount(totalPickPal: Int) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        UserDefaults.standard.set("promos", forKey: "place")
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailGetPickPalCount(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showDialog(title: "PickPal Notipromos", message: "¡Oops! ¡Por el momento no hay un comercio con promociones, vuelve más tarde para aprovechar los descuentos!")
    }
    
    
    
    
}

//Extension para accion en el dialogo
extension MenuViewController: AlertAdsDelegate{
    func closeDialogAndExit(toHome: Bool) {
        
    }
    
    func deletePromoById(id: Int) {
        
    }
}


//Extension para PickPal Cash
extension MenuViewController: userDelegate{
    

    func didSuccessStatusProfile(avalidableAdvs: StatusProfile){
       
        
        statusProfile = avalidableAdvs
        
        
    }
    
    func didFailStatusProfile(title: String, subtitle: String){
        
        print(title,subtitle)
        
    }
}


//EXTENSION SERVICIO AL CLIENTE


extension MenuViewController:CustomerServiceDelegate {
    
    
    func getCustomerServiceList(listCustomerService: [EstablishtmentCustomerService]) {
                LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        if listCustomerService.count > 0{
            
            let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomeTableLinked") as! HomeTableLinkedViewController
            let navController = UINavigationController(rootViewController: newVC)
            newVC.numeroPedidos = amountOrders
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }else{
            
            let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "LinkTableViewController") as! LinkTableViewController
            let navController = UINavigationController(rootViewController: newVC)
            newVC.numeroPedidos = amountOrders
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
            
        }
        
        
    }
    
    func didFailGetCustomerServiceList(error: String, subtitle: String) {
        print("Error: ", error)
       
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
    }
    
    
    
}




// EXTENCION WALLET

extension MenuViewController: walletDelegate{
    
    
    func getEstablishmentWallet(listWallet: [infoHeaderWallet]){
                
        var misWallets = false
        sinWallet = listWallet.count > 0
        
        // SI TIENE WALLETS VERIFICAMOS QUE NO TENGA SALDO EN NINGUNO
        if listWallet.count != 0{
            var index = 0
            
            for wallet in listWallet{
                
                if wallet.moneyWallet == 0{
                    
                    index += 1
                    
                }
                
            }
            
            if index == listWallet.count {
                sinWallet = false
                misWallets = true
                
            }
        }
        
        
        let newXIB = SubMenuWalletViewController(nibName: "SubMenuWalletViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.sinWallet = sinWallet
        newXIB.misWallets = misWallets
        newXIB.delegate = self
        present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    func didFailGetEstablishmentWallet(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func openAlertSinWallet() {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "¡Compra saldo y obtén 10% más! "
        newXIB.mTitle = "No tienes ningún Wallet con saldo"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mToHome = false
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
        
    }
    
    
}



extension MenuViewController :ordersDelegate {
    
    
    func didFailGetLastOrder(error: String, subtitle: String){
        
       
        
    }
    
    
    
    func didSuccessGetLastOrder(orders: [MultipleOrders]) {
        
        
        amountOrders = orders.count
        //        MARK: Sacar los arreglos de pedidos
        LoadingOverlay.shared.hideOverlayView()
        
        //Oscar: Limpar la BD local para actualizarla segun el WS
        DatabaseFunctions.shared.deleteAllItems()
        
        
        
//        numPedidos.alpha = 1
//        pedidoLabel.text = "\(orders.count)"
        for (_, order) in orders.enumerated() {
        //    print("Orden Home 0: \(String(describing:  order.order_id))  bandera: \(order.to_refund)  fecha  \(order.register_date)")
            let reference = Database.database().reference().child("orders").child("-" + order.establishment_id!.description).child(order.register_date_key!).child("-" + order.order_id.description)
            print(reference)
            reference.observe(.value, with: { (snapshot) -> Void in
                //                Se agregan variables de status y se redirige a pantalla correspondiente
                if !(snapshot.value is NSNull) {
                    let valueOrder = (snapshot.value as! NSDictionary)
                    //                    print(valueOrder)
                    //                    self.showListo.append(false)
                    let statusInt = valueOrder.value(forKey: "status_client") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusInt2 = valueOrder.value(forKey: "status") as! Int//(snapshot.value as! NSDictionary).value(forKey: "status_client") as! Int
                    let statusClient = (Constants.status as NSDictionary).value(forKey: "\(statusInt)") as! String
                    let statusController = (Constants.statusControllers as! NSDictionary).value(forKey: "\(statusInt2)") as! String
//                    if let screen = UserDefaults.standard.object(forKey: "screen") as? String, let storyboard = UserDefaults.standard.object(forKey: "storyboard") as? String{
//                        if screen != "Home" {
//                            if !self.homePush {
//                                if screen == "Carrito" {
//                                    if self.isUpdatedVersion {
//                                        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
//                                        let newVC = storyboard.instantiateViewController(withIdentifier: screen)
//                                        self.navigationController?.pushViewController(newVC, animated: true)
//                                    }else{
//                                        self.updateAlert()
//                                    }
//                                }else{
//                                    if statusClient == "in_process"{
//                                        if self.isUpdatedVersion {
//                                            let storyboard = UIStoryboard(name: storyboard, bundle: nil)
//                                            let newVC = storyboard.instantiateViewController(withIdentifier: screen)
//                                            self.navigationController?.pushViewController(newVC, animated: true)
//                                        }else{
//                                            self.updateAlert()
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
                    //                Se recupera el detalle del pedido
                    var dictionaryInfoString = String()
                    var products = NSArray()
                    if valueOrder.value(forKey: "products") != nil {
                        products = valueOrder.value(forKey: "products") as! NSArray
                    }
                    
                    var dictionaryInfo = [Dictionary<String,Any>]()
                    var dictionaryModifiersTotal = [Dictionary<String,Any>]()
                    var dictionaryComplementsTotal = [Dictionary<String,Any>]()
                    var i = 0
                    var y = 0
                    var totalPrice = 0.00
                    for product in products {
                        //                        print(product)
                        var modifiers = NSArray()
                        var modifiersS = String()
                        var modifiers_price = NSArray()
                        var modifiers_priceS = String()
                        var modifiers_id = NSArray()
                        var modifiers_idS = String()
                        var modifiers_cat = NSArray()
                        var modifiers_catS = String()
                        
                        var complements = NSArray()
                        var complementsS = String()
                        var complements_price = NSArray()
                        var complement_priceS = String()
                        var complements_id = NSArray()
                        var complements_idS = String()
                        var complements_cat = NSArray()
                        var complements_catS = String()
                        
                        
                        if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                            modifiers = mod
                        }else{
                            modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
                        }
                        
                        if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                            modifiers_price = mod_price
                        }else{
                            modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
                        }
                        
                        if let mod_id = (product as! NSDictionary).value(forKey: "modifier_ids") as? NSArray {
                            modifiers_id = mod_id
                        }else{
                            modifiers_idS = (product as! NSDictionary).value(forKey: "modifier_ids") as! String
                        }
                        
                        if let mod_cat = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                            modifiers_cat = mod_cat
                        }else{
                            modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
                        }
                        
                        if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                            complements = comp
                        }else{
                            complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
                        }
                        
                        if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                            complements_price = comp_price
                        }else{
                            complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
                        }
                        
                        if let comp_id = (product as! NSDictionary).value(forKey: "complement_ids") as? NSArray {
                            complements_id = comp_id
                        }else{
                            complements_idS = (product as! NSDictionary).value(forKey: "complement_ids") as! String
                        }
                        
                        if let comp_cat = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                            complements_cat = comp_cat
                        }else{
                            complements_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
                        }
                        
                        if modifiers.count > 0 || complements.count > 0 {
                            var modifP = [Double]()
                            var modifId = [Int]()
                            var modifCat = [String]()
                            var compP = [Double]()
                            var compN = [String]()
                            var compC = [Int]()
                            var compId = [Int]()
                            var compCat = [String]()
                            for j in 0..<modifiers.count {
                                modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                                modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
                                modifCat.append(modifiers_cat.object(at: j) as! String)
                            }
                            var aux = 1
                            var index = -1
                            for z in 0..<complements.count {
                                if !compN.contains(complements[z] as! String) {
                                    compN.append(complements[z] as! String)
                                    compC.append(1)
                                    aux = 1
                                    compP.append(Double(complements_price[z] as! String)!)
                                    compId.append(Int(complements_id[z] as! String) ?? 0)
                                    compCat.append(complements_cat[z] as! String)
                                    index += 1
                                }else{
                                    aux += 1
                                    compC[index] = aux
                                    compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                                    compId[index] = (Int(complements_id[z] as! String) ?? 0)
                                    compCat[index] = (complements_cat[z] as! String)
                                }
                            }
                            
                            let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_id": modifId, "modifier_category":modifCat] as [String : Any]
                            dictionaryModifiersTotal.append(dictionaryModifiers)
                            let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_id": compId, "complement_category":compCat] as [String : Any]
                            dictionaryComplementsTotal.append(dictionaryComplements)
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": true, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                            y += 1
                        }else{
                            let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": false, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_id": complements_idS, "complement_category": complements_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_id": modifiers_idS, "modifier_category": modifiers_catS]] as [String : Any]
                            dictionaryInfo.append(dictionary)
                        }
                        totalPrice += (product as! NSDictionary).value(forKey: "price") as! Double
                        i += 1
                    }
                    var qrCode = String()
                    if let qr = valueOrder.value(forKey: "qr") as? String {
                        qrCode = qr
                    }
                    var last4 = String()
                    if let lastDig = valueOrder.value(forKey: "last4") as? String {
                        last4 = lastDig
                    }else{
                        last4 = ""
                    }
                    var flagReady = Bool()
                    if !DatabaseFunctions.shared.getReadyFlag(order_id: order.order_id) {
                        flagReady = false
                    }else{
                        flagReady = true
                    }
             //        print("Orden 1 Home: \(String(describing:  order.order_id)) \(order.to_refund)  ")
                    let sendNotifications =   DatabaseFunctions.shared.getStatusNotifications(idOrder: order.order_id)
                    DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date:order.register_date, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusClient, qr: qrCode, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusController, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag: flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address, shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String (order.time_delivery), to_refund: order.to_refund, days_elapsed: order.days_elapsed, allow_process_nopayment: order.allow_process_nopayment, est_delivery_hour: order.est_delivery_hour, est_ready_hour: order.est_ready_hour, is_generic: order.is_generic, confirmed:valueOrder.value(forKey: "confirmed") as! Bool, confirm_hour: valueOrder.value(forKey: "confirm_hour") as! String, sendNotifications: sendNotifications, notified: valueOrder.value(forKey: "notified") as? Bool ?? false, pickpal_cash_money:valueOrder.value(forKey: "pickpal_cash_money") as? Double ?? 0, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: order.active_customer_service ?? false, total_card: order.total_card, total_cash: order.total_cash, total_to_pay: order.total_to_pay, total_done_payment: order.total_done_payment ,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                    
                    
                   
                    
                    if let isRefunded = valueOrder.value(forKey: "is_refunded") as? Bool {
                        if isRefunded {
                            DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                        }
                    }
                    if statusClient == "expired" {
                        //                        self.numPedidos.alpha = 0
                        
                       
                        DatabaseFunctions.shared.deleteItem()
                        DatabaseFunctions.shared.deleteDeliveredOrders()
                    }else{
//                        self.numPedidos.alpha = 1
//                        self.defaults.set("Home", forKey: "screen")
//                        self.defaults.set("Home", forKey: "storyboard")
                        if statusClient == "delivered" ||  statusClient == "canceled" || statusClient == "expired" || statusClient == "refund"{
                            //                            self.numPedidos.alpha = 0
                            //                            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                            DatabaseFunctions.shared.deleteItem()
                            DatabaseFunctions.shared.deleteDeliveredOrders()
                            //                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                        }
                        if statusClient == "ready"{
                            
                            if DatabaseFunctions.shared.getReadyFlag(order_id: order.order_id) {
                                let readyView = OrdenListaViewController()
                                readyView.modalTransitionStyle = .crossDissolve
                                readyView.modalPresentationStyle = .overCurrentContext
                                if let in_site = valueOrder.value(forKey: "in_site") as? Bool {
                                    if in_site {
                                        readyView.subtitle1 = "Tu pedido de"
                                        readyView.subtitle2 = "será entregado a tu mesa \(valueOrder.value(forKey: "number_table") as! Int)"
                                    }else{
                                        readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                                        readyView.subtitle2 = "y a disfrutar!"
                                    }
                                }else{
                                    readyView.subtitle1 = "¡Pasa a recoger tu pedido a"
                                    readyView.subtitle2 = "y a disfrutar!"
                                    
                                    
                                    if order.order_type == "SD"{
                                        readyView.subtitle1 = "¡Tu pedido está en camino!"
                                        readyView.subtitle2 = ""
                                        
                                    }
                                    
                                }
                                readyView.establishName = order.establishment_name
                                readyView.orderId = order.order_id
//                                readyView.delegate = self
                                
                               print("Orden 2: \(String(describing: valueOrder.value(forKey: "order_number") as! String)) \(order.to_refund)  ")
                                
                                let sendNotifications =   DatabaseFunctions.shared.getStatusNotifications(idOrder: order.order_id)
                                self.present(readyView, animated: true, completion: nil)
                                DatabaseFunctions.shared.insertFromFirebase(establishment_id: order.establishment_id, establishment_name: order.establishment_name, in_site: valueOrder.value(forKey: "in_site") as! Bool, notes: valueOrder.value(forKey: "notes") as! String, billing_address_id: 0, payment_method: valueOrder.value(forKey: "payment_method") as! Int, discount_code: "", total: totalPrice, estimated_time: valueOrder.value(forKey: "full_preparation_time") as! Int, pickpal_comission: valueOrder.value(forKey: "pickpal_service") as! Double, total_after_checkout: totalPrice, order_has_been_sent: true, code: valueOrder.value(forKey: "code") as! String, order_number: valueOrder.value(forKey: "order_number") as! String, folio: valueOrder.value(forKey: "folio") as! String, register_date:order.register_date, process_time: valueOrder.value(forKey: "register_hour") as! String, ready_time: valueOrder.value(forKey: "ready_hour") as! String, pay_time: valueOrder.value(forKey: "register_hour") as! String, order_id: order.order_id, order_elements: dictionaryInfo, statusItem: statusClient, qr: qrCode, is_paid: valueOrder.value(forKey: "is_paid") as! Bool, ttl: valueOrder.value(forKey: "register_hour") as! String, isCash: false, last4: last4, statusController: statusController, is_scheduled: valueOrder.value(forKey: "is_schedule") as! Bool, in_bar: valueOrder.value(forKey: "in_bar") as! Bool, percent: valueOrder.value(forKey: "pickpal_reward") as! Double, table_number: valueOrder.value(forKey: "number_table") as! Int, readyFlag: flagReady, points: valueOrder.value(forKey: "points") as! Double, money_puntos: valueOrder.value(forKey: "money_points") as! Double, order_type: order.order_type, address: order.address, shipping_cost: order.shipping_cost, folio_complete: valueOrder.value(forKey: "complete_folio") as! String, time_delivery: String (order.time_delivery), to_refund: order.to_refund, days_elapsed: order.days_elapsed, allow_process_nopayment: order.allow_process_nopayment, est_delivery_hour: order.est_delivery_hour, est_ready_hour: order.est_ready_hour, is_generic: order.is_generic, confirmed:valueOrder.value(forKey: "confirmed") as! Bool, confirm_hour: valueOrder.value(forKey: "confirm_hour") as! String, sendNotifications: sendNotifications, notified: valueOrder.value(forKey: "notified") as? Bool ?? false, pickpal_cash_money:valueOrder.value(forKey: "pickpal_cash_money") as? Double ?? 0, av_customer_service: valueOrder.value(forKey: "av_customer_service") as? Bool ?? false, two_devices: valueOrder.value(forKey: "two_devices") as? Bool ?? false, active_customer_service: order.active_customer_service, total_card: order.total_card, total_cash: order.total_cash, total_to_pay: order.total_to_pay, total_done_payment: order.total_done_payment,subtotal: valueOrder.value(forKey: "subtotal") as? Double ?? 0 )
                                
                                
                            }
                            
                        }
                        if statusClient == "to_pay" && (order.order_type == "SM" || order.order_type == "RB") {
                            let registerDay = valueOrder.value(forKey: "register_date") as! String
                            let registerHour = valueOrder.value(forKey: "register_hour") as! String
                            let estimated = registerDay + " " + registerHour
                            //        print(estimated)
                            let form = DateFormatter()
                            form.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //                            form.locale = Locale(identifier: "UTC")
                            if form.date(from: estimated) != nil {
                                let dateRegister = form.date(from: estimated)
                                let currentDay = Date()
                                let timeInterval = currentDay.timeIntervalSince(dateRegister!)
                                //                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateStatus"), object: self)
                               // self.orderws.retryOrder(order_id: order_id)
                                if (timeInterval > 780) {
                                    self.orderws.changeOrderStatus(order_id: order.order_id, status: 7)
                                   // DatabaseFunctions.shared.deleteDueOrder(order_id: order.order_id)
                                }
                            }
                            
                        }
                        if let isRefunded = valueOrder.value(forKey: "is_refunded") as? Bool {
                            if isRefunded {
                                DatabaseFunctions.shared.deleteRefundedOrders(orderId: order.order_id)
                            }
                            
                        }
                    }
                }
                
            })
        }
    }
    
}

extension MenuViewController{
    
    @objc func openHomePoints(_ notification: NSNotification) {
        
        if openPuntosPickPal{
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.pushExchange = (notification.userInfo! as NSDictionary).value(forKey: "dataPusExchange") as! PushExchange
        newVC.isPushExchange = true
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
//            openPuntosPickPal = false
        }
//        let ponits = HomePointsViewController()
//        ponits.pushExchange = (notification.userInfo! as NSDictionary).value(forKey: "dataPusExchange") as! PushExchange
//        ponits.isPushExchange = true
//        self.present(ponits, animated: true, completion: nil)
        
        
    }
    
    func restoreSesion() {
        
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
//        let db = try! Connection(request: "\(path)/orders.db")
//        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        self.navigationController?.pushViewController(vc,animated: true)
        
    }
}

extension  MenuViewController{
    
    
    func didSuccessViewProfile(profile: UserProfile) {
       
        var fullName = profile.name ?? ""
        var fullNameArr = fullName.components(separatedBy: " ")
        var firstName: String = fullNameArr[0]
        var lastName: String? = fullNameArr.count > 3 ? fullNameArr[1] : ""
        
//        UserDefaults.standard.set(.replacingOccurrences(of: " IOS", with: ""), forKey: "first_name")
        UserDefaults.standard.set( "\(firstName) \(lastName!)", forKey: "first_name")
    }
    
    func didFailViewProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        if title != "empty"{
            let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.errorMessageString = title
            newXIB.subTitleMessageString = subtitle
            present(newXIB, animated: true, completion: nil)
        }
    }
    
    
}
