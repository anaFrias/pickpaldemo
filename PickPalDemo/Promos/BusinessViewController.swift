//        MARK: ola

import UIKit
import Nuke
import GoogleMaps
import MapKit
import SQLite
import Firebase
import OneSignal

class BusinessViewController: UIViewController, UIScrollViewDelegate, PromosDelegate {
    
    var id: Int!
    var ws = PromosWS()
    var municipio: String!
    var categoria: String!
    var lugar: String!
    var establecimiento: String!
    var amenitiesOps = [String : String]()
    var redes = [String: String]()
    var titles = [String]()
    @IBOutlet weak var heightStuffView: NSLayoutConstraint!
    var images = [""]
    var totalPrice = 0.00
    var defaults = UserDefaults.standard
    
    @IBOutlet weak var placeArrow: UIImageView!
    @IBOutlet weak var catArrow: UIImageView!
    @IBOutlet weak var munArrow: UIImageView!
    
    @IBOutlet weak var stuff: UIView!
    
    //    IBOutlets
    @IBOutlet weak var munLabel: UILabel!
    @IBOutlet weak var catLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var estabLabel: UILabel!
    
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var viewItems: UIView!
    @IBOutlet weak var totalItemsLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    //    Info
 
    var logoString = String()
    @IBOutlet weak var establishment_title: UILabel!
    @IBOutlet weak var descr_label: UILabel!
    
    @IBOutlet weak var schedules_text: UILabel!
    @IBOutlet weak var schedules_label: UILabel!
    @IBOutlet weak var category_text: UILabel!
    @IBOutlet weak var category_label: UILabel!
    @IBOutlet weak var descr_text: UILabel!
    
    @IBOutlet weak var viewContainerStuff: UIView!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var scrollConstraint: NSLayoutConstraint!
    
    //Numero de pedidos
    @IBOutlet weak var numPedidos: UIView!
    //    CONTENEDOR GENERAL
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightStuff: NSLayoutConstraint!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    //    Buttons map and info
    @IBOutlet weak var buttonMap: UIButton!
    @IBOutlet weak var buttonInfo: UIButton!
    
    @IBOutlet weak var detailTableView: UITableView!
    
    
    let offset_HeaderStop:CGFloat = 250.0 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label
    //        MARK: khe
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var singleInfo = PromoSingleEstablishment()
    var infoSection = [String]()
    var dictionaryArray =  [[Dictionary<String, AnyObject>]]()
    
    var expandInfo = Bool()
    var expandMap = Bool()
    
    var municipioStr = String()
    var categoriaStr = String()
    var lugarString = String()
    var establString = String()
    
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var mapa: MKMapView!
    
    var moreLessFlagPerSection = [[String]]()
    var moreLessFlagPerItem = [String]()
    //        MARK: ace?
    
    var latitud = CLLocationDegrees()
    var longitud = CLLocationDegrees()
    var totalItemsPerSection = Int()
    @IBOutlet weak var screenScrollView: UIScrollView!
    var dictionaryInfo = [Dictionary<String,Any>]()
    
    var modifiersItem = [Modifiers]()
    var complementsItem = [Complements]()
    var hideFilter = false
    var sectionChanged = 0
    var scrollSizes = [Dictionary<String,Any>]()
    var indexCategories = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        detailTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        //ws.viewSingleEstablishment(establishmentID: self.id, clientID: 4)
        screenScrollView.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        //LoadingOverlay.shared.showOverlay(view: self.view)
        amenitiesOps["header"] = "header"
        titles.append("header")
        self.makeSingle()
        if municipioStr == "" && lugarString == "" && establString == "" && categoriaStr == ""{
            if let valueM = UserDefaults.standard.object(forKey: "municipalty") as? String {
                municipioStr = valueM
            }
            if let valueC = UserDefaults.standard.object(forKey: "category") as? String {
                categoriaStr = valueC
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "place") as? String {
                lugarString = valueP
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "establishment_name") as? String {
                establString = valueP
            }
        }
        munLabel.text = municipioStr
        if categoriaStr != "Categoría" {
            catLabel.text = categoriaStr
            if lugarString != "Ubicación" {
                placeLabel.text = lugarString
                if establString != "" {
                    estabLabel.text = establString
                }else{
                    estabLabel.alpha = 0
                }
            }else{
                if establString != "" {
                    placeLabel.text = establString
                    placeLabel.textColor = UIColor.init(red: 80/255, green: 104/255, blue: 114/255, alpha: 1)
                }else{
                    estabLabel.alpha = 0
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                }
            }
        }else{
            if lugarString != "Ubicación" {
                catLabel.text = lugarString
                if establString != "" {
                    placeLabel.text = establString
                }else{
                    estabLabel.alpha = 0
                    catLabel.alpha = 0
                    catArrow.alpha = 0
                }
            }else{
                if establString != "" {
                    catLabel.text = establString
                    catLabel.textColor = UIColor.init(red: 80/255, green: 104/255, blue: 114/255, alpha: 1)
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                    estabLabel.alpha = 0
                }else{
                    catLabel.alpha = 0
                    catArrow.alpha = 0
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                    estabLabel.alpha = 0
                }
            }
        }
        // Do any additional setup after loading the view.
        amenitiesOps["redes"] = "redes"
        titles.append("redes")
        detailTableView.reloadData()
    }
    
    func makeSingle(){
        images = singleInfo.gallery
        let latit = CLLocationDegrees(singleInfo.latitude)
        let longi = CLLocationDegrees(singleInfo.longitude)
        self.latitud = latit
        self.longitud = longi
        
        
        let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
        mapa.setCamera(camera, animated: true)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
        let annotation = MKPointAnnotation()
        annotation.coordinate = eyeCoordinate
        mapa.addAnnotation(annotation)
        //if singleInfo.name != nil {
        establishment_title.alpha = 1
        establishment_title.text = singleInfo.name
        //}
        let scrollHeight = 295 + (infoSection.count * 24) + (singleInfo.promotionals.count * 140) //(ind * 140)
        if CGFloat(scrollHeight) != UIScreen.main.bounds.height {
            containerConstraint.constant = CGFloat(scrollHeight)
        }
        
        if(singleInfo.socialnformation != nil){
            if singleInfo.socialnformation.paymentAllow != nil {
                amenitiesOps["Métodos de pago:"] = singleInfo.socialnformation.paymentAllow
                titles.append("amenity")
            }
            if singleInfo.socialnformation.averagePrice != nil {
                amenitiesOps["Precio promedio por persona:"] = singleInfo.socialnformation.averagePrice
                titles.append("amenity")
            }
            if singleInfo.socialnformation.petFriendly != nil {
                amenitiesOps["Comercio pet friendly:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.availableWifi != nil {
                amenitiesOps["Wifi disponible:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.availableAlc != nil {
                amenitiesOps["Venta de alcohol:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.areas != nil {
                amenitiesOps["Áreas:"] = singleInfo.socialnformation.areas
                titles.append("amenity")
            }
            if singleInfo.socialnformation.availableKids != nil {
                amenitiesOps["Área de juegos infantil:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.environment != nil {
                amenitiesOps["Ambiente"] = singleInfo.socialnformation.environment
                titles.append("amenity")
            }
            if singleInfo.socialnformation.music != nil {
                amenitiesOps["Tipo de música:"] = singleInfo.socialnformation.music
                titles.append("amenity")
            }
            if singleInfo.socialnformation.availableSmoke != nil {
                amenitiesOps["Área de fumar:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.availableDelivery != nil {
                amenitiesOps["Servicio a domicilio independiente:"] = "Sí"
                titles.append("amenity")
            }
            if singleInfo.socialnformation.amenities != nil {
                amenitiesOps["Amenities:"] = singleInfo.socialnformation.amenities
                titles.append("amenity")
            }
            if singleInfo.socialnformation.siteURL != nil {
                redes["web"] = singleInfo.socialnformation.siteURL
            }
            if singleInfo.socialnformation.instagramURL != nil {
                redes["insta"] = singleInfo.socialnformation.instagramURL
            }
            if singleInfo.socialnformation.facebookURL != nil {
                redes["face"] = singleInfo.socialnformation.facebookURL
            }
            if singleInfo.socialnformation.twitterURL != nil {
                redes["twitter"] = singleInfo.socialnformation.twitterURL
            }
            
        }
    }
    func didSuccessSingleEstablishment(establishment: PromoSingleEstablishment)  {
        print("****")
        print(establishment.id)
        print(establishment.promotionals)
        print("****")
        images = establishment.gallery
        //singleInfo = establishment
        infoSection.removeAll()
        dictionaryArray.removeAll()
        scrollSizes.removeAll()
        let latit = CLLocationDegrees(establishment.latitude)
        let longi = CLLocationDegrees(establishment.longitude)
        self.latitud = latit
        self.longitud = longi
        
        let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
        mapa.setCamera(camera, animated: true)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
        let annotation = MKPointAnnotation()
        annotation.coordinate = eyeCoordinate
        mapa.addAnnotation(annotation)
        //if singleInfo.name != nil {        establishment_title.alpha = 1
        establishment_title.text = singleInfo.name
        //}
        detailTableView.reloadData()
        
        let scrollHeight = 295 + (infoSection.count * 24) + (singleInfo.promotionals.count * 135) //(ind * 140)
        if CGFloat(scrollHeight) != UIScreen.main.bounds.height {
            containerConstraint.constant = CGFloat(scrollHeight)
        }
        let aroundOffset = infoSection.count * 15
        for i in 0..<infoSection.count {
            for j in 0..<aroundOffset {
                //         let dict = ["size": makeItems(index: i) + CGFloat(j * 10), "index": i] as [String : Any]
                //  scrollSizes.append(dict)
            }
        }
        //tableView.reloadData()
        //collectionView.reloadData()
        //viewDidLayoutSubviews()
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func home(_ sender: Any) {
        UserDefaults.standard.set("promos", forKey: "place")
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func settings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func showMap(_ sender: Any) {
        buttonInfo.isSelected = false
        if expandMap {
            buttonMap.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandMap = false
                self.heightStuffView.constant = 295
                self.tableViewTop.constant = 295
            }
            
        }else{
            buttonMap.isSelected = true
            self.viewMap.alpha = 1
            //            self.descr_text.alpha = 0
            //            self.descr_label.alpha = 0
            //            self.category_text.alpha = 0
            //            self.category_label.alpha = 0
            //            self.schedules_label.alpha = 0
            //            self.schedules_text.alpha = 0
            //            self.kitchenTypeTitle.alpha = 0
            //            self.kitchenTypeInfo.alpha = 0
            //            self.phoneInfoLabel.alpha = 0
            //            self.phoneInfoLabelext.alpha = 0
            //            self.directionDescript.alpha = 0
            //            self.directionsTitleLabel.alpha = 0
            self.detailTableView.alpha = 0
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandMap = true
                
            }
        }
    }
    
    @IBAction func showInfo(_ sender: Any) {
        buttonMap.isSelected = false
        if expandInfo {
            buttonInfo.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandInfo = false
                self.heightStuffView.constant = 295
                self.tableViewTop.constant = 295
            }
        }else{
            buttonInfo.isSelected = true
            self.viewMap.alpha = 0
            self.detailTableView.alpha = 1
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandInfo = true
            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = scrollView.frame.size.width * CGFloat(index)
                frame.size = scrollView.frame.size
                
                let view = UIView(frame: frame)
                view.bounds.size.height = scrollView.frame.size.height
                //            view.backgroundColor = UIColor.red
                let image = UIImageView()
                if images[index] != "" {
                    Nuke.loadImage(with: URL(string: images[index])!, into: image)
                }
                
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: scrollView.frame.size)
                view.clipsToBounds = true
                
                
                scrollView.addSubview(image)
                
                width += Int(scrollView.frame.width)
                
                self.scrollView.addSubview(view)
            }
            scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(images.count)), height: scrollView.frame.size.height)
            scrollView.delegate = self
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == screenScrollView {
            let offset = scrollView.contentOffset.y
            var headerTransform = CATransform3DIdentity
            
            // PULL DOWN -----------------
            if offset < 0 {
                let headerScaleFactor:CGFloat = -(offset) / self.stuff.bounds.height
                let headerSizevariation = ((self.stuff.bounds.height * (1.0 + headerScaleFactor)) - self.stuff.bounds.height)/2.0
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                self.stuff.layer.transform = headerTransform
            }else {
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                if offset <= offset_HeaderStop {
                }else {
                }
            }
            self.stuff.layer.transform = headerTransform
            let index = scrollSizes.index(where: {dict -> Bool in
                return dict.contains(where: {_ in dict["size"] as! CGFloat == scrollView.contentOffset.y})
            })
            if index != nil {
                indexCategories = (scrollSizes[index!]["index"]) as! Int
                //collectionView.reloadData()eloadData()
            }
            if scrollView.contentOffset.y <= 250 {
                indexCategories = 0
                //collectionView.reloadData()eloadData()
            }
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.numberOfPages = images.count
        pageControl.currentPage = Int(pageNumber)
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.size(forNumberOfPages: images.count)
    }
    
    func showMarker(position: CLLocationCoordinate2D) {
        
    }
    
    @IBAction func openMaps(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitud),\(longitud)&zoom=14&views=traffic&q=\(latitud),\(longitud)")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitud, longitud)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = establString
            mapItem.openInMaps(launchOptions: options)
        }
        
    }
    
    func createHorario(horario: [ScheduleItem]) -> String{
        var i = 0;
        var j = 0;
        var horarios = [itemHorario]()
        for item in horario{
            if i == 0{
                let x = itemHorario()
                x.open = item.open_hour
                x.close = item.closing_hour
                x.addname(name: item.day)
                horarios.append(x)
                j += 1
            }else{
                if (horarios[j-1].open == item.open_hour && horarios[j-1].close == item.closing_hour){
                    horarios[j-1].addname(name: item.day)
                }else{
                    let x = itemHorario()
                    x.open = item.open_hour
                    x.close = item.closing_hour
                    x.addname(name: item.day)
                    horarios.append(x)
                    j += 1
                }
            }
            i += 1
        }
        
        var txtHorario = ""
        
        for z in 0..<horarios.count{
            if(z == horarios.count - 1){
                if(horarios[z].names.count > 1){
                    txtHorario += horarios[z].names[0] + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    
                }else{
                    if(horarios[z].names[0] == "Todos"){
                        txtHorario += "Lunes - Domingo: " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }else{
                        txtHorario += horarios[z].names[0] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }
                    
                }
            }else{
                if(horarios[z].names.count > 1){
                    txtHorario +=  horarios[z].names[0]  + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }else{
                    txtHorario += horarios[z].names[0]  + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }
            }
        }
        return txtHorario
    }
    
    func didFailGetSingleEstablishment(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension BusinessViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == detailTableView {
            print("1")
            return 1
        }else{
            print("2")
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == detailTableView {
            return amenitiesOps.count
        }else{
//            print(singleInfo.promotionals.count.description)
            return singleInfo.promotionals.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(tableView.tag)
        if tableView == detailTableView {
            if(titles[indexPath.row] == "header"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailEstablishmentCell", for: indexPath) as! promosAmenitiesCell
                if singleInfo.descripcion != nil {
                    cell.descr_label.text = "Descripción:"
                    cell.descr_text.text = singleInfo.descripcion
                }
                if singleInfo.advsCategorry != nil {
                    cell.category_label.text = "Categoría:"
                    cell.category_text.text = singleInfo.advsCategorry
                }
                if singleInfo.operation_schedule != nil {
                    if singleInfo.operation_schedule.count > 0 {
                        cell.schedules_label.text = "Horario:"
                        cell.schedules_text.text = createHorario(horario: singleInfo.operation_schedule)
                    }
                }
                return cell
            } else if(titles[indexPath.row] == "redes"){
                let cell = tableView.dequeueReusableCell(withIdentifier: "redesCell", for: indexPath) as! RedesSocialesItemCell
                if Array(redes.keys).contains("web") {
                    cell.buttonWeb.isHidden = false
                    
                    cell.webUrl = redes["web"]!
                }else{
                    cell.buttonWeb.isHidden = true
                }
                
                if Array(redes.keys).contains("insta") {
                    cell.buttonInsta.isHidden = false
                    
                    cell.instaUrl = redes["insta"]!
                }else{
                    cell.buttonInsta.isHidden = true
                }
                
                if Array(redes.keys).contains("face"){
                    cell.buttonFace.isHidden = false
                    
                    cell.faceUrl = redes["face"]!
                }else{
                    cell.buttonFace.isHidden = true
                }
                
                if Array(redes.keys).contains("twitter") {
                    cell.buttonTwitter.isHidden = false
                    
                    cell.twitterUrl = redes["twitter"]!
                }else{
                    cell.buttonTwitter.isHidden = true
                }
                
                if Array(redes.keys).contains("web") || Array(redes.keys).contains("face") || Array(redes.keys).contains("twitter") || Array(redes.keys).contains("insta"){
                    cell.lblSocialNetwork.isHidden = false
                }else{
                    cell.lblSocialNetwork.isHidden = true
                }
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "amenitiesCell", for: indexPath) as! AmenitiesCell
                
                cell.amenityTitle.text = Array(amenitiesOps.keys)[indexPath.row]
                cell.amenityDescription.text = Array(amenitiesOps.values)[indexPath.row]
                return cell
            }
        }else{
            print("->2")
//            print(singleInfo.promotionals[indexPath.row].name)
            let cell = tableView.dequeueReusableCell(withIdentifier: "promoCell", for: indexPath) as! CellPromo
            cell.selectionStyle = .none
            if singleInfo.promotionals[indexPath.row].image != nil {
                Nuke.loadImage(with: URL(string: singleInfo.promotionals[indexPath.row].image)!, into: cell.promoImg)
            }
            cell.place.text = establString
            cell.establishment = establString
            cell.promoDesc = singleInfo.promotionals[indexPath.row].name
            cell.title.text = singleInfo.promotionals[indexPath.row].name
            cell.desc.text = singleInfo.promotionals[indexPath.row].descr
            cell.vigency.text = singleInfo.promotionals[indexPath.row].deadline
            
            return cell
            
        }
        
    }
    
    func tintButton(icon: String, button: UIButton){
        let tintedImage = UIImage(named: icon)?.withRenderingMode(.alwaysTemplate)
        button.setImage(tintedImage, for: .normal)
        button.tintColor = .green
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == detailTableView {
            return nil
        }else{
            /*let header = SectionTitleViewController()
             header.nameCategory = "¡Disfruta de las promociones!"*/
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 60))
            headerView.backgroundColor = UIColor.white
            let label = UILabel()
            label.frame = CGRect.init(x: 80, y: -15, width: headerView.frame.width-10, height: headerView.frame.height-10)
            label.text = "¡Disfruta de las promociones!"
            label.font = UIFont.boldSystemFont(ofSize: 17.0)
            label.textColor = UIColor.green
            
            headerView.addSubview(label)
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == detailTableView {
            return 0
        }else{
            return 24
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == detailTableView {
            if titles[indexPath.row] == "header" {
                return 200
            } else if(titles[indexPath.row] == "redes"){
                return 55
            } else {
                return 15
            }
        }else{
            return 200
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        MARK: Zooming (no borrar)
        
    }

}
