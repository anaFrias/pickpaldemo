//
//  CellPromo.swift
//  PickPalTesting
//
//  Created by Hector Vela on 11/13/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import Foundation

class CellPromo: UITableViewCell {
    
    @IBOutlet weak var promoImg: UIImageView!
    @IBOutlet weak var place: UILabel!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var vigency: UILabel!
    var establishment = String()
    var promoDesc = String()
    @IBAction func buttonShare(_ sender: UIButton) {
        let msg = "*¡Descarga pickpal en tu celular y entérate de las promociones de este comercio y de muchos más!*" + "\n¡Aprovecha y ahorra dinero en " + establishment + ". Hoy " + promoDesc + "\nDescarga aqui: itunes.apple.com/us/app/apple-store/1378364730?mt=8"
        let urlWhats = "whatsapp://send?text=\(msg)"

        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    //UIApplication.shared.openURL(whatsappURL as URL)
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(whatsappURL as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(whatsappURL as URL)
                    }
                } else {
                    print("please install watsapp")
                }
            }
        }
    }
}
