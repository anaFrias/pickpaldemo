//
//  SingleAdvsAuxTwoViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/24/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
import GoogleMaps
import MapKit
import SQLite
import Firebase
import OneSignal

class SingleAdvsAuxTwoViewController: UIViewController, UIScrollViewDelegate, PromosDelegate, AlertAdsDelegate, ListPromoDelegate{
    
    var id: Int!
    var ws = PromosWS()
    var municipio: String!
    var categoria: String!
    var lugar: String!
    var establecimiento: String!
    
    @IBOutlet weak var heightStuffView: NSLayoutConstraint!
    @IBOutlet weak var itemCollectionCons: NSLayoutConstraint!
    
    
    @IBOutlet weak var btnGuia: UIButton!
    @IBOutlet weak var btnPromos: UIButton!
    @IBOutlet weak var btnFoodAndDrinks: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    
    var images = [""]
    var totalPrice = 0.00
    var defaults = UserDefaults.standard
    
    @IBOutlet weak var stuff: UIView!
    //    IBOutlets
    @IBOutlet weak var munLabel: UILabel!
    
    @IBOutlet weak var viewItems: UIView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var establishment_logo: UIImageView!
    @IBOutlet weak var establishment_title: UILabel!
    @IBOutlet weak var descr_label: UILabel!
    
    @IBOutlet weak var schedules_text: UILabel!
    @IBOutlet weak var schedules_label: UILabel!
    @IBOutlet weak var category_text: UILabel!
    @IBOutlet weak var category_label: UILabel!
    @IBOutlet weak var descr_text: UILabel!
    
    @IBOutlet weak var directionDescript: UILabel!
    @IBOutlet weak var directionsTitleLabel: UILabel!
    @IBOutlet weak var phoneInfoLabelext: UILabel!
    @IBOutlet weak var phoneInfoLabel: UILabel!
    @IBOutlet weak var viewContainerStuff: UIView!
    @IBOutlet weak var kitchenTypeTitle: UILabel!
    @IBOutlet weak var kitchenTypeInfo: UILabel!
    
    var handle2: DatabaseHandle!
    var handle3: DatabaseHandle!
    
    var extrasObserve: DatabaseReference!
    var modifiersObserve: DatabaseReference!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var scrollConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblNumberPromos: UILabel!
    @IBOutlet weak var imgAmanc: UIImageView!
    
    //Numero de pedidos
    @IBOutlet weak var numPedidos: UIView!
    //    CONTENEDOR GENERAL
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightStuff: NSLayoutConstraint!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    //    Buttons map and info
    @IBOutlet weak var buttonMap: UIButton!
    @IBOutlet weak var buttonInfo: UIButton!
    
    @IBOutlet weak var detailTableView: UITableView!
    var mSingleReward = SingleReward()
    var singleInfoIcons = SingleEstablishment()
    
    let offset_HeaderStop:CGFloat = 250.0 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var singleInfo = SingleEstabAdvs()
    var infoSection = [String]()
    var dictionaryArray =  [[Dictionary<String, AnyObject>]]()
    var cantidad = Int()
    var cantIndividual = 1
    var complements = [[Complements]]()
    var modifiers = [[Modifiers]]()
    var hasComplemets = [Bool]()
    var hasModifiers = [Bool]()
    
    var modificadores = [[String]]()
    var modificadores_precio = [[Double]]()
    
    var complementos = [[String]]()
    var complementos_precio = [[Double]]()
    var complementos_cant = [[Int]]()
    
    var cantitiesItems = [[Int]]()
    
    var ids = [Int]()
    var prices = [Double]()
    var names = [String]()
    var cantis = [Int]()
    
    var total_prices = [Double]()
    var total_cantis = [Int]()
    var total_names = [String]()
    
    var expandInfo = Bool()
    var expandMap = Bool()
    
    var cantitiesPerItem = [[Int]]()
    var cantitiesPerSection = [Int]()
    var namesPerItem = [[String]]()
    var namesPerSection = [String]()
    var index = [Int]()
    var row = [Int]()
    
    var totalItems = [[Int]]()
    var totals = [[Int]]()
    
    var municipioStr = String()
    var categoriaStr = String()
    var lugarString = String()
    var establString = String()
    var root = String()
    
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var mapa: MKMapView!
    
    var moreLessFlagPerSection = [[String]]()
    var moreLessFlagPerItem = [String]()
    
    var latitud = CLLocationDegrees()
    var longitud = CLLocationDegrees()
    var totalItemsPerSection = Int()
    @IBOutlet weak var screenScrollView: UIScrollView!
    var dictionaryInfo = [Dictionary<String,Any>]()
    //    Arreglo de arreglos de dictionaries
    //    var all_orders = [dictionaryInfo]
    var modifiersItem = [Modifiers]()
    var complementsItem = [Complements]()
    var hideFilter = false
    var sectionChanged = 0
    var scrollSizes = [Dictionary<String,Any>]()
    var indexCategories = 0
    //    MARK: Know if there's a current order
    var order_done = Bool()
    var notes = String()
    var itemIds = [Int]()
    var quantity = [Int]()
    var orderBelogChangeEstblishment = false
    var itemReference: DatabaseReference!
    var handle: DatabaseHandle!
    
    var guideWS = GuidePickPalWS()
    
    //var operationRef: DatabaseReference!
    //var handleOperation: DatabaseHandle!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        screenScrollView.delegate = self
        guideWS.delegate = self
        guideWS.viewSingleEstablishment(establishmentID: id)
        self.navigationController?.isNavigationBarHidden = true
        //LoadingOverlay.shared.showOverlay(view: self.view)
        
        //ws.viewSingleAdvsEstablishment(establishmentID: id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
        if municipioStr == "" && lugarString == "" && establString == "" && categoriaStr == ""{
            if let valueM = UserDefaults.standard.object(forKey: "municipalty") as? String {
                municipioStr = valueM
            }
            if let valueC = UserDefaults.standard.object(forKey: "category") as? String {
                categoriaStr = valueC
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "place") as? String {
                lugarString = valueP
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "establishment_name") as? String {
                establString = valueP
            }
        }
    }
    
    
    @IBAction func goFoodAndDrinks(_ sender: Any) {
        
        let imagesList = getIconsServicePickPal(data: singleInfoIcons.pickpal_services)
        let image = getImage(image: imagesList[0])
//        goToNavigation(acttion: image)
        
        
    }
    @IBAction func goToPromos(_ sender: Any) {
        
        let imagesList = getIconsServicePickPal(data: singleInfoIcons.pickpal_services)
        let image = getImage(image: imagesList[1])
//        goToNavigation(acttion: image)
        
    }
    
    @IBAction func goToGuide(_ sender: Any) {
        
        let imagesList = getIconsServicePickPal(data: singleInfoIcons.pickpal_services)
        let image = getImage(image: imagesList[2])
//        goToNavigation(acttion: image)
    }
    
    
    @IBAction func goToPonits(_ sender: Any) {
        
        let imagesList = getIconsServicePickPal(data: singleInfoIcons.pickpal_services)
        let image = getImage(image: imagesList[2])
//        goToNavigation(acttion: image)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        images = singleInfo.gallery
        print(singleInfo.promotionals)
        
        var countHeaderq = 0
        for promo in singleInfo.promotionals {
            if !promo.Header.elementsEqual("Mis Promociones") && !promo.Header.elementsEqual("Mis Noticias") {
                countHeaderq += 1
            }
        }
        
        lblNumberPromos.text = String(countHeaderq)
        for news in singleInfo.news {
            singleInfo.promotionals.append(news)
        }
        order_done = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).1
        let strinr = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        quantity.removeAll()
        itemIds.removeAll()
        
        scrollConstraint.constant = 0
        if !order_done && strinr != "" {
            let encoded = strinr.data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            
            scrollConstraint.constant = 38
            self.numPedidos.alpha = 0
            
            if let tempElements = dictionary as? [Dictionary<String,Any>] {
                var tempPrice = Double()
                var tempCant = Int()
                for temp in tempElements {
                    let double = temp["price"] as! Double
                    tempPrice += double
                    let cants = temp["quantity"] as! Int
                    tempCant += cants
                    if let arrayCan = temp["modifier_prices"] as? NSArray {
                        for arr in arrayCan {
                            let price_modif = Double(arr as! String)
                            tempPrice += price_modif!
                        }
                    }else{
                        if let arrayM = temp["modifiers"] as? NSArray {
                            for modif in arrayM {
                                if let array = modif as? NSDictionary {
                                    if let arr = array.value(forKey: "modifier_price") as? NSArray {
                                        for tempM in arr {
                                            let price_modif = tempM as! Double
                                            tempPrice += price_modif
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if let arrayC = temp["complements"] as? NSDictionary {
                        if let arr = arrayC.value(forKey: "complement_price") as? NSArray {
                            for tempC in arr {
                                let price_comp = tempC as! Double
                                tempPrice += price_comp
                            }
                        }
                    }
                    itemIds.append(temp["id"] as! Int)
                    quantity.append(temp["quantity"] as! Int)
                }
                
                dictionaryInfo = tempElements
            }
        }else{
            totalPrice = 0
            cantidad = 0
            cantitiesPerSection.removeAll()
            cantitiesPerItem.removeAll()
            namesPerSection.removeAll()
            namesPerItem.removeAll()
            moreLessFlagPerItem.removeAll()
            moreLessFlagPerSection.removeAll()
            dictionaryInfo.removeAll()
        }
        
        if order_done {
            self.numPedidos.alpha = 1
        }else{
            self.numPedidos.alpha = 0
        }
        
        tableView.reloadData()
        establishment_title.alpha = 1
        establishment_title.text = singleInfo.name
        
        root = "\(singleInfo.address.city ?? "") > \(singleInfo.advsCategorry ?? "") > \(singleInfo.subadvsCategory ?? "")"
        self.munLabel.text = root
        
        if singleInfo.noProfit {
            imgAmanc.isHidden = false
        }else{
            imgAmanc.isHidden = true
        }
        let latit = CLLocationDegrees(singleInfo.latitude)
        let longi = CLLocationDegrees(singleInfo.longitude)
        self.latitud = latit
        self.longitud = longi
        
        let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
        mapa.setCamera(camera, animated: true)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
        let annotation = MKPointAnnotation()
        annotation.coordinate = eyeCoordinate
        mapa.addAnnotation(annotation)
        
        //detailTableView.reloadData()
        var e = Int()
        var ind = 0
        var countHeader = 0
        
        for _ in 0..<singleInfo.promotionals.count {
            if !singleInfo.promotionals[ind].Header.elementsEqual("Mis Promociones") && !singleInfo.promotionals[ind].Header.elementsEqual("Mis Noticias") {
                totalItemsPerSection += 1
            }else{
                countHeader += 1
            }
            ind += 1
        }
        
        let extraHeigth = countHeader == 1 ? 45 : 90
        
        let scrollHeight = 295 + (totalItemsPerSection * 210) + extraHeigth
        if CGFloat(scrollHeight) != UIScreen.main.bounds.height {
            containerConstraint.constant = CGFloat(scrollHeight)
        }
        
        viewDidLayoutSubviews()
    }
    
    @IBAction func backButton(_ sender: Any) {
        var arrayViews = [Bool]()
        var homeViewController = UIViewController()
        for viewController in (self.navigationController?.viewControllers)! {
            if viewController.isKind(of: HomePromosViewController.self){
                arrayViews.append(true)
                homeViewController = viewController
            }else{
                arrayViews.append(false)
            }
        }
        if arrayViews.contains(true) {
            self.navigationController?.popToViewController(homeViewController, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
            let navController = UINavigationController(rootViewController: newVC)
            navController.modalTransitionStyle = .crossDissolve
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = true
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func showMap(_ sender: Any) {
        buttonInfo.isSelected = false
        if expandMap {
            buttonMap.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandMap = false
                self.itemCollectionCons.constant = 0
                self.heightStuffView.constant = 291
                self.tableViewTop.constant = 291
            }
            
        }else{
            buttonMap.isSelected = true
            self.viewMap.alpha = 1
            self.detailTableView.alpha = 0
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404 - 34
                self.heightStuffView.constant = 291+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 291+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404
                self.heightStuffView.constant = 291+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 291+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandMap = true
                
            }
        }
    }
    
    @IBAction func showInfo(_ sender: Any) {
        detailTableView.reloadData()
        buttonMap.isSelected = false
        if expandInfo {
            buttonInfo.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandInfo = false
                self.itemCollectionCons.constant = 0
                self.heightStuffView.constant = 291
                self.tableViewTop.constant = 291
            }
        }else{
            buttonInfo.isSelected = true
            self.viewMap.alpha = 0
            self.detailTableView.alpha = 1
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404 - 34
                self.heightStuffView.constant = 291+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 291+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404
                self.heightStuffView.constant = 291+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 291+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandInfo = true
            }
        }
    }
    
    @IBAction func viewOrder(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        newVC.modifiers = self.modifiersItem
        newVC.complements = self.complementsItem
        newVC.establishment_id = self.id
        newVC.municipio = municipioStr
        newVC.categoria = categoriaStr
        newVC.lugar = lugarString
        newVC.esablishmentName = establString
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = scrollView.frame.size.width * CGFloat(index)
                frame.size = scrollView.frame.size
                
                let view = UIView(frame: frame)
                view.bounds.size.height = scrollView.frame.size.height
                //            view.backgroundColor = UIColor.red
                let image = UIImageView()
                if images[index] != "" {
                    Nuke.loadImage(with: URL(string: images[index])!, into: image)
                }
                
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: scrollView.frame.size)
                view.clipsToBounds = true
                
                
                scrollView.addSubview(image)
                
                width += Int(scrollView.frame.width)
                
                self.scrollView.addSubview(view)
            }
            scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(images.count)), height: scrollView.frame.size.height)
            scrollView.delegate = self
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == screenScrollView {
            let offset = scrollView.contentOffset.y
            var headerTransform = CATransform3DIdentity
            
            // PULL DOWN -----------------
            if offset < 0 {
                let headerScaleFactor:CGFloat = -(offset) / self.stuff.bounds.height
                let headerSizevariation = ((self.stuff.bounds.height * (1.0 + headerScaleFactor)) - self.stuff.bounds.height)/2.0
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                self.stuff.layer.transform = headerTransform
            }else {
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                if offset <= offset_HeaderStop {
                }else {
                }
            }
            self.stuff.layer.transform = headerTransform
            let index = scrollSizes.index(where: {dict -> Bool in
                return dict.contains(where: {_ in dict["size"] as! CGFloat == scrollView.contentOffset.y})
            })
            
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.numberOfPages = images.count
        pageControl.currentPage = Int(pageNumber)
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.size(forNumberOfPages: images.count)
    }
    
    
    
    func showMarker(position: CLLocationCoordinate2D) {
        
    }
    
    @IBAction func openMaps(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitud),\(longitud)&zoom=14&views=traffic&q=\(latitud),\(longitud)")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitud, longitud)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = establString
            mapItem.openInMaps(launchOptions: options)
        }
        
    }
    
    
    
    func createSimpleItems(name: String, cants: Int, price: Double, id: Int, isPackage: Bool) {
        
        if !dictionaryInfo.contains(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        }) {
            var notes = [String]()
            if let not = UserDefaults.standard.object(forKey: "notas") as? [String]{
                notes = not
            }
            notes.append("")
            defaults.set(notes, forKey: "notas")
            let dictionary = ["name": name,
                              "quantity": cants,
                              "price":price*Double(cants),
                              "complex": false,
                              "id": id,
                              "is_package": isPackage,
                              "price_product": price] as [String : Any]
            dictionaryInfo.append(dictionary)
        }else{
            var notes = [String]()
            if let not = UserDefaults.standard.object(forKey: "notas") as? [String]{
                notes = not
            }
            notes.append("")
            defaults.set(notes, forKey: "notas")
            let index = dictionaryInfo.index(where: {dict -> Bool in
                return dict.contains(where: {_ in dict["name"] as! String == name})
            })
            let cantss = dictionaryInfo[index!]["quantity"] as! Int + cants
            let dictionary = ["name": name,
                              "quantity": cantss,
                              "price":price*Double(cantss),
                              "complex": false,
                              "id": id,
                              "is_package": isPackage,
                              "price_product": price] as [String : Any]
            dictionaryInfo[index!] = dictionary
        }
        //        MARK: Alerta de agregado
    }
    func removeSimpleItems (name: String, cants: Int, price: Double) {
        let index = dictionaryInfo.index(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        })
        dictionaryInfo.remove(at: index!)
        
    }
    
    func removeComplexItems (name: String, cants: Int, price: Double) {
        let index = dictionaryInfo.index(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        })
        if index != nil {
            dictionaryInfo.remove(at: index!)
        }
    }
    func createHorario(horario: [ScheduleItem]) -> String{
        var i = 0;
        var j = 0;
        var horarios = [itemHorario]()
        for item in horario{
            if i == 0{
                let x = itemHorario()
                x.open = item.open_hour
                x.close = item.closing_hour
                x.addname(name: item.day)
                horarios.append(x)
                j += 1
            }else{
                if (horarios[j-1].open == item.open_hour && horarios[j-1].close == item.closing_hour){
                    horarios[j-1].addname(name: item.day)
                }else{
                    let x = itemHorario()
                    x.open = item.open_hour
                    x.close = item.closing_hour
                    x.addname(name: item.day)
                    horarios.append(x)
                    j += 1
                }
            }
            i += 1
        }
        
        var txtHorario = ""
        
        for z in 0..<horarios.count{
            if(z == horarios.count - 1){
                if(horarios[z].names.count > 1){
                    if horarios[z].names.count == 7{
                        txtHorario += horarios[z].names[0] + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + " "
                    }else{
                        for ind in 0..<horarios[z].names.count{
                            if ind == (horarios[z].names.count - 1){
                                txtHorario += horarios[z].names[ind]
                            }else{
                                if ind == (horarios[z].names.count - 2){
                                    txtHorario += horarios[z].names[ind] + " y "
                                }else{
                                    txtHorario += horarios[z].names[ind] + ", "
                                }
                            }
                        }
                        
                        txtHorario += " de " + horarios[z].open + " hrs." + " a " + horarios[z].close + " "
                    }
                }else{
                    if(horarios[z].names[0] == "Todos"){
                        txtHorario += "Lunes - Domingo: " + horarios[z].open + " - " + horarios[z].close + ""
                    }else{
                        txtHorario += horarios[z].names[0] + ": " + horarios[z].open + " - " + horarios[z].close + ""
                    }
                }
            }else{
                if(horarios[z].names.count > 1){
                    txtHorario +=  horarios[z].names[0]  + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }else{
                    txtHorario += horarios[z].names[0]  + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }
            }
        }
        return txtHorario
    }
    func didFailSingleAdvs(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func sendCaptureimageSingle(image: UIImage) {
        
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image as AnyObject)
        
        let activityViewController = UIActivityViewController(activityItems: imagesToShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func onNoinstallWhatsapp() {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "Lo sentimos, no podemos compartir esta promoción porque Whatsapp no esta instalado en este dispositivo."
        newXIB.mTitle = "PickPal Notipromos"
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.isFromAdd = true
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func deletePromoById(id: Int) {
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        
    }
    
}
extension SingleAdvsAuxTwoViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == detailTableView {
            return 1
        }else{
            return singleInfo.promotionals.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == detailTableView {
            return 1
        }else{
            return singleInfo.promotionals.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == detailTableView {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialInformationSingleAdvs", for: indexPath) as! SocialInformationSingleAdvsTableViewCell
            cell.selectionStyle = .none
            
            cell.singleInfo = singleInfo.socialnformation
            cell.lblDescrip.text = singleInfo.descripcion
            cell.lblCat.text = singleInfo.advsCategorry
            cell.lblHorario.text = createHorario(horario: singleInfo.operation_schedule)
            
            var countSocialMedia = 0
            if singleInfo.socialnformation != nil{
                if singleInfo.socialnformation.siteURL == nil {
                    cell.btnWeb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnWeb.isHidden = false
                }
                
                if singleInfo.socialnformation.facebookURL == nil{
                    cell.btnFb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnFb.isHidden = false
                }
                
                if singleInfo.socialnformation.instagramURL == nil{
                    cell.btnInsta.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnInsta.isHidden = false
                }
                
                if singleInfo.socialnformation.twitterURL == nil{
                    cell.btnTwitter.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnTwitter.isHidden = false
                }
                
                if countSocialMedia == 4 {
                    cell.lblRedesSociales.isHidden = true
                    cell.stackSocialMedia.isHidden = true
                }
            }else{
                cell.lblRedesSociales.isHidden = true
                cell.stackSocialMedia.isHidden = true
            }
            return cell
            
        }else{
            
            if singleInfo.promotionals[indexPath.row].isPromo ?? false{
                
            if singleInfo.promotionals[indexPath.row].isCustomImage{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "itemPromo", for: indexPath) as! ListPromoTableViewCell
                cell.selectionStyle = .none
                switch singleInfo.promotionals[indexPath.row].Header {
                    case "Mis Promociones", "Mis Noticias":
                        cell.contentHeader.isHidden = false
                        cell.contentSinglePromo.isHidden = true
                        cell.lblNameHeader.text = singleInfo.promotionals[indexPath.row].Header
                        break
                        
                    default:
                        cell.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                        cell.contentHeader.isHidden = true
                        cell.contentSinglePromo.isHidden = false
                        
                        
                        cell.contentSinglePromo.clipsToBounds = true
                        cell.contentSinglePromo.layer.masksToBounds = true
                        cell.contentSinglePromo.layer.cornerRadius = 15
                        cell.contentSinglePromo.layer.shadowColor = UIColor.black.cgColor
                        cell.contentSinglePromo.layer.shadowOpacity = 0.35
                        cell.contentSinglePromo.layer.shadowOffset = .zero
                        cell.contentSinglePromo.layer.shadowRadius = 20
                        
                        if let image = singleInfo.promotionals[indexPath.row].image, !image.isEmpty {
                            Nuke.loadImage(with: URL(string: image)!, into: cell.imglOGO)
                        }
                        cell.imgSharedPromo.isHidden = !singleInfo.promotionals[indexPath.row].isPromo
                        cell.delegate = self
                        cell.lblDeadLine.text = singleInfo.promotionals[indexPath.row].deadline
                }
            
                    return cell
                    
            }else{

                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "itemPromoImg", for: indexPath) as! ListPromoTableViewCell
                    cell.selectionStyle = .none
                    switch singleInfo.promotionals[indexPath.row].Header {
                    case "Mis Promociones", "Mis Noticias":
                        cell.contentHeader.isHidden = false
                        cell.contentSinglePromo.isHidden = true
                        cell.lblNameHeader.text = singleInfo.promotionals[indexPath.row].Header
                        break
                        
                    default:
                        cell.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                        cell.contentHeader.isHidden = true
                        cell.contentSinglePromo.isHidden = false
                        if let image = singleInfo.promotionals[indexPath.row].image, !image.isEmpty {
                            Nuke.loadImage(with: URL(string: image)!, into: cell.imglOGO)
                            cell.contentImageOfPromo.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                        }
                        cell.contentSinglePromo.clipsToBounds = true
                        cell.contentSinglePromo.layer.masksToBounds = true
                        cell.contentSinglePromo.layer.cornerRadius = 15
                        cell.contentSinglePromo.layer.shadowColor = UIColor.black.cgColor
                        cell.contentSinglePromo.layer.shadowOpacity = 0.35
                        cell.contentSinglePromo.layer.shadowOffset = .zero
                        cell.contentSinglePromo.layer.shadowRadius = 20
                        
                        if singleInfo.promotionals[indexPath.row].isPromo {
                            cell.contentImageOfPromo.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                        }else{
                            cell.contentImageOfPromo.backgroundColor = UIColor(red: 70/255, green: 209/255, blue: 105/255, alpha: 1.0)
                            cell.imglOGO.image = UIImage(named: "imgNewsAmin")
                            cell.imglOGO.frame =  CGRect(x: 26, y: 60, width: 70, height: 70)
                        }
                        
                        cell.imgSharedPromo.isHidden = !singleInfo.promotionals[indexPath.row].isPromo
                        cell.titlePromo = singleInfo.promotionals[indexPath.row].establishment
                        cell.delegate = self
                        cell.lblTitulo.text = singleInfo.promotionals[indexPath.row].name
                        cell.lblDescrip.text = singleInfo.promotionals[indexPath.row].descr
                        cell.lblDeadLine.text = singleInfo.promotionals[indexPath.row].deadline
                    }
                    
                    return cell
                    
                    
                }
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "itemNews", for: indexPath) as! ListPromoTableViewCell
                cell.selectionStyle = .none
                switch singleInfo.promotionals[indexPath.row].Header {
                case "Mis Promociones", "Mis Noticias":
                    cell.contentHeader.isHidden = false
                    cell.contentSinglePromo.isHidden = true
                    cell.lblNameHeader.text = singleInfo.promotionals[indexPath.row].Header
                    break
                    
                default:
                    cell.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                    cell.contentHeader.isHidden = true
                    cell.contentSinglePromo.isHidden = false
                    if let image = singleInfo.promotionals[indexPath.row].image, !image.isEmpty {
                        Nuke.loadImage(with: URL(string: image)!, into: cell.imglOGO)
                        cell.contentImageOfPromo.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                    }
                    
                    if singleInfo.promotionals[indexPath.row].isPromo {
                        cell.contentImageOfPromo.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
                    }else{
                        cell.contentImageOfPromo.backgroundColor = UIColor(red: 70/255, green: 209/255, blue: 105/255, alpha: 1.0)
                        cell.imglOGO.image = UIImage(named: "imgNewsAmin")
                        cell.imglOGO.frame =  CGRect(x: 26, y: 60, width: 70, height: 70)
                    }
                    
                    cell.imgSharedPromo.isHidden = !singleInfo.promotionals[indexPath.row].isPromo
                    cell.titlePromo = singleInfo.promotionals[indexPath.row].establishment
                    cell.delegate = self
                    cell.lblTitulo.text = singleInfo.promotionals[indexPath.row].name
                    cell.lblDescrip.text = singleInfo.promotionals[indexPath.row].descr
                    cell.lblDeadLine.text = singleInfo.promotionals[indexPath.row].deadline
                }
                
                return cell
                
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == detailTableView {
            return 275
        }else{
            switch singleInfo.promotionals[indexPath.row].Header {
            case "Mis Promociones", "Mis Noticias":
                return 44
            default:
                return 210
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        MARK: Zooming (no borrar)
        
    }
    
    
    
}



extension SingleAdvsAuxTwoViewController: GuideDelegate{
    
    
    func getIconsServicePickPal (data: [String]) -> [String]{
        var icons = [String]()

        
        
        for service1 in data{
            if service1 == "POD" {//Pickpal Order guide_icon_food_and_drinks
                icons.append("guide_icon_food_and_drinks")
            }
        }
        
        for service2 in data{
            if service2 == "PPR" {//Pickpal Promos guide_icon
                icons.append("guide_icon_promos")
            }
        }
        
        for service3 in data{
            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
                icons.append("guide_icon")
            }
        }
        
        for service4 in data{
            if service4 == "PPM" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("guide_icon_points")
            }
        }
        
        return icons
    }
    
    
    func didSuccessSingleEstablishmentGuide(info: SingleEstablishment) {
        LoadingOverlay.shared.hideOverlayView()
        images = info.gallery
        singleInfoIcons = info
        infoSection.removeAll()
        totalItems.removeAll()
        cantitiesItems.removeAll()
        dictionaryArray.removeAll()
        scrollSizes.removeAll()
        cantitiesItems.removeAll()
        cantitiesPerItem.removeAll()
        
        if singleInfoIcons.pickpal_services != nil {
            let icons = CustomsFuncs.getIconsServicePickPal(data: singleInfoIcons.pickpal_services)
            switch icons.count {
            case 4:
                
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = false
                
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                break
            case 3:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                break
                
            case 2:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                break
                
            case 1:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                break
                
            default:
                btnFoodAndDrinks.isHidden = true
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                break
            }
            
//            btnFoodAndDrinks.imageView?.contentMode = UIViewContentMode.scaleAspectFit
//            btnPromos.imageView?.contentMode = UIViewContentMode.scaleAspectFit
//            btnGuia.imageView?.contentMode = UIViewContentMode.scaleAspectFit
//            btnPoints.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        }
        
    }
    
    
    func getImage(image: String) -> Int{
        switch image {
        case "guide_icon_food_and_drinks":
            return 1
    
        case "guide_icon_promos":
            return 2
            
        default://guide_icon
            return 3
        }
    }
    
}



