//
//  MyPromotionalsViewController.swift
//  PickPalTesting
//
//  Created by Dev on 12/25/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class MyPromotionalsViewController: UIViewController, PromosDelegate, MyPromosDelegate,
AlertAdsDelegate, ButtonAdminPromosDelgate, userDelegate, AlertSelectNewsPromoDelegate{
    
    @IBOutlet weak var tablePromotionals: UITableView!
    @IBOutlet weak var contentButtonAddPromo: UIView!
    @IBOutlet weak var lblNoMorePromos: UILabel!
    
    var mIdEstab:Int = 0
    var mNameStab:String = ""
    var descriptionEstablish = String()
    
    var mAdminPromo = AdminPromotionals()
    var mListPromos = [Promotionals]()
    var pmws = PromosWS()
    var isFromPromos:Bool = false
    var isUpdatedVersion = Bool()
    var is_month_payment: Bool = true
    var userws = UserWS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pmws.delegate = self
        userws.delegate = self
        
        userws.isUpdateAvailable()
        
        self.tablePromotionals.rowHeight = 60
        tablePromotionals.separatorStyle = UITableViewCellSeparatorStyle.none
        LoadingOverlay.shared.showOverlay(view: self.view)
        pmws.getPromotionalsAdvs(client_id: mIdEstab)
        
    }
    
    func didSuccessIsAppUpdated(isUpdated: Bool) {
        self.isUpdatedVersion = isUpdated
        if !isUpdated{
            self.updateAlert()
        }
    }
    
    @IBAction func goToBack(_ sender: Any) {
        if validateIsEmptyPromos() {
            showDialogToDelete(PromotionalId: 0, nameEstab: mNameStab, valueCall: 1, message: "¿Estás seguro que no quieres dar de alta una promoción?", titleButton: "Si", toHome: false, isNews: false)
        }else{
            closeDialogAndExit(toHome: false)//se cierra la pantalla
        }
    }
    
    @IBAction func OnAddEdditPromo(_ sender: Any) {
        let newXIB = AlertSelectNewsPromoViewController(nibName: "AlertSelectNewsPromoViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mNameEstablishment = mNameStab
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    func readyToCreate(isNews: Bool) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "AddEditPromo") as! AddEditPromoViewController
        newVC.isFromEdit = false
        newVC.mNameStab = mNameStab
        newVC.mIdEstab = mIdEstab
        newVC.mListImages = mAdminPromo.images
        newVC.isFromPromos = isFromPromos
        newVC.descriptionEstablish = descriptionEstablish
        newVC.isNews = isNews
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func goToEdit(id: Int, nameEstab: String, indice: Int, isNews: Bool) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "AddEditPromo") as! AddEditPromoViewController
        newVC.isFromEdit = true
        newVC.mNameStab = mNameStab
        newVC.mIdEstab = mIdEstab
        newVC.mAdminPromotionals = mAdminPromo.promotions[indice]
        newVC.mListImages = mAdminPromo.images
        newVC.isFromPromos = isFromPromos
        newVC.isNews = isNews
        newVC.descriptionEstablish = descriptionEstablish
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func deletePromoById(id: Int) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        pmws.deletePromotionalById(promotional_id: String(id))
    }
    
    func closeDialogAndExit(toHome: Bool) {
        if toHome {
            if !isFromPromos {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                UserDefaults.standard.set("promos", forKey: "place")
                let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "HomePromos") as! HomePromosViewController
                let navController = UINavigationController(rootViewController: newVC)
                navController.modalTransitionStyle = .crossDissolve
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }else{
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVc = storyboard.instantiateViewController(withIdentifier: "MyEstabsAdvs") as! MyEstabsAdvsViewController
            newVc.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVc, animated: true)
        }
    }
    
    func showDialogToDelete(PromotionalId: Int, nameEstab: String, valueCall: Int, message: String, titleButton: String, toHome: Bool, isNews: Bool) {
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = mNameStab
        newXIB.mToHome = toHome
        newXIB.mTitleButton = titleButton
        newXIB.mId = PromotionalId
        newXIB.valueCall = valueCall
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    //Funcion que me indicara que la llamada fue exitosa al consultar todas las promociones
    func didSuccessGetMyPromos(info: AdminPromotionals) {
        mAdminPromo = info
        mListPromos = mAdminPromo.promotions
//        print(mAdminPromo.promotions)
        LoadingOverlay.shared.hideOverlayView()
        
        tablePromotionals.reloadData()
        if is_month_payment {
            if mAdminPromo.currents >= mAdminPromo.total {
                contentButtonAddPromo.isHidden = true
                lblNoMorePromos.isHidden = false
                lblNoMorePromos.text = "Tu paquete de notificaciones mensuales se ha agotado. Por lo tanto no puedes crear una nueva promoción."
            }else{
                lblNoMorePromos.isHidden = true
                contentButtonAddPromo.isHidden = false
            }
        }else{
            contentButtonAddPromo.isHidden = true
            lblNoMorePromos.isHidden = false
            lblNoMorePromos.text = "Tu pago no ha sido recibido. Por lo tanto no puedes crear, editar o eliminar una promoción."
        }
        
    }
    
    func didFailGetMyPromos(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    //Funcion que me indicara que el llamado al servidor fue exitoso al eliminar una promocion
    func didSuccessDeletePromotional(id: Int) {
        LoadingOverlay.shared.hideOverlayView()
        let list = pmws.upDateDataset(dataset: mListPromos, PromotionalId: id)
        mListPromos = list
        tablePromotionals.reloadData()
        
        if validateIsEmptyPromos() {
            showDialogToDelete(PromotionalId: 0, nameEstab: mNameStab, valueCall: 2, message: "Por el momento tu comercio no será visible para tus clientes, hasta que crees una nueva promoción.", titleButton: "Si", toHome: false, isNews: false)
        }
    }
    
    //Funcion que mostrará mensaje al usuario cuando falle al eliminar una promocion
    func didFailDeletePromotional(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func OnAddNewPromo() {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "AddEditPromo") as! AddEditPromoViewController
        newVC.isFromEdit = false
        newVC.mNameStab = mNameStab
        newVC.mIdEstab = mIdEstab
        newVC.mListImages = mAdminPromo.images
        newVC.isFromPromos = isFromPromos
        newVC.descriptionEstablish = descriptionEstablish
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goToPedidosActuales(_ sender: Any) {
        if isUpdatedVersion {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
            newVC.isFromPromos = isFromPromos
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            self.updateAlert()
        }
    }
    
    func updateAlert() {
        let alert = UIAlertController(title: "¡Actualiza la versión de PickPal!", message: "De lo contrario no podrás accesar a la aplicación.", preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "Actualizar", style: .default, handler: { action in
            if let reviewURL = URL(string: "itms-apps://itunes.apple.com/us/app/apple-store/1378364730?mt=8"), UIApplication.shared.canOpenURL(reviewURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(reviewURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(reviewURL)
                }
            }
        })
        alert.addAction(aceptar)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func goToHome(_ sender: Any) {
        if validateIsEmptyPromos() {
            showDialogToDelete(PromotionalId: 0, nameEstab: mNameStab, valueCall: 1, message: "¿Estás seguro que no quieres dar de alta una promoción?", titleButton: "Si", toHome: true, isNews: false)
        }else{
            closeDialogAndExit(toHome: true)//se cierra la pantalla
        }
    }
    
    func OnErrorAddNew(message: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = "Error"
        newXIB.subTitleMessageString = message
        present(newXIB, animated: true, completion: nil)
    }
    
    func validateIsEmptyPromos() -> Bool{
        var noDeleted = 0 //Indicara el numero de promociones eliminadas
        var headersCount: Int = 0
        
        for promoAdvs in mListPromos{
            if promoAdvs.deleted != nil || promoAdvs.editable != nil{
                if promoAdvs.deleted || !promoAdvs.editable{
                    noDeleted = noDeleted + 1
                }
            }
            
            if promoAdvs.headerAdvs == "Header" || promoAdvs.headerAdvs == "PromoTitle" || promoAdvs.headerAdvs == "NewsTitle" {
                headersCount = headersCount + 1
            }
        }
        
        //Josue Valdez.- Validamos si ya pago el mes actual
        if is_month_payment{
            //Josue Valdez.- Validamos si ya elimino todas sus promociones correspondientes al mes actual
            if noDeleted == mAdminPromo.total {
                return false
            }else{
                //Josue Valdez.- Se le rentan dos indices debido a que siempre se le inserta una cabecera y footer. En estos dos casos todos sus valores van como null
                if (noDeleted + headersCount) == mListPromos.count{
                    return true
                }else{
                    return false
                }
            }
        }else{
            return false
        }
    }
}

extension MyPromotionalsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mListPromos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch mListPromos[indexPath.row].headerAdvs {
        case "Header":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminPromosHeader", for: indexPath) as! AdminPromosHeaderCell
            cell.lblTotalPromos.text = String(mAdminPromo.currents) + "/" + String(mAdminPromo.total)
            cell.lblNameEstablishment.text = mNameStab
            return cell
            
        case "PromoTitle":
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitlePromos", for: indexPath) as! cellTitlePromos
            cell.lblTilePromo.text = "Mis Promociones"
            return cell
        case "Promo":
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminPromosBody", for: indexPath) as! AdminPromosBodyCell
            cell.viewPromotionals.layer.cornerRadius = 10
            cell.viewPromotionals.layer.borderWidth = 1
            cell.isNews = mListPromos[indexPath.row].isNews
            
            if mListPromos[indexPath.row].image != nil && !mListPromos[indexPath.row].image.isEmpty{
//                Nuke.loadImage(with: URL(string: mListPromos[indexPath.row].image)!, into: cell.imgImageSelected)
            }
            
            if mListPromos[indexPath.row].deleted || !is_month_payment{
                cell.pickpalLogoContent.backgroundColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.viewPromotionals.layer.borderColor = UIColor.lightGray.cgColor
                cell.lblNamePromo.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgEditPromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgDeletePromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgDeletePromo.isEnabled = false
                cell.imgEditPromo.isEnabled = false
                cell.lblNamePromo.text = mListPromos[indexPath.row].name
            }else{
                if !mListPromos[indexPath.row].editable {
                    cell.imgEditPromo.isEnabled = false
                    cell.imgDeletePromo.isEnabled = false
                    cell.imgEditPromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.imgDeletePromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.lblNamePromo.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.pickpalLogoContent.backgroundColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.viewPromotionals.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lblNamePromo.text = mListPromos[indexPath.row].name + " - EXP"
                }else{
                    cell.imgEditPromo.isEnabled = true
                    cell.imgDeletePromo.isEnabled = true
                    cell.imgEditPromo.tintColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.lblNamePromo.text = mListPromos[indexPath.row].name
                    
                    cell.lblNamePromo.textColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.imgDeletePromo.tintColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.pickpalLogoContent.backgroundColor = UIColor(red: 0/255, green: 219/255, blue: 109/255, alpha: 1.0)
                    cell.viewPromotionals.layer.borderColor = UIColor.black.cgColor
                }
                
                cell.delegate = self
                cell.id = mListPromos[indexPath.row].id
                cell.nameEstab = mNameStab
                cell.mIndice = indexPath.row
            }
            
            return cell
            
        case "NewsTitle":
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitlePromos", for: indexPath) as! cellTitlePromos
            cell.lblTilePromo.text = "Mis Noticias"
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminPromosBody", for: indexPath) as! AdminPromosBodyCell
            cell.viewPromotionals.layer.cornerRadius = 10
            cell.viewPromotionals.layer.borderWidth = 1
            cell.isNews = mListPromos[indexPath.row].isNews
            cell.imgImageSelected.image = UIImage(named: "imgNewsAmin")
            
            if mListPromos[indexPath.row].deleted || !is_month_payment{
                cell.pickpalLogoContent.backgroundColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.viewPromotionals.layer.borderColor = UIColor.lightGray.cgColor
                cell.lblNamePromo.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgEditPromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgDeletePromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                cell.imgDeletePromo.isEnabled = false
                cell.imgEditPromo.isEnabled = false
                cell.lblNamePromo.text = mListPromos[indexPath.row].name
            }else{
                if !mListPromos[indexPath.row].editable {
                    cell.imgEditPromo.isEnabled = false
                    cell.imgDeletePromo.isEnabled = false
                    cell.imgEditPromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.imgDeletePromo.tintColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.lblNamePromo.textColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.pickpalLogoContent.backgroundColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1.0)
                    cell.viewPromotionals.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lblNamePromo.text = mListPromos[indexPath.row].name + " - EXP"
                }else{
                    cell.imgEditPromo.isEnabled = true
                    cell.imgDeletePromo.isEnabled = true
                    cell.imgEditPromo.tintColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.lblNamePromo.text = mListPromos[indexPath.row].name
                    
                    cell.lblNamePromo.textColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.imgDeletePromo.tintColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1.0)
                    cell.pickpalLogoContent.backgroundColor = UIColor(red: 0/255, green: 219/255, blue: 109/255, alpha: 1.0)
                    cell.viewPromotionals.layer.borderColor = UIColor.black.cgColor
                }
                
                cell.delegate = self
                cell.id = mListPromos[indexPath.row].id
                cell.nameEstab = mNameStab
                cell.mIndice = indexPath.row
            }
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch mListPromos[indexPath.row].headerAdvs {
        case "Header":
            return 80
            
        case "PromoTitle":
            return 44
            
        case "Promo":
            return 70
            
        case "NewsTitle":
            return 44
            
        default:
            return 70
        }
    }
}
