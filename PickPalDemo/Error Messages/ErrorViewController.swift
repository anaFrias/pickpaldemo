//
//  Error400ViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 27/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
@objc protocol errorMessageDelegate{
    @objc optional func popViewController(error: String)
}

class ErrorViewController: UIViewController {

    @IBOutlet weak var titleError: UILabel!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    var errorMessageString: String!
    var subTitleMessageString: String!
    var delegate: errorMessageDelegate?
    var popFlag = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        errorMessage.text = subTitleMessageString
        titleError.text = errorMessageString
        viewContainer.layer.cornerRadius = 5
        viewContainer.clipsToBounds = true
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
//            self.dismiss(animated: true, completion: {
//                if self.popFlag {
//                    self.delegate?.popViewController!()
//                }
//            })
//        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if self.popFlag {
                self.delegate?.popViewController!(error: self.errorMessageString)
            }
        })
//        self.delegate?.dismissModal(view: self.view)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
