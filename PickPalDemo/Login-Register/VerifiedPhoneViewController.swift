//
//  VerifiedPhoneViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class VerifiedPhoneViewController: UIViewController, UITextFieldDelegate, userDelegate {
    
    var client_id: Int!
    var is_register = Bool()
    var userws = UserWS()
    var verifiedPhone = false
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var wrongDoneIcon: UIImageView!
    var defaults = UserDefaults.standard
    var phoneWithoutFormat = String()
    
    @IBOutlet weak var phoneWrongText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        phoneNumber.delegate = self
        phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueAction(_ sender: Any) {
        if phoneNumber.text != "" {
            if verifiedPhone {
                LoadingOverlay.shared.showOverlay(view: self.view)
                userws.validatePhone(client_id: client_id, phone: phoneWithoutFormat)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
                newVC.client_id = client_id
                newVC.is_register = is_register
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            wrongDoneIcon.image = UIImage(named: "wrongIcon")
            wrongDoneIcon.alpha = 1
        }
        
    }
    @IBAction func back(_ sender: Any) {
        let alert = UIAlertController(title: "PickPal", message: "Si regresas ahora se perderán tus datos ¿Deseas contiuar?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
            /*for viewControllers in (self.navigationController?.viewControllers)! {
                if viewControllers.isKind(of: RegisterViewController.self) {
//                    print(true)
                    self.navigationController?.popToViewController(viewControllers, animated: true)
                }
            }*/
//            self.navigationController?.popViewController(animated: true)
            self.navigationController?.popViewController(animated: true)
        } ))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @objc func textFieldDidChange(_ sender: UITextField) {
        wrongDoneIcon.alpha = 0
        phoneWrongText.alpha = 0
        
    }
    
    @IBAction func openKeyboard(_ sender: Any) {
        phoneNumber.becomeFirstResponder()
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let formattedPhoneNumber = Extensions.format(phoneNumber: textField.text!) {
            phoneWithoutFormat = textField.text!
            phoneNumber.text = formattedPhoneNumber
        }
        else {
            wrongDoneIcon.alpha = 1
            wrongDoneIcon.image = UIImage(named: "wrongIcon")
            phoneWrongText.text = "El número celular que estás proporcionando no es válido"
            phoneWrongText.alpha = 1
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        wrongDoneIcon.alpha = 0
        phoneWrongText.alpha = 0
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if phoneWithoutFormat != nil, phoneWithoutFormat != ""{
            if (phoneWithoutFormat.characters.count) == 10 {
                userws.validatePhoneExist(phone: phoneWithoutFormat)
            }else{
                wrongDoneIcon.alpha = 1
                wrongDoneIcon.image = UIImage(named: "wrongIcon")
            }
        }else{
            wrongDoneIcon.alpha = 1
            wrongDoneIcon.image = UIImage(named: "wrongIcon")
        }
    }
    func didSuccessValidatePhone() {
       
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
        newVC.client_id = client_id
        newVC.is_register = is_register
        self.navigationController?.pushViewController(newVC, animated: true)

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10 // Bool
    }
//    custom error message
    func didFailValidatePhone(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessValidatePhoneExist() {
        verifiedPhone = true
        wrongDoneIcon.alpha = 1
        wrongDoneIcon.image = UIImage(named: "correctIcon")
        
    }
    func didFailValidatePhoneExist(title: String, subtitle: String) {
        verifiedPhone = false
        wrongDoneIcon.alpha = 1
        wrongDoneIcon.image = UIImage(named: "wrongIcon")
        phoneWrongText.text = "El número celular ya existe"
        phoneWrongText.alpha = 1
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func termsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
