//
//  MailRegisterViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class MailRegisterViewController: UIViewController, UITextFieldDelegate, userDelegate {
    
//    LayoutConstraints

    @IBOutlet weak var confPasswordBottConstr: NSLayoutConstraint!
    @IBOutlet weak var passwordBoxBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneBoxBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mailBoxBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mailBoxTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mailIconTopConstraint: NSLayoutConstraint!
    
//    TextInput
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var reenterpassword: UITextField!
    
//    Images wrong-done
    @IBOutlet weak var mailCheck: UIImageView!
    @IBOutlet weak var phoneCheck: UIImageView!
    @IBOutlet weak var passwordCheck: UIImageView!
    @IBOutlet weak var reenteredpassCheck: UIImageView!
    
//    Labels passwords
    @IBOutlet weak var notSecurePass: UILabel!
    @IBOutlet weak var nosSecureX: UIImageView!
    
    @IBOutlet weak var wrongPass: UILabel!
    @IBOutlet weak var wrongX: UIImageView!
    
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var cellphoneButton: UIButton!
    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var confirmPassword: UIButton!
    
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var confirmPassView: UIView!
    
    @IBOutlet weak var mailWrongMail: UILabel!
    
    @IBOutlet weak var phoneWrongText: UILabel!
    
    var userws = UserWS()
    var defaults = UserDefaults.standard
    var phoneWithoutFormat = String()
    //validationFlags
    var passFlag = false
    var pass2Flag = false
    var emailFlag = false
    var phoneNumberFlag = false
    override func viewDidLoad() {
//        UIApplication.shared.statusBarView?.backgroundColor = .white
        super.viewDidLoad()
        let screen = UIScreen.main.bounds
        mail.delegate = self
        phone.delegate = self
        password.delegate = self
        reenterpassword.delegate = self
        userws.delegate = self
        
//        mailView.layer.borderColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1).cgColor
//        phoneView.layer.borderColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1).cgColor
//        passView.layer.borderColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1).cgColor
//        confirmPassView.layer.borderColor = UIColor.init(red: 37/255, green: 210/255, blue: 115/255, alpha: 1).cgColor
//        
//        mailView.layer.borderWidth = 1
//        phoneView.layer.borderWidth = 1
//        passView.layer.borderWidth = 1
//        confirmPassView.layer.borderWidth = 1
//
//        mailView.layer.cornerRadius = 6
//        phoneView.layer.cornerRadius = 6
//        passView.layer.cornerRadius = 6
//        confirmPassView.layer.cornerRadius = 6

        if screen.height == 568 {
//            confPasswordBottConstr.constant = 13
//            passwordBoxBottomConstraint.constant = 12
//            phoneBoxBottomConstraint.constant = 12
//            mailBoxBottomConstraint.constant = 12
//            mailBoxTopConstraint.constant = 5
//            mailIconTopConstraint.constant = 5
        }
        mail.addTarget(self, action: #selector(closeIconsMail), for: .editingChanged)
        phone.addTarget(self, action: #selector(closeIconsPhone), for: .editingChanged)
        password.addTarget(self, action: #selector(closeIconsPass), for: .editingChanged)
        reenterpassword.addTarget(self, action: #selector(closeIconsReenterPass), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        UIApplication.shared.statusBarView?.backgroundColor = .clear
        UIApplication.shared.statusBarUIView?.backgroundColor = .clear
    }
    @objc func closeIconsMail(_ sender: UITextField) {
        mailCheck.alpha = 0
        mailWrongMail.alpha = 0
    }
    @objc func closeIconsPhone(_ sender: UITextField) {
        phoneCheck.alpha = 0
        phoneWrongText.alpha = 0
    }
    @objc func closeIconsPass(_ sender: UITextField) {
        passwordCheck.alpha = 0
        notSecurePass.alpha = 0
        nosSecureX.alpha = 0
    }
    @objc func closeIconsReenterPass(_ sender: UITextField) {
        reenteredpassCheck.alpha = 0
        wrongPass.alpha = 0
        wrongX.alpha = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phone {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 10 // Bool
        }else{
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func `continue`(_ sender: UIButton) {
        if emailFlag && phoneNumberFlag && passFlag && pass2Flag {
            LoadingOverlay.shared.showOverlay(view: self.view)
            userws.RegisterEmail(password: password.text!, email: mail.text!, phone: phoneWithoutFormat)
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        if mail.text == "" {
            mailCheck.alpha = 1
        }
        if phone.text == "" {
            phoneCheck.alpha = 1
        }
        if password.text == "" {
            passwordCheck.alpha = 1
        }
        if reenterpassword.text == "" {
            reenteredpassCheck.alpha = 1
        }
        
    }
    
    @IBAction func back(_ sender: Any) {
        if mail.text != "" || phone.text != "" || password.text != "" || reenterpassword.text != "" {
            let alert = UIAlertController(title: "PickPal", message: "¿Estás seguro que deseas salir del registro?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
                for viewControllers in (self.navigationController?.viewControllers)! {
                    if viewControllers.isKind(of: RegisterViewController.self) {
//                        print(true)
                        self.navigationController?.popToViewController(viewControllers, animated: true)
                    }
                }
//                self.navigationController?.popViewController(animated: true)
            } ))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            for viewControllers in (self.navigationController?.viewControllers)! {
                if viewControllers.isKind(of: RegisterViewController.self) {
//                    print(true)
                    self.navigationController?.popToViewController(viewControllers, animated: true)
                }
            }
//            self.navigationController?.popViewController(animated: true)
        }
       
        
    }
    func didSuccessRegister(client_id: Int) {
        LoadingOverlay.shared.hideOverlayView()
        defaults.set(true, forKey: "registro")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
        newVC.client_id = client_id
        newVC.is_login = false
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    func didFailRegister(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == phone {
            phoneWithoutFormat = textField.text!
            if let formattedPhoneNumber = Extensions.format(phoneNumber: textField.text!) {
                textField.text = formattedPhoneNumber
            }else {
                phoneCheck.alpha = 1
                phoneCheck.image = UIImage(named: "wrongIcon")
                phoneWrongText.text = "El número celular proporcionado no es válido"
                phoneWrongText.alpha = 1
                phoneNumberFlag = false
                
            }
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mail {
            if isValidEmail(testStr: mail.text!) {
                userws.ValidateEmail(email: mail.text!)
            }else{
                mailCheck.alpha = 1
                mailCheck.image = UIImage(named: "wrongIcon")
                mailWrongMail.text = "El correo electrónico proporcionado no es válido"
                mailWrongMail.alpha = 1
                emailFlag=false
            }
            
        }
        
        if textField == phone {
            if phoneWithoutFormat.count == 10 {
                userws.validatePhoneExist(phone: phoneWithoutFormat)
            }else{
                phoneCheck.alpha = 1
                phoneCheck.image = UIImage(named: "wrongIcon")
                phoneWrongText.text = "El número celular proporcionado no es válido"
                phoneWrongText.alpha = 1
                phoneNumberFlag = false
            }
        }
        
        if textField == password {
            if isValidPassword(testStr: password.text!) {
                passwordCheck.alpha = 1
                passwordCheck.image = UIImage(named: "correctIcon")
                passFlag = true
            }else{
                if (password.text?.characters.count)! > 7 {
                    passwordCheck.alpha = 1
                    passwordCheck.image = UIImage(named: "wrongIcon")
                    notSecurePass.alpha = 1
                    notSecurePass.text = "Debes agregar al menos una mayúscula y un número"
                    nosSecureX.alpha = 1
                }else{
                    passwordCheck.alpha = 1
                    passwordCheck.image = UIImage(named: "wrongIcon")
                    notSecurePass.text = "La contraseña debe tener 8 caracteres mínimo"
                    notSecurePass.alpha = 1
                    nosSecureX.alpha = 1
                }
                
                passFlag = false
            }
        }
        
        if textField == reenterpassword {
            if reenterpassword.text == "" {
                reenteredpassCheck.alpha = 1
                reenteredpassCheck.image = UIImage(named: "wrongIcon")
            }else{
                if password.text! == reenterpassword.text! {
                    reenteredpassCheck.alpha = 1
                    reenteredpassCheck.image = UIImage(named: "correctIcon")
                    pass2Flag = true
                }else{
                    reenteredpassCheck.alpha = 1
                    reenteredpassCheck.image = UIImage(named: "wrongIcon")
                    wrongPass.alpha = 1
                    wrongX.alpha = 1
                    pass2Flag = false
                }
            }
        }
    }
    
    func didSuccessValidateEmail() {
        mailCheck.alpha = 1
        mailCheck.image = UIImage(named: "correctIcon")
        mailWrongMail.alpha = 0
        emailFlag = true
    }
    func didFailValidateEmail(title: String, subtitle: String) {
        mailCheck.alpha = 1
        mailCheck.image = UIImage(named: "wrongIcon")
        mailWrongMail.text = "El correo electrónico ya existe"
        mailWrongMail.alpha = 1
        emailFlag = false
    }
    
    func didSuccessValidatePhoneExist() {
        phoneCheck.alpha = 1
        phoneCheck.image = UIImage(named: "correctIcon")
        phoneWrongText.alpha = 0
        phoneNumberFlag = true
    }
    func didFailValidatePhoneExist(title: String, subtitle: String) {
        phoneCheck.alpha = 1
        phoneCheck.image = UIImage(named: "wrongIcon")
        phoneWrongText.alpha = 1
        phoneWrongText.text = "El número celular ya existe"
        phoneNumberFlag = false
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
    @IBAction func termsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func infoAction(_ sender: Any) {
        let presentVC = InfoViewController(nibName: "InfoViewController", bundle: nil)
        presentVC.modalTransitionStyle = .crossDissolve
        presentVC.modalPresentationStyle =  .overCurrentContext
        self.present(presentVC, animated: true, completion: nil)
    }
    @IBAction func TouchAreaAction(_ sender: UIButton) {
        if sender == mailButton {
            mail.becomeFirstResponder()
        }else if sender == cellphoneButton {
            phone.becomeFirstResponder()
        }else if sender == passwordButton {
            password.becomeFirstResponder()
        }else{
            reenterpassword.becomeFirstResponder()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
//    override var prefersStatusBarHidden: Bool {
//        return true
//    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

}
