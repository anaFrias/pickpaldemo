//
//  PhoneVerificationViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import CoreLocation

class PhoneVerificationViewController: UIViewController, userDelegate {

    var nameString: String!
    var mailString: String?
    var seconds = 10
    var timer = Timer()
    var userws = UserWS()
    var client_id = Int()
    var is_register = Bool()
    var tries = 1
    var login = false
    var is_login = false
    var defaults = UserDefaults.standard
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var SMSCode: UITextField!
    @IBOutlet weak var SMSCode2: UITextField!
    @IBOutlet weak var SMSCode3: UITextField!
    @IBOutlet weak var SMSCode4: UITextField!
    @IBOutlet weak var wrongCode: UIImageView!
    
    @IBOutlet weak var backVerificationCode: UIButton!
    @IBOutlet weak var secondsLeft: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var resendButton: UIButton!
    
    @IBOutlet weak var grayBackArrowImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        progressBar.alpha = 0
        secondsLeft.alpha = 0
        SMSCode.delegate = self
        SMSCode2.delegate = self
        SMSCode3.delegate = self
        SMSCode4.delegate = self
        grayBackArrowImage.image?.withRenderingMode(.alwaysTemplate)
        grayBackArrowImage.tintColor = UIColor.red
        SMSCode.addTarget(self, action: #selector(PhoneVerificationViewController.codeMode(_:)), for: .editingChanged)
        SMSCode2.addTarget(self, action: #selector(PhoneVerificationViewController.codeMode(_:)), for: .editingChanged)
        SMSCode3.addTarget(self, action: #selector(PhoneVerificationViewController.codeMode(_:)), for: .editingChanged)
        SMSCode4.addTarget(self, action: #selector(PhoneVerificationViewController.codeMode(_:)), for: .editingChanged)

        // Do any additional setup after loading the view.
    }


    @IBAction func backToRoot(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
//            print(controller)
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func reSendVerificationCode(_ sender: Any) {
        tries += 1
        seconds = 10
        progressBar.progress = 0
        secondsLeft.text = timeString(time: TimeInterval(seconds))
        if tries > 3 {
            if is_login {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "LoginMail") as! LoginMailViewController
//                newVC.client_id = client_id
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "wrongCode") as! PhoneWrongCodeViewController
                newVC.client_id = client_id
                
                self.navigationController?.pushViewController(newVC, animated: true)
            }
            
        }else{
            userws.resendCode(client_id: client_id, is_login: 0)
        }
        
        
    }
    
    func didSuccessResendCode() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateProgressBar), userInfo: nil, repeats: true)
        progressBar.alpha = 1
        secondsLeft.alpha = 1
    }
    func didFailResendCode(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    @objc func updateProgressBar() {
        progressBar.progress += 0.1
        seconds -= 1
        if seconds == 0 {
            timer.invalidate()
            resendButton.isEnabled = true
            progressBar.alpha = 0
            secondsLeft.alpha = 0
            
        }else{
            resendButton.isEnabled = false
            secondsLeft.text = timeString(time: TimeInterval(seconds))
        }
        
        
    }

    func timeString(time: TimeInterval) -> String {
        let seconds = Int(time)
        let miliseconds = Int(time)  / 3600
        
        return String(format: "%02i:%02is", seconds, miliseconds)
    }
    @IBAction func continueButton(_ sender: Any) {
//        if SMSCode.text != "" && SMSCode2.text != "" && SMSCode3.text != "" && SMSCode4.text != "" {
//            LoadingOverlay.shared.showOverlay(view: self.view)
//            userws.validate_phone_code(client_id: client_id, code: Int(SMSCode.text! + SMSCode2.text! + SMSCode3.text! + SMSCode4.text!)!, is_login: self.is_login)
//        }
    }
    func didSuccessSendCode(email: String, first_name: String, newsFeed: Int, id: Int, complete_info: Bool) {
        self.wrongCode.alpha = 1
        self.wrongCode.image = UIImage(named: "correctCode")
        UserDefaults.standard.set(id, forKey: "client_id")
        defaults.set(newsFeed, forKey: "last_id_news_feed")
        defaults.set(first_name, forKey: "first_name")
        LoadingOverlay.shared.hideOverlayView()
        if login {
            if complete_info {
//                if locationManager.location == nil {
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//                    self.navigationController?.pushViewController(newVC, animated: true)
//
//                }else {
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)
//                }
            }else{
                defaults.set(true, forKey: "complete_register")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
                newVC.client_id = id
                newVC.is_register = is_register
                self.navigationController?.pushViewController(newVC, animated: true)
            }
            
        }else{
            defaults.set(true, forKey: "complete_register")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
            newVC.client_id = id
            newVC.is_register = is_register
            self.navigationController?.pushViewController(newVC, animated: true)
        }
        
    }
    
    func didFailSendCode(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        self.wrongCode.alpha = 1
        self.wrongCode.image = UIImage(named: "wrongCode")
//        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//        newXIB.modalTransitionStyle = .crossDissolve
//        newXIB.modalPresentationStyle = .overCurrentContext
//        newXIB.errorMessageString = error
//        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func termsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    @objc func codeMode(_ sender: UITextField) {
        if sender == SMSCode {
            SMSCode2.becomeFirstResponder()
        }else if sender == SMSCode2 {
            SMSCode3.becomeFirstResponder()
        }else if sender == SMSCode3 {
            SMSCode4.becomeFirstResponder()
        }else{
//            if SMSCode.text != "" && SMSCode2.text != "" && SMSCode3.text != "" && SMSCode4.text != "" {
//                LoadingOverlay.shared.showOverlay(view: self.view)
//                userws.validate_phone_code(client_id: client_id, code: Int(SMSCode.text! + SMSCode2.text! + SMSCode3.text! + SMSCode4.text!)!, is_login: self.is_login)
//            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
extension PhoneVerificationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength == 1 // Bool
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == SMSCode {
            SMSCode2.becomeFirstResponder()
        }else if textField == SMSCode2 {
            SMSCode3.becomeFirstResponder()
        }else if textField == SMSCode3 {
            SMSCode4.becomeFirstResponder()
        }else{
            if SMSCode.text != "" && SMSCode2.text != "" && SMSCode3.text != "" && SMSCode4.text != "" {
                LoadingOverlay.shared.showOverlay(view: self.view)
                userws.validate_phone_code(client_id: client_id, code: Int(SMSCode.text! + SMSCode2.text! + SMSCode3.text! + SMSCode4.text!)!, is_login: self.is_login)
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
      }
    
}
