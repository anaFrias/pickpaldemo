//
//  TermsViewController.swift
//  PickPal
//
//  Created by Hector Vela on 03/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController, userDelegate {

    @IBOutlet weak var txtTerms: UITextView!
    
    @IBOutlet weak var btn_close: UIButton!
    @IBOutlet weak var gradientView: UIView!
    var userws = UserWS()
    
    
    @IBOutlet weak var viewButtons: UIView!
    @IBOutlet weak var constraintText: NSLayoutConstraint!
    var isRegister = Bool()
    var isSocialMedia = Bool()
    var client_id = Int()
    var hidden_btn_close = true
    override func viewDidLoad() {
        super.viewDidLoad()

        userws.delegate = self
        if isRegister {
            constraintText.constant = 0
            viewButtons.alpha = 1
            gradientView.alpha = 1
        }else{
            constraintText.constant = -89.5
            viewButtons.alpha = 0
            gradientView.alpha = 0
        }
        btn_close.isHidden = hidden_btn_close
        
//        let mask = CAGradientLayer()
//        mask.startPoint = CGPoint(x: 0, y: 5)
//        mask.endPoint = CGPoint(x: 1, y: 0.2)
//        let whiteColor = UIColor.white
//        mask.colors = [whiteColor.withAlphaComponent(0).cgColor, whiteColor.withAlphaComponent(1), whiteColor.withAlphaComponent(1.0).cgColor]
//        mask.locations = [NSNumber(value: 0.0), NSNumber(value: 0.2), NSNumber(value: 1.0)]
//        mask.frame = gradientView.bounds
//        gradientView.layer.mask = mask
//        gradientView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.GetTerms()
    }
    
    @IBAction func aceptar(_ sender: Any) {
        if isSocialMedia {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "PhoneVerified") as! VerifiedPhoneViewController
            newVC.is_register = true
            newVC.client_id = client_id
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MailRegister") as! MailRegisterViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    @IBAction func rechazar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func closeTerms(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        if isRegister {
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func didSuccessGetTerms(terms: String){
        LoadingOverlay.shared.hideOverlayView()
        txtTerms.text = terms
    }
    func didFailGetTerms(title: String, subtitle: String){
        LoadingOverlay.shared.hideOverlayView()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
