//
//  InfoViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 31/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var viewContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainer.layer.cornerRadius = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(10), execute: {
            self.dismiss(animated: true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closePopUp(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
