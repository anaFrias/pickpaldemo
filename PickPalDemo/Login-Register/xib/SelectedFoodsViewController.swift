//
//  SelectedFoodsViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol selectedFoodsProtocol {
    func selectedFoods(foods: [String])
}

class SelectedFoodsViewController: UIViewController, establishmentDelegate, selectedButtonDelegate {

    @IBOutlet weak var seleccionarCategoriasLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPickerFoods: UIView!
    var ws = EstablishmentWS()
    var kitch = [Kitchens]()
    var values = [String]()
    var selectedItms = [Bool]()
    var delegate: selectedFoodsProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "TiposDeComidaTableViewCell", bundle: nil), forCellReuseIdentifier: "tiposComida")
        
        ws.getAllTypeKitchenSingle()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(_:)))
        viewPickerFoods.addGestureRecognizer(gesture)
        viewPickerFoods.isUserInteractionEnabled = true
        
        
        // Do any additional setup after loading the view.
    }
    func didSuccessGetAllTypeSingleKitchen(info: [Kitchens]) {
        kitch = info
        for i in info {
            selectedItms.append(false)
        }
        tableView.reloadData()
    }
    func didFailGetAllTypeSingleKitchen(error: String, subtitle: String) {
        print(error)
    }
    @IBAction func continueSelectFood(_ sender: Any) {
        if values.count >= 3 {
            self.delegate.selectedFoods(foods: values)
            self.dismiss(animated: true, completion: nil)
        }else{
            seleccionarCategoriasLabel.alpha = 1
        }
    }
    
    @objc func wasDragged(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            let translation = gestureRecognizer.translation(in: self.view)
            print(gestureRecognizer.view?.center.y)
            if gestureRecognizer.view!.center.y < (self.view.frame.height*2) {
                gestureRecognizer.view?.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                print("if", (self.view.frame.height*2)/2)
            }else{
//                gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y:554)
                gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y:((self.view.frame.height - 1)*2))
                print("else")
            }
//            gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.view)
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
        }
    }
    func selectedButton(indexPath: IndexPath) {
        tableView(tableView, didSelectRowAt: indexPath)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
extension SelectedFoodsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kitch.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tiposComida", for: indexPath) as! TiposDeComidaTableViewCell
        cell.typeFoodLabel.text = kitch[indexPath.row].name
        cell.indexPath = indexPath
        cell.buttonTypeSelected.isSelected = selectedItms[indexPath.row]
        cell.delegate = self
        return cell
//        let cell = UITableViewCell()
//        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let currentCell = tableView.cellForRow(at: indexPath) as! TiposDeComidaTableViewCell
        seleccionarCategoriasLabel.alpha = 0
        if selectedItms[indexPath.row] {
            let index = values.index(of: kitch[indexPath.row].name )
            values.remove(at:index!)
//            selectedItms[indexPath.row] = false
        }else{
            if values.count < 3 {
                values.append(kitch[indexPath.row].name)
            }else{
                values.removeLast()
                values.append(kitch[indexPath.row].name)
            }
//            selectedItms[indexPath.row] = true
        }
        for i in 0..<kitch.count {
            if values.contains(kitch[i].name) {
                selectedItms[i] = true
            }else{
                selectedItms[i] = false
            }
        }
        tableView.reloadData()
    }
}
