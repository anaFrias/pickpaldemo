//
//  DatePickerViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 26/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol getDate {
    func sendDate ( date: String, age: Int)
}

class DatePickerViewController: UIViewController {

    @IBOutlet weak var viewListo: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    var delegate: getDate?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewListo.layer.borderColor = UIColor.init(red: 151/255, green: 151/255, blue: 151/255, alpha: 1).cgColor
        viewListo.layer.borderWidth = 0.5
        let dates = Date()
        let calendar = Calendar.current
        let yearComponent = calendar.component(.year, from: dates)
        var dateComponents = DateComponents()
        dateComponents.month = 1
        dateComponents.day = 31
        dateComponents.year = yearComponent
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.timeZone = TimeZone(abbreviation: "CST")
        let userCalendar = Calendar.current
        let startDate = userCalendar.date(from: dateComponents)
        let date = calendar.date(byAdding: .year, value: -12, to: startDate!)
        let otrodate = calendar.date(byAdding: .month, value: -1, to: date!)
        datePicker.maximumDate = otrodate
        let local = Locale(identifier: "es_MX")
        datePicker.locale = local
        let age = getAge(dateBirth: otrodate!)
//        UserDefaults.standard.set(age, forKey: "age")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getAge(dateBirth: Date) -> Int {
        let currentDate = Date()
        let form = DateComponentsFormatter()
        form.maximumUnitCount = 2
        form.unitsStyle = .full
        form.allowedUnits = [.year, .month, .day]
        let s = form.string(from: dateBirth, to: currentDate)
        let substring = s!.description.split(separator: " ", maxSplits: 1, omittingEmptySubsequences: true)
        print(substring[0])
        return Int(substring[0].description)!
    }
    @IBAction func dismissPicker(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectedDate(_ sender: Any) {
        let date = "\(datePicker.date)".components(separatedBy: " ")
        let dateString = date[0]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString1 = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let formattedDate = dateFormatter.string(from: dateString1!)
        let age = getAge(dateBirth: datePicker.date)
//        UserDefaults.standard.set(age, forKey: "age")
        self.delegate?.sendDate(date: formattedDate, age: age)
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
