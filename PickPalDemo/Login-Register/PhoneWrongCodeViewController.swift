//
//  PhoneWrongCodeViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 30/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class PhoneWrongCodeViewController: UIViewController, UITextFieldDelegate, userDelegate {
    
    var client_id: Int!
    var is_register = Bool()
    var userws = UserWS()
    var verifiedPhone = false
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var wrongIcon: UIImageView!
    var phoneWithoutFormat = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        phoneField.delegate = self
        phoneField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backView(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            print(controller)
            if controller.isKind(of: ViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func touchField(_ sender: Any) {
        phoneField.becomeFirstResponder()
    }
    @IBAction func continueSendCode(_ sender: Any) {
        if phoneField.text == "" {
            self.wrongIcon.alpha = 1
            wrongIcon.image = UIImage(named: "wrongIcon")
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }else{
            if verifiedPhone {
                LoadingOverlay.shared.showOverlay(view: self.view)
                userws.validatePhone(client_id: client_id, phone: phoneWithoutFormat)
            }
        }
        /*else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }*/
    }
    @objc func textFieldDidChange(_ sender: UITextField) {
        wrongIcon.alpha = 0
        
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let formattedPhoneNumber = Extensions.format(phoneNumber: textField.text!) {
            phoneWithoutFormat = textField.text!
            phoneField.text = formattedPhoneNumber
        }
        else {
            wrongIcon.alpha = 1
            wrongIcon.image = UIImage(named: "wrongIcon")
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if phoneWithoutFormat != nil, phoneWithoutFormat != ""{
            if (phoneWithoutFormat.characters.count) == 10 {
                userws.validatePhoneExist(phone: phoneWithoutFormat)
            }else{
                wrongIcon.alpha = 1
                wrongIcon.image = UIImage(named: "wrongIcon")
            }
        }
        
    }
    func didSuccessValidatePhone() {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
        newVC.is_register = is_register
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    //    custom error message
    func didFailValidatePhone(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didSuccessValidatePhoneExist() {
        verifiedPhone = true
        wrongIcon.alpha = 1
        wrongIcon.image = UIImage(named: "correctIcon")
        
    }
    func didFailValidatePhoneExist(title: String, subtitle: String) {
        verifiedPhone = false
        wrongIcon.alpha = 1
        wrongIcon.image = UIImage(named: "wrongIcon")
    }

    
    @IBAction func termsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
