//
//  inputProfileTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol getInfoProtocol {
    func sendInfoProfile(isEmail: Bool, value: String)
}
class inputProfileTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var checkInput: UIImageView!
    @IBOutlet weak var inputText: UITextField!
    
    @IBOutlet weak var viewInput: UIView!
    var isEmail = false
    var delegate: getInfoProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewInput.layer.cornerRadius = 6
        inputText.delegate = self
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if isEmail {
            if textField.text != "" {
                if isValidEmail(testStr: textField.text!) {
                    checkInput.image = UIImage(named: "correctIcon")
                    checkInput.alpha = 1
                }else{
                    checkInput.alpha = 1
                    checkInput.image = UIImage(named: "wrongIcon")
                }
            }else{
                checkInput.alpha = 1
                checkInput.image = UIImage(named: "wrongIcon")
            }
            
        }else{
            if textField.text != ""{
                checkInput.alpha = 1
                checkInput.image = UIImage(named: "correctIcon")
            }else{
                checkInput.alpha = 1
                checkInput.image = UIImage(named: "wrongIcon")
            }
        }
        self.delegate.sendInfoProfile(isEmail: isEmail, value: textField.text!)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        checkInput.alpha = 0
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
