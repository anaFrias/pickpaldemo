//
//  selectKindOfFoodTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol showMealsProtocol {
    func showMeals()
}

class selectKindOfFoodTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var comidaText: UITextField!
    @IBOutlet weak var comidaView: UIView!
    @IBOutlet weak var checkInfo: UIImageView!
    
    var delegate: showMealsProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        comidaView.layer.cornerRadius = 6
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            checkInfo.alpha = 1
            checkInfo.image = UIImage(named: "correctIcon")
        }
        
    }
    @IBAction func showFood(_ sender: Any) {
        self.delegate.showMeals()
    }
    
}

