//
//  birthdayInputTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol genderBirthdayProtocol {
    func openGenderOptions(indexPath: IndexPath)
    func femSelection(indexPath: IndexPath)
    func mascSelection(indexPath: IndexPath)
    func showBirthday()
}
class birthdayInputTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var genderView: UIView!
    
    @IBOutlet weak var dropdownBtn: UIButton!
    @IBOutlet weak var genderText: UITextField!
    
    @IBOutlet weak var check2: UIImageView!
    @IBOutlet weak var check1: UIImageView!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    @IBOutlet weak var viewBtnGender: UIView!
    var indexPath = IndexPath()
    var delegate: genderBirthdayProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        genderView.layer.cornerRadius = 6
        birthdayView.layer.cornerRadius = 6
        genderText.delegate = self
    }
    
    @IBAction func mascBtn(_ sender: Any) {
        check1.alpha = 1
        self.delegate.mascSelection(indexPath: indexPath)
    }
    @IBAction func femBtn(_ sender: Any) {
        check1.alpha = 1
        self.delegate.femSelection(indexPath: indexPath)
    }
    @IBAction func showBirthday(_ sender: Any) {
        //        check2.alpha = 1
        self.delegate.showBirthday()
    }
    @IBAction func dropdown(_ sender: Any) {
        check1.alpha = 0
        self.delegate.openGenderOptions(indexPath: indexPath)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        check1.alpha = 1
        if textField.text != "" {
            check1.image = UIImage(named: "correctIcon")
        }else{
            check1.image = UIImage(named: "wrongIcon")
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        check1.alpha = 0
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
