//
//  sendInfoCompleteProfileTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol sendInfoProtocol {
    func sendInfoProfileButton()
}

class sendInfoCompleteProfileTableViewCell: UITableViewCell {

    var delegate: sendInfoProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func sendInfoButton(_ sender: Any) {
        self.delegate.sendInfoProfileButton()
    }
    

}
