//
//  zipCodeInputTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit

protocol zipCodeProtocol {
    func sendZipInfo(value: String)
}
class zipCodeInputTableViewCell: UITableViewCell, UITextFieldDelegate {
    

    @IBOutlet weak var postalText: UITextField!
    @IBOutlet weak var viewZP: UIView!
    var delegate: zipCodeProtocol!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        postalText.delegate = self
        viewZP.layer.cornerRadius = 6
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate.sendZipInfo(value: postalText.text!)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 5 // Bool
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
