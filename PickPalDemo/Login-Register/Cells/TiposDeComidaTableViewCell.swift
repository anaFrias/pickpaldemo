//
//  TiposDeComidaTableViewCell.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 1/2/19.
//  Copyright © 2019 Innovation Workshop. All rights reserved.
//

import UIKit
protocol selectedButtonDelegate {
    func selectedButton(indexPath: IndexPath)
}

class TiposDeComidaTableViewCell: UITableViewCell {

    @IBOutlet weak var typeFoodLabel: UILabel!
    @IBOutlet weak var buttonTypeSelected: UIButton!
    var indexPath = IndexPath()
    var delegate: selectedButtonDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func selectUnselect(_ sender: Any) {
        self.delegate.selectedButton(indexPath: indexPath)
//        if buttonTypeSelected.isSelected {
//            buttonTypeSelected.isSelected = false
//        }else{
//            buttonTypeSelected.isSelected = true
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
