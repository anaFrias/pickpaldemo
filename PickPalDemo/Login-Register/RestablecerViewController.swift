//
//  RestablecerViewController.swift
//  PickPal
//
//  Created by Alan Abundis on 05/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class RestablecerViewController: UIViewController, userDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var checkEmail: UIImageView!
    
    var userWs = UserWS()
    override func viewDidLoad() {
        super.viewDidLoad()
        userWs.delegate = self
        mail.delegate = self
        self.navigationController?.isNavigationBarHidden = true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            checkEmail.alpha = 1
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        checkEmail.alpha = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSuccessResetPassword() {
        
        let okAction = UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default) {
            UIAlertAction in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginMail") as! LoginMailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let alert = UIAlertController(title: "PickPal", message: "Te hemos enviado un correo para restablecer tu contraseña", preferredStyle: .alert)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func didFailResetPassword(title: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    @IBAction func touchField(_ sender: Any) {
        mail.becomeFirstResponder()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func `continue`(_ sender: Any) {
        if mail.text != ""{
            let mailwithoutSpaces = mail.text?.components(separatedBy: .whitespaces).joined()
            userWs.ResetPassword(email: mailwithoutSpaces!)
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            checkEmail.alpha = 1
        }
    }
    
    @IBAction func terms(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
