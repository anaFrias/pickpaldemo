//
//  LoginPhoneViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import UserNotifications
import OneSignal

class LoginPhoneViewController: UIViewController, userDelegate, UITextFieldDelegate {

    @IBOutlet weak var phoneField: UITextField!
    var userws = UserWS()
    var defaults = UserDefaults.standard
    var phoneWithoutFormat = String()
    @IBOutlet weak var phoneCheck: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        phoneField.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func restableceContra(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Restablecer") as! RestablecerViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func logIn(_ sender: Any) {
        let newString = phoneWithoutFormat.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
        let otherString = newString.replacingOccurrences(of: "(", with: "", options: .literal, range: nil)
        let otherOneString = otherString.replacingOccurrences(of: ")", with: "", options: .literal, range: nil)
        print(otherOneString)  
        if (otherOneString.count) > 9 {
            LoadingOverlay.shared.showOverlay(view: self.view)
            userws.LoginPhone(phone: phoneWithoutFormat)
        }
        if phoneField.text == "" {
            phoneCheck.alpha = 1
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.text = ""
        phoneCheck.alpha = 0
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let formattedPhoneNumber = Extensions.format(phoneNumber: textField.text!) {
            phoneWithoutFormat = textField.text!
            textField.text = formattedPhoneNumber
        }
        else {
            phoneCheck.alpha = 1
            print("phone nil")
        }
        return true
    }
    
    @IBAction func openKey(_ sender: Any) {
        phoneField.becomeFirstResponder()
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10 // Bool
    }
    func didSuccessLogin(is_register: Bool, client_id: Int, complete_info: Bool, phone: Bool, code: Bool) {
        LoadingOverlay.shared.hideOverlayView()
        defaults.set(is_register, forKey: "is_register")
        defaults.removeObject(forKey: "login")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "phoneVerification") as! PhoneVerificationViewController
        newVC.client_id = client_id
        newVC.is_register = is_register
        newVC.login = true
        newVC.is_login = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailLogin(title: String, statusCode: Int, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.subTitleMessageString = subtitle
        newXIB.errorMessageString = title
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func loginWithMail(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LoginMail") as! LoginMailViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func termsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
