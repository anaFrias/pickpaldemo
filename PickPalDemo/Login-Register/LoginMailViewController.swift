//
//  LoginMailViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 03/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import UserNotifications
import OneSignal
import CoreLocation

class LoginMailViewController: UIViewController, userDelegate, UITextFieldDelegate {

    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var buttonMail: UIButton!
    @IBOutlet weak var buttonPass: UIButton!
    
    @IBOutlet weak var wrongPass: UIImageView!
    @IBOutlet weak var wrongMail: UIImageView!
    
    @IBOutlet weak var showPassBtn: UIButton!
    var defaults = UserDefaults.standard
    var userws = UserWS()
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        userws.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        mailField.delegate = self
        passField.delegate = self
        // Do any additional setup after loading the view.
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == mailField {
            if textField.text == "" {
                wrongMail.alpha = 1
            }
        }else{
            if textField.text == "" {
                wrongPass.alpha = 1
            }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mailField {
            wrongMail.alpha = 0
        }else{
            wrongPass.alpha = 0
        }
    }
    @IBAction func tuchFields(_ sender: UIButton) {
        if sender == buttonMail {
            mailField.becomeFirstResponder()
        }else{
            passField.becomeFirstResponder()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showPass(_ sender: Any) {
        if showPassBtn.isSelected {
            passField.isSecureTextEntry = true
            showPassBtn.isSelected = false
        }else{
            passField.isSecureTextEntry = false
            showPassBtn.isSelected = true
        }
        
        
    }
    @IBAction func loginButton(_ sender: Any) {
        if passField.text != "" && mailField.text != "" {
            if isValidEmail(testStr: mailField.text!) {
                LoadingOverlay.shared.showOverlay(view: self.view)
                userws.LoginWithMail(email: mailField.text!, password: passField.text!)
            }else{
                wrongMail.alpha = 1
            }
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        if passField.text == "" {
            wrongMail.alpha = 1
        }
        if mailField.text == "" {
            wrongPass.alpha = 1
        }
    }
    @IBAction func back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
//    func didSuccessLogin(is_register: Bool, client_id: Int, complete_info: Bool, phone: Bool, code: Bool) {
//        LoadingOverlay.shared.hideOverlayView()
//        defaults.set(client_id, forKey: "client_id")
//        if complete_info {
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
//            self.navigationController?.pushViewController(newVC, animated: true)
//
//        }else{
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
//            newVC.client_id = client_id
////            newVC.is_register = is_register
//            self.navigationController?.pushViewController(newVC, animated: true)
//
//        }
//    }
//    func didFailLogin(title: String, statusCode: Int, subtitle: String) {
//        LoadingOverlay.shared.hideOverlayView()
//        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
//        newXIB.modalTransitionStyle = .crossDissolve
//        newXIB.modalPresentationStyle = .overCurrentContext
//        newXIB.subTitleMessageString = subtitle
//        newXIB.errorMessageString = title
//        present(newXIB, animated: true, completion: nil)
//    }
    func didSuccessLoginWithMail(avatar: String, email: String, first_name: String, id: Int, news_feed: Int) {
        defaults.set(first_name, forKey: "first_name")
        defaults.set(id, forKey: "client_id")
        defaults.set(news_feed, forKey: "last_id_news_feed")
        defaults.set(avatar, forKey: "avatar")
        UserDefaults.standard.set(false, forKey: "invited")
//        if locationManager.location == nil {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//            self.navigationController?.pushViewController(newVC, animated: true)
//
//        }else {
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
            
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)

//        }

    }
    func didSuccessLoginWithMailTwo(avatar: String, email: String, first_name: String, id: Int, news_feed: Int, is_register: Bool, complete_info: Bool, age: Int) {
        defaults.set(first_name, forKey: "first_name")
        defaults.set(id, forKey: "client_id")
        defaults.set(news_feed, forKey: "last_id_news_feed")
        defaults.set(avatar, forKey: "avatar")
        UserDefaults.standard.set(is_register, forKey: "is_register")
        UserDefaults.standard.set(!complete_info, forKey: "complete_register")
        UserDefaults.standard.set(false, forKey: "invited")
        UserDefaults.standard.set(age, forKey: "age")
        if !complete_info {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
            newVC.client_id = id
            self.navigationController?.pushViewController(newVC, animated: true)
        }else{
//            if locationManager.location == nil {
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//                self.navigationController?.pushViewController(newVC, animated: true)
//
//            }else {
                /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                self.navigationController?.pushViewController(newVC, animated: true)*/
                let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
                self.navigationController?.pushViewController(newVC, animated: true)
//            }
        }
    }
    func didFailLoginWithMail(title: String, statusCode: Int, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.subTitleMessageString = subtitle
        newXIB.errorMessageString = title
        present(newXIB, animated: true, completion: nil)

    }
    @IBAction func termsAndConditions(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func recuperar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Restablecer") as! RestablecerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
