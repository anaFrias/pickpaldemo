//
//  GPSViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 03/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import CoreLocation

class GPSViewController: UIViewController, CLLocationManagerDelegate {

    var managerLocation = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        managerLocation.requestAlwaysAuthorization()
 
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let authorizationStatus = CLLocationManager.authorizationStatus()

        if (authorizationStatus == CLAuthorizationStatus.denied) {
            getLocation()
        }else{
            
            managerLocation.requestWhenInUseAuthorization()
            managerLocation.delegate = self
            managerLocation.desiredAccuracy = kCLLocationAccuracyBest
            self.navigationController?.isNavigationBarHidden = true
           
        }
        
    }
    

    func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                let alertController = UIAlertController (title: "¡Oh no!", message: "¡Necesitamos tu ubicación para encontrar los comercios cercanos a ti!", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Configuración", style: .cancel) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                                
                                self.goToMenu()
                            
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (_) -> Void in
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                    self.goToMenu()
                }
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
            }
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                if CLLocationManager.authorizationStatus() == .denied  {
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                    self.goToMenu()
                }
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("autorizado")
            }
            
        } else {
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
            self.goToMenu()
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("autorizado")
            manager.requestAlwaysAuthorization()
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
            self.goToMenu()
            break
        case .denied, .notDetermined, .restricted:
            print("denegado")
            self.goToMenu()
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goToMenu(){
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
