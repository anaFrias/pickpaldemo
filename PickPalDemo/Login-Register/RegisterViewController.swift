//
//  RegisterViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 12/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import UserNotifications
import OneSignal
import AuthenticationServices
class RegisterViewController: UIViewController, WebLoginControllerDelegate, userDelegate, errorMessageDelegate {
    var userws = UserWS()
    var first_name = String()
    var last_name = String()
    var email = String()
    var defaults = UserDefaults.standard
    let appleProvider = AppleSignInClient()
    var nameIconApple = "AppleRegistro"
    
    @IBOutlet weak var containerRegisterApple: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        userws.delegate = self
        if #available(iOS 13.0, *) {
            setupSOAppleSignIn()
            containerRegisterApple.isHidden = false
        }else{
           
            containerRegisterApple.isHidden = true
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func fbLogin(_ sender: Any) {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in

            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                print(result)
                if fbloginresult.isCancelled{
                    print ("cancel")
                }else{
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }else{
                        print ("Error")
                    }
                }
            }else{
                print(error)
                LoginManager().logOut()
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "birthday, id, name, first_name, last_name, picture.type(large), email, age_range"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    let resultDic = result as! NSDictionary
                    let token: String = AccessToken.current?.tokenString ?? ""
                    self.first_name = resultDic.value(forKey: "first_name") as! String
                    self.defaults.set(self.first_name, forKey: "first_name")
                    self.last_name =  resultDic.value(forKey: "last_name") as! String
                    if let email = resultDic.value(forKey: "email") as? String {
                        self.email = email
                    }else{
                        self.email = ""
                    }
                    LoadingOverlay.shared.showOverlay(view: self.view)
                    self.userws.login(email: self.email, first_name: self.first_name , last_name: self.last_name, token: token, id: resultDic.value(forKey: "id") as! String, record_medium: 2)
                }else{
                    print(error!)
                    debugPrint(error!)
                }
            })
        }
    }
    @IBAction func InstagramRegister(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Instagram") as! LoginInstagramWVViewController
        newVC.delegate = self
        newVC.modalPresentationStyle = .pageSheet
        
        self.navigationController?.present(newVC, animated: true, completion: nil)
        
    }
    func webLoginController(didFinishLogin userDict: NSDictionary) {
        
        if userDict.count > 0 {
            let str = (userDict.value(forKey: "user") as! NSDictionary).value(forKey: "full_name") as! String
            if let range: Range<String.Index> = str.range(of: " ") {
                last_name = str.substring(from: range.upperBound)
                first_name = str.substring(to: range.upperBound)
            }else{
                last_name = ""
                first_name = str
            }
            
            
            
            defaults.set(first_name, forKey: "first_name")
            LoadingOverlay.shared.showOverlay(view: self.view)
            self.userws.login(email: "", first_name: first_name, last_name: last_name, token: userDict.value(forKey: "access_token") as! String, id: (userDict.value(forKey: "user") as! NSDictionary).value(forKey: "id") as! String, record_medium: 3)
//            print(last_name, first_name)
        }
    }
    func didSuccessLogin(is_register: Bool, client_id: Int, complete_info: Bool, phone: Bool, code: Bool) {
        LoadingOverlay.shared.hideOverlayView()
        defaults.set(is_register, forKey: "is_register")
        UserDefaults.standard.set(false, forKey: "invited")
        if is_register {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "PhoneVerified") as! VerifiedPhoneViewController
//            newVC.is_register = true
//            newVC.client_id = client_id
//            self.navigationController?.pushViewController(newVC, animated: true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
            vc.isRegister = true
            vc.isSocialMedia = true
            vc.client_id = client_id
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            defaults.set(client_id, forKey: "client_id")
            if complete_info {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                newVC.showSplash = false
                self.navigationController?.pushViewController(newVC, animated: true)
                
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "CompleteRegister") as! CompleteProfileViewController
                newVC.client_id = client_id
                //            newVC.is_register = is_register
                self.navigationController?.pushViewController(newVC, animated: true)
            }
        }
        
        
    }
    func dismissModal(view: UIView) {
        view.removeFromSuperview()
    }
    func didFailLogin(title: String, statusCode: Int, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
////        if statusCode == 401 {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "PhoneVerified") as! VerifiedPhoneViewController
//            newVC.client_id = 14
//            self.navigationController?.pushViewController(newVC, animated: true)
//        }else{
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.subTitleMessageString = subtitle
        newXIB.errorMessageString = title
        present(newXIB, animated: true, completion: nil)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func RegisterButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.isRegister = true
        self.navigationController?.pushViewController(vc, animated: true)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let newVC = storyboard.instantiateViewController(withIdentifier: "MailRegister") as! MailRegisterViewController
//        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func logIn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreenViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func termsAndConditions(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
//        vc.modalPresentationStyle = .popover
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        vc.hidden_btn_close = false
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension RegisterViewController{
    
    @available(iOS 13.0, *)
    func setupSOAppleSignIn() {
        let customAppleLoginBtn = UIButton()
        customAppleLoginBtn.backgroundColor = .clear
        customAppleLoginBtn.setBackgroundImage(UIImage(named: nameIconApple), for: .normal)
        //customAppleLoginBtn.setImage(UIImage(named: nameIconApple), for: .normal)
        
//          self.containerRegisterApple.layer.borderWidth = 2
//          self.containerRegisterApple.layer.borderColor = UIColor.black.cgColor
//          self.containerRegisterApple.layer.cornerRadius = 10
        customAppleLoginBtn.addTarget(self, action: #selector(SignInWithAppleAction(sender:)), for: .touchUpInside)
        self.containerRegisterApple.addSubview(customAppleLoginBtn)
        // Setup Layout Constraints to be in the center of the screen
        customAppleLoginBtn.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            customAppleLoginBtn.centerXAnchor.constraint(equalTo: self.containerRegisterApple.centerXAnchor),
            customAppleLoginBtn.centerYAnchor.constraint(equalTo: self.containerRegisterApple.centerYAnchor),
            customAppleLoginBtn.widthAnchor.constraint(equalToConstant: 290),
            customAppleLoginBtn.heightAnchor.constraint(equalToConstant: 55)
            ])
       
        
    }
    
    @available(iOS 13.0, *)
    @objc
    func SignInWithAppleAction(sender: ASAuthorizationAppleIDButton)  {
        appleProvider.handleAppleIdRequest(block: { fullName, email, token in
            // receive data in login class.
            print(fullName ?? "")
            print(email ?? "")
            print(token ?? "")
            
            var mail = ""
            if email != nil{
                mail = email ?? ""
            }
            
            LoadingOverlay.shared.showOverlay(view: self.view)
            UserDefaults.standard.set(fullName, forKey: "first_name")
            self.userws.login(email: mail, first_name: fullName ?? "" , last_name:"", token: token?.trunc(length: 250) ?? "", id: "Appel_email", record_medium: 9)
        })
        
    }
    
}

extension String {
    
  func trunc(length: Int, trailing: String = "…") -> String {
    return (self.count > length) ? self.prefix(length) + trailing : self
  }
    
}
