//
//  CompleteProfileViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CoreLocation

class CompleteProfileViewController: UIViewController, getDate, userDelegate,  UITextFieldDelegate, genderBirthdayProtocol, showMealsProtocol, selectedFoodsProtocol, getInfoProtocol, zipCodeProtocol, sendInfoProtocol {
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var mailView: UIView!
    @IBOutlet weak var genreView: UIView!
    @IBOutlet weak var birthdayView: UIView!
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var mailButton: UIButton!
    @IBOutlet weak var zipButton: UIButton!
    
    @IBOutlet weak var wrongName: UIImageView!
    @IBOutlet weak var wrongBirthday: UIImageView!
    @IBOutlet weak var wrongGender: UIImageView!
    
    //Constraints
    @IBOutlet weak var nameBottomConst: NSLayoutConstraint!
    @IBOutlet weak var mailBottomConstToGenre: NSLayoutConstraint!
    @IBOutlet weak var mailBottomConstBirth: NSLayoutConstraint!
    @IBOutlet var genreBottomConst: NSLayoutConstraint!
    @IBOutlet weak var zipBottomConstr: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var femHeight: NSLayoutConstraint!
    @IBOutlet weak var mascHeight: NSLayoutConstraint!
    
    //    Inputs
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var zipCode: UITextField!
    //    Buttons
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var Female: UIButton!
    
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var indicatorIcons: UIImageView!
    
    @IBOutlet weak var correctNameIndicator: UIImageView!
    
    @IBOutlet weak var mailWrongText: UILabel!
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var tableView: UITableView!
    //    Strings
    var gender = String()
    var client_id: Int!
    var is_register = Bool()
    var defaults = UserDefaults.standard
    var userws = UserWS()
    var phone = String()
    var correcEmail = Bool()
    var showGenderFlag = false
    var profile = UserProfile()
    var genderDropdownHeight = CGFloat(72)
    var isCorrectEmail  = Bool()
    var favoriteMeals = String()
    var showWrongInfo = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let screen = UIScreen.main.bounds
        self.viewHeight.constant = 0
        self.femHeight.constant = 0
        self.mascHeight.constant = 0
        self.male.alpha = 0
        self.Female.alpha = 0
        zipCode.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        indicatorIcons.alpha = 0
        mailWrongText.alpha = 0
        mail.delegate = self
        name.delegate = self
        //        For iPhone SE, 5, 5s
        if screen.height == 568 {
            nameView.frame = CGRect(x: nameView.frame.origin.x, y: nameView.frame.origin.y, width: nameView.frame.size.width, height: 37)
            mailView.frame = CGRect(x: mailView.frame.origin.x, y: mailView.frame.origin.y, width: mailView.frame.size.width, height: 37)
            genreView.frame = CGRect(x: genreView.frame.origin.x, y: genreView.frame.origin.y, width: genreView.frame.size.width, height: 37)
            birthdayView.frame = CGRect(x: birthdayView.frame.origin.x, y: birthdayView.frame.origin.y, width: birthdayView.frame.size.width, height: 37)
            zipView.frame = CGRect(x: zipView.frame.origin.x, y: zipView.frame.origin.y, width: zipView.frame.size.width, height: 37)
            loginButton.frame = CGRect(x: loginButton.frame.origin.x, y: loginButton.frame.origin.y, width: loginButton.frame.size.width, height: 37)
            
            nameBottomConst.constant = 5
            mailBottomConstToGenre.constant = 5
            mailBottomConstBirth.constant = 5
            genreBottomConst.constant = 5
            zipBottomConstr.constant = 15
        }
        mail.addTarget(self, action: #selector(hideWrongIcon(_:)), for: .editingChanged)
        name.addTarget(self, action: #selector(hideWrongIcon(_:)), for: .editingChanged)
        userws.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        userws.viewProfile(client_id: client_id)
    }
    
    func selectedFoods(foods: [String]) {
        //        print(foods)
        favoriteMeals = "\(foods[0]), \(foods[1]), \(foods[2])"
        tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
    }
    @objc func hideWrongIcon(_ sender: UITextField) {
        if sender == mail {
            indicatorIcons.alpha = 0
            //            mailWrongText.alpha = 0
            
        }else{
            wrongName.alpha = 0
        }
        
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        mailWrongText.alpha = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func didSuccessViewProfile(profile: UserProfile) {
        //        if profile.phone != nil {
        //            self.phone = profile.phone!
        //        }
        LoadingOverlay.shared.hideOverlayView()
        //        if profile.birthday != nil, profile.birthday != "" {
        //            birthday.text = profile.birthday
        //            birthday.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
        //        }
        //        if profile.email != nil, profile.email != "" {
        //            mail.text = profile.email
        //            correcEmail = true
        //            mail.isUserInteractionEnabled = false
        //            mailButton.isEnabled = false
        //        }
        //        if profile.genre != nil, profile.genre != "" {
        //            genre.text = profile.genre
        //            genre.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
        //        }
        //        if profile.name != nil, profile.name != "" {
        //            name.text = profile.name
        //        }
        //        if profile.zipCode != nil {
        //            zipCode.text = "\(profile.zipCode!)"
        //        }
        self.profile = profile
        tableView.reloadData()
    }
    func openGenderOptions(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as? birthdayInputTableViewCell
        if showGenderFlag {
            tableView.beginUpdates()
            genderDropdownHeight = 72
            tableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                currentCell?.dropdownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                currentCell?.viewBtnGender.alpha = 0
            }) { (finished) in
                self.showGenderFlag = false
            }
        }else{
            tableView.beginUpdates()
            genderDropdownHeight = 99
            tableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                currentCell?.dropdownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                currentCell?.viewBtnGender.alpha = 1
            }) { (finished) in
                self.showGenderFlag = true
            }
        }
    }
    func femSelection(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as? birthdayInputTableViewCell
        tableView.beginUpdates()
        genderDropdownHeight = 72
        tableView.endUpdates()
        UIView.animate(withDuration: 0.5, animations: {
            currentCell?.dropdownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            currentCell?.viewBtnGender.alpha = 0
            self.profile.genre = "F"
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
        }) { (finished) in
            self.showGenderFlag = false
        }
        
    }
    func mascSelection(indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as? birthdayInputTableViewCell
        tableView.beginUpdates()
        genderDropdownHeight = 72
        tableView.endUpdates()
        UIView.animate(withDuration: 0.5, animations: {
            currentCell?.dropdownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            currentCell?.viewBtnGender.alpha = 0
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .automatic)
            self.profile.genre = "M"
        }) { (finished) in
            self.showGenderFlag = false
        }
        
    }
    func showBirthday() {
        let datePicker = DatePickerViewController(nibName: "DatePickerViewController", bundle: nil)
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.modalTransitionStyle = .coverVertical
        datePicker.delegate = self
        self.present(datePicker, animated: true, completion: nil)
    }
    func showMeals() {
        let viewController = SelectedFoodsViewController(nibName: "SelectedFoodsViewController", bundle: nil)
        viewController.modalPresentationStyle = .overCurrentContext
        viewController.modalTransitionStyle = .coverVertical
        viewController.delegate = self
        self.present(viewController, animated: true, completion: nil)
    }
    @IBAction func genreButton(_ sender: Any) {
        if showGenderFlag {
            UIView.animate(withDuration: 0.5, animations: {
                self.viewHeight.constant = 0
                self.femHeight.constant = 0
                self.mascHeight.constant = 0
                self.male.alpha = 0
                self.Female.alpha = 0
            }) { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(0))
                    self.showGenderFlag = false
                    if self.genre.text == "Sexo *" {
                        self.wrongGender.alpha = 1
                    }
                })
            }
        }else{
            self.wrongGender.alpha = 0
            UIView.animate(withDuration: 0.5, animations: {
                self.viewHeight.constant = 52
                self.femHeight.constant = 26
                self.mascHeight.constant = 26
                self.male.alpha = 1
                self.Female.alpha = 1
            }) { (finished) in
                UIView.animate(withDuration: 0.2, animations: {
                    self.downArrow.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                    self.showGenderFlag = true
                    
                })
            }
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "", textField.text != nil {
            if textField == mail {
                if isValidEmail(testStr: mail.text!) {
                    
                    userws.ValidateEmail(email: mail.text!)
                }else{
                    mailWrongText.text = "El correo electrónico proporcionado no es válido"
                    mailWrongText.alpha = 1
                    
                }
            }
            wrongName.alpha = 0
            //            mailWrongText.alpha = 0
        }else{
            if textField == mail {
                indicatorIcons.alpha = 1
                indicatorIcons.image = UIImage(named: "wrongIcon")
                
            }else if textField == name {
                wrongName.alpha = 1
                indicatorIcons.image = UIImage(named: "wrongIcon")
            }
        }
        
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == zipCode {
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 5 // Bool
        }else{
            return true
        }
        
    }
    func sendInfoProfile(isEmail: Bool, value: String) {
        if isEmail {
            profile.email = value
            userws.ValidateEmail(email: value)
        }else{
            profile.name = value
        }
        
    }
    func sendInfoProfileButton () {
        if profile.name != "", profile.email != "", profile.birthday != "", profile.genre != "", favoriteMeals != "", favoriteMeals != nil {
            let str = profile.name
            LoadingOverlay.shared.showOverlay(view: self.view)
            var isRegister = Int()
            if is_register {
                isRegister = 1
            }else{
                isRegister = 2
            }
            if let range: Range<String.Index> = str!.range(of: " ") {
                let last_name = str!.substring(from: range.upperBound)
                let substring_fn = str!.substring(to: range.upperBound)
                let first_name = substring_fn.trimmingCharacters(in: .whitespaces)
                defaults.set(first_name, forKey: "first_name")
                //                if isCorrectEmail {
                userws.updateProfile(client_id: client_id, first_name: first_name, last_name: last_name, gender: profile.genre!, birth_date: profile.birthday!, zip_code: "\(profile.zipCode ?? 0)", email: profile.email!, is_register: isRegister, food_preference: favoriteMeals)
                //                }
                
            }else{
                defaults.set(profile.name, forKey: "first_name")
                //                if isCorrectEmail {
                userws.updateProfile(client_id: client_id, first_name: profile.name, last_name: "", gender: profile.genre!, birth_date: profile.birthday!, zip_code: "\(profile.zipCode ?? 0)", email: profile.email!, is_register: isRegister, food_preference: favoriteMeals)
                //                }
                
            }
        }else {
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            showWrongInfo = true
            tableView.reloadData()
        }
    }
    func didSuccessValidateEmail() {
        LoadingOverlay.shared.hideOverlayView()
        isCorrectEmail = true
        tableView.reloadData()
    }
    func didFailValidateEmail(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        isCorrectEmail = false
        tableView.reloadData()
    }
    @IBAction func birthdayButton(_ sender: Any) {
        let datePicker = DatePickerViewController(nibName: "DatePickerViewController", bundle: nil)
        datePicker.modalPresentationStyle = .overCurrentContext
        datePicker.modalTransitionStyle = .coverVertical
        datePicker.delegate = self
        datePicker.view.layoutIfNeeded()
        self.present(datePicker, animated: true, completion: nil)
    }
    func sendDate(date: String, age: Int) {
        if date != "" {
            profile.birthday = date
            UserDefaults.standard.set(age, forKey: "age")
            tableView.reloadData()
        }
        
    }
    @IBAction func femAction(_ sender: Any) {
        self.male.alpha = 0
        self.Female.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            
            self.viewHeight.constant = 0
            self.femHeight.constant = 0
            self.mascHeight.constant = 0
            
        }) { (finished) in
            self.genre.text = "Femenino"
            self.gender = "F"
            self.genre.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            UIView.animate(withDuration: 0.2, animations: {
                self.downArrow.transform = CGAffineTransform.identity
            })
        }
        
    }
    @IBAction func mascAction(_ sender: Any) {
        self.male.alpha = 0
        self.Female.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            
            self.viewHeight.constant = 0
            self.femHeight.constant = 0
            self.mascHeight.constant = 0
            
        }) { (finished) in
            self.genre.text = "Masculino"
            self.gender = "M"
            self.genre.textColor = UIColor.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
            UIView.animate(withDuration: 0.2, animations: {
                self.downArrow.transform = CGAffineTransform.identity
            })
        }
        
    }
    
    @IBAction func continueAction(_ sender: Any) {
        if name.text != "", mail.text != "", genre.text != "Sexo *", birthday.text != "Fecha de Nacimiento *" {
            let str = name.text!
            LoadingOverlay.shared.showOverlay(view: self.view)
            var isRegister = Int()
            if is_register {
                isRegister = 1
            }else{
                isRegister = 2
            }
            if let range: Range<String.Index> = str.range(of: " ") {
                let last_name = str.substring(from: range.upperBound)
                let substring_fn = str.substring(to: range.upperBound)
                let first_name = substring_fn.trimmingCharacters(in: .whitespaces)
                defaults.set(first_name, forKey: "first_name")
                if correcEmail {
                    userws.updateProfile(client_id: client_id, first_name: first_name, last_name: last_name, gender: gender, birth_date: birthday.text!, zip_code: zipCode.text!, email: mail.text!, is_register: isRegister)
                }
                
            }else{
                defaults.set(profile.name, forKey: "first_name")
                if correcEmail {
                    userws.updateProfile(client_id: client_id, first_name: profile.name, last_name: "", gender: gender, birth_date: birthday.text!, zip_code: zipCode.text!, email: mail.text!, is_register: isRegister)
                }
                
            }
            
        }else{
            let alert = UIAlertController(title: "Completa el campo", message: "Los campos marcados en rojo son obligatorios", preferredStyle: .alert)
            let action = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        if name.text == "" {
            wrongName.alpha = 1
            wrongName.image = UIImage(named: "wrongIcon")
        }
        if mail.text == "" {
            indicatorIcons.alpha = 1
            indicatorIcons.image = UIImage(named: "wrongIcon")
        }
        if genre.text == "Sexo *" {
            wrongGender.alpha = 1
            wrongGender.image = UIImage(named: "wrongIcon")
        }
        if birthday.text == "Fecha de Nacimiento *" {
            wrongBirthday.alpha = 1
            wrongBirthday.image = UIImage(named: "wrongIcon")
        }
    }
    func didSuccessUpdateProfile() {
        LoadingOverlay.shared.hideOverlayView()
        defaults.removeObject(forKey: "complete_register")
        defaults.set(false, forKey: "login")
//        if locationManager.location == nil {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//            self.navigationController?.pushViewController(newVC, animated: true)
//        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)
//        }
        
    }
    
    func didFailUpdateProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    func didFailViewProfile(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = title
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    
    @IBAction func TermsAndCond(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
        //        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func touchFields(_ sender: UIButton) {
        if sender == nameButton {
            name.becomeFirstResponder()
        }else if sender == mailButton {
            mail.becomeFirstResponder()
        }else{
            zipCode.becomeFirstResponder()
        }
    }
    func sendZipInfo(value: String) {
        profile.zipCode = Int(value)
        tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
extension CompleteProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameProfile", for: indexPath) as! inputProfileTableViewCell
            cell.selectionStyle = .none
            if profile.name != "" {
                cell.inputText.text = profile.name
            }else{
                cell.inputText.text = ""
            }
            if showWrongInfo {
                if cell.inputText.text == "" {
                    cell.checkInput.image = UIImage(named: "wrongIcon")
                    cell.checkInput.alpha = 1
                }
            }
            
            cell.delegate = self
            return cell
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mailProfile", for: indexPath) as! inputProfileTableViewCell
            cell.selectionStyle = .none
            
            if isCorrectEmail {
                cell.checkInput.image = UIImage(named: "correctIcon")
            }else{
                cell.checkInput.image = UIImage(named: "wrongIcon")
            }
            if profile.email != "" {
                cell.inputText.text = profile.email
                cell.checkInput.image = UIImage(named: "correctIcon")
                cell.checkInput.alpha = 1
            }
            cell.isEmail = true
            cell.delegate = self
            return cell
        } else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "genderBirthday", for: indexPath) as! birthdayInputTableViewCell
            cell.delegate = self
            cell.indexPath = indexPath
            cell.selectionStyle = .none
            if profile.genre != "" {
                if profile.genre == "F" {
                    cell.genderText.text = "Femenino"
                }else{
                    cell.genderText.text = "Masculino"
                }
            }else{
                
                cell.genderText.text = ""
            }
            if profile.birthday != "" {
                cell.birthdayLabel.text = profile.birthday
                cell.birthdayLabel.textColor = UIColor.init(red: 90/255, green: 104/255, blue: 114/255, alpha: 1)
            }else{
                cell.birthdayLabel.text = "Fecha de Nacimiento"
                cell.birthdayLabel.textColor = UIColor.init(red: 197/255, green: 200/255, blue: 208/255, alpha: 1)
            }
            
            if showWrongInfo {
                if cell.genderText.text == "" {
                    cell.check1.image = UIImage(named: "wrongIcon")
                    cell.check1.alpha = 1
                }else{
                    cell.check1.image = UIImage(named: "correctIcon")
                    cell.check1.alpha = 1
                    
                }
                if cell.birthdayLabel.text == "Fecha de Nacimiento" {
                    cell.check2.image = UIImage(named: "wrongIcon")
                    cell.check2.alpha = 1
                }else{
                    cell.check2.image = UIImage(named: "correctIcon")
                    cell.check2.alpha = 1
                }
            }else{
                if cell.genderText.text != "" {
                    cell.check1.alpha = 1
                }else{
                    cell.check1.alpha = 0
                }
                if cell.birthdayLabel.text == "Fecha de Nacimiento" {
                    cell.check2.alpha = 0
                }else{
                    cell.check2.alpha = 1
                }
                
            }
            return cell
        }else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "zipCodeCell", for: indexPath) as! zipCodeInputTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            if profile.zipCode != 0 && profile.zipCode != nil {
                cell.postalText.text = "\(profile.zipCode!)"
            }else{
                cell.postalText.text = ""
            }
            
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectFavoriteFood", for: indexPath) as! selectKindOfFoodTableViewCell
            cell.comidaText.text = favoriteMeals
            cell.selectionStyle = .none
            cell.delegate = self
            if showWrongInfo{
                if cell.comidaText.text == "" {
                    cell.checkInfo.image = UIImage(named: "wrongIcon")
                    cell.checkInfo.alpha = 1
                }else{
                    cell.checkInfo.image = UIImage(named: "correctIcon")
                    cell.checkInfo.alpha = 1
                }
            }else{
                if cell.comidaText.text == "" {
                    cell.checkInfo.alpha = 0
                }else{
                    cell.checkInfo.alpha = 1
                    cell.checkInfo.image = UIImage(named: "correctIcon")
                }
            }
            return cell
        }else{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "sendInfo", for: indexPath) as! sendInfoCompleteProfileTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 140
        }else if indexPath.row == 3 {
            return genderDropdownHeight
        }else if indexPath.row == 6 {
            return 59
        }else{
            return 72
        }
    }
}
