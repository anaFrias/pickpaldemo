//
//  ViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
import RevealingSplashView
import CoreLocation

class ViewController: UIViewController, UIScrollViewDelegate, userDelegate {

    @IBOutlet weak var YaTienesCuentaConstraint: NSLayoutConstraint!
    @IBOutlet weak var registrateContraint: NSLayoutConstraint!
    @IBOutlet weak var Slider: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var invited: UIButton!
    
    var images = [String]()
    var description_title = [String]()
    var titles = [String]()
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var ws = UserWS()
    var closeSession = false
    let newXIB = SplashWaitingViewController()
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        if self.closeSession {
            self.ws.welcomeSliders()
            UIApplication.shared.unregisterForRemoteNotifications()
            LoadingOverlay.shared.showOverlay(view: self.view)
            self.navigationController?.isNavigationBarHidden = true
            if UIScreen.main.bounds.height ==  568 {
                self.YaTienesCuentaConstraint.constant = 15
                self.registrateContraint.constant = 15
            }
            self.newXIB.view.frame = UIScreen.main.bounds
            self.view.addSubview(self.newXIB.view)
        
        }else{
            let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "singlelogo")!,iconInitialSize: CGSize(width: 139, height: 135), backgroundColor: UIColor(red:220/255, green:25/255, blue:53/255, alpha:1.0))
            
            //Adds the revealing splash view as a sub view
            self.view.addSubview(revealingSplashView)
            revealingSplashView.startAnimation(){
                self.ws.welcomeSliders()
                UIApplication.shared.unregisterForRemoteNotifications()
                LoadingOverlay.shared.showOverlay(view: self.view)
                self.navigationController?.isNavigationBarHidden = true
                if UIScreen.main.bounds.height ==  568 {
                    self.YaTienesCuentaConstraint.constant = 15
                    self.registrateContraint.constant = 15
                }
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    func didSuccessGetSliders(terms: [Sliders]) {
       
        LoadingOverlay.shared.hideOverlayView()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.newXIB.view.removeFromSuperview()
        })
        
        images.removeAll()
        for t in terms {
            images.append(t.image)
//            description_title.append(t.description_title)
//            titles.append(t.title)
        }
        self.Slider.layoutIfNeeded()
        self.Slider.setNeedsLayout()
        viewDidLayoutSubviews()
        UIView.animate(withDuration: 1, animations: {
            self.Slider.backgroundColor = UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 0)
        }, completion: nil)
    }
    @IBAction func invitedLogin(_ sender: Any) {
        ws.LoginWithMail(email: "foreign@pickpal.com", password: "Admin123")
    }
    
    func didSuccessLoginWithMail(avatar: String, email: String, first_name: String, id: Int, news_feed: Int) {
        UserDefaults.standard.set(first_name, forKey: "first_name")
        UserDefaults.standard.set(id, forKey: "client_id")
        UserDefaults.standard.set(news_feed, forKey: "last_id_news_feed")
        UserDefaults.standard.set(avatar, forKey: "avatar")
        UserDefaults.standard.set(true, forKey: "invited")
//        if locationManager.location == nil {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//            self.navigationController?.pushViewController(newVC, animated: true)
//        }else {
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
//        }
    }
    
    func didSuccessLoginWithMailTwo(avatar: String, email: String, first_name: String, id: Int, news_feed: Int, is_register: Bool, complete_info: Bool, age: Int) {
        UserDefaults.standard.set(first_name, forKey: "first_name")
        UserDefaults.standard.set(id, forKey: "client_id")
        UserDefaults.standard.set(news_feed, forKey: "last_id_news_feed")
        UserDefaults.standard.set(avatar, forKey: "avatar")
        UserDefaults.standard.set(true, forKey: "invited")
        UserDefaults.standard.set(age, forKey: "age")
//        if locationManager.location == nil {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let newVC = storyboard.instantiateViewController(withIdentifier: "GPS") as! GPSViewController
//            self.navigationController?.pushViewController(newVC, animated: true)
//
//        }else {
            let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
            self.navigationController?.pushViewController(newVC, animated: true)
//        }
    }
    func didFailGetSliders(title: String, subtitle: String) {
        print(subtitle)
    }
    override func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = Slider.frame.size.width * CGFloat(index)
                frame.size = Slider.frame.size
                
                let view = UIView(frame: frame)
                Slider.scrollsToTop = true
                view.bounds.size.height = Slider.frame.size.height
                //            view.backgroundColor = UIColor.red
                //            let image = UIImageView(image: images[index])
                var image = UIImageView()
                Nuke.loadImage(with: URL(string: images[index])!, into: image)
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: Slider.frame.size)
                image.contentMode = .scaleAspectFill
                image.clipsToBounds = true
                view.clipsToBounds = true
                
                Slider.addSubview(image)
                //imageView.image = images[index]
                
                width += Int(Slider.frame.width)
                
                self.Slider.addSubview(view)
            }
            
            Slider.contentSize = CGSize(width: (Slider.frame.size.width * CGFloat(images.count)), height: Slider.frame.size.height)
//            print(Slider.contentSize, Slider.frame.size.height)
            Slider.delegate = self
        }
         Slider.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        
        
    }
    
    @IBAction func viewTerms(_ sender: Any) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
//        print(pageNumber)
        pageControl.currentPage = Int(pageNumber)
        pageControl.currentPageIndicatorTintColor = UIColor.white
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Register(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Register") as! RegisterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func LogIn(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let newVC = storyboard.instantiateViewController(withIdentifier: "Login") as! LoginPhoneViewController
//        self.navigationController?.pushViewController(newVC, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LoginScreen") as! LoginScreenViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func TermsAndConditions(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Terms") as! TermsViewController
        vc.modalPresentationStyle = .popover
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

