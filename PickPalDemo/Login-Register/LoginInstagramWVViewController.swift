//
//  LoginInstagramWVViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 25/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import WebKit

protocol WebLoginControllerDelegate {
    
    func webLoginController(didFinishLogin userDict:NSDictionary)
}


class LoginInstagramWVViewController: UIViewController, WKUIDelegate {
    
    @IBOutlet weak var webview: WKWebView!
    
    let indicator = UIActivityIndicatorView()
    var delegate : WebLoginControllerDelegate?

    @IBOutlet weak var viewFlotante: UIView!
    var filViewOrigin: CGPoint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        startAuthorization()
        
        indicator.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        indicator.activityIndicatorViewStyle = .whiteLarge
        indicator.backgroundColor = UIColor.lightText
        indicator.color = UIColor.black
        webview.addSubview(indicator)
        webview.uiDelegate = self
        viewFlotante.layer.cornerRadius = viewFlotante.frame.height / 2
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startAuthorization() {
        print("Executing starauth")
        // Specify the response type which should always be "code".
        let responseType = "code"
        
        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = Constants.redirectURI.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)!
        
        // Set preferred scope.
        //let scope = "r_basicprofile%20r_emailaddress%20w_share"
        
        var authorizationURL = "\(Constants.authorizationEndPoint)?"
        authorizationURL += "client_id=\(Constants.instagramClientID)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "response_type=\(responseType)"
        
        print(authorizationURL)
        
        // Create a URL request and load it in the web view.
        let request = URLRequest(url: URL(string: authorizationURL)!)
        webview.load(request)
    }
    
    func requestForAccessToken(_ authorizationCode: String) {
        let grantType = "authorization_code"
        
        let redirectURL = Constants.redirectURI.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics)!
        
        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(Constants.instagramClientID)&"
        postParams += "client_secret=\(Constants.instagramSecret)&"
        
        // Convert the POST parameters into a NSData object.
        let postData = postParams.data(using: String.Encoding.utf8)
        
        
        // Initialize a mutable URL request object using the access token endpoint URL string.
        var request = URLRequest(url: URL(string: Constants.accessTokenEndPoint)!)
        
        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"
        
        // Set the HTTP body using the postData object created above.
        request.httpBody = postData
        
        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
        
        
        // Initialize a NSURLSession object.
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        
        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request,completionHandler: { (data, response, error) -> Void in
            // Get the HTTP status code of the request.
            if error == nil {
                
                let statusCode = (response as! HTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [String:Any]
                        
                        
                        print(dataDictionary)
                        
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.indicator.stopAnimating()
                            self.delegate!.webLoginController(didFinishLogin: dataDictionary as NSDictionary)
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }else{
                    
//                    self.indicator.stopAnimating()
                    self.dismiss(animated: true, completion: nil)
                }
            }
                
            else {
                
                self.indicator.stopAnimating()
                print(error!.localizedDescription)
                
            }
        })
        
        task.resume()
    }
    
    //MARK: - WebView Delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
         indicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        var action: WKNavigationActionPolicy?

        defer {
            decisionHandler(action ?? .allow)
        }

        guard let requestURLString = navigationAction.request.url else { return }
        print("decidePolicyFor - url: \(requestURLString)")
        
        if requestURLString.absoluteString.hasPrefix(Constants.redirectURI) {
            let range: Range<String.Index> = requestURLString.absoluteString.range(of: "?code=")!
            print(requestURLString.absoluteString.substring(from: range.upperBound))
            requestForAccessToken(requestURLString.absoluteString.substring(from: range.upperBound))
        }
    }

    @IBAction func handlePan(_ sender: UIPanGestureRecognizer) {
        let fileView = sender.view!
        let transation = sender.translation(in: view)
        switch sender.state {
        case .began, .changed:
            fileView.center = CGPoint(x: (fileView.center.x) + transation.x, y: (fileView.center.y) + transation.y)
            sender.setTranslation(CGPoint.zero, in: view)
        case .ended:
            break
        default:
            break
        }
    }
    @IBAction func closeView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
