//
//  LinkTableViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 28/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class LinkTableViewController: UIViewController, AlertAdsDelegate, scanerDelgate, CustomerServiceDelegate,AlertJoinToTableProtocol {
    func reloadListEstablishment() {
        
    }
    
  
    @IBOutlet weak var btnValidar: UIButton!
    @IBOutlet weak var textFileFolio: UITextField!
    @IBOutlet weak var lblFailQR: UILabel!
    @IBOutlet weak var lblFailFolio: UILabel!
    @IBOutlet weak var lblPedidos: UILabel!
    
    @IBOutlet weak var viewNumPedido: UIView!
    
    var customerService = CustomerServiceWS()
    var fromFolio = ""
    var numeroPedidos = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customerService.delegate = self
        
        lblFailFolio.visibility = .gone
        lblFailQR.visibility = .gone
        
        lblPedidos.text = numeroPedidos > 0 ? "\(numeroPedidos)":""
        viewNumPedido.alpha =  numeroPedidos > 0 ? 1 : 0
        
        
        
        btnValidar.clipsToBounds = false
        btnValidar.layer.masksToBounds = false
        btnValidar.layer.cornerRadius = 10
        
       
        
    }
    
    @IBAction func btnOpenScanner(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "ScannerQR") as! ScannerViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.delegate = self
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }

    @IBAction func btnSendFolio(_ sender: Any) {
        
        
        if textFileFolio.text! != ""{
                        
            fromFolio = "Manual"
            customerService.joinToTable(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, folio: textFileFolio.text!)
            
        
        }else{
            
            
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "Por favor, escanea el Código QR de tu mesa o Ingresa el No. de Folio."
            newXIB.mTitle = "Error"
            newXIB.mToHome = false
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.valueCall = 2
            newXIB.isFromAdd = false
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
//            lblFailFolio.visibility = .visible
        }
    }
    
    
    func folioTable(folio: String) {
        fromFolio = "Scanner"
        customerService.joinToTable(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, folio: folio)
        
    }
    
    
    
    func getJoinToTable(establishmentName: String, numberTable: Int){
        
        let newXIB = AlertJoinToTable(nibName: "AlertJoinToTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.name = "\(establishmentName)"
        newXIB.numberTable = numberTable
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func didFailJoinToTable(error: String, subtitle: String,code:Int){
        
        
        if fromFolio == "Scanner"{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Código QR inválido"
            newXIB.code = code
            self.present(newXIB, animated: true, completion: nil)
            
        }else{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Folio inválido"
            newXIB.code = code
            self.present(newXIB, animated: true, completion: nil)
            
        }
        
    }
    
    
    func didFailJoinToTableUser(error: String, subtitle: String){
        
        
        if fromFolio == "Scanner"{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Código QR inválido"
            self.present(newXIB, animated: true, completion: nil)
            
        }else{
            
            let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.textFail = "Folio inválido"
            self.present(newXIB, animated: true, completion: nil)
            
        }
        
        
    }
    
    
    func goToHome() {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeTableLinked") as! HomeTableLinkedViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.numeroPedidos = numeroPedidos
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    @IBAction func goToBack(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
       
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.whereFrom  = Constants.PICKPAL_WALLET
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    
    
    func didFailJoinToTableOrder(error: String, subtitle: String){
        
            
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)

        
    }
    

}
