//
//  SingleCustumerServiceViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 31/03/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

class SingleCustumerServiceViewController: UIViewController, PointsWSDelegate, PromosDelegate,AlertAdsDelegate, ChatDelegate, AlrtDisJointTableDelegate, CustomerServiceDelegate{


    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblNameEstablisment: UILabel!
    @IBOutlet weak var lblNumberTable: UILabel!
    
    @IBOutlet weak var imBtnPedirCuenta: UIButton!
    @IBOutlet weak var imgBtnCancel: UIButton!
    @IBOutlet weak var imgBtnCallWaiter: UIButton!
    @IBOutlet weak var btnGuia: UIButton!
    @IBOutlet weak var btnPromos: UIButton!
    @IBOutlet weak var btnFoodAndDrinks: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    
    @IBOutlet weak var imgServiceEst: UIImageView!
    @IBOutlet weak var imgServiceBar: UIImageView!
    @IBOutlet weak var imgServiceTable: UIImageView!
    @IBOutlet weak var imgServiceHome: UIImageView!
    
    @IBOutlet weak var btnCallWaiter: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var historyTablet: UITableView!
    
    @IBOutlet weak var lblPedidos: UILabel!
    @IBOutlet weak var viewNumPedido: UIView!
    
    @IBOutlet weak var btnDisJointTable: UIButton!
    
    
    var infoSingle = MySingleServiceClient()
    var pmws = PromosWS()
    var pointsSw = PointsWS()
    var isUser = true
    var customerWS = CustomerServiceWS()
    
    var history = [String]()
    var author = [String]()
    var time = [String]()
    let defaults:UserDefaults = UserDefaults.standard
    var userEmail = UserDefaults.standard.object(forKey: "first_name") as! String
    var isConnected = false
    var chatDelegate = ChatHistoryServices()
    
    
    var callWaiter = false
    var AskForAccount = false
    
    var establihsmentName = String()
    var numeroPedidos = 0
    
    var idDeskUser = Int()
    var numberTable = Int()
    var nameEstablisment = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pointsSw.delegate = self
        pmws.delegate = self
        chatDelegate.delegate = self
        historyTablet.delegate = self
        historyTablet.dataSource = self
        customerWS.delegate = self
        
        lblNameEstablisment.text = infoSingle.establishment_name
        lblNumberTable.text = "\(String(format: "%02d", infoSingle.numberTable!))"
        Nuke.loadImage(with: URL(string: infoSingle.image)!, into: imgLogo)

        lblNumberTable.clipsToBounds = true
        lblNumberTable.layer.masksToBounds = false
        lblNumberTable.layer.cornerRadius = 12
        
        
//        btnDisJointTable.backgroundColor = .clear
//        btnDisJointTable.layer.cornerRadius = 5
//        btnDisJointTable.layer.borderWidth = 1
//        btnDisJointTable.layer.borderColor = UIColor(red: 0.96, green: 0.09, blue: 0.17, alpha: 1.00).cgColor
        
        pickpalServices()
       
        
        establihsmentName = infoSingle.establishment_name
    
        lblPedidos.text = numeroPedidos > 0 ? "\(numeroPedidos)":""
        
        viewNumPedido.alpha =  numeroPedidos > 0 ? 1 : 0
        
        
        for val in infoSingle.messages_history  {
            history.append(val.message)
            author.append(val.user)
            time.append(val.time)
            
        }
        
        callWaiter = infoSingle.status_request_waiter
        AskForAccount = infoSingle.status_request_bill
        
        
        historyTablet.register(UINib(nibName: "ClientMesaggeTableViewCell", bundle: nil), forCellReuseIdentifier: "clientMesagge")
        historyTablet.register(UINib(nibName: "ContollerMesaggeTableViewCell", bundle: nil), forCellReuseIdentifier: "controllerMesagger")
        imgBtnCancel.isUserInteractionEnabled = false
  
        
        if callWaiter{
            
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_on.png"),for: .normal)
            //GRISS
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_gris.png"),for: .normal)
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
        }else{
            
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
           
        }
        
        if AskForAccount{
            
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_on.png"),for: .normal)
            //GRISS
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_gray.png"),for: .normal)
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
        }else{
            
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
           
        }
        
        if !AskForAccount && !callWaiter{
            
            imgBtnCancel.setImage(UIImage(named: "ic_cancel_call_waiter_off.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = false
        }else{
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
            
        }
        
        
        
    }
    
    @IBAction func goToBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
//        chatDelegate.closeSocket()
        
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "HomeTableLinked") as! HomeTableLinkedViewController
        let navController = UINavigationController(rootViewController: newVC)
        newVC.numeroPedidos = numeroPedidos
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func orderType(){
    
        let iconsTyOrder = CustomsFuncs.getIconsOrderTypesPickPal(data: infoSingle.order_types)
        switch iconsTyOrder.count {
        case 4:
            imgServiceEst.isHidden = false
            imgServiceBar.isHidden = false
            imgServiceTable.isHidden = false
            imgServiceHome.isHidden = false
            
            imgServiceEst.image = UIImage(named: iconsTyOrder[0])
            imgServiceBar.image = UIImage(named: iconsTyOrder[1])
            imgServiceTable.image = UIImage(named: iconsTyOrder[2])
            imgServiceHome.image = UIImage(named: iconsTyOrder[3])
            break
        case 3:
            imgServiceEst.isHidden = false
            imgServiceBar.isHidden = false
            imgServiceTable.isHidden = false
            imgServiceHome.isHidden = true
            
            imgServiceEst.image = UIImage(named: iconsTyOrder[0])
            imgServiceBar.image = UIImage(named: iconsTyOrder[1])
            imgServiceTable.image = UIImage(named: iconsTyOrder[2])
            break
            
        case 2:
            imgServiceEst.isHidden = false
            imgServiceBar.isHidden = false
            imgServiceTable.isHidden = true
            imgServiceHome.isHidden = true
            
            imgServiceEst.image = UIImage(named: iconsTyOrder[0])
            imgServiceBar.image = UIImage(named: iconsTyOrder[1])
            break
            
        case 1:
            imgServiceEst.isHidden = false
            imgServiceBar.isHidden = true
            imgServiceTable.isHidden = true
            imgServiceHome.isHidden = true
            
            imgServiceEst.image = UIImage(named: iconsTyOrder[0])
            break
            
        default:
            imgServiceEst.isHidden = true
            imgServiceBar.isHidden = true
            imgServiceTable.isHidden = true
            imgServiceHome.isHidden = true
            break
        }
    
    }

    
    func pickpalServices(){
        
        if infoSingle.pickpal_services != nil {
            let icons = CustomsFuncs.getIconsServicePickPal(data: infoSingle.pickpal_services)
            switch icons.count {
            case 4:
                
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = false
                
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                break
            case 3:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                break
                
            case 2:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                break
                
            case 1:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                break
                
            default:
                btnFoodAndDrinks.isHidden = true
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                break
            }
            
        }
        
    }
    
    
    @IBAction func btnServicePO(_ sender: Any) {
        
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: infoSingle.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[0])
        goToNavigation(acttion:image)
        
    }
    
    @IBAction func btnServicePromo(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: infoSingle.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[1])
        goToNavigation(acttion:image)
    }
    @IBAction func btnServiceGuia(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: infoSingle.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[2])
        goToNavigation(acttion:image)
    }
    @IBAction func btnServicePoints(_ sender: Any) {
        let imagesList = CustomsFuncs.getIconsServicePickPal(data: infoSingle.pickpal_services)
        let image = CustomsFuncs.getImage(image: imagesList[3])
        goToNavigation(acttion:image)
    }
    
    
    func goToNavigation(acttion: Int){
        
        switch acttion {
        case 1://FoodAndDrinks
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = self.infoSingle.id
            newVC.municipioStr = self.infoSingle.state
            newVC.categoriaStr = self.infoSingle.business_line
            newVC.lugarString = self.infoSingle.place
            newVC.establString = ""
            self.navigationController?.pushViewController(newVC, animated: true)
            break
        case 2://Promos
            LoadingOverlay.shared.showOverlay(view: self.view)
            pmws.viewSingleAdvsEstablishment(establishmentID:infoSingle.establishment_id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
            break

        case 3:
            
            let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment_guide") as! SingleGuideCollectionViewCell
            newVC.id = infoSingle.id
            newVC.municipioStr = infoSingle.business_line
            newVC.categoriaStr = infoSingle.business_line
            newVC.lugarString = infoSingle.place
            newVC.establString = ""
            newVC.isServicePlus = infoSingle.service_plus ?? false
//            newVC.kilometers = km
            self.navigationController?.pushViewController(newVC, animated: true)

        default:
          
            pointsSw.CountEstablishmentPoints()
            
            break
        }
    }
    
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs) {
        LoadingOverlay.shared.hideOverlayView()
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        newVC.singleInfo = info
        newVC.id = info.id
        newVC.municipioStr = info.address.city
        newVC.categoriaStr = info.address.colony
        newVC.lugarString = info.advsCategorry
        newVC.establString = info.name
        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailSingleAdvs(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        print(subtitle)
    }
    
    func didSuccessCountEstablishmentPoints(number: Int) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailCountEstablishmentPoints(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showDialog(title: "PickPal Puntos Móviles", message: "Aún no tienes Puntos Móviles. Haz compras en los comercios afiliados para ganar puntos.")
    }
    
    func showDialog(title: String, message: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCallWaiter(_ sender: Any) {
  
        chatDelegate.connectedSocket(id_desk_user: infoSingle.id_desk_user!,history:self.history, author: self.author,time:self.time, callWaiter: callWaiter, AskForAccount: AskForAccount, isHome: false)
        if !AskForAccount && !callWaiter{
            
            chatDelegate.sendMessage(message:"Hola, ¿Podrían enviar al mesero?",code: 1)

        }else{
            
            openAlert(code: 5)
        }
        
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {

        chatDelegate.connectedSocket(id_desk_user: infoSingle.id_desk_user!,history:self.history, author: self.author,time:self.time, callWaiter: callWaiter, AskForAccount: AskForAccount, isHome: false)
       
        if callWaiter{
            
            chatDelegate.sendMessage(message:"Ya no necesito que venga el mesero",code: 3)

        }else{
            
            chatDelegate.sendMessage(message:"Ya no necesito la cuenta",code: 4)

            
            
        }
       
        
    }
    
    @IBAction func btnPedirCuenta(_ sender: Any) {
     
        chatDelegate.connectedSocket(id_desk_user: infoSingle.id_desk_user!,history:self.history, author: self.author,time:self.time, callWaiter: callWaiter, AskForAccount: AskForAccount, isHome: false)
        if !AskForAccount && !callWaiter{
         
            chatDelegate.sendMessage(message: "Hola, ¿Podrían traerme la cuenta?", code: 2)
            
        }else{
            
            openAlert(code: 6)
        }
        
    }
    
    
    @IBAction func btnDisjointTable(_ sender: Any) {
        
        
      
        customerWS.checkStatusJoindtable(help_desk_id: infoSingle.id_desk_user!)
        
       
        
        
        
        
    }
    
    
    func didCheckStatusJoindtable() {
        
        let newXIB = AlrtDisJointTable(nibName: "AlrtDisJointTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.idDeskUser = infoSingle.id_desk_user!
        newXIB.textBtnCancel = "No, continuar afiliado"
        newXIB.textBtnAcept = "Si, desafiliarme"
        newXIB.titleText = "¿Deseas desafiliarte  del establecimiento?"
        newXIB.secondText = "\(UserDefaults.standard.object(forKey: "first_name")!), Al salir de una mesa, las funciones \"Llamar Mesero\", \"Pedir Cuenta\" y \"Servicio a Mesa\" ya no estarán disponibles. Para utilizarlas debes afiliarte de nuevo."
        self.present(newXIB, animated: true, completion: nil)
    
    }
    
    
    
}



extension SingleCustumerServiceViewController: UITableViewDelegate, UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return history.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
//        chatDelegate.socket.connect()
        
        let data = userMessages()
        
        data.message = history[indexPath.row]
        data.user = author[indexPath.row]
        data.time = time[indexPath.row]
        
        if author[indexPath.row] == UserDefaults.standard.object(forKey: "first_name") as! String {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "clientMesagge", for: indexPath) as! ClientMesaggeTableViewCell
            
            cell.loadInfoCard(info: data)
            cell.selectionStyle = .none

            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "controllerMesagger", for: indexPath) as! ContollerMesaggeTableViewCell
            
            cell.loadInfoCard(info: data)
            
            cell.selectionStyle = .none

            return cell
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
    }
    

    func formatterHour(hour: String)-> String{
        
        let form = DateFormatter()
        form.dateFormat = "dd/mm/yyyy HH:mm"
        let datePay = form.date(from: hour)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm aa"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        var dateString = String()
        if datePay == nil {
            dateString = ""
        }else{
            dateString = formatter.string(from: datePay!)
        }

        return dateString
    
    }
    
}





extension SingleCustumerServiceViewController {
    
    
    func infoReceive(history: [String], author: [String], time: [String], status_request_bill: Bool, status_request_waiter: Bool) {
        
        self.time = time
        self.history = history
        self.author = author
        
        callWaiter = status_request_waiter
        AskForAccount = status_request_bill
        
        
        if callWaiter{
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_on.png"),for: .normal)
            //GRISS
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_gris.png"),for: .normal)
        }else{
            
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
            
        }
        
        if AskForAccount{
            
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_on.png"),for: .normal)
            //GRISS
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_gray.png"),for: .normal)
            
        }else{
            
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
            
        }
        
        if !AskForAccount && !callWaiter{
            
            imgBtnCancel.setImage(UIImage(named: "ic_cancel_call_waiter_off.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = false
            
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
        }else{
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
        }
        
        
        historyTablet.reloadData()
        if self.history.count > 0 {
//            historyTablet.scrollToRow(at: IndexPath.init(row: self.history.count - 1, section: 0), at: .bottom, animated: false)
            historyTablet.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .bottom, animated: false)
        }
        
        
        
        
        
        
        
    }
    
    func connectedSocketFail() {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = "No se pudo iniciar el chat"
        newXIB.mTitle = "Error"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func openAlert(code: Int) {
           
            let newXIB = AlertServicesClient(nibName: "AlertServicesClient", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.establihsmentName = establihsmentName
            newXIB.code = code
            self.present(newXIB, animated: true, completion: nil)
            
    
        
    }
    
    
    func disJointTable(idDeskUser: Int) {
        
        customerWS.disJointTable(help_desk_id: idDeskUser)
        
    }
    
    
    func didDisjointTable() {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LinkTableViewController") as! LinkTableViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func didFailDisjointTable(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error //"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        newXIB.expander = true
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func didFailCheckStatusJoindtable(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error//"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
}
