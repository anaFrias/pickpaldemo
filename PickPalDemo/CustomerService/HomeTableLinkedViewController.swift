//
//  HomeTableLinkedViewController.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 24/03/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class HomeTableLinkedViewController: UIViewController, CustomerServiceDelegate, AlertJoinToTableProtocol,AlertAdsDelegate, OrderCustomerServiceDelegate {


    @IBOutlet weak var tableViewEstablishment: UITableView!
    
    @IBOutlet weak var btnScannerQR: UIButton!
    @IBOutlet weak var btiFolio: UIButton!
    @IBOutlet weak var txtfFolioTable: UITextField!
    @IBOutlet weak var lblVincularMesa: UILabel!
    
    @IBOutlet weak var containerButtons: UIStackView!
    @IBOutlet weak var lblPedidos: UILabel!
    @IBOutlet weak var viewNumPedido: UIView!
    
    @IBOutlet weak var heightContainerHader: NSLayoutConstraint!
    var listEstablishment = [EstablishtmentCustomerService]()
    
    var dummy = EstablishtmentCustomerService()
    var dummy2 = EstablishtmentCustomerService()
    var dummy3 = EstablishtmentCustomerService()
    var dummy4 = EstablishtmentCustomerService()
    var dummy5 = EstablishtmentCustomerService()
    var dummy6 = EstablishtmentCustomerService()
    var dummy7 = EstablishtmentCustomerService()
    var dummy8 = EstablishtmentCustomerService()
    var customerService = CustomerServiceWS()
    var folioTable = String()
    var joinToTable = false
    var stablishmentName = String()
    var numeroPedidos = 0
    
    var idDeskUser = Int()
    var numberTable = Int()
    var nameEstablisment = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewEstablishment.delegate = self
        tableViewEstablishment.dataSource = self
        customerService.delegate = self
        
       
        btnScannerQR.layer.borderColor = UIColor.black.cgColor
        btnScannerQR.layer.cornerRadius = 5
        
    
        btiFolio.layer.borderColor = UIColor.black.cgColor
        btiFolio.layer.cornerRadius = 5
       
        
        lblPedidos.text = numeroPedidos > 0 ? "\(numeroPedidos)":""
        viewNumPedido.alpha =  numeroPedidos > 0 ? 1 : 0
        containerButtons.visibility = .gone
        lblVincularMesa.visibility = .gone
        
        if joinToTable{
            
            customerService.joinToTable(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, folio: folioTable)
            
        }
        
        customerService.getMyListCustomerService()
        tableViewEstablishment.register(UINib(nibName: "OrderCustomerServiceTableViewCell", bundle: nil), forCellReuseIdentifier: "EstablishmentServiceCell")
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        customerService.getMyListCustomerService()
        tableViewEstablishment.reloadData()
    }
    
    
    @IBAction func btnSetFolioTable(_ sender: Any) {
        
//
        
        if txtfFolioTable.text! != ""{
            
            customerService.joinToTable(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, folio: txtfFolioTable.text!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil))
        
        }else{
            
            showDialog(title: "Error", message: "Ingresa un número de folio")
            
            
        }
        
    }
    
    
    func reloadListEstablishment(){
        
        customerService.getMyListCustomerService()
    }
    

    @IBAction func btnCustomerService(_ sender: Any) {
        
//        customerService.getSingleServiceClientById(help_desk_id: <#T##Int#>)
        
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "ScannerQR") as! ScannerViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    @IBAction func goToBack(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func goToHome(_ sender: Any) {
        
        
        
    }
    @IBAction func goToOrder(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    @IBAction func btnScannerQR(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LinkTableViewController") as! LinkTableViewController
        let navController = UINavigationController(rootViewController: newVC)
        
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func btnSendFolio(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LinkTableViewController") as! LinkTableViewController
        let navController = UINavigationController(rootViewController: newVC)
        
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
        
    }
    
    func getSingleCustomerServiceById(infoSingle: MySingleServiceClient ){
        
        
        
    }
    
    
    func didFailGetSingleCustomerServiceById(error: String, subtitle: String){
        
        
        
    }
    
    
    func getJoinToTable(establishmentName: String, numberTable: Int){
        
        let newXIB = AlertJoinToTable(nibName: "AlertJoinToTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.name = "\(establishmentName)"
        newXIB.numberTable = numberTable
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func didFailJoinToTable(error: String, subtitle: String,code:Int){
        
        
        let newXIB = AlertFailJoinToTable(nibName: "AlertFailJoinToTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.code = code
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    func showDialog(title: String, message: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func openAlert(code: Int) {
        let newXIB = AlertServicesClient(nibName: "AlertServicesClient", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.establihsmentName = stablishmentName
        newXIB.code = code
        self.present(newXIB, animated: true, completion: nil)
    }
    

}


extension HomeTableLinkedViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func getCustomerServiceList(listCustomerService: [EstablishtmentCustomerService]) {
        
        if listCustomerService.count > 0{
            heightContainerHader.constant = 0
            containerButtons.visibility = .gone
            lblVincularMesa.visibility = .gone
            listEstablishment = listCustomerService
            
            tableViewEstablishment.reloadData()
            
        }else{
            heightContainerHader.constant = 150
            containerButtons.visibility = .visible
            lblVincularMesa.visibility = .visible
            
            if !joinToTable{
            
            }
            
        }
        
        
    }
    
    func didFailGetCustomerServiceList(error: String, subtitle: String) {
        print("Error: ", error)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return listEstablishment.count
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EstablishmentServiceCell", for: indexPath) as! OrderCustomerServiceTableViewCell
        let data = listEstablishment[indexPath.row]
        cell.loadInfoCard(info: data)
        cell.delegate = self
        cell.selectionStyle = .none
        stablishmentName = data.establishment_name

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
       
        let data = listEstablishment[indexPath.row]
        
        customerService.getSingleServiceClient(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int, establishment_id: data.establishment_id, number_table: data.table)
    
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.view.isUserInteractionEnabled = false
        
    }
    
    
    func getSingleCustomerService(infoSingle: MySingleServiceClient ){
        
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleCustumerService") as! SingleCustumerServiceViewController
        newVC.infoSingle = infoSingle
        newVC.numeroPedidos = numeroPedidos
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    
    func didFailGetSingleCustomerService(error: String, subtitle: String){
        
        LoadingOverlay.shared.hideOverlayView()
        self.view.isUserInteractionEnabled = true
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 300
    }
    
    
    func reloadData() {
        customerService.getMyListCustomerService()
    }
    
    func goToHome() {
        
    }
    
    
}


extension HomeTableLinkedViewController: AlrtDisJointTableDelegate{
    
    
    func disJointTabletCell(nameEstablisment: String, numberTable: Int, id_desk_user: Int) {
        
        self.idDeskUser = id_desk_user
        self.numberTable = numberTable
        self.nameEstablisment = nameEstablisment
        customerService.checkStatusJoindtable(help_desk_id: id_desk_user)
    }
    
    func didCheckStatusJoindtable() {
        let newXIB = AlrtDisJointTable(nibName: "AlrtDisJointTable", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.nameEstablisment = self.nameEstablisment
        newXIB.numberTable = "\(String(format: "%02d", self.numberTable))"
        newXIB.idDeskUser = self.idDeskUser
        newXIB.textBtnCancel = "No, continuar afiliado"
        newXIB.textBtnAcept = "Si, desafiliarme"
        newXIB.titleText = "¿Deseas desafiliarte  del establecimiento?"
        newXIB.secondText = "\(UserDefaults.standard.object(forKey: "first_name")!), Al salir de una mesa, las funciones \"Llamar Mesero\", \"Pedir Cuenta\" y \"Servicio a Mesa\" ya no estarán disponibles. Para utilizarlas debes afiliarte de nuevo."
        self.present(newXIB, animated: true, completion: nil)
    }
    
    
    func disJointTable(idDeskUser: Int){
        
        customerService.disJointTable(help_desk_id: idDeskUser)
        
    }
    
    
    func didDisjointTable() {
        
        let storyboard = UIStoryboard(name: "CustomerServiceStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "LinkTableViewController") as! LinkTableViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    func didFailDisjointTable(error: String, subtitle: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error//"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        newXIB.expander = true
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    func didFailCheckStatusJoindtable(error: String, subtitle: String) {
        
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error//"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
        
    }
    
    
    
    
}




