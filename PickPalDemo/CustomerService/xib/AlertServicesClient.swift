//
//  AlertServicesClient.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertServicesClient: UIViewController {

    
    @IBOutlet weak var containerAlert: UIView!
    @IBOutlet weak var imgIcono: UIImageView!
    @IBOutlet weak var lblTextTitle: UILabel!
    @IBOutlet weak var lblTexrt1: UILabel!
    @IBOutlet weak var lblText2: UILabel!
    
    var textTitle = String()
    var text1 = String()
    var text2 = String()
    var code = Int()
    var establihsmentName = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        switch code {
        case 1:
            imgIcono.image = UIImage(named: "ic_alert_call_waiter")
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = establihsmentName
            text2 = "Tu solicitud Llamar Mesero ha sido enviada"
            
        case 2:
            imgIcono.image = UIImage(named: "ic_alert_call_waiter")
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = establihsmentName
            text2 = "Tu solicitud para Pedir Cuenta ha sido enviada"
            
        case 3:
            imgIcono.image = UIImage(named: "ic_alert_call_waiter_cancel")
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = establihsmentName
            text2 = "La solicitud para Llamar Mesero ha sido cancelada."
            
        case 4:
            imgIcono.image = UIImage(named: "ic_alert_call_waiter_cancel")
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = establihsmentName
            text2 = "La solicitud  para Pedir Cuenta ha sido cancelada."
            
            //CANCELAR LLAMAR MESERO
        case 5:
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            imgIcono.image = UIImage(named: "ic_alert_call_waiter_message")
            text1 = ""
            text2 = "No podrás seleccionar Llamar Mesero hasta que la solicitud que está activa sea atendida o la canceles"
            
            //Cancelar pedir cuenta
        case 6:
            imgIcono.image = UIImage(named: "ic_alert_call_waiter_message")
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = ""
            text2 = "No podrás seleccionar Pedir Cuenta hasta que la solicitud que está activa sea atendida o la canceles"
            
        default:
            textTitle = "\(UserDefaults.standard.object(forKey: "first_name")!)"
            text1 = ""
            text2 = "No podrás seleccionar Llamar Mesero hasta que la solicitud que está activa sea atendida o la canceles"
        }
        
        lblTextTitle.text = textTitle
        lblTexrt1.text = text1
        lblText2.text = text2
        
    }

    @IBAction func btnOK(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnCloseAlert(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
