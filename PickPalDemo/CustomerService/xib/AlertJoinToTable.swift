//
//  AlertJoinToTable.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 14/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol AlertJoinToTableProtocol {
    func reloadListEstablishment()
    func goToHome()
}

class AlertJoinToTable: UIViewController {

    @IBOutlet weak var lblNumberTable: UILabel!
    @IBOutlet weak var lblEstablishmentName: UILabel!
    @IBOutlet var container: UIView!
    
    var delegate: AlertJoinToTableProtocol!
    var numberTable = 0
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblEstablishmentName.text = "\(name)"
        lblNumberTable.text = String(format: "%02d", numberTable)
        
    }

    @IBAction func btnCloseAlert(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnOk(_ sender: Any) {
        
        delegate.reloadListEstablishment()
        delegate.goToHome()
        self.dismiss(animated: true, completion: nil)
       
        
    }
    
}
