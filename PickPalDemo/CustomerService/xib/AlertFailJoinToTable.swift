//
//  AlertFailJoinToTable.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 14/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertFailJoinToTable: UIViewController {

    @IBOutlet weak var lblMenssage: UILabel!
    @IBOutlet weak var lblTextFail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTextFinish: UILabel!
    @IBOutlet weak var heigthContainer: NSLayoutConstraint!
    
    var textFail = String()
    var textTitle = String()
    var textMessage = String()
    var code = Int()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTextFail.text = "\(textFail)"
       
        
        if textMessage != "" && textTitle != ""{
            
            lblMenssage.text = "\(textMessage)"
            lblTitle.text = "\(textTitle)"
            heigthContainer.constant = 320
            lblTextFinish.visibility = .gone
            
        }else{
            
            if code == 404{
                
                heigthContainer.constant = 385
                lblTitle.text = "Aún no estás asignado a una mesa"
                lblTextFinish.text =  "Nota: Para que puedas utilizar las funciones de \"Llamar Mesero y Pedir Cuenta\" el comercio deberá de tener contratado este servicio. Pregunta al mesero."
                
                lblTextFinish.visibility = .visible
                
            }
        }
       
        
    }


    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnOK(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }


}
