//
//  AlertRequestBill.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertRequestBill: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTypeMessage: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnOk(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnCloseAlert(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
