//
//  AlrtDisJointTable.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 26/05/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol AlrtDisJointTableDelegate {
    func disJointTable(idDeskUser: Int)
}

class AlrtDisJointTable: UIViewController {

    @IBOutlet weak var lblTextTitle: UILabel!
    
    @IBOutlet weak var lblTextSecond: UILabel!
    @IBOutlet weak var btnTextCancel: UIButton!
    @IBOutlet weak var btnTextAcept: UIButton!
    
    var numberTable = String()
    var nameEstablisment = String()
    var idDeskUser = Int()
    var delegate: AlrtDisJointTableDelegate!
    var titleText = String()
    var secondText = String()
    var textBtnCancel = String()
    var textBtnAcept = String()
    
    var newString = NSMutableAttributedString()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTextTitle.text =  titleText
        
        if secondText != ""{
            lblTextSecond.text = secondText
        }else{
            
            lblTextSecond.attributedText = newString
        }
        
        
        
        btnTextCancel.setTitle(textBtnCancel, for: .normal)
        btnTextAcept.setTitle(textBtnAcept, for: .normal)
        
    }

    @IBAction func btnCancel(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnClosetAlert(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnAcept(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate.disJointTable(idDeskUser: idDeskUser)
        
    }
    
}
