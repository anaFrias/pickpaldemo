//
//  OrderCustomerServiceTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 24/03/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke


protocol OrderCustomerServiceDelegate {
    func openAlert(code: Int)
    func reloadData()
    func disJointTabletCell(nameEstablisment: String, numberTable: Int, id_desk_user: Int)
}

class OrderCustomerServiceTableViewCell: UITableViewCell,ChatDelegate {

    @IBOutlet weak var imgEstablishment: UIImageView!
    @IBOutlet weak var lblNameEstablishtment: UILabel!
    @IBOutlet weak var lblBusinesssLine: UILabel!
    @IBOutlet weak var lblNumberTable: UILabel!
    @IBOutlet weak var containerCard: UIView!
    
    @IBOutlet weak var imBtnPedirCuenta: UIButton!
    @IBOutlet weak var imgBtnCancel: UIButton!
    @IBOutlet weak var imgBtnCallWaiter: UIButton!
    
    
    @IBOutlet weak var btnDisJointTablet: UIButton!
    var history = [String]()
    var author = [String]()
    var time = [String]()
    var chatDelegate = ChatHistoryServices()
    var delegate: OrderCustomerServiceDelegate!
    var callWaiter = false
    var AskForAccount = false
    
    var nameEstablisment = String()
    var numberTable = Int()
    var idDeskUser = Int()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerCard.clipsToBounds = false
        containerCard.layer.masksToBounds = false
        containerCard.layer.cornerRadius = 10
        containerCard.layer.shadowColor = UIColor.black.cgColor
        containerCard.layer.shadowOpacity = 0.35
        containerCard.layer.shadowOffset = .zero
        containerCard.layer.shadowRadius = 2
    
        
//        btnDisJointTablet.backgroundColor = .clear
//        btnDisJointTablet.layer.cornerRadius = 5
//        btnDisJointTablet.layer.borderWidth = 1
//        btnDisJointTablet.layer.borderColor = UIColor(red: 0.96, green: 0.09, blue: 0.17, alpha: 1.00).cgColor
        
        chatDelegate.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadInfoCard(info: EstablishtmentCustomerService)  {
        
        
        print(info)
        lblNameEstablishtment.text = "\(info.establishment_name!)"
        lblBusinesssLine.text = "\(info.business_line!) - \(info.place!)"
        lblNumberTable.text = "\(String(format: "%02d", info.table!))"
        Nuke.loadImage(with: URL(string: info.image)!, into: imgEstablishment)
        callWaiter = info.status_request_waiter
        AskForAccount = info.status_request_bill
        
        
        //Desvincular mesa
        
        nameEstablisment = info.establishment_name
        numberTable = info.table
        idDeskUser = info.id_desk_user
        
        chatDelegate.connectedSocket(id_desk_user: info.id_desk_user!,history:self.history, author: self.author,time:self.time, callWaiter: callWaiter, AskForAccount: AskForAccount, isHome: true)
        
        LoadingOverlay.shared.hideOverlayView()
        self.isUserInteractionEnabled = true
        
        if callWaiter{
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_on.png"),for: .normal)
            //GRISS
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_gris.png"),for: .normal)
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
        }else{
            
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
           
        }
        
        if AskForAccount{
            
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_on.png"),for: .normal)
            //GRISS
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_gray.png"),for: .normal)
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
        }else{
            
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
           
        }
        
        if !AskForAccount && !callWaiter{
            
            imgBtnCancel.setImage(UIImage(named: "ic_cancel_call_waiter_off.png"),for: .normal)
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = false
        }else{
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
            
        }
        
        
        
    }
    
    
    @IBAction func btnCallWaiter(_ sender: Any) {
        
        chatDelegate.socket.connect()
        if !AskForAccount && !callWaiter{
            
            chatDelegate.sendMessage(message:"Hola, ¿Podrían enviar al mesero?",code: 1)
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_on.png"),for: .normal)
//            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
//            imgBtnCancel.isUserInteractionEnabled = true
//            callWaiter = true
//
            LoadingOverlay.shared.showOverlay(view: self)
            self.isUserInteractionEnabled = false
            
        }else{
            
            
            openAlert(code: 5)
        }
        
//        self.delegate.reloadData()
        
    }
    
    @IBAction func btnAskForAccount(_ sender: Any) {
        
        chatDelegate.socket.connect()
        if !AskForAccount && !callWaiter{
            
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_on.png"),for: .normal)
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
//            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
//            imgBtnCancel.isUserInteractionEnabled = true
            chatDelegate.sendMessage(message: "Hola, ¿Podrían traerme la cuenta?", code: 2)
            
            
            LoadingOverlay.shared.showOverlay(view: self)
            self.isUserInteractionEnabled = false
            
        }else{
            
            openAlert(code: 6)
        }
  
//        self.delegate.reloadData()
        
    }
    
    
    @IBAction func btnCancel(_ sender: Any) {
        
        chatDelegate.socket.connect()
        if callWaiter{
            
            chatDelegate.sendMessage(message:"Ya no necesito que venga el mesero",code: 3)
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
            
        }else{
            
            chatDelegate.sendMessage(message:"Ya no necesito la cuenta",code: 4)
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
            
            
        }
        LoadingOverlay.shared.showOverlay(view: self)
        self.isUserInteractionEnabled = false
//        imgBtnCancel.setImage(UIImage(named: "ic_cancel_call_waiter_off.png"),for: .normal)
//        imgBtnCancel.isUserInteractionEnabled = false
        
//        self.delegate.reloadData()
        
    }
    
    
    @IBAction func btnDisJointTable(_ sender: Any) {
        
        
        delegate.disJointTabletCell(nameEstablisment: nameEstablisment, numberTable: numberTable, id_desk_user: idDeskUser)
        
    }
    
    
    
    
    
    func infoReceive(history: [String], author: [String], time: [String], status_request_bill: Bool, status_request_waiter: Bool) {
        
        callWaiter = status_request_waiter
        AskForAccount = status_request_bill
        
        
        if callWaiter{
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_on.png"),for: .normal)
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            //GRISS
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_gris.png"),for: .normal)
           
        }else{
            
//            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
            
        }
        
        if AskForAccount{
            
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta_on.png"),for: .normal)
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            //GRISS
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter_gray.png"),for: .normal)
            
            
        }else{
            
//            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
           
        }
        
        
        if !AskForAccount && !callWaiter{
            imBtnPedirCuenta.setImage(UIImage(named: "ic_pedir_cuenta.png"),for: .normal)
            imgBtnCallWaiter.setImage(UIImage(named: "ic_btn_call_waiter.png"),for: .normal)
            imgBtnCancel.setImage(UIImage(named: "ic_cancel_call_waiter_off.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = false
        }else{
            
            imgBtnCancel.setImage(UIImage(named: "ic_btn_cancel.png"),for: .normal)
            imgBtnCancel.isUserInteractionEnabled = true
            
            
        }
        
        self.delegate.reloadData()
//        LoadingOverlay.shared.hideOverlayView()
//        self.isUserInteractionEnabled = true
     
    }
    
    func connectedSocketFail() {
        
        
    }
    
    func openAlert(code: Int) {
       
        self.delegate.openAlert(code:code)
        
    }
    
    
    
    
    
    
    
}
