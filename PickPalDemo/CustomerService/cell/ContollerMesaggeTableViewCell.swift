//
//  ContollerMesaggeTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 21/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ContollerMesaggeTableViewCell: UITableViewCell {

    @IBOutlet weak var containerLbl: UILabel!
    @IBOutlet weak var lblTimermesaggeController: UILabel!
    @IBOutlet weak var lblMesaggeController: UILabel!
    @IBOutlet weak var lblNameController: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        containerLbl.layer.borderColor = UIColor.gray.cgColor
        containerLbl.layer.borderWidth = 1.0
    }
    
    func loadInfoCard(info: userMessages)  {
        
        
        print(info)
       
        lblTimermesaggeController.text = info.time
        lblMesaggeController.text = info.message
        lblNameController.text = info.user
        
    }
    
}
