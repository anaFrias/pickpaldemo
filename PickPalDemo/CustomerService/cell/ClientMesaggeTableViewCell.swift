//
//  ClientMesaggeTableViewCell.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 21/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ClientMesaggeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTimerMesaggeClient: UILabel!
    @IBOutlet weak var lblMesaggeClient: UILabel!
    @IBOutlet weak var lblNameClient: UILabel!
    
    @IBOutlet weak var containarCell: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func loadInfoCard(info: userMessages)  {
        
        
        print(info)
       
        lblTimerMesaggeClient.text = info.time
        lblMesaggeClient.text = info.message
        lblNameClient.text = info.user
        
    }
    
}
