//
//  ChatHistoryServices.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 27/04/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Starscream

protocol ChatDelegate {
    func infoReceive(history: [String], author: [String],time:[String], status_request_bill: Bool,status_request_waiter: Bool )
    func connectedSocketFail()
    func openAlert(code: Int)
}

class ChatHistoryServices : WebSocketDelegate {
    var history = [String]()
    var author = [String]()
    var time = [String]()
    let defaults:UserDefaults = UserDefaults.standard
    var userEmail = UserDefaults.standard.object(forKey: "first_name") as! String
    var socket : WebSocket!
    var isConnected = false
    var delegate:ChatDelegate!
    var from = ""
    var callWaiter = false
    var AskForAccount = false
    var codeAlert = Int()
    var isHome = Bool()
    var sendMessageController = Bool()
    var str = String()
    
    
    func connectedSocket(id_desk_user: Int,history: [String], author: [String],time:[String], callWaiter: Bool, AskForAccount: Bool, isHome: Bool){
        
        self.time = time
        self.history = history
        self.author = author
        
        self.callWaiter = callWaiter
        self.AskForAccount = AskForAccount
        
        let postURL = URL(string: "\(Constants.webSocket)\(Constants.generalChat)\(id_desk_user)/")
        var postRequest = URLRequest(url: postURL!)
        print("Conectando: \(postURL)")
        postRequest.timeoutInterval = 5
        socket = WebSocket(request:postRequest)
        socket.delegate = self
        
        self.isHome = isHome

        if !isHome{
            
            socket.connect()
        }
        
    }
    
    func closeSocket(){
        
        socket.disconnect()
        
    }
    

    func sendMessage(message: String,code: Int) {

        print("Menseje enviado: \(message)")
        let dataenc = message.data(using: String.Encoding.utf8, allowLossyConversion: true)
            
        let encodevalue = String(data: dataenc!, encoding: String.Encoding.utf8)
        let codeSend = "\", \"code\":\"" + String(code) + "\"}"
         str = "{\"user_name\":\"" + userEmail  + "\", \"message\":\"" + encodevalue! + codeSend
        sendMessageController = true
        codeAlert = code
        
    }
    
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        
        switch event {
        
        case .connected(let headers):
            isConnected = true
            
            if sendMessageController{
                self.socket.write(string: str)
               
            }
           
            print("websocket está conectado: \(headers)")
            break
            
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket está desconectado: \(reason) with code: \(code)")
            break
        case .text(let string):
            print("Texto recibido: \(string)")
        
            if let data = string.data(using: .utf16){
                
                let jsonData = try? JSONSerialization.jsonObject(with: data)
                let jsonDict = jsonData as? [String: Any]
                
                let messageType = jsonDict!["message"] as? String
                let handle = jsonDict!["user_name"] as? String
                let timestamp = jsonDict!["timestamp"] as? String
                let message_id = jsonDict!["id"] as? String
                
                let status_request_waiter = jsonDict!["status_request_waiter"] as? Bool
                let code = jsonDict!["code"] as? Int
                let status_request_bill = jsonDict!["status_request_bill"] as? Bool
                
                var historyAux = [String]()
                var authorAux = [String]()
                var timeAux = [String]()
                
                historyAux = history
                authorAux = author
                timeAux = time
                
                time.removeAll()
                history.removeAll()
                author.removeAll()
                
                time.append(timestamp!)
                history.append(messageType!)
                author.append(handle!)
                
                history += historyAux
                time += timeAux
                author += authorAux

                if sendMessageController{
                    delegate.openAlert(code: codeAlert)
                    sendMessageController = false
                }
                
                self.delegate!.infoReceive(history: history, author: author, time: time, status_request_bill: status_request_bill!, status_request_waiter: status_request_waiter!)
                
                if isHome{
                    socket.disconnect()
                    print("websocket está desconectado")
                    
                }
                
                
            }
                        
            break
            
        case .binary(let data):
            print("Datos recibidos: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            print("Error soket: \(error)")
        }
    }
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("socket connected")
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        

    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
//        print(text, "did receive message")
//        guard let data = text.data(using: .utf16),
//            let jsonData = try? JSONSerialization.jsonObject(with: data),
//            let jsonDict = jsonData as? [String: Any],
//            let messageType = jsonDict["message"] as? String,
//            let handle = jsonDict[" "] as? String,
//            let timestamp = jsonDict["time"] as? String,
//            let message_id = jsonDict["user"] as? String else {
//                return
//        }
//        let start = timestamp.index(timestamp.startIndex, offsetBy: 11)
//        let end = timestamp.index(timestamp.endIndex, offsetBy: -16)
//        let range = start..<end
//        let substrg = timestamp.substring(with: range)
//
//
//        time.append(substrg)
//        history.append(messageType)
//        author.append(handle)
//       
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {

    }

}
