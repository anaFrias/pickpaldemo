//
//  ZoomViewViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/08/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import Nuke
protocol zoomDelegate {
    func addOrderDelegate(cantIndividual: Int, totalItemPrice: Double, totalItemCant: Int, name: String, index: IndexPath, price: Double, id: Int, isPackage: Bool)
}

class ZoomViewViewController: UIViewController, UIScrollViewDelegate {

    
    @IBOutlet weak var description_label: UILabel!
    @IBOutlet weak var product_name: UILabel!
    @IBOutlet weak var pagination: UIPageControl!
    @IBOutlet weak var slider: UIScrollView!
    @IBOutlet weak var establishment_name: UILabel!
    
    //vars
    var stablishment_name = String()
    var productNameString = String()
    var description_text = String()
    var images = [String]()
    var price = Double()
    var indexPath = IndexPath()
    var id = Int()
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var isPackage = Bool()
    var delegate: zoomDelegate!
//    var isComplex: Bool!
    override func viewDidLoad() {
        super.viewDidLoad()
        description_label.text = description_text
        product_name.text = productNameString
        establishment_name.text = stablishment_name
        slider.minimumZoomScale = 1.0
        slider.maximumZoomScale = 3.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = slider.frame.size.width * CGFloat(index)
                frame.size = slider.frame.size
                
                let view = UIView(frame: frame)
                view.bounds.size.height = slider.frame.size.height
                //            view.backgroundColor = UIColor.red
                let image = UIImageView()
                if images[index] != "" {
                    Nuke.loadImage(with: URL(string: images[index])!, into: image)
                }
                
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: slider.frame.size)
                view.clipsToBounds = true
                image.contentMode = .scaleAspectFit
                image.clipsToBounds = true
                
                slider.addSubview(image)
                
                width += Int(slider.frame.width)
                
                self.slider.addSubview(view)
            }
            slider.contentSize = CGSize(width: (slider.frame.size.width * CGFloat(images.count)), height: slider.frame.size.height)
            slider.delegate = self
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pagination.numberOfPages = images.count
        pagination.currentPage = Int(pageNumber)
        pagination.currentPageIndicatorTintColor = UIColor.white
        pagination.size(forNumberOfPages: images.count)
    }
    @IBAction func addToMyOrder(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: {
            self.delegate.addOrderDelegate(cantIndividual: 1, totalItemPrice: self.price, totalItemCant: 1, name: self.productNameString, index: self.indexPath, price: self.price, id: self.id, isPackage: self.isPackage)
            })
        
    }
    @IBAction func closeZoom(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
