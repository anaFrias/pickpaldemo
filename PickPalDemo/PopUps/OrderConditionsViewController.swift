//
//  OrderConditionsViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 6/30/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class OrderConditionsViewController: UIViewController {
    
    @IBOutlet weak var viewContainerConditions: UIView!
    @IBOutlet weak var textoInicial: UILabel!
    @IBOutlet weak var textoIntermedio: UILabel!
    
    
    var primertexto = "1. Tu pedido deberá de ser entregado por el repartidor del comercio en el mismo día en que realizaste tu compra. De lo contrario se efectuará un reembolso automático a tu tarjeta por el 100% del importe de tu pedido. Recibirás una notifiación una vez realizado."
    var segundoTexto = "2. El Costo de envío es estipulado por el comercio. Este podrá variar dependiendo de la dirección de entrega."
    var tercertexto = "3. Cualquier aclaración sobre un pedido que no pueda resolver el comercio, por favor comunícate con nosotros ingresando desde la apliacion al botón de Ajustes> Ayuda> selecciona el pedido con el que necesites ayuda y rellena el formulario."
    
    @IBOutlet weak var textoFinal: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewContainerConditions.layer.cornerRadius = 10
        
        textoInicial.text = primertexto
        textoIntermedio.text = segundoTexto
        textoFinal.text = tercertexto

    }
    
    @IBAction func OnCloseDialog(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
