//
//  RemoveAddressViewController.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 26/08/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol confirmationRemoveAddress {
    func removeAddress()
    func cancel()
}

class RemoveAddressViewController: UIViewController {
   
    var delegate: confirmationRemoveAddress!
    @IBOutlet weak var labelNameAddress: UILabel!

    @IBAction func cancelRemoveAddress(_ sender: Any) {
        self.delegate.cancel()
    }
    
    @IBAction func closeAlert(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    

    @IBOutlet weak var removeAdreess: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
