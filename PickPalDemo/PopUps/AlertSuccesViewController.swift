//
//  AlertSuccesViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol closeModalProtocol {
    func goCheckout()
}
class AlertSuccesViewController: UIViewController {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var titleString = String()
    var delegate: closeModalProtocol!
    var singleStablishmentFlag = false
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = titleString
        viewContainer.layer.cornerRadius = 5
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
//            self.dismiss(animated: true, completion: {
//                if !self.singleStablishmentFlag {
//                     self.delegate.goCheckout()
//                }
//            })
//        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            if !self.singleStablishmentFlag {
                self.delegate.goCheckout()
            }
            
            })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
