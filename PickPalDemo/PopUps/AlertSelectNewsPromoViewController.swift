//
//  AlertSelectNewsPromoViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 2/13/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol AlertSelectNewsPromoDelegate{
    func readyToCreate(isNews: Bool)
}

class AlertSelectNewsPromoViewController: UIViewController {

    @IBOutlet weak var lblNameEstab: UILabel!
    @IBOutlet weak var imgRadioNews: UIImageView!
    @IBOutlet weak var imgRadioPromo: UIImageView!
    @IBOutlet weak var btnReady: UIButton!
    
    var mNameEstablishment = String()
    var isNews: Bool!
    var delegate: AlertSelectNewsPromoDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNameEstab.text = mNameEstablishment
        btnReady.isEnabled = false
    }
    
    @IBAction func closeDialog(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectNews(_ sender: Any) {
        btnReady.backgroundColor = UIColor(red: 37/255, green: 238/255, blue: 127/255, alpha: 1.0)
        imgRadioNews.image = UIImage(named: "fillRoundedCheckBox")
        imgRadioPromo.image = UIImage(named: "emptyRoundedCheckBox")
        btnReady.isEnabled = true
        isNews = true
    }
    
    @IBAction func selectPromo(_ sender: Any) {
        btnReady.backgroundColor = UIColor(red: 37/255, green: 238/255, blue: 127/255, alpha: 1.0)
        imgRadioPromo.image = UIImage(named: "fillRoundedCheckBox")
        imgRadioNews.image = UIImage(named: "emptyRoundedCheckBox")
        btnReady.isEnabled = true
        isNews = false
    }
    
    @IBAction func selectReady(_ sender: Any) {
        if isNews != nil {
            self.dismiss(animated: true, completion: nil)
            self.delegate.readyToCreate(isNews: self.isNews)
        }
        
    }
}
