//
//  AlertInfoPopUpviewController.swift
//  PickPalTesting
//
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertInfoPopUpviewController: UIViewController {
    
    @IBOutlet weak var conteinerInfoPopUp: UIView!
    
    @IBOutlet weak var alertInformacionView: UIView!
    @IBOutlet weak var lblTextGreen: UILabel!
    
    @IBOutlet weak var lblWallet: UILabel!
    
    var isWallet = false
    var textGreen  = ""
    @IBAction func cerrarDialog(_ sender: Any) {
       
      
        
         self.dismiss(animated: true, completion: nil)
    }
    
    
    var mTitle = String()
    var mMessage = String()
    
    @IBOutlet weak var conteinerView: UIView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelDescripcion: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitulo.text = mTitle
        labelDescripcion.text = mMessage
        lblWallet.visibility = !isWallet ? .gone : .visible
        
        lblWallet.text = textGreen
        conteinerView.clipsToBounds = false
        conteinerView.layer.masksToBounds = true
        conteinerView.layer.cornerRadius = 10

    }

   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
