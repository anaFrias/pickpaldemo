//
//  AlertAdsViewController.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

@objc protocol AlertAdsDelegate {
    @objc optional func deletePromoById(id: Int)
    @objc optional func closeDialogAndExit(toHome: Bool)
}

class AlertAdsViewController: UIViewController {
    
    @IBOutlet weak var lblTittle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblNewPoints: UILabel!
    @IBOutlet weak var btnWhats: UIButton!
    @IBOutlet weak var containerHeigth: NSLayoutConstraint!
    
    @IBOutlet weak var imgImageHeader: UIImageView!
    
    var delegate: AlertAdsDelegate!
    var mTitle = String()
    var mMessage = String()
    var mTitleButton = String()
    var mId = Int()
    var mToHome = Bool()
    var isFromAdd = Bool()
    var valueCall = Int()
    var isFromHome = false
    var isShowPoints = false
    var hiddenBtnWhats = true
    var msnWhatsApp = String()
    var expander = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTittle.adjustsFontForContentSizeCategory = true
        lblMessage.adjustsFontForContentSizeCategory = true
        lblTittle.text = mTitle
        lblMessage.text = mMessage
        btnOk.setTitle(mTitleButton, for: UIControlState.normal)
        
      //  btnWhats.isHidden = hiddenBtnWhats
        
        if hiddenBtnWhats{
            btnWhats.isHidden = true
        }else{
            btnWhats.isHidden = false
        }
        
        if valueCall == 4{//Propio de puntos moviles
            imgImageHeader.image = UIImage(named: "ic_header_interchange")
        }else if valueCall == 5{
            imgImageHeader.image = UIImage(named: "ic_header_transfer")
        }else if valueCall == 6{
            imgImageHeader.image = UIImage(named: "ic_header_interchange")
        }
        
        if isShowPoints{
            lblNewPoints.isHidden = false
        }else{
            lblNewPoints.isHidden = true
        }
        
        if expander{
            
            containerHeigth.constant = 460
            
        }
        
        
    }

    @IBAction func closeDialog(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        if valueCall == 3{//Para puntos moviles se agrega el campo 3 en value call,para poder redirigir a ciertas pantallas
            self.delegate.deletePromoById!(id: self.mId)
        }else{
            if isFromHome{
                self.delegate.closeDialogAndExit!(toHome: mToHome)
            }
        }
    }
    
    @IBAction func onClickButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        switch valueCall {
        case 1: //Evento que valida si las promociones fuern aliminadas por completo o aun no se agregan ninguna promo
            self.delegate.closeDialogAndExit!(toHome: mToHome)
        case 2://Evento especial para Pickpal Promos y Puntos Moviles
            print("No se hace nada porque no quiero que se haga algo")
            break
        case 3, 4, 5,6://Evento especial para Pickpal Puntos Moviles
            self.delegate.deletePromoById!(id: valueCall)
            break
        default:
            if isFromAdd{
                self.delegate.closeDialogAndExit!(toHome: mToHome)
            }else{
                self.delegate.deletePromoById!(id: self.mId)
            }
        }
    }
    @IBAction func btnSendWhatsApp(_ sender: Any) {
        
        
        print("Mensaje whats ", msnWhatsApp)
        
        let date = Date()
        let urlWhats = "whatsapp://send?text=\(msnWhatsApp)"

        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.openURL(whatsappURL as URL)
                } else {
                    print("please install watsapp")
                }
            }
        }
    }
}
