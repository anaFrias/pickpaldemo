//
//  AlertConditionsTwoViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 6/30/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class AlertConditionsTwoViewController: UIViewController {
    
    @IBOutlet weak var labelInicial: UILabel!
    @IBOutlet weak var containerConditions: UIView!
    @IBOutlet weak var labelSecundario: UILabel!
    
    var primerTexto = "1. El plazo para recoger tu pedido es de 5 días hábiles a partir de tu compra."
    var segundoTexto = "2. En caso contrario, tendrás 10 días naturales para pasar al comercio ha efectuar el reembolso de tu pedido menos el costo de penalidad. Expirado este tiempo, tu compra se dará por cancelada."
    
    @IBAction func closeAlertController(_ sender: Any) {
        
         self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerConditions.layer.cornerRadius = 10
        
        labelInicial.text = primerTexto
        labelSecundario.text = segundoTexto
        
    }
    
}
