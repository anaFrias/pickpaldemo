//
//  AlertAcceptRequestExchange.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 28/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit


protocol AlertAcceptRequestExchangeDelegate {
    func AcceptRequestExchange(total_to_exchange: Int)
}

class AlertAcceptRequestExchange: UIViewController {
    @IBOutlet weak var lblPuntosSolicitados: UILabel!

    @IBOutlet weak var lblTituloIntercambio: UILabel!
    @IBOutlet weak var viewConteiner: UIView!
    @IBOutlet weak var lblPuntosIntercambiar: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    
    @IBAction func btnAceptarIntercambio(_ sender: Any) {
        
        
        print("Aceltar intercambio")
        self.delegate.AcceptRequestExchange(total_to_exchange:infoToExchange.totalToExchange)
        
    }
    
    @IBAction func btnCancelarIntercambio(_ sender: Any) {
        
        print("Nel mijo al intercambio")
        self.dismiss(animated: true, completion: nil)
    }
    
    var delegate: AlertAcceptRequestExchangeDelegate!
    var textEstablestFrom = String()
    var textEstablestTo = String()
    var textTitle  = String()
    var textMenssage  = String()
    var totalToExchange = Int()
     var infoToExchange = TransferDetails()
    var attributedString: NSAttributedString!
    var stringStreet: NSAttributedString!
    let completeTextFrom = NSMutableAttributedString(string: "")
    let completeTextTo = NSMutableAttributedString(string: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewConteiner.clipsToBounds = false
        viewConteiner.layer.masksToBounds = false
        viewConteiner.layer.cornerRadius = 20
        viewConteiner.layer.shadowColor = UIColor.black.cgColor
        viewConteiner.layer.shadowOpacity = 0.35
        viewConteiner.layer.shadowOffset = .zero
        viewConteiner.layer.shadowRadius = 5
        
        
        /*
         alertintercambio.textTitle = "¡Un usuario quiere intercambiar puntos contigo!"

         alertintercambio.textEstablestFrom = "\(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentFromName!)\n  \(infoToExchange.establishmentFromStreet!)"
         alertintercambio.textEstablestTo = "\(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentTomName!)\n \(infoToExchange.establishmentTomStreet!)"
                 
         */
        
        let attributesEstablishment: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller", size: 14.0)!
        ]
        let attributesStreet: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor(red: 0.35, green: 0.41, blue: 0.45, alpha: 1.00),
            .font: UIFont(name: "Aller-Italic", size: 12)!
        ]
        
        
        attributedString = NSAttributedString(string: "\(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentTomName!)\n", attributes: attributesEstablishment)
        
         stringStreet = NSAttributedString(string: " \(infoToExchange.establishmentTomStreet!)", attributes: attributesStreet)
         
        
  
        completeTextFrom.append(attributedString)
         completeTextFrom.append(stringStreet)
        lblPuntosIntercambiar.attributedText =  completeTextFrom
        
        
        attributedString = NSAttributedString(string: "\(infoToExchange.totalToExchange!) pts de \(infoToExchange.establishmentFromName!)\n", attributes: attributesEstablishment)
       
        stringStreet = NSAttributedString(string: " \(infoToExchange.establishmentFromStreet!)", attributes: attributesStreet)
        
        
       
         completeTextTo.append(attributedString)
         completeTextTo.append(stringStreet)
        
        lblPuntosSolicitados.attributedText = completeTextTo
        
        lblTituloIntercambio.text! = textTitle
        lblMessage.text! = textMenssage
        
    
    }



}
