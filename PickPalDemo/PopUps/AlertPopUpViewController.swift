//
//  AlertPopUpViewController.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
protocol OrderBelongsOtherRestaurantDelegate {
    func orderBelong(indexPath: IndexPath, name: String, cants: Int, price: Double, totalItemPrice: Double, totalItemCant: Int, id: Int, isDelegate: Bool, isPackage: Bool)
    func orderCancel(eraseAll: Bool)
}
class AlertPopUpViewController: UIViewController, CustomerServiceDelegate,AlertAdsDelegate {

    @IBOutlet weak var titulo: UILabel!
    
    var defaults = UserDefaults.standard
    var delegate: OrderBelongsOtherRestaurantDelegate!
    var index = IndexPath()
    var name = String()
    var price = Double()
    var cant = Int()
    var totalItemPrice = Double()
    var totalItemCant = Int()
    var idItm = Int()
    var isPackage = Bool()
    var customerService = CustomerServiceWS()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customerService.delegate = self
        
        if let first_name = UserDefaults.standard.object(forKey: "first_name") as? String {
            titulo.text = first_name + " tu pedido actual pertenece a otro restaurante, ¿Deseas continuar con tu pedido actual o comprar en otro establecimiento?"
        }else{
            titulo.text = "Tu pedido actual pertenece a otro restaurante, ¿Deseas continuar con tu pedido actual o comprar en otro establecimiento?"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.delegate.orderCancel(eraseAll: false)
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteEverything(_ sender: Any) {

        self.dismiss(animated: true, completion: nil)
        if let order_elements = UserDefaults.standard.object(forKey: "order_elements") as? [Dictionary<String,Any>] {
            UserDefaults.standard.removeObject(forKey: "order_elements")
            UserDefaults.standard.removeObject(forKey: "last_id")
        }
        customerService.checkStatusJoindtableOrder(client_id: UserDefaults.standard.object(forKey: "client_id") as! Int , establishment_id: DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id))
        
        //        self.delegate.orderCancel(eraseAll: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func didCheckStatusJoindtable(){
        
        self.delegate.orderBelong(indexPath: index, name: name, cants: cant, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: idItm, isDelegate: true, isPackage: isPackage)
        
    }
    
    
    
    func didFailCheckStatusJoindtable(error: String, subtitle: String){
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = subtitle
        newXIB.mTitle = error//"\(UserDefaults.standard.object(forKey: "first_name")!)"
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    
    }
    

}
