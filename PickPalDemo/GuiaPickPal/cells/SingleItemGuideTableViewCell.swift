//
//  SingleItemGuideTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/24/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class SingleItemGuideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productDesc: UILabel!
    @IBOutlet weak var nameProduct: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    
    var cant = 0
    var priceString = 0.00
    var delegate: singleItemDelegate!
    var indexPath: IndexPath!
    var name: String!
    var totalPrice = Double()
    var totalCant = Int()
    var idItem = Int()
    var isPackage = Bool()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}


