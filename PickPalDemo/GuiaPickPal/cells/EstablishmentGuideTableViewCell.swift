//
//  EstablishmentGuideTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

@objc protocol EstablishmentGuideDelegate: class {
    @objc optional func OnGoToGuideSingle(id: Int, business_area: String, place: String, service: Bool, km: String)
    
    @objc optional func OnGoSections(from: Int)
}

class EstablishmentGuideTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgEstablishment: UIImageView!
    @IBOutlet weak var lblNameEstab: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewContentButtons: UIView!
    @IBOutlet weak var lblGuidePlus: UILabel!
    
    @IBOutlet weak var btnFoodAndDrinks: UIButton!
    @IBOutlet weak var btnPromos: UIButton!
    @IBOutlet weak var btnGuidePickPal: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    
    
    var delegate: EstablishmentGuideDelegate!
    var mId = 0
    var mBussinesArea = ""
    var mPlace = ""
    var isServicePlus: Bool!
    var kilometers: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func goFoodAndDrinks(_ sender: Any) {
        self.delegate.OnGoSections?(from: 1)
    }
    
    @IBAction func goPromos(_ sender: Any) {
        self.delegate.OnGoSections?(from: 2)
    }
    
    @IBAction func goGuidePickPal(_ sender: Any) {
        self.delegate.OnGoSections?(from: 3)
    }
    
    @IBAction func goSingle(_ sender: Any) {
        //self.delegate.OnGoToGuideSingle?(id: mId, business_area: "", business_area: )
        self.delegate.OnGoToGuideSingle?(id: mId, business_area: mBussinesArea, place: mPlace, service: isServicePlus, km: kilometers ?? "")
    }
    
}
