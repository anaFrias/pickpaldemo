//
//  CatsCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

protocol CategoryDelegate {
    func onSelectCat(nameCat: String)
}

class CatsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var nameCategory: UILabel!
    @IBOutlet weak var shapeCategory: UIView!
    
    var delegate: CategoryDelegate!
    var catEnable = true
    
    @IBAction func OnClickCat(_ sender: Any) {
        if catEnable{
            self.delegate.onSelectCat(nameCat: nameCategory.text ?? "")
        }
    }
}
