//
//  FilterByCriteriasTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

@objc protocol FilterByCriteriasDelegate: class {
    @objc optional func ShowStateCriterias()
    
    @objc optional func ShowCategoryCriterias()
    
    @objc optional func SearchByCriterias()
}

class FilterByCriteriasTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewCriteriasLeft: UIView!
    @IBOutlet weak var lblEstado: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    
    var delegate: FilterByCriteriasDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCriteriasLeft.clipsToBounds = true
        viewCriteriasLeft.layer.cornerRadius = 20
        viewCriteriasLeft.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func OnShowState(_ sender: Any) {
        self.delegate.ShowStateCriterias?()
    }
    
    @IBAction func OnShowCategory(_ sender: Any) {
        self.delegate.ShowCategoryCriterias?()
    }
    
    @IBAction func OnSearchByCriterias(_ sender: Any) {
        self.delegate.SearchByCriterias?()
    }
    
}
