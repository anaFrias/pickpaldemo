//
//  LocationUserTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class LocationUserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblLocationUser: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
