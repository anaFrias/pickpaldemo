//
//  CategoriesGuideTableViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke

protocol SelectCategoryDelegate {
    func OnSelectCategory(category: String)
}

class CategoriesGuideTableViewCell: UITableViewCell, CategoryDelegate {
    
    @IBOutlet weak var collectionCat: UICollectionView!
    
    var mGuideCategories = [GuideCategories]()
    var delegate: SelectCategoryDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionCat.delegate = self
        collectionCat.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func onSelectCat(nameCat: String) {
        self.delegate.OnSelectCategory(category: nameCat)
    }

}

extension CategoriesGuideTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mGuideCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemOne = collectionView.dequeueReusableCell(withReuseIdentifier: "CatsCollection", for: indexPath) as! CatsCollectionViewCell
        itemOne.nameCategory.text = mGuideCategories[indexPath.row].name
        itemOne.shapeCategory.isHidden = !mGuideCategories[indexPath.row].hide
        itemOne.catEnable = !mGuideCategories[indexPath.row].hide
        if let logo = mGuideCategories[indexPath.row].image, logo != "" {
            Nuke.loadImage(with: URL(string: logo)!, into: itemOne.imgCategory)
        }
        itemOne.delegate = self
        return itemOne
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if mGuideCategories.count > 0 {
            return CGSize(width: 113.0, height: 113.0)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
}
