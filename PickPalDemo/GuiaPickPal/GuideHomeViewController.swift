//
//  GuideHomeViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/18/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import Nuke
import Firebase
import SQLite
import OneSignal

class GuideHomeViewController: UIViewController, googleWSDelegate, userDelegate, CLLocationManagerDelegate,
    GuideDelegate, AlertAdsDelegate, EstablishmentGuideDelegate, setValuesDelegate, FilterByCriteriasDelegate,
selectedSearchValue, SelectCategoryDelegate {
    
    @IBOutlet weak var viewNewsFeed: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var newFeedButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var tableHome: UITableView!
    @IBOutlet weak var newsFeedTotal: UILabel!
    
    var valueInfo = String()
    var valuePlace = String()
    var valueCat = String()
    var valueState = String()
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    
    //    Location variables
    var locationManager = CLLocationManager()
    var myCurrentLocation:CLLocationCoordinate2D!
    var latitudes = [Double]()
    var longitudes = [Double]()
    
    var gws = GoogleServices()
    var userws = UserWS()
    var guideWS = GuidePickPalWS()
    
    var mGuideCategoriesList = [GuideCategories]()
    var mEstablishmentsList = [EstablishmentsGuide]()
    var mIdList = [IdsKm]()
    
    var structures = [String]()
    
    var ref: DatabaseReference!
    var ids = [Int]()
    var defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DatabaseFunctions.shared.createDB()
        registerDeviceNotifications()
        self.setLocationData()
        OneSignal.setSubscription(true)
        tableHome.delegate = self
        tableHome.dataSource = self
        tableHome.separatorStyle = UITableViewCellSeparatorStyle.none
        structures.removeAll()
        ref = Database.database().reference().child("establishmentsguide")
        
        let client_id = UserDefaults.standard.object(forKey: "client_id") as! Int
        userws.getUnreadNewsFeedLog(client_id: client_id)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        gws.delegate = self
        userws.delegate = self
        userws.isUpdateAvailable()
        guideWS.delegate = self
        LoadingOverlay.shared.showOverlay(view: self.view)
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            let clocation = locationManager.location
            if locationManager.location == nil{
//                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//                    locationManager.requestWhenInUseAuthorization()
//                }
               initlocationManager()
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: 0, longitude: 0))
            }else{
                userLat = (clocation?.coordinate.latitude)!
                userLng = (clocation?.coordinate.longitude)!
                /*userLat = 20.5880600
                userLng = -100.3880600*/
//                print(userLat)
//                print(userLng)
                
                gws.getCurrentPlace(location: CLLocationCoordinate2D(latitude: userLat, longitude: userLng))
            }
        }
    }
    
    @IBAction func goToLastOrders(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goToSettings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                newVC.isFromPromos = false
                newVC.isFromGuide = true
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            newVC.isFromPromos = false
            newVC.isFromGuide = true
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func didSuccessRegisterDeviceNotifications() {
        OneSignal.setSubscription(true)
        print("success on register notification")
    }
    
    func didFailRegisterDeviceNotifications(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
//        print(title)
    }
    
    func registerDeviceNotifications(){
        if let userId = OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId {
//            print(userId)
            userws.registerDeviceNotifications(client_pk: UserDefaults.standard.object(forKey: "client_id") as! Int, token: userId)
        }
        
    }
    
    func setLocationData(){
        if let mun = UserDefaults.standard.object(forKey: "municipalty") as? String {
            valueInfo = mun  == "Santiago de Querétaro" ? "Querétaro": mun
        }else{
            valueInfo = "Estado"
        }
        
        if let mun = UserDefaults.standard.object(forKey: "place") as? String {
            valuePlace = mun
        }else{
            valuePlace = "Ubicación"
        }
        
        if let mun = UserDefaults.standard.object(forKey: "category") as? String {
            valueCat = mun
        }else{
            valueCat = "Giro de negocio"
        }
        
        if let st = UserDefaults.standard.object(forKey: "state") as? String {
            valueState = st
        }else{
            valueState = ""
        }
    }
    
    func didSuccessGetGeolocation(location: String, state: String) {
        let geoCoder = CLGeocoder()
        /*userLat = 20.5880600
        userLng = -100.3880600*/
        let location2 = CLLocation(latitude: userLat, longitude: userLng)
        self.setLocationData()
        geoCoder.reverseGeocodeLocation(location2, completionHandler: { (placemarks, error) -> Void in
            if let placeMark = placemarks?[0] {
                if let state = placeMark.administrativeArea {
                    self.valueState = state
                    UserDefaults.standard.set(state, forKey: "state")
                }else{
                  if let st = UserDefaults.standard.object(forKey: "state") as? String {
                        self.valueState = st
                    }
                }
                // City
               
                if location != ""{
                    self.valueInfo = location == "Santiago de Querétaro" ? "Querétaro": location
                    UserDefaults.standard.set(location, forKey: "municipalty")
                }else{
                   if let loc = UserDefaults.standard.object(forKey: "municipalty") as? String {
                       self.valueInfo = loc
                   }
                }
            }
            self.guideWS.viewEstablishmentAroundGuide(latitude: self.userLat, longitude: self.userLng, codeState: self.valueState.stripingDiacritics)
            //self.guideWS.viewEstablishmentAroundGuide(latitude: 20.5880600, longitude: -100.3880600, codeState: self.valueState)
        })
    }
    
    func didSuccessGetViewEstablishmentAround(info: [IdsKm], ids: [Int]) {
//        print(info)
        self.mIdList = info
        if info.count > 0 {
            guideWS.getGuideEstablishmentsById(ids: ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }else{
            LoadingOverlay.shared.hideOverlayView()
            let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
            newXIB.modalTransitionStyle = .crossDissolve
            newXIB.modalPresentationStyle = .overCurrentContext
            newXIB.mMessage = "No se encontraron establecimientos, vuelve a intentarlo más tarde"
            newXIB.mTitle = "PickPal Carta Digital"
            newXIB.mTitleButton = "Aceptar"
            newXIB.mId = 0
            newXIB.isFromHome = true
            newXIB.valueCall = 1
            newXIB.isFromAdd = true
            newXIB.delegate = self
            self.present(newXIB, animated: true, completion: nil)
            
            let pro = Promotional()
            pro.isEmpty = true
            
            structures = ["Hello", "Criterias", "nearEstabs", "LocationUser"]
            tableHome.reloadData()
        }
    }
    
    func deletePromoById(id: Int) {
        
    }
    
    func closeDialogAndExit(toHome: Bool) {
        
    }
    
    func didFailGetViewEstablishmentAround(error: String, subtitle: String) {
        print(subtitle)
        LoadingOverlay.shared.hideOverlayView()
        //mostrar mensaje de error
    }
    
    func didSuccessGetGuideEstablshmentsById(info: [EstablishmentsGuide]) {
//        print(info)
        structures = ["Hello", "Criterias", "nearEstabs", "LocationUser"]
        for _ in info{
            structures.append("GuideEstabs")
        }
        mEstablishmentsList = info
        
        for id in mIdList{
            for estab in mEstablishmentsList{
                if id.id == estab.id {
                    estab.kms = id.Km
                }
            }
        }
        
        guideWS.getCategoriesGuide(codeState: self.valueState.stripingDiacritics)
        
    }
    
    func didFailGetGuideEstablshmentsById(error: String, subtitle: String) {
        print(subtitle)
        LoadingOverlay.shared.hideOverlayView()
        //Si falla aun se tiene que consultar la cuadricula de cuadriculas
        guideWS.getCategoriesGuide(codeState: self.valueState.stripingDiacritics)
    }
    
    func didSuccessGetGuideCategories(info: [GuideCategories]) {
        self.mGuideCategoriesList = info
        structures.append("Categories")
        tableHome.reloadData()
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func didFailGetGuideCategories(error: String, subtitle: String) {
        //mostrar mensaje de error
        LoadingOverlay.shared.hideOverlayView()
        tableHome.reloadData()
    }
    
    func selectedSearchId(id: Int) {
        var tempids = [Int]()
        tempids.append(id)
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
        nwVC.ids = tempids
        nwVC.isFromPromos = false
        nwVC.userLat = self.userLat
        nwVC.userLng = self.userLng
        nwVC.valueCity = self.valueInfo
        nwVC.valueCategory = self.valueCat
        nwVC.valuePlace = self.valuePlace
        nwVC.valueState = self.valueState
        nwVC.mIdList = self.mIdList
        nwVC.from = "guide"
        nwVC.isFromGuide = true
        self.defaults.set(self.ids, forKey: "ids_establishment")
        self.navigationController?.pushViewController(nwVC, animated: true)
    }
    
    
    func OnGoToGuideSingle(id: Int, business_area: String, place: String, service: Bool, km: String) {
        print("hola")
        let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment_guide") as! SingleGuideCollectionViewCell
        newVC.id = id
        newVC.municipioStr = valueInfo
        newVC.categoriaStr = business_area
        newVC.lugarString = place
        newVC.establString = ""
        newVC.isServicePlus = service
        newVC.kilometers = km
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func OnGoSections(from: Int) {
        
    }
    
    @IBAction func onGoToSelectHome(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "MenuPromos") as! MenuViewController
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goNewsFeed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "NewsFeed") as! NewsFeedViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    
    func didSuccessGetNewsFeedLog(newsFeed: Int) {
        if newsFeed > 0 {
            //            newsFeedTotal.text = "\(newsFeed)"
            newsFeedTotal.text = "\(1)"
            viewNewsFeed.alpha = 1
        }else{
            viewNewsFeed.alpha = 0
        }
        
    }
    
    
    func didFailGetNewsFeedLog(title: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func openModalAndSingle(modal: Bool, place: Bool, category: Bool, city: Bool) {
        if modal {
            newFeedButton.alpha = 0
            searchButton.alpha = 0
            viewNewsFeed.alpha = 0
            homeButton.alpha = 0
            if place {
                let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                newXIB.modalTransitionStyle = .coverVertical
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.titleString = "Estado"
                newXIB.municipio =  valueInfo
                newXIB.categoria = valueCat
                newXIB.places = true
                newXIB.delegate = self
                newXIB.from = "guide"
                newXIB.state = valueState
                present(newXIB, animated: true, completion: nil)
            }else if category {
                let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                newXIB.modalTransitionStyle = .coverVertical
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.titleString = "Giro de negocio"
                newXIB.municipio = valueInfo
                newXIB.categoria = "none"
                newXIB.places = false
                newXIB.categories = true
                newXIB.delegate = self
                newXIB.from = "guide"
                newXIB.state = valueState
                present(newXIB, animated: true, completion: nil)
            }else{
                let newXIB = ChooserModalsViewController(nibName: "ChooserModalsViewController", bundle: nil)
                newXIB.modalTransitionStyle = .coverVertical
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.titleString = "Estado"
                newXIB.municipio = "none"
                newXIB.categoria = "none"
                newXIB.places = true
                newXIB.delegate = self
                newXIB.state = valueState
                present(newXIB, animated: true, completion: nil)
            }
        }else{
            print("cat", valueCat)
            print("place", valuePlace)
            if valueCat != "Giro de negocio" && valuePlace != "Ubicación" {
                ref.observe(.value, with: { (snapshot) -> Void in
                    self.ids.removeAll()
                    print("Hola 2",self.valueState)
                    if snapshot != nil{
                        if let state = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary {
                            print(self.valueInfo)
                            if let singleState = state.value(forKey: self.valueInfo) as? NSDictionary {
                                print("entrasingle state",singleState)
                                for item in singleState.allValues as NSArray {
                                    let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                    let categories = (item as! NSDictionary).value(forKey: "business_line_name") as! String
                                    if categories == self.valueCat {
                                        self.ids.append(ids)
                                    }
                                }
                            }
                        }
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
                        nwVC.ids = self.ids
                        nwVC.isFromPromos = false;
                        nwVC.userLat = self.userLat
                        nwVC.userLng = self.userLng
                        nwVC.valueCity = self.valueInfo
                        nwVC.valueCategory = self.valueCat
                        nwVC.valuePlace = self.valuePlace
                        nwVC.valueState = self.valueState
                        nwVC.typeKitchen = false
                        nwVC.from = "guide"
                        nwVC.mIdList = self.mIdList
                        nwVC.isFromGuide = true
                        self.navigationController?.pushViewController(nwVC, animated: true)
                    }
                })
            }else{
                searchByCategory(catgoty: "")
            }
        }
    }
    
    func searchByCategory(catgoty: String) {
        ref.observe( .value, with: { (snapshot) -> Void in
            print("?", snapshot)
            self.ids.removeAll()
            if !(snapshot.value is NSNull) {
                if let stateQuery = (snapshot.value as! NSDictionary).value(forKey: self.valueState) as? NSDictionary{
                    print(stateQuery)
                    print(self.valueInfo)
                    let state = self.valueInfo.elementsEqual("Santiago de Querétaro") ? "Querétaro" : self.valueInfo
                    if let singleState = (stateQuery.value(forKey: state) as? NSDictionary)  {
                        for item in singleState.allValues as NSArray {
                            if catgoty.isEmpty{
                                let ids = (item as! NSDictionary).value(forKey: "id") as! Int
                                self.ids.append(ids)
                            }else{
                                if catgoty == (item as! NSDictionary).value(forKey: "business_line_name") as! String{
                                    self.ids.append((item as! NSDictionary).value(forKey: "id") as! Int)
                                }
                            }
                            
                        }
                    }
                }
            }
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let nwVC = storyboard.instantiateViewController(withIdentifier: "Filter_results") as! FilterResultsViewController
            nwVC.ids = self.ids
            nwVC.isFromPromos = false
            nwVC.valueCity = self.valueInfo
            nwVC.valueCategory = self.valueCat
            nwVC.valuePlace = self.valuePlace
            nwVC.valueState = self.valueState
            nwVC.userLat = self.userLat
            nwVC.userLng = self.userLng
            nwVC.typeKitchen = false
            nwVC.from = "guide"
            nwVC.mIdList = self.mIdList
            nwVC.isFromGuide = true
            self.navigationController?.pushViewController(nwVC, animated: true)
            
        })
    }
    
    func setValue(value: String, place: Bool, category: Bool, state: String, isType: Bool, idBussinessLine: String) {
        newFeedButton.alpha = 1
        searchButton.alpha = 1
        homeButton.alpha = 1
        if place {
            valueInfo = value
            valueState = state
        }else if category {
            valueCat = value
        }else{
            valuePlace = value
        }
        
        tableHome.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
    }
    
    func closeModal() {
        newFeedButton.alpha = 1
        searchButton.alpha = 1
        homeButton.alpha = 1
    }
    
    func ShowStateCriterias() {
        openModalAndSingle(modal: true, place: true, category: false, city: false)
    }
    
    func ShowCategoryCriterias() {
        openModalAndSingle(modal: true, place: false, category: true, city: false)
    }
    
    func SearchByCriterias() {
        openModalAndSingle(modal: false, place: false, category: false, city: false)
    }
    
    func OnSelectCategory(category: String) {
        print("Categoria Filtro: ",category)
        searchByCategory(catgoty: category)
        
    }
    
    @IBAction func OnSearchFilter(_ sender: Any) {
        let newXIB = SearchViewController(nibName: "SearchViewController", bundle: nil)
        newXIB.modalTransitionStyle = .coverVertical
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.delegate = self
        newXIB.isFromPromos = false
        newXIB.state = self.valueState
        newXIB.municipality = self.valueInfo
        newXIB.from = "guide"
        present(newXIB, animated: true, completion: nil)
    }
    
    func getIconsServicePickPal (data: [String]) -> [String]{
        var icons = [String]()
        
        
        for service1 in data{
            if service1 == "POD" {//Pickpal Order guide_icon_food_and_drinks
                icons.append("guide_icon_food_and_drinks")
            }
        }
        
        for service2 in data{
            if service2 == "PPR" {//Pickpal Promos guide_icon
                icons.append("guide_icon_promos")
            }
        }
        
        for service3 in data{
            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
                icons.append("guide_icon")
            }
        }
        
        for service4 in data{
            if service4 == "PPM" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("guide_icon_points")
            }
        }
        
        
        return icons
    }
    
}

extension GuideHomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return structures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(structures[indexPath.row]){
        case "Hello":
            let cell = tableView.dequeueReusableCell(withIdentifier: "helloCell", for: indexPath) as! HelloTableViewCell
            cell.selectionStyle = .none
            
            let date = Date()
            let calendar = Calendar.current
            
            let hour = calendar.component(.hour, from: date)
            let minute = calendar.component(.minute, from: date)
            let second = calendar.component(.second, from: date)
            
            print("Hora actual: \(hour):\(minute):\(second)")
            
            var welcome = ""
            
            if (hour < 12) {// Check if hour is between 8 am and 11pm
                welcome = "¡Buenos días! "
            } else if (hour <= 18) {
                welcome = "¡Buenas tardes! "
            } else {
                welcome = "¡Buenas noches! "
            }
            
            let greenColor = UIColor(red: 37/255, green: 210/255, blue: 115/255, alpha: 1)
            let attributedStringColor = [NSAttributedStringKey.foregroundColor : greenColor];
            let welcomeAttr = NSAttributedString(string: (UserDefaults.standard.object(forKey: "first_name") as? String ?? ""), attributes: attributedStringColor)
            let hi = NSAttributedString(string: welcome, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13.0)])
            
            let newString = NSMutableAttributedString()
            newString.append(hi)
            newString.append(welcomeAttr)
            cell.lblHelloUser.attributedText = newString
            return cell
            
        case "Criterias":
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterByCriterias") as! FilterByCriteriasTableViewCell
            cell.selectionStyle = .none
            cell.lblEstado.text = self.valueInfo
            cell.lblCategory.text = self.valueCat
            cell.delegate = self
            return cell
        case "nearEstabs":
            let cell = tableView.dequeueReusableCell(withIdentifier: "guideCell")!
            cell.selectionStyle = .none
            return cell
        case "LocationUser":
            let cell = tableView.dequeueReusableCell(withIdentifier: "locationUser") as! LocationUserTableViewCell
            cell.selectionStyle = .none
            let name = NSAttributedString(string: "Al parecer estas en ", attributes: [NSAttributedString.Key.font: UIFont(name: "Aller", size: 13)])
            let location = NSAttributedString(string: self.valueInfo, attributes: [NSAttributedString.Key.font: UIFont(name: "Aller-Bold", size: 13)])
            
            let newString = NSMutableAttributedString()
            newString.append(name)
            newString.append(location)
            cell.lblLocationUser.attributedText = newString
            return cell
        case "GuideEstabs":
            if self.mEstablishmentsList.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "establishmentsGuide") as! EstablishmentGuideTableViewCell
                cell.selectionStyle = .none
                cell.delegate = self
                cell.isServicePlus = mEstablishmentsList[indexPath.row - 4].service_plus
                cell.kilometers = mEstablishmentsList[indexPath.row - 4].kms
                cell.mId = mEstablishmentsList[indexPath.row - 4].id
                cell.mBussinesArea = mEstablishmentsList[indexPath.row - 4].business_area
                cell.mPlace = mEstablishmentsList[indexPath.row - 4].place
                cell.lblNameEstab.text = mEstablishmentsList[indexPath.row - 4].name
                if let kms = mEstablishmentsList[indexPath.row - 4].kms{
                    cell.lblGuidePlus.text = mEstablishmentsList[indexPath.row - 4].service_plus ? "Guía Plus | " + kms : "Guía Básica | " + kms
                }else{
                    cell.lblGuidePlus.text = mEstablishmentsList[indexPath.row - 4].service_plus ? "Guía Plus" : "Guía Básica"
                }
                
                cell.lblDescription.text = mEstablishmentsList[indexPath.row - 4].business_area + " - " + mEstablishmentsList[indexPath.row - 4].place
                if let image = mEstablishmentsList[indexPath.row - 4].image, image != "" {
                    Nuke.loadImage(with: URL(string: image)!, into: cell.imgEstablishment)
                }
                
                if mEstablishmentsList[indexPath.row - 4].pickpal_services.count > 0 {
//                    let icons = getIconsServicePickPal(data: mEstablishmentsList[indexPath.row - 4].pickpal_services)
                    let icons = CustomsFuncs.getIconsServicePickPal(data:mEstablishmentsList[indexPath.row - 4].pickpal_services)
//                    switch icons.count {
//                    case 3:
//                        cell.btnFoodAndDrinks.isHidden = false
//                        cell.btnPromos.isHidden = false
//                        cell.btnGuidePickPal.isHidden = false
//                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
//                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
//                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
//                        break
//
//                    case 2:
//                        cell.btnFoodAndDrinks.isHidden = false
//                        cell.btnPromos.isHidden = false
//                        cell.btnGuidePickPal.isHidden = true
//                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
//                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
//                        break
//
//                    case 1:
//                        cell.btnFoodAndDrinks.isHidden = false
//                        cell.btnPromos.isHidden = true
//                        cell.btnGuidePickPal.isHidden = true
//                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
//                        break
//
//                    default:
//                        cell.btnFoodAndDrinks.isHidden = false
//                        cell.btnPromos.isHidden = false
//                        cell.btnGuidePickPal.isHidden = false
//
//                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
//                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
//                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
//                        break
//                    }
                    
                    switch icons.count {
                    case 4:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = false
                        cell.btnPoints.isHidden = false
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
                        cell.btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                        break
                    case 3:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = false
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        cell.btnGuidePickPal.setImage(UIImage(named: icons[2]), for: .normal)
                        break
                        
                    case 2:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = false
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        cell.btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                        break
                        
                    case 1:
                        cell.btnFoodAndDrinks.isHidden = false
                        cell.btnPromos.isHidden = true
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        cell.btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                        break
                        
                    default:
                        cell.btnFoodAndDrinks.isHidden = true
                        cell.btnPromos.isHidden = true
                        cell.btnGuidePickPal.isHidden = true
                        cell.btnPoints.isHidden = true
                        break
                    }
                    
                        cell.btnFoodAndDrinks.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                        cell.btnPromos.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                        cell.btnGuidePickPal.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                        cell.btnPoints.imageView?.contentMode = UIViewContentMode.scaleAspectFit
                    
                }
                
                return cell
            } else {
                return UITableViewCell()
            }
            
        default:
            if self.mGuideCategoriesList.count > 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "categories") as! CategoriesGuideTableViewCell
                cell.mGuideCategories = self.mGuideCategoriesList
                cell.selectionStyle = .none
                cell.delegate = self
                cell.collectionCat.reloadData()
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    //Filtros por palabra.- Nombre, giro y tipo de producto
    //llaves firebase.- nombre,food_type, bussines
    private func numberOfSections(in tableView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch(structures[indexPath.row]){
        case "Hello":
            return 94
        case "Criterias":
            return 75
        case "nearEstabs":
            return 60
        case "LocationUser":
            return 31.5
        case "GuideEstabs":
            return self.mEstablishmentsList.count > 0 ? 255 : 0
        default:
            return self.mGuideCategoriesList.count > 0 ? 408 : 0
        }
    }
}


extension GuideHomeViewController{
    
   
    
    func initlocationManager(){
        
        let authorizationStatus = CLLocationManager.authorizationStatus()

        if (authorizationStatus == CLAuthorizationStatus.denied) {
            getLocation()
        }else{
            
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            self.navigationController?.isNavigationBarHidden = true
           
        }
        
    }

    func getLocation() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .denied {
                let alertController = UIAlertController (title: "¡Oh no!", message: "¡Necesitamos tu ubicación para encontrar los comercios cercanos a ti!", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Configuración", style: .cancel) { (_) -> Void in
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                                
                        })
                    }
                }
                alertController.addAction(settingsAction)
                let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (_) -> Void in
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                  
                }
                alertController.addAction(cancelAction)
                
                present(alertController, animated: true, completion: nil)
            }
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                if CLLocationManager.authorizationStatus() == .denied  {
                    /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
                    self.navigationController?.pushViewController(newVC, animated: true)*/
                    
                }
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("autorizado")
            }
            
        } else {
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("autorizado")
            manager.requestAlwaysAuthorization()
            /*let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Home") as! HomeViewController
            self.navigationController?.pushViewController(newVC, animated: true)*/
           
            break
        case .denied, .notDetermined, .restricted:
            print("denegado")
           
        default:
            break
        }
    }
}
