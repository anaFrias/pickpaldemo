//
//  SingleGuideCollectionViewCell.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/24/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
import GoogleMaps
import MapKit
import SQLite
import Firebase
import OneSignal

class SingleGuideCollectionViewCell: UIViewController, establishmentDelegate, singleItemDelegate, OrderBelongsOtherRestaurantDelegate, UIScrollViewDelegate, zoomDelegate, deleteCarritoDelegate, errorMessageDelegate, isAlcoholicDelegate, GuideDelegate,PointsWSDelegate,AlertAdsDelegate,
    PromosDelegate{
    
    var id: Int!
    var guideWS = GuidePickPalWS()
    var ws = EstablishmentWS()
    var municipio: String!
    var categoria: String!
    var lugar: String!
    var establecimiento: String!
    
    @IBOutlet weak var heightStuffView: NSLayoutConstraint!
    @IBOutlet weak var itemCollectionCons: NSLayoutConstraint!
    var images = [""]
    var totalPrice = 0.00
    var defaults = UserDefaults.standard
    
    @IBOutlet weak var placeArrow: UIImageView!
    @IBOutlet weak var catArrow: UIImageView!
    @IBOutlet weak var munArrow: UIImageView!
    
    @IBOutlet weak var stuff: UIView!
    
    //    IBOutlets
    @IBOutlet weak var munLabel: UILabel!
    @IBOutlet weak var catLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    
    @IBOutlet weak var viewMyOrder: UIView!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var viewItems: UIView!
    @IBOutlet weak var totalItemsLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    //    Info
    @IBOutlet weak var establishment_logo: UIImageView!
    @IBOutlet weak var establishment_title: UILabel!
    @IBOutlet weak var descr_label: UILabel!
    
    @IBOutlet weak var schedules_text: UILabel!
    @IBOutlet weak var schedules_label: UILabel!
    @IBOutlet weak var category_text: UILabel!
    @IBOutlet weak var category_label: UILabel!
    @IBOutlet weak var descr_text: UILabel!
    
    @IBOutlet weak var directionDescript: UILabel!
    @IBOutlet weak var directionsTitleLabel: UILabel!
    @IBOutlet weak var phoneInfoLabelext: UILabel!
    @IBOutlet weak var phoneInfoLabel: UILabel!
    @IBOutlet weak var viewContainerStuff: UIView!
    @IBOutlet weak var kitchenTypeTitle: UILabel!
    @IBOutlet weak var kitchenTypeInfo: UILabel!
    
    @IBOutlet weak var btnGuia: UIButton!
    @IBOutlet weak var btnPromos: UIButton!
    @IBOutlet weak var btnFoodAndDrinks: UIButton!
    @IBOutlet weak var btnPoints: UIButton!
    
    var handle2: DatabaseHandle!
    var handle3: DatabaseHandle!
    
    var extrasObserve: DatabaseReference!
    var modifiersObserve: DatabaseReference!
    
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var scrollConstraint: NSLayoutConstraint!
    
    //Numero de pedidos
    @IBOutlet weak var numPedidos: UIView!
    //    CONTENEDOR GENERAL
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightStuff: NSLayoutConstraint!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    //    Buttons map and info
    @IBOutlet weak var buttonMap: UIButton!
    @IBOutlet weak var buttonInfo: UIButton!
    
    @IBOutlet weak var lblKmBasicPlus: UILabel!
    @IBOutlet weak var detailTableView: UITableView!
    
    
    let offset_HeaderStop:CGFloat = 250.0 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    var singleInfo = SingleEstablishment()
    var infoSection = [String]()
    var dictionaryArray =  [[Dictionary<String, AnyObject>]]()
    var cantidad = Int()
    var cantIndividual = 1
    var complements = [[Complements]]()
    var modifiers = [[Modifiers]]()
    var hasComplemets = [Bool]()
    var hasModifiers = [Bool]()
    
    var modificadores = [[String]]()
    var modificadores_precio = [[Double]]()
    
    var complementos = [[String]]()
    var complementos_precio = [[Double]]()
    var complementos_cant = [[Int]]()
    
    var cantitiesItems = [[Int]]()
    
    var ids = [Int]()
    var prices = [Double]()
    var names = [String]()
    var cantis = [Int]()
    
    var total_prices = [Double]()
    var total_cantis = [Int]()
    var total_names = [String]()
    
    var expandInfo = Bool()
    var expandMap = Bool()
    
    var cantitiesPerItem = [[Int]]()
    var cantitiesPerSection = [Int]()
    var namesPerItem = [[String]]()
    var namesPerSection = [String]()
    var index = [Int]()
    var row = [Int]()
    
    var totalItems = [[Int]]()
    var totals = [[Int]]()
    
    var municipioStr = String()
    var categoriaStr = String()
    var lugarString = String()
    var establString = String()
    var isServicePlus = Bool()
    var kilometers = ""
    
    @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var mapa: MKMapView!
    
    var moreLessFlagPerSection = [[String]]()
    var moreLessFlagPerItem = [String]()
    
    var latitud = CLLocationDegrees()
    var longitud = CLLocationDegrees()
    var totalItemsPerSection = Int()
    @IBOutlet weak var screenScrollView: UIScrollView!
    var dictionaryInfo = [Dictionary<String,Any>]()
    //    Arreglo de arreglos de dictionaries
    //    var all_orders = [dictionaryInfo]
    var modifiersItem = [Modifiers]()
    var complementsItem = [Complements]()
    var hideFilter = false
    var sectionChanged = 0
    var scrollSizes = [Dictionary<String,Any>]()
    var indexCategories = 0
    //    MARK: Know if there's a current order
    var order_done = Bool()
    var notes = String()
    var itemIds = [Int]()
    var quantity = [Int]()
    var orderBelogChangeEstblishment = false
    var itemReference: DatabaseReference!
    var handle: DatabaseHandle!
    var handleOperation: DatabaseHandle!
    
    var pmws = PromosWS()
    var pointsSw = PointsWS()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guideWS.delegate = self
        pmws.delegate = self
        ws.delegate = self
        pointsSw.delegate = self
        
        if kilometers == ""{
            lblKmBasicPlus.text = isServicePlus ? "Carta Digital Plus" : "Carta Digital Básica"
        }else{
            lblKmBasicPlus.text = isServicePlus ? "Carta Digital Plus | " + kilometers : "Carta Digital Básica | " + kilometers
        }
        screenScrollView.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        LoadingOverlay.shared.showOverlay(view: self.view)
        guideWS.viewSingleEstablishment(establishmentID: id)
        if municipioStr == "" && lugarString == "" && establString == "" && categoriaStr == ""{
            if let valueM = UserDefaults.standard.object(forKey: "municipalty") as? String {
                municipioStr = valueM
            }
            if let valueC = UserDefaults.standard.object(forKey: "category") as? String {
                categoriaStr = valueC
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "place") as? String {
                lugarString = valueP
            }
            
            if let valueP = UserDefaults.standard.object(forKey: "establishment_name") as? String {
                establString = valueP
            }
        }
        munLabel.text = municipioStr
        if categoriaStr != "Categoría" {
            catLabel.text = categoriaStr
            if lugarString != "Ubicación" {
                placeLabel.text = lugarString
            }else{
                if establString != "" {
                    placeLabel.text = establString
                    placeLabel.textColor = UIColor.init(red: 80/255, green: 104/255, blue: 114/255, alpha: 1)
                }else{
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                }
            }
        }else{
            if lugarString != "Ubicación" {
                catLabel.text = lugarString
                if establString != "" {
                    placeLabel.text = establString
                }else{
                    catLabel.alpha = 0
                    catArrow.alpha = 0
                }
            }else{
                if establString != "" {
                    catLabel.text = establString
                    catLabel.textColor = UIColor.init(red: 80/255, green: 104/255, blue: 114/255, alpha: 1)
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                }else{
                    catLabel.alpha = 0
                    catArrow.alpha = 0
                    placeLabel.alpha = 0
                    placeArrow.alpha = 0
                }
            }
        }
        // Do any additional setup after loading the view.
        
    }
    func inactivateItemsObserve(items: [Dictionary<String,Any>]) {
        print(id)
        itemReference = Database.database().reference().child("inactive_items").child("-\(id!)").child("items")
        handle = itemReference.observe(.value) { (snapshot) in
            self.guideWS.viewSingleEstablishment(establishmentID: self.id)
            
        }
    }
    
    func deleteCarrito(showView: Bool, stb_id: Int, municipio: String, categoria: String, lugar: String, estbName: String, stayInCart: Bool) {
        if showView  {
            viewMyOrder.alpha = 1
        }else{
            viewMyOrder.alpha = 0
        }
        
        order_done = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).1
        let strinr = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        if !order_done && strinr != "" {
            itemIds.removeAll()
            quantity.removeAll()
            let encoded = strinr.data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            viewMyOrder.alpha = 1
            scrollConstraint.constant = 38
            self.numPedidos.alpha = 0
            
            if let tempElements = dictionary as? [Dictionary<String,Any>] {
                var tempPrice = Double()
                var tempCant = Int()
                for temp in tempElements {
                    let double = temp["price"] as! Double
                    tempPrice += double
                    let cants = temp["quantity"] as! Int
                    tempCant += cants
                    if let arrayCan = temp["modifier_prices"] as? NSArray {
                        for arr in arrayCan {
                            let price_modif = Double(arr as! String)
                            tempPrice += price_modif!
                        }
                    }else{
                        if let arrayM = temp["modifiers"] as? NSArray {
                            for modif in arrayM {
                                if let array = modif as? NSDictionary {
                                    if let arr = array.value(forKey: "modifier_price") as? NSArray {
                                        for tempM in arr {
                                            let price_modif = tempM as! Double
                                            tempPrice += price_modif
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if let arrayC = temp["complements"] as? NSDictionary {
                        if let arr = arrayC.value(forKey: "complement_price") as? NSArray {
                            for tempC in arr {
                                let price_comp = tempC as! Double
                                tempPrice += price_comp
                            }
                        }
                    }
                    itemIds.append(temp["id"] as! Int)
                    quantity.append(temp["quantity"] as! Int)
                    //                    print(temp)
                }
                
                if tempPrice > 0 {
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already.
                    formatter.numberStyle = .currency
                    if let formattedTipAmount = formatter.string(from: Double(tempPrice) as NSNumber) {
                        totalLabel.text = "\(formattedTipAmount)"
                    }
                    totalItemsLabel.text = "\(tempCant)"
                    totalPrice = tempPrice
                    cantidad = tempCant
                }else{
                    viewMyOrder.alpha = 0
                    scrollConstraint.constant = 0
                    totalPrice = 0
                    cantidad  = 0
                }
                
                dictionaryInfo = tempElements
            }
        }
        
        if order_done {
            self.numPedidos.alpha = 1
        }else{
            self.numPedidos.alpha = 0
        }
        resetTotals()
        if dictionaryInfo.isEmpty {
            if cantitiesItems.count > 0 {
                for j in 0..<(singleInfo.menu.categories.count) {
                    var array2 = [Int]()
                    if singleInfo.menu.categories[j].items != nil {
                        for i in 0..<(singleInfo.menu.categories[j].items.count ) {
                            array2.append(0)
                        }
                    }
                    cantitiesItems[j] = array2
                }
                
            }
        }
        tableView.reloadData()
    }
    
    func resetTotals () {
        if cantitiesItems.count > 0 {
            for j in 0..<(singleInfo.menu.categories.count) {
                var array2 = [Int]()
                if singleInfo.menu.categories[j].items != nil {
                    for i in 0..<(singleInfo.menu.categories[j].items.count ) {
                        array2.append(0)
                        if itemIds.count > 0 {
                            for z in 0..<itemIds.count {
                                if itemIds[z] == singleInfo.menu.categories[j].items[i].id {
                                    array2[i] = (quantity[z])
                                }
                            }
                        }
                        
                    }
                }
                cantitiesItems[j] = array2
            }
            //            tableView.reloadData()
            //            print(cantitiesItems)
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if itemReference != nil {
            itemReference.removeObserver(withHandle: handle)
        }
        
        if extrasObserve != nil {
            extrasObserve.removeObserver(withHandle: handle2)
        }
        if modifiersObserve != nil {
            modifiersObserve.removeObserver(withHandle: handle3)
        }
        //        if modifiersObserve != nil {
        //            modifiersObserve.removeObserver(withHandle: handle3)
        //        }
    }
    override func viewWillAppear(_ animated: Bool) {
        order_done = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).1
        let strinr = DatabaseFunctions.shared.getCarrito(query: DatabaseProvider.sharedInstance.order_elements, query2: DatabaseProvider.sharedInstance.order_has_been_sent).0
        quantity.removeAll()
        itemIds.removeAll()
        viewMyOrder.alpha = 0
        scrollConstraint.constant = 0
        if !order_done && strinr != "" {
            let encoded = strinr.data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            viewMyOrder.alpha = 1
            scrollConstraint.constant = 38
            self.numPedidos.alpha = 0
            
            if let tempElements = dictionary as? [Dictionary<String,Any>] {
                var tempPrice = Double()
                var tempCant = Int()
                for temp in tempElements {
                    let double = temp["price"] as! Double
                    tempPrice += double
                    let cants = temp["quantity"] as! Int
                    tempCant += cants
                    if let arrayCan = temp["modifier_prices"] as? NSArray {
                        for arr in arrayCan {
                            let price_modif = Double(arr as! String)
                            tempPrice += price_modif!
                        }
                    }else{
                        if let arrayM = temp["modifiers"] as? NSArray {
                            for modif in arrayM {
                                if let array = modif as? NSDictionary {
                                    if let arr = array.value(forKey: "modifier_price") as? NSArray {
                                        for tempM in arr {
                                            let price_modif = tempM as! Double
                                            tempPrice += price_modif
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if let arrayC = temp["complements"] as? NSDictionary {
                        if let arr = arrayC.value(forKey: "complement_price") as? NSArray {
                            for tempC in arr {
                                let price_comp = tempC as! Double
                                tempPrice += price_comp
                            }
                        }
                    }
                    itemIds.append(temp["id"] as! Int)
                    quantity.append(temp["quantity"] as! Int)
                    //                    print(temp)
                }
                
                if tempPrice > 0 {
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already.
                    formatter.numberStyle = .currency
                    if let formattedTipAmount = formatter.string(from: Double(tempPrice) as NSNumber) {
                        totalLabel.text = "\(formattedTipAmount)"
                    }
                    totalItemsLabel.text = "\(tempCant)"
                    totalPrice = tempPrice
                    cantidad = tempCant
                }else{
                    viewMyOrder.alpha = 0
                    scrollConstraint.constant = 0
                    totalPrice = 0
                    cantidad  = 0
                }
                dictionaryInfo = tempElements
                resetTotals()
                
                //                print(itemIds, quantity)
            }else{
                
            }
        }else{
            totalPrice = 0
            cantidad = 0
            cantitiesPerSection.removeAll()
            cantitiesPerItem.removeAll()
            namesPerSection.removeAll()
            namesPerItem.removeAll()
            moreLessFlagPerItem.removeAll()
            moreLessFlagPerSection.removeAll()
            dictionaryInfo.removeAll()
        }
        inactivateItemsObserve(items: dictionaryInfo)
        if order_done {
            self.numPedidos.alpha = 1
        }else{
            self.numPedidos.alpha = 0
        }
        if dictionaryInfo.isEmpty {
            if cantitiesItems.count > 0 {
                for j in 0..<(singleInfo.menu.categories.count) {
                    var array2 = [Int]()
                    if singleInfo.menu.categories[j].items != nil {
                        for i in 0..<(singleInfo.menu.categories[j].items.count ) {
                            array2.append(0)
                        }
                    }
                    cantitiesItems[j] = array2
                }
            }
        }
        
        tableView.reloadData()
    }
    
    @IBAction func backButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Home")
        newVC.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func pedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores")
        newVC.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func settings(_ sender: Any) {
        if let invited = UserDefaults.standard.object(forKey: "invited") as? Bool {
            if !invited {
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                self.navigationController?.pushViewController(newVC, animated: true)
            }else{
                let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
                let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                })
                let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                    self.restoreSesion()
                })
                alert.addAction(aceptar)
                alert.addAction(irA)
                
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
            self.navigationController?.pushViewController(newVC, animated: true)
        }
    }
    
    @IBAction func showMap(_ sender: Any) {
        if !isServicePlus {
            if !buttonMap.isSelected{
                expandMap = false
                loadMapGuide()
            }
        }else{
            loadMapGuide()
        }
    }
    
    @IBAction func showInfo(_ sender: Any) {
        if !isServicePlus {
            if !buttonInfo.isSelected{
                expandInfo = false
                loadInfo()
            }
        }else{
            loadInfo()
        }
    }
    
    func loadInfo(){
        buttonMap.isSelected = false
        if expandInfo {
            buttonInfo.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandInfo = false
                self.itemCollectionCons.constant = 0
                self.heightStuffView.constant = 295
                self.tableViewTop.constant = 295
            }
        }else{
            buttonInfo.isSelected = true
            self.viewMap.alpha = 0
            self.detailTableView.alpha = 1
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404 - 34
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandInfo = true
            }
        }
    }
    
    func loadMapGuide(){
        buttonInfo.isSelected = false
        if expandMap {
            buttonMap.isSelected = false
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 0
            }) { (finished) in
                self.expandMap = false
                self.itemCollectionCons.constant = 0
                self.heightStuffView.constant = 295
                self.tableViewTop.constant = 295
            }
            
        }else{
            buttonMap.isSelected = true
            self.viewMap.alpha = 1
            self.detailTableView.alpha = 0
            if UIScreen.main.bounds.height == 812 || UIScreen.main.bounds.height == 896 || UIScreen.main.bounds.height == 892 {
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404 - 34
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404) - 34
                self.heightStuff.constant = UIScreen.main.bounds.height - 404 - 34
            }else{
                self.itemCollectionCons.constant = UIScreen.main.bounds.height - 404
                self.heightStuffView.constant = 295+(UIScreen.main.bounds.height - 404)
                self.tableViewTop.constant = 295+(UIScreen.main.bounds.height - 404)
                self.heightStuff.constant = UIScreen.main.bounds.height - 404
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
                self.viewContainerStuff.alpha = 1
                self.screenScrollView.setContentOffset(CGPoint.zero, animated: true)
            }) { (finished) in
                self.expandMap = true
                
            }
        }
    }
    
    @IBAction func viewOrder(_ sender: Any) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        ws.viewMyPointEstablishment(userId: String(UserDefaults.standard.object(forKey: "client_id") as! Int), establishmentId: String(DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)))
    }
    
    override func viewDidLayoutSubviews() {
        var width = 0
        if images.count > 0 {
            for index in 0...images.count - 1{
                frame.origin.x = scrollView.frame.size.width * CGFloat(index)
                frame.size = scrollView.frame.size
                
                let view = UIView(frame: frame)
                view.bounds.size.height = scrollView.frame.size.height
                //            view.backgroundColor = UIColor.red
                let image = UIImageView()
                if images[index] != "" {
                    Nuke.loadImage(with: URL(string: images[index])!, into: image)
                }
                
                image.frame = CGRect(origin: CGPoint(x: width, y: 0), size: scrollView.frame.size)
                view.clipsToBounds = true
                
                
                scrollView.addSubview(image)
                
                width += Int(scrollView.frame.width)
                
                self.scrollView.addSubview(view)
            }
            scrollView.contentSize = CGSize(width: (scrollView.frame.size.width * CGFloat(images.count)), height: scrollView.frame.size.height)
            scrollView.delegate = self
        }
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == screenScrollView {
            let offset = scrollView.contentOffset.y
            var headerTransform = CATransform3DIdentity
            
            // PULL DOWN -----------------
            if offset < 0 {
                let headerScaleFactor:CGFloat = -(offset) / self.stuff.bounds.height
                let headerSizevariation = ((self.stuff.bounds.height * (1.0 + headerScaleFactor)) - self.stuff.bounds.height)/2.0
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                self.stuff.layer.transform = headerTransform
            }else {
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                if offset <= offset_HeaderStop {
                }else {
                }
            }
            self.stuff.layer.transform = headerTransform
            let index = scrollSizes.index(where: {dict -> Bool in
                return dict.contains(where: {_ in dict["size"] as! CGFloat == scrollView.contentOffset.y})
            })
            if index != nil {
                indexCategories = (scrollSizes[index!]["index"]) as! Int
                collectionView.reloadData()
            }
            if scrollView.contentOffset.y <= 250 {
                indexCategories = 0
                collectionView.reloadData()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.numberOfPages = images.count
        pageControl.currentPage = Int(pageNumber)
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.size(forNumberOfPages: images.count)
    }
    
    func didFailSingleEstablishmentGuide(error: String, subtitle: String) {
        
    }
    
    func getIconsServicePickPal (data: [String]) -> [String]{
        var icons = [String]()
//        for service1 in singleInfo.pickpal_services{
//            if service1 == "PFD" {//Food and Drinks guide_icon_food_and_drinks
//                icons.append("guide_icon_food_and_drinks")
//            }
//        }
//
//        for service2 in singleInfo.pickpal_services{
//            if service2 == "PPR" {//Pickpal Promos guide_icon
//                icons.append("guide_icon_promos")
//            }
//        }
//
//        for service3 in singleInfo.pickpal_services{
//            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
//                icons.append("guide_icon")
//            }
//        }
        
        
        for service1 in data{
            if service1 == "POD" {//Pickpal Order guide_icon_food_and_drinks
                icons.append("guide_icon_food_and_drinks")
            }
        }
        
        for service2 in data{
            if service2 == "PPR" {//Pickpal Promos guide_icon
                icons.append("guide_icon_promos")
            }
        }
        
        for service3 in data{
            if service3 == "PGP" || service3 == "PGB" {//Guia PickPal guide_icon_promos
                icons.append("guide_icon")
            }
        }
        
        for service4 in data{
            if service4 == "PPM" {//PickPal Puntos Moviles guide_icon_promos
                icons.append("guide_icon_points")
            }
        }
        
        return icons
    }
    
    func didSuccessSingleEstablishmentGuide(info: SingleEstablishment) {
        LoadingOverlay.shared.hideOverlayView()
        images = info.gallery
        singleInfo = info
        infoSection.removeAll()
        totalItems.removeAll()
        cantitiesItems.removeAll()
        dictionaryArray.removeAll()
        scrollSizes.removeAll()
        cantitiesItems.removeAll()
        cantitiesPerItem.removeAll()
        
        if singleInfo.pickpal_services != nil {
            let icons = CustomsFuncs.getIconsServicePickPal(data: singleInfo.pickpal_services)
            switch icons.count {
            case 4:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = false
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                btnPoints.setImage(UIImage(named: icons[3]), for: .normal)
                break
            case 3:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = false
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                btnGuia.setImage(UIImage(named: icons[2]), for: .normal)
                break
                
            case 2:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = false
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                btnPromos.setImage(UIImage(named: icons[1]), for: .normal)
                break
                
            case 1:
                btnFoodAndDrinks.isHidden = false
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                btnFoodAndDrinks.setImage(UIImage(named: icons[0]), for: .normal)
                break
                
            default:
                btnFoodAndDrinks.isHidden = true
                btnPromos.isHidden = true
                btnGuia.isHidden = true
                btnPoints.isHidden = true
                break
            }
            
            btnFoodAndDrinks.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            btnPromos.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            btnGuia.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            btnPoints.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        }
        
        let latit = CLLocationDegrees(info.latitude)
        let longi = CLLocationDegrees(info.longitude)
        self.latitud = latit
        self.longitud = longi
        
        let camera = MKMapCamera(lookingAtCenter: CLLocationCoordinate2D(latitude: latit, longitude: longi), fromEyeCoordinate: CLLocationCoordinate2D(latitude: latit, longitude: longi), eyeAltitude: 400.0)
        mapa.setCamera(camera, animated: true)
        let eyeCoordinate = CLLocationCoordinate2D(latitude: latit, longitude: longi)
        let annotation = MKPointAnnotation()
        annotation.coordinate = eyeCoordinate
        mapa.addAnnotation(annotation)
        if singleInfo.company_name != nil {
            establishment_logo.alpha = 0
            establishment_title.alpha = 1
            establishment_title.text = singleInfo.name
        }
        detailTableView.reloadData()
        var e = Int()
        var includeAlcohol = [Bool]()
        for j in 0..<(info.menu.categories.count) {
            var dic = [Dictionary<String, AnyObject>]()
            infoSection.append(info.menu.categories[j].name)
            //            print(info.menu.categories[j].name)
            var array = [Int]()
            var array2 = [Int]()
            //            includeAlcohol.append(info.menu.categories[j].include_alcoholic)
            if info.menu.categories[j].items != nil {
                
                for i in 0..<(info.menu.categories[j].items.count ) {
                    e += 1
                    array.append(e)
                    array2.append(0)
                    if itemIds.count > 0 {
                        for z in 0..<itemIds.count {
                            if itemIds[z] == info.menu.categories[j].items[i].id {
                                //                                print(info.menu.categories[j].items[i].id)
                                array2[i] += (quantity[z])
                            }
                        }
                    }
                    //                    print(array2)
                    let dictionary = ["id": info.menu.categories[j].items[i].id,"images": info.menu.categories[j].items[i].images,"name": info.menu.categories[j].items[i].name,"description": info.menu.categories[j].items[i].descr,"rating": info.menu.categories[j].items[i].rating_pos ?? 0,"price": info.menu.categories[j].items[i].price, "is_package": info.menu.categories[j].items[i].is_package, "in_stock": info.menu.categories[j].items[i].in_stock ] as [String : Any]
                    dic.append(dictionary as [String : AnyObject])
                }
            }else{
                dic = []
            }
            totalItems.append(array)
            cantitiesItems.append(array2)
            dictionaryArray.append(dic)
        }
        if includeAlcohol.contains(true) {
            //            aqui debería de ir el disclaimer pero no lo pondré lol
        }
        var ind = 0
        for i in 0..<info.menu.categories.count {
            if info.menu.categories[i].items != nil {
                for _ in 0..<info.menu.categories[i].items.count {
                    ind += 1
                    totalItemsPerSection = ind
                }
            }
        }
        let scrollHeight = 295 + (infoSection.count * 24) + (ind * 140)
        if CGFloat(scrollHeight) != UIScreen.main.bounds.height {
            containerConstraint.constant = CGFloat(scrollHeight)
        }
        let aroundOffset = infoSection.count * 15
        for i in 0..<infoSection.count {
            for j in 0..<aroundOffset {
                let dict = ["size": makeItems(index: i) + CGFloat(j * 10), "index": i] as [String : Any]
                scrollSizes.append(dict)
            }
        }
        if isServicePlus {
            tableView.reloadData()
            collectionView.reloadData()
        }else{
            expandInfo = false
            loadInfo()
        }
        
        viewDidLayoutSubviews()
        print(cantitiesItems)
    }
    
    func getImage(image: String) -> Int{
        switch image {
        case "guide_icon_food_and_drinks":
            return 1
    
        case "guide_icon_promos":
            return 2
            
        default://guide_icon
            return 3
        }
    }
    
    @IBAction func goFoodAndDrinks(_ sender: Any) {
        let imagesList = getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = getImage(image: imagesList[0])
        goToNavigation(acttion: image)
    }
    
    @IBAction func goToPromos(_ sender: Any) {
        let imagesList = getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = getImage(image: imagesList[1])
        goToNavigation(acttion: image)
    }
    
    @IBAction func goToGuide(_ sender: Any) {
        let imagesList = getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = getImage(image: imagesList[2])
        goToNavigation(acttion: image)
    }
    
    
    @IBAction func goToPonits(_ sender: Any) {
        
        let imagesList = getIconsServicePickPal(data: singleInfo.pickpal_services)
        let image = getImage(image: imagesList[2])
        goToNavigation(acttion: image)
        
    }
    
    
    func showMarker(position: CLLocationCoordinate2D) {
        
    }
    
    func goToNavigation(acttion: Int){
        switch acttion {
        case 1://FoodAndDrinks
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = self.id
            newVC.municipioStr = self.municipioStr
            newVC.categoriaStr = self.categoriaStr
            newVC.lugarString = self.lugarString
            newVC.establString = ""
            self.navigationController?.pushViewController(newVC, animated: true)
            break
        case 2://Promos
            LoadingOverlay.shared.showOverlay(view: self.view)
            pmws.viewSingleAdvsEstablishment(establishmentID: self.id, clientID: UserDefaults.standard.object(forKey: "client_id") as! Int)
            break
            
        case 3:
            pointsSw.CountEstablishmentPoints()
            
            
        default: //3 default guia
            print("Ya estamos en el single daahhhhh")
            break
        }
    }
    
    
    
    func didSuccessCountEstablishmentPoints(number: Int) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PuntosPickPal", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "homePoints") as! HomePointsViewController
        let navController = UINavigationController(rootViewController: newVC)
        navController.modalTransitionStyle = .crossDissolve
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailCountEstablishmentPoints(error: String, subtitle: String) {
        self.view.isUserInteractionEnabled = true
        LoadingOverlay.shared.hideOverlayView()
        showDialog(title: "PickPal Puntos Móviles", message: "Aún no tienes Puntos Móviles. Haz compras en los comercios afiliados para ganar puntos.")
    }
    
    
    
    func showDialog(title: String, message: String) {
        
        let newXIB = AlertAdsViewController(nibName: "AlertAdsViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.mMessage = message
        newXIB.mTitle = title
        newXIB.mToHome = false
        newXIB.mTitleButton = "Aceptar"
        newXIB.mId = 0
        newXIB.valueCall = 2
        newXIB.isFromAdd = false
        newXIB.delegate = self
        self.present(newXIB, animated: true, completion: nil)
    }
    
    
    
    @IBAction func goHome(_ sender: Any) {
        let storyboard = UIStoryboard(name: "GuiaPickPal", bundle: nil)
               let newVC = storyboard.instantiateViewController(withIdentifier: "GuideHome") as! GuideHomeViewController
               self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goPedidos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "pedidosAnteriores") as! PedidosAnterioresViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func goSeetings(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        newVC.isFromPromos = false
        newVC.isFromGuide = true
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    @IBAction func openMaps(_ sender: Any) {
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(latitud),\(longitud)&zoom=14&views=traffic&q=\(latitud),\(longitud)")!, options: [:], completionHandler: nil)
        } else {
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitud, longitud)
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = establString
            mapItem.openInMaps(launchOptions: options)
        }
        
    }
    
    func addItem(price: Double, cant: Int, name: String, indexPath: IndexPath, id: Int, isPackage: Bool) {
        let cante = cant + 1
        if !names.contains(name) {
            names.append(name)
            index.append(indexPath.section)
            row.append(indexPath.row)
        }else{
            index[names.index(of: name)!] = indexPath.section
            row[names.index(of: name)!] = indexPath.row
        }
        
        for _ in 0..<infoSection.count {
            if cantitiesPerItem.count < infoSection.count {
                //                    cantitiesItems.append([])
                cantitiesPerItem.append([])
                namesPerItem.append([])
                moreLessFlagPerSection.append([])
            }
        }
        
        for i in 0..<index.count {
            if index[i] != indexPath.section {
                cantitiesPerSection.removeAll()
                namesPerSection.removeAll()
                moreLessFlagPerItem.removeAll()
            }
        }
        for _ in 0..<dictionaryArray[indexPath.section].count {
            if cantitiesPerSection.count < dictionaryArray[indexPath.section].count {
                cantitiesPerSection.append(0)
                namesPerSection.append("")
                moreLessFlagPerItem.append("")
            }
        }
        cantitiesPerSection[indexPath.row] = cante
        cantitiesPerItem[indexPath.section] = cantitiesPerSection
        namesPerSection[indexPath.row] = name
        namesPerItem[indexPath.section] = namesPerSection
        moreLessFlagPerItem[indexPath.row] = "add"
        moreLessFlagPerSection[indexPath.section] = moreLessFlagPerItem
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    func deleteItem(price: Double, cant: Int, name: String, indexPath: IndexPath, id: Int, isPackage: Bool) {
        let cante = cant - 1
        if names.contains(name) {
            index.remove(at: names.index(of: name)!)
            row.remove(at: names.index(of: name)!)
            names.remove(at: names.index(of: name)!)
        }
        if moreLessFlagPerSection.count > 0 {
            moreLessFlagPerSection[indexPath.section][indexPath.row] = "delete"
        }
        if cantitiesPerSection.count > 0 {
            cantitiesPerItem[indexPath.section][indexPath.row] = cante
            //            cantitiesItems[indexPath.section][indexPath.row] = cante
            namesPerItem[indexPath.section][indexPath.row] = name
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func sendItem(cantIndividual: Int, totalItemPrice: Double, totalItemCant: Int, name: String, index: IndexPath, price: Double, id: Int, isPackage: Bool) {
        if let age = UserDefaults.standard.object(forKey: "age") as? Int  {
            if age >= 18 {
                if DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != 0 && DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != self.id {
                    let newXIB = AlertPopUpViewController(nibName: "AlertPopUpViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.index = index
                    newXIB.name = name
                    newXIB.cant = cantIndividual
                    newXIB.price = price
                    newXIB.totalItemPrice = totalItemPrice
                    newXIB.totalItemCant = totalItemCant
                    newXIB.idItm = id
                    newXIB.delegate = self
                    present(newXIB, animated: true, completion: {
                    })
                }else{
                    orderBelong(indexPath: index, name: name, cants: cantIndividual, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: id, isDelegate: false, isPackage: isPackage)
                }
            }else{
                if singleInfo.menu.categories[index.section].items[index.row].is_alcoholic {
                    let newXIB = AlcoholicDrinksDisclaimerViewController(nibName: "AlcoholicDrinksDisclaimerViewController", bundle: nil)
                    newXIB.modalTransitionStyle = .crossDissolve
                    newXIB.modalPresentationStyle = .overCurrentContext
                    newXIB.index = index
                    newXIB.name = name
                    newXIB.cant = cantIndividual
                    newXIB.price = price
                    newXIB.totalItemPrice = totalItemPrice
                    newXIB.totalItemCant = totalItemCant
                    newXIB.idItm = id
                    newXIB.delegate = self
                    newXIB.nombreDelComercio = singleInfo.name
                    newXIB.establishment_id = self.id
                    present(newXIB, animated: true, completion: {
                    })
                }else{
                    if DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != 0 && DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != self.id {
                        let newXIB = AlertPopUpViewController(nibName: "AlertPopUpViewController", bundle: nil)
                        newXIB.modalTransitionStyle = .crossDissolve
                        newXIB.modalPresentationStyle = .overCurrentContext
                        newXIB.index = index
                        newXIB.name = name
                        newXIB.cant = cantIndividual
                        newXIB.price = price
                        newXIB.totalItemPrice = totalItemPrice
                        newXIB.totalItemCant = totalItemCant
                        newXIB.idItm = id
                        newXIB.delegate = self
                        present(newXIB, animated: true, completion: {
                        })
                    }else{
                        orderBelong(indexPath: index, name: name, cants: cantIndividual, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: id, isDelegate: false, isPackage: isPackage)
                    }
                }
                
            }
        }else{
            if DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != 0 && DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id) != self.id {
                let newXIB = AlertPopUpViewController(nibName: "AlertPopUpViewController", bundle: nil)
                newXIB.modalTransitionStyle = .crossDissolve
                newXIB.modalPresentationStyle = .overCurrentContext
                newXIB.index = index
                newXIB.name = name
                newXIB.cant = cantIndividual
                newXIB.price = price
                newXIB.totalItemPrice = totalItemPrice
                newXIB.totalItemCant = totalItemCant
                newXIB.idItm = id
                newXIB.delegate = self
                present(newXIB, animated: true, completion: {
                })
            }else{
                orderBelong(indexPath: index, name: name, cants: cantIndividual, price: price, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, id: id, isDelegate: false, isPackage: isPackage)
            }
        }
        
        
    }
    
    
    func confirtNewOrder(indexPath: IndexPath, name: String, cants: Int, price: Double, totalItemPrice: Double, totalItemCant: Int, id: Int, isDelegate: Bool, isPackage: Bool) {
        let newXIB = AlertPopUpViewController(nibName: "AlertPopUpViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.index = indexPath
        newXIB.name = name
        newXIB.cant = cantIndividual
        newXIB.price = price
        newXIB.totalItemPrice = totalItemPrice
        newXIB.totalItemCant = totalItemCant
        newXIB.idItm = id
        newXIB.delegate = self
        present(newXIB, animated: true, completion: {
        })
    }
    
    
    func zoomIn(indexPath: IndexPath) {
        //        if (singleInfo.menu.categories[indexPath.section].items[indexPath.row].modifiers?.count)! == 0 && (singleInfo.menu.categories[indexPath.section].items[indexPath.row].complements?.count)! == 0{
        //
        //
        //        }else{
        if(singleInfo.menu.categories[indexPath.section].items[indexPath.row].category.uppercased() != "BEBIDAS" || !singleInfo.menu.categories[indexPath.section].items[indexPath.row].in_stock) {
            if (singleInfo.menu.categories[indexPath.section].items[indexPath.row].images.count > 0) {
                if singleInfo.menu.categories[indexPath.section].items[indexPath.row].images[0] != "" {
                    let zoomXIB = ZoomViewViewController(nibName: "ZoomViewViewController", bundle: nil)
                    zoomXIB.modalTransitionStyle = .coverVertical
                    zoomXIB.modalPresentationStyle = .overCurrentContext
                    zoomXIB.description_text = "\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["description"]!))"
                    zoomXIB.productNameString = "\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["name"]!))"
                    //                        print("\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["name"]!))")
                    zoomXIB.stablishment_name = establString
                    if let array = dictionaryArray[indexPath.section][indexPath.row]["images"] as? [String]{
                        if array.count > 0 {
                            zoomXIB.images = array
                        }
                    }
                    if let isPackage = dictionaryArray[indexPath.section][indexPath.row]["is_packge"] as? Bool {
                        zoomXIB.isPackage = isPackage
                    }
                    
                    zoomXIB.price = Double("\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["price"]!))")!
                    zoomXIB.indexPath = indexPath
                    zoomXIB.id = dictionaryArray[indexPath.section][indexPath.row]["id"] as! Int
                    zoomXIB.delegate = self
                    self.present(zoomXIB, animated: true, completion: nil)
                }
            }
        }
        //        }
    }
    func orderCancel(eraseAll: Bool) {
        //        defaults.removeObject(forKey: "last_id")
        if eraseAll {
            //            UserDefaults.standard.removeObject(forKey: "last_id")
            //            UserDefaults.standard.removeObject(forKey: "stb_name")
            totalPrice = 0
            cantidad = 0
            dictionaryInfo = [Dictionary<String,Any>]()
            viewMyOrder.alpha = 0
            DatabaseFunctions.shared.deleteItem()
        }else{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let newVC = storyboard.instantiateViewController(withIdentifier: "single_establishment") as! SingleEstablishmentViewController
            newVC.id = DatabaseFunctions.shared.getEstablishmentId(query: DatabaseProvider.sharedInstance.establishment_id)//defaults.object(forKey: "last_id") as! Int
            newVC.municipioStr = municipioStr
            newVC.categoriaStr = categoriaStr
            newVC.lugarString = lugarString
            newVC.orderBelogChangeEstblishment = true
            newVC.establString = DatabaseFunctions.shared.getEstablishmentName(query: DatabaseProvider.sharedInstance.establishment_name)
            self.navigationController?.pushViewController(newVC, animated: true)
            //self.navigationController?.popViewController(animated: true)
        }
        
    }
    func orderBelong(indexPath: IndexPath, name: String, cants: Int, price: Double, totalItemPrice: Double, totalItemCant: Int, id: Int, isDelegate: Bool, isPackage: Bool) {
        var invited = false
        if let inv = UserDefaults.standard.object(forKey: "invited") as? Bool {
            invited = inv
        }
        if !invited {
            if cants > 0 {
                if isDelegate {
                    totalPrice = 0
                    cantidad = 0
                    dictionaryInfo = [Dictionary<String,Any>]()
                    DatabaseFunctions.shared.deleteItem()
                }
                var isComplex = Bool()
                if moreLessFlagPerSection[indexPath.section][indexPath.row] == "delete" {
                    if (singleInfo.menu.categories[indexPath.section].items[indexPath.row].modifiers?.count)! == 0 && (singleInfo.menu.categories[indexPath.section].items[indexPath.row].complements?.count)! == 0{
                        removeSimpleItems(name: name, cants: cants, price: price)
                    }else{
                        removeComplexItems(name: name, cants: cants, price: price)
                    }
                }else{
                    if (singleInfo.menu.categories[indexPath.section].items[indexPath.row].modifiers?.count)! == 0 && (singleInfo.menu.categories[indexPath.section].items[indexPath.row].complements?.count)! == 0{
                        isComplex = false
                        createSimpleItems(name: name, cants: cants, price: price, id: id, isPackage: isPackage)
                    }else{
                        isComplex = true
                        createComplexItems(name: name, cants: cants, price: price, indexPath: indexPath, id: id, isPackage: isPackage)
                    }
                }
                self.cantIndividual = cants
                cantitiesItems[indexPath.section][indexPath.row] += cants
                totalPrice += totalItemPrice
                let generator = UIImpactFeedbackGenerator(style: .heavy)
                generator.impactOccurred()
                UIView.animate(withDuration: 0.5, animations: {
                    self.viewMyOrder.alpha = 1
                    self.scrollConstraint.constant = 38
                })
                
                cantidad += totalItemCant
                //            defaults.set(dictionaryInfo, forKey: "order_elements")
                if !isComplex {
                    let notes = DatabaseFunctions.shared.getNotes(query: DatabaseProvider.sharedInstance.notes)
                    DatabaseFunctions.shared.insertOrderElements(order_has_been_sent: false, establishment_name: establString, order_elements: dictionaryInfo, establishment_id: self.id, notes: notes)
                }
                
                
                let formatter = NumberFormatter()
                formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: Double(totalPrice) as NSNumber) {
                    totalLabel.text = "\(formattedTipAmount)"
                }
                //totalLabel.text = "$\(totalPrice)0"
                totalItemsLabel.text = "\(cantidad)"
            }
            
            for i in 0..<cantitiesPerItem.count {
                for j in 0..<cantitiesPerItem[i].count {
                    cantitiesPerItem[i][j] = 0
                }
            }
            
            tableView.reloadData()
        }else{
            let alert = UIAlertController(title: "PickPal", message: "Para acceder a esta funcionalidad es necesario registrarte.", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: { action in
            })
            let irA = UIAlertAction(title: "Ir a registro", style: .default, handler: { action in
                self.restoreSesion()
            })
            alert.addAction(aceptar)
            alert.addAction(irA)
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func createSimpleItems(name: String, cants: Int, price: Double, id: Int, isPackage: Bool) {
        
        if !dictionaryInfo.contains(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        }) {
            var notes = [String]()
            if let not = UserDefaults.standard.object(forKey: "notas") as? [String]{
                notes = not
            }
            notes.append("")
            defaults.set(notes, forKey: "notas")
            let dictionary = ["name": name,
                              "quantity": cants,
                              "price":price*Double(cants),
                              "complex": false,
                              "id": id,
                              "is_package": isPackage,
                              "price_product": price
            ] as [String : Any]
            dictionaryInfo.append(dictionary)
        }else{
            var notes = [String]()
            if let not = UserDefaults.standard.object(forKey: "notas") as? [String]{
                notes = not
            }
            notes.append("")
            defaults.set(notes, forKey: "notas")
            let index = dictionaryInfo.index(where: {dict -> Bool in
                return dict.contains(where: {_ in dict["name"] as! String == name})
            })
            let cantss = dictionaryInfo[index!]["quantity"] as! Int + cants
            let dictionary = ["name": name,
                              "quantity": cantss,
                              "price":price*Double(cantss),
                              "complex": false,
                              "id": id,
                              "is_package": isPackage,
                              "price_product": price
            ] as [String : Any]
            dictionaryInfo[index!] = dictionary
        }
        //        MARK: Alerta de agregado
    }
    func removeSimpleItems (name: String, cants: Int, price: Double) {
        let index = dictionaryInfo.index(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        })
        dictionaryInfo.remove(at: index!)
        
    }
    
    func createComplexItems (name: String, cants: Int, price: Double, indexPath: IndexPath, id: Int, isPackage: Bool) {
        defaults.set(singleInfo.menu.categories[indexPath.section].items[indexPath.row].id, forKey: "item_id")
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "single_item") as! ItemViewController
        newVC.complements = singleInfo.menu.categories[indexPath.section].items[indexPath.row].complements!
        newVC.modifiers = singleInfo.menu.categories[indexPath.section].items[indexPath.row].modifiers!
        newVC.cant = cantIndividual
        newVC.doublePrice = price
        newVC.establishmentName = singleInfo.name
        newVC.descriptionText = singleInfo.menu.categories[indexPath.section].items[indexPath.row].descr
        //        print(singleInfo.menu.categories[indexPath.section].items[indexPath.row].limit_extra!)
        newVC.limit_extra = singleInfo.menu.categories[indexPath.section].items[indexPath.row].limit_extra!
        if let array = dictionaryArray[indexPath.section][indexPath.row]["images"] as? [String]{
            if array.count > 0{
                newVC.imageItem =  array[0]
            }
        }
        newVC.idEstablishment = self.id
        newVC.dictionaryInfo = dictionaryInfo
        newVC.total = cants
        newVC.price = "$\(price)"
        newVC.itemNameString = name
        newVC.idItem = id
        newVC.isPackage = isPackage
        self.navigationController?.pushViewController(newVC, animated: true)
        //        cantitiesPerItem[indexPath.section][indexPath.row] += cantitiesPerSection[indexPath.row]
        //        print(cantitiesPerItem[indexPath.section][indexPath.row])
    }
    
    func removeComplexItems (name: String, cants: Int, price: Double) {
        let index = dictionaryInfo.index(where: {dict -> Bool in
            return dict.contains(where: {_ in dict["name"] as! String == name})
        })
        if index != nil {
            dictionaryInfo.remove(at: index!)
        }
    }
    func createHorario(horario: [ScheduleItem]) -> String{
        var i = 0;
        var j = 0;
        var horarios = [itemHorario]()
        for item in horario{
            if i == 0{
                let x = itemHorario()
                x.open = item.open_hour
                x.close = item.closing_hour
                x.addname(name: item.day)
                horarios.append(x)
                j += 1
            }else{
                if (horarios[j-1].open == item.open_hour && horarios[j-1].close == item.closing_hour){
                    horarios[j-1].addname(name: item.day)
                }else{
                    let x = itemHorario()
                    x.open = item.open_hour
                    x.close = item.closing_hour
                    x.addname(name: item.day)
                    horarios.append(x)
                    j += 1
                }
            }
            i += 1
        }
        
        var txtHorario = ""
        
        for z in 0..<horarios.count{
            if(z == horarios.count - 1){
                if(horarios[z].names.count > 1){
                    txtHorario += horarios[z].names[0] + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close
                    
                }else{
                    if(horarios[z].names[0] == "Todos"){
                        txtHorario += "Lunes - Domingo: " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }else{
                        txtHorario += horarios[z].names[0] + ": " + horarios[z].open + " - " + horarios[z].close + " hrs."
                    }
                    
                }
            }else{
                if(horarios[z].names.count > 1){
                    txtHorario +=  horarios[z].names[0]  + " a " + horarios[z].names[horarios[z].names.count - 1] + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }else{
                    txtHorario += horarios[z].names[0]  + ": " + horarios[z].open + " - " + horarios[z].close + ", ";
                }
            }
        }
        return txtHorario
    }
    func didFailGetSingleEstablishment(error: String, subtitle: String) {
        let newXIB = ErrorViewController(nibName: "ErrorViewController", bundle: nil)
        newXIB.modalTransitionStyle = .crossDissolve
        newXIB.modalPresentationStyle = .overCurrentContext
        newXIB.errorMessageString = error
        newXIB.subTitleMessageString = subtitle
        present(newXIB, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addOrderDelegate(cantIndividual: Int, totalItemPrice: Double, totalItemCant: Int, name: String, index: IndexPath, price: Double, id: Int, isPackage: Bool) {
        addItem(price: price, cant: cantIndividual, name: name, indexPath: index, id: id, isPackage: isPackage)
        sendItem(cantIndividual: cantIndividual, totalItemPrice: totalItemPrice, totalItemCant: totalItemCant, name: name, index: index, price: price, id: id, isPackage: isPackage)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func restoreSesion() {
        UserDefaults.standard.removeObject(forKey: "first_name")
        UserDefaults.standard.removeObject(forKey: "client_id")
        UserDefaults.standard.removeObject(forKey: "last_id_news_feed")
        UserDefaults.standard.removeObject(forKey: "avatar")
        UserDefaults.standard.removeObject(forKey: "invited")
        let db = try! Connection("\(path)/orders.db")
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "initialView") as! ViewController
        vc.closeSession = true
        OneSignal.setSubscription(false)
        self.navigationController?.pushViewController(vc,animated: true)
    }
    
    func didSuccessSingleAdvs(info: SingleEstabAdvs) {
        LoadingOverlay.shared.hideOverlayView()
        
        let storyboard = UIStoryboard(name: "PromosStoryBoard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "SingleAdvsAux") as! SingleAdvsAuxTwoViewController
        newVC.singleInfo = info
        newVC.id = info.id
        newVC.municipioStr = info.address.city
        newVC.categoriaStr = info.address.colony
        newVC.lugarString = info.advsCategorry
        newVC.establString = info.name
        //newVC.singleInfo = info
        
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailSingleAdvs(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        print(subtitle)
    }
    
}
extension SingleGuideCollectionViewCell: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == detailTableView {
            return 1
        }else{
            return infoSection.count
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == detailTableView {
            return 1
        }else{
            return dictionaryArray[section].count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == detailTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailEstablishmentCell", for: indexPath) as! DetailSingleGuideTableViewCell
            cell.singleInfo = singleInfo.socialInfo
            if singleInfo.descripcion != nil {
                cell.lblDescrip.text = singleInfo.descripcion
            }
            if singleInfo.busines != nil {
                cell.lblCategory.text = singleInfo.busines
            }else{
                cell.lblCategory.text = ""
            }
            if singleInfo.operation_schedule != nil {
                if singleInfo.operation_schedule.count > 0 {
                    cell.lblHorario.text = createHorario(horario: singleInfo.operation_schedule)
                }
            }
            
            
                
                var direccion = ""
                if singleInfo.address != nil {
                    direccion = singleInfo.address.street //+ " " + singleInfo.address.colony
                    
                    if singleInfo.address.noExt != nil && singleInfo.address.noExt != ""{
                        direccion = direccion + " " + singleInfo.address.noExt
                    }
                    
                    if singleInfo.address.noInt != nil && singleInfo.address.noInt != ""{
                        direccion = direccion + " - " + singleInfo.address.noInt
                    }
                    
                    if singleInfo.address.colony != nil && singleInfo.address.colony != ""{
                        direccion = direccion + " " + singleInfo.address.colony + ","
                    }
                    
                    if singleInfo.address.city != nil && singleInfo.address.city != ""{
                        direccion = direccion + " " + singleInfo.address.city
                    }
                    
                    if singleInfo.address.state != nil && singleInfo.address.state != ""{
                        direccion = direccion + ", " + singleInfo.address.state
                    }
                    
                    if singleInfo.address.zip_code != nil && singleInfo.address.zip_code != ""{
                        direccion = direccion + ", C.P. " + singleInfo.address.zip_code
                    }
                    
                    if singleInfo.address.referencePlace != nil && singleInfo.address.referencePlace != ""{
                        direccion = direccion + "\n" + singleInfo.address.referencePlace
                    }
                }
                
                
                cell.lblDireccion.text = direccion
            
            
            var countSocialMedia = 0
            if singleInfo.socialInfo != nil{
                cell.lblRedesSociales.isHidden = false
                cell.stackSocialMedia.isHidden = false
                
                if singleInfo.socialInfo.delivery == nil {
                    cell.lblServicioADomicilio.text = "No"
                }else{
                    cell.lblServicioADomicilio.text = singleInfo.socialInfo.delivery ? "Si" : "No"
                }
                
                if singleInfo.socialInfo.delivery != nil {
                    cell.lblMetodoPago.text = singleInfo.socialInfo.payment_methods
                }
                
                if singleInfo.socialInfo.siteURL == nil {
                    cell.btnWeb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnWeb.isHidden = false
                }
                
                if singleInfo.socialInfo.facebookURL == nil{
                    cell.btnFb.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnFb.isHidden = false
                }
                
                if singleInfo.socialInfo.instagramURL == nil{
                    cell.btnInsta.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnInsta.isHidden = false
                }
                
                if singleInfo.socialInfo.twitterURL == nil{
                    cell.btnTwitter.isHidden = true
                    countSocialMedia += 1
                }else{
                    cell.btnTwitter.isHidden = false
                }
                
                if countSocialMedia == 4 {
                    cell.lblRedesSociales.isHidden = true
                    cell.stackSocialMedia.isHidden = true
                }
            }else{
                cell.lblRedesSociales.isHidden = true
                cell.stackSocialMedia.isHidden = true
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "item", for: indexPath) as! SingleItemGuideTableViewCell
            cell.selectionStyle = .none
            cell.nameProduct.text = "\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["name"]!))"
            cell.isPackage = dictionaryArray[indexPath.section][indexPath.row]["is_package"] as! Bool
            if let array = dictionaryArray[indexPath.section][indexPath.row]["images"] as? [String]{
                cell.productImg.clipsToBounds = true
                cell.productImg.layer.cornerRadius = 5
                if array.count > 0 {
                    Nuke.loadImage(with: URL(string: array[0])!, into: cell.productImg)
                }else{
                    cell.productImg.image = UIImage(named: "placeholder_items")
                }
            }else{
                cell.productImg.image = UIImage(named: "placeholder_items")
            }
            cell.delegate = self
            let formatter = NumberFormatter()
            formatter.locale = Locale(identifier: "en_US") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            
            if let formattedTipAmount = formatter.string(from: Double(String(describing: dictionaryArray[indexPath.section][indexPath.row]["price"]!))! as NSNumber) {
                cell.price.text = "\(formattedTipAmount)"
            }
            
            cell.priceString = Double("\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["price"]!))")!
            cell.productDesc.text = "\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["description"]!))"
            cell.name = "\(String(describing: dictionaryArray[indexPath.section][indexPath.row]["name"]!))"
            cell.indexPath = indexPath
            cell.idItem = dictionaryArray[indexPath.section][indexPath.row]["id"] as! Int
            
            cell.tag = totalItems[indexPath.section][indexPath.row]
            if cantitiesPerItem.count == 0 {
                cell.cant = 0
            }else{
                if cantitiesPerItem[indexPath.section].count == 0 {
                    cell.cant = 0
                }else{
                    
                    cell.cant = cantitiesPerItem[indexPath.section][indexPath.row]
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == detailTableView {
            return nil
        }else{
            let header = SectionTitleViewController()
            header.nameCategory = infoSection[section]
            return header.view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == detailTableView {
            return 0
        }else{
            return 24
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == detailTableView {
            return 416
        }else{
            return 140
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        MARK: Zooming (no borrar)
        
    }
}

extension SingleGuideCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return infoSection.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itm", for: indexPath) as! ItemFoodCollectionViewCell
        cell.itemTitle.text = infoSection[indexPath.item]
        
        if indexPath.row == indexCategories {
            cell.currentIndicator.alpha = 1
        }else{
            cell.currentIndicator.alpha = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = collectionView.cellForItem(at: indexPath) as! ItemFoodCollectionViewCell
        item.currentIndicator.alpha = 1
        
        collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
        UIView.animate(withDuration: 0.2) {
            self.screenScrollView.contentOffset.y = self.makeItems(index: indexPath.row)
        }
        for i in 0..<collectionView.visibleCells.count {
            if i != indexPath.item {
                if let item = collectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? ItemFoodCollectionViewCell {
                    item.currentIndicator.alpha = 0
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = ((infoSection[indexPath.item] as? String)?.size(withAttributes: nil) ?? CGSize.zero).width + 20
        return CGSize(width: size, height: 45)
    }
    func makeItems(index: Int) -> CGFloat{
        var numberItemsScrolled = Int()
        for i in 0..<index {
            if self.singleInfo.menu.categories.count >= index {
                if self.singleInfo.menu.categories[i].items != nil {
                    numberItemsScrolled += self.singleInfo.menu.categories[i].items.count
                }
            }
        }
        let totalItemsOffset = (numberItemsScrolled * 140)
        let totalSectionsOffset = (index * 24)
        return CGFloat(250 + totalItemsOffset + totalSectionsOffset)
    }
}

extension SingleGuideCollectionViewCell{
    func didSuccessViewMyReward(reward: MyRewardEstablishment) {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        newVC.modifiers = self.modifiersItem
        newVC.complements = self.complementsItem
        newVC.establishment_id = self.id
        newVC.municipio = municipioStr
        newVC.categoria = categoriaStr
        newVC.lugar = lugarString
        newVC.esablishmentName = establString
        newVC.subtotal = totalPrice
        newVC.mMyReward = reward
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    func didFailViewMyReward(error: String, subtitle: String) {
        LoadingOverlay.shared.hideOverlayView()
        let storyboard = UIStoryboard(name: "PedidosStoryboard", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "Carrito") as! CarritoViewController
        newVC.modifiers = self.modifiersItem
        newVC.complements = self.complementsItem
        newVC.establishment_id = self.id
        newVC.municipio = municipioStr
        newVC.categoria = categoriaStr
        newVC.lugar = lugarString
        newVC.subtotal = totalPrice
        newVC.esablishmentName = establString
        newVC.mMyReward = MyRewardEstablishment()
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}

