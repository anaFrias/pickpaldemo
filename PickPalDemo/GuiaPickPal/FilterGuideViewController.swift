//
//  FilterGuideViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/26/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import CoreLocation

class FilterGuideViewController: UIViewController {

    var ids = [Int]()
    var valueState = String()
    var valueCity = String()
    var valueCategory = String()
    var valuePlace = String()
    var ws = EstablishmentWS()
    var values = [Establishment]()
    var promoValues = [PromoEstablishment]()
    var valuesItems = [KitchensComplete]()
    var defaults = UserDefaults.standard
    var typeKitchen = Bool()
    var nameTypeKitchen = String()
    var totalEstablishments = 1
    var kitchenIds = [[Int]]()
    var errorFlag = Bool()
    var from = String()
    var promws = PromosWS()
    var isFromPromos = false
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    
    var guideWS = GuidePickPalWS()
    var mEstablishmentsList = [EstablishmentsGuide]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
