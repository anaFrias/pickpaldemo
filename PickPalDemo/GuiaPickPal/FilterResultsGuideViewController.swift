//
//  FilterResultsGuideViewController.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/26/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit
import Nuke
import CoreLocation

class FilterResultsGuideViewController: UIViewController/*, GuideDelegate, EstablishmentGuideDelegate*/{
    
    /*@IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCatgory: UILabel!
    @IBOutlet weak var tableHome: UITableView!
    @IBOutlet weak var viewCriteriasLeft: UIView!*/
    
    var ids = [Int]()
    var valueState = String()
    var valueCity = String()
    var valueCategory = String()
    var valuePlace = String()
    var ws = EstablishmentWS()
    var values = [Establishment]()
    var promoValues = [PromoEstablishment]()
    var valuesItems = [KitchensComplete]()
    var defaults = UserDefaults.standard
    var typeKitchen = Bool()
    var nameTypeKitchen = String()
    var totalEstablishments = 1
    var kitchenIds = [[Int]]()
    var errorFlag = Bool()
    var from = String()
    var promws = PromosWS()
    var isFromPromos = false
    var userLat = CLLocationDegrees()
    var userLng = CLLocationDegrees()
    
    var guideWS = GuidePickPalWS()
    var mEstablishmentsList = [EstablishmentsGuide]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*guideWS.delegate = self
        tableHome.delegate = self
        tableHome.dataSource = self
        
        viewCriteriasLeft.clipsToBounds = true
        viewCriteriasLeft.layer.cornerRadius = 20
        viewCriteriasLeft.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        if ids.count > 0 {
            guideWS.getGuideEstablishmentsById(ids: ids, client_id: UserDefaults.standard.object(forKey: "client_id") as! Int)
        }*/
    }
    
  /*  @IBAction func backToHome(_ sender: Any) {
        
    }
    
    @IBAction func showState(_ sender: Any) {
    }
    
    @IBAction func showCategory(_ sender: Any) {
    }
    
    @IBAction func searchFilter(_ sender: Any) {
    }
    
    func didSuccessGetGuideEstablshmentsById(info: [EstablishmentsGuide]) {
        print(info)
        mEstablishmentsList = info
        tableHome.reloadData()
    }
    
    func didFailGetGuideEstablshmentsById(error: String, subtitle: String) {
        
    }
    
    func OnGoToGuideSingle(id: Int, business_area: String, place: String) {
        
    }
    
    func OnGoSections(from: Int) {
        
    }*/
    
}
/*
extension FilterResultsGuideViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mEstablishmentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.mEstablishmentsList.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "estabFilter") as! EstablishmentGuideTableViewCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.mId = mEstablishmentsList[indexPath.row].id
            cell.mBussinesArea = mEstablishmentsList[indexPath.row].business_area
            cell.mPlace = mEstablishmentsList[indexPath.row].place
            cell.lblNameEstab.text = mEstablishmentsList[indexPath.row].name
            cell.lblGuidePlus.text = mEstablishmentsList[indexPath.row].service_plus ? "Guía Plus | a 5 km" : "Guía Básica | a 5 km"
            cell.lblDescription.text = mEstablishmentsList[indexPath.row].place + " - " + mEstablishmentsList[indexPath.row].business_area
            if let image = mEstablishmentsList[indexPath.row].image, image != "" {
                Nuke.loadImage(with: URL(string: image)!, into: cell.imgEstablishment)
            }
            
            if mEstablishmentsList[indexPath.row].pickpal_services.count > 0 {
                cell.viewContentButtons.isHidden = false
            }else{
                cell.viewContentButtons.isHidden = true
            }
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return mEstablishmentsList.count > 0 ? 235 : 0
    }
    
    
}*/
