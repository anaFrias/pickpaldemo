//
//  Constants.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/04/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Constants: NSObject {
//    static let baseURL = "http://pickpal.iwsandbox.com/api/" //TICKETS
//    static let baseURL = "http://pickpal2.iwsandbox.com/api/" //TICKETS2 Firebase en pruebas
    
    
//    static let baseURL = "https://pickpal.com/admin/api/" //PRODUCCION firebase en producción
      //static let baseURL = "http://prevalidate.pickpal.com/admin/api/" //PRE-PRODUCCION firebase en producción
    static let baseURL = "http://demo.pickpal.com/admin/api/" //PRE-PRODUCCION firebase en producción
    static let typeTranfer = "transferencia"
    static let typeInterchange = "Intercambio"
  
//    enum Status
    static let status : [String: String] = ["0":"not_confirmed","1":"to_pay",
                                                 "2":"in_process",
                                                 "3":"ready",
                                                 "4":"delivered",
                                                 "5":"refund",
                                                 "6":"delayed",
                                                 "7":"expired",
                                                 "8":"canceled",
                                                 "9":"refund"
                                                 ]
        static let statusControllers : [String: String] = ["0":"not_confirmed","1":"new",
                                                           "2":"to_pay",
                                                           "3":"in_process",
                                                           "4":"ready",
                                                           "5":"delivered",
                                                           "6":"delayed",
                                                           "7":"expired",
                                                           "8":"canceled",
                                                           "9":"by_refund",
                                                           "10":"delivery_delayed"
        ]
    //    Instagram Keys:
        static let redirectURI = "http://www.pickpal.com/"
        static let authorizationEndPoint = "https://api.instagram.com/oauth/authorize/"
        static let accessTokenEndPoint = "https://api.instagram.com/oauth/access_token/"
        static let instagramClientID = "9abe214009774a49ba91d53dd18b4fd6"
        static let instagramSecret = "6f6b1b2bae124e25a3a93bcec39dd1b5"
        
    //    Openpay
        static let MERCHANT_ID = "mtbp27lm8fwvqb4nryd0"
        static let API_KEY = "pk_4aa000b0570c47bf9043605ac487ad93"//"sk_84f1d269e0f1413a80a7a423e3cc43c0"
        
    //    GOOGLE PLACES
        static let googleReference = "https://maps.googleapis.com/maps/api/place/photo?photoreference="
        static let googleKEY = "AIzaSyDNvBTwhgWNqavtBmJmOn8X-ZscUaisZvA"
    //    WS
        static let about = "about/"
        static let login = "login/"
        static let users = "users/"
        static let establishment = "establishment/"
        static let orders = "orders/"
        static let generals = "generals/"
        static let login_manual = "login_manual/"
        static let welcome_sliders = "welcome_slides/"
        static let menu = "menu/"
        static let notifications = "notifications/"
        static let wallets = "wallets/"
        static let helpdesk = "helpdesk/"

    //    USER WS
            static let validate_phone = "validate_phone/"
            static let register = "register/"
            static let resend_code = "resend_code/"
            static let validate_phone_code = "validate_phone_code/"
            static let validate_profile = "view_profile/"
            static let validate_email = "validate_email/"
            static let update_profile = "update_profile/"
            static let change_password = "change_password/"
            static let view_address = "view_address/"
            static let update_address = "update_address/"
            static let validate_phone_exist = "validate_phone_exist/"
            static let reset_password = "reset_password/"
            static let update_avatar = "update_avatar/"
            static let view_my_orders = "view_my_orders/"
            static let delete_address = "delete_address/"
            static let news_feet = "news_feed/"
            static let register_device = "register_device/"
            static let register_help = "register_help/"
            static let retry_billig_sat = "retry_billing_sat/"
            static let get_in_states = "get_in_states/"
            static let register_last_state = "register_last_state/"
            static let delete_account = "delete_account/"
            static let get_rewards_custom = "/get_rewards_custom/"
            static let accepted_termns_conditions = "accepted_termns_conditions/"
            static let terms_and_conditions_wallet = "terms_and_conditions_wallet/"
            static let faqs_wallet = "faqs_wallet/"
            static let get_my_transactions_from_wallet = "get_my_transactions_from_wallet/"
            static let request_generate_qr_payment = "request_generate_qr_payment/"
            static let get_tax_regimen_list = "get_tax_regimen_list/"
        
    //    ESTAblISHMENT WS
        
        static let view_all_establishment = "view_all_establishment/"
        static let view_establishment_around = "view_establishment_around/"
        static let get_sync_establishment = "get_sync_establishment/"
        static let get_sync_descriptive_establishments = "get_sync_descriptive_establishments/"
        static let get_establishments_by_ids = "get_establishments_by_ids/"
        static let get_single_establishment = "view_single_establishment/"
        //static let get_all_type_kitchen = "get_all_type_kitchen_state/"
        //static let get_all_type_kitchen = "get_all_business_line_state/"
        static let get_all_type_kitchen = "get_all_business_line_order_state/"
        static let get_evaluation = "get_evaluation/"
        static let set_evaluation = "set_evaluation/"
        static let set_evaluation_items = "set_evaluation_items/"
        static let get_fast_establishments = "get_fast_establishments/"
        static let get_fast_establishments2 = "get_fast_establishments2/"
        static let skip_evaluation = "skip_evaluation/"
        static let set_evaluation_categories = "set_evaluation_categories/"
        static let get_all_type_kitchen_single = "get_all_type_kitchen/"
        static let get_invoicing_by_pickpal = "get_invoicing_by_pickpal/"
    //    static let view_single_product = "view_single_product/"
    //    static let view_establishment_around = "view_establishment_around/"
        
    //    PAYMENT WS
        static let set_billing_addres = "new_or_update_address/"
        static let view_billing_addres = "view_address/"
        static let get_single_address = "view_single_address/"
        
       
        static let add_card = "add_card/"
        static let get_cards = "get_cards/"
        static let register_order = "register_order/"
        static let register_code = "register_code/"
        static let remove_card = "remove_card/"
        static let update_card = "update_card/"
        static let get_service_pickpal = "get_service_pickpal/"
        
        
        //OPPA
        static let add_card_oppa = "add_card_oppa/"
        static let get_cards_oppa = "get_cards_oppa/"
        static let register_order_oppa = "register_order_oppa/"
        static let update_card_oppa = "update_card_oppa/"
        static let remove_card_oppa = "remove_card_oppa/"
        
        
    //    ORDERSWS
        static let pending_order = "pending_order/"
        static let change_order_status = "change_order_status/"
        static let get_url_invoice = "get_url_invoice/"
        static let check_card = "check_card/"
        static let get_last_order = "get_last_order/"
        static let get_orders_quiz = "get_orders_quiz/"
        static let check_refund = "check_refund/"
        static let checktime_order = "checktime_order"
        static let check_points_bysubtotal = "check_points_bysubtotal/"
    //    static let get_evaluation = "get_evaluation/"
        static let get_service_pickpal_updated = "get_service_pickpal_updated/"
        static let get_refund_details = "get_refund_details/"
        
        
    //    Delivery
        static let view_address_delivery = "view_address_delivery/"
        static let new_or_update_address_delivery = "new_or_update_address_delivery/"
        static let get_price_delivery = "get_price_delivery/"
        static let delete_address_delivery = "delete_address_delivery/"
        

        //    Wallet
        static let get_my_wallets = "get_my_wallets_updated/"
        static let get_available_establishments = "get_available_establishments/"
        static let get_single_establishment_wallet = "get_single_establishment/"
        static let register_order_wallet = "register_order_wallet/"
        static let register_order_wallet_oppa = "register_order_wallet_oppa/"
        static let get_my_wallet = "get_my_wallet/"
        static let check_my_cash = "check_my_cash/"
        
        
    //    CustomerService
        static let get_my_service = "get_my_services/"
        static let get_my_single_service_client = "get_my_single_service_client/"
        static let get_my_single_service_id = "get_my_single_service_id/"
        static let join_to_table = "join_to_table/"
        static let join_to_table_order = "join_to_table_order/"
        static let view_my_signed_tables = "view_my_signed_tables/"
        static let disjoint_table = "disjoint_table/"
        static let check_disjoint_table = "check_disjoint_table/"
        static let disjoint_cart_table = "disjoint_cart_table/"

        
        // Chat
        static let webSocket = "ws://demo.pickpal.com/"
        static let generalChat = "helpdesk/chat/"
        
        
        
        
       
        
    //    MENU
        static let view_single_product = "view_single_product/"
        
    //    ABOUT WS
        static let terms_and_conditions = "terms_and_conditions/"
        static let help = "help/"
        static let about_pickpal = "about_pickpal/"
        static let establishment_help = "establishment_help/"
        static let pickpal_commission = "pickpal_commission/"
        
        static let set_read_news_feed_log = "set_read_news_feed_log/"
        static let get_unread_news_feed_log = "get_unread_news_feed_log/"
        
        static let news_feed_home = "news_feed_home/"
        static let news_feed_home_new_new = "news_feed_home_new/"
        static let news_feed_new = "news_feed_new/"
        static let retry_order = "retry_order/"
        
        // PROMOS WS
        static let view_establishment_around_h2 = "view_establishment_around_H2/"
        static let get_establishments_by_ids_h2 = "get_establishments_by_ids_H2/"
        static let get_last_promotionals = "get_last_promotionals/"
        static let suscribe = "suscribe_to/"
        static let unsuscribe = "unsuscribe_to/"
        static let view_single_advsestablishment = "view_single_advsestablishment/"
        static let get_establishments_advs = "view_my_adminestablishments/"
        static let get_last_adminpromotionals = "get_last_adminpromotionals/"
        static let delete_admin_promotional = "delete_admin_promotional/"
        static let update_admin_promotional = "update_admin_promotionalv2/"
        static let add_admin_promotional = "add_admin_promotionalv2/"
        static let status_profile = "status_profile/"
        static let count_establishment = "count_establishment_H2/"
        static let view_myavailable_establishments_points = "users/view_myavailable_establishments_points/"
        static let get_status_request_exchange = "establishment/get_status_request_exchange/"
        static let accept_request_exchange = "establishment/accept_request_exchange/"
        
           
        
        //GUIA WS
        static let count_estab_guide = "count_establishment_guide/"
        static let view_establishment_around_guide = "view_establishment_around_guide/"
        static let get_establishments_by_ids_guide = "get_establishments_by_ids_guide/"
        static let get_all_business_line_state = "get_all_business_line_state/"
        static let view_single_guide_establishment = "view_single_guide_establishment/"
        
        //Mnedero PickPal
        static let get_rewards = "get_rewards/"
        static let view_single_reward_establishment = "view_single_reward_establishment/"
        static let viewMyPoints = "view_my_point_establishment/"
        static let countpoints = "count_establishment_points/"
        static let view_my_points = "view_my_points/"
        static let transfer_commission = "about/transfer_commission/"
        static let transfer_point_user = "users/transfer_point_user/"
        static let exchange_points_estableshment = "establishment/exchange_points_estableshment/"
        static let exchange_list_pending = "establishment/exchange_list_pending/"
        static let cancelled_exchange =  "establishment/cancelled_exchange/"
        static let view_all_establishment_service_points = "establishment/view_all_establishment_service_points/"
        static let view_establishment_ordertypes = "establishment/view_establishment_ordertypes/"
       
        
        
        //Constantes para indicar de donde abres la pantalla
        static let PICKPAL_PROMOS = "PickPal_Promos"
        static let PICKPAL_FOOD_DRINKS = "PickPal_Food_And_Drinks"
        static let PICKPAL_GUIA = "PickPal_Guia"
        static let PICKPAL_PUNTOS = "PickPal_Puntos"
        static let PICKPAL_WALLET = "PickPal_Wallet"
        
    }

