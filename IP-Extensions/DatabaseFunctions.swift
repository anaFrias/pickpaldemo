//
//  DatabaseFunctions.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/1/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import SQLite

class DatabaseFunctions : NSObject {
    let db = try! Connection("\(path)/orders.db")
    class var shared: DatabaseFunctions {
        struct Static {
            static let instance: DatabaseFunctions = DatabaseFunctions()
        }
        return Static.instance
    }
    func dropDatabase() {
        try! db.run(DatabaseProvider.sharedInstance.orders.drop(ifExists: true))
    }
    func deleteAllItems() {
        let query = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == true)
        try! db.run(query.delete())
        print("se borraron los pedidos anteriores de la base")
    }
//    Insertar en el último checkout (carrito)
    func createDB() {
        do {
            let db = try Connection("\(path)/orders.db")
            try db.run(DatabaseProvider.sharedInstance.orders.create (ifNotExists: true)  { t in
                t.column(DatabaseProvider.sharedInstance.id, primaryKey: true)
                t.column(DatabaseProvider.sharedInstance.establishment_id)
                t.column(DatabaseProvider.sharedInstance.establishment_name)
                t.column(DatabaseProvider.sharedInstance.in_site)
                t.column(DatabaseProvider.sharedInstance.notes)
                t.column(DatabaseProvider.sharedInstance.billing_address_id)
                t.column(DatabaseProvider.sharedInstance.payment_method)
                t.column(DatabaseProvider.sharedInstance.discount_code)
                t.column(DatabaseProvider.sharedInstance.total)
                t.column(DatabaseProvider.sharedInstance.estimated_time)
                t.column(DatabaseProvider.sharedInstance.pickpal_comission)
                t.column(DatabaseProvider.sharedInstance.total_after_checkout)
                t.column(DatabaseProvider.sharedInstance.order_has_been_sent)
                t.column(DatabaseProvider.sharedInstance.code)
                t.column(DatabaseProvider.sharedInstance.order_number)
                t.column(DatabaseProvider.sharedInstance.folio)
                t.column(DatabaseProvider.sharedInstance.register_date)
                t.column(DatabaseProvider.sharedInstance.process_time)
                t.column(DatabaseProvider.sharedInstance.ready_time)
                t.column(DatabaseProvider.sharedInstance.pay_time)
                t.column(DatabaseProvider.sharedInstance.order_id)
                t.column(DatabaseProvider.sharedInstance.order_elements)
                t.column(DatabaseProvider.sharedInstance.statusItem)
                t.column(DatabaseProvider.sharedInstance.qr)
                t.column(DatabaseProvider.sharedInstance.ttl)
                t.column(DatabaseProvider.sharedInstance.is_cash)
                t.column(DatabaseProvider.sharedInstance.last4)
                t.column(DatabaseProvider.sharedInstance.statusController)
                t.column(DatabaseProvider.sharedInstance.is_scheduled)
                t.column(DatabaseProvider.sharedInstance.in_bar)
                t.column(DatabaseProvider.sharedInstance.percent)
                t.column(DatabaseProvider.sharedInstance.number_table)
                t.column(DatabaseProvider.sharedInstance.ready_flag)
                t.column(DatabaseProvider.sharedInstance.puntos)
                t.column(DatabaseProvider.sharedInstance.money_puntos)
                t.column(DatabaseProvider.sharedInstance.address)
                t.column(DatabaseProvider.sharedInstance.order_type)
                t.column(DatabaseProvider.sharedInstance.shipping_cost)
                t.column(DatabaseProvider.sharedInstance.folio_complete)
                t.column(DatabaseProvider.sharedInstance.time_delivery)
                t.column(DatabaseProvider.sharedInstance.to_refund)
                t.column(DatabaseProvider.sharedInstance.days_elapsed)
                t.column(DatabaseProvider.sharedInstance.is_paid)
                t.column(DatabaseProvider.sharedInstance.allow_process_nopayment)
                t.column(DatabaseProvider.sharedInstance.est_delivery_hour)
                t.column(DatabaseProvider.sharedInstance.est_ready_hour)
                t.column(DatabaseProvider.sharedInstance.is_generic)
                t.column(DatabaseProvider.sharedInstance.confirmed)
                t.column(DatabaseProvider.sharedInstance.confirm_hour)
                t.column(DatabaseProvider.sharedInstance.sendNotifications)
                t.column(DatabaseProvider.sharedInstance.notified)
                t.column(DatabaseProvider.sharedInstance.pickpal_cash_money)
                t.column(DatabaseProvider.sharedInstance.av_customer_service)
                t.column(DatabaseProvider.sharedInstance.two_devices)
                t.column(DatabaseProvider.sharedInstance.active_customer_service)
                
                t.column(DatabaseProvider.sharedInstance.total_card)
                t.column(DatabaseProvider.sharedInstance.total_cash)
                t.column(DatabaseProvider.sharedInstance.total_to_pay)
                t.column(DatabaseProvider.sharedInstance.total_done_payment)
                
                t.column(DatabaseProvider.sharedInstance.subtotal)
                
                
                
               
                
                
            })
        }catch let error {
            print(error)
        }
        
    }
    
    func insertUpdateNewItemCart(establishment_id: Int, establishment_name: String, in_site: Bool, notes: String, discount_code: String, total: Double,  pickpal_comission: Double, total_after_checkout: Double, order_has_been_sent: Bool, order_elements: [Dictionary<String,Any>]) {
        var dictionaryInfoString = String()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: order_elements, options: [])
            let decoded = String(bytes: jsonData, encoding: .utf8)
            dictionaryInfoString = decoded!
        }catch let error{
            print(error.localizedDescription)
        }
        
        if try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false).count) == 0 {
            let insert = DatabaseProvider.sharedInstance.orders.insert(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- 0, DatabaseProvider.sharedInstance.payment_method <- 0, DatabaseProvider.sharedInstance.discount_code <- "", DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- 0, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- "", DatabaseProvider.sharedInstance.order_number <- "", DatabaseProvider.sharedInstance.folio <- "", DatabaseProvider.sharedInstance.register_date <- "", DatabaseProvider.sharedInstance.process_time <- "", DatabaseProvider.sharedInstance.ready_time <- "", DatabaseProvider.sharedInstance.pay_time <- "", DatabaseProvider.sharedInstance.order_id <- 0, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- "", DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.ttl <- "", DatabaseProvider.sharedInstance.is_cash <- false, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.is_scheduled <- false, DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0)
            do {
                try db.run(insert)
            }catch let error{
                print("imposible insertar ", error.localizedDescription)
            }
        }else{
            let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)
            let update = rowUpdate.update(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- 0, DatabaseProvider.sharedInstance.payment_method <- 0, DatabaseProvider.sharedInstance.discount_code <- "", DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- 0, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- "", DatabaseProvider.sharedInstance.order_number <- "", DatabaseProvider.sharedInstance.folio <- "", DatabaseProvider.sharedInstance.register_date <- "", DatabaseProvider.sharedInstance.process_time <- "", DatabaseProvider.sharedInstance.ready_time <- "", DatabaseProvider.sharedInstance.pay_time <- "", DatabaseProvider.sharedInstance.order_id <- 0, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- "", DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.ttl <- "", DatabaseProvider.sharedInstance.is_cash <- false, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.is_scheduled <- false, DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0)
            do {
                try db.run(update)
            }catch let error{
                print("imposible insertar ", error.localizedDescription)
            }
        }
    }
    
//    Insertar mientras se agregan/quitan/editan items
    func insertOrderElements(order_has_been_sent: Bool, establishment_name: String, order_elements: [Dictionary<String,Any>], establishment_id: Int, notes: String) {
        var dictionaryInfoString = String()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: order_elements, options: [])
            let decoded = String(bytes: jsonData, encoding: .utf8)
            dictionaryInfoString = decoded!
        }catch let error{
            print(error.localizedDescription)
        }
        
        do {
            if try db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false).count) == 0 {
                let insert = DatabaseProvider.sharedInstance.orders.insert(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- false, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- 0, DatabaseProvider.sharedInstance.payment_method <- 0, DatabaseProvider.sharedInstance.discount_code <- "", DatabaseProvider.sharedInstance.total <- 0, DatabaseProvider.sharedInstance.estimated_time <- 0, DatabaseProvider.sharedInstance.pickpal_comission <- 0, DatabaseProvider.sharedInstance.total_after_checkout <- 0, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- "", DatabaseProvider.sharedInstance.order_number <- "", DatabaseProvider.sharedInstance.folio <- "", DatabaseProvider.sharedInstance.register_date <- "", DatabaseProvider.sharedInstance.process_time <- "", DatabaseProvider.sharedInstance.ready_time <- "", DatabaseProvider.sharedInstance.pay_time <- "", DatabaseProvider.sharedInstance.order_id <- 0, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- "", DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.ttl <- "", DatabaseProvider.sharedInstance.is_cash <- false, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.is_scheduled <- false, DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.ready_flag <- true,  DatabaseProvider.sharedInstance.puntos <- 0, DatabaseProvider.sharedInstance.money_puntos <- 0,
                    DatabaseProvider.sharedInstance.address <- "",
                    DatabaseProvider.sharedInstance.order_type <- "",
                    DatabaseProvider.sharedInstance.shipping_cost <- 0,
                    DatabaseProvider.sharedInstance.folio_complete <- "",
                    DatabaseProvider.sharedInstance.time_delivery <- "0",
                    DatabaseProvider.sharedInstance.to_refund <- false,
                    DatabaseProvider.sharedInstance.days_elapsed <- 0,
                    DatabaseProvider.sharedInstance.is_paid <- false,
                    DatabaseProvider.sharedInstance.allow_process_nopayment <- false,
                    DatabaseProvider.sharedInstance.est_delivery_hour <- "",
                    DatabaseProvider.sharedInstance.est_ready_hour <- "",
                    DatabaseProvider.sharedInstance.is_generic <- false,
                    DatabaseProvider.sharedInstance.confirmed <- false,
                    DatabaseProvider.sharedInstance.confirm_hour <- "",
                    DatabaseProvider.sharedInstance.sendNotifications <- true,
                    DatabaseProvider.sharedInstance.notified <- true,
                    DatabaseProvider.sharedInstance.pickpal_cash_money <- 0,
                    DatabaseProvider.sharedInstance.av_customer_service <- false,
                    DatabaseProvider.sharedInstance.two_devices <- false,
                    DatabaseProvider.sharedInstance.active_customer_service <- false,
                    
                    DatabaseProvider.sharedInstance.total_card <- 0,
                    DatabaseProvider.sharedInstance.total_cash <- 0,
                    DatabaseProvider.sharedInstance.total_to_pay <- 0,
                    DatabaseProvider.sharedInstance.total_done_payment <- 0,
                    DatabaseProvider.sharedInstance.subtotal <- 0
                    
                    
                
                    
                    
                    
                    )
                
                
                
                do {
                    try db.run(insert)
                }catch let error{
                    print("imposible insertar ", error.localizedDescription)
                }
            }else{
                let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)
                let update = rowUpdate.update(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- false, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- 0, DatabaseProvider.sharedInstance.payment_method <- 0, DatabaseProvider.sharedInstance.discount_code <- "", DatabaseProvider.sharedInstance.total <- 0, DatabaseProvider.sharedInstance.estimated_time <- 0, DatabaseProvider.sharedInstance.pickpal_comission <- 0, DatabaseProvider.sharedInstance.total_after_checkout <- 0, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- "", DatabaseProvider.sharedInstance.order_number <- "", DatabaseProvider.sharedInstance.folio <- "", DatabaseProvider.sharedInstance.register_date <- "", DatabaseProvider.sharedInstance.process_time <- "", DatabaseProvider.sharedInstance.ready_time <- "", DatabaseProvider.sharedInstance.pay_time <- "", DatabaseProvider.sharedInstance.order_id <- 0, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- "", DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.ttl <- "", DatabaseProvider.sharedInstance.is_cash <- false, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.is_scheduled <- false, DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0, DatabaseProvider.sharedInstance.ready_flag <- true)
                do {
                    try db.run(update)
                }catch let error{
                    print("imposible insertar ", error.localizedDescription)
                }
            }
        }catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func updateItemCart() -> Bool{
        return true
        
    }
    
    func insertItem(establishment_id: Int, establishment_name: String, in_site: Bool, notes: String, billing_address_id: Int, payment_method: Int, discount_code: String, total: Double, estimated_time: Int,  pickpal_comission: Double, total_after_checkout: Double, order_has_been_sent: Bool, code: String, order_number: String, folio: String, register_date: String, process_time: String, ready_time: String, pay_time: String, order_id: Int, order_elements: [Dictionary<String,Any>], statusItem: String, isCash: Bool) {
        var dictionaryInfoString = String()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: order_elements, options: [])
            let decoded = String(bytes: jsonData, encoding: .utf8)
            dictionaryInfoString = decoded!
        }catch let error{
            print(error.localizedDescription)
        }
        
        if try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id).count) == 0 {
            let insert = DatabaseProvider.sharedInstance.orders.insert(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- billing_address_id, DatabaseProvider.sharedInstance.payment_method <- payment_method, DatabaseProvider.sharedInstance.discount_code <- discount_code, DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- estimated_time, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- code, DatabaseProvider.sharedInstance.order_number <- order_number, DatabaseProvider.sharedInstance.folio <- folio, DatabaseProvider.sharedInstance.register_date <- register_date, DatabaseProvider.sharedInstance.process_time <- process_time, DatabaseProvider.sharedInstance.ready_time <- ready_time, DatabaseProvider.sharedInstance.pay_time <- pay_time, DatabaseProvider.sharedInstance.order_id <- order_id, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- statusItem, DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.ttl <- "", DatabaseProvider.sharedInstance.is_cash <- isCash, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.is_scheduled <- false, DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0, DatabaseProvider.sharedInstance.ready_flag <- true,
                DatabaseProvider.sharedInstance.puntos <- 0,
                DatabaseProvider.sharedInstance.money_puntos <- 0)
            do {
                try db.run(insert)
                //                return true
            }catch let error{
                print("imposible insertar ", error.localizedDescription)
                //                return false
            }
        }else{
            let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)
            let update = rowUpdate.update(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- billing_address_id, DatabaseProvider.sharedInstance.payment_method <- payment_method, DatabaseProvider.sharedInstance.discount_code <- discount_code, DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- estimated_time, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- code, DatabaseProvider.sharedInstance.order_number <- order_number, DatabaseProvider.sharedInstance.folio <- folio, DatabaseProvider.sharedInstance.register_date <- register_date, DatabaseProvider.sharedInstance.process_time <- process_time, DatabaseProvider.sharedInstance.ready_time <- ready_time, DatabaseProvider.sharedInstance.pay_time <- pay_time, DatabaseProvider.sharedInstance.order_id <- order_id, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- statusItem, DatabaseProvider.sharedInstance.qr <- "", DatabaseProvider.sharedInstance.is_cash <- isCash, DatabaseProvider.sharedInstance.last4 <- "", DatabaseProvider.sharedInstance.statusController <- "", DatabaseProvider.sharedInstance.in_bar <- false, DatabaseProvider.sharedInstance.percent <- 0.00, DatabaseProvider.sharedInstance.number_table <- 0, DatabaseProvider.sharedInstance.ready_flag <- true, DatabaseProvider.sharedInstance.puntos <- 0,
                DatabaseProvider.sharedInstance.money_puntos <- 0)
            do {
                try db.run(update)
                //                return true
            }catch let error{
                print("imposible insertar ", error.localizedDescription)
                //                return false
            }
        }
    }
    
    func deleteItem() {
//        if try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false).count) == 0 {
        let delete = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)
        try! db.run(delete.delete())
//        }
    }
    
    func deleteDueOrder(order_id: Int) {
        let delete = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)
        try! db.run(delete.delete())
    }
    
    func getNotes(query: Expression<String>) -> String{
        let query = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)
        for row in try! db.prepare(query) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent) == false {
                return try! row.get(DatabaseProvider.sharedInstance.notes)
//            }else{
//                return ""
//            }
        }
        return ""
    }
    
    func getCarrito(query: Expression<String>, query2: Expression<Bool>) -> (String, Bool) {
//        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders) {
//            print(try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent))
//
//        }
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)) {
            let strinr = try! row.get(query)
            let order_done = try! row.get(query2)

            return (strinr, order_done)
        }
        
        return("", false)
    }
    
    func getEstablishmentId(query: Expression<Int>) -> Int {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent) == false {
                let establishment_id = try! row.get(query)
                return establishment_id
//            }else{
//                return 0
//            }
        }
        return 0
    }
    
    func getEstablishmentName(query: Expression<String>) -> String {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_has_been_sent == false)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent) == false {
                let establishment_name = try! row.get(query)
                return establishment_name
//            }else{
//                return ""
//            }
        }
        return ""
    }
    
    func getEstablishmentNameByOrderId(order_id: Int) -> String {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)) {
            //            if try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent) == false {
            let query = DatabaseProvider.sharedInstance.establishment_name
            let establishment_name = try! row.get(query)
            return establishment_name
            //            }else{
            //                return ""
            //            }
        }
        return ""
    }
    
    func getInfoOrdersString(order_id: Int, queryString: Expression<String>) -> String {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_id) == order_id {
                let infoString = try! row.get(queryString)
                return infoString
//            }
        }
        return ""
    }
    
    func getInfoOrdersDouble(order_id: Int, queryDouble: Expression<Double>) -> Double {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_id) == order_id {
                let infoDouble = try! row.get(queryDouble)
                return infoDouble
//            }
        }
        return 0.0
    }
    
    func getInfoOrdersInt(order_id: Int, queryInt: Expression<Int>) -> Int {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_id) == order_id {
                let infoInt = try! row.get(queryInt)
                return infoInt
//            }
        }
        return 0
    }
    
    func getInfoOrdersBool(order_id: Int, queryBool: Expression<Bool>) -> Bool {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)) {
//            if try! row.get(DatabaseProvider.sharedInstance.order_id) == order_id {
                let infoBool = try! row.get(queryBool)
                return infoBool
//            }
        }
        return false
    }
    
    func getOrdersId () -> [Int] {
        var orders = [Int]()
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders) {
            orders.append(try! row.get(DatabaseProvider.sharedInstance.order_id))
        }
        return orders
    }
    
    func getStatus() -> ([String], [String], [String], [String]){
        var statusToPay = [String]()
        var statusSection = [String]()
        var statusProcess = [String]()
        var statusRefund = [String]()
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders) {
            let stats =  try! row.get(DatabaseProvider.sharedInstance.statusItem)
            if stats == "in_process" || stats == "ready" || stats == "delayed" || stats == "delivery_delayed" || stats == "not_confirmed" {
                if !statusSection.contains("in_process") || stats == "not_confirmed"{
                    statusSection.append("in_process")
                }
            }else if stats == "to_pay" && !stats.contains(""){
                if !statusSection.contains("to_pay") {
                    statusSection.append("to_pay")
                }
            }else{
                if !statusSection.contains("refund") && !statusSection.contains(""){
                    statusSection.append("refund")
                }
            }
            
//            print("status item \(row[DatabaseProvider.sharedInstance.statusItem])")
        }
        let rowToPay = try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "to_pay" || !DatabaseProvider.sharedInstance.is_paid).count)
        for _ in 0..<rowToPay {
            statusToPay.append("to_pay")
        }
//        let rowProcess = try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "in_process" || DatabaseProvider.sharedInstance.statusItem == "ready" || DatabaseProvider.sharedInstance.statusItem == "delayed" || DatabaseProvider.sharedInstance.statusItem == "delivery_delayed").count)
        
//        let statusItem = DatabaseProvider.sharedInstance.statusItem
//        let rowConfirmed = try! db.scalar( DatabaseProvider.sharedInstance.orders.filter(statusItem == "not_confirmed" || statusItem == "confirmed" || statusItem == "new").count)
//
//        let rowProcess = try! db.scalar( DatabaseProvider.sharedInstance.orders.filter(statusItem == "in_process" || statusItem == "ready" || statusItem == "delayed" || statusItem == "delivery_delayed").count)
//        let  totalRow =  rowProcess + rowConfirmed
        
        let  totalRow = try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.is_paid).count)
        
        for _ in 0..<totalRow {
            statusProcess.append("in_process")
        }
        let rowRefund = try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "refund").count)
        for _ in 0..<rowRefund {
            statusRefund.append("refund")
        }
        return (statusSection, statusToPay, statusProcess, statusRefund)
    }
    
    func getHourPay(pagado: String) -> String {

        let form = DateFormatter()
        form.dateFormat = "HH:mm:ss"
        let datePay = form.date(from: pagado)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        var dateString = String()
        if datePay == nil {
            dateString = ""
        }else{
            dateString = formatter.string(from: datePay!)
        }
//        print(dateString, "dateString")
        return dateString
//        return ""
    }
    
    func getEstimated(processHour: String, registerDay: String, prepTime: Int) -> String {
        let estimated = registerDay + " " + processHour
//        print(estimated)
        let form = DateFormatter()
        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if form.date(from: estimated) != nil {
            let datePay = form.date(from: estimated)
            let calendar = Calendar.current
            let date = calendar.date(byAdding:.minute, value: prepTime, to: datePay!)!
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "a.m."
            formatter.pmSymbol = "p.m."
            let dateString = formatter.string(from: date)
            return dateString
        }
        return ""
        
        
    }
    
    func getPedidos() -> [[Pedidos]]{
        var pedidos = [[Pedidos]]()
        var peidosPorPagar = [Pedidos]()
        var pedidosPagados = [Pedidos]()
        var pppp = [Pedidos]()
      /*  for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.is_paid == "to_pay").order(DatabaseProvider.sharedInstance.ttl)) {*/
//        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(!DatabaseProvider.sharedInstance.is_paid  && DatabaseProvider.sharedInstance.statusController != "" ).order(DatabaseProvider.sharedInstance.ttl)) {
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(!DatabaseProvider.sharedInstance.is_paid  && DatabaseProvider.sharedInstance.statusController != "").order(DatabaseProvider.sharedInstance.ttl)) {
            let pedido = Pedidos()
            pedido.establishment_id = try! row.get(DatabaseProvider.sharedInstance.establishment_id)
            pedido.establishment_name = try! row.get(DatabaseProvider.sharedInstance.establishment_name)
            pedido.in_site = try! row.get(DatabaseProvider.sharedInstance.in_site)
            pedido.notes = try! row.get(DatabaseProvider.sharedInstance.notes)
            pedido.billing_address_id = try! row.get(DatabaseProvider.sharedInstance.billing_address_id)
            pedido.payment_method = try! row.get(DatabaseProvider.sharedInstance.payment_method)
            pedido.discount_code = try! row.get(DatabaseProvider.sharedInstance.discount_code)
            pedido.total = try! row.get(DatabaseProvider.sharedInstance.total)
            pedido.estimated_time = try! row.get(DatabaseProvider.sharedInstance.estimated_time)
            pedido.pickpal_comission = try! row.get(DatabaseProvider.sharedInstance.pickpal_comission)
            pedido.total_after_checkout = try! row.get(DatabaseProvider.sharedInstance.total_after_checkout)
            pedido.order_has_been_sent = try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent)
            pedido.code = try! row.get(DatabaseProvider.sharedInstance.code)
            pedido.order_number = try! row.get(DatabaseProvider.sharedInstance.order_number)
            pedido.folio = try! row.get(DatabaseProvider.sharedInstance.folio)
            pedido.register_date = try! row.get(DatabaseProvider.sharedInstance.register_date)
            pedido.process_time = try! row.get(DatabaseProvider.sharedInstance.process_time)
            pedido.ready_time = try! row.get(DatabaseProvider.sharedInstance.ready_time)
            pedido.pay_time = try! row.get(DatabaseProvider.sharedInstance.pay_time)
            pedido.order_id = try! row.get(DatabaseProvider.sharedInstance.order_id)
            let encoded = try! row.get(DatabaseProvider.sharedInstance.order_elements).data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            pedido.order_elements = dictionary as? [Dictionary<String,Any>]
            pedido.statusItem = try! row.get(DatabaseProvider.sharedInstance.statusItem)
            pedido.qr = try! row.get(DatabaseProvider.sharedInstance.qr)
            pedido.isCash = try! row.get(DatabaseProvider.sharedInstance.is_cash)
            pedido.last4 = try! row.get(DatabaseProvider.sharedInstance.last4)
            pedido.statusController = try! row.get(DatabaseProvider.sharedInstance.statusController)
            pedido.is_scheduled = try! row.get(DatabaseProvider.sharedInstance.is_scheduled)
            pedido.in_bar = try! row.get(DatabaseProvider.sharedInstance.in_bar)
            pedido.percent = try! row.get(DatabaseProvider.sharedInstance.percent)
            pedido.points = try! row.get(DatabaseProvider.sharedInstance.puntos)
            pedido.money_puntos = try! row.get(DatabaseProvider.sharedInstance.money_puntos)
            pedido.order_type = try! row.get(DatabaseProvider.sharedInstance.order_type)
            pedido.address = try! row.get(DatabaseProvider.sharedInstance.address)
            pedido.shipping_cost = try! row.get(DatabaseProvider.sharedInstance.shipping_cost)
            pedido.folio_complete = try! row.get(DatabaseProvider.sharedInstance.folio_complete)
            pedido.time_delivery = try! row.get(DatabaseProvider.sharedInstance.time_delivery)
            pedido.to_refund = try! row.get(DatabaseProvider.sharedInstance.to_refund)
            pedido.is_paid = try! row.get(DatabaseProvider.sharedInstance.is_paid)
            pedido.allow_process_nopayment = try! row.get(DatabaseProvider.sharedInstance.allow_process_nopayment)
            pedido.number_table = try! row.get(DatabaseProvider.sharedInstance.number_table)
            pedido.est_delivery_hour = try! row.get(DatabaseProvider.sharedInstance.est_delivery_hour)
            pedido.est_ready_hour = try! row.get(DatabaseProvider.sharedInstance.est_ready_hour)
            pedido.is_generic = try! row.get(DatabaseProvider.sharedInstance.is_generic)
            pedido.confirmed = try! row.get(DatabaseProvider.sharedInstance.confirmed)
            pedido.confirm_hour = try! row.get(DatabaseProvider.sharedInstance.confirm_hour)
            pedido.sendNotifications = try! row.get(DatabaseProvider.sharedInstance.sendNotifications)
            pedido.notified = try! row.get(DatabaseProvider.sharedInstance.notified)
            pedido.pickpal_cash_money = try! row.get(DatabaseProvider.sharedInstance.pickpal_cash_money)
            pedido.av_customer_service = try! row.get(DatabaseProvider.sharedInstance.av_customer_service)
            pedido.two_devices = try! row.get(DatabaseProvider.sharedInstance.two_devices)
            pedido.active_customer_service = try! row.get(DatabaseProvider.sharedInstance.active_customer_service)
            
            pedido.total_card = try! row.get(DatabaseProvider.sharedInstance.total_card)
            pedido.total_cash = try! row.get(DatabaseProvider.sharedInstance.total_cash)
            pedido.total_to_pay = try! row.get(DatabaseProvider.sharedInstance.total_to_pay)
            pedido.total_done_payment = try! row.get(DatabaseProvider.sharedInstance.total_done_payment)
            pedido.subtotal = try! row.get(DatabaseProvider.sharedInstance.subtotal)
            

            
            
            
            peidosPorPagar.append(pedido)
        }
        if peidosPorPagar.count > 0 {
            pedidos.append(peidosPorPagar)
        }
//        let query = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "in_process")
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.is_paid  && DatabaseProvider.sharedInstance.statusController != "").order(DatabaseProvider.sharedInstance.ttl)) {
        /*db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "in_process" || DatabaseProvider.sharedInstance.statusItem == "ready" || DatabaseProvider.sharedInstance.statusItem == "delayed" || DatabaseProvider.sharedInstance.statusItem == "delivery_delayed")) {*/
            let pedido = Pedidos()
            pedido.establishment_id = try! row.get(DatabaseProvider.sharedInstance.establishment_id)
            pedido.establishment_name = try! row.get(DatabaseProvider.sharedInstance.establishment_name)
            pedido.in_site = try! row.get(DatabaseProvider.sharedInstance.in_site)
            pedido.notes = try! row.get(DatabaseProvider.sharedInstance.notes)
            pedido.billing_address_id = try! row.get(DatabaseProvider.sharedInstance.billing_address_id)
            pedido.payment_method = try! row.get(DatabaseProvider.sharedInstance.payment_method)
            pedido.discount_code = try! row.get(DatabaseProvider.sharedInstance.discount_code)
            pedido.total = try! row.get(DatabaseProvider.sharedInstance.total)
            pedido.estimated_time = try! row.get(DatabaseProvider.sharedInstance.estimated_time)
            pedido.pickpal_comission = try! row.get(DatabaseProvider.sharedInstance.pickpal_comission)
            pedido.total_after_checkout = try! row.get(DatabaseProvider.sharedInstance.total_after_checkout)
            pedido.order_has_been_sent = try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent)
            pedido.code = try! row.get(DatabaseProvider.sharedInstance.code)
            pedido.order_number = try! row.get(DatabaseProvider.sharedInstance.order_number)
            pedido.folio = try! row.get(DatabaseProvider.sharedInstance.folio)
            pedido.register_date = try! row.get(DatabaseProvider.sharedInstance.register_date)
            pedido.process_time = try! row.get(DatabaseProvider.sharedInstance.process_time)
            pedido.ready_time = try! row.get(DatabaseProvider.sharedInstance.ready_time)
            pedido.pay_time = try! row.get(DatabaseProvider.sharedInstance.pay_time)
            pedido.order_id = try! row.get(DatabaseProvider.sharedInstance.order_id)
            let encoded = try! row.get(DatabaseProvider.sharedInstance.order_elements).data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            pedido.order_elements = dictionary as? [Dictionary<String,Any>]
            pedido.statusItem = try! row.get(DatabaseProvider.sharedInstance.statusItem)
            pedido.qr = try! row.get(DatabaseProvider.sharedInstance.qr)
            pedido.isCash = try! row.get(DatabaseProvider.sharedInstance.is_cash)
            pedido.last4 = try! row.get(DatabaseProvider.sharedInstance.last4)
            pedido.statusController = try! row.get(DatabaseProvider.sharedInstance.statusController)
            pedido.is_scheduled = try! row.get(DatabaseProvider.sharedInstance.is_scheduled)
            pedido.in_bar = try! row.get(DatabaseProvider.sharedInstance.in_bar)
            pedido.percent = try! row.get(DatabaseProvider.sharedInstance.percent)
            pedido.number_table = try! row.get(DatabaseProvider.sharedInstance.number_table)
            pedido.points = try! row.get(DatabaseProvider.sharedInstance.puntos)
            pedido.money_puntos = try! row.get(DatabaseProvider.sharedInstance.money_puntos)
            pedido.order_type = try! row.get(DatabaseProvider.sharedInstance.order_type)
            pedido.address = try! row.get(DatabaseProvider.sharedInstance.address)
            pedido.shipping_cost = try! row.get(DatabaseProvider.sharedInstance.shipping_cost)
            pedido.folio_complete = try! row.get(DatabaseProvider.sharedInstance.folio_complete)
            pedido.time_delivery = try! row.get(DatabaseProvider.sharedInstance.time_delivery)
            pedido.to_refund = try! row.get(DatabaseProvider.sharedInstance.to_refund)
            pedido.days_elapsed = try! row.get(DatabaseProvider.sharedInstance.days_elapsed)
            pedido.is_paid = try! row.get(DatabaseProvider.sharedInstance.is_paid)
            pedido.allow_process_nopayment = try! row.get(DatabaseProvider.sharedInstance.allow_process_nopayment)
            pedido.est_delivery_hour = try! row.get(DatabaseProvider.sharedInstance.est_delivery_hour)
            pedido.est_ready_hour = try! row.get(DatabaseProvider.sharedInstance.est_ready_hour)
            pedido.is_generic = try! row.get(DatabaseProvider.sharedInstance.is_generic)
            pedido.confirmed = try! row.get(DatabaseProvider.sharedInstance.confirmed)
            pedido.confirm_hour = try! row.get(DatabaseProvider.sharedInstance.confirm_hour)
            pedido.sendNotifications = try! row.get(DatabaseProvider.sharedInstance.sendNotifications)
            pedido.notified = try! row.get(DatabaseProvider.sharedInstance.notified)
            pedido.pickpal_cash_money = try! row.get(DatabaseProvider.sharedInstance.pickpal_cash_money)
            pedido.av_customer_service = try! row.get(DatabaseProvider.sharedInstance.av_customer_service)
            pedido.two_devices = try! row.get(DatabaseProvider.sharedInstance.two_devices)
            pedido.active_customer_service = try! row.get(DatabaseProvider.sharedInstance.active_customer_service)
            
            pedido.total_card = try! row.get(DatabaseProvider.sharedInstance.total_card)
            pedido.total_cash = try! row.get(DatabaseProvider.sharedInstance.total_cash)
            pedido.total_to_pay = try! row.get(DatabaseProvider.sharedInstance.total_to_pay)
            pedido.total_done_payment = try! row.get(DatabaseProvider.sharedInstance.total_done_payment)
            
            pedido.subtotal = try! row.get(DatabaseProvider.sharedInstance.subtotal)
            
            
            
            
            
            pedidosPagados.append(pedido)
        }
        if pedidosPagados.count > 0 {
            pedidos.append(pedidosPagados)
        }
        
//        let query = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "expired")
        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "canceled" || DatabaseProvider.sharedInstance.statusItem == "expired" || DatabaseProvider.sharedInstance.statusItem == "refund"  && DatabaseProvider.sharedInstance.statusController != "")) {
            let pedido = Pedidos()
            pedido.establishment_id = try! row.get(DatabaseProvider.sharedInstance.establishment_id)
            pedido.establishment_name = try! row.get(DatabaseProvider.sharedInstance.establishment_name)
            pedido.in_site = try! row.get(DatabaseProvider.sharedInstance.in_site)
            pedido.notes = try! row.get(DatabaseProvider.sharedInstance.notes)
            pedido.billing_address_id = try! row.get(DatabaseProvider.sharedInstance.billing_address_id)
            pedido.payment_method = try! row.get(DatabaseProvider.sharedInstance.payment_method)
            pedido.discount_code = try! row.get(DatabaseProvider.sharedInstance.discount_code)
            pedido.total = try! row.get(DatabaseProvider.sharedInstance.total)
            pedido.estimated_time = try! row.get(DatabaseProvider.sharedInstance.estimated_time)
            pedido.pickpal_comission = try! row.get(DatabaseProvider.sharedInstance.pickpal_comission)
            pedido.total_after_checkout = try! row.get(DatabaseProvider.sharedInstance.total_after_checkout)
            pedido.order_has_been_sent = try! row.get(DatabaseProvider.sharedInstance.order_has_been_sent)
            pedido.code = try! row.get(DatabaseProvider.sharedInstance.code)
            pedido.order_number = try! row.get(DatabaseProvider.sharedInstance.order_number)
            pedido.folio = try! row.get(DatabaseProvider.sharedInstance.folio)
            pedido.register_date = try! row.get(DatabaseProvider.sharedInstance.register_date)
            pedido.process_time = try! row.get(DatabaseProvider.sharedInstance.process_time)
            pedido.ready_time = try! row.get(DatabaseProvider.sharedInstance.ready_time)
            pedido.pay_time = try! row.get(DatabaseProvider.sharedInstance.pay_time)
            pedido.order_id = try! row.get(DatabaseProvider.sharedInstance.order_id)
            let encoded = try! row.get(DatabaseProvider.sharedInstance.order_elements).data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            pedido.order_elements = dictionary as? [Dictionary<String,Any>]
            pedido.statusItem = try! row.get(DatabaseProvider.sharedInstance.statusItem)
            pedido.qr = try! row.get(DatabaseProvider.sharedInstance.qr)
            pedido.isCash = try! row.get(DatabaseProvider.sharedInstance.is_cash)
            pedido.last4 = try! row.get(DatabaseProvider.sharedInstance.last4)
            pedido.statusController = try! row.get(DatabaseProvider.sharedInstance.statusController)
            pedido.is_scheduled = try! row.get(DatabaseProvider.sharedInstance.is_scheduled)
            pedido.in_bar = try! row.get(DatabaseProvider.sharedInstance.in_bar)
            pedido.percent = try! row.get(DatabaseProvider.sharedInstance.percent)
            pedido.number_table = try! row.get(DatabaseProvider.sharedInstance.number_table)
            pedido.order_type = try! row.get(DatabaseProvider.sharedInstance.order_type)
            pedido.address = try! row.get(DatabaseProvider.sharedInstance.address)
            pedido.shipping_cost = try! row.get(DatabaseProvider.sharedInstance.shipping_cost)
            pedido.folio_complete = try! row.get(DatabaseProvider.sharedInstance.folio_complete)
            pedido.time_delivery = try! row.get(DatabaseProvider.sharedInstance.time_delivery)
            pedido.to_refund = try! row.get(DatabaseProvider.sharedInstance.to_refund)
            pedido.days_elapsed = try! row.get(DatabaseProvider.sharedInstance.days_elapsed)
            pedido.is_paid = try! row.get(DatabaseProvider.sharedInstance.is_paid)
            pedido.allow_process_nopayment = try! row.get(DatabaseProvider.sharedInstance.allow_process_nopayment)
            pedido.est_delivery_hour = try! row.get(DatabaseProvider.sharedInstance.est_delivery_hour)
            pedido.est_ready_hour = try! row.get(DatabaseProvider.sharedInstance.est_ready_hour)
            pedido.is_generic = try! row.get(DatabaseProvider.sharedInstance.is_generic)
            pedido.confirmed = try! row.get(DatabaseProvider.sharedInstance.confirmed)
            pedido.confirm_hour = try! row.get(DatabaseProvider.sharedInstance.confirm_hour)
            pedido.sendNotifications = try! row.get(DatabaseProvider.sharedInstance.sendNotifications)
            pedido.notified = try! row.get(DatabaseProvider.sharedInstance.notified)
            pedido.pickpal_cash_money = try! row.get(DatabaseProvider.sharedInstance.pickpal_cash_money)
            pedido.av_customer_service = try! row.get(DatabaseProvider.sharedInstance.av_customer_service)
            pedido.two_devices = try! row.get(DatabaseProvider.sharedInstance.two_devices)
            pedido.active_customer_service = try! row.get(DatabaseProvider.sharedInstance.active_customer_service)
            
            pedido.total_card = try! row.get(DatabaseProvider.sharedInstance.total_card)
            pedido.total_cash = try! row.get(DatabaseProvider.sharedInstance.total_cash)
            pedido.total_to_pay = try! row.get(DatabaseProvider.sharedInstance.total_to_pay)
            pedido.total_done_payment = try! row.get(DatabaseProvider.sharedInstance.total_done_payment)
            
            pedido.subtotal = try! row.get(DatabaseProvider.sharedInstance.subtotal)
            
            
            
            
           
            
            
            pppp.append(pedido)
        }
        if pppp.count > 0 {
            pedidos.append(pppp)
        }
        return pedidos
    }

    func insertFromFirebase(establishment_id: Int, establishment_name: String, in_site: Bool, notes: String, billing_address_id: Int, payment_method: Int, discount_code: String, total: Double, estimated_time: Int,  pickpal_comission: Double, total_after_checkout: Double, order_has_been_sent: Bool, code: String, order_number: String, folio: String, register_date: String, process_time: String, ready_time: String, pay_time: String, order_id: Int, order_elements: [Dictionary<String,Any>], statusItem: String, qr: String, is_paid: Bool, ttl: String, isCash: Bool, last4: String, statusController: String, is_scheduled: Bool, in_bar: Bool, percent: Double, table_number: Int, readyFlag: Bool, points: Double, money_puntos: Double, order_type: String, address: String, shipping_cost: Double, folio_complete: String, time_delivery: String, to_refund: Bool, days_elapsed: Int, allow_process_nopayment: Bool, est_delivery_hour: String ,est_ready_hour: String, is_generic: Bool, confirmed: Bool, confirm_hour: String, sendNotifications: Bool, notified: Bool, pickpal_cash_money:Double, av_customer_service: Bool, two_devices: Bool, active_customer_service: Bool,total_card :Double,total_cash :Double,total_to_pay :Double,total_done_payment :Double, subtotal: Double) {
        
        
//        print(try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id).count))
        var dictionaryInfoString = String()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: order_elements, options: [])
            let decoded = String(bytes: jsonData, encoding: .utf8)
            dictionaryInfoString = decoded!
        }catch let error{
            print(error.localizedDescription)
        }
//        for row in try! db.prepare(DatabaseProvider.sharedInstance.orders) {
//            print(try! row.get(DatabaseProvider.sharedInstance.order_id), "id de la base")
//        }
        if try! db.scalar(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id).count) == 0 {
        
        print(" Orden Insertado id:  \(String(describing:  order_id))  bandera: \(to_refund)  fecha  \(register_date)")
        
        let insert = DatabaseProvider.sharedInstance.orders.insert(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- billing_address_id, DatabaseProvider.sharedInstance.payment_method <- payment_method, DatabaseProvider.sharedInstance.discount_code <- discount_code, DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- estimated_time, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- code, DatabaseProvider.sharedInstance.order_number <- order_number, DatabaseProvider.sharedInstance.folio <- folio, DatabaseProvider.sharedInstance.register_date <- register_date, DatabaseProvider.sharedInstance.process_time <- process_time, DatabaseProvider.sharedInstance.ready_time <- ready_time, DatabaseProvider.sharedInstance.pay_time <- pay_time, DatabaseProvider.sharedInstance.order_id <- order_id, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- statusItem, DatabaseProvider.sharedInstance.qr <- qr, DatabaseProvider.sharedInstance.ttl <- ttl, DatabaseProvider.sharedInstance.is_cash <- isCash, DatabaseProvider.sharedInstance.last4 <- last4, DatabaseProvider.sharedInstance.statusController <- statusController, DatabaseProvider.sharedInstance.is_scheduled <- is_scheduled, DatabaseProvider.sharedInstance.in_bar <- in_bar, DatabaseProvider.sharedInstance.percent <- percent, DatabaseProvider.sharedInstance.number_table <- table_number, DatabaseProvider.sharedInstance.ready_flag <- readyFlag, DatabaseProvider.sharedInstance.puntos <- points,
            DatabaseProvider.sharedInstance.money_puntos <- money_puntos,
            DatabaseProvider.sharedInstance.order_type <- order_type,
            DatabaseProvider.sharedInstance.address <- address,
            DatabaseProvider.sharedInstance.shipping_cost <- shipping_cost,
            DatabaseProvider.sharedInstance.folio_complete <- folio_complete,
            DatabaseProvider.sharedInstance.time_delivery <- time_delivery,
            DatabaseProvider.sharedInstance.to_refund <- to_refund,
            DatabaseProvider.sharedInstance.days_elapsed <- days_elapsed,
            DatabaseProvider.sharedInstance.is_paid <- is_paid,
            DatabaseProvider.sharedInstance.allow_process_nopayment <- allow_process_nopayment,
            DatabaseProvider.sharedInstance.est_delivery_hour <- est_delivery_hour,
            DatabaseProvider.sharedInstance.est_ready_hour <- est_ready_hour,
            DatabaseProvider.sharedInstance.is_generic <- is_generic,
            DatabaseProvider.sharedInstance.confirmed <- confirmed,
            DatabaseProvider.sharedInstance.confirm_hour <- confirm_hour,
            DatabaseProvider.sharedInstance.sendNotifications <- sendNotifications,
            DatabaseProvider.sharedInstance.notified <- notified,
            DatabaseProvider.sharedInstance.pickpal_cash_money <- pickpal_cash_money,
            DatabaseProvider.sharedInstance.av_customer_service <- av_customer_service,
            DatabaseProvider.sharedInstance.two_devices <- two_devices,
            DatabaseProvider.sharedInstance.active_customer_service <- active_customer_service,
            
            DatabaseProvider.sharedInstance.total_card <- total_card,
            DatabaseProvider.sharedInstance.total_cash <- total_cash,
            DatabaseProvider.sharedInstance.total_to_pay <- total_to_pay,
            DatabaseProvider.sharedInstance.total_done_payment <- total_done_payment,
            DatabaseProvider.sharedInstance.subtotal <- subtotal
            
            
            
            
            
            
            )
            
            
            
        
        
        do {
            try db.run(insert)
            print("Insert success")
        }catch let error{
            print("imposible insertar ", error.localizedDescription)
        }
        
    }else{
        
         print("Orden Actulizada  id:  \(String(describing:  order_id))  bandera: \(to_refund)  fecha  \(register_date)")
        
        let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id)
        let update = rowUpdate.update(DatabaseProvider.sharedInstance.establishment_id <- establishment_id, DatabaseProvider.sharedInstance.establishment_name <- establishment_name, DatabaseProvider.sharedInstance.in_site <- in_site, DatabaseProvider.sharedInstance.notes <- notes, DatabaseProvider.sharedInstance.billing_address_id <- billing_address_id, DatabaseProvider.sharedInstance.payment_method <- payment_method, DatabaseProvider.sharedInstance.discount_code <- discount_code, DatabaseProvider.sharedInstance.total <- total, DatabaseProvider.sharedInstance.estimated_time <- estimated_time, DatabaseProvider.sharedInstance.pickpal_comission <- pickpal_comission, DatabaseProvider.sharedInstance.total_after_checkout <- total_after_checkout, DatabaseProvider.sharedInstance.order_has_been_sent <- order_has_been_sent, DatabaseProvider.sharedInstance.code <- code, DatabaseProvider.sharedInstance.order_number <- order_number, DatabaseProvider.sharedInstance.folio <- folio, DatabaseProvider.sharedInstance.register_date <- register_date, DatabaseProvider.sharedInstance.process_time <- process_time, DatabaseProvider.sharedInstance.ready_time <- ready_time, DatabaseProvider.sharedInstance.pay_time <- pay_time, DatabaseProvider.sharedInstance.order_id <- order_id, DatabaseProvider.sharedInstance.order_elements <- dictionaryInfoString, DatabaseProvider.sharedInstance.statusItem <- statusItem, DatabaseProvider.sharedInstance.qr <- qr, DatabaseProvider.sharedInstance.ttl <- ttl, DatabaseProvider.sharedInstance.is_cash <- isCash, DatabaseProvider.sharedInstance.last4 <- last4, DatabaseProvider.sharedInstance.statusController <- statusController, DatabaseProvider.sharedInstance.is_scheduled <- is_scheduled, DatabaseProvider.sharedInstance.in_bar <- in_bar, DatabaseProvider.sharedInstance.percent <- percent, DatabaseProvider.sharedInstance.number_table <- table_number, DatabaseProvider.sharedInstance.ready_flag <- readyFlag, DatabaseProvider.sharedInstance.puntos <- points,
            DatabaseProvider.sharedInstance.money_puntos <- money_puntos,
            DatabaseProvider.sharedInstance.order_type <- order_type,
            DatabaseProvider.sharedInstance.address <- address,
            DatabaseProvider.sharedInstance.shipping_cost <- shipping_cost,
            DatabaseProvider.sharedInstance.folio_complete <- folio_complete,
            DatabaseProvider.sharedInstance.time_delivery <- time_delivery,
            DatabaseProvider.sharedInstance.is_paid <- is_paid,
            DatabaseProvider.sharedInstance.allow_process_nopayment <- allow_process_nopayment,
            DatabaseProvider.sharedInstance.est_delivery_hour <- est_delivery_hour,
            DatabaseProvider.sharedInstance.est_ready_hour <- est_ready_hour,
            DatabaseProvider.sharedInstance.is_generic <- is_generic,
            DatabaseProvider.sharedInstance.confirmed <- confirmed,
            DatabaseProvider.sharedInstance.confirm_hour <- confirm_hour,
            DatabaseProvider.sharedInstance.sendNotifications <- sendNotifications,
            DatabaseProvider.sharedInstance.notified <- notified,
            DatabaseProvider.sharedInstance.pickpal_cash_money <- pickpal_cash_money,
            DatabaseProvider.sharedInstance.av_customer_service <- av_customer_service,
            DatabaseProvider.sharedInstance.two_devices <- two_devices,
            DatabaseProvider.sharedInstance.active_customer_service <- active_customer_service,
            
            DatabaseProvider.sharedInstance.total_card <- total_card,
            DatabaseProvider.sharedInstance.total_cash <- total_cash,
            DatabaseProvider.sharedInstance.total_to_pay <- total_to_pay,
            DatabaseProvider.sharedInstance.total_done_payment <- total_done_payment,
            DatabaseProvider.sharedInstance.subtotal <- subtotal
            
            
       
            )
        
        
        
        do {
            try db.run(update)
            print("update success")
        }catch let error{
            print("imposible actualzar  ", error.localizedDescription)
        }
        
    }
    
}
    
    func updateStuff(registerDate: String, orderId: Int, statusItem: String, payTime: String, processTime: String, readyTime: String, isCash: Bool) {
        let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == orderId)
        let update = rowUpdate.update(DatabaseProvider.sharedInstance.process_time <- processTime, DatabaseProvider.sharedInstance.ready_time <- readyTime, DatabaseProvider.sharedInstance.pay_time <- payTime, DatabaseProvider.sharedInstance.statusItem <- statusItem, DatabaseProvider.sharedInstance.is_cash <-  isCash)
        do {
            try db.run(update)
            print("update success")
        }catch let error{
            print("imposible actualizar  ", error.localizedDescription)
        }

    }
    
    func deleteDeliveredOrders() {
        let deletedRow = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "delivered" || DatabaseProvider.sharedInstance.statusItem == "expired" || DatabaseProvider.sharedInstance.statusItem == "canceled" || DatabaseProvider.sharedInstance.statusItem == "refund")
        do {
            try db.run(deletedRow.delete())
            print("update success")
        }catch let error{
            print("imposible eliminar  ", error.localizedDescription)
        }
    }
    
    func deleteRefundedOrders(orderId: Int) {
        let deletedRow = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == orderId )
        do {
            try db.run(deletedRow.delete())
            print("update success")
        }catch let error{
            print("imposible eliminar  ", error.localizedDescription)
        }
    }
    
    func createOrderElements(products: NSArray) -> [Dictionary<String,Any>] {
//        for product in products {
//            print(product)
//            var modifiers = NSArray()
//            var modifiersS = String()
//            var modifiers_price = NSArray()
//            var modifiers_priceS = String()
//
//            var complements = NSArray()
//            var complementsS = String()
//            var complements_price = NSArray()
//            var complement_priceS = String()
//
//            if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
//                modifiers = mod
//            }else{
//                modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
//            }
//
//            if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
//                modifiers_price = mod_price
//            }else{
//                modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
//            }
//
//
//
//            if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
//                complements = comp
//            }else{
//                complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
//            }
//
//            if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
//                complements_price = comp_price
//            }else{
//                complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
//            }
//
//
//
//            if modifiers.count > 0 || complements.count > 0 {
//                var modifP = [Double]()
//                var modifId = [Int]()
//                var modifCat = [String]()
//                var compP = [Double]()
//                var compN = [String]()
//                var compC = [Int]()
//                var compId = [Int]()
//                var compCat = [String]()
//                for j in 0..<modifiers.count {
//                    modifP.append(Double(modifiers_price.object(at: j) as! String)!)
//                    modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
//                    modifCat.append(modifiers_cat.object(at: j) as! String)
//                }
//                var aux = 1
//                var index = -1
//                for z in 0..<complements.count {
//                    if !compN.contains(complements[z] as! String) {
//                        compN.append(complements[z] as! String)
//                        compC.append(1)
//                        aux = 1
//                        compP.append(Double(complements_price[z] as! String)!)
//                        compId.append(Int(complements_id[z] as! String) ?? 0)
//                        compCat.append(complements_cat[z] as! String)
//                        index += 1
//                    }else{
//                        aux += 1
//                        compC[index] = aux
//                        compP[index] = Double(complements_price[z] as! String)! * Double(aux)
//                        compId[index] = (Int(complements_id[z] as! String) ?? 0)
//                        compCat[index] = (complements_cat[z] as! String)
//                    }
//                }
//
//            }else{
//            }
//            totalPrice += (product as! NSDictionary).value(forKey: "price") as! Double
//            i += 1
//        }
        var dictionaryInfo = [Dictionary<String,Any>]()
        var dictionaryModifiersTotal = [Dictionary<String,Any>]()
        var dictionaryComplementsTotal = [Dictionary<String,Any>]()
        var i = 0
        var y = 0
        for product in products {
            var modifiers = NSArray()
            var modifiersS = String()
            var modifiers_price = NSArray()
            var modifiers_priceS = String()
            var modifiers_cat = NSArray()
            var modifiers_catS = String()
            var modifiers_id = NSArray()
            var modifiers_idS = String()
            
            var complements = NSArray()
            var complementsS = String()
            var complements_price = NSArray()
            var complement_priceS = String()
            var complements_cat = NSArray()
            var complements_catS = String()
            var complements_id = NSArray()
            var complements_idS = String()
            
            if let mod = (product as! NSDictionary).value(forKey: "modifiers") as? NSArray {
                modifiers = mod
            }else{
                modifiersS = (product as! NSDictionary).value(forKey: "modifiers") as! String
            }
            
            if let mod_price = (product as! NSDictionary).value(forKey: "modifier_prices") as? NSArray {
                modifiers_price = mod_price
            }else{
                modifiers_priceS = (product as! NSDictionary).value(forKey: "modifier_prices") as! String
            }
            
            if let mod_cat = (product as! NSDictionary).value(forKey: "modifier_category") as? NSArray {
                modifiers_cat = mod_cat
            }else{
                modifiers_catS = (product as! NSDictionary).value(forKey: "modifier_category") as! String
            }
            
            if let mod_id = (product as! NSDictionary).value(forKey: "modifier_ids") as? NSArray {
                modifiers_id = mod_id
            }else{
                modifiers_idS = (product as! NSDictionary).value(forKey: "modifier_ids") as! String
            }

            
            if let comp = (product as! NSDictionary).value(forKey: "complements") as? NSArray {
                complements = comp
            }else{
                complementsS = (product as! NSDictionary).value(forKey: "complements") as! String
            }
            
            if let comp_price = (product as! NSDictionary).value(forKey: "complement_prices") as? NSArray {
                complements_price = comp_price
            }else{
                complement_priceS = (product as! NSDictionary).value(forKey: "complement_prices") as! String
            }
            if let comp_cat = (product as! NSDictionary).value(forKey: "complement_category") as? NSArray {
                complements_cat = comp_cat
            }else{
                complements_catS = (product as! NSDictionary).value(forKey: "complement_category") as! String
            }
            
            if let comp_id = (product as! NSDictionary).value(forKey: "complement_ids") as? NSArray {
                complements_id = comp_id
            }else{
                complements_idS = (product as! NSDictionary).value(forKey: "complement_ids") as! String
            }
            
            if modifiers.count > 0 {
                var modifP = [Double]()
                var modifId = [Int]()
                var modifCat = [String]()
                var compP = [Double]()
                var compN = [String]()
                var compC = [Int]()
                var compId = [Int]()
                var compCat = [String]()
                for j in 0..<modifiers.count {
                    modifP.append(Double(modifiers_price.object(at: j) as! String)!)
                    modifId.append(Int(modifiers_id.object(at: j) as! String) ?? 0)
                    modifCat.append(modifiers_cat.object(at: j) as! String)
                }
                var aux = 1
                var index = -1
                for z in 0..<complements.count {
                    if !compN.contains(complements[z] as! String) {
                        compN.append(complements[z] as! String)
                        compC.append(1)
                        aux = 1
                        compP.append(Double(complements_price[z] as! String)!)
                        compId.append(Int(complements_id[z] as! String) ?? 0)
                        compCat.append(complements_cat[z] as! String)
                        index += 1
                    }else{
                        aux += 1
                        compC[index] = aux
                        compP[index] = Double(complements_price[z] as! String)! * Double(aux)
                        compId[index] = (Int(complements_id[z] as! String) ?? 0)
                        compCat[index] = (complements_cat[z] as! String)
                    }
                }
                
                let dictionaryModifiers = ["modifier_name": modifiers, "modifier_price": modifP, "modifier_id": modifId, "modifier_category":modifCat] as [String : Any]
                dictionaryModifiersTotal.append(dictionaryModifiers)
                let dictionaryComplements = ["complement_name": compN, "complement_price": compP, "complement_cant": compC, "complement_id": compId, "complement_category":compCat] as [String : Any]
                dictionaryComplementsTotal.append(dictionaryComplements)
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity": (product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": true, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": dictionaryComplementsTotal[y], "modifiers": [dictionaryModifiersTotal[y]]] as [String : Any]
                dictionaryInfo.append(dictionary)
                y += 1
            }else{
                let dictionary = ["name": (product as! NSDictionary).value(forKey: "name") as! String, "quantity":(product as! NSDictionary).value(forKey: "quantity") as! Int, "price": (product as! NSDictionary).value(forKey: "price") as! Double, "complex": false, "id":(product as! NSDictionary).value(forKey: "id") as! String , "complements": ["complement_name": complementsS, "complement_price": complement_priceS, "complement_id": complements_idS, "complement_category": complements_catS], "modifiers": ["modifier_name": modifiersS, "modifier_price": modifiers_priceS, "modifier_id": modifiers_idS, "modifier_category": modifiers_catS]] as [String : Any]
                dictionaryInfo.append(dictionary)
            }
            //                    print(dictionaryInfo)
            i += 1
        }
        return dictionaryInfo
    }
    
    func checkStatusOrder(orderId: Int) -> String{
        var statusItem = String()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == orderId))
        for row in query {
            statusItem = try! row.get(DatabaseProvider.sharedInstance.statusItem)
        }
        return statusItem
    }
//    func getTimeToLive() -> Int {
//         let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.statusItem == "to_pay"))
//        for row in query {
//            let
//        }
//    }
    
    func getSinglePedido (order_id: Int) -> [Dictionary<String, Any>]{
        var currentOrder = [Dictionary<String, Any>]()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            print(try? row.get(DatabaseProvider.sharedInstance.order_elements))
            let encoded = try! row.get(DatabaseProvider.sharedInstance.order_elements).data(using: .utf8)
            let dictionary = try? JSONSerialization.jsonObject(with: encoded!, options: .mutableLeaves)
            currentOrder = dictionary as! [Dictionary<String, Any>]
            return currentOrder
        }
        return currentOrder
    }
    
    func getOrderIdStatus(order_id: Int) -> String{
        var status = String()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            status = try! row.get(DatabaseProvider.sharedInstance.statusItem)
           return status
        }
        return status
    }
    
    
    func getOrderIdType(order_id: Int) -> String{
        var typeOrder = String()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            typeOrder = try! row.get(DatabaseProvider.sharedInstance.order_type)
           return typeOrder
        }
        return typeOrder
    }
    
    func getIsConfirmedOrder(order_id: Int) -> Bool{
        var typeOrder = Bool()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            typeOrder = try! row.get(DatabaseProvider.sharedInstance.confirmed)
           return typeOrder
        }
        return typeOrder
    }
    
    
    func getAllowProcessNopaymentOrder(order_id: Int) -> Bool{
        var allowProcessNopaymentOrder = Bool()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            allowProcessNopaymentOrder = try! row.get(DatabaseProvider.sharedInstance.allow_process_nopayment)
           return allowProcessNopaymentOrder
        }
        return allowProcessNopaymentOrder
    }
    
    func getReadyFlag(order_id: Int) -> Bool{
        var status = Bool()
        let query = try! db.prepare(DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == order_id))
        for row in query {
            status = try! row.get(DatabaseProvider.sharedInstance.ready_flag)
            return status
        }
        return status
    }
    
    func getOrderDate (processHour: String, registerDay: String) -> Date{
        let estimated = registerDay + " " + processHour
        let form = DateFormatter()
        form.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var dateRegister = Date()
        
        if form.date(from: estimated) != nil {
            dateRegister = form.date(from: estimated)!
            return dateRegister
        }
        return dateRegister
    }
    
    func getLastDay(date: Date) -> String{
        let form = DateFormatter()
        form.dateFormat = "dd/MM/yy"
        var dateRegister = String()
        
        if form.string(from: date) != nil {
            dateRegister = form.string(from: date)
            return dateRegister
        }
        return dateRegister
    }
    
    func updateStatusNotifications(idOrder: Int, sendNotifications: Bool ){
        
        let rowUpdate = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == idOrder)
        let update = rowUpdate.update(DatabaseProvider.sharedInstance.sendNotifications <- sendNotifications)
        do {
            try db.run(update)
            //                return true
        }catch let error{
            print("imposible insertar ", error.localizedDescription)
           
        }
        
    }
    
    
    
    func getStatusNotifications(idOrder: Int ) -> Bool{
        
        let query = DatabaseProvider.sharedInstance.orders.filter(DatabaseProvider.sharedInstance.order_id == idOrder)
        for row in try! db.prepare(query) {

            return try! row.get(DatabaseProvider.sharedInstance.sendNotifications)

        }
        return false
    }
    
    
}
