//
//  EstablishtmentCustomerService.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 24/03/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation
class EstablishtmentCustomerService : NSObject {
    

    
    var establishment_name: String!
    var business_line: String!
    var table: Int!
    var logo: String!
    var state:String!
    var place:String!
    var establishment_UPPER: String!
    var establishment_id: Int!
    var id_desk_user: Int!
    var status_request_bill: Bool!
    var status_request_waiter: Bool!
    var image: String!
    

}
