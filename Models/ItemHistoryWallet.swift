//
//  ItemHistoryWallet.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 26/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation

class ItemHistoryWallet : NSObject {
    
    
    var id: Int!
    var reference:String!
    var createdAt:String!
    var timeAt:String!
    var establishmentName: String!
    var logo:String!
    var concept:String!
    var conceptInt: Int!
    var amount:String!
    var percentReward: Double!
    var reward:String!
    var total: String!
    
    
}
