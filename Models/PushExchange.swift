//
//  PushExchange.swift
//  PickPalTesting
//
//  Created by Oscar on 28/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//
//  nuevo
import Foundation


class ExchangeDetails: NSObject {

    var amount: Int!
    var id: Int!
    var establishmentidFrom: String!
    var establishmentidTo: String!
    var isCancel: Bool!
    
}
