//
//  PromoEstablishment.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/13/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromoEstablishment : NSObject {
    var id: Int!
    var isOpen: Bool!
    var establishmentCategory: String!
    var place: String!
    var image: String!
    var kitchenType: String!
    var time: String!
    var logo: String!
    var name: String!
    var state: String!
    var municipality: String!
    var futureAvailable: Bool!
    var activeNotifications: Bool!
    var advsCategory: String!
    var subadvsCategory: String!
    var currentPromotionals: Int!
    var rating: Int!
    var status: Int!
    var roleRevision: Int!
    var noProcessed: Int!
    var firstActivationDate: String!
    var billingStartActive: Bool!
    var activationDate: String!
    var createdAt: String!
    var updatedAt: String!
    var isAdvs: Bool!
    var noProfit: Bool!
    var administrativeEmail: String!
    var paymentInfo: Int!
    var geographicalArea: Int!
    var fiscalAddress: Int!
    var socialInformation: Int!
    var itemInactive: String!
    var packageInactive: String!
    var extraInactive: String!
}
