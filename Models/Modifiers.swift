//
//  Modifiers.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 16/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Modifiers: NSObject {
    var modifiersName: String!
    var max_required: Int!
    var modifiers : [modifier_prop]!
    var requiredInfo: Bool!
}
