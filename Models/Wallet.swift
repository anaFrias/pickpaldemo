//
//  Wallet.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 02/02/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation


class Wallet : NSObject {
    

    
    var  id: Int!
    var available_money_to_buy: Double!
    var expiration_date: String!
    var reference: String!
    var money_wallet: Double!
        
    
}
