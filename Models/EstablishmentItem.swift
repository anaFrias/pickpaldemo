//
//  EstablishmentItem.swift
//  PickPal
//
//  Created by Alan Abundis on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class EstablishmentItem : NSObject {
    var id: Int!
    var images: [String]!
    var desc: String!
    var name: String!
    var price: String!
    var rating: Int!
}
