//
//  ExchangeDetails.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 30/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//
//  nuevo
import Foundation

class PushExchange: NSObject {

    var amount: Int!
    var establishmentid_from: Int!
    var identifier: Int!
    var establishmentid_to: Int!
    var is_invite_exchange: Bool!
    var name_establishmentid_from: String!
    var name_establishmentid_to: String!
    
}
