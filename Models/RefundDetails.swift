//
//  RefundDetails.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 03/08/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation

class RefundDetails : NSObject {
    
    var refund_points_money: Double!
    var total: Double!
    var refund_pickpal_cash: Double!
    var refund_points: Double!
    var refund_extra: Double!
    var subtotal: Double!
    var total_aprox_to_refund: Double!
    var fail_percent: Double!
    
    var refund_extra_method: String!
    var type_refund: String!
    var order_type: String!
    var order_number: String!

}
