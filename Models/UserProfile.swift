//
//  UserProfile.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 02/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class UserProfile : NSObject {
    var name: String!
    var email: String?
    var availableAdvs: Bool?
    var genre: String?
    var birthday: String?
    var zipCode: Int?
    var avatar: String?
    var phone: String?
}
