//
//  Invoicing.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 7/24/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class Invoicing: NSObject {
    var order_mode: [Int]!
    var payment_str: String!
    var invoicing_by_pickpal: Bool!
    var establishment_id: Int!
    var schedule_orders: ScheduleOrders!
    var payment_methods: Int!
    var carry_mode: Int!
    var default_reward: Int!
    var number_tables: Int!
    var allow_points_movile: Bool!
    var available_service: [String]!
    var order_payments: NSDictionary!
    var minimum_billing: Double!
    
}
