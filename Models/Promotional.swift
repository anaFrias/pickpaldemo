//
//  Promotional.swift
//  PickPalTesting
//
//  Created by Hector Vela on 11/25/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import Foundation

class Promotional: NSObject {
    var id: Int!
    var establishment: String!
    var category_info: String!
    var name: String!
    var descr: String!
    var deadline: String!
    var image: String!
    var deleted: Bool!
    var Header: String!
    var isPromo: Bool!
    var isEmpty = false
    var isCustomImage: Bool!
}
