//
//  EstablishmentAddress.swift
//  PickPal
//
//  Created by Hector Vela on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class EstablishementAddress : NSObject {
    var street: String!
    var colony: String!
    var city: String!
    var state: String!
    var country: String!
    var zip_code: String!
    var noExt: String!
    var noInt: String!
    var referencePlace:String!
    
}
