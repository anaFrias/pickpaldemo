//
//  Kitchens.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Kitchens : NSObject {
    var image: String!
    var name: String!
    var hide: Bool!
}
