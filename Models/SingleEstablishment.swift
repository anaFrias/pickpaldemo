//
//  SingleEstablishment.swift
//  PickPal
//
//  Created by Hector Vela on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class SingleEstablishment : NSObject {
    var id: Int!
    var latitude: Double!
    var longitude: Double!
    var category: String!
    var place: String!
    var name: String!
    var company_name: String!
    var descripcion: String!
    var phone: String!
    var logo: String!
    var rating: Int!
    var busines: String!
    var gallery: [String]!
    var pickpal_services: [String]!
    var order_types: [String]!
    var kitchen_type: String!
    var operation_schedule: [ScheduleItem]!
    var address: EstablishementAddress!
    var menu: Menu!
    var socialInfo: SocialInformation!
    var generic_service: Bool?
    var service_plus: Bool?
    var reference_location: String!
}

