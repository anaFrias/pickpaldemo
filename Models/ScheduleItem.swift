//
//  ScheduleItem.swift
//  PickPal
//
//  Created by Hector Vela on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class ScheduleItem : NSObject {
    var open_hour: String!
    var closing_hour: String!
    var day: String!
}

