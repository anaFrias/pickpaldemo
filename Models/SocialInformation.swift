//
//  SocialInformation.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/23/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class SocialInformation: NSObject {
    var paymentAllow, averagePrice: String!
    var petFriendly, availableWifi, delivery, availableAlc: Bool!
    var areas: String!
    var availableKids: Bool!
    var environment, music, payment_methods: String!
    var availableSmoke, availableDelivery: Bool!
    var amenities: String!
    var siteURL, instagramURL, facebookURL, twitterURL: String!
}
