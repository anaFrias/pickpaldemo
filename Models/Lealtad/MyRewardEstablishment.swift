//
//  MyRewardEstablishment.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/21/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class MyRewardEstablishment: NSObject {
    
    var establishmentName: String!
    var establishmentId: Int!
    var tipoCambio: Double = 0.0
    var totalMoney: Double = 0.0
    var totalPoints: Double = 0.0
    var minExchange: Double = 0.0
    

}
