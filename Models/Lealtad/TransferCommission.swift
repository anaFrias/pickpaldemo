//
//  TransferCommission.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TransferCommission: NSObject {
    
    var comision_transferencia: Double!
    var comision_intercambio: Double!
    var tipo_cambio: Double!

}
