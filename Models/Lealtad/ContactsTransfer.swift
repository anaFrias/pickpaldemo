//
//  ContactsTransfer.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/10/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ContactsTransfer: NSObject {
    
    var phone: String!
    var name: String!
    var isSelect: Bool!

}
