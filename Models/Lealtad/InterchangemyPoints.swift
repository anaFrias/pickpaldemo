//
//  InterchangemyPoints.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/21/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class InterchangemyPoints: NSObject {
    
    var from_est: String!
    var client: Int!
    var to_estpk: Int!
    var to_est: String!
    var amount: Double!
    var conversionA: Double!
    var conversionB: Double!
              
}
