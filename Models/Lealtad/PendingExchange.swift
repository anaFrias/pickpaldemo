//
//  PendingExchange.swift
//  PickPalTesting
//
//  Created by Hector Vela on 5/19/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PendingExchange: NSObject {
    
    var establishment_end_two: String!
    var establecimient_origin: String!
    var establishment_end_three: String!
    var concepInterchange: String!
    var date: String!
    var establishment_end_one: String!
    var id: Int!
    var amount: Double!
    var leftAmount: Double!
    var current_balance: Double!
    var tax: Double!
    var tax_total: Double!
    var exchange_from: String!
    var exchange_to: [String]!
    var inprocess_exchange: Bool!
    var pending_transfer: Bool!
    var next_balance: Double!
    var money_next_balance: Double!
    var time_at: String!
    var exchange_to_address_array: [String]!
    var left_amount: Double!
    var left_amount_money: Double!
    var exchange_details =  [ExchangeDetails]()
    var address: String!
    var reference: String!
    var total: Double!
    
    
}
