//
//  SingleReward.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class SingleReward: NSObject {
    var id: Int!
    var name: String!
    var logo: String!
    var image: String!
    var phone: String!
    var state: String!
    var place: String!
    var descrip: String!
    var municipality: String!
    var total_points_firts: Int!
    var total_points_second: Int!
    var validity_points_firts: String!
    var validity_points_last: String!
    var establishmentCategory: String!
    var latitude: Double!
    var longitude: Double!
    var totalPoints: Double!
    var total_money: Double!
    var servicePlus: Bool!
    var kitchenType: [String]!
    var pickpal_services: [String]!
    var establishment_gallery: [String]!
    var address: EstablishementAddress!
    var operation_schedule: [ScheduleItem]!
    var socialInfo: SocialInformation!
    var history: [HistoryRewards]!
    var reference_location: String!
    
}
