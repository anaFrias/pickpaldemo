//
//  HistoryRewards.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/20/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class HistoryRewards: NSObject {
    
    var id: Int!
    var created_at: String!
    var concept: String!
    var concept_int: Int!
    var establishment_name: String!
    var total: Double!
    var next_balance: Double!
    var money_next_balance: Double!
    var amount: Double!
    var amount_money: Double!
    var tax: Double!
    var tax_money: Double!
    var status: Int!
    var status_label: String!
    var transfer_to: String!
    var transfer_from: String!
    var exchange_from: String!
    var exchange_to: [String]!
    var status_exchange: Int!
    var left_amount: Double!
    var left_amount_money: Double!
    var pending_transfer: Bool!
    var time_at: String!
    var reference: String!
    var str_tax: String!
    var total_money: Double!
    var exchange_details =  [ExchangeDetails]()
    var exchange_to_address_array: [String]!
    var address: String!
    var is_cancelled: Bool!
    
}
