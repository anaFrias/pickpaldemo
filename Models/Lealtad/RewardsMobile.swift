//
//  RewardsMobile.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/7/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class RewardsMobile: NSObject {
    
    var total_money: Double!
    var total_points: Double!
    var establishment_name: String!
    var logo: String!
    var place: String!
    var state: String!
    var establishment_id: Int!
    var backGroundColor: String!
    var isSelect = false
    var isPrevious = false
    var available = false
    var minExchange: Double!

}
