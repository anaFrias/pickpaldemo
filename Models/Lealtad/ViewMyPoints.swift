//
//  ViewMyPoints.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/29/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ViewMyPoints: NSObject {

    var id: Int!
    var created_at: String!
    var concep: String!
    var concep_int: Int!
    var establishment_name: String!
    var amount: Double! //probablemente puede ser int
    var next_balance: Double!
    var money_amount: Double!
    var money_next_balance: Double!
    var tax: Double!
    var tax_money: Double!
    var name_from: String!
    var phone_client: String!
    var establishment_from: String!
    var transfer_from: String!
    var transfer_to: String!
    var exchange_to: [String]!
    var exchange_from: String!
    var status_label: String!
    var time_at: String!
    var reference: String!
    var str_tax: String!
    var total_money: Double!
    var exchange_details =  [ExchangeDetails]()
    var exchange_to_address_array: [String]!
    var left_amount: Double!
    var left_amount_money: Double!
    var address: String!
    var total: Double!
    var is_cancelled: Bool!
    
}
