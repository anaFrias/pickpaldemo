//
//  OrderQuizItem.swift
//  PickPal
//
//  Created by Alan Abundis on 28/08/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class OrderQuizItem: NSObject {
    var id: Int!
    var delivery_at: String!
    var establishment: String!
}
