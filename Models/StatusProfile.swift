//
//  StatusProfile.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 22/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation


class StatusProfile : NSObject {
    
    var  availableAdvs: Bool!
    var  totalPoints: Double!
    var  totalMoney: Double!
    var  walletAcceptedTerms: Bool!
    var  walletTermnsConditions: String!
    
}
