//
//  Establishment.swift
//  PickPal
//
//  Created by Hector Vela on 09/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Establishment : NSObject {
    var id: Int!
    var is_open: Bool!
    var category: String!
    var place: String!
    var name: String!
    var logo: String!
    var rating: Int!
    var smallImg: String!
    var time: String!
    var future_available: Bool?
    var pickpal_services: [String]!
    var servicePlus: Bool!
    var order_types: [String]!
    var business_area: String!
}
