//
//  ItemSingelWallet.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 28/01/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation




class ItemSingelWallet : NSObject {
    

    
    var  id: Int!
    var name: String!
    var logo: String!
    var descripcion: String!
    var establishmentName: String!
    var address: EstablishementAddress!
    var longitude: Double!
    var latitude: Double!
    var phone: String!
    var state: String!
    var municipality:String!
    var place: String!
    var establishmentCategory: Int!
    var kitchenType: Int!
    var socialnformation: SocialInformation!
    var business_area:String!
    var amountAvailable: Double!
    var rewardPercent: Double!
    var userCurrentAmount: Double!
    var operation_schedule: [ScheduleItem]!
    
    
    
    
}
