//
//  DescriptiveEstablishment.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 08/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class DescriptiveEstablishment: NSObject {
    var latitude: Double!
    var longitude: Double!
    var type_kitchen: String!
    var reference_stablishment_id: Int!
    var version_cks: Int!
}
