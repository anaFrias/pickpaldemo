//
//  Menu.swift
//  PickPal
//
//  Created by Alan Abundis on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class MenuItem : NSObject {
    var name: String!
    var items: [EstablishmentItem]!
}
