//
//  Items.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Items: NSObject {
    var id: Int!
    var images: [String]!
    var name: String!
    var descr: String!
    var price: Double!
    var category: String!
    var complements: [Complements]?
    var modifiers: [Modifiers]?
    var is_package: Bool!
    var prep_time: Int!
    var prep_time_max: Int!
    var rating_neg: Int!
    var rating_pos: Int!
    var season: Int?
    var limit_extra: Int?
    var in_stock: Bool!
    var is_alcoholic: Bool!
}
