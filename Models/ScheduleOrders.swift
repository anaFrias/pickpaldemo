//
//  ScheduleOrders.swift
//  PickPalTesting
//
//  Created by Ana Victoria Frias on 5/10/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class ScheduleOrders : NSObject {
    var finish_hour: String!
    var order_type: Int!
    var is_active: Bool!
    var open_date: String!
    var finish_date: String!
    var pickpal_service_cost: Float!
    
}
