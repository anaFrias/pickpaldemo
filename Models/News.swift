//
//  News.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class News : NSObject {
    var title: String!
    var content: String!
    var image: String!
    var big_image: String!
    var category: String!
    var id: Int?
}

