//
//  Card.swift
//  PickPal
//
//  Created by Alan Abundis on 21/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Card: NSObject {
    var card: String!
    var brand: String!
    var payment_source_id: String!
    var name: String!
}
