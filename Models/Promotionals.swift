//
//  Promotionals.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class Promotionals: NSObject {
    
    var id: Int!
    var establishment_id: Int!
    var image_key: Int!
    var deleted: Bool!
    var editable: Bool!
    var isNews: Bool!
    var headerAdvs: String!
    var name: String!
    var descriptionAdvs: String!
    var start_date: String!
    var end_date: String!
    var image: String!
    var isCustomImage: Bool!
    
}
