//
//  WindsRewards.swift
//  PickPalTesting
//
//  Created by Hector Vela on 4/24/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class WindsRewards: NSObject {
    var points: Double!
    var money_points: Double!
    var order_id: Int!
    var establishment_name: String!
}
