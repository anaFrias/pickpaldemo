//
//  modifier_prop.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class modifier_prop: NSObject {
    var id: Int!
    var modifier_name: String!
    var value: Double!
    var is_active: Bool!
}
