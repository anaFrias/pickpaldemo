//
//  TableInfo.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 07/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class TableInfo: NSObject {
    var latitude: Double!
    var longitude: Double!
    var municipality: String!
    var state: String!
    var establishment_category: String!
    var place: String!
    var establishment_name: String!
    var reference_establishment_id: Int!
    var version_cks: Int!
}
