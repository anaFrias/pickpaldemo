//
//  CountEstablishmentPoints.swift
//  PickPalTesting
//
//  Created by Hector Vela on 31/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class CountEstablishmentPoints: NSObject {
    
    var amount: Double!
    var amount_money: Double!
    var total_amount: Double!
    var total_amount_money: Double!
}
