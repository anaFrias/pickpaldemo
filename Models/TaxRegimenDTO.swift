//
//  TaxRegimenDTO.swift
//  PickPalTesting
//
//  Created by Josue Valdez Morales on 13/01/23.
//  Copyright © 2023 Ana Victoria Frias. All rights reserved.
//

import UIKit

class TaxRegimenDTO: NSObject {
    
    var value: String!
    var key: Int!

}
