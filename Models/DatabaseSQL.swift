//
//  Database.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 04/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
import SQLite

class DatabaseSQL: NSObject {
//    data orders
//    Tabla
    let orders = Table("orders")
//    Columnas
    let id = Expression<Int64>("id")
    let establishment_id = Expression<Int>("establishment_id")
    let establishment_name = Expression<String>("establishment_name")
    let in_site = Expression<Bool>("in_site")
    let notes = Expression<String>("notes")
    let billing_address_id = Expression<Int>("billing_address_id")
    let payment_method = Expression<Int>("payment_method")
    let discount_code = Expression<String>("discount_code")
    let total = Expression<Double>("total")
    let estimated_time = Expression<Int>("estimated_time")
    let pickpal_comission = Expression<Double>("pickpal_comission")
    let total_after_checkout = Expression<Double>("total_after_checkout")
    let order_has_been_sent = Expression<Bool>("order_has_been_sent")
    let code = Expression<String>("code")
    let order_number = Expression<String>("order_number")
    let folio = Expression<String>("folio")
    let register_date = Expression<String>("register_date")
    let process_time = Expression<String>("process_time")
    let ready_time = Expression<String>("ready_time")
    let pay_time = Expression<String>("pay_time")
    let order_id = Expression<Int>("order_id")
    let order_elements = Expression<String>("order_elements")
    let statusItem = Expression<String>("statusItem")
    let qr = Expression<String>("qr")
    let ttl = Expression<String>("ttl")
    let is_cash = Expression<Bool>("is_cash")
    let last4 = Expression<String>("last4")
    let statusController = Expression<String>("statusController")
    let is_scheduled = Expression<Bool>("is_scheduled")
    let in_bar = Expression<Bool>("in_bar")
    let percent = Expression<Double>("percent")
    let number_table = Expression<Int>("number_table")
    let ready_flag = Expression<Bool>("ready_flag")
    let puntos = Expression<Double>("puntos")
    let money_puntos = Expression<Double>("money_puntos")
    let address = Expression<String>("address")
    let order_type = Expression<String>("order_type")
    let shipping_cost = Expression<Double>("shipping_cost")
    let folio_complete = Expression<String>("folio_complete")
    let time_delivery = Expression<String>("time_delivery")
    let to_refund = Expression<Bool>("to_refund")
    let days_elapsed = Expression<Int>("days_elapsed")
    let is_paid = Expression<Bool>("is_paid")
    let allow_process_nopayment = Expression<Bool>("allow_process_nopayment")
    let est_delivery_hour = Expression<String>("est_delivery_hour")
    let est_ready_hour = Expression<String>("est_ready_hour")
    let is_generic = Expression<Bool>("is_generic")
    let confirmed = Expression<Bool>("confirmed")
    let confirm_hour = Expression<String>("confirm_hour")
    let sendNotifications = Expression<Bool>("sendNotifications")
    let notified = Expression<Bool>("notified")
    let pickpal_cash_money = Expression<Double>("pickpal_cash_money")
    let av_customer_service = Expression<Bool>("av_customer_service")
    let two_devices = Expression<Bool>("two_devices")
    let active_customer_service = Expression<Bool>("active_customer_service")
    
    let total_card = Expression<Double>("total_card")
    let total_cash = Expression<Double>("total_cash")
    let total_to_pay = Expression<Double>("total_to_pay")
    let total_done_payment = Expression<Double>("total_done_payment")
    
    let subtotal = Expression<Double>("subtotal")
    
    
    
}
