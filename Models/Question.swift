//
//  Question.swift
//  PickPal
//
//  Created by Alan Abundis on 12/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit
class Question : NSObject {
    var id: Int!
    var question: String!
    var items: [String]!
    var categories: [Int]!
    var labels: [String]!
}
