//
//  SingleEstabAdvs.swift
//  PickPalTesting
//
//  Created by Hector Vela on 1/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import UIKit

class SingleEstabAdvs: NSObject {
    var id: Int!
    var latitude: Double!
    var longitude: Double!
    var category: String!
    var place: String!
    var name: String!
    var company_name: String!
    var descripcion: String!
    var phone: String!
    var logo: String!
    var rating: Int!
    var gallery: [String]!
    var kitchen_type: String!
    var operation_schedule: [ScheduleItem]!
    var address: EstablishementAddress!
    var menu: Menu!
    
    var socialnformation: SocialInformation!
    var advsCategorry, subadvsCategory: String!
    var activeNotifications: Bool!
    var promotionals: [Promotional]!
    var news: [Promotional]!
    var noProfit: Bool!
}
