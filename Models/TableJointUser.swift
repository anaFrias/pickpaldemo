//
//  TableJointUser.swift
//  PickPalTesting
//
//  Created by Oscar Martinez on 26/05/21.
//  Copyright © 2021 Ana Victoria Frias. All rights reserved.
//

import Foundation


class TableJointUser : NSObject {
    
    var numberTable: Int!
    var deskUserId: Int!
    var establishmentId: Int!
    var establishmentName: String!

}
