//
//  EstablishmentsAdvs.swift
//  PickPalTesting
//
//  Created by Dev on 12/24/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class EstablishmentsAdvs: NSObject {

    var id: Int!
    var establishmentName: String!
    var name: String!
    var RFC: String!
    var is_month_payment: Bool!
    var descriptionEstablishment: String!
    
}
