//
//  EstablishmentsGuide.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import Foundation

public class EstablishmentsGuide: NSObject{
    
    var business_area: String!
    var id: Int!
    var place: String!
    var name: String!
    var image: String!
    var state: String!
    var municipality: String!
    var logo: String!
    var service_plus: Bool!
    var kms: String!
    var pickpal_services = [String]()
    var foodTypes = [String]()
    
    
}
