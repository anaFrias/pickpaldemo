//
//  KitchensComplete.swift
//  PickPal
//
//  Created by Hector Vela on 10/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class KitchensComplete : NSObject {
    var image: String!
    var name: String!
    var nearby: Int!
    var ids: [Int]!
    var hide: Bool!
}
