//
//  File.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 10/19/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class MultipleOrders: NSObject {
    var establishment_id: Int!
    var establishment_name: String!
    var order_id: Int!
    var register_date: String!
    var register_date_key: String!
    var register_hour: String!
    var complete_folio: String!
    var qr_code: String!
    var money_points: Double!
    var points: Double!
    var address: String!
    var order_type: String!
    var shipping_cost: Double!
    var folio_order: String!
    var to_refund: Bool!
    var days_elapsed: Int!
    var time_delivery: Int!
    var allow_process_nopayment: Bool!
    var est_delivery_hour: String!
    var est_ready_hour: String!
    var is_generic: Bool!
    var confirmed: Bool!
    var confirm_hour: String!
    var notified: Bool!
    var two_devices: Bool!
    var active_customer_service: Bool!
    
    var total_card: Double!
    var total_cash: Double!
    var total_to_pay: Double!
    var total_done_payment: Double!
    
}
