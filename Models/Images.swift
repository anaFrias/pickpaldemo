//
//  Images.swift
//  PickPalTesting
//
//  Created by Dev on 12/26/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class Images: NSObject {
    var id: Int!
    var image: String!
    var isSelected: Bool!
}
