//
//  BillingAddress.swift
//  PickPal
//
//  Created by Alan Abundis on 15/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class BillingAddress: NSObject {
    var id: Int!
    var RFC: String!
    var email_send: String!
    var full_name: String!
    var tax_regime: Int? = 0
    var zip_code: String? = ""
}
