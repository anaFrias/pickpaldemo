//
//  complement_prop.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 09/07/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class complement_prop: NSObject {
    var complement_name: String!
    var id: Int!
    var value: String!
    var in_stock: Bool!
    var is_active: Bool!
}
