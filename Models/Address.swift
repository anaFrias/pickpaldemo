//
//  Address.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 06/07/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//
//  nuevo
import Foundation


class Address: NSObject {

    var pk: Int!
    var street: String!
    var numeroExterior: String!
    var numeroInterior: String!
    var colony: String!
    var state: String!
    var city: String!
    var zip_code: String!
    var calleReferencia1: String!
    var calleReferencia2: String!
    var referencia: String!
    var latitude: Double!
    var longitude: Double!
    
}
