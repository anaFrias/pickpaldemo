//
//  Places.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 29/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Places: NSObject {
    var placeId: Int!
    var image: String?
    var placeName: String!
    var type: [String]!
    var logo: String!
    var time: String!
}
