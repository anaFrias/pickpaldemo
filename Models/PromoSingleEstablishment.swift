//
//  PromoSingleEstablishment.swift
//  PickPalTesting
//
//  Created by Hector Vela on 12/23/19.
//  Copyright © 2019 Ana Victoria Frias. All rights reserved.
//

import UIKit

class PromoSingleEstablishment : SingleEstablishment {
    var socialnformation: SocialInformation!
    var advsCategorry, subadvsCategory: String!
    var activeNotifications: Bool!
    var promotionals: [Promotional]!
    var exceptionSchedule: [ScheduleItem]!
    var noProfit: Bool!
    var addresss: EstablishementAddress!
    var news: [Promotional]!
}
