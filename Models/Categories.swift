//
//  Categories.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 14/05/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Categories: NSObject {
    var name: String!
    var items: [Items]!
    var include_alcoholic: Bool! 
}
