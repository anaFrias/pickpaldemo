//
//  Orders.swift
//  PickPal
//
//  Created by Alan Abundis on 07/06/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Orders : NSObject {
    var comercio: String!
    var fecha: String!
    var referencia: String!
    var verificador: String!
    var total: String!
    var status_display: String!
    var img: String!
    var category: String!
    var place: String!
    var rating: Int!
    var itemsOrders: [ItemOrders]!
    var stablishment_id: Int!
    var order_id: Int!
    var is_billed: Bool!
    var payment_method: String!
    var in_site: Bool!
    var card: String!
    var pickpal_service: String!
    var status_invoice: String?
    var order_info: NSArray!
    var daily_folio: String!
    var is_scheduled: Bool!
    var in_bar: Bool!
    var propina: String!
    var number_table: Int!
    var delivered: Bool!
    var refund: Bool!
    var subtotal: String!
    var points: Double!
    var new_points: Double!
    var new_points_money: Double!
    var moneyPoints: String!
    var allow_points_movile: Bool!
    var delivery_address: String!
    var type_order: String!
    var shippingCost: Double!
    var folio_order: String!
    var is_refund: Bool!
    var expired_refund: Bool!
    var refundDate: String!
    var confirmed: Bool!
    var pickpal_cash_money: Double!
    var total_card: Double!
    var total_cash: Double!
    var total_to_pay: Double!
    var total_done_payment: Double!
    
    
    
}
