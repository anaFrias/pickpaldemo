//
//  Pedidos.swift
//  PickPal
//
//  Created by Ana Victoria Frias on 11/4/18.
//  Copyright © 2018 Innovation Workshop. All rights reserved.
//

import UIKit

class Pedidos: NSObject {
    var establishment_id: Int!
    var establishment_name: String!
    var in_site: Bool!
    var notes: String?
    var billing_address_id: Int?
    var payment_method: Int?
    var discount_code: String?
    var total: Double!
    var estimated_time: Int!
    var pickpal_comission: Double!
    var total_after_checkout: Double!
    var order_has_been_sent: Bool!
    var code: String!
    var order_number: String?
    var folio: String!
    var register_date: String?
    var process_time: String?
    var ready_time: String?
    var pay_time: String?
    var order_id: Int!
    var order_elements: [Dictionary<String,Any>]!
    var statusItem: String!
    var qr: String!
    var isCash: Bool!
    var last4: String!
    var statusController: String!
    var is_scheduled: Bool!
    var in_bar: Bool!
    var percent: Double!
    var number_table: Int!
    var points: Double!
    var money_puntos: Double!
    var order_type: String!
    var address: String!
    var shipping_cost: Double!
    var folio_complete: String!
    var time_delivery: String!
    var to_refund: Bool!
    var days_elapsed: Int!
    var is_paid: Bool!
    var allow_process_nopayment: Bool!
    var est_delivery_hour: String!
    var est_ready_hour: String!
    var is_generic: Bool!
    var confirmed: Bool!
    var confirm_hour: String!
    var sendNotifications: Bool!
    var notified: Bool!
    var pickpal_cash_money: Double!
    var av_customer_service: Bool!
    var two_devices: Bool!
    var subtotal: Double!
    var active_customer_service: Bool!
    var total_cash: Double!
    var total_to_pay: Double!
    var total_done_payment: Double!
    var total_card: Double!
    
    
    
}
