//
//  TransferDetails.swift
//  PickPalTesting
//
//  Created by IW DEV TEMPO on 03/11/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//
//  nuevo
import Foundation


class TransferDetails: NSObject {

    var aboutOther: String!
    var totalToExchange: Int!
    var establishmentFromName: String!
    var establishmentFromStreet: String!
    var establishmentTomName: String!
    var establishmentTomStreet: String!
    var AboutMe: String!
    
}
