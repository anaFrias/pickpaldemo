//
//  GuideCategories.swift
//  PickPalTesting
//
//  Created by Hector Vela on 3/23/20.
//  Copyright © 2020 Ana Victoria Frias. All rights reserved.
//

import Foundation

public class GuideCategories: NSObject{
    
    var image: String!
    var name: String!
    var hide: Bool!
}
